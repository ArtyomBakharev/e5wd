set GLASSFISH_HOME=E:\work\glassfish3\glassfish

call %GLASSFISH_HOME%\bin\asadmin stop-domain

copy ..\..\lib\mysql-connector-java-5.1.18-bin.jar  %GLASSFISH_HOME%\domains\domain\lib\ext 

call %GLASSFISH_HOME%\bin\asadmin start-domain 

call %GLASSFISH_HOME%\bin\asadmin delete-jdbc-resource  jdbc/e5wd 
call %GLASSFISH_HOME%\bin\asadmin delete-jdbc-connection-pool  e5wd-pool 


call %GLASSFISH_HOME%\bin\asadmin create-jdbc-connection-pool --datasourceclassname com.mysql.jdbc.jdbc2.optional.MysqlDataSource --property user=e5wd:password=e5wd:DatabaseName=e5wd:ServerName=localhost:port=3306 e5wd-pool 
call %GLASSFISH_HOME%\bin\asadmin create-jdbc-resource --connectionpoolid e5wd-pool jdbc/e5wd 
