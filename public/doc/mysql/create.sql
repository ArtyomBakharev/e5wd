CREATE USER 'e5wd'@'localhost' IDENTIFIED BY 'e5wd';
CREATE SCHEMA e5wd CHARACTER SET utf8 COLLATE utf8_general_ci;
GRANT ALL PRIVILEGES ON e5wd.* TO 'e5wd'@'localhost';
