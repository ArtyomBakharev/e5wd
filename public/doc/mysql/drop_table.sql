SET FOREIGN_KEY_CHECKS = 0;

drop table `e5wd`.`nostatusaction`;
drop table `e5wd`.`pre_action_activities`;
drop table `e5wd`.`post_action_activities`;
drop table `e5wd`.`status_change_action`;
drop table `e5wd`.`action`;


drop table `e5wd`.`invoke_script_activity`;
drop table `e5wd`.`invoke_method_activity`;
drop table `e5wd`.`parametrized_property_local_activity`;
drop table `e5wd`.`property_change_descriptor`;
drop table `e5wd`.`set_property_activity`;
drop table `e5wd`.`remote_transaction_activity`;

drop table `e5wd`.`recipients`;
drop table `e5wd`.`cc_recipients`;
drop table `e5wd`.`bcc_recipients`;
drop table `e5wd`.`send_mail_activity`;

drop table `e5wd`.`process_access_list`;
drop table `e5wd`.`status_access_list`;
drop table `e5wd`.`access_entry`;
drop table `e5wd`.`status`;

drop table `e5wd`.`process`;

drop table `e5wd`.`blank_activity`;
drop table `e5wd`.`status_postStatusActivities`;
drop table `e5wd`.`status_preStatusActivities`;
drop table `e5wd`.`status_localActivities`;

SET FOREIGN_KEY_CHECKS = 1;

