package ru.efive.workflow.designer.client;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import ru.efive.workflow.designer.model.*;
import ru.efive.workflow.designer.rest.BatchContainer;
import ru.efive.workflow.designer.rest.BatchResponse;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBException;
import java.util.ArrayList;
import java.util.List;


/**
 *
 */


public class E5wdClient {
    private String strUrl;
    private WebResource resource;


    public E5wdClient(String strUrl){
        this.strUrl = strUrl;
        ClientConfig cc = new DefaultClientConfig();
        cc.getClasses().add(JAXBProvider.class);
        Client client = Client.create(cc);
        resource = client.resource(this.strUrl);
    }

    // @GET
    public BatchResponse postBach(BatchContainer c) throws JAXBException {
        return resource.path("/batch").accept(MediaType.APPLICATION_XML_TYPE+"; charset=utf-8")
                .post(new GenericType<BatchResponse>() { }, c);
    }

// @GET
    public JaxbList<ProcessXml> getProcesses() throws JAXBException {
        return resource.accept(MediaType.APPLICATION_XML_TYPE+"; charset=utf-8")
                .get(new GenericType<JaxbList<ProcessXml>> (){});
    }
// @GET
    public JaxbList<ProcessXml> getProcessesByName(String name) throws JAXBException {
        return resource.queryParam("name",name)
                .accept(MediaType.APPLICATION_XML_TYPE+"; charset=utf-8")
                .get(new GenericType<JaxbList<ProcessXml>> (){});
    }

// @GET  (- Поиск процессов по data type.)
    public JaxbList<ProcessXml> getProcessesByDataType(String dataType) throws JAXBException {
        return resource.queryParam("data_type",dataType)
                .accept(MediaType.APPLICATION_XML_TYPE+"; charset=utf-8")
                .get(new GenericType<JaxbList<ProcessXml>> (){});
    }

// @GET
    public ProcessXml getProcess(String id) throws JAXBException {
        return resource.path(id)
                .accept(MediaType.APPLICATION_XML_TYPE+"; charset=utf-8")
                .get(ProcessXml.class);
    }

// @GET
    public JaxbList<StatusXml> getStatuses(ProcessXml process) throws JAXBException {
        JaxbList<StatusXml> statusXmlJaxbList = null;
        JaxbList<ActivityXml> activityXmlJaxbList = null;

        statusXmlJaxbList =  resource.path(process.getId()+ "/statuses")
                .accept(MediaType.APPLICATION_XML_TYPE+"; charset=utf-8")
                .get(new GenericType<JaxbList<StatusXml>> (){});


        if(statusXmlJaxbList!=null) {
            activityXmlJaxbList =  getActivities(process,null,null);
        }

        if(statusXmlJaxbList!=null && activityXmlJaxbList!=null) {

            addStatusActivities(activityXmlJaxbList,statusXmlJaxbList);

        }

        return statusXmlJaxbList;

    }

    private void addStatusActivities(JaxbList<ActivityXml> activityXmlJaxbList,JaxbList<StatusXml> statusXmlJaxbList){

        for(StatusXml a:statusXmlJaxbList.getItems()) {
            List<ActivityXml> preActivities = new ArrayList<ActivityXml>();
            List<ActivityXml> postActivities = new ArrayList<ActivityXml>();
            List<ActivityXml> localActivities = new ArrayList<ActivityXml>();
            //LocalActivities
            for(Object o : a.getLocalActivities()){
                String id = o.toString();
                for(ActivityXml ac:activityXmlJaxbList.getItems()){
                    if(o.equals(ac.getId())) {
                        localActivities.add(ac);
                        continue;
                    }
                }
            }
            //PreActivities
            for(Object o : a.getPreActivities()){
                String id = o.toString();
                for(ActivityXml ac:activityXmlJaxbList.getItems()){
                    if(o.equals(ac.getId())) {
                        preActivities.add(ac);
                        continue;
                    }
                }
            }
            //PostActivitie
            for(Object o : a.getPostActivities()){
                String id = o.toString();
                for(ActivityXml ac:activityXmlJaxbList.getItems()){
                    if(o.equals(ac.getId())) {
                        postActivities.add(ac);
                        continue;
                    }
                }
            }

            a.setLocalActivities(localActivities);
            a.setPreActivities(preActivities);
            a.setPostActivities(postActivities);
        }

    }

    // @GET
    /*
    * - Получение всех status change action, у которых в from_status_list есть указываемый status id (получение всех доступных действий из текущего статуса)
    */
    public JaxbList<ActionXml> getActions(ProcessXml process, StatusXml byFromStatus) throws JAXBException {
        JaxbList<ActivityXml> activityXmlJaxbList = null;
        JaxbList<ActionXml> actionXmlJaxbList = null;

        if(byFromStatus!=null) {
            actionXmlJaxbList = resource.path(process.getId()+ "/actions")
                .queryParam("ByFromStatus",byFromStatus.getId())
                .accept(MediaType.APPLICATION_XML_TYPE+"; charset=utf-8")
                .get(new GenericType<JaxbList<ActionXml>> (){});

        }
        else {
            actionXmlJaxbList = resource.path(process.getId()+ "/actions")
                    .accept(MediaType.APPLICATION_XML_TYPE+"; charset=utf-8")
                    .get(new GenericType<JaxbList<ActionXml>> (){});
        }

        if(actionXmlJaxbList!=null) {

            activityXmlJaxbList =  getActivities(process,null,null);
            if(activityXmlJaxbList!=null)
                addActionActivities(activityXmlJaxbList,actionXmlJaxbList);

        }

        return   actionXmlJaxbList;

    }


    private  void addActionActivities(JaxbList<ActivityXml> activityXmlJaxbList,JaxbList<ActionXml> actionXmlJaxbList){

        for(ActionXml a:actionXmlJaxbList.getItems()) {
            List<ActivityXml> preActivities = new ArrayList<ActivityXml>();
            List<ActivityXml> postActivities = new ArrayList<ActivityXml>();
            List<ActivityXml> localActivities = new ArrayList<ActivityXml>();
            //LocalActivities
            for(Object o : a.getLocalActivities()){
                String id = o.toString();
                for(ActivityXml ac:activityXmlJaxbList.getItems()){
                    if(o.equals(ac.getId())) {
                        localActivities.add(ac);
                        continue;
                    }
                }
            }
            //PreActivities
            for(Object o : a.getPreActivities()){
                String id = o.toString();
                for(ActivityXml ac:activityXmlJaxbList.getItems()){
                    if(o.equals(ac.getId())) {
                        preActivities.add(ac);
                        continue;
                    }
                }
            }
            //PostActivitie
            for(Object o : a.getPostActivities()){
                String id = o.toString();
                for(ActivityXml ac:activityXmlJaxbList.getItems()){
                    if(o.equals(ac.getId())) {
                        postActivities.add(ac);
                        continue;
                    }
                }
            }

            a.setLocalActivities(localActivities);
            a.setPreActivities(preActivities);
            a.setPostActivities(postActivities);
        }

    }





// @GET
    /*
    * - Получение всех local, pre , post activities для  action (параметр - action id).
    * - Получение всех local, pre status, post activities для объекта status (параметр - status id).
    * */
    public JaxbList<ActivityXml> getActivities(ProcessXml process,
                                               ActionXml byAction,
                                               StatusXml byStatus
    ) throws JAXBException {
        if(byAction!=null){
            return resource.path(process.getId()+ "/activities")
                    .queryParam("ByAction",byAction.getId())
                    .accept(MediaType.APPLICATION_XML_TYPE+"; charset=utf-8")
                    .get(new GenericType<JaxbList<ActivityXml>>() {
                    });
        }else if(byStatus!=null) {
            return resource.path(process.getId()+ "/activities")
                    .queryParam("ByStatus",byStatus.getId())
                    .accept(MediaType.APPLICATION_XML_TYPE+"; charset=utf-8")
                    .get(new GenericType<JaxbList<ActivityXml>>() {
                    });
        } else{
        return resource.path(process.getId()+ "/activities")
                .accept(MediaType.APPLICATION_XML_TYPE+"; charset=utf-8")
                .get(new GenericType<JaxbList<ActivityXml>>() {
                });
        }
    }


// @GET
    /*
    * - Получение status по id
     */
    public StatusXml getStatus(ProcessXml process,StatusXml status ) throws JAXBException {
        JaxbList<ActivityXml> activityXmlJaxbList = null;
        JaxbList<StatusXml> ret = resource.path(process.getId()+ "/statuses")
                .queryParam("id",status.getId())
                .accept(MediaType.APPLICATION_XML_TYPE+"; charset=utf-8")
                .get(new GenericType<JaxbList<StatusXml>> (){})
                ;

        if(ret!=null) {
            activityXmlJaxbList =  getActivities(process,null,status);
        }

        if(ret!=null && activityXmlJaxbList!=null) {

            addStatusActivities(activityXmlJaxbList,ret);

        }

        if(ret!=null && ret.getItems()!=null && ret.getItems().size()>0) return ret.getItems().get(0);
        return null;
    }

// @GET
    /*
    * - Получение Action no  id.
    */
    public ActionXml getAction(ProcessXml process,ActionXml action ) throws JAXBException {
        JaxbList<ActivityXml> activityXmlJaxbList = null;
        JaxbList<ActionXml> ret = resource.path(process.getId()+ "/actions")
                .queryParam("id",action.getId())
                .accept(MediaType.APPLICATION_XML_TYPE+"; charset=utf-8")
                .get(new GenericType<JaxbList<ActionXml>> (){})
        ;

        if(ret!=null) {
            activityXmlJaxbList =  getActivities(process,action,null);
        }

        if(ret!=null && activityXmlJaxbList!=null) {

            addActionActivities(activityXmlJaxbList,ret);

        }

        if(ret!=null && ret.getItems()!=null && ret.getItems().size()>0) return ret.getItems().get(0);
        return null;
    }


}