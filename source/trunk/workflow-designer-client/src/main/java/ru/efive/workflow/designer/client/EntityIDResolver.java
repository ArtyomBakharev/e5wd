package ru.efive.workflow.designer.client;

import com.sun.xml.bind.IDResolver;
import org.apache.log4j.Logger;
import org.xml.sax.SAXException;
import ru.efive.workflow.designer.interfaces.*;
import ru.efive.workflow.designer.interfaces.Process;
import ru.efive.workflow.designer.model.*;

import java.util.concurrent.Callable;


/**
 * EntityIDResolver resolve object from Id
 *
 * @author Dmitry Parshin
 *
 */
public class EntityIDResolver extends IDResolver {

	@Override
	public void bind(final String id, final Object value) throws SAXException {
	}

	@Override
	public Callable<?> resolve(final String id, final Class type) throws SAXException {
		return new Callable<Object>() {
			public Object call() throws Exception {


				if (type == ProcessXml.class) {
                    ProcessXml temp = new ProcessXml();
                    temp.setId(id);
					return temp;
				}
				else if( type == NoStatusActionXml.class)
				{
					NoStatusActionXml temp = new NoStatusActionXml();
                    temp.setId(id);
                    return temp;
                }
				else if(type == StatusChangeActionXml.class)
				{
					StatusChangeActionXml temp=new StatusChangeActionXml();
                    temp.setId(id);
                    return temp;
                }
				else if( type == StatusXml.class)
				{
					StatusXml temp=new StatusXml();
                    temp.setId(id);
                    return temp;
                }

                else if( type == SendMailActivityXml.class)
                {
                    SendMailActivityXml temp=new SendMailActivityXml();
                    temp.setId(id);
                    return temp;
                }
                else if( type == InvokeMethodActivityXml.class)
                {
                    InvokeMethodActivityXml temp=new InvokeMethodActivityXml();
                    temp.setId(id);
                    return temp;
                }
                else if( type == InvokeScriptActivityXml.class)
                {
                    InvokeScriptActivityXml temp=new InvokeScriptActivityXml();
                    temp.setId(id);
                    return temp;
                }
                else if( type == ParametrizedPropertyLocalActivityXml.class)
                {
                    ParametrizedPropertyLocalActivityXml temp=new ParametrizedPropertyLocalActivityXml();
                    temp.setId(id);
                    return temp;
                }
                else if( type == SetPropertyActivityXml.class)
                {
                    SetPropertyActivityXml temp=new SetPropertyActivityXml();
                    temp.setId(id);
                    return temp;
                }
                else if( type == ActivityXml.class)
                {
                    ActivityXml temp=new ActivityXml();
                    temp.setId(id);
                    return temp;
                }
                else if( type == SendMailActivityXml.class)
                {
                    SendMailActivityXml temp=new SendMailActivityXml();
                    temp.setId(id);
                    return temp;
                }
                else if( type == RemoteTransactionActivityXml.class)
                {
                    RemoteTransactionActivityXml temp=new RemoteTransactionActivityXml();
                    temp.setId(id);
                    return temp;
                }

                else if( type == Object.class)
                {
                    return id;
                }

                return null;
			}
		};
	}

}