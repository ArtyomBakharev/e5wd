package ru.efive.workflow.designer.client;

import com.sun.xml.bind.IDResolver;
import org.apache.log4j.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.Provider;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

/**
 * JAXBProvider
 *
 * @author Dmitry Parshin
 *
 */
@Provider
public class JAXBProvider implements MessageBodyReader<Object> {
	EntityIDResolver idresolver = new EntityIDResolver();

	public boolean isReadable(Class<?> type, Type genericType,Annotation[] annotations, MediaType mediaType) {
        return type.isAnnotationPresent(XmlRootElement.class);
	}

	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Object readFrom(Class<Object> type, Type genericType,Annotation[] annotations, MediaType mediaType,
			MultivaluedMap<String, String> httpHeaders, InputStream entityStream)
			throws IOException {
		try {

            JAXBContext jaxb = JAXBContext.newInstance(type);

			Unmarshaller unmarshaller = jaxb.createUnmarshaller();
			unmarshaller.setProperty(IDResolver.class.getName(),idresolver);
			
			Object obj = unmarshaller.unmarshal(entityStream);

			return obj;
		} catch (WebApplicationException e) {
			throw e;
		} catch (Throwable e) {
			throw new WebApplicationException(e);
		}
	}

	public long getSize(Object arg0) {
		return -1;
	}

	public boolean isWriteable(Class<?> type, Type genericType,
			Annotation[] annotations, MediaType mediaType) {
		return type.isAnnotationPresent(XmlRootElement.class);
	}

}