package ru.efive.workflow.designer.client;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Autor: Shashok Pavel
 */
public class JsonProvider {
    private String strUrl;
    protected static Logger log = Logger.getLogger(JsonProvider.class);

    public JsonProvider(String strUrl) {
        this.strUrl = strUrl;
    }

     public List<String> getProcesses()
    {
        try {
            URL connection = new URL(this.strUrl);
            HttpURLConnection urlconn = (HttpURLConnection) connection.openConnection();

            urlconn.setRequestMethod("GET");
            urlconn.setRequestProperty("Content-Type","application/json; charset=utf-8");
            urlconn.connect();

            java.io.InputStream in = urlconn.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in,"utf-8"));
            String line = null;
            List<String> la = new ArrayList<String>();
            while ((line = reader.readLine()) != null)
            {
                la.add(line);
            }
            urlconn.disconnect();
            return la;
        } catch (Exception e) {
            log.error("***** Exception in getStatuses : " + e);
        }
        return null;
    }

    public List<String> getProcess(String processId)
    {
        try {
            URL connection = new URL(this.strUrl+"/"+processId+"/");
            HttpURLConnection urlconn = (HttpURLConnection) connection.openConnection();

            urlconn.setRequestMethod("GET");
            urlconn.setRequestProperty("Content-Type","application/json; charset=utf-8");
            urlconn.connect();

            java.io.InputStream in = urlconn.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in,"utf-8"));
            String line = null;
            List<String> la = new ArrayList<String>();
            while ((line = reader.readLine()) != null)
            {
                la.add(line);
            }
            urlconn.disconnect();
            return la;
        } catch (Exception e) {
            log.error("***** Exception in getStatuses : " + e);
        }
        return null;
    }

    public List<String> getStatuses(String processId)
    {
        try {
            URL connection = new URL(this.strUrl+"/"+processId+"/statuses");
            HttpURLConnection urlconn = (HttpURLConnection) connection.openConnection();

            urlconn.setRequestMethod("GET");
            urlconn.setRequestProperty("Content-Type","application/json; charset=utf-8");
            urlconn.connect();

            java.io.InputStream in = urlconn.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in,"utf-8"));
            String line = null;
            List<String> la = new ArrayList<String>();
            while ((line = reader.readLine()) != null)
            {
                la.add(line);
            }
            urlconn.disconnect();
            return la;
        } catch (Exception e) {
            log.error("***** Exception in getStatuses : " + e);
        }
        return null;
    }

    public List<String> getActions(String processId)
    {
        try {
            URL connection = new URL(this.strUrl+"/"+processId+"/actions");
            HttpURLConnection urlconn = (HttpURLConnection) connection.openConnection();

            urlconn.setRequestMethod("GET");
            urlconn.setRequestProperty("Content-Type","application/json; charset=utf-8");
            urlconn.connect();

            java.io.InputStream in = urlconn.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in,"utf-8"));
            String line = null;
            List<String> la = new ArrayList<String>();
            while ((line = reader.readLine()) != null)
            {
                la.add(line);
            }
            urlconn.disconnect();
            return la;
        } catch (Exception e) {
            log.error("***** Exception in getStatuses : " + e);
        }
        return null;
    }

    public List<String> getActivitys(String processId)
    {
        try {
            URL connection = new URL(this.strUrl+"/"+processId+"/activities");
            HttpURLConnection urlconn = (HttpURLConnection) connection.openConnection();

            urlconn.setRequestMethod("GET");
            urlconn.setRequestProperty("Content-Type","application/json; charset=utf-8");
            urlconn.connect();

            java.io.InputStream in = urlconn.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in,"utf-8"));
            String line = null;
            List<String> la = new ArrayList<String>();
            while ((line = reader.readLine()) != null)
            {
                la.add(line);
            }
            urlconn.disconnect();
            return la;
        } catch (Exception e) {
            log.error("***** Exception in getStatuses : " + e);
        }
        return null;
    }
}
