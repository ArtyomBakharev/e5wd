package ru.efive.workflow.designer.client;

import ru.efive.workflow.designer.model.*;
import ru.efive.workflow.designer.rest.BatchContainer;
import ru.efive.workflow.designer.rest.Operation;
import ru.efive.workflow.designer.rest.ResponseItem;

import javax.xml.bind.JAXBException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with Dmitry Parshin
 * Date: 28.05.12
 */
public class TestClient {



    public static void main(String[] args) {
        E5wdClient e5wdClient = new E5wdClient("http://localhost:8080/workflow-designer-web/rest/processes");
        //E5wdClient e5wdClient = new E5wdClient("http://sibedge.com:1006/workflow-designer-web/rest/processes");
        ProcessXml processXml = null;
        StatusXml statusXml1 = new StatusXml();
        StatusChangeActionXml actionXml1 = new StatusChangeActionXml();
        String processId=null;
        String stutusId=null;
        String actionId=null;


        try {
            // get Process list
            JaxbList<ProcessXml> processXmlJaxbList = e5wdClient.getProcesses();
            if(processXmlJaxbList!=null){
                System.out.println("#### Load Process List");
                for (ProcessXml o : processXmlJaxbList.getItems()) {
                    System.out.println("$$ id - "+o.getId()+" name - "+o.getName());
                    System.out.println("---------------------------------------");
                    processId = o.getId();
                }
            }
            // get Process
            if(processId!=null){
                processXml = e5wdClient.getProcess(processId);
                System.out.println("#### get Process - "+processXml.toString());
            }

            /*
            // get Process list by Name
            JaxbList<ProcessXml> processXmlJaxbList1 = e5wdClient.getProcessesByName("test process2");
            if(processXmlJaxbList!=null){
                System.out.println("#### Load Process List by Name - (test process)");
                for (ProcessXml o : processXmlJaxbList1.getItems()) {
                    System.out.println("$$ id - "+o.getId()+" name - "+o.getName());
                    System.out.println("---------------------------------------");
                }
            }
            // get Process list by Data Type
            JaxbList<ProcessXml> processXmlJaxbList2 = e5wdClient.getProcessesByDataType("test process2");
            if(processXmlJaxbList!=null){
                System.out.println("#### Load Process List by Data Type - (test process)");
                for (ProcessXml o : processXmlJaxbList2.getItems()) {
                    System.out.println("$$ id - "+o.getId()+" name - "+o.getName());
                    System.out.println("---------------------------------------");
                }
            }
            // get Process list by Name (%)
            JaxbList<ProcessXml> processXmlJaxbList3 = e5wdClient.getProcessesByName("test%");
            if(processXmlJaxbList!=null){
                System.out.println("#### Load Process List by Name - (test%)");
                for (ProcessXml o : processXmlJaxbList3.getItems()) {
                    System.out.println("$$ id - "+o.getId()+" name - "+o.getName());
                    System.out.println("---------------------------------------");
                }
            }
            // get Process list by Data Type (%)
            JaxbList<ProcessXml> processXmlJaxbList4 = e5wdClient.getProcessesByDataType("test%");
            if(processXmlJaxbList!=null){
                System.out.println("#### Load Process List by Data Type - (test%)");
                for (ProcessXml o : processXmlJaxbList4.getItems()) {
                    System.out.println("$$ id - "+o.getId()+" name - "+o.getName());
                    System.out.println("---------------------------------------");
                }
            }
            */

            // get Status list
            System.out.println("#### Load Status Process Id = "+processXml.getId());
            processXml.setId(processId);
            JaxbList<StatusXml> statusXmlJaxbList = e5wdClient.getStatuses(processXml);
            if(statusXmlJaxbList!=null){
                for (StatusXml o : statusXmlJaxbList.getItems()) {
                    System.out.println("$$ id - "+o.getId()+" name - "+o.getName());
                    System.out.println("---------------------------------------");
                    System.out.println("--------- LocalActivities "+o.getLocalActivities().toString());
                    //for(ActivityXml a:o.getLocalActivities()) if(a instanceof SendMailActivityXml) System.out.println("--------- LocalActivities SendMailActivityXml "+((SendMailActivityXml)a).toString());
                    System.out.println("--------- PreActivities "+o.getPreActivities().toString());
                    System.out.println("--------- PostActivities "+o.getPostActivities().toString());
                    stutusId = o.getId();
                }
            }

            // get Action list from Process, Status
            System.out.println("#### Load Action list from Process = " + processXml.getId()+" Status = "+stutusId);
            StatusXml statusXml = new StatusXml();
            statusXml.setId(stutusId);
            JaxbList<ActionXml> actionXmlJaxbList = e5wdClient.getActions(processXml,statusXml);
            if(actionXmlJaxbList!=null){
                for (ActionXml o : actionXmlJaxbList.getItems()) {
                    System.out.println("--------- $$ id - "+o.getId()+" name - "+o.getName());
                    System.out.println("--------- LocalActivities "+o.getLocalActivities().toString());
                    System.out.println("--------- PreActivities "+o.getPreActivities().toString());
                    System.out.println("--------- PostActivities "+o.getPostActivities().toString());
                    System.out.println("--------- "+o.toString());
                    if(o instanceof StatusChangeActionXml)  System.out.println("&&&&& getFromStatus().getId()   -   "+((StatusChangeActionXml)o).getFromStatus().getId());
                    if(o instanceof StatusChangeActionXml)  System.out.println("&&&&& getToStatus().getId()   -   "+((StatusChangeActionXml)o).getToStatus().getId());
                    System.out.println("---------------------------------------");
                    actionId = o.getId();
                }
            }

            // get Action list from Process
            System.out.println("#### Load Action list from Process = " + processXml.getId());
            statusXml.setId(stutusId);
            JaxbList<ActionXml> actionXmlJaxbList1 = e5wdClient.getActions(processXml,null);
            if(actionXmlJaxbList!=null){
                for (ActionXml o : actionXmlJaxbList1.getItems()) {
                    System.out.println("--------- $$ id - "+o.getId()+" name - "+o.getName());
                    System.out.println("--------- LocalActivities "+o.getLocalActivities().toString());
                    System.out.println("--------- PreActivities "+o.getPreActivities().toString());
                    System.out.println("--------- PostActivities "+o.getPostActivities().toString());

                    if(o instanceof StatusChangeActionXml)  System.out.println(((StatusChangeActionXml)o).toString());
                    if(o instanceof StatusChangeActionXml)  System.out.println("--------- &&&&& getFromStatus().getId()   -   "+((StatusChangeActionXml)o).getFromStatus().getId());
                    if(o instanceof StatusChangeActionXml)  System.out.println("--------- &&&&& getToStatus().getId()   -   "+((StatusChangeActionXml)o).getToStatus().getId());
                    if(o instanceof NoStatusActionXml)  System.out.println("--------- &&&&& NoStatusActionXml   -   "+((NoStatusActionXml)o).toString());
                    System.out.println("---------------------------------------");
                }
            }

            // get Activity list from Process
            System.out.println("#### Load Activity list from Process id = " + processXml.getId());
            JaxbList<ActivityXml> activityXmlJaxbList = e5wdClient.getActivities(processXml, null, null);
            if(activityXmlJaxbList!=null){
                for (ActivityXml o : activityXmlJaxbList.getItems()) {
                    System.out.println("--------- $$ id - "+o.getId()+" name - "+o.getName());
                    if(o instanceof SendMailActivityXml)  System.out.println(((SendMailActivityXml)o).toString());
                    System.out.println("---------------------------------------");
                }
            }

            // get Activity list from Process, Status
            System.out.println("#### Load Activity list from Process id = " + processXml.getId()+" Status = "+stutusId);
            JaxbList<ActivityXml> activityXmlJaxbList2 = e5wdClient.getActivities(processXml, null, statusXml);
            if(activityXmlJaxbList2!=null){
                for (ActivityXml o : activityXmlJaxbList2.getItems()) {
                    System.out.println("--------- $$ id - "+o.getId()+" name - "+o.getName());
                    if(o instanceof SendMailActivityXml)  System.out.println(((SendMailActivityXml)o).toString());
                    System.out.println("---------------------------------------");
                }
            }
            // get Activity list from Process, Action
           /* System.out.println("#### Load Activity list from Process id = " + processXml.getId()+"  Action = "+actionId);
            StatusChangeActionXml statusChangeActionXml = new StatusChangeActionXml();
            statusChangeActionXml.setId(actionId);
            JaxbList<ActivityXml> activityXmlJaxbList3 = e5wdClient.getActivities(processXml, statusChangeActionXml, null);
            if(activityXmlJaxbList3!=null){
                for (ActivityXml o : activityXmlJaxbList3.getItems()) {
                    System.out.println("--------- $$ id - "+o.getId()+" name - "+o.getName());
                    if(o instanceof SendMailActivityXml)  System.out.println(((SendMailActivityXml)o).toString());
                    System.out.println("---------------------------------------");
                }
            }

            // get Stutus
            if(stutusId!=null){
                System.out.println("--------- #### get Stutus - "+stutusId);
                statusXml1.setId(stutusId);
                statusXml1 = e5wdClient.getStatus(processXml,statusXml1 );
                System.out.println("--------- #### get Stutus name "+statusXml1.getId());
                System.out.println("--------- #### Stutus "+statusXml1.toString());
                System.out.println("--------- LocalActivities "+statusXml1.getLocalActivities().toString());
                System.out.println("--------- PreActivities "+statusXml1.getPreActivities().toString());
                System.out.println("--------- PostActivities "+statusXml1.getPostActivities().toString());
            }

            // get Action
            if(actionId!=null){
                System.out.println("--------- #### get Action - "+actionId);
                actionXml1.setId(actionId);
                actionXml1 = (StatusChangeActionXml) e5wdClient.getAction(processXml, actionXml1);
                System.out.println("--------- #### get Action name "+actionXml1.getName());
                System.out.println("--------- #### get Action "+actionXml1.toString());
                System.out.println("--------- LocalActivities "+actionXml1.getLocalActivities().toString());
                System.out.println("--------- PreActivities "+actionXml1.getPreActivities().toString());
                System.out.println("--------- PostActivities "+actionXml1.getPostActivities().toString());
            }     */

            // Bach response   У элементов необходимо заполнять только id и processId
            System.out.println("---------#### post Bach---------------");
            BatchContainer c = new BatchContainer();
            List<Operation> operations = new ArrayList<Operation>();
            Operation op = new Operation();
            Operation op2 = new Operation();
            StatusXml sxml = e5wdClient.getStatuses(processXml).getItems().get(0);
            ActivityXml axml = e5wdClient.getActivities(processXml, null, null).getItems().get(0);
            op.setElement(sxml);
            op2.setElement(axml);
            operations.add(op);
            operations.add(op2);
            c.setItems(operations);
            for(ResponseItem i : e5wdClient.postBach(c).getItems())   {
                System.out.println("#### post Bach - " + i.getElement().toString());
            }
            System.out.println("---------------------------------------");



            System.out.println("***** JsonProvider testing...");
            JsonProvider p = new JsonProvider("http://sibedge.com:1006/workflow-designer-web/rest/processes");
            System.out.println("***** getProcess(): " + p.getProcess(processId));
            /*System.out.println("***** getProcesses(): " + p.getProcesses());
            System.out.println("***** getStatuses(): " + p.getStatuses(processId));
            System.out.println("***** getActions(): " + p.getActions(processId));
            System.out.println("***** getActivities(): " + p.getActivitys(processId));
            */

        } catch (JAXBException e) {
            e.printStackTrace();
        }



    }

}


/*
Доработка выполнена,
Для Action и Status объекты Activity (local, pre, post) заполняются полностью.
Для этого в методах клиента:
JaxbList<ActionXml> getActions
ActionXml getAction
JaxbList<StatusXml> getStatuses
StatusXml getStatus

Выполняется два запроса к серверу, одит на получение самих объектов, второй на получение их Activity, далее в цикле Activity распределяются по объектам.

Распределения списка  Activity (local, pre, post) по типам с помощью   instanceof

*/