package ru.efive.workflow.designer.interfaces;

/**
 * AbstractEntity interface
 *
 * @author Shashok Pavel
 *
 */
public interface AbstractEntity {
	
	public String getId();
	public void setId(String id);

}
