package ru.efive.workflow.designer.interfaces;


/**
 * AccessControlEntry interface
 *
 * @author Shashok Pavel
 *
 */
public interface AccessControlEntry {
	
	public String getSid();

	public void setSid(String sid);

	public int getPermission();

	public void setPermission(int permission);

	public boolean hasPermission(int permission);

	public void grantPermission(int permission);

	public void revokePermission(int permission);

}
