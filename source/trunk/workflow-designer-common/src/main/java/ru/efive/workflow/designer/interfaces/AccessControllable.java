package ru.efive.workflow.designer.interfaces;

import java.util.List;

/**
 * AccessControllable interface
 *
 * @author Shashok Pavel
 *
 */
public interface AccessControllable {
	
	public List<? extends AccessControlEntry> getAcl();

	public void setAcl(List<? extends AccessControlEntry> acl);

}
