package ru.efive.workflow.designer.interfaces;

import java.util.List;

/**
 * Action interface
 *
 * @author Shashok Pavel
 *
 */
public interface Action extends BaseEntity {
	
	public List<? extends Activity> getPreActivities();

	public void setPreActivities(List<? extends Activity> preActivities);

	public List<? extends Activity> getPostActivities();

	public void setPostActivities(List<? extends Activity> postActivities);

    public List<? extends Activity> getLocalActivities();

    public void setLocalActivities(List<? extends Activity> localActivities);
	
	public String getDataType();

	public void setDataType(String dataType);

	public String getAvailabilityCondition();

	public void setAvailabilityCondition(String availabilityCondition);

	public String getEvaluationMessage();

	public void setEvaluationMessage(String evaluationMessage);

	public boolean isHistoryEnabled();

	public void setHistoryEnabled(boolean historyEnabled);
	
	public boolean isAutocommitEnabled();

	public void setAutocommitEnabled(boolean autocommitEnabled);

}
