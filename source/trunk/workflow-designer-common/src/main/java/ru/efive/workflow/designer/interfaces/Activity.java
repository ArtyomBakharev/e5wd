package ru.efive.workflow.designer.interfaces;

/**
 * Activity interface
 *
 * @author Shashok Pavel
 *
 */
public interface Activity extends BaseEntity {
	
	public String getRunCondition();

	public void setRunCondition(String runCondition);

	public ReturnType getReturnType();

	public void setReturnType(ReturnType returnType);

	public String getReturnFormula();

	public void setReturnFormula(String returnFormula);

}
