package ru.efive.workflow.designer.interfaces;

/**
 * BaseEntity interface
 *
 * @author Shashok Pavel
 */
public interface BaseEntity extends NamedEntity  {
	
	public Process getProcess();

	public void setProcess(Process process);

}
