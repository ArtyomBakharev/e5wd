package ru.efive.workflow.designer.interfaces;

public enum DataType {
	
	STRING,

	INTEGER,

	DECIMAL;

}
