package ru.efive.workflow.designer.interfaces;

/**
 * PropertyChangeDescriptor interface
 *
 * @author Shashok Pavel
 */
public interface EditableProperty extends PropertyChangeDescriptor {

    public int getScope();

    public void setScope(int scope);
}