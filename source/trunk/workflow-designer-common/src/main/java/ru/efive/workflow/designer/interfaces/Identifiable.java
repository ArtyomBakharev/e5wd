package ru.efive.workflow.designer.interfaces;

/**
 * Indicates that class implementing this interface is capable of being identified 
 *
 * @author Sergey Zaytsev
 *
 */
public interface Identifiable {

	
	/**
	 * Returns the identifier of an object
	 * 
	 * @return the ID of an object
	 */
	public String getId();
}
