package ru.efive.workflow.designer.interfaces;


/**
 * InvokeMethodActivity interface
 *
 * @author Shashok Pavel
 */
public interface InvokeMethodActivity  extends SetPropertyActivity {
	
	public String getClassName();

	public void setClassName(String className);

	public String getMethodName();

	public void setMethodName(String methodName);

}
