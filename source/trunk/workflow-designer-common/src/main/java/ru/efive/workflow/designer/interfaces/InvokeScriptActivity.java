package ru.efive.workflow.designer.interfaces;

/**
 * Created by IntelliJ IDEA.
 * User: spa
 * Date: 17.05.12
 * Time: 15:35
 * To change this template use File | Settings | File Templates.
 */
public interface InvokeScriptActivity extends Activity   {

    public String getScript();
    public void setScript(String scr);

}
