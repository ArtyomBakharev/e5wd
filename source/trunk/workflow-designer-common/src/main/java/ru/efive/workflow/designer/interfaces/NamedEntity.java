package ru.efive.workflow.designer.interfaces;

/**
 * NamedEntity interface
 *
 * @author Shashok Pavel
 */
public interface NamedEntity extends AbstractEntity {
	
	public String getMetadata();

	public void setMetadata(String metadata);

	public String getName();

	public void setName(String name);

	public String getDescription();

	public void setDescription(String description);

}
