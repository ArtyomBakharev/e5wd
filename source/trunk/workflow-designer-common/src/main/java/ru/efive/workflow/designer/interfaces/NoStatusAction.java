package ru.efive.workflow.designer.interfaces;

/**
 * NoStatusAction interface
 *
 * @author Shashok Pavel
 */
public interface NoStatusAction extends Action  {

}
