package ru.efive.workflow.designer.interfaces;


import java.util.List;

/**
 * ParametrizedPropertyLocalActivity interface
 *
 * @author Shashok Pavel
 */
public interface ParametrizedPropertyLocalActivity extends Activity  {
	
	public String getForm();

	public void setForm(String form);

    public List<? extends EditableProperty> getPropertyChanges();

    public void setPropertyChanges(List<? extends EditableProperty> propertyChanges);

}
