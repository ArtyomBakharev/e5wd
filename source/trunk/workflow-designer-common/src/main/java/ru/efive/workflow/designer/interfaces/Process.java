package ru.efive.workflow.designer.interfaces;


/**
 * Process interface
 *
 * @author Shashok Pavel
 */
public interface Process extends NamedEntity,AccessControllable {
	
	public int getRepository();
	
	public void setRepository(int repository);

    public String getDataType();

    public void setDataType(String dataType);

    public String getInitialStatusId();

    public void setInitialStatusId(String statusId);

}
