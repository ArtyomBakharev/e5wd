package ru.efive.workflow.designer.interfaces;

/**
 * PropertyChangeDescriptor interface
 *
 * @author Shashok Pavel
 */
public interface PropertyChangeDescriptor {

    public String getName();

    public void setName(String name);

    public String getValue();

    public void setValue(String value);

}
