package ru.efive.workflow.designer.interfaces;

/**
 * Created by IntelliJ IDEA.
 * User: spa
 * Date: 11.05.12
 * Time: 10:57
 * To change this template use File | Settings | File Templates.
 */
public interface RemoteTransactionActivity extends Activity  {
    
    public String getActionProcessId();
    public void setActionProcessId(String p);

    public String getActionId();
    public void setActionId(String a);

    public String getDataType();
    public void setDataType(String s);

    public String getDataSearchScript();
    public void setDataSearchScript(String s);


}
