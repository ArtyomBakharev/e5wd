package ru.efive.workflow.designer.interfaces;


import java.util.List;

/**
 * SendMailActivity interface
 *
 * @author Shashok Pavel
 */
public interface SendMailActivity extends Activity {
	
	public List<String> getRecipients();

	public void setRecipients(List<String> recipients);

	public List<String> getCcRecipients();

	public void setCcRecipients(List<String> ccRecipients);
	
	public List<String> getBccRecipients();

	public void setBccRecipients(List<String> bccRecipients);
	
	public String getSubject();

	public void setSubject(String subject);

	public String getContent();

	public void setContent(String content);

	public String getCharset();

	public void setCharset(String charset);

}
