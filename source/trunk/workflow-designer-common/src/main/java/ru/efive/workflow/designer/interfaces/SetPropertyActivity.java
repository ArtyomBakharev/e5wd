package ru.efive.workflow.designer.interfaces;

import java.util.List;

/**
 * SetPropertyActivity interface
 *
 * @author Shashok Pavel
 */
public interface SetPropertyActivity extends Activity  {
	
	public List<? extends PropertyChangeDescriptor> getPropertyChanges();

	public void setPropertyChanges(List<? extends PropertyChangeDescriptor> propertyChanges);

	
}
