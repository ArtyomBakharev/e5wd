package ru.efive.workflow.designer.interfaces;

import java.util.List;


/**
 * Status interface
 *
 * @author Shashok Pavel
 */
public interface Status extends BaseEntity  {
	
	public String getAlias();

	public void setAlias(String alias);

	public String getDataType();

	public void setDataType(String dataType);

	public List<? extends Activity> getLocalActivities();

	public void setLocalActivities(List<? extends Activity> localActivities) ;

	public List<? extends Activity> getPreActivities();

	public void setPreActivities(List<? extends Activity> preActivities) ;

	public List<? extends Activity> getPostActivities();

	public void setPostActivities(List<? extends Activity> postActivities);

	public List<? extends AccessControlEntry> getAcl();
	
	public void setAcl(List<? extends AccessControlEntry> acl);

}
