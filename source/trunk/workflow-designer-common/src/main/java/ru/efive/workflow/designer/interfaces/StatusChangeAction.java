package ru.efive.workflow.designer.interfaces;


/**
 * StatusChangeAction interface
 *
 * @author Shashok Pavel
 */
public interface StatusChangeAction  extends Action {
	
	public Status getFromStatus();

	public void setFromStatus(Status fromStatus);

	public Status getToStatus();

	public void setToStatus(Status toStatus);

}
