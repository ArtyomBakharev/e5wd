package ru.efive.workflow.designer.model;

import ru.efive.workflow.designer.interfaces.AbstractEntity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;

/**
 * Meant to be extended by concrete entity implementations.
 *
 * @author Sergey Zaytsev
 *
 */
@XmlTransient
@SuppressWarnings("serial")
public abstract class AbstractEntityXml implements Serializable, AbstractEntity {

	@XmlID
	@XmlElement(name = "id", required = true)
	private String id;

	public AbstractEntityXml() {
	}

	public AbstractEntityXml(String id) {
		this.id = id;
	}

	@Override
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

    @Override
    public String toString() {
        return "AbstractEntityXml{" +
                "id='" + id + '\'' +
                '}';
    }
}
