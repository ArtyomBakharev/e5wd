package ru.efive.workflow.designer.model;

import ru.efive.workflow.designer.interfaces.AccessControlEntry;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;



@XmlType(name = "access_entry", propOrder = { "sid", "permission" })
public class AccessControlEntryXml implements Serializable, AccessControlEntry {

	private static final long serialVersionUID = 1L;

	public static final int PERM_READ = 1;

	public static final int PERM_WRITE = 2;

	public static final int PERM_EXEC = 4;

	// security identity, either principal or authority
	@XmlElement(name = "accessor")
	private String sid;

	@XmlElement(name = "level")
	private int permission;

	public AccessControlEntryXml() {
	}

	/**
	 * Constructs {@link AccessControlEntryXml} with specified SID and permission values
	 *
	 * @param sid
	 * @param permission
	 */
	public AccessControlEntryXml(String sid, int permission) {
		this.sid = sid;
		this.permission = permission;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public int getPermission() {
		return permission;
	}

	public void setPermission(int permission) {
		this.permission = permission;
	}

	public boolean hasPermission(int permission) {
		return (this.permission & permission) != 0;
	}

	public void grantPermission(int permission) {
		this.permission |= permission;
	}

	public void revokePermission(int permission) {
		this.permission &= ~permission;
	}
	
	public void fromInterface(AccessControlEntry i)
	{
		this.setPermission(i.getPermission());
		this.setSid(i.getSid());
	}

    @Override
    public String toString() {
        return "AccessControlEntryXml{" +
                "sid='" + sid + '\'' +
                ", permission=" + permission +
                '}';
    }
}