package ru.efive.workflow.designer.model;

import ru.efive.workflow.designer.interfaces.Action;
import ru.efive.workflow.designer.interfaces.Activity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlTransient;
import java.util.ArrayList;
import java.util.List;

/**
 * Abstract class representing an action as a part of workflow
 *
 * @author Sergey Zaytsev
 *
 */
@XmlTransient
@SuppressWarnings("serial")
public abstract class ActionXml extends BaseEntityXml implements Action {

	@XmlElement(name = "data_type")
	private String dataType;

	@XmlElement(name = "availability_condition")
	private String availabilityCondition;

	@XmlList
	@XmlIDREF
	@XmlElement(name = "pre_action_activities")
	private List<ActivityXml> preActivities = new ArrayList<ActivityXml>();

	@XmlList
	@XmlIDREF
	@XmlElement(name = "post_action_activities")
	private List<ActivityXml> postActivities = new ArrayList<ActivityXml>();

    @XmlList
    @XmlIDREF
    @XmlElement(name = "local_action_activities")
    private List<ActivityXml> localActivities = new ArrayList<ActivityXml>();

	@XmlElement(name = "evaluation_message")
	private String evaluationMessage;

	@XmlElement(name = "is_history_enabled")
	private boolean historyEnabled;

	@XmlElement(name = "is_autocommit_enabled")
	private boolean autocommitEnabled;

	public List<ActivityXml> getPreActivities() {
		return preActivities;
	}

	@SuppressWarnings("unchecked")
	public void setPreActivities(List<? extends Activity> preActivities) {
		this.preActivities = (List<ActivityXml>) preActivities;
	}

	public List<ActivityXml> getPostActivities() {
		return postActivities;
	}

	@SuppressWarnings("unchecked")
	public void setPostActivities(List<? extends Activity> postActivities) {
		this.postActivities = (List<ActivityXml>) postActivities;
	}

    @Override
    public List<ActivityXml> getLocalActivities() {
        return localActivities;
    }

    @Override
    public void setLocalActivities(List<? extends Activity> localActivities) {
        this.localActivities  = (List<ActivityXml>) localActivities;
    }

    public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getAvailabilityCondition() {
		return availabilityCondition;
	}

	public void setAvailabilityCondition(String availabilityCondition) {
		this.availabilityCondition = availabilityCondition;
	}

	public String getEvaluationMessage() {
		return evaluationMessage;
	}

	public void setEvaluationMessage(String evaluationMessage) {
		this.evaluationMessage = evaluationMessage;
	}

	public boolean isHistoryEnabled() {
		return historyEnabled;
	}

	public void setHistoryEnabled(boolean historyEnabled) {
		this.historyEnabled = historyEnabled;
	}

	public boolean isAutocommitEnabled() {
		return autocommitEnabled;
	}

	public void setAutocommitEnabled(boolean autocommitEnabled) {
		this.autocommitEnabled = autocommitEnabled;
	}
	
	public void fromInterface(Action i)
	{
		super.fromInterface(i);
		this.setDataType(i.getDataType());
		this.setAvailabilityCondition(i.getAvailabilityCondition());
		this.setEvaluationMessage(i.getEvaluationMessage());
		this.setHistoryEnabled(i.isHistoryEnabled());
		this.setAutocommitEnabled(i.isAutocommitEnabled());
		
		List<? extends Activity> la = i.getPostActivities();
		postActivities.clear();
		for(Activity a : la){
			ActivityXml ax = new ActivityXml();
			ax.fromInterface(a);
			postActivities.add(ax);						
		}
		
		la = i.getPreActivities();
		preActivities.clear();
		for(Activity a : la){
			ActivityXml ax = new ActivityXml();
			ax.fromInterface(a);
			preActivities.add(ax);						
		}

        la = i.getLocalActivities();
        localActivities.clear();
        for(Activity a : la){
            ActivityXml ax = new ActivityXml();
            ax.fromInterface(a);
            localActivities.add(ax);
        }
		
		
	}

    @Override
    public boolean isCorrect()
    {
        if(super.isCorrect() == false)
            return false;
        
        for(ActivityXml a : preActivities)  {
            if( !a.getProcess().getId().equals( process.getId() ) )     {
                log.error("$$$ incorrect preActivities ActivityXml a = " + a);
                return false;
            }
        }
        for(ActivityXml a : postActivities)  {
            if( !a.getProcess().getId().equals( process.getId() ) )     {
                log.error("$$$ incorrect postActivities ActivityXml a = " + a);
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString() {
        return "ActionXml{" +
                "dataType=" + dataType +
                ", availabilityCondition='" + availabilityCondition + '\'' +
                ", preActivities=" + preActivities +
                ", postActivities=" + postActivities +
                ", evaluationMessage='" + evaluationMessage + '\'' +
                ", historyEnabled=" + historyEnabled +
                ", autocommitEnabled=" + autocommitEnabled +
                "} " + super.toString();
    }
}
