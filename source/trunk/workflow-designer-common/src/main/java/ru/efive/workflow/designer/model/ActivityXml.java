package ru.efive.workflow.designer.model;

import ru.efive.workflow.designer.interfaces.Activity;
import ru.efive.workflow.designer.interfaces.ReturnType;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Base class for representing a particular activity involved into workflow
 *
 * @author Sergey Zaytsev
 *
 */
@XmlRootElement(name = "blank_activity")
@XmlType(name = "blank_activity", propOrder = { "id", "name", "process", "runCondition",
		"returnType", "returnFormula", "description", "metadata" })
public class ActivityXml extends BaseEntityXml implements Activity {

	private static final long serialVersionUID = 1L;


	@XmlElement(name = "run_condition")
	private String runCondition;

	@XmlElement(name = "return_type")
	private ReturnType returnType;

	@XmlElement(name = "return_formula")
	private String returnFormula;

	public String getRunCondition() {
		return runCondition;
	}

	public void setRunCondition(String runCondition) {
		this.runCondition = runCondition;
	}

	public ReturnType getReturnType() {
		return returnType;
	}

	public void setReturnType(ReturnType returnType) {
		this.returnType = returnType;
	}

	public String getReturnFormula() {
		return returnFormula;
	}

	public void setReturnFormula(String returnFormula) {
		this.returnFormula = returnFormula;
	}
	
	public void fromInterface(Activity i)
	{
		super.fromInterface(i);
		this.setReturnFormula(i.getReturnFormula());
		this.setReturnType(i.getReturnType());
		this.setRunCondition(i.getRunCondition());

	}

    @Override
    public boolean isCorrect()
    {
        if(super.isCorrect() == false)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "ActivityXml{" +
                "runCondition='" + runCondition + '\'' +
                ", returnType=" + returnType +
                ", returnFormula='" + returnFormula + '\'' +
                "} " + super.toString();
    }
}
