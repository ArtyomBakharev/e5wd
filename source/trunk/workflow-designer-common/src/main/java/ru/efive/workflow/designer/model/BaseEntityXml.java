package ru.efive.workflow.designer.model;

import org.apache.log4j.Logger;
import ru.efive.workflow.designer.interfaces.BaseEntity;
import ru.efive.workflow.designer.interfaces.Process;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Represents a bases abstraction
 *
 * @author Pavel Shashok
 *
 */

@XmlTransient
@SuppressWarnings("serial")
public class BaseEntityXml extends NamedEntityXml implements BaseEntity {
    protected static Logger log = Logger.getLogger(NamedEntityXml.class);
	
	@XmlElement(name = "process")
	@XmlIDREF
	ProcessXml process;

	public Process getProcess() {
		return (Process) process;
	}

	public void setProcess(Process process) {
		this.process = (ProcessXml) process;
	}
	
	public void fromInterface(BaseEntity i)
	{
		super.fromInterface(i);
        ProcessXml temp = new ProcessXml();
        temp.fromInterface(i.getProcess());
		this.setProcess(temp);
	}

    public boolean isCorrect()
    {
        if(process == null || process.getId() == null)   {
            log.error("$$$ incorrect process");
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BaseEntityXml{" +
                "process=" + process +
                "} " + super.toString();
    }
}
