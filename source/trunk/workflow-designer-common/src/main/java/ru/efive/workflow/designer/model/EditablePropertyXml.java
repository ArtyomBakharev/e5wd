package ru.efive.workflow.designer.model;

import ru.efive.workflow.designer.interfaces.EditableProperty;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 *
 * @author Shashok Pavel
 *
 */
@XmlType(name = "editable_property")
public class EditablePropertyXml  extends PropertyChangeDescriptorXml implements EditableProperty {

    @XmlElement(name = "scope")
    private int scope;
    
    @Override
    public int getScope() {
        return scope;
    }

    @Override
    public void setScope(int scope) {
        this.scope = scope;
    }

    public void fromInterface(EditableProperty i)
    {
        super.fromInterface(i);
        this.setScope(i.getScope());
    }

    @Override
    public String toString() {
        return "EditablePropertyXml{" +
                "scope=" + scope +
                "} " + super.toString();
    }
}
