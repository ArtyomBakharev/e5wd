package ru.efive.workflow.designer.model;

import ru.efive.workflow.designer.interfaces.Identifiable;

/**
 * Indicates that class implementing this interface is capable of being identified 
 *
 * @author Sergey Zaytsev
 *
 */
public interface IdentifiableXml extends Identifiable{

	
	/**
	 * Returns the identifier of an object
	 * 
	 * @return the ID of an object
	 */
	public String getId();
}
