package ru.efive.workflow.designer.model;

import ru.efive.workflow.designer.interfaces.InvokeMethodActivity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 
 * 
 * @author Pavel Shashok
 * 
 */
@XmlType(name = "invoke_method_activity")
@XmlRootElement(name = "invoke_method_activity")
public class InvokeMethodActivityXml extends SetPropertyActivityXml implements InvokeMethodActivity {

	private static final long serialVersionUID = 1L;
	
	@XmlElement(name = "class_name")
	private String className;
	
	@XmlElement(name = "method_name")
	private String methodName;

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}
	
	public void fromInterface(InvokeMethodActivity i)
	{
		super.fromInterface(i);
		this.setClassName(i.getClassName());
		this.setMethodName(i.getMethodName());
	}

    @Override
    public boolean isCorrect()
    {
        if(super.isCorrect() == false)
            return false;

        return true;
    }


    @Override
    public String toString() {
        return "InvokeMethodActivityXml{" +
                "className='" + className + '\'' +
                ", methodName='" + methodName + '\'' +
                "} " + super.toString();
    }
}
