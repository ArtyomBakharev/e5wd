package ru.efive.workflow.designer.model;

import ru.efive.workflow.designer.interfaces.InvokeScriptActivity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * InvokeScriptActivityXml implements InvokeScriptActivity
 *
 * @author Pavel Shashok
 *
 */
@XmlType(name = "invoke_script_activity")
@XmlRootElement(name = "invoke_script_activity")
public class InvokeScriptActivityXml extends ActivityXml implements InvokeScriptActivity {

    @XmlElement(name = "script", required = true)
    String script;
    
    @Override
    public String getScript() {
        return script; 
    }

    @Override
    public void setScript(String scr) {
        script = scr;
    }

    public void fromInterface(InvokeScriptActivity i)
    {
        super.fromInterface(i);
        setScript(i.getScript());
    }

    @Override
    public boolean isCorrect()
    {
        if(super.isCorrect() == false)
            return false;

        return true;
    }

    @Override
    public String toString() {
        return "InvokeScriptActivityXml{" +
                "script='" + script + '\'' +
                "} " + super.toString();
    }
}
