package ru.efive.workflow.designer.model;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * JaxbList
 *
 * @author Pavel Shashok
 *
 */
@XmlRootElement(name = "items")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso({ProcessXml.class, ActionXml.class, StatusChangeActionXml.class, NoStatusActionXml.class, BaseEntityXml.class,
	StatusXml.class, ActivityXml.class, SendMailActivityXml.class, SetPropertyActivityXml.class, InvokeMethodActivityXml.class
	,ParametrizedPropertyLocalActivityXml.class, RemoteTransactionActivityXml.class, InvokeScriptActivityXml.class, Object.class})
public class JaxbList<T>  {
	
	@XmlAnyElement(lax=true)
   	private List<T> items;
	
	public List<T> getItems() {
		if (items == null) {
			items = new ArrayList<T>();
		}

		return items;
	}

	public void setItems(List<T> items) {
		this.items = items;
	}

}
