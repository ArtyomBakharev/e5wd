package ru.efive.workflow.designer.model;

import ru.efive.workflow.designer.interfaces.NamedEntity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Represents a named abstraction
 *
 * @author Sergey Zaytsev
 *
 */
@XmlTransient
@SuppressWarnings("serial")
public abstract class NamedEntityXml extends AbstractEntityXml implements NamedEntity {

	@XmlElement(name = "name", required = true, nillable = false)
	private String name;

	@XmlElement(name = "commentary")
	private String description;
	
	@XmlElement(name = "metadata")
	private String metadata;

	public String getMetadata() {
		return metadata;
	}

	public void setMetadata(String metadata) {
		this.metadata = metadata;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public void fromInterface(NamedEntity i)
	{
		this.setId(i.getId());
		this.setDescription(i.getDescription());
		this.setMetadata(i.getMetadata());
		this.setName(i.getName());
	}

    @Override
    public String toString() {
        return "NamedEntityXml{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", metadata='" + metadata + '\'' +
                "} " + super.toString();
    }
}
