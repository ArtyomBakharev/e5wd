package ru.efive.workflow.designer.model;

import ru.efive.workflow.designer.interfaces.NoStatusAction;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * The {@code NoStatusAction} class represents an action that do not changes status of a process
 *
 * @author Sergey Zaytsev
 *
 */
@XmlRootElement(name = "no_status_action")
@XmlType(name = "no_status_action", propOrder = {
		"id", "name", "process","dataType", "availabilityCondition", "preActivities", "postActivities", "localActivities",
		"evaluationMessage", "historyEnabled", "autocommitEnabled", "description", "metadata"
})
public class NoStatusActionXml extends ActionXml implements NoStatusAction {

	private static final long serialVersionUID = 1L;

    @Override
    public boolean isCorrect()
    {
        if(super.isCorrect() == false)
            return false;

        return true;
    }


}
