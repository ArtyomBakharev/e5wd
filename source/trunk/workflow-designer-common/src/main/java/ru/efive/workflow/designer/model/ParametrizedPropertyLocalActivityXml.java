package ru.efive.workflow.designer.model;



import ru.efive.workflow.designer.interfaces.EditableProperty;
import ru.efive.workflow.designer.interfaces.ParametrizedPropertyLocalActivity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 * 
 * @author Pavel Shashok
 * 
 */
@XmlType(name = "parametrized_property_local_activity")
@XmlRootElement(name = "parametrized_property_local_activity")
public class ParametrizedPropertyLocalActivityXml extends ActivityXml implements ParametrizedPropertyLocalActivity {


	private static final long serialVersionUID = 1L;
	
	@XmlElement(name = "form")
	private String form;

    @XmlElement(name = "property_change")
    @XmlElementWrapper(name = "property_changes")
    private List<EditablePropertyXml> propertyChanges;
	
	public String getForm() {
		return form;
	}

	public void setForm(String form) {
		this.form = form;
	}

    @Override
    public List<EditablePropertyXml> getPropertyChanges() {
        if (propertyChanges == null) {
            propertyChanges = new ArrayList<EditablePropertyXml>();
        }
        return propertyChanges;
    }

    @Override
    public void setPropertyChanges(List<? extends EditableProperty> propertyChanges) {
        this.propertyChanges = (List<EditablePropertyXml> )propertyChanges;
    }

    public void fromInterface(ParametrizedPropertyLocalActivity i)
    {
        super.fromInterface(i);
        this.setForm(i.getForm());

        if (propertyChanges == null) {
            propertyChanges = new ArrayList<EditablePropertyXml>();
        }
        propertyChanges.clear();

        List<? extends EditableProperty> pcdl = i.getPropertyChanges();

        for(EditableProperty d : pcdl){
            EditablePropertyXml pcdx = new EditablePropertyXml();
            pcdx.fromInterface(d);
            propertyChanges.add(pcdx);
        }
    }


    @Override
    public boolean isCorrect()
    {
        if(super.isCorrect() == false)
            return false;

        return true;
    }


    @Override
    public String toString() {
        return "ParametrizedPropertyLocalActivityXml{" +
                "form='" + form + '\'' +
                "} " + super.toString();
    }
}
