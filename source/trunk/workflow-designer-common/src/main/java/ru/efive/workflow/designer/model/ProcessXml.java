package ru.efive.workflow.designer.model;

import ru.efive.workflow.designer.interfaces.AccessControlEntry;
import ru.efive.workflow.designer.interfaces.Process;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;



/**
 * Represents a bases abstraction
 *
 * @author Pavel Shashok
 *
 */


@XmlType(name = "process", propOrder = {"id", "name","repository","dataType", "acl", "description", "metadata", "initialStatusId"} )
@XmlRootElement(name = "process")
public class ProcessXml extends NamedEntityXml implements Process {
	
	private static final long serialVersionUID = 1L;
		
	
	@XmlElement(name = "acess_entry")
	@XmlElementWrapper(name = "access_list")
	private List<AccessControlEntryXml> acl;
	
	@XmlElement(name = "repository")
    private int repository;

    @XmlElement(name = "data_type")
    private String dataType;

    @XmlElement(name = "initial_status_id")
    private String initialStatusId;

    public List<AccessControlEntryXml> getAcl() {
		if (acl == null) {
			acl = new ArrayList<AccessControlEntryXml>();
		}

		return acl;
	}

	@SuppressWarnings("unchecked")
	public void setAcl(List<? extends AccessControlEntry> acl) {
		this.acl = (List<AccessControlEntryXml>) acl;
	}

    @Override
    public String getInitialStatusId() {
        return initialStatusId;
    }

    @Override
    public void setInitialStatusId(String statusId) {
        initialStatusId = statusId;
    }

    public void fromInterface(Process i)
	{
		super.fromInterface(i);

        setRepository(i.getRepository());
        setDataType(i.getDataType());
        setInitialStatusId(i.getInitialStatusId());

		if (acl == null) {
			acl = new ArrayList<AccessControlEntryXml>();
		}
		acl.clear();
		List<? extends AccessControlEntry> iacl = i.getAcl();
		for(AccessControlEntry ac : iacl) {
			AccessControlEntryXml acx = new AccessControlEntryXml();
			acx.fromInterface(ac);
			acl.add(acx);						
		}		
	}
	
		
	@Override
	public boolean equals(Object other)
	{
        if (this == other) 
        	return true;
        if (!(other instanceof ProcessXml))    
        	return false;        
        return this.getId().equals( ((ProcessXml)other).getId() );
	}
	
	@Override	
	public int hashCode()
	{
        return this.getId().hashCode();
	}

	@Override
	public int getRepository() {
		return repository;
	}

	@Override
	public void setRepository(int r) {
		repository = r;		
	}

    @Override
    public String getDataType() {
        return dataType;
    }

    @Override
    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    @Override
    public String toString() {
        return "ProcessXml{" +
                "acl=" + acl +
                ", repository=" + repository +
                ", dataType='" + dataType + '\'' +
                ", initialStatusId='" + initialStatusId + '\'' +
                "} " + super.toString();
    }

}
