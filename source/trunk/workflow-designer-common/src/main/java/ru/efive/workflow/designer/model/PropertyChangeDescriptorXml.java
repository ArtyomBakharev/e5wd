package ru.efive.workflow.designer.model;

import ru.efive.workflow.designer.interfaces.PropertyChangeDescriptor;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
*
*
* @author Shashok Pavel
*
*/
@XmlType(name = "property_change_descriptor")
public class PropertyChangeDescriptorXml implements PropertyChangeDescriptor {

	@XmlElement(name = "name")
	private String name;

	@XmlElement(name = "value")
	private String value;

	public PropertyChangeDescriptorXml() {
	}

	public PropertyChangeDescriptorXml(String name, String value) {
		this.name = name;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	public void fromInterface(PropertyChangeDescriptor i)
	{
		this.setName(i.getName());
		this.setValue(i.getValue());
	}	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (prime * result) + ((name == null) ? 0 : name.hashCode());
		result = (prime * result) + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final PropertyChangeDescriptorXml other = (PropertyChangeDescriptorXml) obj;
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (value == null) {
			if (other.value != null) {
				return false;
			}
		} else if (!value.equals(other.value)) {
			return false;
		}
		return true;
	}

    @Override
    public String toString() {
        return "PropertyChangeDescriptorXml{" +
                "name='" + name + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}