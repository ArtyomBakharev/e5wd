package ru.efive.workflow.designer.model;

import ru.efive.workflow.designer.interfaces.RemoteTransactionActivity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by IntelliJ IDEA.
 * User: spa
 * Date: 11.05.12
 * Time: 14:52
 * To change this template use File | Settings | File Templates.
 */
@XmlType(name = "remote_transaction_activity")
@XmlRootElement(name = "remote_transaction_activity")
public class RemoteTransactionActivityXml extends ActivityXml implements RemoteTransactionActivity {

    @XmlElement(name = "process_id", required = true)
    String actionProcessId;

    @XmlElement(name = "action_id", required = true)
    String actionId;

    @XmlElement(name = "data_type", required = true)
    String dataType;

    @XmlElement(name = "data_search_script", required = true)
    String dataSearchScript;

    @Override
    public String getActionProcessId() {
        return actionProcessId;
    }

    @Override
    public void setActionProcessId(String p) {
        actionProcessId = p;
    }

    @Override
    public String getActionId() {
        return actionId; 
    }

    @Override
    public void setActionId(String a) {
        actionId = a;
    }

    @Override
    public String getDataType() {
        return dataType;  
    }

    @Override
    public void setDataType(String s) {
        dataType = s;
    }

    @Override
    public String getDataSearchScript() {
        return dataSearchScript;  
    }

    @Override
    public void setDataSearchScript(String s) {
        dataSearchScript = s;
    }

    public void fromInterface(RemoteTransactionActivity i)
    {
        super.fromInterface(i);
        
        this.setActionProcessId(i.getActionProcessId());        
        this.setActionId(i.getActionId());
        this.setDataType(i.getDataType());
        this.setDataSearchScript(i.getDataSearchScript());
    }

    @Override
    public boolean isCorrect()
    {
        if(super.isCorrect() == false)
            return false;

        return true;
    }


    @Override
    public String toString() {
        return "RemoteTransactionActivityXml{" +
                "actionProcessId='" + actionProcessId + '\'' +
                ", actionId='" + actionId + '\'' +
                ", dataType='" + dataType + '\'' +
                ", dataSearchScript='" + dataSearchScript + '\'' +
                "} " + super.toString();
    }
}
