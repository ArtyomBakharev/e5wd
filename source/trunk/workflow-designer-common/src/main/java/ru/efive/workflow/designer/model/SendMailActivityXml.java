package ru.efive.workflow.designer.model;

import ru.efive.workflow.designer.interfaces.SendMailActivity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

/**
 * The {@code SendMailActivity} class represents a model of mail dispatching activity.
 *
 * @author Sergey Zaytsev
 *
 */
@XmlType(name = "send_mail_activity")
@XmlRootElement(name = "send_mail_activity")
public class SendMailActivityXml extends ActivityXml implements SendMailActivity {

	private static final long serialVersionUID = 1L;

	@XmlList
	@XmlElement(name = "sendto", required = true)
	private List<String> recipients;

	@XmlList
	@XmlElement(name = "copyto", required = true)
	private List<String> ccRecipients;

	@XmlList
	@XmlElement(name = "blindcopyto", required = true)
	private List<String> bccRecipients;

	@XmlElement(name = "subject", required = true)
	private String subject;

	@XmlElement(name = "body_content", required = true)
	private String content;

	@XmlElement(name = "charset", required = true)
	private String charset;

	public List<String> getRecipients() {
		if (recipients == null) {
			recipients = new ArrayList<String>();
		}

		return recipients;
	}

	public void setRecipients(List<String> recipients) {
		this.recipients = recipients;
	}

	public List<String> getCcRecipients() {
		if (ccRecipients == null) {
			ccRecipients = new ArrayList<String>();
		}

		return ccRecipients;
	}

	public void setCcRecipients(List<String> ccRecipients) {
		this.ccRecipients = ccRecipients;
	}

	public List<String> getBccRecipients() {
		if (bccRecipients == null) {
			bccRecipients = new ArrayList<String>();
		}

		return bccRecipients;
	}

	public void setBccRecipients(List<String> bccRecipients) {
		this.bccRecipients = bccRecipients;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCharset() {
		return charset;
	}

	public void setCharset(String charset) {
		this.charset = charset;
	}
	
	public void fromInterface(SendMailActivity i)
	{
		super.fromInterface(i);
		if(i.getBccRecipients()!=null){
            for(String s:i.getBccRecipients()){
                this.getBccRecipients().add(s);
            }
        }
        if(i.getCcRecipients()!=null){
            for(String s:i.getCcRecipients()){
                this.getCcRecipients().add(s);
            }
        }
        if(i.getRecipients()!=null){
            for(String s:i.getRecipients()){
                this.getRecipients().add(s);
            }
        }
/*
        this.setBccRecipients(i.getBccRecipients());
		this.setCcRecipients(i.getCcRecipients());
        this.setRecipients(i.getRecipients());
*/
        this.setCharset(i.getCharset());
		this.setContent(i.getContent());
		this.setSubject(i.getSubject());

	}

    @Override
    public boolean isCorrect()
    {
        if(super.isCorrect() == false)
            return false;

        return true;
    }

    @Override
    public String toString() {
        return "SendMailActivityXml{" +
                "recipients=" + recipients +
                ", ccRecipients=" + ccRecipients +
                ", bccRecipients=" + bccRecipients +
                ", subject='" + subject + '\'' +
                ", content='" + content + '\'' +
                ", charset='" + charset + '\'' +
                "} " + super.toString();
    }
}
