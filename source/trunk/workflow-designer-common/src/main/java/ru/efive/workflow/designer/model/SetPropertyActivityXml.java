package ru.efive.workflow.designer.model;

import ru.efive.workflow.designer.interfaces.PropertyChangeDescriptor;
import ru.efive.workflow.designer.interfaces.SetPropertyActivity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

/**
 *
 *
 * @author Sergey Zaytsev
 *
 */
@XmlType(name = "set_property_activity")
@XmlRootElement(name = "set_property_activity")
public class SetPropertyActivityXml extends ActivityXml implements SetPropertyActivity {

	private static final long serialVersionUID = 1L;

	@XmlElement(name = "property_change")
	@XmlElementWrapper(name = "property_changes")
	private List<PropertyChangeDescriptorXml> propertyChanges;

	public List<PropertyChangeDescriptorXml> getPropertyChanges() {
		if (propertyChanges == null) {
			propertyChanges = new ArrayList<PropertyChangeDescriptorXml>();
		}

		return propertyChanges;
	}

	@SuppressWarnings("unchecked")
	public void setPropertyChanges(List<? extends PropertyChangeDescriptor> propertyChanges) {
		this.propertyChanges = (List<PropertyChangeDescriptorXml>) propertyChanges;
	}
	
	public void fromInterface(SetPropertyActivity i)
	{
		super.fromInterface(i);
		
		if (propertyChanges == null) {
			propertyChanges = new ArrayList<PropertyChangeDescriptorXml>();
		}
		propertyChanges.clear();
		
		List<? extends PropertyChangeDescriptor> pcdl = i.getPropertyChanges();
		
		for(PropertyChangeDescriptor d : pcdl){
			PropertyChangeDescriptorXml pcdx = new PropertyChangeDescriptorXml();
			pcdx.fromInterface(d);
			propertyChanges.add(pcdx);									
		}
	}

    @Override
    public boolean isCorrect()
    {
        if(super.isCorrect() == false)
            return false;

        return true;
    }

    @Override
    public String toString() {
        return "SetPropertyActivityXml{" +
                "propertyChanges=" + propertyChanges +
                "} " + super.toString();
    }
}
