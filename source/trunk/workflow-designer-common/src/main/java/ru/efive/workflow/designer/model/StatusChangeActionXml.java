package ru.efive.workflow.designer.model;

import org.apache.log4j.Logger;
import ru.efive.workflow.designer.interfaces.Status;
import ru.efive.workflow.designer.interfaces.StatusChangeAction;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Represents a transition of a process from one state to another.
 *
 * @author Sergey Zaytsev
 *
 */
@XmlRootElement(name = "status_change_action")
@XmlType(name = "status_change_action", propOrder = {
		"id", "name", "process", "dataType", "availabilityCondition",
		"fromStatus", "toStatus",
		"preActivities", "postActivities", "localActivities","evaluationMessage",
		"historyEnabled", "autocommitEnabled", "description", "metadata"
})
public class StatusChangeActionXml extends ActionXml implements StatusChangeAction {
    private static final long serialVersionUID = 1L;
    protected static Logger log = Logger.getLogger(StatusChangeActionXml.class);

	@XmlElement(name = "from_status_id", required = true, nillable = false)
	@XmlIDREF
	private StatusXml fromStatus;

	@XmlElement(name = "to_status_id", required = true, nillable = false)
	@XmlIDREF
	private StatusXml toStatus;

	public StatusXml getFromStatus() {
		return fromStatus;
	}

	public void setFromStatus(Status fromStatus) {
		this.fromStatus = (StatusXml) fromStatus;
	}

	public Status getToStatus() {
		return toStatus;
	}

	public void setToStatus(Status toStatus) {
		this.toStatus = (StatusXml) toStatus;
	}

	public void fromInterface(StatusChangeAction i)
	{
        super.fromInterface(i);
        if(i.getFromStatus() != null )    {
            if( fromStatus == null)
                fromStatus = new  StatusXml();
            fromStatus.fromInterface(i.getFromStatus());
        }
        else{
            fromStatus = null;
        }

        if(i.getToStatus() != null )    {
            if( toStatus == null)
                toStatus = new  StatusXml();
            toStatus.fromInterface(i.getToStatus());
        }
        else{
            toStatus = null;
        }
	}

    @Override
    public boolean isCorrect()
    {
        log.error("$$$ StatusChangeActionXml::isCorrect");
        if(super.isCorrect() == false)
            return false;
        
        /*if( !fromStatus.getProcess().getId().equals( process.getId() ) )   {
            log.error("$$$ incorrect fromStatus");
            return false;
        }

        if( !toStatus.getProcess().getId().equals( process.getId() ) )    {
            log.error("$$$ incorrect toStatus");
            return false;
        }    */

        return true;
    }

    @Override
    public String toString() {
        return "StatusChangeActionXml{" +
                "fromStatus=" + fromStatus +
                ", toStatus=" + toStatus +
                "} " + super.toString();
    }
}
