package ru.efive.workflow.designer.model;

import ru.efive.workflow.designer.interfaces.AccessControlEntry;
import ru.efive.workflow.designer.interfaces.Activity;
import ru.efive.workflow.designer.interfaces.Status;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents a particular state of a process
 *
 * @author Sergey Zaytsev
 *
 */
@XmlType(name = "status", propOrder = {
		"id", "name", "process", "alias", "dataType", "localActivities", "preActivities", 
		"postActivities", "acl", "description", "metadata"
})
@XmlRootElement(name = "status")
public class StatusXml extends BaseEntityXml implements Status {

	private static final long serialVersionUID = 1L;

	@XmlElement(name = "alias")
	private String alias;

	@XmlElement(name = "data_type")
	private String dataType;

	@XmlList
	@XmlIDREF
	@XmlElement(name = "localActivities")
	private List<ActivityXml> localActivities = new ArrayList<ActivityXml>();

	@XmlList
	@XmlIDREF
	@XmlElement(name = "preStatusActivities")
	private List<ActivityXml> preActivities = new ArrayList<ActivityXml>();

	@XmlList
	@XmlIDREF
	@XmlElement(name = "postStatusActivities")
	private List<ActivityXml> postActivities = new ArrayList<ActivityXml>();

	@XmlElement(name = "acess_entry")
	@XmlElementWrapper(name = "access_list")
	private List<AccessControlEntryXml> acl;

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public List<ActivityXml> getLocalActivities() {
		if (localActivities == null) {
			localActivities = new ArrayList<ActivityXml>();
		}

		return localActivities;
	}

	@SuppressWarnings("unchecked")
	public void setLocalActivities(List<? extends Activity> localActivities) {
		this.localActivities = (List<ActivityXml>) localActivities;
	}

	public List<ActivityXml> getPreActivities() {
		if (preActivities == null) {
			preActivities = new ArrayList<ActivityXml>();
		}

		return preActivities;
	}

	@SuppressWarnings("unchecked")
	public void setPreActivities(List<? extends Activity> preActivities) {
		this.preActivities = (List<ActivityXml>) preActivities;
	}

	public List<ActivityXml> getPostActivities() {
		if (postActivities == null) {
			postActivities = new ArrayList<ActivityXml>();
		}

		return postActivities;
	}

	@SuppressWarnings("unchecked")
	public void setPostActivities(List<? extends Activity> postActivities) {
		this.postActivities = (List<ActivityXml>) postActivities;
	}

	public List<AccessControlEntryXml> getAcl() {
		if (acl == null) {
			acl = new ArrayList<AccessControlEntryXml>();
		}

		return acl;
	}

	@SuppressWarnings("unchecked")
	public void setAcl(List<? extends AccessControlEntry> acl) {
		this.acl = (List<AccessControlEntryXml>) acl;
	}

	public void fromInterface(Status i)
	{
		super.fromInterface(i);
		
		if (acl == null) {
			acl = new ArrayList<AccessControlEntryXml>();
		}
		acl.clear();
		List<? extends AccessControlEntry> iacl = i.getAcl();
		for(AccessControlEntry ac : iacl) {
			AccessControlEntryXml acx = new AccessControlEntryXml();
			acx.fromInterface(ac);
			acl.add(acx);						
		}		
		
		this.setAlias(i.getAlias());
		this.setDataType(i.getDataType());
		
		List<? extends Activity> la = i.getPostActivities();
		postActivities.clear();
		for(Activity a : la){
			ActivityXml ax = new ActivityXml();
			ax.fromInterface(a);
			postActivities.add(ax);						
		}
		
		la = i.getPreActivities();
		preActivities.clear();
		for(Activity a : la){
			ActivityXml ax = new ActivityXml();
			ax.fromInterface(a);
			preActivities.add(ax);						
		}
		
		la = i.getLocalActivities();
		localActivities.clear();
		for(Activity a : la){
			ActivityXml ax = new ActivityXml();
			ax.fromInterface(a);
			localActivities.add(ax);						
		}
	}

    @Override
    public boolean isCorrect()
    {
        if(super.isCorrect() == false)
            return false;

        for(Activity a : localActivities){
            if( !a.getProcess().getId().equals( process.getId() ) )     {
                log.error("$$$ incorrect localActivities Activity a = " + a);
                return false;
            }
        }

        for(Activity a : preActivities){
            if( !a.getProcess().getId().equals( process.getId() ) )     {
                log.error("$$$ incorrect preActivities Activity a = " + a);
                return false;
            }
        }

        for(Activity a : postActivities){
            if( !a.getProcess().getId().equals( process.getId() ) )     {
                log.error("$$$ incorrect postActivities Activity a = " + a);
                return false;
            }
        }

        return true;
    }

    @Override
    public String toString() {
        return "StatusXml{" +
                "alias='" + alias + '\'' +
                ", dataType='" + dataType + '\'' +
                ", localActivities=" + localActivities +
                ", preActivities=" + preActivities +
                ", postActivities=" + postActivities +
                ", acl=" + acl +
                "} " + super.toString();
    }
}
