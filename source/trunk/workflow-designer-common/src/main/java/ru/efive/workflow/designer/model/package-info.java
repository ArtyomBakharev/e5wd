@XmlSchema(
		xmlns = {
				//@XmlNs(prefix = "e5wd", namespaceURI = "http://efive.ru/workflow/designer"),
				@XmlNs(prefix = "xsi", namespaceURI ="http://www.w3.org/2001/XMLSchema-instance")
		},
		//namespace = "http://efive.ru/workflow/designer",
		elementFormDefault = XmlNsForm.QUALIFIED)
@XmlAccessorType(XmlAccessType.FIELD)
package ru.efive.workflow.designer.model;

import javax.xml.bind.annotation.*;
