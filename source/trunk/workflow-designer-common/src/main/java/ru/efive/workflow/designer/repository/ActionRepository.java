package ru.efive.workflow.designer.repository;

import ru.efive.workflow.designer.interfaces.Action;
import ru.efive.workflow.designer.interfaces.Activity;
import ru.efive.workflow.designer.interfaces.Status;
import ru.efive.workflow.designer.interfaces.Process;

import com.google.common.collect.Multimap;

/**
 * Generic repository for managing {@link ActionXml} entities
 *
 * @author Sergey Zaytsev
 *
 */
public interface ActionRepository<T extends Action> extends Repository<String, T>, BasedEntityRepository<T> {

	/**
	 * Returns all objects grouped by status
	 *
	 * @param process process
	 * @param offset offset of result
	 * @param limit limit of result
	 * @return matching entities
	 */
	Multimap<? extends Status, ? extends Action> findAllGroupedByStatus(Process process, int offset, int limit, String orderBy, boolean asc);


    //Получение всех status change action, у которых в from_status_list есть указываемый status id (получение всех доступных действий из текущего статуса)
    public Iterable<T> findFromStatus(Process process,String id);
    //Получение всех local, pre action, post action для status change и no status action (параметр - action id)
    public Iterable<Activity> findActivity(Process process,String id);

}
