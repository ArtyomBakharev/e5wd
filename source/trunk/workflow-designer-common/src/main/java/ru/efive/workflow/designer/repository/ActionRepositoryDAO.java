package ru.efive.workflow.designer.repository;

import ru.efive.workflow.designer.interfaces.Action;
import ru.efive.workflow.designer.interfaces.Activity;
import ru.efive.workflow.designer.model.ActivityXml;

/**
 * Created by IntelliJ IDEA.
 * User: work
 * Date: 25.04.12
 * Time: 19:14
 * To change this template use File | Settings | File Templates.
 */
public interface ActionRepositoryDAO<T extends Action> extends ActionRepository<T>{
    }
