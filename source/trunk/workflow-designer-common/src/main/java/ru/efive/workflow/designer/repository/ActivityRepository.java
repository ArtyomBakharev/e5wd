package ru.efive.workflow.designer.repository;

import ru.efive.workflow.designer.interfaces.Process;
import ru.efive.workflow.designer.interfaces.Activity;
import ru.efive.workflow.designer.interfaces.Status;
import ru.efive.workflow.designer.interfaces.Action;

import com.google.common.collect.Multimap;

/**
 * Generic repository for managing {@link ActivityXml} entities
 *
 * @author Sergey Zaytsev
 *
 */
public interface ActivityRepository<T extends Activity> extends Repository<String, T>, BasedEntityRepository<T> {

	/**
	 * Returns all objects grouped by status
	 *
	 * @param process process
	 * @param offset offset of result
	 * @param limit limit of result
	 * @return matching entities
	 */
	Multimap<? extends Status, ? extends Activity> findAllGroupedByStatus(Process process, int offset, int limit, String orderBy, boolean asc);

	/**
	 * Returns all objects grouped by action
	 *
	 * @param process process
	 * @param offset offset of result
	 * @param limit limit of result
	 * @return matching entities
	 */
	Multimap<? extends Action,? extends Activity> findAllGroupedByAction(Process process, int offset, int limit, String orderBy, boolean asc);


    //Получение всех local, pre action, post action для status change и no status action (параметр - action id)
    public Iterable<Activity> findActivity(Process process,String id);
}
