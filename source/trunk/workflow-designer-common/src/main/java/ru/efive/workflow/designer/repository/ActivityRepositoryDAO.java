package ru.efive.workflow.designer.repository;

import ru.efive.workflow.designer.interfaces.Activity;

/**
 * Created by IntelliJ IDEA.
 * User: work
 * Date: 25.04.12
 * Time: 19:13
 * To change this template use File | Settings | File Templates.
 */
public interface ActivityRepositoryDAO<T extends Activity> extends ActivityRepository<T>{
}
