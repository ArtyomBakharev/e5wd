package ru.efive.workflow.designer.repository;

import ru.efive.workflow.designer.model.BaseEntityXml;
import ru.efive.workflow.designer.interfaces.BaseEntity;
import ru.efive.workflow.designer.interfaces.Process;

/**
 * Interface for managing {@link BaseEntityXml}
 *
 * @author Sergey Zaytsev
 *
 */
public interface BasedEntityRepository<T extends BaseEntity> {

	/**
	 * Returns all objects with matching name
	 *
	 * @param name the name to search for
	 * @param process process
	 * @param offset offset of result
	 * @param limit limit of result
	 * @return matching entities
	 */
	Iterable<T> findAllByName(Process process, String name, int offset, int limit, String orderBy, boolean asc);

	/**
	 * Returns total number of entities with matching name
	 *
	 * @param name
	 * @return
	 */
	long countByName(String name);
}
