package ru.efive.workflow.designer.repository;

import ru.efive.workflow.designer.interfaces.Process;

/**
 * @author ShashokPA
 *
 */
public interface ProcessRepository<T extends Process> extends Repository<String,T>, NamedEntityRepository<T> {

}
