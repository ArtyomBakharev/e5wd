package ru.efive.workflow.designer.repository;

import ru.efive.workflow.designer.interfaces.*;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: work
 * Date: 25.04.12
 * Time: 19:13
 * To change this template use File | Settings | File Templates.
 */
public interface ProcessRepositoryDAO<T extends ru.efive.workflow.designer.interfaces.Process> extends ProcessRepository<T>{
    public  String getEntityName();
    public  String getProcessPath();
    public String getQry(T process,final String name, String orderBy, boolean asc);
    public List<T> getEntity(List<T> mEntity);
    public T getEntity(T mEntity);
    public List<T> findAllListRootEntity(String qry, int offset, int limit);
}
