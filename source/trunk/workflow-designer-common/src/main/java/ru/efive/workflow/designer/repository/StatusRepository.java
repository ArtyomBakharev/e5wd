package ru.efive.workflow.designer.repository;


import ru.efive.workflow.designer.interfaces.*;
import ru.efive.workflow.designer.model.ActivityXml;

/**
 * Repository for {@link StatusXml} entity
 *
 * @author Sergey Zaytsev
 *
 */
public interface StatusRepository<T extends Status> extends Repository<String, T>, BasedEntityRepository<T> {

    //Получение всех local, pre status, post status activities для объекта status (параметр - status id)
    public Iterable<Activity> findActivity(ru.efive.workflow.designer.interfaces.Process process,String id);

    }
