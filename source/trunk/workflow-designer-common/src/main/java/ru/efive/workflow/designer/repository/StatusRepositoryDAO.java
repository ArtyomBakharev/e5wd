package ru.efive.workflow.designer.repository;

import ru.efive.workflow.designer.interfaces.Status;

/**
 * Created by IntelliJ IDEA.
 * User: work
 * Date: 25.04.12
 * Time: 19:12
 * To change this template use File | Settings | File Templates.
 */
public interface StatusRepositoryDAO<T extends Status> extends StatusRepository<T>{
}
