package ru.efive.workflow.designer.rest;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: spa
 * Date: 05.06.12
 * Time: 12:44
 * To change this template use File | Settings | File Templates.
 */
@XmlRootElement(name = "batch_container")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso({Operation.class })
public class BatchContainer {

    @XmlAnyElement(lax=true)
    private List<Operation> items;

    public List<Operation> getItems() {
        if (items == null) {
            items = new ArrayList<Operation>();
        }

        return items;
    }

    public void setItems(List<Operation> items) {
        this.items = items;
    }

    public Operation get(){
        return items.get(0);
    }

}