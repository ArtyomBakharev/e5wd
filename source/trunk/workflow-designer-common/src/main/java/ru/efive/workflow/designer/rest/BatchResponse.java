package ru.efive.workflow.designer.rest;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: spa
 * Date: 07.06.12
 * Time: 14:17
 * To change this template use File | Settings | File Templates.
 */
@XmlRootElement(name = "batch_response")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso({ResponseItem.class})
public class BatchResponse {

    @XmlAnyElement(lax = true)
    private List<ResponseItem> items;

    public List<ResponseItem> getItems() {
        if (items == null) {
            items = new ArrayList<ResponseItem>();
        }

        return items;
    }

    public void setItems(List<ResponseItem> items) {
        this.items = items;
    }

}