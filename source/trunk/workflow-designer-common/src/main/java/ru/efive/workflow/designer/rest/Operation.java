package ru.efive.workflow.designer.rest;


import ru.efive.workflow.designer.model.*;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 * Created by IntelliJ IDEA.
 * User: spa
 * Date: 05.06.12
 * Time: 12:05
 * To change this template use File | Settings | File Templates.
 */
@XmlRootElement(name = "operation")
@XmlSeeAlso({ProcessXml.class, ActionXml.class, StatusChangeActionXml.class, NoStatusActionXml.class, BaseEntityXml.class,
        StatusXml.class, ActivityXml.class, SendMailActivityXml.class, SetPropertyActivityXml.class, InvokeMethodActivityXml.class
        ,ParametrizedPropertyLocalActivityXml.class, RemoteTransactionActivityXml.class, InvokeScriptActivityXml.class})
public class Operation {

    public enum Method {
        POST,
        PUT,
        DELETE
    }

    private static final long serialVersionUID = 1L;

    private int number;

    private Method method;

    private NamedEntityXml element;

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public NamedEntityXml getElement() {
        return element;
    }

    public void setElement(NamedEntityXml element) {
        this.element = element;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}