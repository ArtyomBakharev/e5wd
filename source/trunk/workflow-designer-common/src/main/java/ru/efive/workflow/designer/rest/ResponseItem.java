package ru.efive.workflow.designer.rest;

import ru.efive.workflow.designer.model.*;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 * Created by IntelliJ IDEA.
 * User: spa
 * Date: 07.06.12
 * Time: 14:23
 * To change this template use File | Settings | File Templates.
 */
@XmlRootElement(name = "item")
@XmlSeeAlso({ProcessXml.class, ActionXml.class, StatusChangeActionXml.class, NoStatusActionXml.class, BaseEntityXml.class,
        StatusXml.class, ActivityXml.class, SendMailActivityXml.class, SetPropertyActivityXml.class, InvokeMethodActivityXml.class
        ,ParametrizedPropertyLocalActivityXml.class, RemoteTransactionActivityXml.class, InvokeScriptActivityXml.class})
public class ResponseItem {
    int status;
    
    String ret_id;

    private int number;

    private NamedEntityXml element;

    public NamedEntityXml getElement() {
        return element;
    }

    public void setElement(NamedEntityXml element) {
        this.element = element;
    }

    public String getRetId() {
        return ret_id;
    }

    public void setRetId(String ret_id) {
        this.ret_id = ret_id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }



    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
