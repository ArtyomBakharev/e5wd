package ru.efive.workflow.designer.model;

import static org.junit.Assert.assertEquals;
import ru.efive.workflow.designer.interfaces.ReturnType;

/**
 * Tests for {@link ActivityXml} class
 *
 * @author Sergey Zaytsev
 *
 */
public class ActivityTest extends XmlTestSupport {

	public ActivityTest() throws Exception {
		super("activities.xsd", ActivityXml.class);
	}

	@Override
	public void unmarshallOk() throws Exception {
	 	final ActivityXml activity = unmarshall("blank_activity.xml");

		assertEquals("test_id", activity.getId());
		assertEquals("test_name", activity.getName());
		assertEquals("1>0", activity.getRunCondition());
		assertEquals(ReturnType.VOID, activity.getReturnType());
		assertEquals("1+1", activity.getReturnFormula());
		assertEquals("test_commentary", activity.getDescription());
	}

	@Override
	public void marshallOk() throws Exception {
		final ActivityXml activity = new ActivityXml();
		
		final ProcessXml process = new ProcessXml();
		process.setId("test_process");		

		activity.setId("test_id");
		activity.setName("test_name");
		activity.setProcess(process);
		activity.setRunCondition("1>0");
		activity.setReturnType(ReturnType.VOID);
		activity.setReturnFormula("1-1");
		activity.setDescription("test_commentary");

		marshall(activity);
	}
}
