package ru.efive.workflow.designer.model;

import ru.efive.workflow.designer.interfaces.ReturnType;

import static org.junit.Assert.assertEquals;

/**
 * Tests for {@link InvokeMethodActivityXml} class
 *
 * @author Shashok Pavel
 *
 */
public class InvokeMethodActivityTest extends XmlTestSupport {

    public InvokeMethodActivityTest() throws Exception {
        super("activities.xsd", InvokeMethodActivityXml.class);
    }

    @Override
    public void unmarshallOk() throws Exception {
        final InvokeMethodActivityXml activity = unmarshall("invoke_method_activity.xml");

        assertEquals("test_id", activity.getId());
        assertEquals("test_name", activity.getName());
        assertEquals("1>0", activity.getRunCondition());
        assertEquals(ReturnType.VOID, activity.getReturnType());
        assertEquals("1+1", activity.getReturnFormula());
        assertEquals("test_commentary", activity.getDescription());

        assertEquals("test_class_name", activity.getClassName());
        assertEquals("test_method_name", activity.getMethodName());
    }

    @Override
    public void marshallOk() throws Exception {
        final InvokeMethodActivityXml activity = new InvokeMethodActivityXml();

        final ProcessXml process = new ProcessXml();
        process.setId("test_process");

        activity.setId("test_id");
        activity.setName("test_name");
        activity.setProcess(process);
        activity.setRunCondition("1>0");
        activity.setReturnType(ReturnType.VOID);
        activity.setReturnFormula("1-1");
        activity.setDescription("test_commentary");

        activity.setClassName("test_class_name");
        activity.setMethodName("test_method_name");

        marshall(activity);
    }
}