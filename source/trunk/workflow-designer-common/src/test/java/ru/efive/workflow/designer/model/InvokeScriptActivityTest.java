package ru.efive.workflow.designer.model;

import ru.efive.workflow.designer.interfaces.ReturnType;

import static org.junit.Assert.assertEquals;

/**
 * Tests for {@link InvokeScriptActivityXml} class
 *
 * @author Shashok Pavel
 *
 */
public class InvokeScriptActivityTest extends XmlTestSupport {

    public InvokeScriptActivityTest() throws Exception {
        super("activities.xsd", InvokeScriptActivityXml.class);
    }

    @Override
    public void unmarshallOk() throws Exception {
        final InvokeScriptActivityXml activity = unmarshall("invoke_script_activity.xml");

        assertEquals("test_id", activity.getId());
        assertEquals("test_name", activity.getName());
        assertEquals("1>0", activity.getRunCondition());
        assertEquals(ReturnType.VOID, activity.getReturnType());
        assertEquals("1+1", activity.getReturnFormula());
        assertEquals("test_commentary", activity.getDescription());

        assertEquals("test_script", activity.getScript());
    }

    @Override
    public void marshallOk() throws Exception {
        final InvokeScriptActivityXml activity = new InvokeScriptActivityXml();

        final ProcessXml process = new ProcessXml();
        process.setId("test_process");

        activity.setId("test_id");
        activity.setName("test_name");
        activity.setProcess(process);
        activity.setRunCondition("1>0");
        activity.setReturnType(ReturnType.VOID);
        activity.setReturnFormula("1-1");
        activity.setDescription("test_commentary");

        activity.setScript("test_script");
;

        marshall(activity);
    }
}