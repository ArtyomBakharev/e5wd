package ru.efive.workflow.designer.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Tests for {@link NoStatusActionXml} class
 *
 * @author Sergey Zaytsev
 *
 */
public class NoStatusActionTest extends XmlTestSupport {

	public NoStatusActionTest() throws Exception {
		super("no_status_action.xsd", NoStatusActionXml.class);
	}

	@Override
	public void unmarshallOk() throws Exception {
		final NoStatusActionXml action = (NoStatusActionXml) unmarshall("no_status_action.xml");

		assertEquals("test_id", action.getId());
		assertEquals("tasty_name", action.getName());
		assertEquals("STRING", action.getDataType());
		assertEquals("test_commentary", action.getDescription());

		assertTrue(action.isHistoryEnabled());
		assertTrue(action.isAutocommitEnabled());
	}

	@Override
	public void marshallOk() throws Exception {
		final NoStatusActionXml action = new NoStatusActionXml();
		final ProcessXml process = new ProcessXml();
		process.setId("test_process");

		action.setId("test-id");
		action.setName("test");
		action.setProcess(process);
		action.setDataType("STRING");
		action.setAvailabilityCondition("1 > 0");
		action.setEvaluationMessage("test");
		action.setDescription("123");

		marshall(action);
	}
}
