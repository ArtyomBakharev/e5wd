package ru.efive.workflow.designer.model;

import static org.junit.Assert.assertEquals;

/**
 * Tests for {@link SetPropertyActivityXml} class
 *
 * @author Shashok Pavel
 *
 */
public class ParametrizedPropertyLocalActivityTest extends XmlTestSupport {

    public ParametrizedPropertyLocalActivityTest() throws Exception {
        super("activities.xsd", ParametrizedPropertyLocalActivityXml.class);
    }

    @Override
    public void unmarshallOk() throws Exception {
        final ParametrizedPropertyLocalActivityXml activity = unmarshall("parametrized_property_local_activity.xml");

        assertEquals("test_id", activity.getId());
        assertEquals("test_name", activity.getName());
        assertEquals("1>0", activity.getRunCondition());
        //assertEquals(ReturnType.VOID, activity.getReturnType());
        assertEquals("1-1", activity.getReturnFormula());
        assertEquals("test_commentary", activity.getDescription());
        assertEquals("test_form", activity.getForm());

        assertEquals(1, activity.getPropertyChanges().size());
        assertEquals("name", activity.getPropertyChanges().get(0).getName());
        assertEquals("value", activity.getPropertyChanges().get(0).getValue());
        assertEquals(0, activity.getPropertyChanges().get(0).getScope());
    }

    @Override
    public void marshallOk() throws Exception {
        /*final ParametrizedPropertyLocalActivityXml activity = new ParametrizedPropertyLocalActivityXml();
        final ProcessXml process = new ProcessXml();
        process.setId("test_process");

        activity.setId("test_id");
        activity.setName("test_name");
        activity.setProcess(process);
        activity.setRunCondition("1>0");
        //activity.setReturnType(ReturnType.VOID);
        activity.setReturnFormula("1-1");
        activity.setDescription("test_commentary");
        activity.setForm("test_form");

        EditablePropertyXml ep = new EditablePropertyXml();
        ep.setName("name");
        ep.setValue("value");
        ep.setScope(0);

        activity.getPropertyChanges().add( ep );

        marshall(activity);   */
    }


}
