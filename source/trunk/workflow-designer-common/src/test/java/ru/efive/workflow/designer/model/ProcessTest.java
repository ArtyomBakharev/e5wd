package ru.efive.workflow.designer.model;

import static junit.framework.Assert.assertEquals;


/**
 * Tests for XML marshalling/unmarshalling of {@link ProcessXml}
 *
 * @author Pavel Shashok
 *
 */
public class ProcessTest extends XmlTestSupport {
	
	public ProcessTest() throws Exception {
		super("process.xsd", ProcessXml.class);
	}

	@Override
	public void unmarshallOk() throws Exception {
		final ProcessXml process = (ProcessXml) unmarshall("process.xml");

		assertEquals("testid", process.getId());
		assertEquals("testname", process.getName());
		assertEquals("bla-bla-bla", process.getDescription());
        assertEquals("error read data_type", "testDataType", process.getDataType());
        assertEquals("testuser", process.getAcl().get(0).getSid());
		assertEquals(0, process.getAcl().get(0).getPermission());
	}

	@Override
	public void marshallOk() throws Exception {

		final ProcessXml process = new ProcessXml();

		process.setId("1234");
		process.setName("Test Status");
		process.setRepository(0);
		process.setDataType("testDataType");
        process.setInitialStatusId("1111");
        process.setDescription("asdfasdf");
		process.getAcl().add(new AccessControlEntryXml("test", 1));

		marshall(process);
	}

}
