package ru.efive.workflow.designer.model;

import ru.efive.workflow.designer.interfaces.ReturnType;

import static org.junit.Assert.assertEquals;

/**
 * Tests for {@link ActivityXml} class
 *
 * @author Sergey Zaytsev
 *
 */
public class RemoteTransactionActivityTest extends XmlTestSupport {

    public RemoteTransactionActivityTest() throws Exception {
        super("activities.xsd", RemoteTransactionActivityXml.class);
    }

    @Override
    public void unmarshallOk() throws Exception {
        final RemoteTransactionActivityXml activity = unmarshall("remote_transaction_activity.xml");

        assertEquals("test_id", activity.getId());
        assertEquals("test_name", activity.getName());
        assertEquals("1>0", activity.getRunCondition());
        assertEquals(ReturnType.VOID, activity.getReturnType());
        assertEquals("1+1", activity.getReturnFormula());
        assertEquals("test_commentary", activity.getDescription());
        assertEquals("test_process_id", activity.getActionProcessId());
        assertEquals("test_action_id", activity.getActionId());
        assertEquals("test_data_type", activity.getDataType());
        assertEquals("test_data_search_script", activity.getDataSearchScript());
    }

    @Override
    public void marshallOk() throws Exception {
        final RemoteTransactionActivityXml activity = new RemoteTransactionActivityXml();

        final ProcessXml process = new ProcessXml();
        process.setId("test_process");

        activity.setId("test_id");
        activity.setName("test_name");
        activity.setProcess(process);
        activity.setRunCondition("1>0");
        activity.setReturnType(ReturnType.VOID);
        activity.setReturnFormula("1-1");
        activity.setDescription("test_commentary");

        activity.setActionProcessId("test_process_id");
        activity.setActionId("test_action_id");
        activity.setDataType("test_data_type");
        activity.setDataSearchScript("test_data_search_script");


        marshall(activity);
    }
}
