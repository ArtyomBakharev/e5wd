package ru.efive.workflow.designer.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


/**
 * TODO: Document type
 *
 * @author Sergey Zaytsev
 *
 */
public class SatusChangeActionTest extends XmlTestSupport {

	public SatusChangeActionTest() throws Exception {
		super("status_change_action.xsd", StatusChangeActionXml.class);
	}

	@Override
	public void unmarshallOk() throws Exception {
		final StatusChangeActionXml action = (StatusChangeActionXml) unmarshall("status_change_action.xml");

		assertEquals("test_id", action.getId());
		assertEquals("test_name", action.getName());
		assertEquals("STRING", action.getDataType());

		assertTrue(action.isHistoryEnabled());
		assertTrue(action.isAutocommitEnabled());
	}

	@Override
	public void marshallOk() throws Exception {
		final StatusChangeActionXml action = new StatusChangeActionXml();
		final ProcessXml process = new ProcessXml();
		process.setId("test_process");

		action.setId("test-id");
		action.setName("test");
		action.setProcess(process);
		action.setDataType("STRING");
		action.setAvailabilityCondition("1 > 0");
		action.setEvaluationMessage("test");
		action.setDescription("123");

		final StatusXml from = new StatusXml();
		from.setId("from_id");
		action.setFromStatus(from);
		final StatusXml to = new StatusXml();
		to.setId("to_id");
		action.setToStatus(to);

		marshall(action);
	}
}
