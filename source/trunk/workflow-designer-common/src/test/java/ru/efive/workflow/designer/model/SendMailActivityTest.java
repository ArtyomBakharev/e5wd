package ru.efive.workflow.designer.model;

import static org.junit.Assert.assertEquals;
import ru.efive.workflow.designer.interfaces.ReturnType;

/**
 * TODO: Document type
 *
 * @author Sergey Zaytsev
 *
 */
public class SendMailActivityTest extends XmlTestSupport {

	public SendMailActivityTest() throws Exception {
		super("activities.xsd", SendMailActivityXml.class);
	}

	/* (non-Javadoc)
	 * @see ru.efive.workflow.designer.model.XmlTestSupport#unmarshallOk()
	 */
	@Override
	public void unmarshallOk() throws Exception {
	 	final SendMailActivityXml activity = unmarshall("send_mail_activity.xml");

		assertEquals("test_id", activity.getId());
		assertEquals("test_name", activity.getName());
		assertEquals("1>0", activity.getRunCondition());
		assertEquals(ReturnType.VALUE, activity.getReturnType());
		assertEquals("1-1", activity.getReturnFormula());
		assertEquals("test_commentary", activity.getDescription());
		assertEquals(1, activity.getRecipients().size());
		assertEquals("sendto@test.test", activity.getRecipients().get(0));
		assertEquals(1, activity.getCcRecipients().size());
		assertEquals("cc@test.test", activity.getCcRecipients().get(0));
		assertEquals(1, activity.getBccRecipients().size());
		assertEquals("bcc@test.test", activity.getBccRecipients().get(0));
		assertEquals("test_subject", activity.getSubject());
		assertEquals("bla-bla-bla", activity.getContent());
		assertEquals("test", activity.getCharset());

	}

	/* (non-Javadoc)
	 * @see ru.efive.workflow.designer.model.XmlTestSupport#marshallOk()
	 */
	@Override
	public void marshallOk() throws Exception {
		final SendMailActivityXml activity = new SendMailActivityXml();
		final ProcessXml process = new ProcessXml();
		process.setId("test_process");

		activity.setId("test_id");
		activity.setName("test_name");
		activity.setProcess(process);
		activity.setRunCondition("1>0");
		activity.setReturnType(ReturnType.VALUE);
		activity.setReturnFormula("1-1");
		activity.setDescription("test_commentary");

		activity.getRecipients().add("sendto@test.test");
		activity.getCcRecipients().add("cc@test.test");
		activity.getBccRecipients().add("bcc@test.test");
		activity.setSubject("test_subject");
		activity.setContent("bla-bla");
		activity.setCharset("test");

		marshall(activity);
	}

}
