package ru.efive.workflow.designer.model;

import static org.junit.Assert.assertEquals;
import ru.efive.workflow.designer.interfaces.ReturnType;
import ru.efive.workflow.designer.model.PropertyChangeDescriptorXml;

/**
 * Tests for {@link SetPropertyActivityXml} class
 *
 * @author Sergey Zaytsev
 *
 */
public class SetPropertyActivityTest extends XmlTestSupport {

	public SetPropertyActivityTest() throws Exception {
		super("activities.xsd", SetPropertyActivityXml.class);
	}

	@Override
	public void unmarshallOk() throws Exception {
		final SetPropertyActivityXml activity = unmarshall("set_property_activity.xml");

		assertEquals("test_id", activity.getId());
		assertEquals("test_name", activity.getName());
		assertEquals("1>0", activity.getRunCondition());
		//assertEquals(ReturnType.VOID, activity.getReturnType());
		assertEquals("1-1", activity.getReturnFormula());
		assertEquals("test_commentary", activity.getDescription());

		assertEquals(1, activity.getPropertyChanges().size());
		assertEquals("name", activity.getPropertyChanges().get(0).getName());
		assertEquals("value", activity.getPropertyChanges().get(0).getValue());
	}

	@Override
	public void marshallOk() throws Exception {
		final SetPropertyActivityXml activity = new SetPropertyActivityXml();
		final ProcessXml process = new ProcessXml();
		process.setId("test_process");

		activity.setId("test_id");
		activity.setName("test_name");
		activity.setProcess(process);
		activity.setRunCondition("1>0");
		activity.setReturnType(ReturnType.VOID);
		activity.setReturnFormula("1-1");
		activity.setDescription("test_commentary");

		activity.getPropertyChanges().add(new PropertyChangeDescriptorXml("name", "value"));

		marshall(activity);
	}


}
