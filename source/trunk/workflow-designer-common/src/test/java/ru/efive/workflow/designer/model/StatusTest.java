package ru.efive.workflow.designer.model;

import static junit.framework.Assert.assertEquals;


/**
 * Tests for XML marshalling/unmarshalling of {@link StatusXml}
 *
 * @author Sergey Zaytsev
 *
 */
public class StatusTest extends XmlTestSupport {

	public StatusTest() throws Exception {
		super("status.xsd", StatusXml.class);
	}

	@Override
	public void unmarshallOk() throws Exception {
		final StatusXml status = (StatusXml) unmarshall("status.xml");

		assertEquals("testid", status.getId());
		assertEquals("testname", status.getName());
		assertEquals("testalias", status.getAlias());
		assertEquals("testdatatype", status.getDataType());
		assertEquals("bla-bla-bla", status.getDescription());

		assertEquals("testuser", status.getAcl().get(0).getSid());
		assertEquals(0, status.getAcl().get(0).getPermission());
	}

	@Override
	public void marshallOk() throws Exception {

		final StatusXml status = new StatusXml();
		final ProcessXml process = new ProcessXml();
		process.setId("test_process");

		status.setId("1234");
		status.setName("Test Status");
		status.setProcess(process);
		status.setAlias("test");
		status.setDescription("asdfasdf");
		status.setDataType("test");
		status.getAcl().add(new AccessControlEntryXml("test", 1));

		ActivityXml a = new ActivityXml();
		a.setId("2222");
		status.getLocalActivities().add(a);

		a = new ActivityXml();
		a.setId("2223");
		status.getLocalActivities().add(a);

		marshall(status);
	}
}
