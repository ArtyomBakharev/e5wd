package ru.efive.workflow.designer.model;

import java.io.IOException;
import java.io.OutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import junit.framework.Assert;

import org.junit.Test;

/**
 * TODO: Document type
 *
 * @author Sergey Zaytsev
 *
 */
public abstract class XmlTestSupport {

	final JAXBContext jaxbContext;
	final Schema schema;

	public XmlTestSupport(String schemaName, Class<?>... classesToBeBound) throws Exception {
		jaxbContext = JAXBContext.newInstance(classesToBeBound);
		schema = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema").newSchema(
				new StreamSource(getClass().getResourceAsStream(schemaName)));
	}

	void marshall(Object o) throws Exception {
		final Marshaller marshaller = jaxbContext.createMarshaller();
		marshaller.setSchema(schema);
		marshaller.marshal(o, new OutputStream() {
			@Override
			public void write(int b) throws IOException {
			}
		});
		//		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
//		marshaller.marshal(o, System.out);
	}

	@SuppressWarnings("unchecked")
	<T> T unmarshall(String filename) throws Exception {
		return (T) jaxbContext.createUnmarshaller().unmarshal(getClass().getResourceAsStream(filename));
	}

	@Test
	public void unmarshallOk() throws Exception {
		Assert.fail("Not implemented");
	}

	@Test
	public void marshallOk() throws Exception {
		Assert.fail("Not implemented");
	}
}
