package ru.efive.workflow.designer.jsf;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;

/**
 * Created Dmitry Parshin
 * Date: 25.05.12
 */
@Named("efwdViewer")
@SessionScoped
public class EfwdViewerBean  implements Serializable {
    private String efwdUrl = "http://localhost:8080/workflow-designer-web/rest/processes";
    private String dataProcess = "1";
    private String dataCurrentStatus = "2";
    private String style="width: 500px; height: 500px;";
    private String extend = "{\n" +
            "\t\t\t\t\t\t\"statusBox\": {\n" +
            "\t\t\t\t\t\t\t\"style\": {\n" +
            "\t\t\t\t\t\t\t\t\"box\": {\n" +
            "\t\t\t\t\t\t\t\t\t\"attrs\": {\n" +
            "\t\t\t\t\t\t\t\t\t\t\"fill\": \"={currentStatus} ? '#FF9EC5' : '#9EFFA5'\"\n" +
            "\t\t\t\t\t\t\t\t\t}\n" +
            "\t\t\t\t\t\t\t\t}\n" +
            "\t\t\t\t\t\t\t}\n" +
            "\t\t\t\t\t\t}\n" +
            "\t\t\t\t\t}";
    private String replace = "{\n" +
            "\t\t\t\t\t\t\"status.style.title.attrs\": {\n" +
            "\t\t\t\t\t\t\t\"fill\": \"blue\",\n" +
            "\t\t\t\t\t\t\t\"font-size\": 12,\n" +
            "\t\t\t\t\t\t\t\"y\": -10,\n" +
            "\t\t\t\t\t\t\t\"text\": \"=_.name\"\n" +
            "\t\t\t\t\t\t}\n" +
            "\t\t\t\t\t}";

    public String getExtend() {
        return extend;
    }

    public void setExtend(String extend) {
        this.extend = extend;
    }

    public String getReplace() {
        return replace;
    }

    public void setReplace(String replace) {
        this.replace = replace;
    }

    public String getEfwdUrl() {
        return efwdUrl;
    }

    public void setEfwdUrl(String efwdUrl) {
        this.efwdUrl = efwdUrl;
    }

    public String getDataProcess() {
        return dataProcess;
    }

    public void setDataProcess(String dataProcess) {
        this.dataProcess = dataProcess;
    }

    public String getDataCurrentStatus() {
        return dataCurrentStatus;
    }

    public void setDataCurrentStatus(String dataCurrentStatus) {
        this.dataCurrentStatus = dataCurrentStatus;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }
}
