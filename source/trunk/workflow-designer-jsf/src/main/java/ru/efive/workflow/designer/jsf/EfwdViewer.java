package ru.efive.workflow.designer.jsf;

import ru.efive.workflow.designer.client.JsonProvider;

import javax.faces.application.ResourceDependencies;
import javax.faces.application.ResourceDependency;
import javax.faces.component.FacesComponent;
import javax.faces.component.UIComponent;
import javax.faces.component.UIComponentBase;
import javax.faces.component.UIParameter;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Created with Dmitry Parshin
 * Date: 24.05.12
 */
@FacesComponent("efwdViewer")
@ResourceDependencies({
        @ResourceDependency(name = "/viewer/resource/efwdViewer/css/style.css", target = "head"),
        @ResourceDependency(name = "/viewer/resource/efwdViewer/icon/ajax-loader.gif"),
        @ResourceDependency(name = "/viewer/script/efwdViewer-all.js", target = "head")
})
public class EfwdViewer extends UIComponentBase{
    private static final String FAMILY = "efwdViewer";

    @Override
    public String getFamily() {
        return FAMILY;
    }

    @Override
    public void encodeBegin(FacesContext context) throws IOException {
        ResponseWriter responseWriter = context.getResponseWriter();

        String dataProcess       = (String) getAttributes().get("data_process");
        String dataCurrentStatus = (String) getAttributes().get("data_current_status");
        String style             = (String) getAttributes().get("style");
        String efwdUrl           = (String) getAttributes().get("efwd_url");

        String extend= (String) getAttributes().get("extend");
        String replace = (String) getAttributes().get("replace");

        String id = (String) getAttributes().get("id");
        if(id==null) id="sk";


        String load = null;
        int i = 0;

        for (UIComponent kid : getChildren()) {
            if (kid instanceof UIParameter) {
                UIParameter param = (UIParameter) kid;

                if(param.getName().equals("load")){
                    if(load==null) load = "[";
                    else load+=",";

                    String val = param.getValue().toString();
                    load+="\""+id+i+"\"";

                    if(null != val && !"".equals(val)) {
                        responseWriter.startElement("script", null);
                        responseWriter.writeAttribute("type", "text/javascript", null);
                        responseWriter.writeAttribute("id", id+i, null);
                        responseWriter.writeAttribute("src", val, null);
                        responseWriter.endElement("script");
                    }
                i++;
                }

            }
        }

        if(load!=null) load += "]";

        responseWriter.startElement("div", null);
        responseWriter.writeAttribute("class", "efwd-viewer", null);

        if(null != dataProcess && !"".equals(dataProcess)) {
            responseWriter.writeAttribute("data-process", dataProcess, null);
        }

        if(null != dataCurrentStatus  && !"".equals(dataCurrentStatus)) {
            responseWriter.writeAttribute("data-current-status", dataCurrentStatus, null);
        }

        if(null != style && !"".equals(style)) {
            responseWriter.writeAttribute("style", style, null);
        }


        responseWriter.startElement("script", null);
        responseWriter.writeAttribute("type", "text/json", null);

    
        if(null != efwdUrl && null != dataProcess && !"".equals(dataProcess) && !"".equals(efwdUrl)) {
            JsonProvider jsonProvaider = new JsonProvider(efwdUrl);
            List<String> statuses = jsonProvaider.getStatuses(dataProcess);
            List<String> actions =  jsonProvaider.getActions(dataProcess);
            List<String> proces =  jsonProvaider.getProcess(dataProcess);

            responseWriter.writeText("{",null);
            responseWriter.writeText("\"data\":{",null);


            if(proces==null || proces.size()==0) {
                responseWriter.writeText("\"process\": null",null);
            } else {
                responseWriter.writeText("\"process\":",null);
                for(String s:proces)  {
                    responseWriter.writeText(s,null);
                }
            }

            if(statuses==null || statuses.size()==0) {
                responseWriter.writeText(",\"actions\": null, \"statuses\": null",null);
            } else {
                responseWriter.writeText(",\"statuses\":",null);
                for(String s:statuses)  {
                    responseWriter.writeText(s,null);
                }
                responseWriter.writeText(",\"actions\":",null);
                if(actions==null || actions.size()==0) {
                    responseWriter.writeText(" null",null);
                } else {
                    for(String s:actions)  {
                        responseWriter.writeText(s,null);
                    }
                }
            }

            responseWriter.writeText("}",null);


            if(
               ( null != extend && !"".equals(extend) ) ||
               ( null != replace && !"".equals(replace) ) ||
               ( null != load && !"".equals(load) )
              ) {
                responseWriter.writeText(",\"schemaExtend\": {",null);
                String comma = "";
                if( null != extend && !"".equals(extend) ) {
                    responseWriter.writeText("\"extend\":",null);
                    responseWriter.writeText(extend,null);
                    comma = ",";
                }
                if( null != replace && !"".equals(replace) ) {
                    responseWriter.writeText(comma+"\"replace\":",null);
                    responseWriter.writeText(replace,null);
                    comma = ",";
                }
                if( null != load && !"".equals(load) ) {
                    responseWriter.writeText(comma+"\"load\":",null);
                    responseWriter.writeText(load,null);
                }

                responseWriter.writeText("}",null);
            }

            responseWriter.writeText("}",null);

        } else{
            responseWriter.writeText("{\"data\":{\"actions\": null, \"statuses\": null}}",null);
        }



        responseWriter.endElement("script");

        responseWriter.endElement("div");
    }

}


