package ru.efive.workflow.designer.repository.plainxml;


import ru.efive.workflow.designer.interfaces.Process;
import ru.efive.workflow.designer.model.NamedEntityXml;
import ru.efive.workflow.designer.model.ProcessXml;
import ru.efive.workflow.designer.repository.NamedEntityRepository;

import java.util.Comparator;

/**
 * AbstractNamedPlainXmlRepository implements NamedEntityRepository
 *
 * @author Pavel Shashok
 *
 */
public abstract class AbstractNamedPlainXmlRepository<T extends NamedEntityXml> extends AbstractPlainXmlRepository<T> 
implements NamedEntityRepository<T>{
	
	protected CompartorFactory<T> getComparatorFactory() {
		return new CompartorFactory<T>() {

			@Override
			public Comparator<T> createComparator(String field) {
				if ("name".equals(field)) {
					return new Comparator<T>() {

						@Override
						public int compare(T o1, T o2) {
							return o1.getName().compareTo(o2.getName());
						}

					};
				} else if (null == field) {
					return new Comparator<T>() {

						@Override
						public int compare(T o1, T o2) {
							// TODO Auto-generated method stub
							return 0;
						}
					};
				}

				throw new IllegalArgumentException("Unsupported field " + field);
			}

			@Override
			public Comparator<T> createComparator(String field, boolean inverse) {
				final Comparator<T> comparator = createComparator(field);
				if (inverse) {
					return new InverseComparator<T>(comparator);
				} else {
					return comparator;
				}
			}

		};
	}
	
	/**
	 * TODO: Document type
	 * 
	 * @author Sergey Zaytsev
	 * 
	 */
	static final class NameMatcher<T extends NamedEntityXml> extends
			ElementMatcher<T> {
		private final String name;

		private NameMatcher(String name) {
			this.name = name.toLowerCase();
		}

		@Override
		public boolean matches(T element) {
			return (element.getName().toLowerCase().contains(name));
		}
	}
	
	@Override
	public long countByName(String name) {
		return count(new NameMatcher<T>(name));
	}

	@Override
	public Iterable<T> findAllByName(Process process, final String name, int offset, int limit,
			String orderBy, boolean asc) {
		return findInternal((ProcessXml)process, offset, limit, orderBy, asc, new NameMatcher<T>(
				name));
	}


}
