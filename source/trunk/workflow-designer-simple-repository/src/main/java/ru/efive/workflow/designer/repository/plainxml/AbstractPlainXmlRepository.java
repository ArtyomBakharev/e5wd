package ru.efive.workflow.designer.repository.plainxml;

import java.util.*;
import java.util.Map.Entry;

import javax.ejb.EJB;

import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;

import ru.efive.workflow.designer.interfaces.Process;
import ru.efive.workflow.designer.model.AbstractEntityXml;
import ru.efive.workflow.designer.model.ProcessXml;
import ru.efive.workflow.designer.model.BaseEntityXml;
import ru.efive.workflow.designer.repository.NamedEntityRepository;
import ru.efive.workflow.designer.repository.Repository;

/**
 * Base class for implementing {@link PlainXmlWorkflowStorage}-backed repository
 * 
 * @author Sergey Zaytsev
 * 
 */
public abstract class AbstractPlainXmlRepository<T extends AbstractEntityXml>
		implements Repository<String, T> {

	@EJB
	private PlainXmlWorkflowStorage storage;

	protected abstract Map<String, T> getEntities();

	protected abstract T saveInternal(T entity);

	protected abstract void deleteInternal(T entity);

	protected void flush() {
		getStorage().flush();
	}

	static class InverseComparator<T> implements Comparator<T> {

		private final Comparator<T> delegate;

		/**
		 * @param delegate
		 */
		public InverseComparator(Comparator<T> delegate) {
			this.delegate = delegate;
		}

		@Override
		public int compare(T o1, T o2) {
			return -delegate.compare(o1, o2);
		}

	}
	
	protected abstract CompartorFactory<T> getComparatorFactory();

	@Override
	public T save(T entity) {
		final T result = saveInternal(entity);
		flush();

		return result;
	}

	@Override
	public void delete(T entity) {
		deleteInternal(entity);
		flush();
	}

	@Override
	public Iterable<T> save(Iterable<? extends T> entities) {
		final List<T> result = new ArrayList<T>();

		for (final T entity : entities) {
			result.add(save(entity));
		}

		return result;
	}

	@Override
	public T findOne(String id) {
		return getEntities().get(id);
	}

	@Override
	public boolean exists(String id) {
		return getEntities().containsKey(id);
	}

	@Override
	public Iterable<T> findAll() {
		return getEntities().values();
	}

	@Override
	public long count() {
		return getEntities().size();
	}

	@Override
	public void delete(String id) {
		delete(findOne(id));
	}

	@Override
	public void delete(Iterable<? extends T> entities) {
		for (final T entity : entities) {
			delete(entity);
		}
	}

	@Override
	public void deleteAll() {
		delete(findAll());
	}
	
	@SuppressWarnings("rawtypes")
	protected Iterable<T> findInternal(ProcessXml process, int offset, int limit, String orderBy,
			boolean asc, ElementMatcher<T> matcher) {
		List<T> all = new ArrayList<T>(getEntities().values());

		final ElementMatcher<T> m;
		if (null != matcher) {
			m = matcher;
		} else {
			m = anyMatcher();
		}
		
		ProcessMatcher mp = null;
		if(process != null) {
			mp = new ProcessMatcher(process);		
			all = mp.filter(all);
		} 	

		if (offset < all.size()) {
			Collections.sort(all,
					getComparatorFactory().createComparator(orderBy));
			if (!asc) {
				Collections.reverse(all);
			}

			return slice(m.filter(all), offset, limit);
		}

		return Collections.emptyList();
	}
	



	@Override
	public Iterable<T> findAll(Process process, int offset, int limit,
			String orderBy, boolean asc) {
		ProcessXml temp = new ProcessXml();
		temp.fromInterface(process);
		return findInternal((ProcessXml)temp, offset, limit, orderBy, asc, null );
	}

	static abstract class ElementMatcher<T> {

		public abstract boolean matches(T element);

		public List<T> filter(List<T> src) {
			final List<T> result = new ArrayList<T>();
			for (final T element : src) {
				if (matches(element)) {
					result.add(element);
				}
			}
			return result;
		}
	}


	static final class ProcessMatcher<T extends BaseEntityXml> extends
			ElementMatcher<T> {
		private final ProcessXml process;

		private ProcessMatcher(ProcessXml process) {
			this.process = process;
		}

		@Override
		public boolean matches(T element) {
			return ( element.getProcess().equals(process) );
		}
	}

	static <T> ElementMatcher<T> anyMatcher() {
		return new ElementMatcher<T>() {

			@Override
			public boolean matches(T element) {
				return true;
			}

			@Override
			public List<T> filter(List<T> src) {
				return src;
			}
		};
	}

	protected long count(ElementMatcher<T> matcher) {
		long result = 0;
		for (final T element : getEntities().values()) {
			if (matcher.matches(element)) {
				result++;
			}
		}

		return result;
	}

	static <T> List<T> slice(List<T> src, int offset, int limit) {
		if (offset < src.size()) {
			return Collections.unmodifiableList(src.subList(offset, offset
					+ Math.min(limit, src.size() - offset)));
		}

		return Collections.emptyList();
	}

	public PlainXmlWorkflowStorage getStorage() {
		return storage;
	}

	public void setStorage(PlainXmlWorkflowStorage storage) {
		this.storage = storage;
	}

	protected <K, V> Multimap<K, V> subMultimap(Multimap<K, V> src, int offset,
			int limit) {
		final ListMultimap<K, V> result = Multimaps
				.newListMultimap(new LinkedHashMap<K, Collection<V>>(),
						new ListSupplier<V, T>());

		final int i = 0;
		for (final Entry<K, V> entry : src.entries()) {
			if (i >= offset) {
				if ((i + offset) < limit) {
					result.put(entry.getKey(), entry.getValue());
				} else {
					break;
				}
			}
		}

		return result;
	}
}
