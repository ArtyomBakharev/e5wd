package ru.efive.workflow.designer.repository.plainxml;

import java.util.Comparator;

/**
 * Provides interface for creating custom {@link Comparator} implementations
 *
 * @author Sergey Zaytsev
 *
 */
public interface CompartorFactory<T> {

	/**
	 * Returns instance of {@link Comparator} that is capable of sorting objects of type {@code T} by specified field
	 *
	 * @param field the exact name of the field
	 *
	 * @return {@link Comparator} instance
	 */
	Comparator<T> createComparator(String field);

	Comparator<T> createComparator(String field, boolean inverse);
}
