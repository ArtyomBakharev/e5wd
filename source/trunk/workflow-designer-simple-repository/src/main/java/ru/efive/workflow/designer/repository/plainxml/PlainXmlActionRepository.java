package ru.efive.workflow.designer.repository.plainxml;

import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import ru.efive.workflow.designer.interfaces.Activity;
import ru.efive.workflow.designer.interfaces.Process;
import ru.efive.workflow.designer.model.ActionXml;
import ru.efive.workflow.designer.model.ProcessXml;
import ru.efive.workflow.designer.model.StatusChangeActionXml;
import ru.efive.workflow.designer.model.StatusXml;
import ru.efive.workflow.designer.repository.ActionRepository;

import javax.ejb.Local;
import javax.ejb.Stateless;
import java.util.*;

/**
 * Implementation of {@link ActionRepository} based on {@link PlainXmlWorkflowStorage}
 *
 * @author Sergey Zaytsev
 *
 */
@Stateless
@Local(ActionRepository.class)
public class PlainXmlActionRepository<T extends ActionXml> extends AbstractNamedPlainXmlRepository<T> implements
		ActionRepository<T> {

	@Override
	protected T saveInternal(T entity) {
		return getStorage().saveAction(entity);
	}

	@Override
	protected void deleteInternal(T entity) {
		getStorage().removeAction(entity);
	}

	@Override
	@SuppressWarnings("unchecked")
	protected Map<String, T> getEntities() {
		return (Map<String, T>) getStorage().getActions();
	}

	@Override
	public Multimap<StatusXml, ActionXml> findAllGroupedByStatus(Process process, int offset, int limit, String orderBy, boolean asc) {
		@SuppressWarnings({ "rawtypes", "unchecked" })
		final ListMultimap<StatusXml, ActionXml> all = Multimaps.newListMultimap(
				new TreeMap<StatusXml, Collection<ActionXml>>((Comparator) getComparatorFactory()
						.createComparator("name")), new ListSupplier<ActionXml, T>());

		final Collection<? extends ActionXml> values = getStorage().getActions().values();
		for (final ActionXml action : values) {
			if (action instanceof StatusChangeActionXml) {
				final StatusChangeActionXml statusChangeAction = (StatusChangeActionXml) action;
				if(action.getProcess().getId() == process.getId())
				{
					all.put(statusChangeAction.getFromStatus(), statusChangeAction);
					all.put((StatusXml) statusChangeAction.getToStatus(), statusChangeAction);
				}
			}
		}

		return subMultimap(all, offset, limit);
	}

    /**
     * TODO: Document type
     *
     * @author Pavel Shashok
     *
     */
    static final class FromStatusListMatcher<T extends ActionXml> extends
            ElementMatcher<T> {
        private final String statusId;

        private FromStatusListMatcher(String id) {
            this.statusId = id.toLowerCase();
        }

        @Override
        public boolean matches(T element) {
            if(!(element instanceof StatusChangeActionXml) )      {
                return false;
            }
            StatusChangeActionXml t = (StatusChangeActionXml)element;
            if(t.getFromStatus()==null)     {
                return false;
            }
            if(t.getFromStatus().getId()==null)     {
                return false;
            }
            return t.getFromStatus().getId().equals(statusId);
        }
    }

    //Получение всех status change action, у которых в from_status_list есть указываемый status id (получение всех доступных действий из текущего статуса)
    @Override
    public Iterable<T> findFromStatus(Process process,String id){
        return findInternal((ProcessXml)process, 0, (int)this.count(), null, false, new FromStatusListMatcher<T>(
                id));
    }
    //Получение всех local, pre action, post action для status change и no status action (параметр - action id)
    @Override
    public Iterable<Activity> findActivity(Process process,String id){
        List<Activity> r = new ArrayList<Activity>();
        ActionXml a = this.findOne(id);
        r.addAll(a.getPreActivities());
        r.addAll(a.getPostActivities());
        return null;
    }
}
