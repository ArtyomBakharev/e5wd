package ru.efive.workflow.designer.repository.plainxml;

import java.util.*;

import javax.ejb.Local;
import javax.ejb.Stateless;

import ru.efive.workflow.designer.interfaces.Activity;
import ru.efive.workflow.designer.interfaces.Process;
import ru.efive.workflow.designer.model.ActionXml;
import ru.efive.workflow.designer.model.ActivityXml;
import ru.efive.workflow.designer.model.StatusXml;
import ru.efive.workflow.designer.model.ProcessXml;
import ru.efive.workflow.designer.repository.ActivityRepository;

import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;

/**
 * Implementation of {@link ActivityRepository} based on {@link PlainXmlWorkflowStorage}
 *
 * @author Sergey Zaytsev
 *
 */
@Stateless
@Local(ActivityRepository.class)
public class PlainXmlActivityRepository<T extends ActivityXml> extends AbstractNamedPlainXmlRepository<T> implements
		ActivityRepository<T> {

	@Override
	protected T saveInternal(T entity) {
		return getStorage().saveActivity(entity);
	}

	@Override
	protected void deleteInternal(T entity) {
		getStorage().removeActivity(entity);
	}

	@Override
	@SuppressWarnings("unchecked")
	protected Map<String, T> getEntities() {
		return (Map<String, T>) getStorage().getActivities();
	}

	@Override
	public Multimap<StatusXml, ActivityXml> findAllGroupedByStatus(Process process, int offset, int limit, String orderBy, boolean asc) {
		@SuppressWarnings({ "rawtypes", "unchecked" })
		final ListMultimap<StatusXml, ActivityXml> all = Multimaps.newListMultimap(
				new TreeMap<StatusXml, Collection<ActivityXml>>((Comparator) getComparatorFactory()
						.createComparator("name")), new ListSupplier<ActivityXml, T>());

		for (final StatusXml status : getStorage().getStatuses().values()) {
			if(status.getProcess().getId() == process.getId())
			{
				all.putAll(status, status.getPreActivities());
				all.putAll(status, (Iterable<? extends ActivityXml>) status.getPostActivities());
			}
		}

		return subMultimap(all, offset, limit);
	}

	@Override
	public Multimap<ActionXml, ActivityXml> findAllGroupedByAction(Process process, int offset, int limit, String orderBy, boolean asc) {
		@SuppressWarnings({ "rawtypes", "unchecked" })
		final ListMultimap<ActionXml, ActivityXml> all = Multimaps.newListMultimap(
				new TreeMap<ActionXml, Collection<ActivityXml>>((Comparator) getComparatorFactory()
						.createComparator("name")), new ListSupplier<ActivityXml, T>());

		for (final ActionXml action : getStorage().getActions().values()) {
			if(action.getProcess().getId() == process.getId())
			{
				all.putAll(action, action.getPreActivities());
				all.putAll(action, action.getPostActivities());
			}
		}

		return subMultimap(all, offset, limit);
	}

    //Получение всех local, pre action, post action для status change и no status action (параметр - action id)
    @Override
    public Iterable<Activity> findActivity(Process process,String id){
        return null;
    }

}
