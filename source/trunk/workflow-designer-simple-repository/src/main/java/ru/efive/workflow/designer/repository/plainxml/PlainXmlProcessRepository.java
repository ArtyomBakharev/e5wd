package ru.efive.workflow.designer.repository.plainxml;

import org.apache.log4j.Logger;
import ru.efive.workflow.designer.interfaces.Process;
import ru.efive.workflow.designer.model.ActionXml;
import ru.efive.workflow.designer.model.ActivityXml;
import ru.efive.workflow.designer.model.ProcessXml;
import ru.efive.workflow.designer.model.StatusXml;
import ru.efive.workflow.designer.repository.ProcessRepository;

import javax.ejb.Local;
import javax.ejb.Stateless;
import java.util.Map;


/**
 * Implementation of {@link ProcessRepository} based on {@link PlainXmlWorkflowStorage}
 *
 * @author Sergey Zaytsev
 *
 */
@Stateless
@Local(ProcessRepository.class)
public class PlainXmlProcessRepository extends AbstractNamedPlainXmlRepository<ProcessXml> implements ProcessRepository<ProcessXml> {
    protected static Logger log = Logger.getLogger(PlainXmlProcessRepository.class);

	@Override
	protected ProcessXml saveInternal(ProcessXml entity) {
		return getStorage().saveProcess(entity);
	}
	

	@Override
	protected void deleteInternal(ProcessXml entity) {
        log.warn("$$$ deleteInternal entity = " + entity);
		for(ActivityXml a : getStorage().getActivities().values() )
			if(a.getProcess().equals(entity))
				getStorage().removeActivity(a);
		for(ActionXml a : getStorage().getActions().values() )
			if(a.getProcess().equals(entity))
				getStorage().removeAction(a);
		for(StatusXml a : getStorage().getStatuses().values() )    {
            log.warn("$$$ StatusXml = " + a);
            log.warn("$$$ a.getProcess() = " + a.getProcess());
			if(a.getProcess().equals(entity))
				getStorage().removeStatus(a);
        }
		getStorage().removeProcess(entity);
	}

	@Override
	protected Map<String, ProcessXml> getEntities() {
		return getStorage().getProcesses();
	}
	
	@Override
	public Iterable<ProcessXml> findAll(Process process, int offset, int limit,
			String orderBy, boolean asc) {
		return super.findInternal(null, offset, limit, orderBy, asc, null );
	}
	
	

}
