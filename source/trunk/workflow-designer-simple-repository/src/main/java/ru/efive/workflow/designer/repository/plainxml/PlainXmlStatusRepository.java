package ru.efive.workflow.designer.repository.plainxml;

import ru.efive.workflow.designer.interfaces.Activity;
import ru.efive.workflow.designer.model.StatusXml;
import ru.efive.workflow.designer.repository.StatusRepository;

import javax.ejb.Local;
import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Implementation of {@link StatusRepository} based on {@link PlainXmlWorkflowStorage}
 *
 * @author Sergey Zaytsev
 *
 */
@Stateless
@Local(StatusRepository.class)
public class PlainXmlStatusRepository extends AbstractNamedPlainXmlRepository<StatusXml> implements StatusRepository<StatusXml> {

	@Override
	protected StatusXml saveInternal(StatusXml entity) {
		return getStorage().saveStatus(entity);
	}

	@Override
	protected void deleteInternal(StatusXml entity) {
		getStorage().removeStatus(entity);
	}

	@Override
	protected Map<String, StatusXml> getEntities() {
		return getStorage().getStatuses();
	}

    //Получение всех local, pre status, post status activities для объекта status (параметр - status id)
    @Override
    public Iterable<Activity> findActivity(ru.efive.workflow.designer.interfaces.Process process,String id){
        List<Activity> r = new ArrayList<Activity>();
        StatusXml s = this.findOne(id);
        r.addAll(s.getLocalActivities());
        r.addAll(s.getPostActivities());
        r.addAll(s.getPreActivities());
        return r;
    }
    }
