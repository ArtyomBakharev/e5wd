package ru.efive.workflow.designer.repository.plainxml;

import java.util.Map;

import javax.ejb.Lock;
import javax.ejb.LockType;

import ru.efive.workflow.designer.model.ActionXml;
import ru.efive.workflow.designer.model.ActivityXml;
import ru.efive.workflow.designer.model.StatusXml;
import ru.efive.workflow.designer.model.ProcessXml;

/**
 * Low-level interface for XML-file-backed data storage
 *
 * @author Sergey Zaytsev
 *
 */
public interface PlainXmlWorkflowStorage {

	/**
	 * Returns read-only map of {@link StatusXml} objects mapped by Id
	 *
	 * @return map of {@link StatusXml} objects
	 */
	Map<String, StatusXml> getStatuses();
	
	/**
	 * Returns read-only map of {@link ProcessXml} objects mapped by Id
	 *
	 * @return map of {@link ProcessXml} objects
	 */
	Map<String, ProcessXml> getProcesses();

	/**
	 * Returns read-only map of {@link ActionXml} objects mapped by Id
	 *
	 * @return a map of String:{@link ActionXml} entries
	 */
	Map<String, ? extends ActionXml> getActions();

	/**
	 * Returns read-only map of {@link ActivityXml} objects mapped by Id
	 *
	 * @return a map of String:{@link ActivityXml} entries
	 */
	Map<String, ? extends ActivityXml> getActivities();

	/**
	 * Saves new or updates existing {@link StatusXml} object
	 *
	 * @param status the object to save
	 * @return possibly new instance of {@link StatusXml}
	 */
	@Lock(LockType.WRITE)
	StatusXml saveStatus(StatusXml status);

	/**
	 * Saves new or updates existing {@link ActionXml} object
	 *
	 * @param action the action to be saved/updated
	 * @return persistent {@link ActionXml} object
	 */
	@Lock(LockType.WRITE)
	<T extends ActionXml> T saveAction(T action);
	
	/**
	 * Saves new or updates existing {@link ProcessXml} object
	 *
	 * @param action the action to be saved/updated
	 * @return persistent {@link ProcessXml} object
	 */
	@Lock(LockType.WRITE)
	<T extends ProcessXml> T saveProcess(T process);
	

	/**
	 * Saves new or updates existing {@link ActivityXml} object
	 *
	 * @param activity the activity object to be saved/updated
	 * @return persistent {@link ActivityXml} object
	 */
	@Lock(LockType.WRITE)
	<T extends ActivityXml> T saveActivity(T activity);


	/**
	 * @param status
	 */
	public void removeStatus(StatusXml status);

	/**
	 * @param action
	 */
	public <T extends ActionXml> void removeAction(T action);

	/**
	 * @param activity
	 */
	public <T extends ActivityXml> void removeActivity(T activity);
	
	/**
	 * @param process
	 */
	public <T extends ProcessXml> void removeProcess(T process);
	

	/**
	 * Writes any unsaved changes to persistent storage
	 */
	@Lock(LockType.WRITE)
	void flush();

	/**
	 * Reverts storage to a previous state. Cancels all unsaved data.
	 */
	@Lock(LockType.WRITE)
	void reset();
}
