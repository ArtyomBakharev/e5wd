package ru.efive.workflow.designer.repository.plainxml;

import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.efive.workflow.designer.model.*;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.*;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * The {@code PlainXmlWorkflowStorageBean} class implements {@link PlainXmlWorkflowStorage}.
 *
 * @author Sergey Zaytsev
 *
 */
@Startup
@Singleton(name = "PlainXmlWorkflowStorageBean")
@Lock(LockType.READ)
@Local(PlainXmlWorkflowStorage.class)
@AccessTimeout(value = 10, unit = TimeUnit.SECONDS)
public class PlainXmlWorkflowStorageBean implements PlainXmlWorkflowStorage {

	private static final Logger LOGGER = LoggerFactory.getLogger(PlainXmlWorkflowStorageBean.class);

	private static final String DEFAULT_STORAGE_DIR = ".e5wd";

	public static final String STORAGE_PATH_PARAM = "ru.efive.workflow.storage.simple";

	public static class NamedComparator<T extends BaseEntityXml> implements Comparator<T> {

		@Override
		public int compare(T o1, T o2) {
			return o1.getName().compareTo(o2.getName());
		}

	}

	private JAXBContext xmlBindingContext;

	private File fileStorage;

	private Storage dataStorage;

	private Map<String, StatusXml> statuses = new HashMap<String, StatusXml>();

	private Map<String, ActionXml> actions = new HashMap<String, ActionXml>();
	
	private Map<String, ProcessXml> processes = new HashMap<String, ProcessXml>();

	private Map<String, ActivityXml> activities = new HashMap<String, ActivityXml>();

	private boolean dirty;

	/**
	 * Returns initialized {@link JAXBContext}
	 *
	 * @return
	 */
	private JAXBContext getXmlBindingContext() {
		// lazily creates context
		if (xmlBindingContext == null) {
			// prepare class scanner
			final Reflections reflections = new Reflections(IdentifiableXml.class.getPackage().getName());
			final Set<Class<?>> contextClasses = new HashSet<Class<?>>();
			// add root storage root class
			contextClasses.add(Storage.class);
			// search for XmlRootElement and XmlType annotated classes inside of a target package
			contextClasses.addAll(reflections.getTypesAnnotatedWith(XmlRootElement.class));
			contextClasses.addAll(reflections.getTypesAnnotatedWith(XmlType.class));

			LOGGER.debug("Classes to be bound {}", contextClasses);

			final Class<?>[] bindToClasses = new Class[contextClasses.size()];
			contextClasses.toArray(bindToClasses);

			try {
				xmlBindingContext = JAXBContext.newInstance(bindToClasses);
			} catch (final JAXBException e) {
				LOGGER.error("Unable to construct JAXB context", e);
				throw new IllegalStateException(e);
			}
		}

		return xmlBindingContext;
	}

	private void initStorage() throws IOException {
		fileStorage = new File(getStorageDir(), "storage.xml");
		dataStorage = fileStorage.canRead() ? readStorage(fileStorage) : new Storage();
		populateLookupTables();
	}

	private String getStorageDir() {
		final String dir = System.getProperty(STORAGE_PATH_PARAM, System.getProperty("user.home") + File.separator
				+ DEFAULT_STORAGE_DIR);

		new File(dir).mkdirs();
		return dir;
	}

	private Storage readStorage(File source) throws IOException {
		try {
			LOGGER.debug("About to read data from {}", source);
			return (Storage) getXmlBindingContext().createUnmarshaller().unmarshal(source);
		} catch (final JAXBException e) {
			LOGGER.error("Failed to read data from {}: {}", source, e);
			throw new IOException(e);
		}
	}

	private void populateLookupTables() {
		for (final StatusXml s : dataStorage.getStatuses()) {
			statuses.put(s.getId(), s);
		}
		
		for (final ProcessXml p : dataStorage.getProcesses()) {
			processes.put(p.getId(), p);
		}

		for (final ActionXml action : dataStorage.getActions()) {
			actions.put(action.getId(), action);
		}

		for (final ActivityXml activity : dataStorage.getActivities()) {
			activities.put(activity.getId(), activity);
		}

		dirty = false;
	}

	private void writeStorage() throws IOException {
		try {
			dataStorage.setStatuses(new ArrayList<StatusXml>(statuses.values()));
			dataStorage.setActions(new ArrayList<ActionXml>(actions.values()));
			dataStorage.setActivities(new ArrayList<ActivityXml>(activities.values()));
			dataStorage.setProcesses(new ArrayList<ProcessXml>(processes.values()));

			LOGGER.debug("About to write {} to {}", dataStorage, fileStorage);

			final Marshaller marshaller = getXmlBindingContext().createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			marshaller.marshal(dataStorage, fileStorage);

			dirty = false;
		} catch (final JAXBException e) {
			LOGGER.error("Unable to write to storage {}: {}", fileStorage, e);
			throw new IOException(e);
		}
	}

	static String newId() {
		return UUID.randomUUID().toString();
	}

	private <T extends AbstractEntityXml> boolean isNew(T entity) {
		return null == entity.getId();
	}

	private <T extends AbstractEntityXml> T assignId(T entity) {
		entity.setId(newId());
		return entity;
	}

	private <T extends AbstractEntityXml> T save(T entity, Map<String, T> dest) {
		T result = entity;
		if (isNew(entity)) {
			result = assignId(entity);
		}

		dest.put(result.getId(), result);
		dirty = true;

		return result;
	}

	private void remove(AbstractEntityXml entity, Map<String, ? extends AbstractEntityXml> container) {
        LOGGER.debug("remove(AbstractEntityXml entity, Map<String, ? extends AbstractEntityXml> container) entity =" +
                entity + " container = " + container);
		dirty = (null != container.remove(entity.getId())) || dirty;
	}

	/**
	 * @throws Exception
	 */
	@PostConstruct
	void initialize() throws Exception {
		initStorage();
		LOGGER.debug("Initialization completed");
	}

	@PreDestroy
	void terminate() throws Exception {
		if (dirty) {
			writeStorage();
		}
		LOGGER.debug("Terminated");
	}

	@Override
	public Map<String, StatusXml> getStatuses() {
		return Collections.unmodifiableMap(statuses);
	}
	
	@Override
	public Map<String, ProcessXml> getProcesses() {
		return Collections.unmodifiableMap(processes);
	}

	@Override
	public Map<String, ? extends ActionXml> getActions() {
		return Collections.unmodifiableMap(actions);
	}

	@Override
	public Map<String, ? extends ActivityXml> getActivities() {
		return Collections.unmodifiableMap(activities);
	}

	@Override
	@Lock(LockType.WRITE)
	public StatusXml saveStatus(StatusXml status) {
		return save(status, statuses);
	}

	@Override
	@Lock(LockType.WRITE)
	@SuppressWarnings("unchecked")
	public <T extends ActionXml> T saveAction(T action) {
		return (T) save(action, actions);
	}
	
	@Override
	@Lock(LockType.WRITE)
	@SuppressWarnings("unchecked")
	public <T extends ProcessXml> T saveProcess(T process) {
		return (T) save(process, processes);
	}

	@Override
	@Lock(LockType.WRITE)
	@SuppressWarnings("unchecked")
	public <T extends ActivityXml> T saveActivity(T activity) {
		return (T) save(activity, activities);
	}

	@Override
	public void removeStatus(StatusXml status) {
        LOGGER.warn("$$$ removeStatus status = " + status);
		remove(status, statuses);
	}
	
	@Override
	public <T extends ProcessXml> void removeProcess(T process) {
		remove(process, processes);		
	}

	@Override
	public <T extends ActionXml> void removeAction(T action) {
		remove(action, actions);
	}

	@Override
	public <T extends ActivityXml> void removeActivity(T activity) {
		remove(activity, activities);
	}

	@Override
	@Lock(LockType.WRITE)
	public void flush() {
		try {
			writeStorage();
		} catch (final IOException e) {
			throw new WorkflowStorageException(e);
		}
	}

	@Override
	@Lock(LockType.WRITE)
	public void reset() {
		try {
			initStorage();
		} catch (final IOException e) {
			throw new WorkflowStorageException(e);
		}
	}


}
