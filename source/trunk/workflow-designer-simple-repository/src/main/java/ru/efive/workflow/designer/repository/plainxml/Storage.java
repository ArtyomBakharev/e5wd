package ru.efive.workflow.designer.repository.plainxml;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.*;

import ru.efive.workflow.designer.model.ActionXml;
import ru.efive.workflow.designer.model.ActivityXml;
import ru.efive.workflow.designer.model.StatusXml;
import ru.efive.workflow.designer.model.ProcessXml;

/**
 * Wrapper class for workflow model objects
 *
 * @author Sergey Zaytsev
 *
 */
@XmlRootElement(name = "storage")
@XmlAccessorType(XmlAccessType.NONE)
public class Storage {

	@XmlElementRef(type = StatusXml.class)
	private List<StatusXml> statuses = new ArrayList<StatusXml>();
	
	@XmlElementRef(type = ProcessXml.class)
	private List<ProcessXml> processes = new ArrayList<ProcessXml>();

	@XmlAnyElement(lax = true) //kind a hack. Action is marked as @XmlTransient to allow property reordering so it can't be used directly by JAXB
	private List<ActionXml> actions = new ArrayList<ActionXml>();

	@XmlElementRef
	private List<ActivityXml> activities = new ArrayList<ActivityXml>();
	
	public List<ProcessXml> getProcesses() {
		return processes;
	}

	public void setProcesses(List<ProcessXml> processes) {
		this.processes = processes;
	}
	

	public List<StatusXml> getStatuses() {
		return statuses;
	}

	public void setStatuses(List<StatusXml> statuses) {
		this.statuses = statuses;
	}

	public List<ActionXml> getActions() {
		return actions;
	}

	public void setActions(List<ActionXml> actions) {
		this.actions = actions;
	}

	public List<ActivityXml> getActivities() {
		return activities;
	}

	public void setActivities(List<ActivityXml> activities) {
		this.activities = activities;
	}

	@Override
	public String toString() {
		return "Storage [statuses=" + statuses + ", actions=" + getActions() + ", activities=" + getActivities() + "]";
	}
}
