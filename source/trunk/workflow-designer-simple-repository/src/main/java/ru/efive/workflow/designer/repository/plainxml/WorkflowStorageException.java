package ru.efive.workflow.designer.repository.plainxml;

/**
 *
 *
 * @author Sergey Zaytsev
 *
 */
public class WorkflowStorageException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	/**
	 *
	 */
	public WorkflowStorageException() {
		super();
	}

	public WorkflowStorageException(String message, Throwable cause) {
		super(message, cause);
	}

	public WorkflowStorageException(String message) {
		super(message);
	}

	public WorkflowStorageException(Throwable cause) {
		super(cause);
	}
}
