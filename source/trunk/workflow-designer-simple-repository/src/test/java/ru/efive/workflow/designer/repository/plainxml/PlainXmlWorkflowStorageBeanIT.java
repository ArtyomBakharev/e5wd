package ru.efive.workflow.designer.repository.plainxml;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.EnterpriseArchive;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.jboss.shrinkwrap.resolver.api.DependencyResolvers;
import org.jboss.shrinkwrap.resolver.api.maven.MavenDependencyResolver;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import ru.efive.workflow.designer.interfaces.ReturnType;
import ru.efive.workflow.designer.model.*;

import javax.ejb.EJB;
import java.io.File;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Integration tests (IT) for {@link PlainXmlWorkflowStorageBean} class
 *
 * @author Sergey Zaytsev
 *
 */
@RunWith(Arquillian.class)
public class PlainXmlWorkflowStorageBeanIT {

	static String getStorageDir() {
		return System.getProperty("java.io.tmpdir") + File.separator + "test" + File.separator;
	}

	@Deployment
	public static Archive<?> createDeployment() {
		System.setProperty(PlainXmlWorkflowStorageBean.STORAGE_PATH_PARAM, getStorageDir());
		final MavenDependencyResolver resolver = DependencyResolvers.use(MavenDependencyResolver.class)
				.loadMetadataFromPom("pom.xml");

		return ShrinkWrap
				.create(EnterpriseArchive.class, "test-app.ear")
				.addAsModule(ShrinkWrap.create(JavaArchive.class, "test-ejb.jar")
						.addPackage(PlainXmlWorkflowStorage.class.getPackage()))
				.addAsLibraries(
						resolver.artifact("ru.efive.workflow.designer:workflow-designer-common:0.0.1-SNAPSHOT")
								.resolveAsFiles());
	}

	@EJB(mappedName = "java:global/test-app/test-ejb/PlainXmlWorkflowStorageBean")
	public PlainXmlWorkflowStorage workflowStorage;

	@After
	public void resetStorage() {
		final File dir = new File(getStorageDir());
		for (final File f : dir.listFiles()) {
			// TODO: need to force file deletion
			f.delete();
		}
		workflowStorage.reset();
	}

	//@Test
	public void testReadWrite() {
		assertTrue(workflowStorage.getStatuses().isEmpty());

		final StatusXml status = new StatusXml();
		status.setName("Test Status");
		status.setDescription("test descr");
		final StatusXml status0 = workflowStorage.saveStatus(status);

		final NoStatusActionXml action = new NoStatusActionXml();
		action.setName("test action");
		action.setDataType("STRING");
		action.setAvailabilityCondition("1 > 0");
		action.setEvaluationMessage("test");
		action.setDescription("123");
		workflowStorage.saveAction(action);

		assertFalse(workflowStorage.getStatuses().isEmpty());
		workflowStorage.flush();
		workflowStorage.reset();

		final StatusXml status1 = workflowStorage.getStatuses().get(status0.getId());
		assertNotNull(status1);
		assertEquals(status0.getName(), status1.getName());
		assertEquals(status0.getId(), status1.getId());
	}

	@Test
	public void crudWorks() {
		System.out.println("==== after preflight checks 0");
		assertTrue(workflowStorage.getStatuses().isEmpty());
		System.out.println("==== after preflight checks 1");
		assertTrue(workflowStorage.getActions().isEmpty());
		System.out.println("==== after preflight checks 2");
		assertTrue(workflowStorage.getActivities().isEmpty());

		System.out.println("==== after preflight checks");

		ActivityXml activity0 = new ActivityXml();
		activity0.setName("Test Activity 0");
		activity0.setRunCondition("1>0");
		activity0.setReturnType(ReturnType.VALUE);
		activity0.setReturnFormula("1-1");
		activity0.setDescription("test_commentary");
		activity0 = workflowStorage.saveActivity(activity0);
		assertNotNull(activity0.getId());
		assertEquals(1, workflowStorage.getActivities().size());

		SetPropertyActivityXml activity1 = new SetPropertyActivityXml();
		activity1.setName("Test Activity 1");
		activity1.setRunCondition("1>0");
		activity1.setReturnType(ReturnType.VALUE);
		activity1.setReturnFormula("1-1");
		activity1.setDescription("test_commentary");
		activity1.getPropertyChanges().add(new PropertyChangeDescriptorXml("test", "value"));
		activity1 = workflowStorage.saveActivity(activity1);
		assertNotNull(activity1.getId());
		assertEquals(2, workflowStorage.getActivities().size());

		final StatusXml status = new StatusXml();
		status.setName("Test Status");
		status.getPreActivities().add(activity0);
		((List<ActivityXml>)status.getPostActivities()).add(activity1);
		final StatusXml status0 = workflowStorage.saveStatus(status);
		assertEquals(1, workflowStorage.getStatuses().size());

		final NoStatusActionXml action = new NoStatusActionXml();
		action.setName("test action");
		action.setDataType("STRING");
		action.setAvailabilityCondition("1 > 0");
		action.setEvaluationMessage("test");
		action.setDescription("123");
		final NoStatusActionXml action1 = workflowStorage.saveAction(action);

		workflowStorage.flush();
		workflowStorage.reset();

		System.out.println("==== after reset");

		final StatusXml status1 = workflowStorage.getStatuses().get(status0.getId());
		assertNotNull(status1);
		assertEquals(status0.getId(), status1.getId());
		assertEquals(status.getName(), status1.getName());
		assertEquals(1, status1.getPostActivities().size());

		workflowStorage.removeStatus(status1);
		assertTrue(workflowStorage.getStatuses().isEmpty());
		assertEquals(2, workflowStorage.getActivities().size());
	}
}
