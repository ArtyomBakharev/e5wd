package ru.efive.workflow.designer.hibernate.dao;

import ru.efive.workflow.designer.hibernate.entity.ProcessEntity;
import ru.efive.workflow.designer.interfaces.NamedEntity;
import ru.efive.workflow.designer.interfaces.Process;
import ru.efive.workflow.designer.model.NamedEntityXml;
import ru.efive.workflow.designer.repository.NamedEntityRepository;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Dmitry Parshin
 * Date: 11.04.12
 */
public abstract class AbstractNamedRepositoryDAO<T extends NamedEntity> extends AbstractRepositoryDAO <T>   implements NamedEntityRepository<T>{

    /**
     * Returns all objects for process with matching name
     *
     * @param name    the name to search for
     * @param process process
     * @param offset  offset of result
     * @param limit   limit of result
     * @return matching entities
     */
    @Override
    public Iterable<T> findAllByName(Process process, String name, int offset, int limit, String orderBy, boolean asc) {
        List<T> ret=new ArrayList<T>();
        //log.warn(" ####ProcessRepositoryDAOBean Iterable findAllByName process id - "+process.getId()+" Entity - "+getEntityName()
        //+" ####ProcessRepositoryDAOBean Query - "+getQry(process,name,orderBy,asc));
        try {
            Query query = getEntityManager().createQuery(getQry(process, name, orderBy, asc));
            query.setFirstResult(offset);
            if(limit>0) query.setMaxResults(limit);
            ret  = query.getResultList();
            //log.warn(" #### ProcessRepositoryDAOBeanIterable findAllByName List size - "+ret.size());
        } catch (Exception e) {
            log.error(" ####ProcessRepositoryDAOBean ERROR findAllByName ", e);
        }
        return getEntity(ret);
    }

    public long countByName(String name) {
        //log.warn(" #### countByName - "+getEntityName()+" Name - "+name);
        return getEntity(findAllListRootEntity(getQry(null,name, null, true), 0, 0)).size();
    }

    protected CompartorFactory<T> getComparatorFactory() {
        return new CompartorFactory<T>() {

            @Override
            public Comparator<T> createComparator(String field) {
                if ("name".equals(field)) {
                    return new Comparator<T>() {

                        @Override
                        public int compare(T o1, T o2) {
                            return o1.getName().compareTo(o2.getName());
                        }

                    };
                } else if (null == field) {
                    return new Comparator<T>() {

                        @Override
                        public int compare(T o1, T o2) {
                            // TODO Auto-generated method stub
                            return 0;
                        }
                    };
                }

                throw new IllegalArgumentException("Unsupported field " + field);
            }

            @Override
            public Comparator<T> createComparator(String field, boolean inverse) {
                final Comparator<T> comparator = createComparator(field);
                if (inverse) {
                    return new InverseComparator<T>(comparator);
                } else {
                    return comparator;
                }
            }

        };
    }

    /**
     * TODO: Document type
     *
     * @author Sergey Zaytsev
     *
     */
    static final class NameMatcher<T extends NamedEntityXml> extends
            ElementMatcher<T> {
        private final String name;

        private NameMatcher(String name) {
            this.name = name.toLowerCase();
        }

        @Override
        public boolean matches(T element) {
            return (element.getName().toLowerCase().contains(name));
        }
    }

}
