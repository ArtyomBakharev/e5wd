package ru.efive.workflow.designer.hibernate.dao;

import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import org.apache.log4j.Logger;
import ru.efive.workflow.designer.hibernate.entity.ProcessEntity;
import ru.efive.workflow.designer.interfaces.*;
import ru.efive.workflow.designer.interfaces.Process;
import ru.efive.workflow.designer.repository.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import java.util.*;

/**
 * Created by Dmitry Parshin
 * Date: 11.04.12
 */
public abstract class AbstractRepositoryDAO <T extends AbstractEntity>  implements Repository<String,T> {
    protected static Logger log = Logger.getLogger(AbstractRepositoryDAO.class);

    public abstract String getEntityName();
    public abstract String getProcessPath();
    public abstract EntityManager getEntityManager();
    public abstract Class getClassEntity();

    public String getQry(Process process,final String name, String orderBy, boolean asc){
        //log.warn("####-1 AbstractRepositoryDAO  getQry");
        String qry = "from "+getEntityName();
        String where = " where  ";

        if(process != null){
            if(process.getId()!=null && !process.getId().equals("0")){
                qry += where+getProcessPath()+"processId = "+process.getId()+" ";
                where = " and ";
            }
            if(process.getDataType()!= null) {
                if(process.getDataType().indexOf("%")!=-1)
                    qry += where+getProcessPath()+"dataType like '"+process.getDataType()+"' ";
                else
                    qry += where+getProcessPath()+"dataType = '"+process.getDataType()+"' ";
                //where = " and ";
            }
        }
        if(name!=null){
            if(getProcessPath()!=null){
                if(name.indexOf("%")!=-1)
                    qry += where+" upper(name) like upper('"+name+"') ";
                else
                    qry += where+" upper(name) = upper('"+name+"') ";
            }
        }
        if(orderBy!=null){
            qry +=" order by "+orderBy;
            if(asc) qry +=" ASC ";
            else qry +=" DESC ";
        }
        //log.warn("####  getQry  - "+qry);
        return qry;
    }

    /**
     * Saves a given entity. Use the returned instance for further operations as the save operation might have changed
     * the
     * entity instance completely.
     *
     * @param entity
     * @return the saved entity
     */
    @Override
    public abstract T save(T entity);

    /**
     * Saves all given entities.
     *
     * @param entities
     * @return
     */
    @Override
    public Iterable<T> save(Iterable<? extends T> entities) {
        //log.warn("####-1 AbstractRepositoryDAO  Iterable<T> save  )");
        List<T> ret = new ArrayList<T>();
        T t=null;
            for(T pe:entities){
                t=pe;
                pe=save(pe);
                if(pe==null) log.error("####AbstractRepositoryDAO ERROR (Iterable) Save - " + t.toString());
                else {
                    //log.warn("####AbstractRepositoryDAO (Iterable) Save - " + pe.toString());
                    ret.add(pe);
                }
            }
        return ret;
    }


    public List<T> findAllListRootEntity(String qry, int offset, int limit){
        //log.warn("####-1 AbstractRepositoryDAO  List<T> findAllListRootEntity  (session.hashCode " );
        List<T> ret = new ArrayList<T>();

        try {
            Query query = getEntityManager().createQuery(qry);
            if(offset>0) query.setFirstResult(offset);
            if(limit>0) query.setMaxResults(limit);
            ret = query.getResultList();

            //log.warn(" #### AbstractRepositoryDAO (parameterized) findAllListRootEntity ( offset - "+offset+"  ,limit - "+limit+" , query - "+qry +")   return List size - "+ret.size());
        } catch (Exception e) {
            log.error(" #### AbstractRepositoryDAO ERROR findAllListRootEntity " + qry, e);
        }
        return  getEntity(ret);
    }

    public List<T> findAllListRootEntity(){
        //log.warn("####-1 AbstractRepositoryDAO  List<T> findAllListRootEntit ");
        List<T> ret = new ArrayList<T>();
        String qry = getQry(null,null, null, true);

        try {
            Query query = getEntityManager().createQuery(qry);
            ret = query.getResultList();

            //log.warn(" ####AbstractRepositoryDAO findAllListRootEntity - "+qry+") return List size - "+ret.size());
        } catch (Exception e) {
            log.error(" ####AbstractRepositoryDAO ERROR findAllListRootEntity - " + qry, e);
        }
        return  getEntity(ret);
    }

    public List<T> getEntity(List<T> mEntity){
        //log.warn("####-1 AbstractRepositoryDAO  List<T> getEntity");
        List<T> ret =  new ArrayList<T>();
        for(T t: mEntity) {
            ret.add(getEntity(t));
        }
        return ret;
    }

    public abstract T getEntity(T mEntity);


    /**
     * Returns all instances of the type.
     *
     * @return all entities
     */
    @Override
    public  Iterable<T> findAll(){
        //log.warn(" ####-1 AbstractRepositoryDAO  Iterable findAll "+getEntityName());
        List<T> ret = findAllListRootEntity();
        return  ret;
    }

    /**
     * Returns a partial view of entities meeting the specified restrictions
     *
     * @param offset  the offset from beginning (zero-based)
     * @param limit   the max number of elements to be returned
     * @param orderBy the name of the field to order by
     * @param asc     if set to {@code true} then entities are sorted in ascending manner, and descending otherwise
     * @return
     */
    @Override
    public Iterable<T> findAll(Process process, int offset, int limit, String orderBy, boolean asc) {
        //log.warn(" ####-1 AbstractRepositoryDAO  Iterable findAll process id - "+process.getId()+" Entity - "+getEntityName());
        return   findAllListRootEntity(getQry(process,null, orderBy, asc), offset, limit);
    }

    /**
     * Returns the number of entities available.
     *
     * @return the number of entities
     */
    @Override
    public  long count(){
        //log.warn(" ####-1 AbstractRepositoryDAO count");
        List<T> ret = findAllListRootEntity();
        //log.warn(" #### AbstractRepositoryDAO count - " + getEntityName() + " ret.size() = " + ret.size());
       return ret.size();
    }

    /**
     * Deletes a given entity.
     *
     * @param entity
     */
    @Override
    public void delete(T entity) {
        //log.warn(" ####-1 AbstractRepositoryDAO delete Entity "+getEntityName());
        delete(entity.getId());
    }

    /**
     * Deletes the given entities.
     *
     * @param entities
     */
    @Override
    public void delete(Iterable<? extends T> entities) {
        //log.warn(" ####-1 AbstractRepositoryDAO (Iterable) delete - "+getEntityName());
        Iterator<T> p = (Iterator<T>) entities.iterator();
        while ( p.hasNext() ){
            T pr =(T) p.next();
            delete(pr.getId());
        }
    }

    /**
     * Deletes all entities managed by the repository.
     */
    @Override
    public void deleteAll() {
        //log.warn(" ####-1 AbstractRepositoryDAO  delete All - "+getEntityName());
        List<T> ret = findAllListRootEntity();
        for (T t: ret) delete(t.getId());
    }

    /**
     * Deletes the entity with the given id.
     *
     * @param id
     */
    @Override
    public void delete(String id) {
        //log.warn("####-1 ProcessRepositoryDAOBean  delete -"+getEntityName()+"  ID="+id);
        Integer entity_id=null;
        Object entity=null;
        if(id!=null){
            try {
                entity_id = Integer.parseInt(id);
            } catch (NumberFormatException e) {
                log.error("####ProcessRepositoryDAOBean Error delete ID is not number - "+id,e);
                return;
            }

            try {
                entity = getEntityManager().find(getClassEntity(),  entity_id);
                getEntityManager().remove(entity);
            } catch (Exception e) {
                log.error("####ProcessRepositoryDAOBean Error delete ID is not number - " + id,e);
            }
        }else  log.error("####ProcessRepositoryDAOBean Error delete ID is null ");
    }

    /**
     * Returns whether an entity with the given id exists.
     *
     * @param id
     * @return true if an entity with the given id exists, alse otherwise
     * @throws IllegalArgumentException if primaryKey is {@code null}
     */

    @Override
    public boolean exists(String id) {
        //log.warn("####-1 AbstractRepositoryDAO  exists, ID="+id);
        boolean ret = false;
        if(findOne(id) != null) ret=true;
        //log.warn(" #### exists ID - "+id+"  "+ret+"  Entity "+getEntityName());
        return ret;
    }

    /**
     * Retrieves an entity by its primary key.
     *
     * @param id
     * @return the entity with the given primary key or {@code null} if none found
     * @throws IllegalArgumentException if primaryKey is {@code null}
     */
    @Override
    public T findOne(String id) {
        //log.warn(" ####-1 AbstractRepositoryDAO findOne - "+getEntityName()+" ID - "+id);
        T searchEntity=null;
        Integer entity_id=null;
        if(id!=null){
            try {
                entity_id = Integer.parseInt(id);
            } catch (NumberFormatException e) {
                log.error("####AbstractRepositoryDAO Error findOne ID is not number - "+id,e);
                return null;
            }
            try {
                searchEntity = (T) getEntityManager().find(getClassEntity(), entity_id);
                //log.warn(" ####ProcessRepositoryDAOBean findOne Entity - "+searchEntity.toString());
                return   getEntity(searchEntity);
            } catch (Exception e) {
                log.error("####AbstractRepositoryDAO Error findOne ID - " + id, e);
            }
        }  log.error("####AbstractRepositoryDAO Error findOne ID is null  ");
        return null;
    }



    static class InverseComparator<T> implements Comparator<T> {

        private final Comparator<T> delegate;

        /**
         * @param delegate
         */
        public InverseComparator(Comparator<T> delegate) {
            this.delegate = delegate;
        }

        @Override
        public int compare(T o1, T o2) {
            return -delegate.compare(o1, o2);
        }

    }

    static abstract class ElementMatcher<T> {

        public abstract boolean matches(T element);

        public List<T> filter(List<T> src) {
            final List<T> result = new ArrayList<T>();
            for (final T element : src) {
                if (matches(element)) {
                    result.add(element);
                }
            }
            return result;
        }
    }


    static final class ProcessMatcher<T extends BaseEntity> extends
            ElementMatcher<T> {
        private final Process process;

        private ProcessMatcher(Process process) {
            this.process = process;
        }

        @Override
        public boolean matches(T element) {
            return ( element.getProcess().equals(process) );
        }
    }

    static  <T> ElementMatcher<T> anyMatcher() {
        return new ElementMatcher<T>() {

            @Override
            public boolean matches(T element) {
                return true;
            }

            @Override
            public List<T> filter(List<T> src) {
                return src;
            }
        };
    }

    protected <K, V> Multimap<K, V> subMultimap(Multimap<K, V> src, int offset,
            int limit) {
        final ListMultimap<K, V> result = Multimaps
                .newListMultimap(new LinkedHashMap<K, Collection<V>>(),
                        new ListSupplier<V, T>());

        final int i = 0;
        for (final Map.Entry<K, V> entry : src.entries()) {
            if (i >= offset) {
                if ((i + offset) < limit) {
                    result.put(entry.getKey(), entry.getValue());
                } else {
                    break;
                }
            }
        }

        return result;
    }
}
