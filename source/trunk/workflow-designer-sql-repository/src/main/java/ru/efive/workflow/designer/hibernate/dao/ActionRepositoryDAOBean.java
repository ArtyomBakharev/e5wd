package ru.efive.workflow.designer.hibernate.dao;

import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import org.apache.log4j.Logger;
import ru.efive.workflow.designer.hibernate.entity.*;
import ru.efive.workflow.designer.interfaces.*;
import ru.efive.workflow.designer.interfaces.Process;
import ru.efive.workflow.designer.model.NoStatusActionXml;
import ru.efive.workflow.designer.model.StatusChangeActionXml;
import ru.efive.workflow.designer.repository.ActionRepositoryDAO;

import javax.ejb.Local;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.*;

/**
 * Created by Dmitry Parshin
 * Date: 12.04.12
 */
@Singleton(name = "ActionRepositoryDAOBean")
//@Stateless
@Local(ActionRepositoryDAO.class)
public class ActionRepositoryDAOBean<T extends Action> extends AbstractNamedRepositoryDAO<T> implements ActionRepositoryDAO<T> {
    protected static Logger log = Logger.getLogger(ActionRepositoryDAOBean.class);
    @PersistenceContext(unitName = "e5wd")
    private EntityManager em;

    public ActionRepositoryDAOBean() {
    }
    public ActionRepositoryDAOBean(EntityManager em) {
        this.em = em;
    }

    @Override
    public String getEntityName() {
        return "ActionEntity";
    }

    @Override
    public String getProcessPath() {
        return "process.";
    }

    @Override
    public EntityManager getEntityManager() {
        return em;
    }
    private void setEntityManager(EntityManager em){
        this.em = em;
    }

    @Override
    public Class getClassEntity() {
        return ActionEntity.class;
    }


    /**
      * Saves a given entity. Use the returned instance for further operations as the save operation might have changed
      * the
      * entity instance completely.
      *
      * @param entity
      * @return the saved entity
      */
     @Override
    public T save(T entity) {
         //log.warn("####  ActionRepositoryDAOBean save  "+entity.toString());

         T ret=null;
         Integer entityId=null;
         ProcessEntity processEntity =  null;
         ActionEntity actionEntity = null;
         NoStatusActionEntity noStatusActionEntity = null;
         StatusChangeActionEntity statusChangeActionEntity = null;


         if(entity==null) {
             log.error("#### ERROR ActionRepositoryDAOBean Save entity is null ");
             return null;
         }

         if(entity.getId()!=null){
             try {
                 entityId = Integer.parseInt(entity.getId());
             } catch (NumberFormatException e) {
                 log.error("####  ActionRepositoryDAOBean Save, ID is not number - "+entity.getId());
                 return null;
             }
             if(entityId.equals(new Integer(0))){
                 entityId=null;
             }
         }


/*get Process*/
         if(entity.getProcess()!=null && entity.getProcess().getId()!=null){
             Integer processId=null;
             try {
                 processId = Integer.parseInt(entity.getProcess().getId());
             } catch (NumberFormatException e) {
                 log.error("####  ActionRepositoryDAOBean Save, Process ID is not number - "+entity.getProcess().getId());
                 return null;
             }

             try {
                 processEntity = (ProcessEntity) em.find(ProcessEntity.class, processId);
                 if(processEntity==null)  {
                     log.error("####  ActionRepositoryDAOBean Save, Process ID not found - " + entity.getProcess().getId());
                     return null;
                 }
             } catch (Exception e) {
                 log.error("####  ActionRepositoryDAOBean Save, Process ID not found - " + entity.getProcess().getId());
                 return null;
             }
         } else{
             log.error("####  ActionRepositoryDAOBean Save, Process is NULL  )");
             return null;
         }

         //log.warn("####  ActionRepositoryDAOBean Save, Process  found - "+processEntity.getId());


/*ActionEntity*/
             if(entityId!=null){

                 try {
                     actionEntity = (ActionEntity)em.find(ActionEntity.class,entityId);
                     if(actionEntity==null) {
                         log.error("####  ActionRepositoryDAOBean Save, actionEntity ID not found " + entityId);
                         return null;
                     }
                 } catch (Exception e) {
                     log.error("####  ActionRepositoryDAOBean Save, actionEntity ID not found " + entityId);
                     return null;
                 }
             }else{
                 actionEntity = new ActionEntity();
             }

         actionEntity.fromInterface(entity);
         actionEntity.setProcess(processEntity);
         try {
             em.persist(actionEntity);
             processEntity.getActionEntity().add(actionEntity);
             em.flush();
         } catch (Exception e) {
             log.error("#### ERROR ActionRepositoryDAOBean Save, insert new actionEntity ",e);
             return null;
         }
         //log.warn("####  ActionRepositoryDAOBean Save, insert new actionEntity ");


         entityId=actionEntity.getActionId();

/*getPreActivities*/ //log.warn("####  ActionRepositoryDAOBean Save PreActivities");
             actionEntity.getPreActivities().clear();
             if(entity.getPreActivities()!=null){
                 for(Activity a:entity.getPreActivities()){
                     ActivityEntity activityEntity=null;
                     Integer activitiId=null;
                     if(a.getId()!=null){
                         try {
                             activitiId = Integer.parseInt(a.getId());
                         } catch (NumberFormatException e) {
                             log.error("####  ActionRepositoryDAOBean Save, getPreActivities, Activity ID is not number - "+a.toString());
                             continue;
                         }
                         if(activitiId.equals(new Integer(0))) activitiId=null;
                     }

                     if(activitiId!=null) {
                         try {
                             activityEntity = (ActivityEntity)em.find(ActivityEntity.class, activitiId);
                             if(activityEntity!=null) actionEntity.getPreActivities().add(activityEntity);
                         } catch (Exception e) {
                             log.error("#### ERROR ActionRepositoryDAOBean Save, PreActivities, Activity ID is not number - "+a.toString());
                         }
                     }
                 }
             }  else log.error("####  ActionRepositoryDAOBean Save, getPreActivities is null ");


/*getPostActivities*/   //log.warn("####  ActionRepositoryDAOBean Save PostActivities ");

             actionEntity.getPostActivities().clear();
             if(entity.getPostActivities()!=null){
                 for(Activity a:entity.getPostActivities()){
                     ActivityEntity activityEntity=null;
                     Integer activitiId=null;
                     if(a.getId()!=null){
                         try {
                             activitiId = Integer.parseInt(a.getId());
                         } catch (NumberFormatException e) {
                             log.error("####  ActionRepositoryDAOBean Save, getPostActivities, Activity ID is not number - "+a.toString());
                             continue;
                         }
                         if(activitiId.equals(new Integer(0))) activitiId=null;
                     }
                     if(activitiId!=null){
                        try {
                             activityEntity = (ActivityEntity)em.find(ActivityEntity.class, activitiId);
                             if(activityEntity!=null)  actionEntity.getPostActivities().add(activityEntity);
                         } catch (Exception e) {
                            log.error("#### ERROR ActionRepositoryDAOBean Save, getPostActivities, Activity ID is not number - "+a.toString());
                         }
                     }
                 }
             } else log.error("####  ActionRepositoryDAOBean Save, getPostActivities is null");

             em.flush();

/*getLocalActivities*/   //log.warn("####  ActionRepositoryDAOBean Save localActivities ");

         actionEntity.getLocalActivities().clear();
         if(entity.getLocalActivities()!=null){
             for(Activity a:entity.getLocalActivities()){
                 ActivityEntity activityEntity=null;
                 Integer activitiId=null;
                 if(a.getId()!=null){
                     try {
                         activitiId = Integer.parseInt(a.getId());
                     } catch (NumberFormatException e) {
                         log.error("####  ActionRepositoryDAOBean Save, getLocalActivities, Activity ID is not number - "+a.toString());
                         continue;
                     }
                     if(activitiId.equals(new Integer(0))) activitiId=null;
                 }
                 if(activitiId!=null){
                     try {
                         activityEntity = (ActivityEntity)em.find(ActivityEntity.class, activitiId);
                         if(activityEntity!=null)  actionEntity.getLocalActivities().add(activityEntity);
                     } catch (Exception e) {
                         log.error("#### ERROR ActionRepositoryDAOBean Save, getLocalActivities, Activity ID is not number - "+a.toString());
                     }
                 }
             }
         } else log.error("####  ActionRepositoryDAOBean Save, getLocalActivities is null");

         em.flush();


/*NoStatusAction*/
             if(entity instanceof NoStatusAction) {
                 //log.warn("####  ActionRepositoryDAOBean Save NoStatusActionEntity");
                     try {
                         noStatusActionEntity = (NoStatusActionEntity)em.find(NoStatusActionEntity.class, entityId);
                         if(noStatusActionEntity==null) noStatusActionEntity = new NoStatusActionEntity();
                     } catch (Exception e) {
                         noStatusActionEntity = new NoStatusActionEntity();
                     }
                 noStatusActionEntity.setActionEntity(actionEntity);
                 try {
                     em.persist(noStatusActionEntity);
                     actionEntity.setNoStatusActionEntity(noStatusActionEntity);
                     em.flush();
                 } catch (Exception e1) {
                     log.error("####ERROR  ActionRepositoryDAOBean Save NoStatusActionEntity, insert new Status ");
                     return null;
                 }

                     NoStatusActionXml noStatusActionXml = new NoStatusActionXml();
                     noStatusActionXml.fromInterface(noStatusActionEntity);

                     ret = (T) noStatusActionXml;
             }

/*StatusChangeAction*/
             if(entity instanceof StatusChangeAction) {
                 //log.warn("####  ActionRepositoryDAOBean Save, StatusChangeAction");

                     try {
                         statusChangeActionEntity = (StatusChangeActionEntity)em.find(StatusChangeActionEntity.class, entityId);
                         if(statusChangeActionEntity==null) statusChangeActionEntity = new StatusChangeActionEntity();
                     } catch (Exception e) {
                         statusChangeActionEntity = new StatusChangeActionEntity();
                     }

/*getFromStatus*/
                 if( ((StatusChangeAction)entity).getFromStatus()==null){
                     log.error("#### ERROR  ActionRepositoryDAOBean Save StatusChangeActionEntity - getFromStatus()==null ");
                     return null;
                 }
                 if( ((StatusChangeAction)entity).getFromStatus().getId()==null){
                     log.error("#### ERROR  ActionRepositoryDAOBean Save StatusChangeActionEntity - ((StatusChangeAction)entity).getFromStatus().getId()==null ");
                     return null;
                 }
                 Integer fromStatusId = null;
                 try {
                     fromStatusId = Integer.parseInt(((StatusChangeAction)entity).getFromStatus().getId());
                 } catch (NumberFormatException ex) {
                     log.error("####  ActionRepositoryDAOBean Save StatusChangeActionEntity, fromStatusId is not number ",ex);
                     return null;
                 }

                 //log.warn("#### ActionRepositoryDAOBean load fromStatus id - "+fromStatusId+"  (session.hashCode - ");

                 StatusEntity fromStatus = null;
                 try {
                     fromStatus = (StatusEntity) em.find(StatusEntity.class,fromStatusId);
                     if(fromStatus==null) {
                         log.error("####  ActionRepositoryDAOBean Save StatusChangeActionEntity, fromStatus not found ");
                         return null;
                     }
                 } catch (Exception ex) {
                     log.error("####  ActionRepositoryDAOBean Save StatusChangeActionEntity, fromStatus not found ",ex);
                     return null;
                 }

/*getToStatus*/
                 if( ((StatusChangeAction)entity).getToStatus()==null){
                     log.error("#### ERROR  ActionRepositoryDAOBean Save StatusChangeActionEntity - getToStatus()==null ");
                     return null;
                 }
                 if( ((StatusChangeAction)entity).getToStatus().getId()==null){
                     log.error("#### ERROR  ActionRepositoryDAOBean Save StatusChangeActionEntity - ((StatusChangeAction)entity).getToStatus().getId()==null ");
                     return null;
                 }
                 Integer toStatusId = null;
                 try {
                     toStatusId = Integer.parseInt(((StatusChangeAction)entity).getToStatus().getId());
                 } catch (NumberFormatException ex) {
                     log.error("####  ActionRepositoryDAOBean Save StatusChangeActionEntity, getToStatus is not number ",ex);
                     return null;
                 }

                 //log.warn("#### ActionRepositoryDAOBean load toStatus id - "+toStatusId);

                 StatusEntity toStatus = null;
                 try {
                     toStatus = (StatusEntity)em.find(StatusEntity.class, toStatusId);
                     if(toStatus==null) {
                         log.error("####  ActionRepositoryDAOBean Save StatusChangeActionEntity, ToStatus not found");
                         return null;
                     }
                 } catch (Exception ex) {
                     log.error("####  ActionRepositoryDAOBean Save StatusChangeActionEntity, ToStatus not found",ex);
                     return null;
                 }

 /*statusChangeActionEntity*/
                     statusChangeActionEntity.setActionEntity(actionEntity);
                     statusChangeActionEntity.setFromStatus(fromStatus);
                     statusChangeActionEntity.setToStatus(toStatus);
                 try {
                     em.persist(statusChangeActionEntity);
                     actionEntity.setStatusChangeActionEntity(statusChangeActionEntity);
                     //fromStatus.setFromStatusChangeAction(statusChangeActionEntity);
                     //toStatus.setToStatusChangeAction(statusChangeActionEntity);
                     em.flush();
                 } catch (Exception e) {
                     log.error("####ERROR  ActionRepositoryDAOBean Save StatusChangeActionEntity, insert new StatusChangeAction ", e);
                     return null;
                 }
                 //log.warn("####  ActionRepositoryDAOBean Save StatusChangeActionEntity, insert new StatusChangeAction ");

                 em.flush();
                 StatusChangeActionXml statusChangeActionXml = new StatusChangeActionXml();
                 statusChangeActionXml.fromInterface(statusChangeActionEntity);
                 ret = (T) statusChangeActionXml;

             }

         return ret;
     }


    @Override
     public T getEntity(T mEntity){

        if(mEntity==null) return null;
        //log.warn("####ActionRepositoryDAOBean  getEntity "+mEntity.toString());

        ActionEntity actionEntity =  (ActionEntity) mEntity;
        //log.warn("####ActionRepositoryDAOBean  actionEntity "+actionEntity.toString());

        if( actionEntity.getNoStatusActionEntity()!=null) {
            //log.warn("#### RETURN ActionRepositoryDAOBean  getEntity getNoStatusActionEntity " + actionEntity.getNoStatusActionEntity().toString());
            NoStatusActionXml noStatusActionXml = new NoStatusActionXml();
            noStatusActionXml.fromInterface(actionEntity.getNoStatusActionEntity());
            return (T) noStatusActionXml;

        }
        if( actionEntity.getStatusChangeActionEntity()!=null) {
            //log.warn("#### RETURN ActionRepositoryDAOBean  getEntity getStatusChangeActionEntity " + actionEntity.getStatusChangeActionEntity().toString());
            StatusChangeActionXml statusChangeActionXml = new StatusChangeActionXml();
            statusChangeActionXml.fromInterface(actionEntity.getStatusChangeActionEntity());
            return (T) statusChangeActionXml;
        }
        //log.warn("####ActionRepositoryDAOBean  getEntity NULL");
        return null;
     }



    public Multimap<Status, Action> findAllGroupedByStatus(ru.efive.workflow.designer.interfaces.Process process, int offset, int limit, String orderBy, boolean asc) {

        final ListMultimap<Status, Action> all = Multimaps.newListMultimap(
                new TreeMap<Status, Collection<Action>>((Comparator) getComparatorFactory()
                        .createComparator("name")), new ListSupplier<Action, T>());
        Query query = em.createQuery("select  sc  from   StatusChangeActionEntity sc,ActionEntity a,ProcessEntity p\n" +
                        "where\n" +
                        "a.statusChangeActionEntity.actionId = sc.actionId\n" +
                        "and a.process.processId  = p.processId\n" +
                        "and p.id = :process_id\n"
                        );
        query.setParameter("process_id",process.getId());
        final List<StatusChangeAction> statusChangeActions = (List<StatusChangeAction>) query.getResultList();

        for(StatusChangeAction statusChangeAction:statusChangeActions){
            all.put(statusChangeAction.getFromStatus(),statusChangeAction);
            all.put(statusChangeAction.getToStatus(),statusChangeAction);
        }
        return subMultimap(all, offset, limit);
    }

    @Override
    public Iterable<T> findAllByName(Process process, String name, int offset, int limit, String orderBy, boolean asc) {

        //log.warn(" ####ActionRepositoryDAOBean Iterable findAllByName process id - "+process.getId()+" Entity - "+getEntityName());
        //log.warn(" ####ActionRepositoryDAOBean Query - "+getQry(process,name,orderBy,asc));

        try {
            Query query = em.createQuery(getQry(process,name,orderBy,asc));
            query.setFirstResult(offset);
            if(limit>0) query.setMaxResults(limit);
            List<T> ret  = query.getResultList();
            //log.warn(" ####ActionRepositoryDAOBean Iterable findAllByName List size - "+ret.size());
            return getEntity(ret);
        } catch (Exception e) {
            log.error(" ####ActionRepositoryDAOBean ERROR findAllByName ",e);
        }
        return null;
    }
    //Получение всех status change action, у которых в from_status_list есть указываемый status id (получение всех доступных действий из текущего статуса)
    @Override
    public Iterable<T> findFromStatus(Process process,String id){
        List<T> ret= null;
        Integer entity_id=null;
        if(id!=null){
            try {
                entity_id = Integer.parseInt(id);
            } catch (NumberFormatException e) {
                log.error("####AbstractRepositoryDAO Error findOne ID is not number - "+id,e);
                return null;
            }
            try {
                Query query = em.createQuery(" from StatusChangeActionEntity where fromStatus.id = :status_id");
                query.setParameter("status_id",entity_id);
                ret = (List<T>) query.getResultList();
            } catch (Exception e) {
                log.error(" ####ActionRepositoryDAOBean ERROR findFromStatus ",e);
            }
        }
        return ret;
    }
    //Получение всех local, pre action, post action для status change и no status action (параметр - action id)
    @Override
    public Iterable<Activity> findActivity(Process process,String id){
        List<Activity> ret= null;
        Integer entity_id=null;

        ActivityRepositoryDAOBean activityRepositoryDAOBean = new ActivityRepositoryDAOBean();

        if(id!=null){
            try {
                entity_id = Integer.parseInt(id);
            } catch (NumberFormatException e) {
                log.error("####AbstractRepositoryDAO Error findOne ID is not number - "+id,e);
                return null;
            }
            ActionEntity actionEntity = (ActionEntity) em.find(getClassEntity(),entity_id);
            if(actionEntity!=null){
                for(ActivityEntity a:actionEntity.getPreActivities()) {
                    if(ret==null) ret = new ArrayList<Activity>();
                    ret.add(activityRepositoryDAOBean.getEntity(a));
                }
                for(ActivityEntity a:actionEntity.getPostActivities()) {
                    if(ret==null) ret = new ArrayList<Activity>();
                    ret.add(activityRepositoryDAOBean.getEntity(a));
                }
            }
        }
        return ret;
    }

}
