package ru.efive.workflow.designer.hibernate.dao;

import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import org.apache.log4j.Logger;
import ru.efive.workflow.designer.hibernate.entity.*;
import ru.efive.workflow.designer.interfaces.*;
import ru.efive.workflow.designer.interfaces.Process;
import ru.efive.workflow.designer.model.*;
import ru.efive.workflow.designer.repository.ActivityRepositoryDAO;

import javax.ejb.Local;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.*;

/**
 * Created by Dmitry Parshin
 * Date: 12.04.12
 */
@Singleton(name = "ActivityRepositoryDAOBean")
//@Stateless
@Local(ActivityRepositoryDAO.class)
public class ActivityRepositoryDAOBean<T extends Activity> extends AbstractNamedRepositoryDAO<T> implements ActivityRepositoryDAO<T> {
    protected static Logger log = Logger.getLogger(ActivityRepositoryDAOBean.class);
    @PersistenceContext(unitName = "e5wd")
    private EntityManager em;

    public ActivityRepositoryDAOBean() {
    }
    public ActivityRepositoryDAOBean(EntityManager em) {
        this.em = em;
    }

    @Override
    public String getEntityName() {
         return "ActivityEntity";
    }

    @Override
    public String getProcessPath() {
        return "process.";
    }

    @Override
    public EntityManager getEntityManager() {
        return em;
    }
    private void setEntityManager(EntityManager em){
        this.em = em;
    }

    @Override
    public Class getClassEntity() {
        return ActivityEntity.class;
    }


    /**
     * Saves a given entity. Use the returned instance for further operations as the save operation might have changed
     * the
     * entity instance completely.
     *
     * @param entity
     * @return the saved entity
     */
    @Override
    public T save(T entity) {
        //log.warn("####-1 ActivityRepositoryDAOBean save  "+entity.toString());

        T ret=null;
        Integer entityId=null;

        ActivityEntity activityEntity = null;
        SetPropertyActivityEntity setPropertyActivityEntity = null;
        SendMailActivityEntity sendMailActivityEntity = null;
        InvokeMethodActivityEntity invokeMethodActivityEntity = null;
        ParametrizedPropertyLocalActivityEntity parametrizedPropertyLocalActivityEntity = null;
        RemoteTransactionActivityEntity remoteTransactionActivityEntity = null;
        InvokeScriptActivityEntity invokeScriptActivityEntity = null;

        ProcessEntity processEntity =  null;


        if(entity==null) {
            log.error("#### ERROR ActivityRepositoryDAOBean Save entity is null  ");
            return null;
        }

        if(entity.getId()!=null){
            try {
                entityId = Integer.parseInt(entity.getId());
            } catch (NumberFormatException e) {
                log.error("####  ActivityRepositoryDAOBean Save, ID is not number - "+entity.getId());
                return null;
            }
            if(entityId.equals(new Integer(0))){
                entityId=null;
            }
        }


        try {
/*get Process*/
            if(entity.getProcess()!=null && entity.getProcess().getId()!=null){
                Integer processId=null;
                try {
                    processId = Integer.parseInt(entity.getProcess().getId());
                } catch (NumberFormatException e) {
                    log.error("####  ActivityRepositoryDAOBean Save, Process ID is not number - "+entity.getProcess().getId());
                    return null;
                }

                try {
                    processEntity = (ProcessEntity) em.find(ProcessEntity.class, processId);
                    if(processEntity==null)  {
                        log.error("####  ActivityRepositoryDAOBean Save, Process ID not found - " + entity.getProcess().getId());
                        return null;
                    }
                } catch (Exception e) {
                    log.error("####  ActivityRepositoryDAOBean Save, Process ID not found - " + entity.getProcess().getId());
                    return null;
                }
            } else{
                log.error("####  ActivityRepositoryDAOBean Save, Process is NULL  )");
                return null;
            }




/*save ActivityEntity*/
            //log.warn("####  ActivityRepositoryDAOBean Save ActivityEntity ");

            if(entityId!=null){
                try {
                    activityEntity = (ActivityEntity) em.find(ActivityEntity.class,entityId);
                    if(activityEntity==null){
                        log.error("#### ERROR ActivityRepositoryDAOBean Save, update ActivityEntity ID not found ");
                        return null;
                    }
                } catch (Exception e) {
                    log.error("#### ERROR ActivityRepositoryDAOBean Save, update ActivityEntity ID not found ");
                    return null;
                }
            }else{
                activityEntity = new ActivityEntity();
                //log.warn("####  ActivityRepositoryDAOBean Save, insert new ActivityEntity ");
            }


            activityEntity.fromInterface(entity);
            activityEntity.setProcess(processEntity);
            em.persist(activityEntity);
            em.flush();

            processEntity.getActivityEntities().add(activityEntity);

            ActivityXml activityXml = new ActivityXml();
            activityXml.fromInterface(activityEntity);

            ret = (T) activityXml;
            //log.warn("####  ActivityRepositoryDAOBean save, return  ActivityEntity"+activityEntity.toString());
            //log.warn("####  ActivityRepositoryDAOBean save, return  ActivityXml"+activityXml.toString());

            entityId = activityEntity.getBlankActivityId();

/*save SetPropertyActivity*/
            if(entity instanceof SetPropertyActivity){ //log.warn("####  ActivityRepositoryDAOBean Save SetPropertyActivity ");

                try {
                    setPropertyActivityEntity = (SetPropertyActivityEntity)em.find(SetPropertyActivityEntity.class, entityId);
                    if(setPropertyActivityEntity==null) setPropertyActivityEntity = new SetPropertyActivityEntity();
                } catch (Exception e) {
                    setPropertyActivityEntity = new SetPropertyActivityEntity();
                }

                setPropertyActivityEntity.setActivityEntity(activityEntity);
                try {
                    em.persist(setPropertyActivityEntity);
                    activityEntity.setSetPropertyActivityEntity(setPropertyActivityEntity);
                    em.flush();
                } catch (Exception e1) {
                    log.error("####ERROR  ActivityRepositoryDAOBean Save, insert new SetPropertyActivityEntity ", e1);
                    return null;
                }


                setPropertyActivityEntity.getPropertyChanges().clear();
                if(((SetPropertyActivity)entity).getPropertyChanges()!=null){
                    for(PropertyChangeDescriptor p:((SetPropertyActivity)entity).getPropertyChanges()){
                        PropertyChangeDescriptorEntity propertyChangeDescriptorEntity = new PropertyChangeDescriptorEntity();
                        propertyChangeDescriptorEntity.fromInterface(p);
                        try {
                            em.persist(propertyChangeDescriptorEntity);
                            setPropertyActivityEntity.getPropertyChanges().add(propertyChangeDescriptorEntity);
                            em.flush();
                        } catch (Exception e) {
                            log.error("####ERROR  ActivityRepositoryDAOBean Save, insert new setPropertyChanges ", e);
                        }
                    }
                }

                SetPropertyActivityXml setPropertyActivityXml = new SetPropertyActivityXml();
                setPropertyActivityXml.fromInterface(setPropertyActivityEntity);

                ret = (T) setPropertyActivityXml;
                //log.warn("####  ActivityRepositoryDAOBean save, return  SetPropertyActivityEntity"+setPropertyActivityEntity.toString());
                //log.warn("####  ActivityRepositoryDAOBean save, return  SetPropertyActivityXml"+setPropertyActivityXml.toString());

            }
/*save SendMailActivity*/
            if(entity instanceof SendMailActivity){ //log.warn("####  ActivityRepositoryDAOBean Save SendMailActivity ");

                try {
                    sendMailActivityEntity = (SendMailActivityEntity)em.find(SendMailActivityEntity.class,entityId);
                    if(sendMailActivityEntity==null)  sendMailActivityEntity = new SendMailActivityEntity();
                } catch (Exception e) {
                    sendMailActivityEntity = new SendMailActivityEntity();
                }

                sendMailActivityEntity.fromInterface((SendMailActivity)entity);
                sendMailActivityEntity.setActivityEntity(activityEntity);
                try {
                    em.persist(sendMailActivityEntity);
                    activityEntity.setSendMailActivityEntity(sendMailActivityEntity);
                    em.flush();
                } catch (Exception e1) {
                    log.error("####ERROR  ActivityRepositoryDAOBean Save, insert new SendMailActivityEntity ", e1);
                    return null;
                }
                //log.warn("####  ActivityRepositoryDAOBean Save, insert new SendMailActivityEntity ");


                SendMailActivityXml sendMailActivityXml = new SendMailActivityXml();
                sendMailActivityXml.fromInterface(sendMailActivityEntity);

                ret = (T) sendMailActivityXml;
                //log.warn("####  ActivityRepositoryDAOBean save, return  SendMailActivityEntity"+sendMailActivityEntity.toString());
                //log.warn("####  ActivityRepositoryDAOBean save, return  SendMailActivityXml"+sendMailActivityXml.toString());
            }
/*save InvokeMethodActivity*/
            if(entity instanceof InvokeMethodActivity){ //log.warn("####  ActivityRepositoryDAOBean Save InvokeMethodActivity");

                try {
                    invokeMethodActivityEntity = (InvokeMethodActivityEntity) em.find(InvokeMethodActivityEntity.class,entityId);
                    if(invokeMethodActivityEntity==null) invokeMethodActivityEntity = new InvokeMethodActivityEntity();
                } catch (Exception e) {
                    invokeMethodActivityEntity = new InvokeMethodActivityEntity();
                }

                invokeMethodActivityEntity.fromInterface((InvokeMethodActivity)entity);
                invokeMethodActivityEntity.setSetPropertyActivityEntity(setPropertyActivityEntity);
                try {
                    em.persist(invokeMethodActivityEntity);
                    setPropertyActivityEntity.setInvokeMethodActivityEntity(invokeMethodActivityEntity);
                    em.flush();
                } catch (Exception e1) {
                    log.error("####ERROR  ActivityRepositoryDAOBean Save, insert new InvokeMethodActivityEntity ", e1);
                    return null;
                }
                //log.warn("####  ActivityRepositoryDAOBean Save, insert new InvokeMethodActivityEntity ");


                InvokeMethodActivityXml invokeMethodActivityXml = new InvokeMethodActivityXml();
                invokeMethodActivityXml.fromInterface(invokeMethodActivityEntity);

                ret = (T) invokeMethodActivityXml;
                //log.warn("####  ActivityRepositoryDAOBean save, return  InvokeMethodActivityEntity"+invokeMethodActivityEntity.toString());
                //log.warn("####  ActivityRepositoryDAOBean save, return  InvokeMethodActivityXml"+invokeMethodActivityXml.toString());
            }
/*save ParametrizedPropertyLocalActivity*/
            if(entity instanceof ParametrizedPropertyLocalActivity){ //log.warn("####  ActivityRepositoryDAOBean Save ParametrizedPropertyLocalActivity ");

                try {
                    parametrizedPropertyLocalActivityEntity = (ParametrizedPropertyLocalActivityEntity) em.find(ParametrizedPropertyLocalActivityEntity.class, entityId);
                    if(parametrizedPropertyLocalActivityEntity==null) parametrizedPropertyLocalActivityEntity = new ParametrizedPropertyLocalActivityEntity();
                } catch (Exception e) {
                    parametrizedPropertyLocalActivityEntity = new ParametrizedPropertyLocalActivityEntity();
                }

                parametrizedPropertyLocalActivityEntity.setActivityEntity(activityEntity);
                try {
                    em.persist(parametrizedPropertyLocalActivityEntity);
                    activityEntity.setParametrizedPropertyLocalActivityEntity(parametrizedPropertyLocalActivityEntity);
                    em.flush();
                } catch (Exception e1) {
                    log.error("####ERROR  ActivityRepositoryDAOBean Save, insert new ParametrizedPropertyLocalActivityEntity ",e1);
                    return null;
                }

                parametrizedPropertyLocalActivityEntity.getPropertyChanges().clear();
                if(((ParametrizedPropertyLocalActivity)entity).getPropertyChanges()!=null){
                    for(EditableProperty p:((ParametrizedPropertyLocalActivity)entity).getPropertyChanges()){
                        EditablePropertyEntity editablePropertyEntity = new EditablePropertyEntity();
                        editablePropertyEntity.fromInterface(p);
                        try {
                            em.persist(editablePropertyEntity);
                            parametrizedPropertyLocalActivityEntity.getPropertyChanges().add(editablePropertyEntity);
                            em.flush();
                        } catch (Exception e) {
                            log.error("####ERROR  ActivityRepositoryDAOBean Save, insert new setPropertyChanges ", e);
                        }
                    }
                }

                ParametrizedPropertyLocalActivityXml parametrizedPropertyLocalActivityXml = new ParametrizedPropertyLocalActivityXml();
                log.error("parametrizedPropertyLocalActivityXml.fromInterface(parametrizedPropertyLocalActivityEntity); " + parametrizedPropertyLocalActivityEntity);
                parametrizedPropertyLocalActivityXml.fromInterface(parametrizedPropertyLocalActivityEntity);

                ret = (T) parametrizedPropertyLocalActivityXml;
                //log.warn("####  ActivityRepositoryDAOBean save, return  ParametrizedPropertyLocalActivityEntity"+parametrizedPropertyLocalActivityEntity.toString());
                //log.warn("####  ActivityRepositoryDAOBean save, return  ParametrizedPropertyLocalActivityXml"+parametrizedPropertyLocalActivityXml.toString());
            }
/*save RemoteTransactionActivityEntity*/
            if(entity instanceof RemoteTransactionActivity){ //log.warn("####  ActivityRepositoryDAOBean Save RemoteTransactionActivityEntity ");

                try {
                    remoteTransactionActivityEntity = (RemoteTransactionActivityEntity)em.find(RemoteTransactionActivityEntity.class,entityId);
                    if(remoteTransactionActivityEntity==null)  remoteTransactionActivityEntity = new RemoteTransactionActivityEntity();
                } catch (Exception e) {
                    remoteTransactionActivityEntity = new RemoteTransactionActivityEntity();
                }

                remoteTransactionActivityEntity.fromInterface((RemoteTransactionActivity)entity);
                remoteTransactionActivityEntity.setActivityEntity(activityEntity);
                try {
                    em.persist(remoteTransactionActivityEntity);
                    activityEntity.setRemoteTransactionActivityEntity(remoteTransactionActivityEntity);
                    em.flush();
                } catch (Exception e1) {
                    log.error("####ERROR  ActivityRepositoryDAOBean Save, insert new RemoteTransactionActivityEntity ", e1);
                    return null;
                }
                //log.warn("####  ActivityRepositoryDAOBean Save, insert new RemoteTransactionActivityEntity ");


                RemoteTransactionActivityXml remoteTransactionActivityXml = new RemoteTransactionActivityXml();
                remoteTransactionActivityXml.fromInterface(remoteTransactionActivityEntity);

                ret = (T) remoteTransactionActivityXml;
                //log.warn("####  ActivityRepositoryDAOBean save, return  RemoteTransactionActivityEntity"+remoteTransactionActivityEntity.toString());
                //log.warn("####  ActivityRepositoryDAOBean save, return  RemoteTransactionActivityXml"+remoteTransactionActivityXml.toString());
            }

/*save InvokeScriptActivityEntity*/
            if(entity instanceof InvokeScriptActivity){ //log.warn("####  ActivityRepositoryDAOBean Save InvokeScriptActivity ");

                try {
                    invokeScriptActivityEntity = (InvokeScriptActivityEntity)em.find(InvokeScriptActivityEntity.class,entityId);
                    if(invokeScriptActivityEntity==null)  invokeScriptActivityEntity = new InvokeScriptActivityEntity();
                } catch (Exception e) {
                    invokeScriptActivityEntity = new InvokeScriptActivityEntity();
                }

                invokeScriptActivityEntity.fromInterface((InvokeScriptActivity)entity);
                invokeScriptActivityEntity.setActivityEntity(activityEntity);
                try {
                    em.persist(invokeScriptActivityEntity);
                    activityEntity.setInvokeScriptActivityEntity(invokeScriptActivityEntity);
                    em.flush();
                } catch (Exception e1) {
                    log.error("####ERROR  ActivityRepositoryDAOBean Save, insert new InvokeScriptActivityEntity ", e1);
                    return null;
                }
                //log.warn("####  ActivityRepositoryDAOBean Save, insert new InvokeScriptActivityEntity ");


                InvokeScriptActivityXml invokeScriptActivityXml = new InvokeScriptActivityXml();
                invokeScriptActivityXml.fromInterface(invokeScriptActivityEntity);

                ret = (T) invokeScriptActivityXml;
                //log.warn("####  ActivityRepositoryDAOBean save, return  InvokeScriptActivityEntity"+invokeScriptActivityEntity.toString());
                //log.warn("####  ActivityRepositoryDAOBean save, return  InvokeScriptActivityXml"+invokeScriptActivityXml.toString());
            }


        } catch (Exception e) {
            log.error("#### ERROR Save ActivityRepositoryDAOBean  - " + entity.toString(),e);
            return null;
        }

        em.flush();
        return ret;

    }

    @Override
     public T getEntity(T mEntity){
     T ret=null;

        if(mEntity==null) return null;

        if(((ActivityEntity) mEntity).getParametrizedPropertyLocalActivityEntity()!=null ) {

            ParametrizedPropertyLocalActivityXml parametrizedPropertyLocalActivityXml = new ParametrizedPropertyLocalActivityXml();
            parametrizedPropertyLocalActivityXml.fromInterface(((ActivityEntity) mEntity).getParametrizedPropertyLocalActivityEntity());

            ret = (T) parametrizedPropertyLocalActivityXml;
            //log.warn("####  ActivityRepositoryDAOBean getEntity, return  ParametrizedPropertyLocalActivityEntity"+((ActivityEntity) mEntity).getSetPropertyActivityEntity().getParametrizedPropertyLocalActivityEntity().toString());
            //log.warn("####  ActivityRepositoryDAOBean getEntity, return  ParametrizedPropertyLocalActivityXml"+parametrizedPropertyLocalActivityXml.toString());
            return ret  ;
        }
        if( ((ActivityEntity) mEntity).getSetPropertyActivityEntity() !=null&&
                ((ActivityEntity) mEntity).getSetPropertyActivityEntity().getInvokeMethodActivityEntity()!=null) {

            InvokeMethodActivityXml invokeMethodActivityXml = new InvokeMethodActivityXml();
            invokeMethodActivityXml.fromInterface( ((ActivityEntity) mEntity).getSetPropertyActivityEntity().getInvokeMethodActivityEntity());

            ret = (T) invokeMethodActivityXml;
            //log.warn("####  ActivityRepositoryDAOBean getEntity, return  InvokeMethodActivityEntity"+ ((ActivityEntity) mEntity).getSetPropertyActivityEntity().getInvokeMethodActivityEntity().toString());
            //log.warn("####  ActivityRepositoryDAOBean getEntity, return  InvokeMethodActivityXml"+invokeMethodActivityXml.toString());
                return ret;
        }
        if( ((ActivityEntity) mEntity).getSendMailActivityEntity()!=null) {

            SendMailActivityXml sendMailActivityXml = new SendMailActivityXml();
            sendMailActivityXml.fromInterface(((ActivityEntity) mEntity).getSendMailActivityEntity());

            ret = (T) sendMailActivityXml;
            //log.warn("####  ActivityRepositoryDAOBean save, getEntity  SendMailActivityEntity"+((ActivityEntity) mEntity).getSendMailActivityEntity().toString());
            //log.warn("####  ActivityRepositoryDAOBean save, getEntity  SendMailActivityXml"+sendMailActivityXml.toString());
                return ret ;
        }
        if( ((ActivityEntity) mEntity).getSetPropertyActivityEntity()!=null) {

            SetPropertyActivityXml setPropertyActivityXml = new SetPropertyActivityXml();
            setPropertyActivityXml.fromInterface(((ActivityEntity) mEntity).getSetPropertyActivityEntity());

            ret = (T) setPropertyActivityXml;
            //log.warn("####  ActivityRepositoryDAOBean save, getEntity  SetPropertyActivityEntity"+((ActivityEntity) mEntity).getSetPropertyActivityEntity().toString());
            //log.warn("####  ActivityRepositoryDAOBean save, getEntity  SetPropertyActivityXml"+setPropertyActivityXml.toString());
                return ret ;
        }

        if( ((ActivityEntity) mEntity).getRemoteTransactionActivityEntity()!=null) {

            RemoteTransactionActivityXml remoteTransactionActivityXml = new RemoteTransactionActivityXml();
            remoteTransactionActivityXml.fromInterface(((ActivityEntity) mEntity).getRemoteTransactionActivityEntity());

            ret = (T) remoteTransactionActivityXml;
            //log.warn("####  ActivityRepositoryDAOBean save, getEntity  RemoteTransactionActivityEntity"+((ActivityEntity) mEntity).getRemoteTransactionActivityEntity().toString());
            //log.warn("####  ActivityRepositoryDAOBean save, getEntity  RemoteTransactionActivityXml"+remoteTransactionActivityXml.toString());
            return ret ;
        }

        if( ((ActivityEntity) mEntity).getInvokeScriptActivityEntity()!=null) {

            InvokeScriptActivityXml invokeScriptActivityXml = new InvokeScriptActivityXml();
            invokeScriptActivityXml.fromInterface(((ActivityEntity) mEntity).getInvokeScriptActivityEntity());

            ret = (T) invokeScriptActivityXml;
            //log.warn("####  ActivityRepositoryDAOBean save, getEntity  invokeScriptActivityEntity"+((ActivityEntity) mEntity).getInvokeScriptActivityEntity().toString());
            //log.warn("####  ActivityRepositoryDAOBean save, getEntity  invokeScriptActivityXml"+invokeScriptActivityXml.toString());
            return ret ;
        }

        ActivityXml activityXml = new ActivityXml();
        activityXml.fromInterface(mEntity);

        ret = (T) activityXml;
        //log.warn("####  ActivityRepositoryDAOBean getEntity, return  ActivityEntity"+mEntity.toString());
        //log.warn("####  ActivityRepositoryDAOBean getEntity, return  ActivityXml"+activityXml.toString());

        return ret;
     }

    @Override
    public Multimap<Status,Activity> findAllGroupedByStatus(ru.efive.workflow.designer.interfaces.Process process, int offset, int limit, String orderBy, boolean asc) {

        final ListMultimap<Status, Activity> all = Multimaps.newListMultimap(
                new TreeMap<Status, Collection<Activity>>((Comparator) getComparatorFactory()
                        .createComparator("name")), new ListSupplier<Activity, T>());

        Query query = em.createQuery(
                "select  s  from   StatusEntity s,ProcessEntity p\n" +
                        "where \n" +
                        "s.process.processId = p.processId \n" +
                        "and p.id = :process_id"
        );
        query.setParameter("process_id",process.getId());
        final List<Status> statuses = (List<Status>) query.getResultList();

        for(Status status:statuses){
            all.putAll(status,status.getPreActivities());
            all.putAll(status,status.getPostActivities());
        }
        return subMultimap(all, offset, limit);
    }

    @Override
    public Multimap<Action,Activity> findAllGroupedByAction(Process process, int offset, int limit, String orderBy, boolean asc) {
        final ListMultimap<Action, Activity> all = Multimaps.newListMultimap(
                new TreeMap<Action, Collection<Activity>>((Comparator) getComparatorFactory()
                        .createComparator("name")), new ListSupplier<Activity, T>());

        Query query = em.createQuery(
                "select  a  from   ActionEntity a,ProcessEntity p\n" +
                        "where \n" +
                        "a.process.processId  = p.processId\n" +
                        "and p.id = :process_id"
        );
        query.setParameter("process_id",process.getId());
        final List<Action> actions = (List<Action>) query.getResultList();

        for(Action action:actions){
            all.putAll(action,action.getPreActivities());
            all.putAll(action,action.getPostActivities());
        }
        return subMultimap(all, offset, limit);
    }

    //Получение всех local, pre action, post action для status change и no status action (параметр - action id)
    @Override
    public Iterable<Activity> findActivity(Process process,String id){
        List<Activity> ret= null;
        Integer entity_id=null;

        ActionRepositoryDAOBean actionRepositoryDAOBean = new ActionRepositoryDAOBean();

        if(id!=null){
            try {
                entity_id = Integer.parseInt(id);
            } catch (NumberFormatException e) {
                log.error("####AbstractRepositoryDAO Error findOne ID is not number - "+id,e);
                return null;
            }
            ActionXml actionXml = (ActionXml) actionRepositoryDAOBean.findOne(id);

            if(actionXml!=null){
                for(ActivityXml a:actionXml.getPreActivities()) {
                    if(ret==null) ret = new ArrayList<Activity>();
                    ret.add(a);
                }
                for(ActivityXml a:actionXml.getPostActivities()) {
                    if(ret==null) ret = new ArrayList<Activity>();
                    ret.add(a);
                }
            }

        }
        return ret;
    }
}
