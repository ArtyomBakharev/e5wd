package ru.efive.workflow.designer.hibernate.dao;

import java.util.List;

import com.google.common.base.Supplier;
import com.google.common.collect.Lists;

/**
 * TODO: Document type
 *
 * @author Sergey Zaytsev
 *
 */
public final class ListSupplier<E, T> implements Supplier<List<E>> {
	@Override
	public List<E> get() {
		return Lists.newArrayList();
	}
}