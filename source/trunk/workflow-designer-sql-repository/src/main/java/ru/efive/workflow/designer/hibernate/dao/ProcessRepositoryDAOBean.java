package ru.efive.workflow.designer.hibernate.dao;

import org.apache.log4j.Logger;
import ru.efive.workflow.designer.hibernate.entity.AccessControlEntryEntity;
import ru.efive.workflow.designer.hibernate.entity.ProcessEntity;
import ru.efive.workflow.designer.interfaces.AccessControlEntry;
import ru.efive.workflow.designer.interfaces.Process;
import ru.efive.workflow.designer.model.ProcessXml;
import ru.efive.workflow.designer.repository.ProcessRepositoryDAO;

import javax.ejb.Local;
import javax.ejb.Singleton;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by Parshin Dmitriy
 * Date: 26.04.12
 */
@Singleton(name = "ProcessRepositoryDAOBean")
@Stateless
@Local(ProcessRepositoryDAO.class)
public class ProcessRepositoryDAOBean<T extends Process> extends AbstractNamedRepositoryDAO<T> implements ProcessRepositoryDAO<T> {
    protected static Logger log = Logger.getLogger(ProcessRepositoryDAOBean.class);

    @PersistenceContext(unitName = "e5wd")
    private EntityManager em;
/*
    @Resource
    private UserTransaction utx;
*/
    public ProcessRepositoryDAOBean() {
    }
    public ProcessRepositoryDAOBean(EntityManager em) {
        this.em = em;
    }


    @Override
    public String getEntityName() {
        return "ProcessEntity";
    }
    @Override
    public String getProcessPath(){
        return "";
    }

    @Override
    public EntityManager getEntityManager() {
        return this.em;
    }
    private void setEntityManager(EntityManager em){
        this.em = em;
    }



    @Override
    public Class getClassEntity() {
        return ProcessEntity.class;
    }


    /**
     * Saves a given entity. Use the returned instance for further operations as the save operation might have changed
     * the
     * entity instance completely.
     *
     * @param entity
     * @return the saved entity
     */
    @Override
    public T save(T entity) {
        log.warn("####-1 ProcessRepositoryDAOBean Save - " + entity.toString());

        T ret=null;
        Integer entityId=null;
        ProcessEntity processEntity =null;

        if(entity==null) {
            log.error("####2 ERROR ProcessRepositoryDAOBean Save entity is null ");
            return null;
        }

        if(entity.getId()!=null){
            try {
                entityId = Integer.parseInt(entity.getId());
            } catch (NumberFormatException e) {
                log.error("####3  ProcessRepositoryDAOBean Save, ID is not number - "+entity.getId());
                return null;
            }
            if(entityId.equals(new Integer(0))) entityId=null;
        }

        try {

            if(entity instanceof ProcessXml) {

                if(entityId!=null){
                    try {
                        processEntity = (ProcessEntity) em.find(ProcessEntity.class,entityId);
                        if(processEntity==null){
                            log.error("####  ProcessRepositoryDAOBean Save, id Process not found");
                            return null;
                        }
                    } catch (Exception e) {
                        log.error("####  ProcessRepositoryDAOBean Save, id Process not found");
                        return null;
                    }
                } else{
                    processEntity = new ProcessEntity();
                }

                processEntity.fromInterface(entity);
                em.persist(processEntity);
                //log.warn("####  ProcessRepositoryDAOBean Save, insert new Process"+processEntity.toString());



                //log.warn("####  ProcessRepositoryDAOBean AccessControlEntry Save - "+processEntity.toString());
                processEntity.getAcl().clear();
                if(entity.getAcl()!=null){
                    for(AccessControlEntry a: entity.getAcl()){
                        AccessControlEntryEntity accessControlEntryEntity = new AccessControlEntryEntity();
                        accessControlEntryEntity.fromInterface(a);
                        em.persist(accessControlEntryEntity);
                        processEntity.getAcl().add(accessControlEntryEntity);
                        em.flush();
                    }
                }

                //log.warn("####6  ProcessRepositoryDAOBean Save - "+processEntity.toString());
                ret = (T) processEntity;
            }
        } catch (Exception e) {
            log.error("#### ERROR ProcessRepositoryDAOBean Save ",e);
            return null;
        }

        //log.warn("#### ProcessRepositoryDAOBean Save, return - " + processEntity.toString());
        return getEntity(ret);
    }

    @Override
    public T getEntity(T mEntity) {
            //log.warn("####-1 ProcessRepositoryDAOBean  getEntity");
        if(mEntity==null) return null;

            ProcessXml processXml = new ProcessXml();
            processXml.fromInterface(mEntity);
            return (T)processXml;
    }

}
