package ru.efive.workflow.designer.hibernate.dao;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import ru.efive.workflow.designer.hibernate.entity.AccessControlEntryEntity;
import ru.efive.workflow.designer.hibernate.entity.ActivityEntity;
import ru.efive.workflow.designer.hibernate.entity.ProcessEntity;
import ru.efive.workflow.designer.hibernate.entity.StatusEntity;
import ru.efive.workflow.designer.interfaces.AccessControlEntry;
import ru.efive.workflow.designer.interfaces.Activity;
import ru.efive.workflow.designer.interfaces.Status;
import ru.efive.workflow.designer.model.ActivityXml;
import ru.efive.workflow.designer.model.ProcessXml;
import ru.efive.workflow.designer.model.StatusXml;
import ru.efive.workflow.designer.repository.StatusRepositoryDAO;
import ru.efive.workflow.designer.interfaces.Process;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Singleton;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

/**
 * Created by Dmitry Parshin
 * Date: 12.04.12
 *
 */
@Singleton(name = "StatusRepositoryDAOBean")
//@Stateless
@Local(StatusRepositoryDAO.class)
public class StatusRepositoryDAOBean<T extends Status> extends AbstractNamedRepositoryDAO<T> implements StatusRepositoryDAO<T> {
    @PersistenceContext(unitName = "e5wd")
    private EntityManager em;


    protected static Logger log = Logger.getLogger(StatusRepositoryDAOBean.class);

    public StatusRepositoryDAOBean() {
        //BasicConfigurator.configure();
    }
    public StatusRepositoryDAOBean(EntityManager em) {
        //BasicConfigurator.configure();
        this.em = em;
    }


    @Override
    public String getEntityName() {
        return "StatusEntity";
    }

    @Override
    public String getProcessPath(){
        return "process.";
    }

    @Override
    public EntityManager getEntityManager() {
        return em;
    }
    private void setEntityManager(EntityManager em){
        this.em = em;
    }

    @Override
    public Class getClassEntity() {
        return StatusEntity.class;
    }


    /**
     * Saves a given entity. Use the returned instance for further operations as the save operation might have changed
     * the
     * entity instance completely.
     *
     * @param entity
     * @return the saved entity
     */
    @Override
    public T save(T entity) {
        //log.warn("####-1 StatusRepositoryDAOBean Save - "+entity.toString());
        T ret=null;
        Integer entityId=null;
        StatusEntity statusEntity=null;
        ProcessEntity processEntity =null;

        if(entity==null) {
            log.error("#### ERROR StatusRepositoryDAOBean Save entity is null)");
            return null;
        }

        if(entity.getId()!=null){
            try {
                entityId = Integer.parseInt(entity.getId());
            } catch (NumberFormatException e) {
                log.error("####  StatusRepositoryDAOBean Save, ID is not number - "+entity.getId());
                return null;
            }
            if(entityId.equals(new Integer(0))){
                entityId=null;
            }
        }



            if(entity.getProcess()!=null && entity.getProcess().getId()!=null){
                Integer processId=null;
                try {
                    processId = Integer.parseInt(entity.getProcess().getId());
                } catch (NumberFormatException e) {
                    log.error("####  StatusRepositoryDAOBean Save, Process ID is not number - "+entity.getProcess().getId());
                    return null;
                }

                try {
                    processEntity = (ProcessEntity) em.find(ProcessEntity.class, processId);
                    if(processEntity==null)  {
                        log.error("####  StatusRepositoryDAOBean Save, Process ID not found - " + entity.getProcess().getId());
                        return null;
                    }
                } catch (Exception e) {
                    log.error("####  StatusRepositoryDAOBean Save, Process ID not found - " + entity.getProcess().getId());
                    return null;
                }
            } else{
                log.error("####  StatusRepositoryDAOBean Save, Process is NULL  )");
                return null;
            }

        //log.warn("####  StatusRepositoryDAOBean Save StatusEntity, Process - "+processEntity.toString());

        if(entityId!=null){
            try {
                statusEntity = (StatusEntity) em.find(StatusEntity.class,entityId);
                if(statusEntity==null) {
                    log.error("####  StatusRepositoryDAOBean , not found ID = "+entityId);
                    return null;
                }
            } catch (Exception e) {
                log.error("####  StatusRepositoryDAOBean , not found ID = "+entityId);
                return null;
            }
        }else{
            statusEntity = new StatusEntity();
        }

        statusEntity.fromInterface(entity);
        statusEntity.setProcess(processEntity);
        try {
            em.persist(statusEntity);
            em.flush();
            processEntity.getStatusEntities().add(statusEntity);
        } catch (Exception e1) {
            log.error("####  StatusRepositoryDAOBean Save, ERROR insert new Status )", e1);
            return null;
        }


/*getAcl*/  //log.warn("####  StatusRepositoryDAOBean Save Acl ");
            statusEntity.getAcl().clear();
            if(entity.getAcl()!=null){
                for(AccessControlEntry a: entity.getAcl()){
                    AccessControlEntryEntity accessControlEntryEntity = new AccessControlEntryEntity();
                    accessControlEntryEntity.fromInterface(a);
                    try {
                        em.persist(accessControlEntryEntity);
                        statusEntity.getAcl().add(accessControlEntryEntity);
                        em.flush();
                    } catch (Exception e) {
                        log.error("#### ERROR StatusRepositoryDAOBean Save Acl ",e);
                    }
                }
            }

/*getLocalActivities*/  //log.warn("####  StatusRepositoryDAOBean Save LocalActivities  ");
            statusEntity.getLocalActivities().clear();
            if(entity.getLocalActivities()!=null){
                for(Activity a:entity.getLocalActivities()){
                    ActivityEntity activityEntity=null;
                    Integer activitiId=null;
                    if(a.getId()!=null){
                        try {
                            activitiId = Integer.parseInt(a.getId());
                        } catch (NumberFormatException e) {
                            log.error("####  StatusRepositoryDAOBean Save, getLocalActivities, Activity ID is not number - "+a.toString());
                            continue;
                        }
                        if(activitiId.equals(new Integer(0))) activitiId=null;
                    }
                    if(activitiId!=null)
                        try {
                            activityEntity = (ActivityEntity) em.find(ActivityEntity.class,activitiId);
                            if(activityEntity !=null)  statusEntity.getLocalActivities().add(activityEntity);
                        } catch (Exception e) {
                            log.error("#### StatusRepositoryDAOBean Save LocalActivities  ",e);
                        }
                }
            }

/*getPreActivities*/ //log.warn("####  StatusRepositoryDAOBean Save PreActivities  ");
            statusEntity.getPreActivities().clear();
            if(entity.getPreActivities()!=null){
                for(Activity a:entity.getPreActivities()){
                    ActivityEntity activityEntity=null;
                    Integer activitiId=null;
                    if(a.getId()!=null){
                        try {
                            activitiId = Integer.parseInt(a.getId());
                        } catch (NumberFormatException e) {
                            log.error("####  StatusRepositoryDAOBean Save, getPreActivities, Activity ID is not number - "+a.toString());
                            continue;
                        }
                        if(activitiId.equals(new Integer(0))) activitiId=null;
                    }
                    if(activitiId!=null)
                        try {
                            activityEntity = (ActivityEntity)em.find(ActivityEntity.class, activitiId);
                            if(activityEntity !=null) statusEntity.getPreActivities().add(activityEntity);
                        } catch (Exception e) {
                            log.error("#### StatusRepositoryDAOBean Save PreActivities  ",e);
                        }
                }
            }

/*getPostActivities*/   //log.warn("####  StatusRepositoryDAOBean Save PostActivities )");
            statusEntity.getPostActivities().clear();
            if(entity.getPostActivities()!=null){
                for(Activity a:entity.getPostActivities()){
                    ActivityEntity activityEntity=null;
                    Integer activitiId=null;
                    if(a.getId()!=null){
                        try {
                            activitiId = Integer.parseInt(a.getId());
                        } catch (NumberFormatException e) {
                            log.error("####  StatusRepositoryDAOBean Save, getPostActivities, Activity ID is not number - "+a.toString());
                            continue;
                        }
                        if(activitiId.equals(new Integer(0))) activitiId=null;
                    }
                    if(activitiId!=null)
                        try {
                            activityEntity = (ActivityEntity) em.find(ActivityEntity.class,activitiId);
                            if(activityEntity!=null) statusEntity.getPostActivities().add(activityEntity);
                        } catch (Exception e) {
                            log.error("#### StatusRepositoryDAOBean Save PostActivities  ",e);
                        }

                }
            }
                //log.warn("####  StatusRepositoryDAOBean Save, return - "+statusEntity.toString());

        em.flush();

        return getEntity((T)statusEntity);
    }

    @Override
    public T getEntity(T mEntity) {
        //log.warn("####-1 StatusRepositoryDAOBean  getEntity");
        if(mEntity==null) return null;

        StatusXml statusXml = new StatusXml();
        statusXml.fromInterface(mEntity);
        return  (T) statusXml;
    }

   //Получение всех local, pre status, post status activities для объекта status (параметр - status id)
   @Override
   public Iterable<Activity> findActivity(Process process,String id){
       List<Activity> ret= null;
       Integer entity_id=null;
       ActivityRepositoryDAOBean activityRepositoryDAOBean = new ActivityRepositoryDAOBean();
       if(id!=null){
           //log.warn("#### StatusRepositoryDAOBean findActivity id - "+id);
           try {
               entity_id = Integer.parseInt(id);
           } catch (NumberFormatException e) {
               log.error("####AbstractRepositoryDAO Error findOne ID is not number - "+id,e);
               return null;
           }
           StatusEntity statusEntity = (StatusEntity) em.find(getClassEntity(),entity_id);
           if(statusEntity!=null){
               for(ActivityEntity a:statusEntity.getLocalActivities()) {
                   if(ret==null) ret = new ArrayList<Activity>();
                   ret.add(activityRepositoryDAOBean.getEntity(a));
               }
               for(ActivityEntity a:statusEntity.getPreActivities()) {
                   if(ret==null) ret = new ArrayList<Activity>();
                   ret.add(activityRepositoryDAOBean.getEntity(a));
               }
               for(ActivityEntity a:statusEntity.getPostActivities()) {
                   if(ret==null) ret = new ArrayList<Activity>();
                   ret.add(activityRepositoryDAOBean.getEntity(a));
               }
           }
       }
       return ret;
   }

}
