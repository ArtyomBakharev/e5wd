package ru.efive.workflow.designer.hibernate.entity;


import org.hibernate.annotations.Cascade;
import ru.efive.workflow.designer.interfaces.AccessControlEntry;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Dmitry Parshin
 */
@javax.persistence.Table(name = "access_entry", schema = "", catalog = "e5wd")
@Entity
public class AccessControlEntryEntity implements Serializable, AccessControlEntry {
    private int id;
    private int permission;
    private String sid;
    private ProcessEntity processEntity;
    private StatusEntity statusEntity;


    @javax.persistence.Column(
            name = "access_entry_id",
            nullable = false,
            insertable = false,
            updatable = false,
            length = 19,
            precision = 0)
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    @javax.persistence.Column(
            name = "level",
            nullable = true,
            insertable = true,
            updatable = true,
            length = 19,
            precision = 0)
    @Basic
    public int getPermission() {
        return permission;
    }

    public void setPermission(int permission) {
        this.permission = permission;
    }

    @javax.persistence.Column(
            name = "accessor",
            nullable = true,
            insertable = true,
            updatable = true,
            columnDefinition="TEXT",
            length =  65535,
            precision = 0)
    @Basic
    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }


    @ManyToOne(cascade =CascadeType.REFRESH,fetch = FetchType.LAZY)
    @JoinTable(name = "process_access_list",
            joinColumns = @JoinColumn(name = "access_entry_id",referencedColumnName = "access_entry_id"),
            inverseJoinColumns = @JoinColumn(name = "process_id",referencedColumnName = "process_id")
    )
    public ProcessEntity getProcessEntity() {
        return processEntity;
    }
    public void setProcessEntity(ProcessEntity processEntity) {
        this.processEntity = processEntity;
    }


    @ManyToOne(cascade =CascadeType.REFRESH,fetch = FetchType.LAZY)
    @JoinTable(name = "status_access_list",
            joinColumns = @JoinColumn(name = "access_entry_id",referencedColumnName = "access_entry_id"),
            inverseJoinColumns = @JoinColumn(name = "status_id",referencedColumnName = "status_id")
    )
    public StatusEntity getStatusEntity() {
        return statusEntity;
    }

    public void setStatusEntity(StatusEntity statusEntity) {
        this.statusEntity = statusEntity;
    }

    @Transient
    @Override
    public boolean hasPermission(int permission) {
        return (this.permission & permission) != 0;
    }
    @Transient
    @Override
    public void grantPermission(int permission) {
        this.permission |= permission;
    }
    @Transient
    @Override
    public void revokePermission(int permission) {
        this.permission &= ~permission;
    }


    @Transient
    public void fromInterface(AccessControlEntry accessControlEntry){
        setPermission(accessControlEntry.getPermission());
        setSid(accessControlEntry.getSid());
    }

    @Override
    public String toString() {
        return "AccessControlEntryEntity ["
                +"getId()=" + getId()
                +", getSid()=" + getSid()
                +", getPermission()="+ getPermission()
                +"]";
    }
}
