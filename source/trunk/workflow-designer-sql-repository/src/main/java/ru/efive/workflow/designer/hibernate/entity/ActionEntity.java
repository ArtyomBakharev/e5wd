package ru.efive.workflow.designer.hibernate.entity;

import org.hibernate.annotations.Cascade;
import ru.efive.workflow.designer.interfaces.*;
import ru.efive.workflow.designer.interfaces.Process;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dmitry Parshin
 */
@javax.persistence.Table(name = "action", schema = "", catalog = "e5wd")
@Entity
public class ActionEntity  implements Action {
    private int actionId;
    private String name;
    private String description;
    private String dataType;
    private String availabilityCondition;
    private String evaluationMessage;
    private boolean historyEnabled;
    private boolean autocommitEnabled;
    private String metadata;
    private ProcessEntity process;
    private List<ActivityEntity> preActivities;
    private List<ActivityEntity> postActivities;
    private List<ActivityEntity> localActivities;

    private StatusChangeActionEntity statusChangeActionEntity;
    private NoStatusActionEntity noStatusActionEntity;


    @javax.persistence.Column(
            name = "action_id",
            nullable = false,
            insertable = false,
            updatable = false,
            length = 19,
            precision = 0)
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getActionId() {
        return actionId;
    }

    public void setActionId(int actionId) {
        this.actionId = actionId;
    }

    @javax.persistence.Column(
            name = "name",
            nullable = true,
            insertable = true,
            updatable = true,
            columnDefinition="TEXT",
            length =  65535,
            precision = 0)
    @Basic
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @javax.persistence.Column(
            name = "commentary",
            nullable = true,
            insertable = true,
            updatable = true,
            columnDefinition="TEXT",
            length =  65535,
            precision = 0)
    @Basic
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    @javax.persistence.Column(
            name = "data_type",
            nullable = true,
            insertable = true,
            updatable = true,
            columnDefinition="TEXT",
            length =  65535,
            precision = 0)
    @Basic
    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    @javax.persistence.Column(
            name = "availability_condition",
            nullable = true,
            insertable = true,
            updatable = true,
            columnDefinition="TEXT",
            length =  65535,
            precision = 0)
    @Basic
    public String getAvailabilityCondition() {
        return availabilityCondition;
    }

    public void setAvailabilityCondition(String availabilityCondition) {
        this.availabilityCondition = availabilityCondition;
    }


    @javax.persistence.Column(
            name = "evaluation_message",
            nullable = true,
            insertable = true,
            updatable = true,
            columnDefinition="TEXT",
            length =  65535,
            precision = 0)
    @Basic
    public String getEvaluationMessage() {
        return evaluationMessage;
    }

    public void setEvaluationMessage(String evaluationMessage) {
        this.evaluationMessage = evaluationMessage;
    }


    @javax.persistence.Column(
            name = "is_history_enabled",
            nullable = true,
            insertable = true,
            updatable = true,
            length = 0,
            precision = 0)
    @Basic
    public boolean getHistoryEnabled() {
        return historyEnabled;
    }

    public void setHistoryEnabled(boolean historyEnabled) {
        this.historyEnabled = historyEnabled;
    }


    @javax.persistence.Column(
            name = "is_autocommit_enabled",
            nullable = true,
            insertable = true,
            updatable = true,
            length = 0,
            precision = 0)
    @Basic
    public boolean getAutocommitEnabled() {
        return autocommitEnabled;
    }

    public void setAutocommitEnabled(boolean autocommitEnabled) {
        this.autocommitEnabled = autocommitEnabled;
    }


    @javax.persistence.Column(
            name = "metadata",
            nullable = true,
            insertable = true,
            updatable = true,
            columnDefinition="TEXT",
            length =  65535,
            precision = 0)
    @Basic
    public String getMetadata() {
        return metadata;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }


    @ManyToMany(cascade =CascadeType.REFRESH,fetch = FetchType.LAZY)
    @JoinTable(name = "pre_action_activities",
            joinColumns = @JoinColumn(name = "action_id"),
            inverseJoinColumns = @JoinColumn(name = "blank_activity_id")
    )
    public List<ActivityEntity> getPreActivities() {
        if(preActivities==null) preActivities = new ArrayList<ActivityEntity>();
        return preActivities;
    }

    public void setPreActivities(List<? extends Activity> preActivities) {
        this.preActivities = (List<ActivityEntity>)preActivities;
    }

    @ManyToMany(cascade =CascadeType.REFRESH,fetch = FetchType.LAZY)
    @JoinTable(name = "post_action_activities",
            joinColumns = @JoinColumn(name = "action_id"),
            inverseJoinColumns = @JoinColumn(name = "blank_activity_id")
    )
    public List<ActivityEntity> getPostActivities() {
        if(postActivities==null) postActivities = new ArrayList<ActivityEntity>();
        return postActivities;
    }

    public void setPostActivities(List<? extends Activity> postActivities) {
        this.postActivities = (List<ActivityEntity>)postActivities;
    }

    @ManyToMany(cascade =CascadeType.REFRESH,fetch = FetchType.LAZY)
    @JoinTable(name = "local_action_activities",
            joinColumns = @JoinColumn(name = "action_id"),
            inverseJoinColumns = @JoinColumn(name = "blank_activity_id")
    )
    public List<ActivityEntity> getLocalActivities() {
        if(localActivities==null) localActivities = new ArrayList<ActivityEntity>();
        return localActivities;
    }

    public void setLocalActivities(List<? extends Activity> localActivities) {
        this.localActivities = (List<ActivityEntity>)localActivities;
    }


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "process_id")
    public ProcessEntity getProcess() {
        return process;
    }

    public void setProcess(Process process) {
        this.process = (ProcessEntity) process;
    }

    @OneToOne(mappedBy="actionEntity",fetch = FetchType.LAZY)
    @Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
    public StatusChangeActionEntity getStatusChangeActionEntity() {
        return statusChangeActionEntity;
    }

    public void setStatusChangeActionEntity(StatusChangeActionEntity statusChangeActionEntity) {
        this.statusChangeActionEntity = statusChangeActionEntity;
    }

    @OneToOne (mappedBy="actionEntity",fetch = FetchType.LAZY)
    @Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
    public NoStatusActionEntity getNoStatusActionEntity() {
        return noStatusActionEntity;
    }

    public void setNoStatusActionEntity(NoStatusActionEntity noStatusActionEntity) {
        this.noStatusActionEntity = noStatusActionEntity;
    }



    @Transient
    @Override
    public String getId() {
        return Integer.toString(actionId);
    }
    @Override
    public void setId(String id) {}

    @Transient
    @Override
    public boolean isHistoryEnabled() {
        return historyEnabled;
    }

    @Transient
    @Override
    public boolean isAutocommitEnabled() {
        return autocommitEnabled;
    }

    @Transient
    public void fromInterface(Action action){
        setName(action.getName());
        setDescription(action.getDescription());
        setDataType(action.getDataType());
        setAvailabilityCondition(action.getAvailabilityCondition());
        setEvaluationMessage(action.getEvaluationMessage());
        setAutocommitEnabled(action.isAutocommitEnabled());
        setHistoryEnabled(action.isHistoryEnabled());
        setMetadata(action.getMetadata());
    }

    @Override
    public String toString() {
        String ret = new String();

        ret = "ActionEntity ["
                +"getId()=" + getId()
                +", getName()=" + getName()
                +", getDescription()="+ getDescription()
                +", getDataType()="+ getDataType()
                +", getMetadata()="+ getMetadata()
                +", getAvailabilityCondition()="+ getAvailabilityCondition()
                +", getEvaluationMessage()="+ getEvaluationMessage()
                +", isHistoryEnabled()="+ isHistoryEnabled()
                +", getMetadata()="+ getMetadata()
        ;
        if(getProcess()!=null){
            ret+=", { "+getProcess()+"}";
        }
        if(getPreActivities()!=null){
            ret+=",{ getPreActivities() = ";
            for(ActivityEntity a: getPreActivities()){
                a.toString();
            }
            ret+="}";
        }
        if(getPostActivities()!=null){
            ret+=",{ getPostActivities() = ";
            for(ActivityEntity a: getPostActivities()){
                a.toString();
            }
            ret+="}";
        }
        ret+="]";
        return ret;
    }

}
