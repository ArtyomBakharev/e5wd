package ru.efive.workflow.designer.hibernate.entity;

import org.hibernate.annotations.Cascade;
import ru.efive.workflow.designer.interfaces.Activity;
import ru.efive.workflow.designer.interfaces.Process;
import ru.efive.workflow.designer.interfaces.ReturnType;

import javax.persistence.*;

/**
 * Created by Dmitry Parshin
 */
@javax.persistence.Table(name = "blank_activity", schema = "", catalog = "e5wd")
@Entity
public class ActivityEntity  implements Activity {
    /*Activity*/
    private int blankActivityId;
    private String name;
    private String runCondition;
    private ReturnType returnType;
    private String returnFormula;
    private String description;
    private String metadata;
    private ProcessEntity process;

    private SendMailActivityEntity sendMailActivityEntity;
    private SetPropertyActivityEntity setPropertyActivityEntity;
    private ParametrizedPropertyLocalActivityEntity parametrizedPropertyLocalActivityEntity;
    private RemoteTransactionActivityEntity remoteTransactionActivityEntity;
    private InvokeScriptActivityEntity invokeScriptActivityEntity;


    @javax.persistence.Column(
            name = "blank_activity_id",
            nullable = false,
            insertable = false,
            updatable = false,
            length = 19,
            precision = 0)
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getBlankActivityId() {
        return blankActivityId;
    }

    public void setBlankActivityId(int blankActivityId) {
        this.blankActivityId = blankActivityId;
    }

    @Transient
    @Override
    public String getId(){
        return Integer.toString(blankActivityId);
    }

    @Transient
    @Override
    public void setId(String id){

    }


    @javax.persistence.Column(
            name = "name",
            nullable = true,
            insertable = true,
            updatable = true,
            columnDefinition="TEXT",
            length =  65535,
            precision = 0)
    @Basic
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @javax.persistence.Column(
            name = "run_condition",
            nullable = true,
            insertable = true,
            updatable = true,
            columnDefinition="TEXT",
            length =  65535,
            precision = 0)
    @Basic
    public String getRunCondition() {
        return runCondition;
    }

    public void setRunCondition(String runCondition) {
        this.runCondition = runCondition;
    }


    @javax.persistence.Column(
            name = "return_type",
            nullable = true,
            insertable = true,
            updatable = true,
            length = 10,
            precision = 0)
    @Basic
    public ReturnType getReturnType() {
        return returnType;
    }

    public void setReturnType(ReturnType returnType) {
        this.returnType = returnType;
    }


    @javax.persistence.Column(
            name = "return_formula",
            nullable = true,
            insertable = true,
            updatable = true,
            columnDefinition="TEXT",
            length =  65535,
            precision = 0)
    @Basic
    public String getReturnFormula() {
        return returnFormula;
    }

    public void setReturnFormula(String returnFormula) {
        this.returnFormula = returnFormula;
    }


    @javax.persistence.Column(
            name = "commentary",
            nullable = true,
            insertable = true,
            updatable = true,
            columnDefinition="TEXT",
            length =  65535,
            precision = 0)
    @Basic
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    @javax.persistence.Column(
            name = "metadata",
            nullable = true,
            insertable = true,
            updatable = true,
            columnDefinition="TEXT",
            length =  65535,
            precision = 0)
    @Basic
    public String getMetadata() {
        return metadata;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "process_id")
    public ProcessEntity getProcess() {
        return process;
    }

    public void setProcess(Process process) {
        this.process = (ProcessEntity) process;
    }


    @OneToOne (mappedBy="activityEntity",cascade =CascadeType.ALL,fetch = FetchType.LAZY)
    @Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
    public SendMailActivityEntity getSendMailActivityEntity() {
        return sendMailActivityEntity;
    }

    public void setSendMailActivityEntity(SendMailActivityEntity sendMailActivityEntity) {
        this.sendMailActivityEntity = sendMailActivityEntity;
    }

    @OneToOne (mappedBy="activityEntity",cascade =CascadeType.ALL,fetch = FetchType.LAZY)
    @Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
    public SetPropertyActivityEntity getSetPropertyActivityEntity() {
        return setPropertyActivityEntity;
    }

    public void setSetPropertyActivityEntity(SetPropertyActivityEntity setPropertyActivityEntity) {
        this.setPropertyActivityEntity = setPropertyActivityEntity;
    }

    @OneToOne (mappedBy="activityEntity",cascade =CascadeType.ALL,fetch = FetchType.LAZY)
    @Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
    public ParametrizedPropertyLocalActivityEntity getParametrizedPropertyLocalActivityEntity() {
        return parametrizedPropertyLocalActivityEntity;
    }

    public void setParametrizedPropertyLocalActivityEntity(ParametrizedPropertyLocalActivityEntity parametrizedPropertyLocalActivityEntity) {
        this.parametrizedPropertyLocalActivityEntity = parametrizedPropertyLocalActivityEntity;
    }


    @OneToOne (mappedBy="activityEntity",cascade =CascadeType.ALL,fetch = FetchType.LAZY)
    @Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
    public RemoteTransactionActivityEntity getRemoteTransactionActivityEntity() {
        return remoteTransactionActivityEntity;
    }

    public void setRemoteTransactionActivityEntity(RemoteTransactionActivityEntity remoteTransactionActivityEntity) {
        this.remoteTransactionActivityEntity = remoteTransactionActivityEntity;
    }


    @OneToOne (mappedBy="activityEntity",cascade =CascadeType.ALL,fetch = FetchType.LAZY)
    @Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
    public InvokeScriptActivityEntity getInvokeScriptActivityEntity() {
        return invokeScriptActivityEntity;
    }

    public void setInvokeScriptActivityEntity(InvokeScriptActivityEntity invokeScriptActivityEntity) {
        this.invokeScriptActivityEntity = invokeScriptActivityEntity;
    }

    @Transient
    public void fromInterface(Activity activity){
        setName(activity.getName());
        setRunCondition(activity.getRunCondition());
        setReturnType(activity.getReturnType());
        setReturnFormula(activity.getReturnFormula());
        setDescription(activity.getDescription());
        setMetadata(activity.getMetadata());

    }

    @Override
    public String toString() {
        String ret = new String();

        ret = "ActivityEntity ["
                +"getId()=" + getId()
                +", getName()=" + getName()
                +", getRunCondition()="+ getRunCondition()
                +", getReturnType()="+ getReturnType()
                +", getReturnFormula()="+ getReturnFormula()
                +", getDescription()="+ getDescription()
                +", getMetadata()="+ getMetadata()
        ;
        if(getProcess()!=null){
            ret+=", { "+getProcess()+"}";
        }
        ret+="]";
        return ret;
    }
}
