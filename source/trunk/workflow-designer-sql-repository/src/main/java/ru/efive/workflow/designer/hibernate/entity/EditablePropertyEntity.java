package ru.efive.workflow.designer.hibernate.entity;

import ru.efive.workflow.designer.interfaces.EditableProperty;

import javax.persistence.*;

/**
 * User: work
 * Date: 24.10.12
 */
@javax.persistence.Table(name = "editable_property", schema = "", catalog = "e5wd")
@Entity
public class EditablePropertyEntity implements EditableProperty {
    private int id;
    private int scope;
    private String name;
    private String value;
    private ParametrizedPropertyLocalActivityEntity parametrizedPropertyLocalActivityEntity;

    @javax.persistence.Column(
            name = "editable_property_id",
            nullable = false,
            insertable = false,
            updatable = false,
            length = 19,
            precision = 0)
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }



    @javax.persistence.Column(
            name = "scope",
            nullable = true,
            insertable = true,
            updatable = true,
            length = 19,
            precision = 0)
    @Basic
    public int getScope() {
        return scope;
    }
    public void setScope(int scope) {
        this.scope = scope;
    }

    @javax.persistence.Column(
            name = "name",
            nullable = true,
            insertable = true,
            updatable = true,
            columnDefinition="TEXT",
            length =  65535,
            precision = 0)
    @Basic
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @javax.persistence.Column(
            name = "value",
            nullable = true,
            insertable = true,
            updatable = true,
            columnDefinition="TEXT",
            length =  65535,
            precision = 0)
    @Basic
    public String getValue() {
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }

    @Transient
    public void fromInterface(EditableProperty editableProperty){
        setScope(editableProperty.getScope());
        setName(editableProperty.getName());
        setValue(editableProperty.getValue());
    }

    @ManyToOne(cascade =CascadeType.REFRESH,fetch = FetchType.LAZY)
    @JoinTable(name = "editable_property_list",
            joinColumns = @JoinColumn(name = "editable_property_id",referencedColumnName = "editable_property_id"),
            inverseJoinColumns = @JoinColumn(name = "blank_activity_id",referencedColumnName = "blank_activity_id")
    )
    public ParametrizedPropertyLocalActivityEntity getParametrizedPropertyLocalActivityEntity() {
        return parametrizedPropertyLocalActivityEntity;
    }

    public void setParametrizedPropertyLocalActivityEntity(ParametrizedPropertyLocalActivityEntity parametrizedPropertyLocalActivityEntity) {
        this.parametrizedPropertyLocalActivityEntity = parametrizedPropertyLocalActivityEntity;
    }
}
