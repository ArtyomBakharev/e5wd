package ru.efive.workflow.designer.hibernate.entity;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.GenericGenerator;
import ru.efive.workflow.designer.interfaces.*;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Dmitry Parshin
 */
@javax.persistence.Table(name = "invoke_method_activity", schema = "", catalog = "e5wd")
@Entity
public class InvokeMethodActivityEntity  implements InvokeMethodActivity {
    private int blankActivityId;
    private String className;
    private String methodName;
    private SetPropertyActivityEntity setPropertyActivityEntity;


    @GenericGenerator(name = "generator", strategy = "foreign",
            parameters = @org.hibernate.annotations.Parameter(name = "property", value = "setPropertyActivityEntity")
    )
    @javax.persistence.Column(
            name = "blank_activity_id", 
            nullable = false, 
            insertable = true, 
            updatable = true, 
            length = 10, 
            precision = 0)
    @Id
    @GeneratedValue(generator = "generator")
    public int getBlankActivityId() {
        return blankActivityId;
    }

    public void setBlankActivityId(int blankActivityId) {
        this.blankActivityId = blankActivityId;
    }


    @javax.persistence.Column(
            name = "class_name", 
            nullable = true, 
            insertable = true, 
            updatable = true,
            columnDefinition="TEXT",
            length =  65535,
            precision = 0)
    @Basic
    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }


    @javax.persistence.Column(
            name = "method_name", 
            nullable = true, 
            insertable = true, 
            updatable = true,
            columnDefinition="TEXT",
            length =  65535,
            precision = 0)
    @Basic
    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }


    @OneToOne
    @PrimaryKeyJoinColumn
    public SetPropertyActivityEntity getSetPropertyActivityEntity() {
        if(setPropertyActivityEntity==null) setPropertyActivityEntity= new SetPropertyActivityEntity();
        return setPropertyActivityEntity;
    }

    public void setSetPropertyActivityEntity(SetPropertyActivityEntity setPropertyActivityEntity) {
        this.setPropertyActivityEntity = setPropertyActivityEntity;
    }

    @Transient
    @Override
    public String getId(){
        return setPropertyActivityEntity.getActivityEntity().getId();
    }
    @Override
    public void setId(String id){}
    @Transient
    @Override
    public String getMetadata() {
        return setPropertyActivityEntity.getActivityEntity().getMetadata();
    }
    @Override
    public void setMetadata(String metadata) {}
    @Transient
    @Override
    public String getName() {
        return setPropertyActivityEntity.getActivityEntity().getName();
    }
    @Override
    public void setName(String name) {}
    @Transient
    @Override
    public String getDescription() {
        return setPropertyActivityEntity.getActivityEntity().getDescription();
    }
    @Override
    public void setDescription(String description) {}
    @Transient
    @Override
    public ru.efive.workflow.designer.interfaces.Process getProcess() {
        return setPropertyActivityEntity.getActivityEntity().getProcess();
    }
    @Override
    public void setProcess(ru.efive.workflow.designer.interfaces.Process process) {}
    @Transient
    @Override
    public String getRunCondition() {
        return setPropertyActivityEntity.getActivityEntity().getRunCondition();
    }
    @Override
    public void setRunCondition(String runCondition) {}
    @Transient
    @Override
    public ReturnType getReturnType() {
        return setPropertyActivityEntity.getActivityEntity().getReturnType();
    }
    @Override
    public void setReturnType(ReturnType returnType) {}
    @Transient
    @Override
    public String getReturnFormula() {
        return setPropertyActivityEntity.getActivityEntity().getReturnFormula();
    }
    @Override
    public void setReturnFormula(String returnFormula) {}

    @Transient
    @Override
    public List<? extends PropertyChangeDescriptor> getPropertyChanges() {
        return setPropertyActivityEntity.getPropertyChanges();
    }
    @Override
    public void setPropertyChanges(List<? extends PropertyChangeDescriptor> propertyChanges) {
    }

    @Transient
    public void fromInterface(InvokeMethodActivity invokeMethodActivity){
        setClassName(invokeMethodActivity.getClassName());
        setMethodName(invokeMethodActivity.getMethodName());
    }
@Override
public String toString() {
    String ret = new String();

    ret = "InvokeMethodActivityEntity ["
            +"getId()=" + getId()
            +", getClassName()=" +getClassName()
            +", getMethodName()="+ getMethodName()
    ;
    if(getSetPropertyActivityEntity()!=null){
        ret+=", { "+getSetPropertyActivityEntity()+"}";
    }
    ret += "]";
    return ret;
}
}
