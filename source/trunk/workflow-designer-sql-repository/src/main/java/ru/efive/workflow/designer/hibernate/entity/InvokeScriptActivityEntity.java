package ru.efive.workflow.designer.hibernate.entity;

import org.hibernate.annotations.GenericGenerator;
import ru.efive.workflow.designer.interfaces.*;
import ru.efive.workflow.designer.interfaces.Process;

import javax.persistence.*;

/**
 * Created Dmitry Parshin
 * Date: 18.05.12
 */
@javax.persistence.Table(name = "invoke_script_activity", schema = "", catalog = "e5wd")
@Entity
public class InvokeScriptActivityEntity implements InvokeScriptActivity {
    private int blankActivityId;
    private ActivityEntity activityEntity;
    private String script;

    @GenericGenerator(name = "generator", strategy = "foreign",
            parameters = @org.hibernate.annotations.Parameter(name = "property", value = "activityEntity")
    )

    @javax.persistence.Column(
            name = "blank_activity_id",
            nullable = false,
            insertable = true,
            updatable = true,
            length = 10,
            precision = 0)
    @Id
    @GeneratedValue(generator = "generator")
    public int getBlankActivityId() {
        return blankActivityId;
    }

    public void setBlankActivityId(int blankActivityId) {
        this.blankActivityId = blankActivityId;
    }

    @OneToOne
    @PrimaryKeyJoinColumn
    public ActivityEntity getActivityEntity() {
        if (activityEntity == null) activityEntity= new  ActivityEntity();
        return activityEntity;
    }

    public void setActivityEntity(ActivityEntity activityEntity) {
        this.activityEntity = activityEntity;
    }

    @javax.persistence.Column(
            name = "script",
            nullable = true,
            insertable = true,
            updatable = true,
            columnDefinition="TEXT",
            length =  65535,
            precision = 0)
    @Basic
    public String getScript() {
        return script;
    }
    public void setScript(String script) {
        this.script = script;
    }


    @Transient
    @Override
    public String getRunCondition() {
        return activityEntity.getRunCondition();
    }
    @Override
    public void setRunCondition(String runCondition) {
    }

    @Transient
    @Override
    public ReturnType getReturnType() {
        return activityEntity.getReturnType();
    }
    @Override
    public void setReturnType(ReturnType returnType) {
    }

    @Transient
    @Override
    public String getReturnFormula() {
        return activityEntity.getReturnFormula();
    }
    @Override
    public void setReturnFormula(String returnFormula) {
    }

    @Transient
    @Override
    public Process getProcess() {
        return activityEntity.getProcess();
    }
    @Override
    public void setProcess(Process process) {
    }

    @Override
    public String getMetadata() {
        return activityEntity.getMetadata();
    }
    @Override
    public void setMetadata(String metadata) {
    }

    @Transient
    @Override
    public String getName() {
        return activityEntity.getName();
    }
    @Override
    public void setName(String name) {
    }

    @Transient
    @Override
    public String getDescription() {
        return activityEntity.getDescription();
    }
    @Override
    public void setDescription(String description) {
    }

    @Transient
    @Override
    public String getId() {
        return activityEntity.getId();
    }
    @Override
    public void setId(String id) {
    }

    @Transient
    public void fromInterface(InvokeScriptActivity invokeScriptActivity){
        setScript(invokeScriptActivity.getScript());
    }

    @Override
    public String toString() {
        String ret = new String();

        ret = "InvokeScriptActivityEntity ["
                +"getId()=" + getId()
                +", getName()=" + getName()
                +", getScript()="+ getScript()
                +"]";
        return ret;
    }


}
