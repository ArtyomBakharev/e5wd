package ru.efive.workflow.designer.hibernate.entity;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.GenericGenerator;
import ru.efive.workflow.designer.interfaces.AccessControlEntry;
import ru.efive.workflow.designer.interfaces.EditableProperty;
import ru.efive.workflow.designer.interfaces.ParametrizedPropertyLocalActivity;
import ru.efive.workflow.designer.interfaces.ReturnType;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dmitry Parshin
 */
@javax.persistence.Table(name = "parametrized_property_local_activity", schema = "", catalog = "e5wd")
@Entity
public class ParametrizedPropertyLocalActivityEntity implements ParametrizedPropertyLocalActivity {
    private int blankActivityId;
    private ActivityEntity activityEntity;
    private String form;
    private List<EditablePropertyEntity> propertyChanges;

    @GenericGenerator(name = "generator", strategy = "foreign",
            parameters = @org.hibernate.annotations.Parameter(name = "property", value = "activityEntity")
    )

    @javax.persistence.Column(
            name = "blank_activity_id",
            nullable = false,
            insertable = true,
            updatable = true,
            length = 10,
            precision = 0)
    @Id
    @GeneratedValue(generator = "generator")
    public int getBlankActivityId() {
        return blankActivityId;
    }

    public void setBlankActivityId(int blankActivityId) {
        this.blankActivityId = blankActivityId;
    }

    @javax.persistence.Column(
            name = "form",
            nullable = true,
            insertable = true,
            updatable = true,
            columnDefinition="TEXT",
            length =  65535,
            precision = 0)
    @Basic
    public String getForm() {
        return form;
    }

    public void setForm(String form) {
        this.form = form;
    }


    @OneToOne
    @PrimaryKeyJoinColumn
    public ActivityEntity getActivityEntity() {
        if (activityEntity == null) activityEntity= new  ActivityEntity();
        return activityEntity;
    }

    public void setActivityEntity(ActivityEntity activityEntity) {
        this.activityEntity = activityEntity;
    }
    /*
        @CollectionOfElements
        @JoinTable(name="set_property_change",
                joinColumns = @JoinColumn(name="blankActivity_id"),
                inverseJoinColumns = @JoinColumn(name = "set_property_change_id"))
    */


    @OneToMany(cascade =CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinTable(name = "editable_property_list",
            joinColumns = @JoinColumn(name = "blank_activity_id"),
            inverseJoinColumns = @JoinColumn(name = "editable_property_id")
    )
    @Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
    public List<EditablePropertyEntity> getPropertyChanges() {
        if(propertyChanges==null) propertyChanges = new ArrayList<EditablePropertyEntity>();
        return propertyChanges;
    }

    public void setPropertyChanges(List<? extends EditableProperty> propertyChanges) {
        this.propertyChanges = (List<EditablePropertyEntity>) propertyChanges;
    }



/*ActivityEntity*/

    @Transient
    @Override
    public String getId(){
        return activityEntity.getId();
    }
    @Override
    public void setId(String id){}
    @Transient
    @Override
    public String getMetadata() {
        return activityEntity.getMetadata();
    }
    @Override
    public void setMetadata(String metadata) {}
    @Transient
    @Override
    public String getName() {
        return activityEntity.getName();
    }
    @Override
    public void setName(String name) {}
    @Transient
    @Override
    public String getDescription() {
        return activityEntity.getDescription();
    }
    @Override
    public void setDescription(String description) {}
    @Transient
    @Override
    public ru.efive.workflow.designer.interfaces.Process getProcess() {
        return activityEntity.getProcess();
    }
    @Override
    public void setProcess(ru.efive.workflow.designer.interfaces.Process process) {}
    @Transient
    @Override
    public String getRunCondition() {
        return activityEntity.getRunCondition();
    }
    @Override
    public void setRunCondition(String runCondition) {}
    @Transient
    @Override
    public ReturnType getReturnType() {
        return activityEntity.getReturnType();
    }
    @Override
    public void setReturnType(ReturnType returnType) {}
    @Transient
    @Override
    public String getReturnFormula() {
        return activityEntity.getReturnFormula();
    }
    @Override
    public void setReturnFormula(String returnFormula) {}

    /*End ActivityEntity*/
    @Transient
    public void fromInterface(ParametrizedPropertyLocalActivity i){
    }


    @Override
    public String toString() {
        return "ParametrizedPropertyLocalActivityEntity{" +
                "blankActivityId=" + blankActivityId +
                ", activityEntity=" + activityEntity +
                ", form='" + form + '\'' +
                ", propertyChanges=" + propertyChanges +
                '}';
    }


}
