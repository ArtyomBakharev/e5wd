package ru.efive.workflow.designer.hibernate.entity;

import org.hibernate.annotations.Cascade;
import ru.efive.workflow.designer.interfaces.AccessControlEntry;
import ru.efive.workflow.designer.interfaces.Process;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dmitry Parshin
 */
@javax.persistence.Table(name = "process", schema = "", catalog = "e5wd")
@Entity
public class ProcessEntity  implements Process {
    private int processId;
    private String name;
    private String description;
    private String metadata;
    private int repository;
    private String dataType;
    private String initialStatusId;
    private List<AccessControlEntryEntity> acl=new ArrayList<AccessControlEntryEntity>();
    private List<StatusEntity> statusEntities;
    private List<ActivityEntity> activityEntities;
    private List<ActionEntity> actionEntity;

    public ProcessEntity() {}

    public ProcessEntity(String name, String description, String metadata) {
        this.name = name;
        this.description = description;
        this.metadata = metadata;
    }

    @javax.persistence.Column(
            name = "process_id",
            nullable = false,
            insertable = false,
            updatable = false,
            length = 19,
            precision = 0)
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getProcessId() {
        return processId;
    }

    public void setProcessId(int processId) {
        this.processId = processId;
    }
    @Transient
    @Override
    public String getId(){
       return Integer.toString(processId);
    }
    @Transient
    @Override
    public void setId(String id){
        this.processId =  Integer.parseInt(id);
    }

    @javax.persistence.Column(
            name = "name",
            nullable = true,
            insertable = true,
            updatable = true,
            columnDefinition="TEXT",
            length =  65535,
            precision = 0)
    @Basic
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @javax.persistence.Column(
            name = "repository",
            nullable = true,
            insertable = true,
            updatable = true,
            length = 19,
            precision = 0)
    @Basic
    public int getRepository() {
        return repository;
    }

    public void setRepository(int repository) {
        this.repository = repository;
    }

    @javax.persistence.Column(
            name = "data_type",
            nullable = true,
            insertable = true,
            updatable = true,
            columnDefinition="TEXT",
            length =  65535,
            precision = 0)
    @Basic
    public String getDataType() {
        return dataType;
    }
    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    @javax.persistence.Column(
            name = "initial_status_id",
            nullable = true,
            insertable = true,
            updatable = true,
            columnDefinition="TEXT",
            length =  65535,
            precision = 0)
    @Basic
    public String getInitialStatusId() {
        return initialStatusId;
    }


    public void setInitialStatusId(String statusId) {
        initialStatusId = statusId;
    }

    @javax.persistence.Column(
            name = "commentary",
            nullable = true,
            insertable = true,
            updatable = true,
            columnDefinition="TEXT",
            length =  65535,
            precision = 0)
    @Basic
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    @javax.persistence.Column(
            name = "metadata",
            nullable = true,
            insertable = true,
            updatable = true,
            columnDefinition="TEXT",
            length =  65535,
            precision = 0)
    @Basic
    public String getMetadata() {
        return metadata;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }
    @OneToMany(cascade =CascadeType.ALL,fetch = FetchType.EAGER)
    @JoinTable(name = "process_access_list",
            joinColumns = @JoinColumn(name = "process_id"),
            inverseJoinColumns = @JoinColumn(name = "access_entry_id")
    )
    @Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
     public List<AccessControlEntryEntity> getAcl() {
        if(acl==null) acl= new ArrayList<AccessControlEntryEntity>();
        return acl;
    }
    public void setAcl(List<? extends AccessControlEntry> acl) {
        this.acl = (List<AccessControlEntryEntity>) acl;
    }


    @OneToMany(mappedBy="process"/*cascade =CascadeType.ALL*/,fetch = FetchType.LAZY)
    //@JoinColumn(name = "process_id")
    @Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
    public List<StatusEntity> getStatusEntities() {
        if(statusEntities==null) statusEntities = new ArrayList<StatusEntity>();
        return statusEntities;
    }

    public void setStatusEntities(List<StatusEntity> statusEntities) {
        this.statusEntities = statusEntities;
    }

    @OneToMany(mappedBy="process"/*cascade =CascadeType.ALL*/,fetch = FetchType.LAZY)
    //@JoinColumn(name = "process_id")
    @Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
    public List<ActivityEntity> getActivityEntities() {
        if(activityEntities == null) activityEntities = new ArrayList<ActivityEntity>();
        return activityEntities;
    }

    public void setActivityEntities(List<ActivityEntity> activityEntities) {
        this.activityEntities = activityEntities;
    }

    @OneToMany(mappedBy="process"/*,cascade =CascadeType.PERSIST*/,fetch = FetchType.LAZY)
//    @JoinColumn(name = "process_id")
    @Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
    public List<ActionEntity> getActionEntity() {
        if(actionEntity==null) actionEntity = new ArrayList<ActionEntity>();
        return actionEntity;
    }

    public void setActionEntity(List<ActionEntity> actionEntity) {
        this.actionEntity = actionEntity;
    }


@Transient
public void fromInterface(Process process){
    setName(process.getName());
    setInitialStatusId(process.getInitialStatusId());
    setDescription(process.getDescription());
    setMetadata(process.getMetadata());
    setDataType(process.getDataType());
    if(getProcessId()==0) setRepository(process.getRepository());
}

    @Override
    public String toString() {
        String ret=new String();
        ret= "ProcessEntity ["
                +"getId()=" + getId()
                +", getName()="+ getName()
                +", getDescription()="+ getDescription()
                +", getMetadata()="+ getMetadata()
                +", getRepository()="+ getRepository()
                ;
        if(getAcl()!=null){
            ret+=",{ getAcl() = ";
            for(AccessControlEntry a: getAcl()){
                a.toString();
            }
            ret+="}";
        }
        ret+="]";
        return ret;
    }
    
}
