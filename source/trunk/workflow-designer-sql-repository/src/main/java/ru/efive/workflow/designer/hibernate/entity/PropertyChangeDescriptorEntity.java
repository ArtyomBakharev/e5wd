package ru.efive.workflow.designer.hibernate.entity;


import ru.efive.workflow.designer.interfaces.PropertyChangeDescriptor;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Dmitry Parshin
 */
@javax.persistence.Table(name = "property_change_descriptor", schema = "", catalog = "e5wd")
@Entity
public class PropertyChangeDescriptorEntity  implements PropertyChangeDescriptor {
    private int propertyChangeDescriptorId;
    private String name;
    private String value;
    SetPropertyActivityEntity setPropertyActivityEntity;

    @javax.persistence.Column(
            name = "property_change_descriptor_id",
            nullable = false,
            insertable = true,
            updatable = true,
            length = 10,
            precision = 0)
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getPropertyChangeDescriptorId() {
        return propertyChangeDescriptorId;
    }

    public void setPropertyChangeDescriptorId(int propertyChangeDescriptorId) {
        this.propertyChangeDescriptorId = propertyChangeDescriptorId;
    }


    @javax.persistence.Column(
            name = "name",
            nullable = true,
            insertable = true,
            updatable = true,
            columnDefinition="TEXT",
            length =  65535,
            precision = 0)
    @Basic
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @javax.persistence.Column(
            name = "value",
            nullable = true,
            insertable = true,
            updatable = true,
            columnDefinition="TEXT",
            length =  65535,
            precision = 0)
    @Basic
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


    @ManyToOne(cascade =CascadeType.REFRESH,fetch = FetchType.LAZY)
    @JoinColumn(name = "blank_activity_id")
    public SetPropertyActivityEntity getSetPropertyActivityEntity() {
        return setPropertyActivityEntity;
    }
    public void setSetPropertyActivityEntity(SetPropertyActivityEntity setPropertyActivityEntity) {
        this.setPropertyActivityEntity = setPropertyActivityEntity;
    }



    @Transient
    public void fromInterface(PropertyChangeDescriptor propertyChangeDescriptor){
        setName(propertyChangeDescriptor.getName());
        setValue(propertyChangeDescriptor.getValue());
    }

    @Override
    public String toString() {
    String ret = new String();

    ret = "PropertyChangeDescriptorEntity ["
          +", getName()="+getName()
          +", getValue()="+getValue()
          +"]"
    ;

    return ret;
}

}
