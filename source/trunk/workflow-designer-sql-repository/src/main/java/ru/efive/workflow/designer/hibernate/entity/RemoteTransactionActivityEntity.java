package ru.efive.workflow.designer.hibernate.entity;

import org.hibernate.annotations.GenericGenerator;
import ru.efive.workflow.designer.interfaces.*;
import ru.efive.workflow.designer.interfaces.Process;

import javax.persistence.*;

/**
 * Created by Dmitry Parshin
 */
@javax.persistence.Table(name = "remote_transaction_activity", schema = "", catalog = "e5wd")
@Entity
public class RemoteTransactionActivityEntity implements RemoteTransactionActivity {
    private int blankActivityId;
    private ActivityEntity activityEntity;
    private String actionProcessId;
    private String actionId;
    private String dataType;
    private String dataSearchScript;

    @GenericGenerator(name = "generator", strategy = "foreign",
            parameters = @org.hibernate.annotations.Parameter(name = "property", value = "activityEntity")
    )

    @javax.persistence.Column(
            name = "blank_activity_id",
            nullable = false,
            insertable = true,
            updatable = true,
            length = 10,
            precision = 0)
    @Id
    @GeneratedValue(generator = "generator")
    public int getBlankActivityId() {
        return blankActivityId;
    }

    public void setBlankActivityId(int blankActivityId) {
        this.blankActivityId = blankActivityId;
    }

    @OneToOne
    @PrimaryKeyJoinColumn
    public ActivityEntity getActivityEntity() {
        if (activityEntity == null) activityEntity= new  ActivityEntity();
        return activityEntity;
    }

    public void setActivityEntity(ActivityEntity activityEntity) {
        this.activityEntity = activityEntity;
    }

    @javax.persistence.Column(
            name = "action_process_id",
            nullable = true,
            insertable = true,
            updatable = true,
            columnDefinition="TEXT",
            length =  65535,
            precision = 0)
    @Basic
    public String getActionProcessId() {
        return actionProcessId;
    }
    public void setActionProcessId(String p) {
        this.actionProcessId = p;
    }



    @javax.persistence.Column(
            name = "action_id",
            nullable = true,
            insertable = true,
            updatable = true,
            columnDefinition="TEXT",
            length =  65535,
            precision = 0)
    @Basic
    public String getActionId() {
        return this.actionId;
    }
    public void setActionId(String a) {
        this.actionId = a;
    }




    @javax.persistence.Column(
            name = "data_type",
            nullable = true,
            insertable = true,
            updatable = true,
            columnDefinition="TEXT",
            length =  65535,
            precision = 0)
    @Basic
    public String getDataType() {
        return this.dataType;
    }
    public void setDataType(String s) {
        this.dataType = s;
    }



    @javax.persistence.Column(
            name = "data_search_script",
            nullable = true,
            insertable = true,
            updatable = true,
            columnDefinition="TEXT",
            length =  65535,
            precision = 0)
    @Basic
    public String getDataSearchScript() {
        return this.dataSearchScript;
    }
    public void setDataSearchScript(String s) {
        this.dataSearchScript = s;
    }

    @Transient
    @Override
    public String getRunCondition() {
        return activityEntity.getRunCondition();
    }
    @Override
    public void setRunCondition(String runCondition) {
    }


    @Transient
    @Override
    public ReturnType getReturnType() {
        return activityEntity.getReturnType();
    }
    @Override
    public void setReturnType(ReturnType returnType) {
    }


    @Transient
    @Override
    public String getReturnFormula() {
        return activityEntity.getReturnFormula();
    }
    @Override
    public void setReturnFormula(String returnFormula) {
    }


    @Transient
    @Override
    public Process getProcess() {
        return activityEntity.getProcess();
    }
    @Override
    public void setProcess(Process process) {
    }

    @Transient
    @Override
    public String getMetadata() {
        return activityEntity.getMetadata();
    }
    @Override
    public void setMetadata(String metadata) {
    }

    @Transient
    @Override
    public String getName() {
        return activityEntity.getName();
    }
    @Override
    public void setName(String name) {
    }


    @Transient
    @Override
    public String getDescription() {
        return activityEntity.getDescription();
    }
    @Override
    public void setDescription(String description) {
    }


    @Transient
    @Override
    public String getId() {
        return activityEntity.getId();
    }
    @Override
    public void setId(String id) {
    }


    @Transient
    public void fromInterface(RemoteTransactionActivity remoteTransactionActivity){
        setActionProcessId(remoteTransactionActivity.getActionProcessId());
        setActionId(remoteTransactionActivity.getActionId());
        setDataType(remoteTransactionActivity.getDataType());
        setDataSearchScript(remoteTransactionActivity.getDataSearchScript());
    }

        @Override
    public String toString() {
        String ret = new String();

        ret = "RemoteTransactionActivityEntity ["
                +"getId()=" + getId()
                +", getName()=" + getName()
                +", getActionProcessId()="+ getActionProcessId()
                +", getActionId()="+ getActionId()
                +", getDataType()="+ getDataType()
                +", getDataSearchScript()="+ getDataSearchScript()
                +"]";
        return ret;
    }

}
