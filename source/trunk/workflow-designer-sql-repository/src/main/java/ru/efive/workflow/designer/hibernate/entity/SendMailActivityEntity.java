package ru.efive.workflow.designer.hibernate.entity;

import org.hibernate.annotations.*;
import ru.efive.workflow.designer.interfaces.*;
import ru.efive.workflow.designer.interfaces.Process;

import javax.persistence.*;
import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dmitry Parshin
 */
@javax.persistence.Table(name = "send_mail_activity", schema = "", catalog = "e5wd")
@Entity
public class SendMailActivityEntity  implements SendMailActivity
{
    private int blankActivityId;
    private String subject;
    private String content;
    private String charset;
    private ActivityEntity activityEntity;

    private List<String> recipients;
    private List<String> ccRecipients;
    private List<String> bccRecipients;

    @GenericGenerator(name = "generator", strategy = "foreign",
            parameters = @org.hibernate.annotations.Parameter(name = "property", value = "activityEntity")
    )

    @javax.persistence.Column(
            name = "blank_activity_id",
            nullable = false,
            insertable = true,
            updatable = true,
            length = 10,
            precision = 0)
    @Id
    @GeneratedValue(generator = "generator")
    public int getBlankActivityId() {
        return blankActivityId;
    }

    public void setBlankActivityId(int blankActivityId) {
        this.blankActivityId = blankActivityId;
    }


    @javax.persistence.Column(
            name = "subject",
            nullable = true,
            insertable = true,
            updatable = true,
            columnDefinition="TEXT",
            length =  65535,
            precision = 0)
    @Basic
    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }


    @javax.persistence.Column(
            name = "body_content",
            nullable = true,
            insertable = true,
            updatable = true,
            columnDefinition="TEXT",
            length =  65535,
            precision = 0)
    @Basic
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


    @javax.persistence.Column(
            name = "charset",
            nullable = true,
            insertable = true,
            updatable = true,
            columnDefinition="TEXT",
            length =  65535,
            precision = 0)
    @Basic
    public String getCharset() {
        return charset;
    }

    public void setCharset(String charset) {
        this.charset = charset;
    }

    @OneToOne
    @PrimaryKeyJoinColumn
    public ActivityEntity getActivityEntity() {
        if (activityEntity == null) activityEntity= new  ActivityEntity();
        return activityEntity;
    }

    public void setActivityEntity(ActivityEntity activityEntity) {
        this.activityEntity = activityEntity;
    }

    @Transient
    @Override
    public String getId(){
        return activityEntity.getId();
    }
    @Override
    public void setId(String id){}
    @Transient
    @Override
    public String getMetadata() {
        return activityEntity.getMetadata();
    }
    @Override
    public void setMetadata(String metadata) {}
    @Transient
    @Override
    public String getName() {
        return activityEntity.getName();
    }
    @Override
    public void setName(String name) {}
    @Transient
    @Override
    public String getDescription() {
        return activityEntity.getDescription();
    }
    @Override
    public void setDescription(String description) {}
    @Transient
    @Override
    public ru.efive.workflow.designer.interfaces.Process getProcess() {
        return activityEntity.getProcess();
    }
    @Override
    public void setProcess(Process process) {}
    @Transient
    @Override
    public String getRunCondition() {
        return activityEntity.getRunCondition();
    }
    @Override
    public void setRunCondition(String runCondition) {}
    @Transient
    @Override
    public ReturnType getReturnType() {
        return activityEntity.getReturnType();
    }
    @Override
    public void setReturnType(ReturnType returnType) {}
    @Transient
    @Override
    public String getReturnFormula() {
        return activityEntity.getReturnFormula();
    }
    @Override
    public void setReturnFormula(String returnFormula) {}


    @CollectionOfElements
    @JoinTable(name="recipients",joinColumns = @JoinColumn(name="blankActivity_id"))
    @Column(name="sendto",columnDefinition="TEXT",length =  65535)
    public List<String> getRecipients() {
        if(recipients==null) recipients=new ArrayList<String>();
        return recipients;
    }
    public void setRecipients(List<String> recipients) {
        this.recipients = recipients;
    }


    @CollectionOfElements
    @JoinTable(name="cc_recipients",joinColumns = @JoinColumn(name="blankActivity_id"))
    @Column(name="copyto",columnDefinition="TEXT",length =  65535)
    public List<String> getCcRecipients() {
        if(ccRecipients==null) ccRecipients=new ArrayList<String>();
        return ccRecipients;
    }
    public void setCcRecipients(List<String> ccRecipients) {
        this.ccRecipients = ccRecipients;
    }


    @CollectionOfElements
    @JoinTable(name="bcc_recipients", joinColumns = @JoinColumn(name="blankActivity_id"))
    @Column(name="blindcopyto",columnDefinition="TEXT",length =  65535)
    public List<String> getBccRecipients() {
        if(bccRecipients==null) bccRecipients=new ArrayList<String>();
        return bccRecipients;
    }
    public void setBccRecipients(List<String> bccRecipients) {
        this.bccRecipients = bccRecipients;
    }

    @Transient
    public void fromInterface(SendMailActivity sendMailActivity){

        setSubject(sendMailActivity.getSubject());
        setContent(sendMailActivity.getContent());
        setCharset(sendMailActivity.getCharset());

        getRecipients().clear();
        if (sendMailActivity.getRecipients()!=null){
            for(String s:sendMailActivity.getRecipients()){
                getRecipients().add(s);
            }
        }
        getCcRecipients().clear();
        if(sendMailActivity.getCcRecipients()!=null){
            for(String s:sendMailActivity.getCcRecipients()){
                getCcRecipients().add(s);
            }
        }
        getBccRecipients().clear();
        if(sendMailActivity.getBccRecipients()!=null){
            for(String s:sendMailActivity.getBccRecipients()){
                getBccRecipients().add(s);
            }
        }
    }

@Override
public String toString() {
    String ret = new String();

    ret = "SendMailActivityEntity ["
            +"getId()=" + getId()
            +", getSubject()=" + getSubject()
            +", getContent()="+ getContent()
            +", getCharset()="+ getCharset()
    ;
    if(getActivityEntity()!=null){
        ret+=", { "+getActivityEntity()+"}";
    }
    if(getRecipients()!=null){
        ret+=", getRecipients(){ ";
        for(String s:getRecipients())
           ret+=", "+s;
        ret+="}";
    }
    if(getCcRecipients()!=null){
        ret+=", getCcRecipients(){ ";
        for(String s:getCcRecipients())
            ret+=", "+s;
        ret+="}";
    }
    if(getBccRecipients()!=null){
        ret+=", getBccRecipients(){ ";
        for(String s:getBccRecipients())
            ret+=", "+s;
        ret+="}";
    }
    ret+="]";
    return ret;
}

}
