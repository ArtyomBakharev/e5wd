package ru.efive.workflow.designer.hibernate.entity;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.GenericGenerator;
import ru.efive.workflow.designer.interfaces.PropertyChangeDescriptor;
import ru.efive.workflow.designer.interfaces.ReturnType;
import ru.efive.workflow.designer.interfaces.SetPropertyActivity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dmitry Parshin
 * Date: 16.04.12
 */
@javax.persistence.Table(name = "set_property_activity", schema = "", catalog = "e5wd")
@Entity
public class SetPropertyActivityEntity implements SetPropertyActivity {
    private int blankActivityId;
    private ActivityEntity activityEntity;
    private List<PropertyChangeDescriptorEntity> propertyChanges;

    private InvokeMethodActivityEntity invokeMethodActivityEntity;

    @GenericGenerator(name = "generator", strategy = "foreign",
            parameters = @org.hibernate.annotations.Parameter(name = "property", value = "activityEntity")
    )

    @javax.persistence.Column(
            name = "blank_activity_id",
            nullable = false,
            insertable = true,
            updatable = true,
            length = 10,
            precision = 0)
    @Id
    @GeneratedValue(generator = "generator")
    public int getBlankActivityId() {
        return blankActivityId;
    }

    public void setBlankActivityId(int blankActivityId) {
        this.blankActivityId = blankActivityId;
    }
    @OneToOne
    @PrimaryKeyJoinColumn
    public ActivityEntity getActivityEntity() {
        if (activityEntity == null) activityEntity= new  ActivityEntity();
        return activityEntity;
    }

    public void setActivityEntity(ActivityEntity activityEntity) {
        this.activityEntity = activityEntity;
    }
/*
    @CollectionOfElements
    @JoinTable(name="set_property_change",
            joinColumns = @JoinColumn(name="blankActivity_id"),
            inverseJoinColumns = @JoinColumn(name = "set_property_change_id"))
*/
    @OneToMany(cascade =CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumn(name = "blank_activity_id")
    @Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
    public List<PropertyChangeDescriptorEntity> getPropertyChanges() {
        if(propertyChanges==null) propertyChanges = new ArrayList<PropertyChangeDescriptorEntity>();
        return propertyChanges;
    }

    public void setPropertyChanges(List<? extends PropertyChangeDescriptor> propertyChanges) {
        this.propertyChanges = (List<PropertyChangeDescriptorEntity>) propertyChanges;
    }

    @OneToOne (mappedBy="setPropertyActivityEntity",cascade =CascadeType.ALL,fetch = FetchType.LAZY)
    @Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
    public InvokeMethodActivityEntity getInvokeMethodActivityEntity() {
        return invokeMethodActivityEntity;
    }

    public void setInvokeMethodActivityEntity(InvokeMethodActivityEntity invokeMethodActivityEntity) {
        this.invokeMethodActivityEntity = invokeMethodActivityEntity;
    }


/*ActivityEntity*/

    @Transient
    @Override
    public String getId(){
        return activityEntity.getId();
    }
    @Override
    public void setId(String id){}
    @Transient
    @Override
    public String getMetadata() {
        return activityEntity.getMetadata();
    }
    @Override
    public void setMetadata(String metadata) {}
    @Transient
    @Override
    public String getName() {
        return activityEntity.getName();
    }
    @Override
    public void setName(String name) {}
    @Transient
    @Override
    public String getDescription() {
        return activityEntity.getDescription();
    }
    @Override
    public void setDescription(String description) {}
    @Transient
    @Override
    public ru.efive.workflow.designer.interfaces.Process getProcess() {
        return activityEntity.getProcess();
    }
    @Override
    public void setProcess(ru.efive.workflow.designer.interfaces.Process process) {}
    @Transient
    @Override
    public String getRunCondition() {
        return activityEntity.getRunCondition();
    }
    @Override
    public void setRunCondition(String runCondition) {}
    @Transient
    @Override
    public ReturnType getReturnType() {
        return activityEntity.getReturnType();
    }
    @Override
    public void setReturnType(ReturnType returnType) {}
    @Transient
    @Override
    public String getReturnFormula() {
        return activityEntity.getReturnFormula();
    }
    @Override
    public void setReturnFormula(String returnFormula) {}

/*End ActivityEntity*/
    @Transient
    public void fromInterface(SetPropertyActivity setPropertyActivity){
    }

    @Override
    public String toString() {
        String ret = new String();

        ret = "SetPropertyActivityEntity [";
        if(getActivityEntity()!=null){
            ret+=", { "+getActivityEntity()+"}";
        }
        if(getPropertyChanges()!=null){
            ret+=", getPropertyChanges(){ ";
            for(PropertyChangeDescriptorEntity s:getPropertyChanges())
                ret+=", {"+s.toString()+"}";
            ret+="}";
        }


        return ret;
    }
}
