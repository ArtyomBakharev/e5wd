package ru.efive.workflow.designer.hibernate.entity;

import org.hibernate.annotations.*;
import ru.efive.workflow.designer.interfaces.*;
import ru.efive.workflow.designer.interfaces.Process;

import javax.persistence.*;
import javax.persistence.Entity;
import java.util.List;

/**
 * Created by Dmitry Parshin
 */
@javax.persistence.Table(name = "status_change_action", schema = "", catalog = "e5wd")
@Entity
public class StatusChangeActionEntity implements StatusChangeAction {
    private int actionId;
    private ActionEntity actionEntity;
    private StatusEntity  fromStatus;
    private StatusEntity  toStatus;



@GenericGenerator(name = "generator", strategy = "foreign",
        parameters = @org.hibernate.annotations.Parameter(name = "property", value = "actionEntity")
)
@javax.persistence.Column(
        name = "action_id",
        nullable = false,
        insertable = true,
        updatable = true,
        length = 10,
        precision = 0)
@Id
@GeneratedValue(generator = "generator")
public int getActionId() {
    return actionId;
}

    public void setActionId(int actionId) {
        this.actionId = actionId;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    public ActionEntity getActionEntity() {
        return actionEntity;
    }

    public void setActionEntity(ActionEntity actionEntity) {
        this.actionEntity = actionEntity;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="from_status_id")
    public StatusEntity getFromStatus() {
        return fromStatus;
    }

    public void setFromStatus(Status fromStatus) {
        this.fromStatus = (StatusEntity) fromStatus;
    }


    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="to_status_id")
    public StatusEntity getToStatus() {
        return toStatus;
    }

    public void setToStatus(Status toStatus) {
        this.toStatus = (StatusEntity)toStatus;
    }


    @Transient
    @Override
    public String getId() {
        return actionEntity.getId();
    }
    @Override
    public void setId(String id) {}
    @Transient
    @Override
    public String getMetadata() {
        return actionEntity.getMetadata();
    }
    @Override
    public void setMetadata(String metadata) {}

    @Transient
    @Override
    public String getName() {
        return actionEntity.getName();
    }
    @Override
    public void setName(String name) {}

    @Transient
    @Override
    public String getDescription() {
        return actionEntity.getDescription();
    }
    @Override
    public void setDescription(String description) {}


    @Transient
    @Override
    public ru.efive.workflow.designer.interfaces.Process getProcess() {
        return actionEntity.getProcess();
    }
    @Override
    public void setProcess(Process process) {}
    @Transient
    @Override
    public List<? extends Activity> getPreActivities() {
        return actionEntity.getPreActivities();
    }

    @Override
    public void setPreActivities(List<? extends Activity> preActivities) {}

    @Transient
    @Override
    public List<? extends Activity> getPostActivities() {
        return actionEntity.getPostActivities();
    }

    @Override
    public void setPostActivities(List<? extends Activity> postActivities) {}

    @Transient
    @Override
    public List<? extends Activity> getLocalActivities() {
        return actionEntity.getLocalActivities();
    }

    @Override
    public void setLocalActivities(List<? extends Activity> localActivities) {}

    @Transient
    @Override
    public String getDataType() {
        return actionEntity.getDataType();
    }

    @Override
    public void setDataType(String dataType) {}

    @Transient
    @Override
    public String getAvailabilityCondition() {
        return actionEntity.getAvailabilityCondition();
    }

    @Override
    public void setAvailabilityCondition(String availabilityCondition) {}

    @Transient
    @Override
    public String getEvaluationMessage() {
        return actionEntity.getEvaluationMessage();
    }

    @Override
    public void setEvaluationMessage(String evaluationMessage) {}

    @Transient
    @Override
    public boolean isHistoryEnabled() {
        return actionEntity.isHistoryEnabled();
    }

    @Override
    public void setHistoryEnabled(boolean historyEnabled) {}

    @Transient
    @Override
    public boolean isAutocommitEnabled() {
        return actionEntity.isAutocommitEnabled();
    }

    @Override
    public void setAutocommitEnabled(boolean autocommitEnabled) {}

    @Override
    public String toString() {
        String ret = new String();

        ret = "StatusChangeActionEntity [";
        if(getActionEntity()!=null)
            ret+="{"+getActionEntity().toString()+"}";
        if(getFromStatus()!=null)
            ret+="{"+getFromStatus().toString()+"}";
        if(getToStatus()!=null)
            ret+="{"+getToStatus().toString()+"}";
        ret+="]";
        return ret;
    }
}
