package ru.efive.workflow.designer.hibernate.entity;


import org.hibernate.annotations.*;
import ru.efive.workflow.designer.interfaces.AccessControlEntry;
import ru.efive.workflow.designer.interfaces.Activity;
import ru.efive.workflow.designer.interfaces.Status;
import ru.efive.workflow.designer.interfaces.Process;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dmitry Parshin
 */
@javax.persistence.Table(name = "status", schema = "", catalog = "e5wd")
@Entity
public class StatusEntity implements Status {
    private int statusId;
    private String name;
    private String alias;
    private String dataType;
    private String description;
    private String metadata;
    private ProcessEntity process;
    private List<AccessControlEntryEntity> acl=new ArrayList<AccessControlEntryEntity>();

    private List<ActivityEntity> localActivities;
    private List<ActivityEntity> preActivities;
    private List<ActivityEntity> postActivities;

/*
    private StatusChangeActionEntity fromStatusChangeAction;
    private StatusChangeActionEntity toStatusChangeAction;
*/

    @javax.persistence.Column(
            name = "status_id", 
            nullable = false, 
            insertable = false, 
            updatable = false, 
            length = 19, 
            precision = 0)
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }


    @javax.persistence.Column(
            name = "name", 
            nullable = true, 
            insertable = true, 
            updatable = true,
            columnDefinition="TEXT",
            length =  65535,
            precision = 0)
    @Basic
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @javax.persistence.Column(
            name = "alias", 
            nullable = true, 
            insertable = true, 
            updatable = true,
            columnDefinition="TEXT",
            length =  65535,
            precision = 0)
    @Basic
    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }


    @javax.persistence.Column(
            name = "data_type", 
            nullable = true, 
            insertable = true, 
            updatable = true,
            columnDefinition="TEXT",
            length =  65535,
            precision = 0)
    @Basic
    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }


    @javax.persistence.Column(
            name = "commentary", 
            nullable = true, 
            insertable = true, 
            updatable = true,
            columnDefinition="TEXT",
            length =  65535,
            precision = 0)
    @Basic
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    @javax.persistence.Column(
            name = "metadata", 
            nullable = true, 
            insertable = true, 
            updatable = true,
            columnDefinition="TEXT",
            length =  65535,
            precision = 0)
    @Basic
    public String getMetadata() {
        return metadata;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "process_id")
    public ProcessEntity getProcess() {
        return process;
    }

    public void setProcess(Process process) {
        this.process = (ProcessEntity) process;
    }

    @ManyToMany(cascade =CascadeType.REFRESH,fetch = FetchType.LAZY)
    @JoinTable(name = "status_localActivities",
            joinColumns = @JoinColumn(name = "status_id"),
            inverseJoinColumns = @JoinColumn(name = "blank_activity_id")
    )
    //@Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
    public List<ActivityEntity> getLocalActivities() {
        if(localActivities==null) localActivities = new ArrayList<ActivityEntity>();
        return localActivities;
    }

    public void setLocalActivities(List<? extends Activity> localActivities) {
        this.localActivities = (List<ActivityEntity>) localActivities;
    }

    @ManyToMany(cascade =CascadeType.REFRESH,fetch = FetchType.LAZY)
    @JoinTable(name = "status_preStatusActivities",
            joinColumns = @JoinColumn(name = "status_id"),
            inverseJoinColumns = @JoinColumn(name = "blank_activity_id")
    )
    //@Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
    public List<ActivityEntity> getPreActivities() {
        if(preActivities==null) preActivities = new ArrayList<ActivityEntity>();
        return preActivities;
    }

    public void setPreActivities(List<? extends Activity> preActivities) {
        this.preActivities = (List<ActivityEntity>) preActivities;
    }


    @ManyToMany(cascade =CascadeType.REFRESH,fetch = FetchType.LAZY)
    @JoinTable(name = "status_postStatusActivities",
            joinColumns = @JoinColumn(name = "status_id"),
            inverseJoinColumns = @JoinColumn(name = "blank_activity_id")
    )
    //@Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
    public List<ActivityEntity> getPostActivities() {
        if(postActivities==null) postActivities = new ArrayList<ActivityEntity>();
        return postActivities;
    }

    public void setPostActivities(List<? extends Activity> postActivities) {
        this.postActivities = (List<ActivityEntity>) postActivities;
    }


    @OneToMany(cascade =CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinTable(name = "status_access_list",
            joinColumns = @JoinColumn(name = "status_id"),
            inverseJoinColumns = @JoinColumn(name = "access_entry_id")
    )
    @Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
    public List<AccessControlEntryEntity> getAcl() {
        if(acl==null) acl = new ArrayList<AccessControlEntryEntity>();
        return acl;
    }

    public void setAcl(List<? extends AccessControlEntry> acl) {
        this.acl = (List<AccessControlEntryEntity>) acl;
    }

/*
    @OneToOne(mappedBy= "fromStatus",fetch = FetchType.LAZY)
    @Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
    public StatusChangeActionEntity getFromStatusChangeAction() {
        return fromStatusChangeAction;
    }

    public void setFromStatusChangeAction(StatusChangeActionEntity fromStatusChangeAction) {
        this.fromStatusChangeAction = fromStatusChangeAction;
    }

    @OneToOne(mappedBy= "toStatus",fetch = FetchType.LAZY)
    @Cascade(org.hibernate.annotations.CascadeType.DELETE_ORPHAN)
    public StatusChangeActionEntity getToStatusChangeAction() {
        return toStatusChangeAction;
    }

    public void setToStatusChangeAction(StatusChangeActionEntity toStatusChangeAction) {
        this.toStatusChangeAction = toStatusChangeAction;
    }
*/

    @Transient
    @Override
    public String getId() {
        return Integer.toString(statusId);
    }

    @Override
    public void setId(String id) {}


    @Transient
    public void fromInterface(Status status){
        if(status!=null){
            setName(status.getName());
            setAlias(status.getAlias());
            setDataType(status.getDataType());
            setDescription(status.getDescription());
            setMetadata(status.getMetadata());

            getAcl().clear();
            if(status.getAcl()!=null){
                for(AccessControlEntry a: status.getAcl()){
                    AccessControlEntryEntity accessControlEntryEntity = new AccessControlEntryEntity();
                    accessControlEntryEntity.fromInterface(a);
                    getAcl().add(accessControlEntryEntity);
                }
            }
        }
    }

    @Override
    public String toString() {
        String ret = new String();
        
        ret = "StatusEntity ["
                +"getId()=" + getId()
                +", getName()=" + getName()
                +", getAlias()="+ getAlias()
                +", getDataType()="+ getDataType()
                +", getMetadata()="+ getMetadata()
                +", getAlias()="+ getAlias()
                ;
        if(getProcess()!=null){
           ret+=", { "+getProcess()+"}";
        }

        if(getAcl()!=null){
            ret+=",{ getAcl() = ";
            for(AccessControlEntry a: getAcl()){
                a.toString();
            }
            ret+="}";
        }
        ret+="]";
        return ret;
    }

}
