package ru.efive.workflow.designer.hibernate.dao;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 * Created Dmitry Parshin
 * Date: 03.05.12
 */
public class RepositoryTest {
    protected static Logger log = Logger.getLogger(RepositoryTest.class);
    private EntityManagerFactory emf = null;
    private EntityManager em = null;

    @Before
    public void setUp(){
        /*emf = Persistence.createEntityManagerFactory("e5wd_test");
        em = emf.createEntityManager();  */
    }


    @After
    public void setDown(){
        /*if(em !=null && em.isOpen()) em.close();
        if(emf!=null && emf.isOpen()) emf.close();   */
    }

    @Test
    public void testSave() throws Exception {
        /*assertNotNull("Entity manager was null",em);
        assertTrue("Entity manager wasn't connected", em.isOpen());   */
/*
//ProcessXml
        ProcessRepositoryDAOBean processRepositoryDAO = new ProcessRepositoryDAOBean(em);
        ProcessXml processXml = new ProcessXml();

        processXml.setName("Test Process - 1");
        processXml.setDataType("test data type - 1");
        processXml.setRepository(1);
        em.getTransaction().begin();
        processXml = (ProcessXml) processRepositoryDAO.save(processXml);
        em.getTransaction().commit();

        assertNotNull("Process won't find", processRepositoryDAO.findOne(processXml.getId()));
        assertEquals("Process won't find name",  ((ProcessXml) processRepositoryDAO.findOne(processXml.getId())).getName()  ,  processXml.getName());

        ProcessXml processXmlSearch = new ProcessXml();
        processXmlSearch.setDataType("test data type - 1");
        assertEquals("Process won't find data type",1,((List)processRepositoryDAO.findAll(processXmlSearch,0,0,null,true)).size());


//StatusXml
        StatusRepositoryDAOBean statusRepositoryDAO = new StatusRepositoryDAOBean(em);
        StatusXml statusXml1 = new StatusXml();
        StatusXml statusXml2 = new StatusXml();

        statusXml1.setName("Test Status 1");
        statusXml1.setProcess(processXml);
        statusXml2.setName("Test Status 2");
        statusXml2.setProcess(processXml);

        em.getTransaction().begin();
        statusXml1 = (StatusXml) statusRepositoryDAO.save(statusXml1);
        statusXml2 = (StatusXml) statusRepositoryDAO.save(statusXml2);
        em.getTransaction().commit();

        assertNotNull("Status N1 won't find",statusRepositoryDAO.findOne(statusXml1.getId()));
        assertNotNull("Status N2 won't find",statusRepositoryDAO.findOne(statusXml2.getId()));

//ActivityXml
        ActivityRepositoryDAOBean activityRepositoryDAO = new ActivityRepositoryDAOBean(em);

        ActivityXml activityXml = new ActivityXml();
        SendMailActivityXml sendMailActivityXml = new SendMailActivityXml();
        InvokeMethodActivityXml invokeMethodActivityXml = new InvokeMethodActivityXml();
        SetPropertyActivityXml setPropertyActivityXml = new InvokeMethodActivityXml();
        ParametrizedPropertyLocalActivityXml parametrizedPropertyLocalActivityXml = new ParametrizedPropertyLocalActivityXml();
        RemoteTransactionActivityXml remoteTransactionActivityXml = new RemoteTransactionActivityXml();
        InvokeScriptActivityXml invokeScriptActivityXml = new InvokeScriptActivityXml();

        activityXml.setName("test activityXml");
        activityXml.setProcess(processXml);
        em.getTransaction().begin();
        activityXml = (ActivityXml) activityRepositoryDAO.save(activityXml);
        em.getTransaction().commit();
        assertNotNull("Activity won't find",activityRepositoryDAO.findOne(activityXml.getId()));
        assertEquals( ((Activity) activityRepositoryDAO.findOne(activityXml.getId())).getName(),activityXml.getName());

        sendMailActivityXml.setName("sendMailActivityXml test");
        sendMailActivityXml.setProcess(processXml);
        em.getTransaction().begin();
        sendMailActivityXml = (SendMailActivityXml) activityRepositoryDAO.save(sendMailActivityXml);
        em.getTransaction().commit();
        assertNotNull("SendMailActivity won't find",activityRepositoryDAO.findOne(sendMailActivityXml.getId()));
        assertEquals(((Activity)activityRepositoryDAO.findOne(sendMailActivityXml.getId())).getName(),sendMailActivityXml.getName());

        invokeMethodActivityXml.setName("invokeMethodActivityXml test");
        invokeMethodActivityXml.setProcess(processXml);
        em.getTransaction().begin();
        invokeMethodActivityXml = (InvokeMethodActivityXml) activityRepositoryDAO.save(invokeMethodActivityXml);
        em.getTransaction().commit();
        assertNotNull("InvokeMethodActivity won't find",activityRepositoryDAO.findOne(invokeMethodActivityXml.getId()));
        assertEquals(((Activity)activityRepositoryDAO.findOne(invokeMethodActivityXml.getId())).getName(),invokeMethodActivityXml.getName());

        setPropertyActivityXml.setName("setPropertyActivityXml test");
        setPropertyActivityXml.setProcess(processXml);
        em.getTransaction().begin();
        setPropertyActivityXml = (SetPropertyActivityXml) activityRepositoryDAO.save(setPropertyActivityXml);
        em.getTransaction().commit();
        assertNotNull("SetPropertyActivity won't find",activityRepositoryDAO.findOne(setPropertyActivityXml.getId()));
        assertEquals(((Activity)activityRepositoryDAO.findOne(setPropertyActivityXml.getId())).getName(),setPropertyActivityXml.getName());

        parametrizedPropertyLocalActivityXml.setName("parametrizedPropertyLocalActivityXml test");
        parametrizedPropertyLocalActivityXml.setProcess(processXml);
        em.getTransaction().begin();
        parametrizedPropertyLocalActivityXml = (ParametrizedPropertyLocalActivityXml) activityRepositoryDAO.save(parametrizedPropertyLocalActivityXml);
        em.getTransaction().commit();
        assertNotNull("ParametrizedPropertyLocalActivity won't find",activityRepositoryDAO.findOne(parametrizedPropertyLocalActivityXml.getId()));
        assertEquals(((Activity)activityRepositoryDAO.findOne(parametrizedPropertyLocalActivityXml.getId())).getName(),parametrizedPropertyLocalActivityXml.getName());

        remoteTransactionActivityXml.setName("remoteTransactionActivityXml test");
        remoteTransactionActivityXml.setActionId("23");
        remoteTransactionActivityXml.setProcess(processXml);
        em.getTransaction().begin();
        remoteTransactionActivityXml = (RemoteTransactionActivityXml) activityRepositoryDAO.save(remoteTransactionActivityXml);
        em.getTransaction().commit();
        assertNotNull("RemoteTransactionActivity won't find", activityRepositoryDAO.findOne(remoteTransactionActivityXml.getId()));
        assertEquals(((Activity) activityRepositoryDAO.findOne(remoteTransactionActivityXml.getId())).getName(), remoteTransactionActivityXml.getName());
        assertEquals(((RemoteTransactionActivityXml)activityRepositoryDAO.findOne(remoteTransactionActivityXml.getId())).getActionId(),remoteTransactionActivityXml.getActionId());

        invokeScriptActivityXml.setName("invokeScriptActivityXml test");
        invokeScriptActivityXml.setScript("test script");
        invokeScriptActivityXml.setProcess(processXml);
        em.getTransaction().begin();
        invokeScriptActivityXml = (InvokeScriptActivityXml) activityRepositoryDAO.save(invokeScriptActivityXml);
        em.getTransaction().commit();
        assertNotNull("InvokeScriptActivity won't find", activityRepositoryDAO.findOne(invokeScriptActivityXml.getId()));

//
        statusXml1.getLocalActivities().add(activityXml);
        statusXml1.getLocalActivities().add(sendMailActivityXml);
        statusXml1.getLocalActivities().add(invokeMethodActivityXml);
        statusXml1.getLocalActivities().add(setPropertyActivityXml);
        statusXml1.getLocalActivities().add(parametrizedPropertyLocalActivityXml);
        statusXml1.getLocalActivities().add(remoteTransactionActivityXml);
        statusXml1.getLocalActivities().add(invokeScriptActivityXml);

        statusXml1.getPreActivities().add(activityXml);
        statusXml1.getPreActivities().add(sendMailActivityXml);
        statusXml1.getPreActivities().add(invokeMethodActivityXml);
        statusXml1.getPreActivities().add(setPropertyActivityXml);
        statusXml1.getPreActivities().add(parametrizedPropertyLocalActivityXml);
        statusXml1.getPreActivities().add(remoteTransactionActivityXml);

        statusXml1.getPostActivities().add(activityXml);
        statusXml1.getPostActivities().add(sendMailActivityXml);
        statusXml1.getPostActivities().add(invokeMethodActivityXml);
        statusXml1.getPostActivities().add(setPropertyActivityXml);
        statusXml1.getPostActivities().add(parametrizedPropertyLocalActivityXml);
        statusXml1.getPostActivities().add(remoteTransactionActivityXml);

        em.getTransaction().begin();
        statusXml1 = (StatusXml) statusRepositoryDAO.save(statusXml1);
        em.getTransaction().commit();

        assertEquals( "StatusRepositoryDAOBean findActivity", 19, ( (List) statusRepositoryDAO.findActivity(null,statusXml1.getId())).size());


//ActionXml
        ActionRepositoryDAOBean actionRepositoryDAO = new ActionRepositoryDAOBean(em);
        StatusChangeActionXml statusChangeActionXml = new StatusChangeActionXml();
        NoStatusActionXml noStatusActionXml = new NoStatusActionXml();

        statusChangeActionXml.setName("statusChangeActionXml test");
        statusChangeActionXml.setProcess(processXml);
        statusChangeActionXml.setFromStatus(statusXml1);
        statusChangeActionXml.setToStatus(statusXml2);

        statusChangeActionXml.getPreActivities().add(activityXml);
        statusChangeActionXml.getPreActivities().add(sendMailActivityXml);
        statusChangeActionXml.getPreActivities().add(invokeMethodActivityXml);
        statusChangeActionXml.getPreActivities().add(setPropertyActivityXml);
        statusChangeActionXml.getPreActivities().add(parametrizedPropertyLocalActivityXml);
        statusChangeActionXml.getPreActivities().add(remoteTransactionActivityXml);

        statusChangeActionXml.getPostActivities().add(activityXml);
        statusChangeActionXml.getPostActivities().add(sendMailActivityXml);
        statusChangeActionXml.getPostActivities().add(invokeMethodActivityXml);
        statusChangeActionXml.getPostActivities().add(setPropertyActivityXml);
        statusChangeActionXml.getPostActivities().add(parametrizedPropertyLocalActivityXml);
        statusChangeActionXml.getPostActivities().add(remoteTransactionActivityXml);


        em.getTransaction().begin();
        statusChangeActionXml = (StatusChangeActionXml) actionRepositoryDAO.save(statusChangeActionXml);
        em.getTransaction().commit();

        assertNotNull("StatusChangeAction won't find",actionRepositoryDAO.findOne(statusChangeActionXml.getId()));
        assertEquals(((Action)actionRepositoryDAO.findOne(statusChangeActionXml.getId())).getName(),statusChangeActionXml.getName());
        assertEquals( "ActionRepositoryDAOBean statusChangeActionXml findActivity", 12, ( (List) actionRepositoryDAO.findActivity(null,statusChangeActionXml.getId())).size());
        assertEquals( "ActionRepositoryDAOBean statusChangeActionXml findFromStatus", 1,( (List) actionRepositoryDAO.findFromStatus(null,statusXml1.getId())).size());

//
        noStatusActionXml.setName("noStatusActionXml test");
        noStatusActionXml.setProcess(processXml);

        noStatusActionXml.getPreActivities().add(activityXml);
        noStatusActionXml.getPreActivities().add(sendMailActivityXml);
        noStatusActionXml.getPreActivities().add(invokeMethodActivityXml);
        noStatusActionXml.getPreActivities().add(setPropertyActivityXml);
        noStatusActionXml.getPreActivities().add(parametrizedPropertyLocalActivityXml);
        noStatusActionXml.getPreActivities().add(remoteTransactionActivityXml);

        noStatusActionXml.getPostActivities().add(activityXml);
        noStatusActionXml.getPostActivities().add(sendMailActivityXml);
        noStatusActionXml.getPostActivities().add(invokeMethodActivityXml);
        noStatusActionXml.getPostActivities().add(setPropertyActivityXml);
        noStatusActionXml.getPostActivities().add(parametrizedPropertyLocalActivityXml);
        noStatusActionXml.getPostActivities().add(remoteTransactionActivityXml);

        em.getTransaction().begin();
        noStatusActionXml = (NoStatusActionXml) actionRepositoryDAO.save(noStatusActionXml);
        em.getTransaction().commit();
        assertNotNull("NoStatusAction won't find",actionRepositoryDAO.findOne(noStatusActionXml.getId()));
        assertEquals(((Action)actionRepositoryDAO.findOne(noStatusActionXml.getId())).getName(),noStatusActionXml.getName());
        assertEquals( "ActionRepositoryDAOBean noStatusActionXml findActivity", 12, ( (List) actionRepositoryDAO.findActivity(null,noStatusActionXml.getId())).size());


//


//DELETE ActionXml
        em.getTransaction().begin();
        actionRepositoryDAO.delete(statusChangeActionXml.getId());
        actionRepositoryDAO.delete(noStatusActionXml.getId());
        em.getTransaction().commit();

        assertNull("StatusChangeAction won't delete",actionRepositoryDAO.findOne(statusChangeActionXml.getId()));
        assertNull("NoStatusAction won't delete",actionRepositoryDAO.findOne(noStatusActionXml.getId()));


//DELETE ActivityXml
        //log.warn("DELETE ActivityXml");
        em.getTransaction().begin();
        activityRepositoryDAO.delete(activityXml.getId());
        activityRepositoryDAO.delete(sendMailActivityXml.getId());
        activityRepositoryDAO.delete(invokeMethodActivityXml.getId());
        activityRepositoryDAO.delete(setPropertyActivityXml.getId());
        activityRepositoryDAO.delete(parametrizedPropertyLocalActivityXml.getId());
        activityRepositoryDAO.delete(remoteTransactionActivityXml.getId());
        activityRepositoryDAO.delete(invokeScriptActivityXml.getId());
        em.getTransaction().commit();

        assertNull("Activity won't delete",activityRepositoryDAO.findOne(activityXml.getId()));
        assertNull("SendMailActivity won't delete",activityRepositoryDAO.findOne(sendMailActivityXml.getId()));
        assertNull("InvokeMethodActivity won't delete",activityRepositoryDAO.findOne(invokeMethodActivityXml.getId()));
        assertNull("SetPropertyActivity won't delete",activityRepositoryDAO.findOne(setPropertyActivityXml.getId()));
        assertNull("ParametrizedPropertyLocalActivity won't delete",activityRepositoryDAO.findOne(parametrizedPropertyLocalActivityXml.getId()));
        assertNull("RemoteTransactionActivity won't delete",activityRepositoryDAO.findOne(remoteTransactionActivityXml.getId()));
        assertNull("invokeScriptActivity won't delete",activityRepositoryDAO.findOne(invokeScriptActivityXml.getId()));

//DELETE StatusXml
        //log.warn("DELETE StatusXml");
        em.getTransaction().begin();
        statusRepositoryDAO.delete(statusXml1.getId());
        statusRepositoryDAO.delete(statusXml2.getId());
        em.getTransaction().commit();


        assertNull("Status N1 won't delete",statusRepositoryDAO.findOne(statusXml1.getId()));
        assertNull("Status N2 won't delete",statusRepositoryDAO.findOne(statusXml2.getId()));

//DELETE ProcessXml
        //log.warn("DELETE ProcessXml");
        em.getTransaction().begin();
        processRepositoryDAO.delete(processXml.getId());
        em.getTransaction().commit();

        assertNull("Process won't delete",processRepositoryDAO.findOne(processXml.getId()));
*/
    }
}
