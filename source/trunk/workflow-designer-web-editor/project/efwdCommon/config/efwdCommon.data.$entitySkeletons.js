goog.provide("__symbol__.efwdConfig.efwdCommon.data.$entitySkeletons");

/**
 * Описание структуры и значений по умолчанию для каждого типа сущностей
 */
AppTools.config("efwdConfig.efwdCommon.data.$entitySkeletons", {

	action: {
		/**
		 * Поля входят в каждый подтип сущности
		 */
		$common: {
			id: null,
			name: null,
			commentary: null,
			availability_condition: "true",
			data_type: "java.lang.Object",
			evaluation_message: "Действие успешно выполнено",
			is_autocommit_enabled: true,
			is_history_enabled: true,
			local_action_activities: [],
			post_action_activities: [],
			pre_action_activities: [],
			process: null,
			$type: null
		},
		/**
		 * Поля уникальные для подтипов сущности
		 */
		$subtypes: {
			status_change_action: {
				from_status_id: null,
				to_status_id: null,
				metadata: {
					joints: [],
					labelPosition: { x: 0, y: 0 },
					sourcePoint: { x: null, y: null },
					destPoint: { x: null, y: null }
				}
			}
		}
	},

	activity: {
		$common: {
			commentary: null,
			id: null,
			name: null,
			process: null,
			return_formula: "true",
			return_type: "VOID",
			run_condition: "true",
			$type: null
		},
		$subtypes: {
			send_mail_activity: {
				blindcopyto: null,
				body_content: null,
				copyto: null,
				sendto: null,
				subject: null
			},
			set_property_activity: {
				property_changes: null
			},
			invoke_method_activity: {
				class_name: null,
				method_name: null,
				property_changes: null
			},
			parametrized_property_local_activity: {
				form: null,
				property_changes: null
			},
			remote_transaction_activity: {
				process_id: null,
				action_id: null,
				data_type: "java.lang.Object",
				data_search_script: null
			},
			invoke_script_activity: {
				script: null
			}
		}
	},

	process: {
		$type: null,
		access_list: [],
		commentary: null,
		data_type: "java.lang.Object",
		id: null,
		name: null,
		repository: null,
		initial_status_id: null
	},

	status: {
		alias: null,
		commentary: null,
		data_type: "java.lang.Object",
		id: null,
		localActivities: [],
		name: null,
		postStatusActivities: [],
		preStatusActivities: [],
		process: null,
		$type: null,
		access_list: [],
		metadata: {
			x: 0,
			y: 0
		}
	}

});