goog.require("__symbol__.efwdConfig.efwdCommon.data.$entitySkeletons");

AppTools.config("efwdConfig.efwdCommon", {
	data: {
		types: {
			$activityCategories: {
				status: ["localActivities", "preStatusActivities", "postStatusActivities"],
				action: ["local_action_activities", "pre_action_activities", "post_action_activities"]
			},
			$activityTypes: [
				"send_mail_activity",
				"set_property_activity",
				"invoke_method_activity",
				"parametrized_property_local_activity",
				"remote_transaction_activity",
				"invoke_script_activity"
			]
		}
	},

	Communication: {
		codes: {
			diagramLocked: 450, //диаграмма заблокирована другим пользователем,
			entityDoesNotExists: 451 //сущность, которую нужно удалить/изменить отсутствует на сервере
		}
	}
});
