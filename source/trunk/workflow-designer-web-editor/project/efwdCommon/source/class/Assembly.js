goog.require("efwdCommon.prepare");
goog.require("efwdCommon.data.Mapper");
goog.require("efwdCommon.data.Repository");
goog.require("efwdCommon.data.JsonRepository");
goog.require("efwdCommon.Communication");
goog.require("efwdCommon.Helper");
goog.require("efwdCommon.Inflector");

efwdCommon.Dependencies = {
	DEPENDENCIES: [
		//efwdCommon
		[efwdCommon.Communication],

		//efwdCommon.data
		[efwdCommon.data.Mapper],
		[efwdCommon.data.Repository, {args: [
			efwdCommon.Communication,
			efwdCommon.data.Mapper
		]}]
	]
};
