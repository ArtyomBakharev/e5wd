goog.provide("efwdCommon.Communication");

/**
 * Сервис инкапсулирующий логику связи с сервером
 */
efwdCommon.Communication = function ()
{
	/**
	 * Следует назначить эту функцию если необходимо глобально обрабатывать все ошибки коммуникации. Функция будет
	 * вызвана только если при отправке запроса не была передана функция для обработки ошибки, либо она была передана
	 * но при вызове не вернула true
	 *
	 * @type {Function}
	 * @param {String} responseText
	 * @param {Number} status
	 */
	this.onErrorHandler = null;

	this.__tmp = 0;
};

efwdCommon.Communication.prototype = {
	/**
	 * послать запрос на сервер
	 *
	 * @param {Object} query
	 * @param {String} query.url
	 * @param {Object} query.params get-параметры запроса
	 * @param {String} query.method метод запроса (GET, POST, PUT, DELETE)
	 * @param {*} [query.data=null] данные запроса
	 * @param {Boolean} [query.asXml=false] послать запрос и получить ответ в виде XML
	 * @param {Boolean} [query.async=true] сделать зарос асинхронным
	 * @param {Function} [callback=null] функция для обработки ответа
	 * @param {Function} [errback=null] функция для обработки ошибки при запросе, должна вернуть true, если ошибка обработана,
	 * принимает параметры: resonseText и status
	 * @param {Object} [context=null] объект, в контексте которого будут вызваны callback и errback
	 * @return {XMLHttpRequest}
	 */
	sendQuery: function (query, callback, errback, context)
	{
		var $this = this;
		var baseUrl = efwdConfig.efwdCommon.Communication.baseUrl;

		var req = AppTools.getXHR();

		req.onreadystatechange = function ()
		{
			if (req.readyState == 4)
			{
				if (req.status >= 200 && req.status < 400)
				{
					callback && callback.call(context || window, req.responseText, req.status);
				}
				else if (!errback || !errback.call(context || window, req.responseText, req.status))
				{
					$this.onErrorHandler && $this.onErrorHandler(req.responseText, req.status);
				}
			}
		};

		if (!query.params)
		{
			query.params = {};
		}
		query.params.nocache = AppTools.getUniqueString();

		var params = [];
		for (var param in query.params)
		{
			if (query.params.hasOwnProperty(param))
			{
				params.push(param + "=" + encodeURIComponent(query.params[param]));
			}
		}

		req.open(query.method, baseUrl + query.url + "?" + params.join("&"), query.async === undefined || query.async);

		req.setRequestHeader("Content-Type", query.asXml ? "application/xml" : "application/json");
		req.setRequestHeader("Accept", query.asXml ? "application/xml" : "application/json");

		req.send(query.data || null);

		return req;
	}
};
