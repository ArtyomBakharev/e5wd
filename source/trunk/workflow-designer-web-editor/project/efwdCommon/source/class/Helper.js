goog.provide("efwdCommon.Helper");

efwdCommon.Helper = {

	/**
	 * Возвращает url редактора процесса с переданным id
	 * @param {*} processId
	 * @return {String}
	 */
	getProcessEditorUrl: function (processId)
	{
		if (qx.core.Environment.get("qx.debug"))
		{
			return location.href.replace(/(efwdEditor|efwdProcesses).*$/, "")
				+ "efwdEditor/source/index.html?process-id=" + processId;
		}
		return location.href.replace(/\/[^\/]*$/, "") + "/index.html?process-id=" + processId;
	},

	/**
	 * Возвращает url списка процессов
	 * @return {String}
	 */
	getProcessesListUrl: function()
	{
		if (qx.core.Environment.get("qx.debug"))
		{
			return location.href.replace(/(efwdEditor|efwdProcesses).*$/, "")
				+ "efwdProcesses/source/processes.html";
		}
		return location.href.replace(/\/[^\/]*$/, "") + "/processes.html";
	},

	getEntitySkeleton: function(type, subtype)
	{
		!this.__skeletons && (this.__skeletons = {});

		var key = type + "$$" + subtype;
		if (!this.__skeletons[key])
		{
			var skeleton = efwdConfig.efwdCommon.data.$entitySkeletons[type];

			if (skeleton.$subtypes)
			{
				var result = {};
				if (skeleton.$common)
				{
					AppTools.shallowCopy(skeleton.$common, result);
				}
				if (skeleton.$subtypes[subtype])
				{
					AppTools.shallowCopy(skeleton.$subtypes[subtype], result);
				}

				skeleton = result;
			}
			this.__skeletons[key] = skeleton;
		}

		return this.__skeletons[key];
	}

};
