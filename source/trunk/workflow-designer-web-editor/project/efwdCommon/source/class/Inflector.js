goog.provide("efwdCommon.Inflector");

efwdCommon.Inflector = {
	/**
	 * Возвращает множественную форму слова
	 * @param {String} word
	 * @return {String}
	 */
	plural: function(word)
	{
		var lastLetter = word[word.length - 1];
		switch(lastLetter)
		{
			case "s":
			case "z":
				return word + "es";

			case "y":
				return word.substr(0, word.length - 1) + "ies";

			default:
				return word + "s";
		}
	}
};
