goog.provide("efwdCommon.data.JsonRepository");

/**
 * Репозиторий возвращающий списки объектов системы.
 */
efwdCommon.data.JsonRepository = AppTools.createClass({
	/**
	 * @param {efwdCommon.data.Mapper} mapper
	 */
	construct: function (mapper)
	{
		this.__mapper = mapper;
		this.__data = {};
	},

	members: {

		/**
		 * Получает объект efwdCommon.data.types.Process с сервера и передаёт его в callback
		 * @param {*} processId
		 * @param {Function} callback
		 */
		getProcess: function (processId, callback, context)
		{
			callback.call(context || window, this.__data[processId].process);
		},

		/**
		 * Получает список объектов efwdCommon.data.types.Action с сервера и передаёт их в callback
		 * @param {*} processId
		 * @param {Function} callback
		 */
		getActions: function (processId, callback, context)
		{
			callback.call(context || window, this.__data[processId].actions);
		},

		/**
		 * Получает список объектов efwdCommon.data.types.Status с сервера и передаёт их в callback
		 * @param {*} processId
		 * @param {Function} callback
		 */
		getStatuses: function (processId, callback, context)
		{
			callback.call(context || window, this.__data[processId].statuses);
		},

		registerData: function (processId, data)
		{
			this.__data[processId] = {
				process: this.__mapper.mapToClientObject("process", data.process),
				actions: data.actions.items.map(function (x){ return this.__mapper.mapToClientObject("action", x) },
					this),
				statuses: data.statuses.items.map(function (x){ return this.__mapper.mapToClientObject("status", x) },
					this)
			};
		}
	}
});
