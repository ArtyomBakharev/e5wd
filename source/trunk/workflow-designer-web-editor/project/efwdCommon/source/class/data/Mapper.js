goog.provide("efwdCommon.data.Mapper");

/**
 * Класс, преобразующий серверные объекты в клиентские и наоборот
 */
efwdCommon.data.Mapper = function ()
{
	this.__activityTypes = efwdConfig.efwdCommon.data.types.$activityCategories;
};

efwdCommon.data.Mapper.prototype = {
	/**
	 * Преобразовние серверного объекта в клиентский
	 * @param {Object} object
	 * @return {Object}
	 */
	mapToClientObject: function (type, object)
	{
		object = AppTools.deepCopy(object);

		if (type == "action")
		{
			object.$type = object.from_status_id ? "status_change_action" : "no_status_action";
		}
		else
		{
			object.$type = type;
		}

		if (type in this.__activityTypes)
		{
			this.__activityTypes[type].forEach(function (key)
			{
				if (key in object)
				{
					if (typeof object[key] == "string")
					{
						object[key] = object[key].split(" ");
					}
					else if (object[key])
					{
						object[key] = object[key].map(function (x){return x.id});
					}
					else
					{
						object[key] = [];
					}
				}
			});
		}

		if ("process" in object && typeof object.process == "object")
		{
			object.process = object.process.id;
		}

		if (type == "action")
		{
			([
				"from_status_id",
				"to_status_id"
			]).forEach(function (key)
			{
				if (key in object && typeof object[key] == "object")
				{
					object[key] = object[key].id;
				}
			});
		}

		if ("metadata" in object)
		{
			object.metadata = object.metadata ? JSON.parse(object.metadata) : {};
		}

		return object;
	},

	/**
	 * Преобразовние клиентского объекта в серверный
	 * @param {Object} object
	 * @return {Object}
	 */
	mapToServerObject: function (type, object)
	{
		object = Object.deepCopy(object);

		delete object.$type;

		if (type in this.__activityTypes)
		{
			this.__activityTypes[type].forEach(function (key)
			{
				if (key in object && Array.isArray(object[key]))
				{
					object[key] = object[key].join(" ");
				}
			});
		}

		if (object.metadata)
		{
			object.metadata = JSON.stringify(object.metadata);
		}

		return object;
	}
};
