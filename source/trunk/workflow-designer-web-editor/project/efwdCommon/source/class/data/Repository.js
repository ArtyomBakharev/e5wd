goog.provide("efwdCommon.data.Repository");

/**
 * Репозиторий возвращающий списки объектов системы.
 */
efwdCommon.data.Repository = AppTools.createClass({
	/**
	 * @param {efwdCommon.Communication} communication
	 * @param {efwdCommon.data.Mapper} mapper
	 */
	construct: function (communication, mapper)
	{
		this.__communication = communication;
		this.__mapper = mapper;
	},

	members: {

		/**
		 * Получает список объектов efwdCommon.data.types.Action с сервера и передаёт их в callback
		 * @param {*} processId
		 * @param {Function} callback
		 */
		getActions: function (processId, callback, context)
		{
			this.__communication.sendQuery({
				url: "processes/" + processId + "/actions",
				method: "GET"
			}, function (result)
			{
				callback.call(context || window, this.__processItemsResult(result, "action"));
			}, null, this);
		},

		/**
		 * Получает объект efwdCommon.data.types.Process с сервера и передаёт его в callback
		 * @param {*} processId
		 * @param {Function} callback
		 */
		getProcess: function (processId, callback, context)
		{
			this.__communication.sendQuery({
				url: "processes/" + processId,
				method: "GET"
			}, function (result)
			{

				var source = this.__mapper.mapToClientObject("process", JSON.parse(result));
				callback.call(context || window, source);

			}, null, this);
		},

		/**
		 * Получает список объектов efwdCommon.data.types.Status с сервера и передаёт их в callback
		 * @param {*} processId
		 * @param {Function} callback
		 */
		getStatuses: function (processId, callback, context)
		{
			this.__communication.sendQuery({
				url: "processes/" + processId + "/statuses",
				method: "GET"
			}, function (result)
			{

				callback.call(context || window, this.__processItemsResult(result, "status"));

			}, null, this);
		},

		/**
		 * @private
		 */
		__processItemsResult: function (result, type)
		{
			var items = JSON.parse(result).items;
			return items ? items.map(this.__mapper.mapToClientObject.bind(this.__mapper, type)) : [];
		}
	}
});
