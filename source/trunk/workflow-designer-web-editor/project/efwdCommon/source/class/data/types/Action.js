goog.provide("efwdCommon.data.types.Action");

/**
 * Primitive class!
 */
efwdCommon.data.types.Action = function ()
{
};

efwdCommon.data.types.Action.prototype = {

	/**
	 * @type {string}
	 */
	availability_condition: null,


	/**
	 * @type {string}
	 */
	data_type: null,


	/**
	 * @type {string}
	 */
	evaluation_message: null,


	/**
	 * @type {Boolean}
	 */
	is_autocommit_enabled: null,


	/**
	 * @type {Boolean}
	 */
	is_history_enabled: null,


	/**
	 * @type {string}
	 */
	post_action_activities: null,


	/**
	 * @type {string}
	 */
	pre_action_activities: null,


	/**
	 * @type {string}
	 */
	process: null,


	/**
	 * @type {string}
	 */
	$type: null
};
