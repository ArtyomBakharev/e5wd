goog.provide("efwdCommon.data.types.Activity");

/**
 * Primitive class!
 */
efwdCommon.data.types.Activity = function ()
{
};

efwdCommon.data.types.Activity.prototype = {

	/**
	 * @type {string}
	 */
	commentary: null,


	/**
	 * @type {string}
	 */
	id: null,


	/**
	 * @type {string}
	 */
	name: null,


	/**
	 * @type {string}
	 */
	process: null,


	/**
	 * @type {string}
	 */
	return_formula: null,


	/**
	 * @type {string}
	 */
	return_type: null,


	/**
	 * @type {string}
	 */
	run_condition: null,


	/**
	 * @type {string}
	 */
	$type: null
};
