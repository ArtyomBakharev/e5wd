goog.provide("efwdCommon.data.types.BlankActivity");
goog.require("efwdCommon.data.types.Activity");

/**
 * Primitive class!
 */
efwdCommon.data.types.BlankActivity = function ()
{
};
InheritClass(efwdCommon.data.types.BlankActivity, efwdCommon.data.types.Activity);

efwdCommon.data.types.BlankActivity.prototype = {

};
