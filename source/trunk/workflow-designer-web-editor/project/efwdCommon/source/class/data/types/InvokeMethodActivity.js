goog.provide("efwdCommon.data.types.InvokeMethodActivity");
goog.require("efwdCommon.data.types.Activity");

/**
 * Primitive class!
 */
efwdCommon.data.types.InvokeMethodActivity = function ()
{
};
AppTools.inheritClass(efwdCommon.data.types.InvokeMethodActivity, efwdCommon.data.types.Activity);

efwdCommon.data.types.InvokeMethodActivity.prototype = {

	/**
	 * @type {string}
	 */
	class_name: null,


	/**
	 * @type {string}
	 */
	method_name: null,


	/**
	 * @type {Object[]}
	 */
	property_changes: null
};
