goog.provide("efwdCommon.data.types.ParametrizedPropertyLocalActivity");
goog.require("efwdCommon.data.types.Activity");

/**
 * Primitive class!
 */
efwdCommon.data.types.ParametrizedPropertyLocalActivity = function ()
{
};
AppTools.inheritClass(efwdCommon.data.types.ParametrizedPropertyLocalActivity, efwdCommon.data.types.Activity);

efwdCommon.data.types.ParametrizedPropertyLocalActivity.prototype = {

	/**
	 * @type {string}
	 */
	form: null,


	/**
	 * @type {Object[]}
	 */
	property_changes: null
};
