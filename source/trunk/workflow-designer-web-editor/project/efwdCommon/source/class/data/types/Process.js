goog.provide("efwdCommon.data.types.Process");

/**
 * Primitive class!
 */
efwdCommon.data.types.Process = function ()
{
};

efwdCommon.data.types.Process.prototype = {

	/**
	 * @type {string}
	 */
	commentary: null,


	/**
	 * @type {string}
	 */
	id: null,


	/**
	 * @type {string}
	 */
	name: null,

	access_list: null,


	/**
	 * @type {string}
	 */
	$type: null
};
