goog.provide("efwdCommon.data.types.PropertyChangeType");

/**
 * Primitive class!
 */
efwdCommon.data.types.PropertyChangeType = function ()
{
};

efwdCommon.data.types.PropertyChangeType.prototype = {

	/**
	 * @type {string}
	 */
	name: null,


	/**
	 * @type {string}
	 */
	value: null
};
