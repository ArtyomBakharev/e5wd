goog.provide("efwdCommon.data.types.SendMailActivity");
goog.require("efwdCommon.data.types.Activity");

/**
 * Primitive class!
 */
efwdCommon.data.types.SendMailActivity = function ()
{
};
InheritClass(efwdCommon.data.types.SendMailActivity, efwdCommon.data.types.Activity);

efwdCommon.data.types.SendMailActivity.prototype = {

	/**
	 * @type {string}
	 */
	blindcopyto: null,


	/**
	 * @type {string}
	 */
	body_content: null,


	/**
	 * @type {string}
	 */
	copyto: null,


	/**
	 * @type {string}
	 */
	sendto: null,


	/**
	 * @type {string}
	 */
	subject: null
};
