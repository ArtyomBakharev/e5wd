goog.provide("efwdCommon.data.types.SetPropertyActivity");
goog.require("efwdCommon.data.types.Activity");

/**
 * Primitive class!
 */
efwdCommon.data.types.SetPropertyActivity = function ()
{
};
InheritClass(efwdCommon.data.types.SetPropertyActivity, efwdCommon.data.types.Activity);

efwdCommon.data.types.SetPropertyActivity.prototype = {

	/**
	 * @type {efwdCommon.data.types.PropertyChangeType[]}
	 */
	property_changes: null
};
