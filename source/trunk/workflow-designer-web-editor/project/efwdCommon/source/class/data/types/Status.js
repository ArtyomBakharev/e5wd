goog.provide("efwdCommon.data.types.Status");

/**
 * Primitive class!
 */
efwdCommon.data.types.Status = function ()
{
};

efwdCommon.data.types.Status.prototype = {

	/**
	 * @type {string}
	 */
	alias: null,


	/**
	 * @type {string}
	 */
	commentary: null,


	/**
	 * @type {string}
	 */
	data_type: null,


	/**
	 * @type {string}
	 */
	id: null,


	/**
	 * @type {string[]}
	 */
	localActivities: null,


	/**
	 * @type {string}
	 */
	name: null,


	/**
	 * @type {string[]}
	 */
	postStatusActivities: null,


	/**
	 * @type {string[]}
	 */
	preStatusActivities: null,


	/**
	 * @type {string}
	 */
	process: null,


	/**
	 * @type {string}
	 */
	$type: null,

	/**
	 * @type {Object}
	 */
	metadata: null,

	access_list: null
};
