goog.provide("efwdCommon.data.types.StatusChangeAction");
goog.require("efwdCommon.data.types.Action");

/**
 * Primitive class!
 */
efwdCommon.data.types.StatusChangeAction = function ()
{
};
InheritClass(efwdCommon.data.types.StatusChangeAction, efwdCommon.data.types.Action);

efwdCommon.data.types.StatusChangeAction.prototype = {

	/**
	 * @type {string}
	 */
	from_status_id: null,


	/**
	 * @type {string}
	 */
	to_status_id: null
};
