AppTools.config("efwdConfig.efwdDal", {

	repository: {
		SessionRepository: {
			pingInterval: 15 * 1000
		}
	},

	saving: {
		PackageSaver: {
			savingLatency: 0//5 * 1000
		}
	}

});