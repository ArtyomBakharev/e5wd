/**
 * Генерирует имена для новых безымянных сущностей
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdDal.AutoNamesGenerator", {

	extend: qx.core.Object,

	/**
	 * @param {efwdDal.Context} context
	 */
	construct: function (context)
	{
		this.__context = context;
	},

	members: {

		initialize: function ()
		{
			this.__context.addListener("preEntityAttach", function (e)
			{
				var entity = e.getData().entity;
				if (!entity.getSource().getName())
				{
					this.__generateName(entity);
				}
			}, this);
		},

		/**
		 * @param {efwdDal.entity.AbstractEntity} entity
		 * @private
		 */
		__generateName: function (entity)
		{
			var prefix = entity.getEntityType() + " ";
			var processChild = entity.includesMixin(efwdDal.entity.MProcessChild);
			var byName = this.__context.getEntities()
				.filter(function(x){
					return (!x.includesMixin(efwdDal.entity.MProcessChild) && !processChild)
						|| (x.includesMixin(efwdDal.entity.MProcessChild) && processChild && x.getProcess() == entity.getProcess());
				})
				.buildIndex(function (x){ return x.getSource().getName() });

			for (var i = 1; ; ++i)
			{
				if (!byName[prefix + i])
				{
					entity.getSource().setName(prefix + i);
					return;
				}
			}
		}
	}
});
