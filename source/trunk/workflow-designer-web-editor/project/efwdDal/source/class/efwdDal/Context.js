/*
 #ignore(efwdConfig)
 #ignore(efwdConfig.*)
 #ignore(efwdCommon)
 #ignore(efwdCommon.*)
 */

/**
 * Контекст сущностей ситсемы. Следит за добавлениями, обновлениями, удалениями сущностей и позволяет сохранять эти изменения.
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdDal.Context", {

	extend: qx.core.Object,

	events: {
		/**
		 * @param {efwdDal.entity.AbstractEntity} entity
		 */
		"entityAttached": "qx.event.type.Data",

		/**
		 * @param {efwdDal.entity.AbstractEntity} entity
		 */
		"preEntityAttach": "qx.event.type.Data",

		/**
		 * @param {efwdDal.entity.AbstractEntity} entity
		 */
		"entityDetached": "qx.event.type.Data"
	},

	/**
	 * @param {efwdDal.saving.AbstractSaver} saver
	 * @param {efwdDal.IdsMapper} idsMapper
	 */
	construct: function (saver, idsMapper)
	{
		this.__saver = saver;
		this.__idsMapper = idsMapper;

		this.__entities = {};
		this.__changes = new efwdDal.saving.ChangeUnitsQueue();
		this.__listenersStore = new library.ListenersStore();
	},

	members: {

		/**
		 * прикрепить сушность к контексту
		 * @param {efwdDal.entity.AbstractEntity} entity
		 * @param {Boolean} fromServer
		 */
		attachEntity: function (entity, fromServer)
		{
			this.__attachEntity(entity, !fromServer);
		},

		/**
		 * Не сохранять изменения на сервере, которые были внесены с времени последнего вызова метода saveChanges
		 */
		clearChanges: function()
		{
			this.__changes.clear();
		},

		/**
		 * Генерировать модель исходника для сущности переданного типа
		 *
		 * @param {String} type
		 * @param {Object} source
		 * @return {qx.core.Object}
		 */
		generateSource: function (type, source, fromServer)
		{
			if (fromServer)
			{
				this.__idsMapper.mapToClientIds(type, source);
			}
			else
			{
				source.id = this.__idsMapper.generateClientId();
			}

			var model = qx.data.marshal.Json.createModel(efwdCommon.Helper.getEntitySkeleton(type, source.$type));
			library.data.Helper.restoreObjectToModel(source, model);
			library.data.Helper.setUpHierarchyEvents(model);

			return model;
		},

		/**
		 * возвращает все прикреплённые сущности
		 *
		 * @return {efwdDal.entity.AbstractEntity[]}
		 */
		getEntities: function ()
		{
			return Object.getValues(this.__entities);
		},

		/**
		 * Передача очереди изменений в очередь сохранения на сервер
		 */
		saveChanges: function ()
		{
			if (this.__changes.getLength() > 0)
			{
				this.__saver.addChangeUnits(this.__changes.toArray());
				this.__changes.clear();
			}
		},

		/**
		 * @param {efwdDal.entity.AbstractEntity} entity
		 * @param {Boolean} saveToServer
		 * @private
		 */
		__attachEntity: function (entity, saveToServer)
		{
			this.fireDataEvent("preEntityAttach", {entity: entity});

			this.__entityAdded(entity, saveToServer);

			this.__listenersStore.addListener(entity, "removed", function ()
			{
				this.__entityRemoved(entity);
				this.fireDataEvent("entityDetached", {entity: entity});
			}, this);

			this.__listenersStore.addListener(entity.getSource(), "selfChanged",
				function (){ this.__entityUpdated(entity) }, this);

			this.fireDataEvent("entityAttached", {entity: entity});
		},

		/**
		 * @param {efwdDal.entity.AbstractEntity} entity
		 * @param {Boolean} pushChangeUnit
		 * @private
		 */
		__entityAdded: function (entity, pushChangeUnit)
		{
			this.__entities[entity.getSource().getId()] = entity;

			//если прикрепляемая сущность не присутствует на сервере
			if (pushChangeUnit)
			{
				this.__pushChangeUnit(entity, efwdDal.saving.ChangeUnit.CHANGE_TYPE_CREATE);
			}
		},

		/**
		 * @param {efwdDal.entity.AbstractEntity} entity
		 * @private
		 */
		__entityRemoved: function (entity)
		{
			entity.addListenerOnce("restored", function (){ this.__attachEntity(entity, true) }, this);

			delete this.__entities[entity.getSource().getId()];
			this.__listenersStore.removeObject(entity);

			this.__pushChangeUnit(entity, efwdDal.saving.ChangeUnit.CHANGE_TYPE_DELETE);
		},

		/**
		 * @param {efwdDal.entity.AbstractEntity} entity
		 * @private
		 */
		__entityUpdated: function (entity)
		{
			this.__pushChangeUnit(entity, efwdDal.saving.ChangeUnit.CHANGE_TYPE_UPDATE);
		},

		/**
		 * @param {efwdDal.entity.AbstractEntity} entity
		 * @param {String} changeType
		 * @private
		 */
		__pushChangeUnit: function (entity, changeType)
		{
			this.__changes.createAndPush(entity, changeType);
		}
	}
});
