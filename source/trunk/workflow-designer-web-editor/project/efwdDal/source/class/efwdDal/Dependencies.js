/*
 #ignore(efwdCommon)
 #ignore(efwdCommon.*)
 */

/**
 * Класс хранит список зависимостей для IOC-контейнера
 */
qx.Class.define("efwdDal.Dependencies", {

	type: "static",

	statics: {
		DEPENDENCIES: [

		/** {@link efwdDal} */
			[efwdDal.AutoNamesGenerator, {args: [
				efwdDal.Context
			]}],
			[efwdDal.Context, {args: [
				efwdDal.saving.AbstractSaver,
				efwdDal.IdsMapper,
				efwdCommon.data.Repository
			]}],
			[efwdDal.IdsMapper, {args: [
				efwdDal.repository.SessionRepository
			]}],
			[efwdDal.Mapper, {args: [
				efwdCommon.data.Mapper
			]}],
			[efwdDal.Service, {args: [
				efwdDal.Mapper,
				efwdCommon.Communication
			]}],

		/** {@link efwdDal.repository} */
			[efwdDal.repository.ActionsRepository, {args: [
				efwdDal.Context,
				efwdDal.Service,
				efwdDal.IdsMapper
			]}],
			[efwdDal.repository.ActivitiesRepository, {args: [
				efwdDal.Context,
				efwdDal.Service,
				efwdDal.IdsMapper
			]}],
			[efwdDal.repository.ProcessesRepository, {args: [
				efwdDal.Context,
				efwdDal.Service,
				efwdDal.IdsMapper
			]}],
			[efwdDal.repository.SessionLocalStorage],
			[efwdDal.repository.SessionRepository, {args: [
				efwdDal.Service,
				efwdDal.repository.SessionLocalStorage
			]}],
			[efwdDal.repository.StatusesRepository, {args: [
				efwdDal.Context,
				efwdDal.Service,
				efwdDal.IdsMapper
			]}]
		]
	}
});
