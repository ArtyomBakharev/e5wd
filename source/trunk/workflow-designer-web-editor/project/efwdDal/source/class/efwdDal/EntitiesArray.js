/**
 * Объект инкапсулирует массив типа {@link qx.data.Array}, в котором находятся сущности.
 * В конструкторе массив принимает статический фильтр - функция, которая возвращает true, если сущность нужно включить в массив.
 * Массив пополняется автоматически, когда подходящая сущность прикрепляется к контексту ({@link efwdDal.Context}), а также при
 * создании массива в него попадают все сущности уже находящиеся в контексте проходящие фильтрация с помощью статического фильтра.
 * Также из него автоматически удаляются элементы, когда они открепляются от контекста.
 * Важно! Если изначально сущность не попала в массив, а в последствии она изменилась так, что стала удовлетворять статическому
 * фильтру массива, она в него не попадёт, так как массив не отслеживает иземенения сущностей. Также и наоборот, когда сущность
 * перестаёт удовлетворять условиям фильтра.
 * В этих (и только в этих!) случаях нужно вручную вызывать методы {@link efwdDal.EntitiesArray#addEntity}
 * и {@link efwdDal.EntitiesArray#removeEntity} массива.
 * Важно вызывать метод dispose массива после его использования, так как большое количество действующих массивов может
 * снизить производительность.
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdDal.EntitiesArray", {

	extend: qx.core.Object,

	statics: {

		/**
		 * Объединение нескольких массивов в один посредством объединения их фильтров в один через оператор ИЛИ.
		 * Явное добавление и удаление элементов из исходных массивов не отражается на результирующем
		 * @param {efwdDal.EntitiesArray} varargs
		 * @return {efwdDal.EntitiesArray}
		 */
		mergeArrays: function (varargs)
		{
			var arrs = Array.slice(arguments);

			var staticFilter = Function.mergePredicates.apply(Function,
				["some"].concat(arrs.map(function (x){ return x.__staticFilter })));

//			var dynamics = arrs.filter(function(x){ return !!x.__dynamicFilter }).map(function(x){ return x.__dynamicFilter });
//			var dynamicFilter = dynamics.length
//				? Function.mergePredicates.apply(Function, ["some"].concat(dynamics))
//				: null;

			var listenersStore = new library.ListenersStore();

			var result = new efwdDal.EntitiesArray(arguments[0].__context, staticFilter/*, dynamicFilter*/);
			listenersStore.addMultiBinding(arrs.map(function (arr){ return [arr, "loaded"] }), result, "loaded",
				function ()
				{
					return Array.prototype.every.call(arguments, Function.returnFirstArg);
				});

			result.addListener("disposed", function (){ listenersStore.dispose() });

			return result;
		},

		/**
		 * Возвращает массив, элементы которого удовлетворяют как фильтру исходного массива, так и переданному статическому фильтру.
		 * Явное добавление и удаление элементов из исходного массива ОТРАЖАЕТСЯ на результирующем
		 * @param {efwdDal.EntitiesArray} array
		 * @param {Function} staticFilter
		 * @return {efwdDal.EntitiesArray}
		 */
		filterArray: function (array, staticFilter/*, dynamicFilter*/)
		{
//			var resultStaticFilter = staticFilter
//				? Function.mergePredicates("every", staticFilter, array.__staticFilter)
//				: array.__staticFilter;

//			var resultDynamicFilter = dynamicFilter
//				? (array.__dynamicFilter ? Function.mergePredicates("every", dynamicFilter, array.__dynamicFilter) : dynamicFilter)
//				: array.__dynamicFilter;
//
//			return new efwdDal.EntitiesArray(array.__context, resultStaticFilter, resultDynamicFilter);

			var listenersStore = new library.ListenersStore();
			staticFilter = Function.mergePredicates("every", array.__staticFilter, staticFilter);
			var filteredArray = new efwdDal.EntitiesArray(array.__context, staticFilter);

			listenersStore.addListener(array, "addEntityExplicitly", function (e)
			{
				var entity = e.getData().entity;
				if (staticFilter(entity))
				{
					filteredArray.addEntity(entity);
				}
			});

			listenersStore.addListener(array, "removeEntityExplicitly", function (e)
			{
				var entity = e.getData().entity;
				filteredArray.removeEntity(entity);
			});

			listenersStore.addBinding(array, "loaded", filteredArray, "loaded");

			filteredArray.addListener("disposed", function (){ listenersStore.dispose() });

			return filteredArray;
		}
	},

	events: {
		/**
		 * добавлена новая сущность
		 * @params {efwdDal.entity.AbstractEntity} entity
		 */
		"addEntity": "qx.event.type.Data",

		/**
		 * новая сущность добавлена явно с помощью метода {@link efwdDal.EntitiesArray#addEntity}
		 * @params {efwdDal.entity.AbstractEntity} entity
		 */
		"addEntityExplicitly": "qx.event.type.Data",

		/**
		 * сущность удалена из массива явно с помощью метода {@link efwdDal.EntitiesArray#removeEntity}
		 * @params {efwdDal.entity.AbstractEntity} entity
		 */
		"removeEntityExplicitly": "qx.event.type.Data",

		/**
		 * массив разрушен
		 */
		"disposed": "qx.event.type.Event"
	},

	properties: {
		loaded: {
			deferredInit: true,
			nullable: false,
			check: "Boolean",
			event: "changeLoaded"
		}
	},

	/**
	 * @param {efwdDal.Context} context
	 * @param {Function} staticFilter(efwdDal.entity.AbstractEntity):Boolean
	 * @param {Boolean} [loaded=false]
	 */
	construct: function (context, staticFilter/*, dynamicFilter*/, loaded)
	{
		this.__staticFilter = staticFilter;
//		this.__dynamicFilter = dynamicFilter;
		this.__context = context;
		this.__listenersStore = new library.ListenersStore();

		var items = this.__context.getEntities().filter(staticFilter);
		this.__array = new qx.data.Array(/*dynamicFilter ? items.filter(dynamicFilter) : */items);

		items.forEach(this.__processEntity, this);

		this.__listenersStore.addListener(this.__context, "entityAttached", function (e)
		{
			var entity = e.getData().entity;
			if (staticFilter(entity))
			{
				this.__processEntity(entity);
//				if (!dynamicFilter || dynamicFilter(entity))
//				{
				this.__array.push(entity);

				this.fireDataEvent("addEntity", {entity: entity});
//				}
			}
		}, this);

		this.initLoaded(!!loaded);
	},

	members: {

		/**
		 * Добавить в массив сущность явно. Сущность должна удовлятворять статическому фильтру.
		 * @param {efwdDal.entity.AbstractEntity} entity
		 */
		addEntity: function (entity)
		{
			this.__array.push(entity);
			this.__processEntity(entity);
			this.fireDataEvent("addEntity", {entity: entity});
			this.fireDataEvent("addEntityExplicitly", {entity: entity});
		},

		/**
		 * @return {library.Deferred}
		 */
		deferred: function ()
		{
			if (this.isLoaded())
			{
				return library.Deferred.success(this);
			}
			else
			{
				var deferred = new library.Deferred();
				this.addListenerOnce("changeLoaded", function ()
				{
					if (this.isLoaded())
					{
						deferred.call(this)
					}
				}, this);
				return deferred;
			}
		},

		/**
		 * Удалить сущность из массива явно
		 * @param {efwdDal.entity.AbstractEntity} entity
		 */
		removeEntity: function (entity)
		{
			this.__array.remove(entity);
			this.fireDataEvent("removeEntityExplicitly", {entity: entity});
		},

		/**
		 * Возвращает внутренний массив типа {@link qx.data.Array}. Использовать только для подписки на события и
		 * как модели для виджетов. Не удалять/помещать сущности в этот массив напрямую.
		 * @return {qx.data.Array}
		 */
		getDataArray: function ()
		{
			return this.__array;
		},

		/**
		 * @return {Function} статический фильтр массива
		 */
		getFilter: function ()
		{
			return this.__staticFilter;
//			var staticFilter = this.__staticFilter;
//			var dynamicFilter = this.__dynamicFilter;
//
//			return function(x){ return staticFilter(x) && (!dynamicFilter || dynamicFilter(x)) };
		},

		/**
		 * @return {Number} количество сушностей в массиве
		 */
		getLength: function ()
		{
			return this.__array.getLength();
		},

		/**
		 * проход по всем элементам массива
		 * @param {Function} func
		 * @param {Object} context
		 */
		forEach: function (func, context)
		{
			this.__array.forEach(func, context);
		},

		/**
		 * сырой массив сущностей. Перед измененением его нужно скопировать!
		 * @return {efwdDal.entity.AbstractEntity[]}
		 */
		toArray: function ()
		{
			return this.__array.toArray();
		},

		/**
		 * @param {efwdDal.entity.AbstractEntity} entity
		 * @private
		 */
		__processEntity: function (entity)
		{
			this.__listenersStore.addListener(entity, "removed", function (){ this.__array.remove(entity) }, this);
//			if (this.__dynamicFilter)
//				this.__listenersStore.addListener(entity, "changeSource", function(){
//					var filtered = this.__dynamicFilter(entity);
//					var index = this.__array.indexOf(entity);
//					if (filtered && index === -1)
//						this.__array.push(entity);
//					else if (!filtered && index !== -1)
//						this.__array.removeAt(index);
//				}, this);

			this.__listenersStore.removeObjectOn(entity, "removed");
		}
	},

	destruct: function ()
	{
		this.__listenersStore.dispose();
		this.fireEvent("disposed");
	}
});
