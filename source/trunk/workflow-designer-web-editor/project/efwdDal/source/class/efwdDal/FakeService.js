qx.Class.define("efwdDal.FakeService", {
	extend: qx.core.Object,

	events: {
		/**
		 * @param {efwdDal.saving.ChangeUnit[]} package
		 * @param {efwdDal.saving.ChangeUnit} changeUnit
		 * @param {Object} retrieve
		 * @param {String} retrieve.entityType
		 * @param {Number} retrieve.processId
		 * @param {Number} status
		 * @param {*} response
		 */
		"handleError": "qx.event.type.Data",

		/**
		 * @param {efwdDal.saving.ChangeUnit[]} package
		 * @param {efwdDal.saving.ChangeUnit} changeUnit
		 * @param {Object} retrieve
		 * @param {String} retrieve.entityType
		 * @param {Number} retrieve.processId
		 * @param {Number} status
		 * @param {*} response
		 */
		"unhandledError": "qx.event.type.Data"
	},

	properties: {
		sessionId: {
			init: null
		}
	},

	/**
	 * @param {efwdDal.Mapper} mapper
	 * @param {efwdCommon.Communication} communication
	 */
	construct: function (mapper, communication)
	{
	},

	members: {

		/**
		 * @param processId
		 * @param sessionId
		 * @return {library.Deferred}
		 */
		checkSession: function (processId, sessionId)
		{
			return library.Deferred.success();
		},

		/**
		 * @param processId
		 * @param {Boolean} async
		 * @return {library.Deferred}
		 */
		createSession: function (processId, async)
		{
			return library.Deferred.success("sessionid");
		},

		/**
		 * @param processId
		 * @param sessionId
		 * @param {Boolean} async
		 * @return {library.Deferred}
		 */
		deleteSession: function (processId, sessionId, async)
		{
			return library.Deferred.success();
		},

		/**
		 * @param {String} entityType
		 * @param [processId=null]
		 * @return {library.Deferred} Object[]
		 */
		getEntitiesResource: function (entityType, processId)
		{
			return library.Deferred.success([]);
		},

		/**
		 * @param {String} entityType
		 * @param processId
		 * @param [entityId=null]
		 * @return {library.Deferred} Object[]
		 */
		getEntityResource: function (entityType, processId, entityId)
		{
			var source = Object.merge({id: 1},
				efwdCommon.Helper.getEntitySkeleton(entityType, entityType));
			return library.Deferred.success(source);
		},

		/**
		 * @param {String} entityType
		 * @param [processId=null]
		 * @return {library.Deferred} Object[]
		 */
		retrieveEntities: function (entityType, processId)
		{
			return library.Deferred.success([]);
		},

		/**
		 * @param {String} entityType
		 * @param processId
		 * @param [entityId=null]
		 * @return {library.Deferred} Object[]
		 */
		retrieveEntity: function (entityType, processId, entityId)
		{
			var source = Object.merge({id: Math.round(Math.random() * 100)},
				efwdCommon.Helper.getEntitySkeleton(entityType, entityType));
			return library.Deferred.success(source);
		},

		/**
		 * @param {efwdDal.saving.ChangeUnit} changeUnit
		 * @return {library.Deferred} *
		 */
		sendChangeUnit: function (changeUnit)
		{
			return library.Deferred.success();
		},

		/**
		 * @param {efwdDal.saving.ChangeUnit[]} changeUnits
		 * @return {library.Deferred} Array {changeUnit: ...[, response: ...][, errorData: ...]}
		 */
		sendPackage: function (changeUnits)
		{
			return library.Deferred.success();
		}
	}
});
