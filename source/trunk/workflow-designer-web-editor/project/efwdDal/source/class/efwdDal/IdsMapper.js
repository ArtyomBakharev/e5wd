/*
 #ignore(efwdConfig.)
 #ignore(efwdConfig.*)
 */

/**
 * Класс заменяет все серверные ID в исходнике сущности на клиентские и наоборот. Клиент оперирует только клиенсткими
 * id, чтобы не зависеть от времени создания/удаления сущностей на сервере.
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdDal.IdsMapper", {

	extend: qx.core.Object,

	/**
	 * @param {efwdDal.repository.SessionRepository} sessionRepository
	 */
	construct: function (sessionRepository)
	{
		this.__sessionRepository = sessionRepository;

		this.__serverToClient = {};
		this.__clientToServer = {};
		this.__nextClientNum = null;
		this.__activityCategories = efwdConfig.efwdCommon.data.types.$activityCategories;
	},

	members: {

		/**
		 * Возвращает клиентский id для сущности типа type с переданным серверным id
		 * @param {String} type
		 * @param {*} serverId
		 * @return {String}
		 */
		getClientId: function (type, serverId)
		{
			return this.__mapToClientId(type, serverId);
		},

		/**
		 * Возвращает серверный id для сущности с переданным клиентским id
		 * @param {String} clientId
		 * @return {*}
		 */
		getServerId: function (clientId)
		{
			return this.__clientToServer[clientId];
		},

		/**
		 * Генерация клиентского id
		 * @return {String}
		 */
		generateClientId: function ()
		{
			if (this.__nextClientNum === null)
			{
				this.__nextClientNum = this.__sessionRepository.getSessionData("nextClientNum") || 1;
			}

			var id = "cid-" + this.__sessionRepository.getSessionId() + "-" + (this.__nextClientNum++);
			this.__sessionRepository.setSessionData("nextClientNum", this.__nextClientNum);

			return id;
		},

		/**
		 * Выполняет замену всех id в исходнике с серверных на клиентские
		 *
		 * @param {String} type
		 * @param {Object} source
		 */
		mapToClientIds: function (type, source)
		{
			this.__proceedMapping(this.__mapToClientId, type, source);
		},

		/**
		 * Выполняет замену всех клиентских id в исходнике source на серверные
		 *
		 * @param {String} type
		 * @param {Object} source
		 */
		mapToServerIds: function (type, source)
		{
			this.__proceedMapping(this.__mapToServerId, type, source);
		},

		/**
		 * Ставит клиентскому id в соотстветсвие серверный id
		 * @param {String} type
		 * @param {String} clientId
		 * @param {*} serverId
		 */
		setServerId: function (type, clientId, serverId)
		{
			this.unsetServerId(type, clientId);

			this.__clientToServer[clientId] = serverId;
			this.__serverToClient[this.__getUniqueServerId(type, serverId)] = clientId;
		},

		/**
		 * @param type
		 * @param clientId
		 */
		unsetServerId: function(type, clientId)
		{
			if (this.__clientToServer[clientId])
			{
				delete this.__serverToClient[this.__getUniqueServerId(type, this.__clientToServer[clientId])];
				delete this.__clientToServer[clientId];
			}
		},

		/**
		 * @param {String} type
		 * @param {*} serverId
		 * @return {String}
		 * @private
		 */
		__getUniqueServerId: function (type, serverId)
		{
			return type + "$$" + serverId;
		},

		/**
		 * @param {String} type
		 * @param {*} serverId
		 * @return {String}
		 * @private
		 */
		__mapToClientId: function (type, serverId)
		{
			var unique = this.__getUniqueServerId(type, serverId);
			if (!this.__serverToClient[unique])
			{
				this.setServerId(type, this.generateClientId(), serverId);
			}

			return this.__serverToClient[unique];
		},

		/**
		 * @param {String} type
		 * @param {String} clientId
		 * @return {*}
		 * @private
		 */
		__mapToServerId: function (type, clientId)
		{
			return clientId in this.__clientToServer ? this.__clientToServer[clientId] : clientId;
		},

		/**
		 * @param {Function} mapFunc
		 * @param {String} type
		 * @param {Object} source
		 * @private
		 */
		__proceedMapping: function (mapFunc, type, source)
		{
			source.id = mapFunc.call(this, type, source.id);

			if ("process" in source)
			{
				source.process = mapFunc.call(this, "process", source.process);
			}

			//map activities
			if (type in this.__activityCategories)
			{
				this.__activityCategories[type].forEach(function (category)
				{
					if (source[category])
					{
						source[category] = source[category].map(function (x)
						{
							return mapFunc.call(this, "activity", x)
						}, this);
					}
				}, this);
			}

			//map linked statuses
			if (type == "action" && "from_status_id" in source)
			{
				source.from_status_id = mapFunc.call(this, "status", source.from_status_id);
				source.to_status_id = mapFunc.call(this, "status", source.to_status_id);
			}

			//map activity type
			if (type == "activity" && source.$type == "remote_transaction_activity")
			{
				if ("action_id" in source)
				{
					source.action_id = mapFunc.call(this, "action", source.action_id);
					source.process_id = mapFunc.call(this, "process", source.process_id);
				}
			}

			//map process type
			if (type == "process")
			{
				if ("initial_status_id" in source)
				{
					source.initial_status_id = mapFunc.call(this, "status", source.initial_status_id);
				}
			}
		}
	}
});
