/*
 #ignore(XML.*)
 */

/**
 * Класс для отображения сущностей, полученных с сервера на клиентские и наоборот
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdDal.Mapper", {

	extend: qx.core.Object,

	/**
	 * @param {efwdCommon.data.Mapper} commonMapper
	 */
	construct: function (commonMapper)
	{
		this.__commonMapper = commonMapper;

		this.__objTree = new XML.ObjTree();
	},

	members: {

		/**
		 * @param {String} type тип сущности
		 * @param {String} xml сущность в XML
		 * @return {Object}
		 */
		mapToClientObject: function (type, xml)
		{
			var parsed = this.__objTree.parseXML(xml);
			var subType = Object.fetchFirstKey(parsed);
			delete parsed[subType]["-xmlns:xsi"];
			return this.__mapToClientObject(parsed[subType], type, subType);
		},

		/**
		 * Отобразить списко сущностей, полученный с сервера на массив клиентский сущностей
		 * @param {String} type тип сущности
		 * @param {String} xml список сущностей в XML
		 * @return {Object[]}
		 */
		mapToClientObjects: function (type, xml)
		{
			var tree = this.__objTree.parseXML(xml);
			var objects = [];

			for (var key in tree.items)
			{
				if (tree.items.hasOwnProperty(key))
				{
					var list = tree.items[key];

					if (typeof list == "object" && !Array.isArray(list))
					{
						list = [list];
					}

					if (Array.isArray(list))
					{
						objects.append(list.map(function (item)
						{
							//noinspection JSReferencingMutableVariableFromClosure
							return this.__mapToClientObject(item, type, key);
						}, this));
					}
				}
			}

			return objects;
		},

		/**
		 * Отобразить клиентскую сущности в XML для отправки на сервер
		 * @param {String} type
		 * @param {Object} object клиентская сущность
		 * @param {Boolean} [asXML=true]
		 * @return {String} XML-представление сущности
		 */
		mapToServerObject: function (type, object, asXML)
		{
			if (asXML === undefined)
			{
				asXML = true;
			}

			var subType = object.$type;
			var source = this.__commonMapper.mapToServerObject(type, object);
			if (asXML)
			{
				source["-xmlns:xsi"] = "http://www.w3.org/2001/XMLSchema-instance";
			}

			if ("access_list" in source)
			{
				source.access_list = this.__mapArrayToServer(source.access_list, "acess_entry");
			}

			if (["parametrized_property_local_activity", "invoke_method_activity", "set_property_activity"].indexOf(subType) !== -1)
			{
				source.property_changes = this.__mapArrayToServer(source.property_changes, "property_change");
			}

			var result = {};

			result[subType] = source;

			return asXML ? this.__objTree.writeXML(result) : source;
		},

		/**
		 * @param {Array} array
		 * @param {String} itemName
		 * @return {Array}
		 * @private
		 */
		__mapArrayToClient: function (array, itemName)
		{
			if (array)
			{
				array = array[itemName];
				if (!Array.isArray(array))
				{
					array = [array];
				}
			}

			return array || [];
		},

		/**
		 * @param {Array} array
		 * @param {String} itemName
		 * @return {Object}
		 * @private
		 */
		__mapArrayToServer: function (array, itemName)
		{
			if (!array || array.length == 0)
			{
				return null;
			}
			var obj = {};
			obj[itemName] = array.length == 1 ? array[0] : array;
			return obj;
		},

		__mapToClientObject: function (object, type, subType)
		{
			var item = this.__commonMapper.mapToClientObject(type, object);
			item.$type = subType;

			if ("access_list" in item)
			{
				item.access_list = this.__mapArrayToClient(item.access_list, "acess_entry");
			}

			if (["parametrized_property_local_activity", "invoke_method_activity", "set_property_activity"].indexOf(item.$type) !== -1)
			{
				item.property_changes = this.__mapArrayToClient(item.property_changes, "property_change");
			}

			return item;
		}
	}
});
