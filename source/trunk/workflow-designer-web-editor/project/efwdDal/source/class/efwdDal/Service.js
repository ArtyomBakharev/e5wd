qx.Class.define("efwdDal.Service", {
	extend: qx.core.Object,

	statics: {
		__METHOD_BY_CHANGE_TYPE: {}
	},

	events: {
		/**
		 * @param {efwdDal.saving.ChangeUnit[]} package
		 * @param {efwdDal.saving.ChangeUnit} changeUnit
		 * @param {Object} retrieve
		 * @param {String} retrieve.entityType
		 * @param {Number} retrieve.processId
		 * @param {Number} status
		 * @param {*} response
		 */
		"handleError": "qx.event.type.Data",

		/**
		 * @param {efwdDal.saving.ChangeUnit[]} package
		 * @param {efwdDal.saving.ChangeUnit} changeUnit
		 * @param {Object} retrieve
		 * @param {String} retrieve.entityType
		 * @param {Number} retrieve.processId
		 * @param {Number} status
		 * @param {*} response
		 */
		"unhandledError": "qx.event.type.Data"
	},

	properties: {
		sessionId: {
			init: null
		}
	},

	/**
	 * @param {efwdDal.Mapper} mapper
	 * @param {efwdCommon.Communication} communication
	 */
	construct: function (mapper, communication)
	{
		this.__mapper = mapper;
		this.__communication = communication;

		this.__operationNumber = 0;
		this.__objTree = new XML.ObjTree();
		this.__entitiesResources = {};
	},

	members: {

		/**
		 * @param processId
		 * @param sessionId
		 * @return {library.Deferred}
		 */
		checkSession: function (processId, sessionId)
		{
			return this.__sendQuery({
				url: "processes/" + processId + "/sessions/" + sessionId,
				method: "GET"
			});
		},

		/**
		 * @param processId
		 * @param {Boolean} async
		 * @return {library.Deferred}
		 */
		createSession: function (processId, async)
		{
			return this.__sendQuery({
				url: "processes/" + processId + "/sessions/0",
				method: "POST",
				async: async
			});
		},

		/**
		 * @param processId
		 * @param sessionId
		 * @param {Boolean} async
		 * @return {library.Deferred}
		 */
		deleteSession: function (processId, sessionId, async)
		{
			return this.__sendQuery({
				url: "processes/" + processId + "/sessions/" + sessionId,
				method: "DELETE",
				async: !!async
			});
		},

		/**
		 * @param {String} entityType
		 * @param [processId=null]
		 * @return {library.Deferred} Object[]
		 */
		getEntitiesResource: function (entityType, processId)
		{
			var entitiesResourceKey = entityType + "$" + (processId || 0);
			var resource;
			if (!(resource = this.__entitiesResources[entitiesResourceKey]))
			{
				resource = this.__entitiesResources[entitiesResourceKey] = new library.Deferred.Resource();
				resource.init(this.retrieveEntities(entityType, processId));
			}

			return resource.getResource();
		},

		/**
		 * @param {String} entityType
		 * @param processId
		 * @param [entityId=null]
		 * @return {library.Deferred} Object[]
		 */
		getEntityResource: function (entityType, processId, entityId)
		{
			var entitiesResourceKey = entityType + "$" + processId + "$" + (entityId || 0);
			var resource;
			if (!(resource = this.__entitiesResources[entitiesResourceKey]))
			{
				resource = this.__entitiesResources[entitiesResourceKey] = new library.Deferred.Resource();
				resource.init(this.retrieveEntity(entityType, processId, entityId));
			}

			return resource.getResource();
		},

		/**
		 * @param {String} entityType
		 * @param [processId=null]
		 * @return {library.Deferred} Object[]
		 */
		retrieveEntities: function (entityType, processId)
		{
			return this.__sendQuery(
				{
					url: "processes"
						+ (entityType != "process" ? "/" + processId + "/" + efwdCommon.Inflector.plural(entityType) : ""),
					method: "GET",
					asXml: true
				},
				this.__mapper.mapToClientObjects.bind(this.__mapper, entityType),
				{
					retrieve: { entityType: entityType, processId: processId }
				});
		},

		/**
		 * @param {String} entityType
		 * @param processId
		 * @param [entityId=null]
		 * @return {library.Deferred} Object[]
		 */
		retrieveEntity: function (entityType, processId, entityId)
		{
			return this.__sendQuery(
				{
					url: "processes/" + processId
						+ (entityType != "process" ? "/" + efwdCommon.Inflector.plural(entityType) + "/" + entityId : ""),
					method: "GET",
					asXml: true
				},
				this.__mapper.mapToClientObject.bind(this.__mapper, entityType),
				{
					retrieve: { entityType: entityType, processId: processId, entityId: entityId || processId }
				});
		},

		/**
		 * @param {efwdDal.saving.ChangeUnit} changeUnit
		 * @return {library.Deferred} *
		 */
		sendChangeUnit: function (changeUnit)
		{
			return this.__sendQuery(this.__getChangeUnitQuery(changeUnit), null, {changeUnit: changeUnit});
		},

		/**
		 * @param {efwdDal.saving.ChangeUnit[]} changeUnits
		 * @return {library.Deferred} Array {changeUnit: ...[, response: ...][, errorData: ...]}
		 */
		sendPackage: function (changeUnits)
		{
			var firstUnit = changeUnits[0];
			var processId = firstUnit.getEntity() instanceof efwdDal.entity.Process
				? firstUnit.getSource().id
				: firstUnit.getSource().process;

			var query = {
				url: "processes/" + processId + "/batch",
				method: "POST",
				data: this.__buildPackage(changeUnits)
			};
			if (this.getSessionId())
			{
				query.params = {session: this.getSessionId()};
			}

			return this.__sendQuery(query, null, {
				"package": changeUnits
			})
				.hand(function (response)
				{
					return JSON.parse(response).items.map(function (item, index)
					{
						var result = {
							status: item.status
						};

						if (item.status < 200 || item.status >= 400)
						{
							result.errorData = {
								changeUnit: changeUnits[index],
								response: item.response
							};
							this.__handleError(result.errorData);
						}
						else
						{
							result.changeUnit = changeUnits[index];
							if (result.changeUnit.getChangeType() == efwdDal.saving.ChangeUnit.CHANGE_TYPE_CREATE)
							{
								result.response = item.retId;
							}
							else
							{
								result.response = item.response;
							}
						}

						return result;
					}, this);
				}, library.Deferred.failure, this);
		},

		__buildPackage: function (changeUnits)
		{
			var $package = {};
			var batch = $package.batch_container = {};
			batch["-xmlns:xsi"] = "http://www.w3.org/2001/XMLSchema-instance";
			batch.operation = [];

			changeUnits.forEach(function (changeUnit)
			{
				var entity = changeUnit.getEntity();
				var source = changeUnit.getSource();

				var operation = {
					number: ++this.__operationNumber,
					method: efwdDal.Service.__METHOD_BY_CHANGE_TYPE[changeUnit.getChangeType()]
				};
				var element;
				if (changeUnit.getChangeType() == efwdDal.saving.ChangeUnit.CHANGE_TYPE_DELETE)
				{
					element = {id: source.id};
				}
				else
				{
					element = this.__mapper.mapToServerObject(entity.getEntityType(), source, false);

					if (changeUnit.getChangeType() == efwdDal.saving.ChangeUnit.CHANGE_TYPE_CREATE)
					{
						element.id = entity.getSource().getId();
					}
				}
				element["-xsi:type"] = source.$type;
				operation.element = element;
//				operation[source.$type] = element;

				batch.operation.push(operation);
			}, this);

			return this.__objTree.writeXML($package);
		},

		/**
		 * @param {library.Deferred} deferred
		 * @param {Object} errorData
		 * @private
		 */
		__failDeferredAndHandleError: function (deferred, errorData)
		{
			var handled = false;

			function errorHandled()
			{
				handled = true;
			}

			deferred.fail(errorData, errorHandled);
			(function ()
			{
				if (!handled)
				{
					this.__handleError(errorData);
				}
			}).defer(this);
		},

		/**
		 * @param {efwdDal.saving.ChangeUnit} changeUnit
		 * @return {Object}
		 * @private
		 */
		__getChangeUnitQuery: function (changeUnit)
		{
			var entity = changeUnit.getEntity();
			var source = changeUnit.getSource();

			var data = null;
			if (changeUnit.getChangeType() != efwdDal.saving.ChangeUnit.CHANGE_TYPE_DELETE)
			{
				data = this.__mapper.mapToServerObject(entity.getEntityType(), source);
			}

			var query = {
				url: (entity instanceof efwdDal.entity.Process ? "" : "processes/" + source.process + "/"),
				data: data
			};

			query.url += efwdCommon.Inflector.plural(entity.getEntityType());
			if (entity.hasSubtypes())
			{
				query.url += "/" + source.$type;
			}

			query.url += changeUnit.getChangeType() == efwdDal.saving.ChangeUnit.CHANGE_TYPE_CREATE ? "/0" : "/" + source.id;

			if (this.getSessionId())
			{
				query.params = {session: this.getSessionId()};
			}

			query.method = this.self(arguments).__METHOD_BY_CHANGE_TYPE[changeUnit.getChangeType()];

			return query;
		},

		__handleError: function (errorData)
		{
			if (this.fireDataEvent("handleError", errorData, null, true))
			{
				this.fireDataEvent("unhandledError", errorData);
			}
		},

		/**
		 * @param {Object} query
		 * @param {Function} [responseProcessor=null]
		 * @param {Object} [errorData=null]
		 * @return {library.Deferred}
		 * @private
		 */
		__sendQuery: function (query, responseProcessor, errorData)
		{
			if (!responseProcessor)
			{
				responseProcessor = Function.returnFirstArg;
			}
			var deferred = new library.Deferred();

			this.__communication.sendQuery(query, function (response)
			{
				deferred.call(responseProcessor(response));
			}, function (response, status)
			{
				this.__failDeferredAndHandleError(deferred,
					Object.merge({response: response, status: status}, errorData || {}));
				return true;
			}, this);

			return deferred;
		}
	},

	defer: function (statics)
	{
		statics.__METHOD_BY_CHANGE_TYPE[efwdDal.saving.ChangeUnit.CHANGE_TYPE_DELETE] = "DELETE";
		statics.__METHOD_BY_CHANGE_TYPE[efwdDal.saving.ChangeUnit.CHANGE_TYPE_CREATE] = "POST";
		statics.__METHOD_BY_CHANGE_TYPE[efwdDal.saving.ChangeUnit.CHANGE_TYPE_UPDATE] = "PUT";
	}
});
