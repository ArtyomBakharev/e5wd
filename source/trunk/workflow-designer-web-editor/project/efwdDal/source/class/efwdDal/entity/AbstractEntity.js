/**
 * Базовый класс для всех сущностей приложения
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdDal.entity.AbstractEntity", {

	extend: qx.core.Object,

	events: {
		/**
		 * объект был удалён
		 */
		"removed": "qx.event.type.Event",

		/**
		 * во время удаления объекта
		 */
		"removing": "qx.event.type.Event",

		/**
		 * объект был восстановлен после удаления
		 */
		"restored": "qx.event.type.Event"
	},

	properties: {
		/**
		 * исходный объект сущности
		 */
		source: {
			deferredInit: true,
			event: "changeSource"
		}
	},

	construct: function (source)
	{
		this.initSource(source);
		this.__collections = {};
		this.__collectionsLoaders = {};
	},

	members: {

		/**
		 * @param {String} name
		 * @return {efwdDal.EntitiesArray} именованная коллекция сущностей
		 */
		getCollection: function (name)
		{
			if (this.__collectionsLoaders[name])
			{
				this.__collectionsLoaders[name]();
				delete this.__collectionsLoaders[name];
			}

			return this.__collections[name];
		},

		/**
		 * @abstract
		 * @return {string} строковое представление типа сущности
		 */
		getEntityType: function ()
		{
			this.abstractMethod(arguments);
		},

		/**
		 * Имеет ли сущность подтипы, например статусы не имеют подтипов, а действия имеют два подтипа:
		 * status_change_action и no_status_action
		 * @return {Boolean}
		 */
		hasSubtypes: function()
		{
			this.abstractMethod(arguments);
		},

		/**
		 * @return {Boolean}
		 */
		isRemoved: function()
		{
			return this._removed;
		},

		toString: function()
		{
			return this.getEntityType() + " (" + this.getSource().getId() + ", " + this.getSource().getName() + ")";
		},

		/**
		 * удалить объект из системы
		 */
		remove: function ()
		{
			if (this._removed)
			{
				throw new Error("The entity is already removed");
			}

			this.fireEvent("removing");
			this._removed = true;
			this.fireEvent("removed");
		},

		/**
		 * Восстановление сущности после её удаления
		 */
		restore: function ()
		{
			if (!this._removed)
			{
				throw new Error("The entity isn't removed");
			}

			this._removed = false;
			this.fireEvent("restored");
		},

		/**
		 * Назначить коллекцию сущностей
		 * @param {String} name имя коллекции
		 * @param {efwdDal.EntitiesArray} collection
		 * @param {Function} [loader=null] функция, вызываемая для подгрузки сущностей в коллекцию (lazy load)
		 */
		setCollection: function (name, collection, loader)
		{
			this.__collections[name] = collection;
			if (loader)
			{
				this.__collectionsLoaders[name] = loader;
			}

			this.addListenerOnce("removed", function(){ collection.dispose() });
		}
	}

});
