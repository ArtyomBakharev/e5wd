/**
 * Действие
 * @extends {efwdDal.entity.AbstractEntity}
 * @extends {efwdDal.entity.AbstractEntity}
 */
qx.Class.define("efwdDal.entity.Action", {

	extend: efwdDal.entity.AbstractEntity,

	include: [efwdDal.entity.MActivitiesOwner, efwdDal.entity.MProcessChild],

	/**
	 * @param {efwdCommon.data.types.Action} action
	 * @param {efwdDal.entity.Status} [source=null]
	 * @param {efwdDal.entity.Status} [dest=null]
	 */
	construct: function (action, source, dest)
	{
		this.__actionListenersStore = new library.ListenersStore();

		source === undefined && (source = null);
		dest === undefined && (dest = null);
		if (Boolean.xor(source === null, dest === null))
		{
			throw new Error("the source and the dest statuses should be passed together");
		}

		this.base(arguments, action);
		this.__sourceStatus = source;
		this.__destStatus = dest;

		this.__prepareAction();
	},

	members: {

		/**
		 * @return {efwdDal.entity.Status} статус от которого идёт действие
		 */
		getDestStatus: function ()
		{
			return this.__destStatus;
		},

		/**
		 * @return {string}
		 */
		getEntityType: function ()
		{
			return "action";
		},

		/**
		 * @return {efwdDal.entity.Status} статус, к которому идёт действие
		 */
		getSourceStatus: function ()
		{
			return this.__sourceStatus;
		},

		/**
		 * @return {Boolean}
		 */
		hasSubtypes: function()
		{
			return true;
		},

		/**
		 * true - если действие связывает статусы
		 * @return {Boolean}
		 */
		isStatusesLess: function ()
		{
			return !this.__sourceStatus;
		},

		remove: function ()
		{
			this.__actionListenersStore.removeAllListeners();
			this.base(arguments);
		},

		restore: function ()
		{
			this.__prepareAction();
			this.base(arguments);
		},

		/**
		 * @private
		 */
		__prepareAction: function ()
		{
			if (this.__sourceStatus)
			{
				this.__actionListenersStore.addListener(this.__sourceStatus, "removing", function (){ this.remove() }, this);
			}

			if (this.__destStatus)
			{
				this.__actionListenersStore.addListener(this.__destStatus, "removing", function (){ this.remove() }, this);
			}
		}
	}
});
