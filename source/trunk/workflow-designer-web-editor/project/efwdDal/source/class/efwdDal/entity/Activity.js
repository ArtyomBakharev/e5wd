/**
 * Активность
 * @extends {efwdDal.entity.AbstractEntity}
 */
qx.Class.define("efwdDal.entity.Activity", {

	extend: efwdDal.entity.AbstractEntity,

	include: efwdDal.entity.MProcessChild,

	members: {
		/**
		 * @return {string}
		 */
		getEntityType: function ()
		{
			return "activity";
		},

		/**
		 * @return {Boolean}
		 */
		hasSubtypes: function()
		{
			return true;
		}
	}
});
