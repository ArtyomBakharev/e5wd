/**
 * Миксин, примешивается к сущности, которая может владеть активностями
 */
qx.Mixin.define("efwdDal.entity.MActivitiesOwner", {

	members: {

		/**
		 * Добавить активность сущности
		 *
		 * @param {String} category
		 * @param {efwdDal.entity.Activity} activity
		 */
		addActivity: function (category, activity)
		{
			var activities = this.getSource().get(category);
			activities.push(activity.getSource().getId());

			this.__processActivity(activity, category);

			this.getActivities(category).addEntity(activity);
		},

		/**
		 * Если category=null, то возвращает хэш ключами, которого являются категории активностей сущности, а значениями массивы активностей.
		 * Если передана категория, то возвращает массив активностей сущности данной категории.
		 *
		 * @param {String} [category=null]
		 * @return {efwdDal.EntitiesArray|Object}
		 */
		getActivities: function (category)
		{
			if (!this.__activities)
			{
				throw new Error("Entity doesn't contain activities");
			}

			return category ? this.__activities[category] : this.__activities;
		},

		/**
		 * Инициализирует массивы активностей сущности
		 *
		 * @param {Object} activities
		 */
		initializeActivities: function (activities)
		{
			if (this.__activities || this._removed)
			{
				throw new Error("Can't initialize activities");
			}

			this.__activities = activities;
			Object.forEach(this.__activities, function (activities, category)
			{
				activities.addListener("addEntity", function (e){ this.__processActivity(e.getData().entity, category) }, this);
				activities.forEach(function (x){ this.__processActivity(x, category) }, this);
			}, this);

			this.addListenerOnce("removed", function ()
			{
				Object.forEach(activities, function (x){ x.dispose() });
				this.__activities = null;
			}, this);
		},

		/**
		 * Удалить активность из сущности
		 *
		 * @param {String} category
		 * @param {efwdDal.entity.Activity} activity
		 * @param {Boolean} [implicitly=false] неявное удаление, при котором не происходит удаление id активности из исходника сущности
		 */
		removeActivity: function (category, activity, implicitly)
		{
			this.getSource().get(category).remove(activity.getSource().getId());
			if (!implicitly)
			{
				this.__activities[category].removeEntity(activity);
			}
			this.__activitiesListenersStore.removeObject(activity);
		},

		/**
		 * @private
		 */
		__processActivity: function (activity, category)
		{
			if (!this.__activitiesListenersStore)
			{
				this.__activitiesListenersStore = new library.ListenersStore();
				this.addListener("removed", function (){ this.__activitiesListenersStore.removeAllListeners() }, this);
			}

			this.__activitiesListenersStore.addListener(activity, "removing", function ()
			{
				this.removeActivity(category, activity, true);
			}, this);
			this.__activitiesListenersStore.removeObjectOn(activity, "removed");
		}
	}
});
