/**
 * Миксин примешивается к классам наследованным от {@link efwdDal.entity.AbstractEntity},
 * которые представляют сущности дочерние к процессу
 */
qx.Mixin.define("efwdDal.entity.MProcessChild", {
	members: {

		/**
		 * @return {efwdDal.entity.Process} Родительский процесс
		 */
		getProcess: function()
		{
			return this.__process;
		},

		/**
		 * @param {efwdDal.entity.Process} process
		 */
		initializeProcess: function(process)
		{
			this.__process = process;
			if (this._onInitializeProcess)
			{
				this._onInitializeProcess(process);
			}
		}
	}
});
