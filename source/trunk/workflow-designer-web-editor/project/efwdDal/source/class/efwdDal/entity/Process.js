/**
 * Процесс
 * @extends {efwdDal.entity.AbstractEntity}
 */
qx.Class.define("efwdDal.entity.Process", {

	extend: efwdDal.entity.AbstractEntity,

	members: {

		/**
		 * @return {efwdDal.EntitiesArray} массив действий процесс
		 */
		getActions: function()
		{
			return this.getCollection("action");
		},

		/**
		 * @return {efwdDal.EntitiesArray} массив активностей процесса
		 */
		getActivities: function()
		{
			return this.getCollection("activity");
		},

		/**
		 * @return {string}
		 */
		getEntityType: function ()
		{
			return "process";
		},

		/**
		 * @return {efwdDal.EntitiesArray} массив статусов процесса
		 */
		getStatuses: function()
		{
			return this.getCollection("status");
		},

		/**
		 * @return {efwdDal.EntitiesArray} массив действий типа status_change_action
		 * @cacheResult
		 */
		getStatusChangeActions: function()
		{
			return efwdDal.EntitiesArray.filterArray(this.getActions(), function(x){ return !x.isStatusesLess() });
		},

		/**
		 * @return {efwdDal.EntitiesArray} массив действий типа no_status_action
		 * @cacheResult
		 */
		getNoStatusActions: function()
		{
			return efwdDal.EntitiesArray.filterArray(this.getActions(), function(x){ return x.isStatusesLess() });
		},

		/**
		 * @return {Boolean}
		 */
		hasSubtypes: function()
		{
			return false;
		}
	},

	defer: function(statics, members)
	{
		members.getStatusChangeActions = members.getStatusChangeActions.cacheResult();
		members.getNoStatusActions = members.getNoStatusActions.cacheResult();
	}
});
