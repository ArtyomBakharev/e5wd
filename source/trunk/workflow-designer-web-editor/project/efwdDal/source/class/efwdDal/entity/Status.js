/**
 * Статус
 * @extends {efwdDal.entity.AbstractEntity}
 */
qx.Class.define("efwdDal.entity.Status", {

	extend: efwdDal.entity.AbstractEntity,

	include: [efwdDal.entity.MActivitiesOwner, efwdDal.entity.MProcessChild],

	properties: {
		initialStatus: {
			init: false,
			check: "Boolean",
			event: "changeInitialStatus"
		}
	},

	members: {

		/**
		 * @return {string}
		 */
		getEntityType: function ()
		{
			return "status";
		},

		/**
		 * @return {Boolean}
		 */
		hasSubtypes: function ()
		{
			return false;
		},

		/**
		 * @param {efwdDal.entity.Process} process
		 * @protected
		 */
		_onInitializeProcess: function (process)
		{
			var bindingId;
			var $this = this;

			function bindInitialStatusProperty()
			{
				bindingId = process.bind("source.initial_status_id", $this, "initialStatus", {converter: function (x)
				{
					return $this.getSource().getId() == x;
				}});
			}

			bindInitialStatusProperty();

			this.addListener("removing", function (){
				process.removeBinding(bindingId);
				if (this.isInitialStatus())
				{
					process.getSource().setInitial_status_id(null);
				}
			}, this);
			this.addListener("restored", bindInitialStatusProperty);

			//reverse binding
			this.bind("initialStatus", process, "source.initial_status_id", {converter: function (x)
			{
				return x ? $this.getSource().getId() : process.getSource().getInitial_status_id()
			}});
		}
	}
});
