/**
 * Репозиторий, выбирающий сущности дочерние к процессу, добавляет к процессу соответствующию коллекцию
 */
qx.Class.define("efwdDal.repository.AbstractProcessChildRepository", {

	type: "abstract",
	extend: qx.core.Object,

	/**
	 * @param {efwdDal.Context} context
	 * @param {efwdDal.IdsMapper} idsMapper
	 */
	construct: function (context, idsMapper)
	{
		this.__context = context;
		this.__idsMapper = idsMapper;

		this.__entitiesArrays = {};
		this.__entitiesArrayLoadingFlags = {};

		this.__entityType = this._getEntityClass().prototype.getEntityType();
	},

	members: {

		initialize: function ()
		{
			this.__context.addListener("preEntityAttach", function (e)
			{
				var entity = e.getData().entity;
				if (entity instanceof efwdDal.entity.Process)
				{
					this.__initializeProcessChildren(entity);
				}
			}, this);
		},

		/**
		 * @param {efwdDal.entity.Process} process
		 * @return {efwdDal.EntitiesArray} массив сущностей
		 */
		getEntities: function (process)
		{
			var id = process.getSource().getId();
			var array = this.__getEntitiesArray(process);

			if (!this.__entitiesArrayLoadingFlags[id])
			{
				this.__entitiesArrayLoadingFlags[id] = true;

				this._createEntitiesFromServer(process, this.__idsMapper.getServerId(id))
					.next(function (){ array.setLoaded(true) }, this);
			}

			return array;
		},

		/**
		 * Метод для создания сущности
		 *
		 * @param {efwdDal.entity.Process} process
		 * @param {Object} source
		 * @param {Boolean} fromServer сущность была подгружена с сервера
		 * @param {Array} [addArgs=null] аргументы, которые нужно добавить к конструктору сущности помимо source
		 * @return {efwdDal.entity.AbstractEntity}
		 * @protected
		 */
		_createEntity: function (process, source, fromServer, addArgs)
		{
			if (!fromServer)
			{
				source.process = process.getSource().getId();
			}

			var entity = Object.construct(this._getEntityClass(),
				[this.__context.generateSource(this.__entityType, source, fromServer)].concat(addArgs || []));
			entity.initializeProcess(process);
			this.__context.attachEntity(entity, fromServer);

			return entity;
		},

		/**
		 * Получить с сервера и создать сущности
		 * @param {efwdDal.entity.Process} process
		 * @param {*} processServerId
		 * @protected
		 * @virtual
		 */
		_createEntitiesFromServer: function (process, processServerId)
		{
			this.abstractMethod(arguments);
		},

		/**
		 * @return {Function} Класс сущностей с которым работает репозиторий
		 * @protected
		 * @virtual
		 */
		_getEntityClass: function ()
		{
			this.abstractMethod(arguments);
		},

		/**
		 * @param {efwdDal.entity.Process} process
		 * @return {efwdDal.EntitiesArray}
		 * @private
		 */
		__getEntitiesArray: function (process)
		{
			var entityClass = this._getEntityClass();
			var id = process.getSource().getId();
			if (!this.__entitiesArrays[id])
			{
				this.__entitiesArrays[id] = new efwdDal.EntitiesArray(this.__context, function (x)
				{
					return x instanceof entityClass && x.getSource().getProcess() == id
				});
			}

			return this.__entitiesArrays[id];
		},

		/**
		 * @param {efwdDal.entity.Process} process
		 * @private
		 * @private
		 */
		__initializeProcessChildren: function (process)
		{
			process.setCollection(this.__entityType, this.__getEntitiesArray(process),
				this.getEntities.bind(this, process));
		}
	},

	defer: function (statics, members)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}
});
