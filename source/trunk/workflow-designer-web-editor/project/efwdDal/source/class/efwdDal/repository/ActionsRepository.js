/**
 * репозиторий хранящий и создающий действия
 */
qx.Class.define("efwdDal.repository.ActionsRepository", {

	extend: efwdDal.repository.AbstractProcessChildRepository,

	/**
	 * @param {efwdDal.Context} context
	 * @param {efwdDal.Service} service
	 * @param {efwdDal.IdsMapper} idsMapper
	 */
	construct: function (context, service, idsMapper)
	{
		this.base(arguments, context, idsMapper);

		this.__service = service;
		this.__idsMapper = idsMapper;
	},

	members: {

		/**
		 * @param {efwdDal.entity.Process} process
		 * @param {efwdCommon.data.types.Action} [action=null]
		 * @param {efwdDal.entity.Status} [source=null]
		 * @param {efwdDal.entity.Status} [dest=null]
		 * @return {efwdDal.entity.Action}
		 */
		createAction: function (process, action, source, dest)
		{
			source === undefined && (source = null);
			dest === undefined && (dest = null);
			var fromServer = !!action;

			if (!action)
			{
				action = {
					$type: source ? "status_change_action" : "no_status_action",
					data_type: process.getSource().getData_type()
				};
			}

			if (source && !action.from_status_id)
			{
				action.from_status_id = source.getSource().getId();
			}

			if (dest && !action.to_status_id)
			{
				action.to_status_id = dest.getSource().getId();
			}

			return this._createEntity(process, action, fromServer, [source, dest]);
		},

		/**
		 * Возвращает список действий связанных с переданным статусом
		 *
		 * @param {efwdDal.entity.Status} status
		 * @return {efwdDal.entity.Action[]}
		 */
		getActionsByStatus: function (status)
		{
			return this.getEntities(status.getProcess()).toArray().filter(function (action)
			{
				return !action.isStatusesLess() && (action.getSourceStatus() == status || action.getDestStatus() == status);
			});
		},

		/**
		 * Возвращает список действий, переводящих статус sourceStatus в статус destStatus
		 *
		 * @param {efwdDal.entity.Status} sourceStatus
		 * @param {efwdDal.entity.Status} destStatus
		 * @param {Boolean} [includeReversed=false] учитывать также действия переводящие destStatus в sourceStatus
		 * @return {efwdDal.entity.Action[]}
		 */
		getActionsByStatuses: function (sourceStatus, destStatus, includeReversed)
		{
			return this.getEntities(sourceStatus.getProcess()).toArray().filter(function (action)
			{
				return !action.isStatusesLess()
					&& (
						(action.getSourceStatus() == sourceStatus && action.getDestStatus() == destStatus)
						|| (includeReversed && action.getSourceStatus() == destStatus && action.getDestStatus() == sourceStatus)
					);
			});
		},

		/**
		 * @protected
		 */
		_createEntitiesFromServer: function (process, processServerId)
		{
			return library.Deferred.parallel([
				process.getStatuses().deferred(),
				this.__service.getEntitiesResource("action", processServerId)
			])
				.next(function (statuses, actions)
				{
					statuses = statuses.toArray().buildIndex(function (status){ return status.getSource().getId() });

					actions.forEach(function (actionSource)
					{
						if ("from_status_id" in actionSource)
						{
							var fromStatus =
								statuses[this.__idsMapper.getClientId("status", actionSource.from_status_id) || 0];
							var toStatus =
								statuses[this.__idsMapper.getClientId("status", actionSource.to_status_id) || 0];

							if (fromStatus && toStatus)
							{
								this.createAction(process, actionSource, fromStatus, toStatus);
							}
							else
							//todo: take it off
							{
								qx.log.Logger.warn(this,
									"Целостность нарушена! Отсутствует связанный с действием статус! id действия - " + actionSource.id);
							}
						}
						else
						{
							this.createAction(process, actionSource);
						}
					}, this);
				}, this);
		},

		/**
		 * @return {Function}
		 * @private
		 */
		_getEntityClass: function ()
		{
			return efwdDal.entity.Action;
		}
	}
});
