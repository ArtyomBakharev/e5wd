/*
 #ignore(efwdConfig)
 #ignore(efwdConfig.*)
 */

/**
 * Репозиторий активностей. Автоматически добавляет все массивы активностей к каждому владельцу активностей
 * @extends {efwdDal.repository.AbstractProcessChildRepository}
 */
qx.Class.define("efwdDal.repository.ActivitiesRepository", {

	extend: efwdDal.repository.AbstractProcessChildRepository,

	/**
	 * @param {efwdDal.Context} context
	 * @param {efwdDal.Service} service
	 * @param {efwdDal.IdsMapper} idsMapper
	 */
	construct: function (context, service, idsMapper)
	{
		this.base(arguments, context, idsMapper);

		this.__context = context;
		this.__service = service;
	},

	members: {

		initialize: function ()
		{
			this.base(arguments);

			this.__context.addListener("preEntityAttach", function (e)
			{
				var entity = e.getData().entity;
				if (entity.includesMixin(efwdDal.entity.MActivitiesOwner))
				{
					this.__initializeActivitiesFor(entity);
				}
			}, this);
		},

		/**
		 * @param {efwdCommon.data.types.Activity|String} source исходник новой активности либо строкой тип активности
		 * @return {efwdDal.entity.Activity}
		 */
		createActivity: function (process, source)
		{
			var fromServer = !qx.lang.Type.isString(source);
			return this._createEntity(process, fromServer ? source : {$type: source}, fromServer);
		},

		/**
		 * Возвращает массив сущностей, которые владеют переданной активностью
		 * @param {efwdDal.entity.Activity} activity
		 * @return {Object[]} {owner: efwdDal.entity.MActivitiesOwner, category: String}[]
		 */
		getOwnersOfActivity: function (activity)
		{
			var array = activity.getProcess().getStatuses().toArray().concat(activity.getProcess().getActions().toArray());
			var owners = [];
			array.forEach(function (owner)
			{
				var activitiesMap = owner.getActivities();
				Object.forEach(activitiesMap, function (activities, category)
				{
					if (activities.toArray().indexOf(activity) !== -1)
					{
						owners.push({owner: owner, category: category});
					}
				});
			});

			return owners;
		},

		/**
		 * @protected
		 */
		_createEntitiesFromServer: function (process, processServerId)
		{
			return this.__queryActivities(processServerId)
				.next(function (activities)
				{
					activities.forEach(function (activity)
					{
						this.createActivity(process, activity);
					}, this);
				}, this);
		},

		/**
		 * @return {Function}
		 * @private
		 */
		_getEntityClass: function ()
		{
			return efwdDal.entity.Activity;
		},

		/**
		 * @param {efwdDal.entity.MActivitiesOwner} owner
		 * @private
		 */
		__initializeActivitiesFor: function (owner)
		{
			var categories = efwdConfig.efwdCommon.data.types.$activityCategories[owner.getEntityType()];
			var activities = {};
			categories.forEach(function (category)
			{
				activities[category] = efwdDal.EntitiesArray.filterArray(owner.getProcess().getActivities(),
					function (entity)
					{
						var activitiesList = owner.getSource().get(category);
						return activitiesList && activitiesList.indexOf(entity.getSource().getId()) !== -1;
					});
			}, this);

			owner.initializeActivities(activities);

			owner.getProcess().getActivities();
		},

		/**
		 * @private
		 */
		__queryActivities: function (processId)
		{
			return this.__service.getEntitiesResource("activity", processId).next(function (activities)
			{
				return activities.filter(function (x)
				{
					return efwdConfig.efwdCommon.data.types.$activityTypes.indexOf(x.$type) !== -1
				})
			});
		}
	},

	defer: function (statics, members)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}
});
