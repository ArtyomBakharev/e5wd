/**
 * репозиторий хранящий процесс
 */
qx.Class.define("efwdDal.repository.ProcessesRepository", {

	extend: qx.core.Object,

	/**
	 * @param {efwdDal.Context} context
	 * @param {efwdDal.Service} service
	 * @param {efwdDal.IdsMapper} idsMapper
	 */
	construct: function (context, service, idsMapper)
	{
		this.__context = context;
		this.__service = service;
		this.__idsMapper = idsMapper;

		this.__processResource = {};
	},

	members: {

		/**
		 * @param {Object|Boolean} source source or is db repository
		 */
		createProcess: function (source)
		{
			var fromServer = !qx.lang.Type.isBoolean(source);
			if (!fromServer)
			{
				source = {$type: "process", repository: source ? 1 : 0};
			}

			var entity = new efwdDal.entity.Process(this.__context.generateSource("process", source, fromServer));
			this.__context.attachEntity(entity, fromServer);
			return entity;
		},

		/**
		 * @param {*} processId
		 * @param {Boolean} [isServerId=false]
		 * @return {Deferred} multinext
		 */
		getProcessById: function (processId, isServerId)
		{
			var serverId = isServerId ? processId : this.__idsMapper.getServerId(processId);
			var clientId = isServerId ? this.__idsMapper.getClientId("process", processId) : processId;

			if (this.__processesArray)
			{
				return this.__processesArray.deferred().next(function (processes)
				{
					return processes.toArray().filter(function (x){ return x.getSource().getId() == clientId })[0];
				});
			}

			if (this.__processResource[serverId])
			{
				return this.__processResource[serverId].getResource();
			}

			var processResource = this.__processResource[serverId] = new library.Deferred.Resource();
			processResource.init(
				this.__service.getEntityResource("process", serverId)
					.next(function (process)
					{
						return this.createProcess(process);
					}, this)
			);

			return processResource.getResource();
		},

		/**
		 * @return {efwdDal.EntitiesArray}
		 * @cacheResult
		 */
		getProcesses: function ()
		{
			this.__processesArray = new efwdDal.EntitiesArray(this.__context, function (x)
			{
				return x instanceof efwdDal.entity.Process
			});

			this.__service.getEntitiesResource("process")
				.next(function (processes)
				{
					processes.forEach(function (source)
					{
						//если процесс был получен ранее отдельно
						var retrievedProcess = this.__processResource[source.id];
						retrievedProcess = retrievedProcess && retrievedProcess.getResource().syncResult();
						return retrievedProcess || this.createProcess(source);
					}, this);

					this.__processesArray.setLoaded(true);

				}, this);

			return this.__processesArray;
		}
	},

	defer: function(statics, members)
	{
		members.getProcesses = members.getProcesses.cacheResult();
	}
});
