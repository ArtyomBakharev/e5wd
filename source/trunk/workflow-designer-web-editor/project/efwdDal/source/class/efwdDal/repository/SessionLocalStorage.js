/*
 #ignore(AppTools)
 #ignore(AppTools.*)
 */

/**
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdDal.repository.SessionLocalStorage", {

	extend: qx.core.Object,

	statics: {
		__SESSION_QUERY_TIMEOUT: 4000
	},

	construct: function ()
	{
		/** @type qx.bom.storage.Local */
		this.__storage = qx.bom.storage.Local.getInstance();
	},

	members: {

		/**
		 * @param {*} processId
		 * @return {library.Deferred}
		 */
		getFreeSession: function (processId)
		{
			if (!qx.bom.Event.supportsEvent(window, "storage") && !qx.bom.Event.supportsEvent(document, "storage"))
			{
				return library.Deferred.failure();
			}

			var deferred = new library.Deferred();
			var key = this.__getProcessKey(processId);
			var sessionId = this.__storage.getItem(key);

			if (sessionId)
			{
//				console.log("session exists - " + sessionId);
				var $this = this;

				function clear()
				{
					if (listenerId)
					{
						$this.__storage.removeListenerById(listenerId);
						listenerId = null;
					}
					if (timeout)
					{
						clearTimeout(timeout);
						timeout = null;
					}
					$this.__storage.removeItem(queryKey);
				}

				//запрашиваем у других вкладок браузера: свободна ли сессия?
				var queryKey = "message/checkProcessSession/" + sessionId + "?" + AppTools.getUniqueString();
				var listenerId = this.__storage.addListener("storage", function (e)
				{
//					console.timeEnd("query answer");
					var data = e.getData();
					if (data.key.startsWith(queryKey) && data.newValue == "1")
					{
						//сессия занята
						clear();
//						console.log("session is locked - " + sessionId);
						deferred.fail(sessionId);
					}
				}, this);
//				console.log("send query - " + queryKey);
				this.__storage.setItem(queryKey, 0);

//				console.time("query answer");
				//за отведённое время ни один клиент не откликнулся, считаем сессию свободной
				var timeout = setTimeout(function ()
				{
					clear();
//					console.log("session is free - " + sessionId);
					$this.setSession(processId, sessionId);
					deferred.call(sessionId);
				}, this.self(arguments).__SESSION_QUERY_TIMEOUT);
			}
			else
			{
//				console.log("session does not exist");
				deferred.fail();
			}

			return deferred;
		},

		getSessionData: function(sessionId, key)
		{
			return (this.__storage.getItem("session-" + sessionId + "-data") || {})[key];
		},

		/**
		 * @param {*} processId
		 * @param {*} sessionId
		 */
		setSession: function (processId, sessionId)
		{
			var key = this.__getProcessKey(processId);
//			console.log("set session id - " + sessionId);
			this.__storage.setItem(key, sessionId);

			//когда редактор в другой вкладке запрашивает свободна ли данная сессия отсылаем ответ, что сессия занята
			this.__storage.addListener("storage", function (e)
			{
				var data = e.getData();
				if (data.key.startsWith("message/checkProcessSession/" + sessionId + "?") && data.newValue == "0")
				{
//					console.log("my session queried - " + sessionId);
					this.__storage.setItem.defer(this.__storage, data.key, 1);
				}
			}, this);
		},

		setSessionData: function(sessionId, key, value)
		{
			var data = this.__storage.getItem("session-" + sessionId + "-data") || {};
			data[key] = value;
			this.__storage.setItem("session-" + sessionId + "-data", data);
		},

		/**
		 * @param {*} processId
		 */
		deleteSession: function (processId, sessionId)
		{
			var key = this.__getProcessKey(processId);
			if (this.__storage.getItem(key) == sessionId)
			{
				this.__storage.removeItem(key);
				this.__storage.removeItem("session-" + sessionId + "-data");
			}
		},

		/**
		 * @param {*} processId
		 * @private
		 */
		__getProcessKey: function (processId)
		{
			return "processSession-" + processId;
		}

	}
});
