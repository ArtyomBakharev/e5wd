/*
 #ignore(efwdConfig)
 #ignore(efwdConfig.*)
 */

/**
 * Репозиторий для создания, поддержания и уничтожения сессии. Сессии используются для запрета одновременного редактирования
 * одной диаграммы несколькими пользователями. Если при получении сессии на определённый процесс выясняется, что сессия
 * для него уже получена, то редактор открывается в режиме "только чтение"
 */
qx.Class.define("efwdDal.repository.SessionRepository", {

	extend: qx.core.Object,

	/**
	 * @param {efwdDal.Service} service
	 * @param {efwdDal.repository.SessionLocalStorage} sessionLocalStorage
	 */
	construct: function (service, sessionLocalStorage)
	{
		this.__service = service;
		this.__sessionLocalStorage = sessionLocalStorage;

		this.__sessionId = null;
	},

	members: {

		/**
		 * @param {*} currentProcessId
		 */
		initialize: function (currentProcessId)
		{
			this.__currentProcessId = currentProcessId;
		},

		/**
		 * Создать сессию и получить её id
		 * @param {Boolean} [async=true]
		 * @return {library.Deferred}
		 */
		acquireSessionId: function (async)
		{
			return this.__sessionLocalStorage.getFreeSession(this.__currentProcessId)
				.hand(function (success, sessionId)
				{
					if (sessionId)
					{
						if (success)
						{
							var deferred = new library.Deferred();
							this.__checkSession(sessionId, function ()
							{
								this.__sessionId = sessionId;
								this.__service.setSessionId(sessionId);
								this.__pingSession();
								deferred.call();
							}, function ()
							{
								deferred.fail();
								return true;
							}, this);
							return deferred;
						}
						else
						{
							return library.Deferred.failure();
						}
					}
					else
					{
						return this.__createSession(async).hand(function (sessionId)
						{
							this.__sessionId = sessionId;
							this.__service.setSessionId(sessionId);
							this.__sessionLocalStorage.setSession(this.__currentProcessId, sessionId);
							this.__pingSession();
						}, library.Deferred.failure, this);
					}
				}, true, this);
		},

		/**
		 * Удалить сессию
		 * sync call!
		 * return {library.Deferred}
		 */
		deleteSession: function ()
		{
			this.__sessionLocalStorage.deleteSession(this.__currentProcessId, this.__sessionId);
			return this.__service.deleteSession(this.__currentProcessId, this.__sessionId, false);
		},

		getSessionData: function (key)
		{
			return this.isSessionIdAcquired() ? this.__sessionLocalStorage.getSessionData(this.__sessionId, key) : null;
		},

		/**
		 * id текущей сесси
		 * @return {*}
		 */
		getSessionId: function ()
		{
			return this.__sessionId;
		},

		/**
		 * был ли получен id сессии
		 * @return {Boolean}
		 */
		isSessionIdAcquired: function ()
		{
			return this.__sessionId !== null;
		},

		setSessionData: function (key, value)
		{
			if (this.isSessionIdAcquired())
			{
				this.__sessionLocalStorage.setSessionData(this.__sessionId, key, value);
			}
		},

		/**
		 * @param sessionId
		 * @private
		 */
		__checkSession: function (sessionId, callback, errback, context)
		{
			return this.__service.checkSession(this.__currentProcessId, sessionId).hand(function ()
			{
				callback && callback.call(context);
			}, function (data, errorHandled)
			{
				if (errback && errback.call(context))
				{
					errorHandled();
				}
			});
		},

		/**
		 * @param {Boolean} [async=true]
		 * @return {library.Deferred}
		 * @private
		 */
		__createSession: function (async)
		{
			return this.__service.createSession(this.__currentProcessId, async === undefined || async)
				.hand(library.Deferred.success, function (errorData, errorHandled)
				{
					if (errorData.status == efwdConfig.efwdCommon.Communication.codes.diagramLocked)
					{
						errorHandled();
					}

					return library.Deferred.failure();
				});
		},

		/**
		 * @private
		 */
		__pingSession: function ()
		{
			var timeManager = qx.util.TimerManager.getInstance();

			var timerId = timeManager.start(
				function ()
				{
					this.__checkSession(this.__sessionId,
						null,
						function ()
						{
							timeManager.stop(timerId);
						}, this);
				},
				efwdConfig.efwdDal.repository.SessionRepository.pingInterval, this, null,
				efwdConfig.efwdDal.repository.SessionRepository.pingInterval);
		}
	}
});
