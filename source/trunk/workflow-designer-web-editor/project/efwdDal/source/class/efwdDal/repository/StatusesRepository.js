/**
 * репозиторий для хранения, создания статусов
 * @extends {efwdDal.repository.AbstractProcessChildRepository}
 */
qx.Class.define("efwdDal.repository.StatusesRepository", {

	extend: efwdDal.repository.AbstractProcessChildRepository,

	/**
	 * @param {efwdDal.Context} context
	 * @param {efwdDal.Service} service
	 * @param {efwdDal.IdsMapper} idsMapper
	 */
	construct: function (context, service, idsMapper)
	{
		this.base(arguments, context, idsMapper);
		this.__service = service;
	},

	members: {

		/**
		 * Создаёт статус на основе исходника, либо генерирует новый исходник
		 * @param {efwdDal.entity.Process} process
		 * @param {efwdCommon.data.types.Status} [source=null]
		 * @return {efwdDal.entity.Status}
		 */
		createStatus: function (process, source)
		{
			return this._createEntity(process, source || {
				$type: "status",
				data_type: process.getSource().getData_type()
			}, !!source);
		},

		/**
		 * @protected
		 */
		_createEntitiesFromServer: function (process, processServerId)
		{
			return this.__service.getEntitiesResource("status", processServerId)
				.next(function (statuses)
				{
					statuses.forEach(function (status)
					{
						this.createStatus(process, status);
					}, this);
				}, this);
		},

		/**
		 * @return {Function}
		 * @private
		 */
		_getEntityClass: function ()
		{
			return efwdDal.entity.Status;
		}
	},

	defer: function (statics, members)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}
});
