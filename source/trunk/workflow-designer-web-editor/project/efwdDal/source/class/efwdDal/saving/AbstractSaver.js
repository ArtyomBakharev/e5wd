/*
 #ignore(efwdCommon)
 #ignore(efwdCommon.*)
 */

/**
 * Класс управляющий сохранением очереди изменений
 */
qx.Class.define("efwdDal.saving.AbstractSaver", {

	type: "abstract",
	extend: qx.core.Object,

	events: {
		/**
		 * Переход в состояние сохранения очереди изменений
		 */
		"savingStart": "qx.event.type.Event",

		/**
		 * Сохранение всех полученных изменений (очередь очищена)
		 * @param {Date} revisionTime время получения серии изменений
		 */
		"savingComplete": "qx.event.type.Data"
	},

	properties: {
		/**
		 * Осталось серий для сохранения
		 */
		batchesLeft: {
			init: 0,
			check: "Integer",
			event: "changeBatchesLeft",
			nullable: false
		}
	},

	/**
	 * @param {efwdDal.IdsMapper} idsMapper
	 */
	construct: function (idsMapper)
	{
		this.__idsMapper = idsMapper;

		this._savingEnabled = true;
		this.__unitsQueue = new efwdDal.saving.ChangeUnitsQueue();
	},

	members: {

		/**
		 * Добавление единицы изменения данных в очередь
		 * @param {efwdDal.saving.ChangeUnit[]} changeUnits
		 */
		addChangeUnits: function (changeUnits)
		{
			if (!this._savingEnabled)
			{
				return;
			}

			var length = this.__unitsQueue.getLength();
			changeUnits.forEach(function (x){this.__unitsQueue.push(x)}, this);

			var batchesLeft = this.getBatchesLeft();
			this.setBatchesLeft(batchesLeft + (this.__unitsQueue.getLength() - length));

			var firstBatch = !this.__duringCommit;
			if (batchesLeft == 0 && this.getBatchesLeft() > 0)
			{
				this.__duringCommit = true;
				this.fireEvent("savingStart");
			}

			this.__lastUpdate = new Date();
			this._changeUnitsAdded(firstBatch);
		},

		disableSaving: function()
		{
			this._savingEnabled = false;
			this.__duringCommit = false;
		},

		/**
		 * Происходит ли в данный момент сохранение изменений
		 * @return {Boolean}
		 */
		isDuringSaving: function ()
		{
			return !!this.__duringCommit;
		},

		/**
		 * @param {Boolean} firstBatch
		 * @protected
		 */
		_changeUnitsAdded: function (firstBatch){ this.abstractMethod(arguments); },

		/**
		 * @param {efwdDal.saving.ChangeUnit} unit
		 * @param {Boolean} success
		 * @param response {*}
		 * @protected
		 */
		_processChangeUnitResult: function (unit, success, response)
		{
			if (success && unit.getChangeType() == efwdDal.saving.ChangeUnit.CHANGE_TYPE_CREATE)
			{
				this.__idsMapper.setServerId(unit.getEntity().getEntityType(), unit.getEntity().getSource().getId(),
					response);
			}

			this.setBatchesLeft(this.getBatchesLeft() - 1);

			if (this.getBatchesLeft() == 0)
			{
				this.__duringCommit = false;
				this.fireDataEvent("savingComplete", {revisionTime: this.__lastUpdate});
			}
		},

		/**
		 * @return {efwdDal.saving.ChangeUnit}
		 * @protected
		 */
		_shiftChangeUnit: function ()
		{
			if (this.__unitsQueue.getLength() == 0)
			{
				return null;
			}

			var unit = this.__unitsQueue.shift();

			var entity = unit.getEntity();
			var source = unit.getSource();
			this.__idsMapper.mapToServerIds(entity.getEntityType(), source);

			if (unit.getChangeType() == efwdDal.saving.ChangeUnit.CHANGE_TYPE_CREATE)
			{
				delete source.id;
			}
			else if (unit.getChangeType() == efwdDal.saving.ChangeUnit.CHANGE_TYPE_DELETE)
			{
				this.__idsMapper.unsetServerId(entity.getEntityType(), entity.getSource().getId());
			}

			return unit;
		}
	}
});
