/**
 * Единица изменения данных
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdDal.saving.ChangeUnit", {

	extend: qx.core.Object,

	statics: {

		/**
		 * @type {String}
		 */
		CHANGE_TYPE_CREATE: "create",


		/**
		 * @type {String}
		 */
		CHANGE_TYPE_DELETE: "delete",


		/**
		 * @type {String}
		 */
		CHANGE_TYPE_UPDATE: "update"
	},

	members: {
		/**
		 * @return {string} тип изменения
		 */
		getChangeType: function ()
		{
			return this.__changeType;
		},

		/**
		 * @param {string} changeType
		 */
		setChangeType: function (changeType)
		{
			this.__changeType = changeType;
		},

		/**
		 * @return {efwdDal.entity.AbstractEntity}
		 */
		getEntity: function ()
		{
			return this.__entity;
		},

		/**
		 * при назначении сущности делается глубокая копия её исходника
		 * @param {efwdDal.entity.AbstractEntity} entity
		 */
		setEntity: function (entity)
		{
			this.__entity = entity;
			this.setSource(qx.util.Serializer.toNativeObject(entity.getSource()));
		},

		/**
		 * @return {Object}
		 */
		getSource: function ()
		{
			return this.__source;
		},

		/**
		 * изменить исходник сущности (сделать новую копию)
		 * @param {Object} source
		 */
		setSource: function (source)
		{
			this.__source = source;
		}
	}
});
