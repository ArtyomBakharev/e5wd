/**
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdDal.saving.ChangeUnitsQueue", {
	extend: qx.core.Object,

	properties: {
		length: {
			init: 0,
			check: "Integer",
			nullable: false,
			event: "changeLength"
		}
	},

	construct: function ()
	{
		this.__array = [];
	},

	members: {

		clear: function ()
		{
			this.__array = [];
			this.__updateLength();
		},

		/**
		 * @param {efwdDal.entity.AbstractEntity} entity
		 * @param {String} type
		 */
		createAndPush: function (entity, type)
		{
			if (!this.__mergeUnitToQueue(entity, type))
			{
				var unit = new efwdDal.saving.ChangeUnit();
				unit.setChangeType(type);
				unit.setEntity(entity);

				this.__array.push(unit);
				this.__updateLength();
			}
		},

		/**
		 * @param {efwdDal.saving.ChangeUnit} unit
		 */
		push: function (unit)
		{
			if (!this.__mergeUnitToQueue(unit.getEntity(), unit.getChangeType(), unit.getSource()))
			{
				this.__array.push(unit);
				this.__updateLength();
			}
		},

		/**
		 * @return {efwdDal.saving.ChangeUnit[]}
		 */
		toArray: function()
		{
			return this.__array;
		},

		/**
		 * @return {efwdDal.saving.ChangeUnit}
		 */
		shift: function ()
		{
			var item = this.__array.shift();
			this.__updateLength();
			return item;
		},

		/**
		 * @param {efwdDal.entity.AbstractEntity} entity
		 * @param {String} type
		 * @param {Object} [source=null]
		 */
		__mergeUnitToQueue: function (entity, type, source)
		{
			if (type != efwdDal.saving.ChangeUnit.CHANGE_TYPE_UPDATE)
			{
				return false;
			}

			for (var i = this.__array.length - 1, unit; unit = this.__array[i--];)
			{
				if (entity == unit.getEntity() && unit.getChangeType() != efwdDal.saving.ChangeUnit.CHANGE_TYPE_DELETE)
				{
					if (source)
					{
						unit.setSource(source);
					}
					else
					{
						unit.setEntity(entity);
					}
					return true;
				}

				if (unit.getChangeType() != efwdDal.saving.ChangeUnit.CHANGE_TYPE_UPDATE)
				{
					return false;
				}
			}

			return false;
		},

		__updateLength: function()
		{
			this.setLength(this.__array.length);
		}

	}
});
