/*
 #ignore(efwdCommon)
 #ignore(efwdCommon.*)
 */

qx.Class.define("efwdDal.saving.FakeSaver", {

	extend: efwdDal.saving.AbstractSaver,

	/**
	 * @param {efwdDal.IdsMapper} idsMapper
	 * @param {efwdDal.Service} service
	 */
	construct: function (idsMapper, service)
	{
	},

	members: {

		/**
		 * @param {Boolean} firstBatch
		 * @protected
		 */
		_changeUnitsAdded: function (firstBatch)
		{
		}
	}
});
