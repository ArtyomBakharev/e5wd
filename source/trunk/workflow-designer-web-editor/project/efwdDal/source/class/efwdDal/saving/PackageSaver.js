/*
 #ignore(efwdCommon)
 #ignore(efwdCommon.*)
 */

qx.Class.define("efwdDal.saving.PackageSaver", {

	extend: efwdDal.saving.AbstractSaver,

	/**
	 * @param {efwdDal.IdsMapper} idsMapper
	 * @param {efwdDal.Service} service
	 */
	construct: function (idsMapper, service)
	{
		this.base(arguments, idsMapper);
		this.__service = service;

		this._changeUnitsAdded
			= this._changeUnitsAdded.lateThrottling(efwdConfig.efwdDal.saving.PackageSaver.savingLatency);
	},

	members: {

		/**
		 * @param {Boolean} firstBatch
		 * @protected
		 */
		_changeUnitsAdded: function (firstBatch)
		{
			if (this._savingEnabled && !this.__duringPackageCommit)
			{
				var units = [];
				var unit;
				while (unit = this._shiftChangeUnit())
				{
					units.push(unit);
				}

				if (units.length == 0)
				{
					return;
				}

				this.__duringPackageCommit = true;

				this.__service.sendPackage(units).hand(
					function (results)
					{
						results.forEach(function (x)
						{
							this._processChangeUnitResult(x.changeUnit, !x.errorData,
								x.errorData ? x.errorData.response : x.response);
						}, this);
						this.__duringPackageCommit = false;
						this._changeUnitsAdded(true);
					},
					function (errorData)
					{
						units.forEach(function (x)
						{
							this._processChangeUnitResult(x, false, null);
						}, this);
						this.__duringPackageCommit = false;
						this._changeUnitsAdded(true);
					}, this);
			}
		}
	}
});
