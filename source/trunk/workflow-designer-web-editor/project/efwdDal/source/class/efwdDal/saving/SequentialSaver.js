/*
 #ignore(efwdCommon)
 #ignore(efwdCommon.*)
 */

qx.Class.define("efwdDal.saving.SequentialSaver", {

	extend: efwdDal.saving.AbstractSaver,

	/**
	 * @param {efwdDal.IdsMapper} idsMapper
	 * @param {efwdDal.Service} service
	 */
	construct: function (idsMapper, service)
	{
		this.base(arguments, idsMapper);
		this.__service = service;
	},

	members: {

		/**
		 * @param {Boolean} firstBatch
		 * @protected
		 */
		_changeUnitsAdded: function (firstBatch)
		{
			if (this._savingEnabled && firstBatch)
			{
				var unit = this._shiftChangeUnit();
				if (unit)
				{
					this.__service.sendChangeUnit(unit).hand(function (response)
					{
						this._processChangeUnitResult(unit, true, response);
						this._changeUnitsAdded(true);
					}, function (errorData, errorHandled)
					{
						this._processChangeUnitResult(unit, false, errorData.response);
						this._changeUnitsAdded(true);
					}, this);
				}
			}
		}
	}
});
