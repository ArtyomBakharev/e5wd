goog.provide("__symbol__.efwdConfig.efwdEditor.canvas.$schema.action");

//noinspection JSUnusedGlobalSymbols
AppTools.config("efwdConfig.efwdEditor.canvas.$schema", {

	action: {
		style: {
			shared: {
				connector: {
					activeColor: "#8080FF"
				}
			},
			joint: {
				circle: {
					attrs: {
						r: "={hovered} ? 6 : 4",
						fill: "={hovered} ? 'black' : parent.{hovered} && !parent.{selected} ? $.shared.connector.activeColor : $.shared.connector.defaultColor",
						"stroke-opacity": 0,
						opacity: "=parent.{selected} || parent.{hovered} ? 1 : 0"
					}
				},
				transparentZone: {
					attrs: {
						r: 7,
						opacity: 0
					}
				},
				enabled: true
			},
			labelFixation: {
				shared: {
					stroke: "=parent.{hovered} && !parent.{selected} ? $.shared.connector.activeColor : $.shared.connector.defaultColor"
				},
				enabled: "={hovered} || {selected}"
			}
		},

		drawChildren: {
			arrow: {
				0: {
					set: {
						selected: "=parent.{selected}",
						hovered: "=parent.{hovered}"
					}
				},
				append: [
					{
						child: "sourcePinPoint",
						include: "actionPinPoint",
						set: {
							x: "=parent.{pathArray} ? parent.{pathArray}[0].x : $geo.OUT_OF_CANVAS_POINT.x",
							y: "=parent.{pathArray} ? parent.{pathArray}[0].y : $geo.OUT_OF_CANVAS_POINT.y"
						}
					},
					{
						child: "destPinPoint",
						include: "actionPinPoint",
						set: {
							x: "=parent.{pathArray} ? parent.{pathArray}[parent.{pathArray}.length - 1].x : $geo.OUT_OF_CANVAS_POINT.x",
							y: "=parent.{pathArray} ? parent.{pathArray}[parent.{pathArray}.length - 1].y : $geo.OUT_OF_CANVAS_POINT.y"
						}
					}
				]
			},
			joint: {
				prepend: [
					{
						call: "circle",
						attrs: "$.joint.transparentZone.attrs",
						modifiers: {
							bbox: true
						}
					}
				]
			}
		}
	},

	actionArrow: {
		style: {
			line: {
				attrs: {
					stroke: "=parent.{hovered} && !parent.{selected} ? $$.action.shared.connector.activeColor : $$.action.shared.connector.defaultColor",
					"stroke-width": "=parent.{selected} ? 3 : 1"
				}
			},
			transparentZone: {
				attrs: {
					"stroke-width": 20,
					opacity: 0
				}
			}
		},

		properties: {
			transparentZone: { initial: true }
		},

		drawChildren: {
			line: {
				prepend: [
					{
						call: "path",
						attrs: {
							path: "=parent.{path}",
							include: ["$.transparentZone.attrs"]
						},
						modifiers: {
							enabled: "=parent.{transparentZone}",
							bbox: true
						}
					}
				]
			}
		}
	},

	actionPinPoint: {
		type: "positioned",

		style: {
			point: {
				attrs: {
					r: 6,
					fill: "=$$.action.shared.connector.activeColor",
					"stroke-opacity": 0,
					opacity: "={active} || {hovered} ? 1 : 0"
				}
			},
			indicator: {
				attrs: {
					r: 20,
					fill: "#FF9999",
					stroke: "#FF9999",
					opacity: 0.5
				}
			}
		},

		properties: {
			active: { dynamic: true, initial: false }
		},

		draw: [
			{
				call: "circle",
				attrs: "$.indicator.attrs",
				modifiers: {
					enabled: "={active}"
				}
			},
			{
				call: "circle",
				attrs: "$.point.attrs"
			}
		]
	},

	actionGhost: {
		type: "actionArrow",
		layer: "creation",
		style: {
			emptyZone: 5
		},

		properties: {
			transparentZone: { initial: false },
			source: { dynamic: true },
			dest: { dynamic: true },
			pathArray: { bind: "{source} && {dest} ? $f.getArrowPathArray({source}, {dest}, null, $.emptyZone) : null" }
		}
	},

	quickActionTool: {
		layer: "connectorsUp",

		style: {
			shared: {
				pinButtonOffset: {
					x: "=$$.quickActionToolPinButton.shared.iconWidth / 2",
					y: "=-$$.quickActionToolPinButton.shared.iconHeight / 2 - 7"
				}
			}
		},

		properties: {
			element: { dynamic: true },
			pinButtonIcon: { dynamic: true }
		},

		draw: [
			{
				call: "rect"
			},
			{
				child: "sourcePointPinButton",
				include: "quickActionToolPinButton",
				set: {
					x: "={element|pathArray} ? {element|pathArray}[0].x + $.shared.pinButtonOffset.x : 0",
					y: "={element|pathArray} ? {element|pathArray}[0].y + $.shared.pinButtonOffset.y : 0",
					icon: "={pinButtonIcon}"
				},
				modifiers: {
					enabled: "=!!{element|sourcePoint}"
				}
			},
			{
				child: "destPointPinButton",
				include: "quickActionToolPinButton",
				set: {
					x: "={element|pathArray} ? {element|pathArray}[{element|pathArray}.length - 1].x + $.shared.pinButtonOffset.x : 0",
					y: "={element|pathArray} ? {element|pathArray}[{element|pathArray}.length - 1].y + $.shared.pinButtonOffset.y : 0",
					icon: "={pinButtonIcon}"
				},
				modifiers: {
					enabled: "=!!{element|destPoint}"
				}
			}
		]
	},

	quickActionToolPinButton: {

		type: "positioned",

		style: {
			shared: {
				iconWidth: 16,
				iconHeight: 16
			},
			icon: {
				attrs: {
					src: "={icon}",
					cursor: "pointer",
					"stroke-opacity": 0,
					width: "=$.shared.iconWidth",
					height: "=$.shared.iconHeight"
				}
			}
		},

		properties: {
			icon: {},
			width: { bind: "$.shared.iconWidth" },
			height: { bind: "$.shared.iconHeight" }
		},

		draw: [
			{
				call: "image",
				attrs: "$.icon.attrs",
				modifiers: {
					centered: true
				}
			}
		]
	}
});

