goog.provide("__symbol__.efwdConfig.efwdEditor.canvas.$schema");

//noinspection JSUnusedGlobalSymbols
AppTools.config("efwdConfig.efwdEditor.canvas.$schema", {

	$layers: [
		"background",
		"states",
		"connectors",
		"statesUp",
		"connectorsUp",
		"creation"
	],

	activitiesIndicator: {
		style: {
			count: {
				attrs: {
					fill: "={hovered} || {selected} ? 'red' : 'black'"
				}
			},
			transparentZone: {
				attrs: {
					width: 25,
					height: 15,
					opacity: 0,
					fill: "black"
				}
			}
		},

		properties: {
			width: { bind: "$.transparentZone.attrs.width" },
			height: { bind: "$.transparentZone.attrs.height" }
		},

		draw: {
			prepend: [
				{
					call: "rect",
					attrs: "$.transparentZone.attrs",
					modifiers: {
						bbox: true,
						centered: true
					}
				}
			]
		}
	},

	moveGhostBox: {
		layer: "creation",

		style: {
			shared: {
				color: "#FF9999"
			},
			rect: {
				attrs: {
					fill: "=$.shared.color",
					stroke: "=$.shared.color",
					opacity: 0.5,
					x: "={box}.left",
					y: "={box}.top",
					width: "={box}.width",
					height: "={box}.height"
				}
			},
			horizontal: {
				attrs: {
					stroke: "=$.shared.color",
					path: "=$f.getPath([{x: {box}.left, y: {point}.y}, {x: {box}.right, y: {point}.y}])"
				}
			},
			vertical: {
				attrs: {
					stroke: "=$.shared.color",
					path: "=$f.getPath([{x: {point}.x, y: {box}.top}, {x: {point}.x, y: {box}.bottom}])"
				}
			}
		},

		properties: {
			box: { dynamic: true, initial: {left: 0, top: 0, right: 0, bottom: 0, width: 0, height: 0} },
			point: { dynamic: true, initial: {x: 0, y: 0} }
		},

		draw: [
			{
				call: "rect",
				attrs: "$.rect.attrs",
				modifiers: {
					bbox: true
				}
			},
			{
				call: "path",
				attrs: "$.horizontal.attrs"
			},
			{
				call: "path",
				attrs: "$.vertical.attrs"
			}
		]
	},

	selectionBox: {
		layer: "creation",
		style: {
			box: {
				attrs: {
					fill: "blue",
					opacity: 0.3,
					x: "=Math.min({firstPoint}.x, {secondPoint}.x)",
					y: "=Math.min({firstPoint}.y, {secondPoint}.y)",
					width: "=Math.abs({firstPoint}.x - {secondPoint}.x)",
					height: "=Math.abs({firstPoint}.y - {secondPoint}.y)"
				}
			}
		},
		properties: {
			firstPoint: { dynamic: true, initial: {x: 0, y: 0} },
			secondPoint: { dynamic: true, initial: {x: 0, y: 0} }
		},
		draw: [
			{
				call: "rect",
				attrs: "$.box.attrs",
				modifiers: {
					bbox: true
				}
			}
		]
	}

});

