goog.provide("__symbol__.efwdConfig.efwdEditor.canvas.$schema.status");

//noinspection JSUnusedGlobalSymbols
AppTools.config("efwdConfig.efwdEditor.canvas.$schema", {
	status: {
		draw: {
			prepend: [
				{
					include: "statusHighlighter",
					modifiers: {
						enabled: "={hovered}"
					}
				}
			],
			0: {
				set: {
					selected: "={selected}"
				}
			}
		}
	},

	statusBox: {
		style: {
			box: {
				oldFill: efwdConfig.efwdRenderer.elements.statusBox.style.box.attrs.fill,
				attrs: {
					fill: "={selected} ? '#7A9FF5' : $.box.oldFill"
				}
			}
		}
	},

	statusHighlighter: {
		type: "positioned",

		style: {
			box: {
				attrs: {
					width: "={width}",
					height: "={height}",
					"stroke-width": 15,
					"stroke-opacity": 0.5,
					"r": "=$$.status.shared.box.r"
				}
			}
		},

		properties: {
			width: { bind: "$$.status.shared.box.width" },
			height: { bind: "$$.status.shared.box.height" }
		},

		draw: [
			{
				call: "rect",
				attrs: "$.box.attrs",
				modifiers: {
					centered: true
				}
			}
		]
	},

	statusGhost: {
		type: "statusBox",
		layer: "creation",

		properties: {
			x: { dynamic: true },
			y: { dynamic: true }
		}
	},

	quickStatusTool: {
		layer: "statesUp",

		style: {
			shared: {
				buttonGap: 5
			}
		},

		properties: {
			elementBBox: { bind: "{element|bbox}" },
			element: { dynamic: true }
		},

		draw: [
			{
				child: "actionButton",
				include: "quickStatusToolButton",
				set: {
					x: "={elementBBox}.right - $$.quickStatusToolButton.shared.width / 2",
					y: "={elementBBox}.top - $$.quickStatusToolButton.shared.height / 2 - $.shared.buttonGap"
				}
			}
		]
	},

	quickStatusToolButton: {

		type: "positioned",

		style: {
			shared: {
				width: 24,
				height: 24
			},
			box: {
				attrs: {
					width: "=$.shared.width",
					height: "=$.shared.height",
					fill: "white",
					cursor: "pointer"
				}
			},
			icon: {
				attrs: {
					width: "=$.shared.width - 4",
					height: "=$.shared.height - 4",
					x: "=- $.shared.width / 2 + 2",
					y: "=- $.shared.height / 2 + 2",
					src: "={icon} || $$.Constants.emptyGif",
					cursor: "pointer"
				}
			}
		},

		properties: {
			icon: { dynamic: true },
			width: { bind: "$.shared.width" },
			height: { bind: "$.shared.height" }
		},

		draw: [
			{
				call: "rect",
				attrs: "$.box.attrs",
				modifiers: {
					centered: true
				}
			},
			{
				call: "image",
				attrs: "$.icon.attrs"
			}
		]
	}

});

