goog.provide("__symbol__.efwdConfig.efwdEditor.canvas");

AppTools.config("efwdConfig.efwdEditor.canvas", {

	$style: {
		defaultGridLength: 10,
		backgroundColor: "white",
		paperPadding: 0,
		//минимальный зазор между элементами и краем канвы
		elementsGap: 20,
		//Зазор между элементом и краем канвы, который отводится для полос прокрутки
		scrollbarsGap: 20,
		//расстояние от активного элемента до края вьюпорта канвы, при котором канва начинает скроллиться
		scrollTriggerZone: 70
	},

	behavior: {
		tool: {
			ConnectorJointsManaging: {
				//минимальное расстояние от новой точки до отрезка проведённого от предыдущей точки до следующей
				//достаточное для сохранения этой точки
				minDistanceFromLineToJoint: 10
			}
		}
	}

});
