goog.require("__symbol__.efwdConfig.efwdEditor.canvas");
goog.require("__symbol__.efwdConfig.efwdEditor.canvas.$schema");
goog.require("__symbol__.efwdConfig.efwdEditor.canvas.$schema.action");
goog.require("__symbol__.efwdConfig.efwdEditor.canvas.$schema.status");

AppTools.config("efwdConfig.efwdEditor", {

	$timeFormat: "HH:mm:ss",

	behavior: {
		GeneralCommandsProvider: {
			moveElementsLongStep: efwdConfig.efwdEditor.canvas.$style.defaultGridLength,
			moveElementsShortStep: 1
		}
	},

	commands: {
		types: {
			PasteEntities: {
				elementsOffset: {
					x: efwdConfig.efwdEditor.canvas.$style.defaultGridLength * 2,
					y: efwdConfig.efwdEditor.canvas.$style.defaultGridLength * 2
				}
			}
		}
	},

	ui: {
		$shortcuts: {
			addAction: "A",
			addStatus: "S",
			copy: "Control+C",
			defaultTool: "Escape",
			deleteElement: "Delete",
			deselectAllElements: "Control+D",
			paste: "Control+V",
			redo: ["Control+Y", "Control+Shift+Z"],
			undo: "Control+Z",
			selectAllElements: "Control+A"
		},

		$style: {
			layout: {
				leftPanelWidth: 300
			}
		}
	}

});
