/*
 #asset(qx/icon/Oxygen/22/actions/go-last.png)

 #asset(efwdEditor/*)
 #ignore(efwdConfig.efwdEditor)
 #ignore(efwdConfig.efwdEditor.*)
 #ignore(efwdCommon)
 #ignore(efwdCommon.*)
 #ignore(ioc)
 #ignore(JsfIoc)
 */

qx.Class.define("efwdEditor.Application", {

	extend: efwdUI.Application,

	statics: {
		/**
		 * Статическая инициализация приложения (используется как в приложении так и в юнит-тестах)
		 */
		staticInit: function ()
		{
			this.__fixQooxdooSupportsEvent();
			efwdUI.Application.staticInit();
		},

		__fixQooxdooSupportsEvent: function()
		{
			var oldMethod = qx.bom.Event.supportsEvent;
			qx.bom.Event.supportsEvent = function(target, event)
			{
				if (target === window && event == "storage" && qx.core.Environment.get("browser.name") == "firefox"
					&& parseFloat(qx.core.Environment.get("browser.version")) >= 3.5)
				{
					return true;
				}
				else
				{
					return oldMethod.call(qx.bom.Event, target, event);
				}
			}
		}
	},

	members: {

		/**
		 * @return {library.IOC}
		 */
		getContainer: function()
		{
			return this.__container;
		},

		/**
		 * This method contains the initial application code and gets called
		 * during startup of the application
		 */
		main: function ()
		{
			// Call super class
			this.base(arguments);
			this.self(arguments).staticInit();

			this.__processId = library.BOM.getUrlParameters()["process-id"];
			this.__container = new library.IOC(new JsfIoc());
			if (qx.core.Environment.get("qx.debug"))
			{
				ioc = this.__container;
				app = this;
			}

			this.__registerServices();
			this.__initializeFirstServices();
			this.__preLoad();
			this.__acquireSessionId()
				.next(this.__acquireCurrentProcess, this)
				.next(this.__initializeServices, this)
				.next(removeAppLoadIndicator);
		},

		simulate: function()
		{
			var command = this.__container.getService(efwdEditor.commands.types.MoveStatuses);
			command.initialize(this.__container.getService(efwdDal.repository.StatusesRepository).getEntities(this.__container.getService(efwdDal.entity.Process)).toArray());
			var offset = {x: 0, y: 0};
			var times = 0;
			var timer = qx.util.TimerManager.getInstance();

//			library.dev.Debug.fpsStart();
			var start = new Date();
			var id = setInterval(function(){
				offset.x += 1;
				offset.y += 1;
				command.move(Object.clone(offset));

				if (++times > 90)
				{
					clearInterval(id);
//					library.dev.Debug.fpsStop();
					var stop = new Date();
					console.log(300000 / (stop - start));
				}
			}, 1);
		},

		/**
		 * @private
		 */
		__acquireSessionId: function ()
		{
			var sessionRepository = this.__container.getService(efwdDal.repository.SessionRepository);
			sessionRepository.initialize(this.__processId);

			var pageModel = this.__container.getService(efwdUI.behavior.PageModel);

			return sessionRepository.acquireSessionId(false)
				.hand(function ()
				{
					pageModel.setReadOnly(false);
				}, function ()
				{
					pageModel.setReadOnly(true);
					this.getMessagesQueue().addMessage(library.ui.messagesqueue.Message.createInfoMessage("The diagram opened in read only mode."));
					return library.Deferred.success();
				}, this);
		},

		__preLoad: function()
		{
			/** @type {efwdDal.Service} */
			var service = this.__container.getService(efwdDal.Service);
			service.getEntityResource("process", this.__processId);
			service.getEntitiesResource("status", this.__processId);
			service.getEntitiesResource("action", this.__processId);
		},

		/**
		 * @private
		 */
		__registerServices: function ()
		{
			//application
			this.__container.register(efwdUI.Application, {instance: this});
			this.__container.register(efwdEditor.Application, {instance: this});

			//interfaces
			this.__container.register(library.IServiceLocator, {instance: this.__container});

			//dependencies
			this.__container.registerAll(efwdCommon.Dependencies.DEPENDENCIES);
			this.__container.registerAll(efwdDal.Dependencies.DEPENDENCIES);
			this.__container.registerAll(efwdUI.Dependencies.DEPENDENCIES);
			this.__container.registerAll(efwdEditor.Dependencies.DEPENDENCIES);
		},

		/**
		 * @private
		 */
		__initializeFirstServices: function()
		{
			library.ui.management.ShortcutsManager.getInstance().setShortcuts(efwdConfig.efwdEditor.ui.$shortcuts);

			this.__container.getService(efwdDal.AutoNamesGenerator).initialize();
			this.__container.getService(efwdDal.repository.ActionsRepository).initialize();
			this.__container.getService(efwdDal.repository.ActivitiesRepository).initialize();
			this.__container.getService(efwdDal.repository.StatusesRepository).initialize();

			this.__container.getService(efwdUI.ApplicationBehaviorController).initialize();
		},

		/**
		 * @private
		 */
		__initializeServices: function ()
		{
			return this.__container.getService(efwdDal.repository.ProcessesRepository)
				.getProcessById(this.__processId, true)
				.next(function (process)
				{
					this.__container.register(efwdDal.entity.Process, {instance: process});
					this.__container.getService(efwdEditor.behavior.manipulationtool.Model).initialize();
					this.__container.getService(efwdEditor.ui.Controller).initialize();
				}, this);
		}
	}
});
