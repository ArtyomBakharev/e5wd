/*
 #ignore(efwdCommon)
 #ignore(efwdCommon.*)
 #ignore(efwdRenderer)
 #ignore(efwdRenderer.*)
 */

/**
 * Класс хранит список зависимостей для IOC-контейнера
 */
qx.Class.define("efwdEditor.Dependencies", {

	type: "static",

	statics: {
		DEPENDENCIES: [

		/** {@link efwdDal.saving} */
			[efwdDal.saving.AbstractSaver, {cls: efwdDal.saving.PackageSaver, args: [
				efwdDal.IdsMapper,
				efwdDal.Service
			]}],

		/** {@link efwdEditor.behavior} */
			[efwdEditor.behavior.Clipboard, {args: [
				efwdDal.repository.ActionsRepository,
				efwdDal.repository.ActivitiesRepository,
				efwdDal.repository.StatusesRepository
			]}],
			[efwdEditor.behavior.DialogsModel],
			[efwdEditor.behavior.GeneralCommandsProvider, {args: [
				library.IServiceLocator,
				efwdUI.commands.Queue,
				efwdUI.behavior.PageModel,
				efwdEditor.behavior.manipulationtool.Model
			]}],
			[efwdEditor.behavior.SelectionModel],

		/** {@link efwdEditor.behavior.manipulationtool} */
			[efwdEditor.behavior.manipulationtool.Model, {args: [
				efwdUI.commands.Queue
			]}],

		/** {@link efwdEditor.canvas} */
			[efwdEditor.canvas.BBoxesModel, {args: [
				efwdEditor.canvas.CanvasElementsHandling
			]}],
			[efwdEditor.canvas.CanvasElementsHandling, {args: [
				library.IServiceLocator,
				efwdDal.entity.Process,
				efwdEditor.canvas.views.ElementsRenderer
			]}],
			[efwdEditor.canvas.CanvasGridModel, {args: [
				efwdEditor.canvas.views.Canvas
			]}],
			[efwdEditor.canvas.Controller, {args: [
				efwdEditor.canvas.views.Canvas,
				efwdEditor.canvas.CanvasElementsHandling,
				efwdEditor.behavior.manipulationtool.Model,
				efwdEditor.canvas.behavior.ElementsSelection,
				efwdEditor.canvas.behavior.ElementsCreation,
				efwdEditor.canvas.behavior.ElementsMove,
				efwdEditor.canvas.behavior.ElementsEdit,
				efwdEditor.canvas.behavior.ResizeAndScrollCanvas,
				efwdEditor.canvas.behavior.ConnectorsTransformation,
				efwdEditor.canvas.behavior.ActivitiesListsShowing,
				efwdEditor.canvas.behavior.SelectedElementTool,
				efwdEditor.canvas.behavior.MoveActionPinPoint,
				efwdEditor.canvas.behavior.ContextMenu,
				efwdRenderer.Manager,
				efwdRenderer.SchemaCompiler
			]}],
			[efwdEditor.canvas.CommandsProvider, {args: [
				efwdEditor.behavior.SelectionModel,
				library.IServiceLocator,
				efwdUI.commands.Queue,
				efwdUI.behavior.PageModel,
				efwdEditor.behavior.Clipboard,
				efwdEditor.canvas.CanvasElementsHandling,
				efwdEditor.behavior.manipulationtool.Model,
				efwdEditor.canvas.CanvasGridModel
			]}],

		/** {@link efwdEditor.canvas.behavior} */
			[efwdEditor.canvas.behavior.ActivitiesListsShowing, {args: [
				efwdEditor.canvas.CanvasElementsHandling,
				efwdEditor.behavior.manipulationtool.Model,
				efwdEditor.behavior.DialogsModel
			]}],
			[efwdEditor.canvas.behavior.ConnectorsTransformation, {args: [
				efwdEditor.canvas.views.Canvas,
				efwdEditor.canvas.CanvasGridModel,
				efwdEditor.behavior.manipulationtool.Model,
				library.IServiceLocator,
				efwdUI.commands.Queue,
				efwdEditor.canvas.CanvasElementsHandling,
				efwdEditor.canvas.behavior.ResizeAndScrollCanvas,
				efwdUI.behavior.PageModel,
				efwdEditor.behavior.SelectionModel
			]}],
			[efwdEditor.canvas.behavior.ContextMenu, { args: [
				efwdEditor.canvas.views.Canvas,
				efwdEditor.behavior.manipulationtool.Model,
				efwdEditor.behavior.SelectionModel,
				efwdEditor.canvas.CommandsProvider,
				efwdUI.behavior.GeneralCommandsProvider,
				efwdUI.behavior.PageModel
			]}],
			[efwdEditor.canvas.behavior.ElementsCreation, {args: [
				efwdEditor.behavior.manipulationtool.Model,
				library.IServiceLocator,
				efwdEditor.canvas.CanvasElementsHandling,
				efwdUI.behavior.PageModel,
				efwdEditor.canvas.views.Canvas
			]}],
			[efwdEditor.canvas.behavior.ElementsEdit, {args: [
				efwdEditor.behavior.SelectionModel,
				efwdUI.behavior.EntityEditionModel,
				efwdEditor.canvas.CanvasElementsHandling
			]}],
			[efwdEditor.canvas.behavior.ElementsMove, {args: [
				efwdEditor.canvas.views.Canvas,
				efwdEditor.behavior.manipulationtool.Model,
				library.IServiceLocator,
				efwdEditor.canvas.CanvasElementsHandling,
				efwdUI.behavior.PageModel,
				efwdEditor.behavior.SelectionModel
			]}],
			[efwdEditor.canvas.behavior.ElementsSelection, {args: [
				efwdEditor.canvas.views.Canvas,
				efwdEditor.behavior.manipulationtool.Model,
				efwdEditor.behavior.SelectionModel,
				efwdEditor.canvas.CanvasElementsHandling,
				efwdUI.behavior.EntityEditionModel,
				efwdEditor.canvas.BBoxesModel,
				efwdEditor.canvas.behavior.ResizeAndScrollCanvas,
				efwdRenderer.Factory
			]}],
			[efwdEditor.canvas.behavior.MoveActionPinPoint, {args: [
				efwdEditor.canvas.views.Canvas,
				efwdEditor.behavior.manipulationtool.Model,
				library.IServiceLocator,
				efwdUI.commands.Queue,
				efwdEditor.canvas.CanvasElementsHandling,
				efwdUI.behavior.PageModel,
				efwdEditor.canvas.CanvasGridModel
			]}],
			[efwdEditor.canvas.behavior.ResizeAndScrollCanvas, {args: [
				efwdEditor.canvas.views.Canvas,
				efwdEditor.canvas.CanvasElementsHandling
			]}],
			[efwdEditor.canvas.behavior.SelectedElementTool, {args: [
				library.IServiceLocator,
				efwdUI.behavior.PageModel,
				efwdEditor.behavior.SelectionModel
			]}],

		/** {@link efwdEditor.canvas.behavior.tool} */
			[efwdEditor.canvas.behavior.tool.ActionCreation, {scope: "transient", args: [
				efwdEditor.canvas.views.Canvas,
				library.IServiceLocator,
				efwdUI.commands.Queue,
				efwdEditor.canvas.CanvasElementsHandling,
				efwdEditor.canvas.behavior.ResizeAndScrollCanvas,
				efwdEditor.canvas.BBoxesModel,
				efwdRenderer.Factory,
				efwdRenderer.Manager
			]}],
			[efwdEditor.canvas.behavior.tool.ConnectorJointsManaging, {scope: "transient", args: [
				efwdEditor.canvas.views.Canvas,
				efwdEditor.canvas.CanvasGridModel,
				efwdEditor.behavior.manipulationtool.Model,
				library.IServiceLocator,
				efwdUI.commands.Queue,
				efwdEditor.canvas.CanvasElementsHandling,
				efwdEditor.canvas.behavior.ResizeAndScrollCanvas,
				efwdEditor.behavior.SelectionModel
			]}],
			[efwdEditor.canvas.behavior.tool.ElementsMove, {scope: "transient", args: [
				efwdEditor.canvas.CanvasGridModel,
				library.IServiceLocator,
				efwdUI.commands.Queue,
				efwdEditor.canvas.behavior.ResizeAndScrollCanvas,
				efwdDal.repository.ActionsRepository,
				efwdRenderer.Factory,
				efwdEditor.canvas.CanvasElementsHandling
			]}],
			[efwdEditor.canvas.behavior.tool.StatusCreation, {scope: "transient", args: [
				efwdEditor.canvas.views.Canvas,
				library.IServiceLocator,
				efwdUI.commands.Queue,
				efwdEditor.canvas.CanvasElementsHandling,
				efwdEditor.canvas.CanvasGridModel,
				efwdRenderer.Factory,
				efwdEditor.canvas.behavior.ResizeAndScrollCanvas
			]}],

		/** {@link efwdEditor.canvas.behavior.tool.selectedElementTool} */
			[efwdEditor.canvas.behavior.tool.selectedElementTool.Action, {scope: "transient", args: [
				efwdRenderer.Factory,
				library.IServiceLocator,
				efwdUI.commands.Queue
			]}],
			[efwdEditor.canvas.behavior.tool.selectedElementTool.Status, {scope: "transient", args: [
				efwdRenderer.Factory,
				efwdEditor.behavior.manipulationtool.Model
			]}],

		/** {@link efwdEditor.canvas.views} */
			[efwdEditor.canvas.views.Canvas, {args: [
				efwdEditor.canvas.views.Cursor
			]}],
			[efwdEditor.canvas.views.Cursor, {args: [
				efwdEditor.behavior.manipulationtool.Model
			]}],
			[efwdEditor.canvas.views.ElementsRenderer, {args: [
				efwdRenderer.Factory
			]}],

		/** {@link efwdEditor.commands} */
			[efwdEditor.commands.Controller, {args: [
				efwdEditor.behavior.DialogsModel,
				efwdUI.commands.Queue,
				efwdUI.behavior.EntityEditionModel,
				efwdEditor.behavior.SelectionModel,
				efwdEditor.canvas.CanvasElementsHandling
			]}],

		/** {@link efwdEditor.commands.types} */
			[efwdEditor.commands.types.PasteEntities, {scope: "transient", args: [
				efwdDal.Context,
				library.IServiceLocator,
				efwdEditor.behavior.Clipboard
			]}],
			[efwdEditor.commands.types.MoveActionConnectorJoint, {scope: "transient", args: [
				efwdDal.Context
			]}],
			[efwdEditor.commands.types.MoveActionConnectorPinPoint, {scope: "transient", args: [
				efwdDal.Context
			]}],
			[efwdEditor.commands.types.MoveActionLabel, {scope: "transient", args: [
				efwdDal.Context
			]}],
			[efwdEditor.commands.types.MoveStatuses, {scope: "transient", args: [
				efwdDal.Context,
				efwdDal.repository.ActionsRepository
			]}],
			[efwdEditor.commands.types.CreateActionAndDestStatus, {scope: "transient", args: [
				library.IServiceLocator
			]}],

		/** {@link efwdEditor.ui} */
			[efwdEditor.ui.Controller, {args: [
				efwdDal.repository.ProcessesRepository,
				efwdEditor.ui.views.Layout,
				efwdUI.ui.settingspanel.Controller,
				efwdEditor.canvas.Controller,
				efwdEditor.behavior.manipulationtool.Model,
				efwdUI.commands.Queue,
				efwdEditor.ui.DialogsController,
				efwdEditor.Application,
				efwdEditor.commands.Controller,
				efwdEditor.behavior.GeneralCommandsProvider,
				efwdEditor.canvas.views.Canvas,
				efwdEditor.canvas.CommandsProvider
			]}],
			[efwdEditor.ui.DialogsController, {args: [
				efwdEditor.behavior.DialogsModel,
				library.IServiceLocator,
				efwdDal.entity.Process
			]}],

		/** {@link efwdEditor.ui.dialog.entitieslist} */
			[efwdUI.ui.dialog.entitieslist.ActionsList, {cls: efwdEditor.ui.dialog.entitieslist.ActionsList,
				scope: "transient", args: [
					efwdEditor.behavior.DialogsModel
				]}],

		/** {@link efwdEditor.ui.views} */
			[efwdEditor.ui.views.Layout, {args: [
				efwdEditor.ui.views.Toolbar,
				efwdUI.ui.views.EditorContainer,
				efwdEditor.ui.views.Palette,
				efwdDal.entity.Process
			]}],
			[efwdEditor.ui.views.Palette, {args: [
				efwdEditor.behavior.manipulationtool.Model,
				efwdUI.behavior.PageModel
			]}],
			[efwdEditor.ui.views.Toolbar, {args: [
				efwdDal.saving.AbstractSaver,
				efwdEditor.behavior.GeneralCommandsProvider,
				efwdUI.commands.Queue,
				efwdEditor.behavior.DialogsModel,
				efwdUI.behavior.PageModel,
				efwdEditor.canvas.CommandsProvider
			]}],

		/** {@link efwdRenderer} */
			[efwdRenderer.Factory, {args: [
				efwdRenderer.Manager
			]}],
			[efwdRenderer.Manager, {args: [
				efwdRenderer.SchemaCompiler
			]}],
			[efwdRenderer.SchemaCompiler]
		]
	}
});
