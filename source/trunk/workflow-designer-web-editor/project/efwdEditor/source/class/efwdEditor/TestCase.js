/**
 * @extends {qx.dev.unit.TestCase}
 */
qx.Class.define("efwdEditor.TestCase", {

	extend: qx.dev.unit.TestCase,

	include: library.dev.MMock,

	construct: function ()
	{
		if (!this.__staticInited)
		{
			efwdEditor.Application.staticInit();
			this.__staticInited = true;
		}

		this._ioc = new library.dev.StubIOC(this, efwdEditor.Dependencies.DEPENDENCIES);
	}
});
