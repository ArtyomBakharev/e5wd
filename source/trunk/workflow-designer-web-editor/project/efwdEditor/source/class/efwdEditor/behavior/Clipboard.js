/**
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdEditor.behavior.Clipboard", {
	extend: qx.core.Object,

	properties: {
		empty: {
			init: true,
			check: "Boolean",
			nullable: false,
			event: "changeEmpty"
		}
	},

	/**
	 * @param {efwdDal.repository.ActionsRepository} actionsRepository
	 * @param {efwdDal.repository.ActivitiesRepository} activitiesRepository
	 * @param {efwdDal.repository.StatusesRepository} statusesRepository
	 */
	construct: function (actionsRepository, activitiesRepository, statusesRepository)
	{
		this.__actionsRepository = actionsRepository;
		this.__activitiesRepository = activitiesRepository;
		this.__statusesRepository = statusesRepository;
	},

	members: {

		/**
		 * @param {efwdDal.entity.AbstractEntity[]} entities
		 */
		copy: function (entities)
		{
			this.__entities = entities;
			this.__process = entities[0].getProcess();

			this.__statuses = this.__entities.filter(function (x){ return x instanceof efwdDal.entity.Status })
				.map(function (x){ return qx.util.Serializer.toNativeObject(x.getSource()) });

			this.__actions = this.__entities.filter(function (x)
			{
				return x instanceof efwdDal.entity.Action && !x.isStatusesLess();
			}, this).map(function (x){ return qx.util.Serializer.toNativeObject(x.getSource()) });

			this.setEmpty(this.__statuses.length == 0 && this.__actions.length == 0);

			this.__consecutiveInsertionsCount = 0;
		},

		getConsecutiveInsertionsCount: function()
		{
			return this.__consecutiveInsertionsCount;
		},

		/**
		 * @return {efwdDal.entity.AbstractEntity[]}
		 */
		paste: function ()
		{
			var createdStatuses = {};
			this.__statuses.forEach(function (source)
			{
				var status = this.__statusesRepository.createStatus(this.__process);
				this.__restoreEntityState(status, source);

				createdStatuses[source.id] = status;
			}, this);

			var createdActions = {};
			this.__actions.forEach(function (source)
			{
				var sourceStatus = this.__getStatus(createdStatuses, source.from_status_id);
				var destStatus = this.__getStatus(createdStatuses, source.to_status_id);
				if (sourceStatus && destStatus)
				{
					var action = this.__actionsRepository.createAction(this.__process, null, sourceStatus, destStatus);
					this.__restoreEntityState(action, source);
				}

				createdActions[source.id] = action;
			}, this);

			++this.__consecutiveInsertionsCount;
			return Object.getValues(createdStatuses).concat(Object.getValues(createdActions));
		},

		__getStatus: function (createdStatuses, statusId)
		{
			return createdStatuses[statusId]
				|| this.__statusesRepository.getEntities(this.__process).toArray()
				.filter(function (x){ return x.getSource().getId() == statusId })[0];
		},

		/**
		 * @param {efwdDal.entity.AbstractEntity} entity
		 * @param {Object} source
		 * @private
		 */
		__restoreEntityState: function (entity, source)
		{
			var activityCategories = efwdConfig.efwdCommon.data.types.$activityCategories[entity.getEntityType()];

			//restore state
			var sourceCopy = Object.clone(source);
			delete sourceCopy.id;
			delete sourceCopy.from_status_id;
			delete sourceCopy.to_status_id;
			activityCategories && activityCategories.forEach(function (x){ delete sourceCopy[x] });

			library.data.Helper.restoreObjectToModel(sourceCopy, entity.getSource());

			if (activityCategories)
			{
				//restore activities
				var activities = this.__activitiesRepository.getEntities(this.__process);
				activityCategories.forEach(function (category)
				{
					source[category].forEach(function (activityId)
					{
						var activity = activities.toArray()
							.filter(function (x){ return x.getSource().getId() == activityId })[0];
						activity && entity.addActivity(category, activity);
					});
				}, this);
			}
		}

	}
});
