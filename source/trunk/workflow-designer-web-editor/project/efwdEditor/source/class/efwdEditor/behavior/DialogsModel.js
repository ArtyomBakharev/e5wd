/**
 * Модель открытых диалогов редактора
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdEditor.behavior.DialogsModel", {

	extend: qx.core.Object,

	events: {
		/**
		 * Открытие списка активностей для определённой сущности
		 * @param {efwdDal.entity.AbstractEntity} entity владелец активностей
		 */
		"openActivitiesListDialog": "qx.event.type.Data",

		/**
		 * Закрытие списка активностей для определённой сущности
		 * @param {efwdDal.entity.AbstractEntity} entity владелец активностей
		 */
		"closeActivitiesListDialog": "qx.event.type.Data"
	},

	properties: {
		/**
		 * Открыт ли диалог действий процесса
		 */
		globalActionsListDialog: {
			init: false,
			check: "Boolean",
			event: "changeGlobalActionsListDialog"
		},

		/**
		 * Открыт ли список всех активностей
		 */
		globalActivitiesListDialog: {
			init: false,
			check: "Boolean",
			event: "changeGlobalActivitiesListDialog"
		},

		/**
		 * Открыт ли диалог со свойствами процесса
		 */
		processPropertiesDialog: {
			init: false,
			check: "Boolean",
			event: "changeProcessPropertiesDialog"
		}
	},

	construct: function()
	{
		this.__openedActivitiesDialogs = {};
	},

	members: {

		/**
		 * Закрыть список активностей переданной сущности
		 *
		 * @param {efwdDal.entity.AbstractEntity} entity
		 */
		closeActivitiesListDialog: function(entity)
		{
			var entityKey = Object.getUniqueId(entity);
			if (this.__openedActivitiesDialogs[entityKey])
			{
				delete this.__openedActivitiesDialogs[entityKey];
				this.fireDataEvent("closeActivitiesListDialog", {entity: entity});
			}
		},

		/**
		 * Открыт ли список активностей переданной сущности
		 *
		 * @param {efwdDal.entity.AbstractEntity} entity
		 * @return {Boolean}
		 */
		isActivitiesListDialogOpened: function(entity)
		{
			var entityKey = Object.getUniqueId(entity);
			return !!this.__openedActivitiesDialogs[entityKey];
		},

		/**
		 * Открыть список активностей переданной сущности
		 *
		 * @param {efwdDal.entity.AbstractEntity} entity
		 */
		openActivitiesListDialog: function(entity)
		{
			var entityKey = Object.getUniqueId(entity);
			if (!this.__openedActivitiesDialogs[entityKey])
			{
				this.__openedActivitiesDialogs[entityKey] = true;
				this.fireDataEvent("openActivitiesListDialog", {entity: entity});
			}
		},

		/**
		 * Сменить состояние списка активностей переданной сущности
		 *
		 * @param {efwdDal.entity.AbstractEntity} entity
		 */
		toggleActivitiesListDialog: function(entity)
		{
			var entityKey = Object.getUniqueId(entity);

			if (this.__openedActivitiesDialogs[entityKey])
			{
				this.closeActivitiesListDialog(entity);
			}
			else
			{
				this.openActivitiesListDialog(entity);
			}
		}

	}
});
