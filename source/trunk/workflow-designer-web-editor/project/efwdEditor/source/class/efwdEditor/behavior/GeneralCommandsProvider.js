/*
 #ignore(efwdCommon)
 #ignore(efwdCommon.*)
 */

/**
 * Поставщик основных команд интерфейса редактора
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdEditor.behavior.GeneralCommandsProvider", {

	extend: qx.core.Object,

	/**
	 * @param {library.IServiceLocator} serviceLocator
	 * @param {efwdUI.commands.Queue} queue
	 * @param {efwdUI.behavior.PageModel} pageModel
	 * @param {efwdEditor.behavior.manipulationtool.Model} manipulationToolModel
	 */
	construct: function (serviceLocator, queue, pageModel, manipulationToolModel)
	{
		this.__serviceLocator = serviceLocator;
		this.__queue = queue;
		this.__pageModel = pageModel;
		this.__manipulationToolModel = manipulationToolModel;

//		this.__moveElementsCommands = {};
	},

	members: {

		/**
		 * @return {library.ui.Command}
		 * @cacheResult
		 */
		getAddActionCommand: function ()
		{
			var command = new library.ui.Command(function ()
			{
				this.__manipulationToolModel.setTool(new efwdEditor.behavior.manipulationtool.ActionTool());
			}, this);

			command.set({
				label: "Add action",
				icon: efwdUI.ui.IconsStore.icons.action
			});

			this.__pageModel.bind("readOnly", command, "enabled", {converter: function (x){ return !x }});

			return command;
		},

		/**
		 * @return {library.ui.Command}
		 * @cacheResult
		 */
		getAddStatusCommand: function ()
		{
			var command = new library.ui.Command(function ()
			{
				this.__manipulationToolModel.setTool(new efwdEditor.behavior.manipulationtool.StatusTool());
			}, this);

			command.set({
				label: "Add status",
				icon: efwdUI.ui.IconsStore.icons.status
			});

			this.__pageModel.bind("readOnly", command, "enabled", {converter: function (x){ return !x }});

			return command;
		},

		/**
		 * Возвращает команду для возврата к списку процессов
		 * @return {library.ui.Command}
		 * @cacheResult
		 */
		getBackToProcessesListCommand: function ()
		{
			var command = new library.ui.Command(function ()
			{
				location.replace(efwdCommon.Helper.getProcessesListUrl());
			}, this);

			command.set({
				label: "Go to processes list",
				icon: efwdUI.ui.IconsStore.icons.goBackToProcesses
			});

			return command;
		}

//		/**
//		 * @param {String} direction (up, down, right, left)
//		 * @return {library.ui.Command}
//		 */
//		getMoveElementsCommand: function (direction)
//		{
//			if (!this.__moveElementsCommands[direction])
//			{
//				var command = this.__moveElementsCommands[direction] = new library.ui.Command(function ()
//				{
//					this.__selectionModel.getSelection();
//				}, this);
//
//				command.set({
//					label: "Deselect all"
//				});
//			}
//
//			return this.__moveElementsCommands[direction];
//		},


	},

	defer: function (statics, members)
	{
		members.getAddActionCommand = members.getAddActionCommand.cacheResult();
		members.getAddStatusCommand = members.getAddStatusCommand.cacheResult();
		members.getBackToProcessesListCommand = members.getBackToProcessesListCommand.cacheResult();
	}
});
