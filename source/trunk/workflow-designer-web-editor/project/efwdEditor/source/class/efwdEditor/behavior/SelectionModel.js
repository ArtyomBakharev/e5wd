/**
 * Модель выделения элементов на канве
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdEditor.behavior.SelectionModel", {

	extend: qx.core.Object,

	properties: {

		multiselect: {
			init: false,
			nullable: false,
			check: "Boolean",
			event: "changeMultiselect"
		},

		/**
		 * Массив выделенных элементов
		 * @type {qx.data.Array}
		 */
		selection: {
			deferredInit: true,
			event: "changeSelection",
			check: "qx.data.Array"
		}
	},

	construct: function ()
	{
		this.initSelection(new qx.data.Array());
	},

	members: {

		deselect: function(element)
		{
			this.getSelection().remove(element);
		},

		/**
		 * выделить определённый элемент
		 * @param {efwdRenderer.Element} element
		 */
		select: function (element)
		{
			var selection = this.getSelection();
			if (this.isMultiselect())
			{
				if (selection.indexOf(element) !== -1)
				{
					return;
				}
				selection.push(element);
			}
			else
			{
				if (selection.getLength() == 1 && selection.getItem(0) == element)
				{
					return;
				}
				selection.splice(0, selection.getLength(), element);
			}
		},

		/**
		 * выделить определённый элемент
		 * @param {efwdRenderer.Element[]} elements
		 */
		selectExact: function(elements)
		{
			if (elements.length > 1)
			{
				this.setMultiselect(true);
			}

			var selection = this.getSelection();
			selection.splice.apply(selection, [0, selection.getLength()].concat(elements));
		},

		toggle: function(element)
		{
			var selection = this.getSelection();
			var index = selection.indexOf(element);
			if (index !== -1)
			{
				selection.removeAt(index);
			}
			else if (this.isMultiselect())
			{
				selection.push(element);
			}
			else
			{
				selection.splice(0, selection.getLength(), element);
			}
		},

		/**
		 * Снять выделение со всех элементов
		 */
		deselectAll: function ()
		{
			this.getSelection().removeAll();
		}
	}
});
