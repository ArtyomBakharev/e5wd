/**
 * Инструмент создания действия
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdEditor.behavior.manipulationtool.ActionTool", {

	extend: qx.core.Object,

	implement: efwdEditor.behavior.manipulationtool.ITool,

	events: {
		/**
		 * Сброс инструмента в начальное состояние
		 */
		"resetState": "qx.event.type.Event"
	},

	properties: {
		/**
		 * Конечный статус (может быть назначен только если назначен начальный статус)
		 */
		destStatus: {
			init: null,
			nullable: true,
			check: "efwdDal.entity.Status",
			apply: "__applyDestStatus",
			event: "changeDestStatus"
		},
		/**
		 * Начальный статус (может быть назначен только если ещё не назначен конечный статус)
		 */
		sourceStatus: {
			init: null,
			nullable: true,
			check: "efwdDal.entity.Status",
			apply: "__applySourceStatus",
			event: "changeSourceStatus"
		}
	},

	members: {

		resetState: function ()
		{
			this.__resetState = true;
			this.setSourceStatus(null);
			this.setDestStatus(null);
			this.__resetState = false;
			this.fireEvent("resetState");
		},

		/**
		 * @private
		 */
		__applySourceStatus: function (value, old)
		{
			if (value === null)
			{
				if (!this.__resetState)
				{
					throw new Error("[efwdEditor.behavior.manipulationtool.ActionTool#__applySourceStatus] use resetState for setting statuses to null");
				}
			}
			else if (old || this.getDestStatus())
			{
				throw new Error("[efwdEditor.behavior.manipulationtool.ActionTool#__applySourceStatus] can't set source status because tool is not in clear state");
			}
		},

		/**
		 * @private
		 */
		__applyDestStatus: function (value, old)
		{
			if (value === null)
			{
				if (!this.__resetState)
				{
					throw new Error("[efwdEditor.behavior.manipulationtool.ActionTool#__applySourceStatus] use resetState for setting statuses to null");
				}
			}
			else if (!this.getSourceStatus() || old)
			{
				throw new Error("[efwdEditor.behavior.manipulationtool.ActionTool#setDestStatus] can't set dest status because tool is in clear state or dest status already setted");
			}
		}
	}
});
