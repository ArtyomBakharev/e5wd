/**
 * Инструмент по-умолчанию
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdEditor.behavior.manipulationtool.DefaultTool", {

	extend: qx.core.Object,

	implement: efwdEditor.behavior.manipulationtool.ITool,

	members: {

	}
});
