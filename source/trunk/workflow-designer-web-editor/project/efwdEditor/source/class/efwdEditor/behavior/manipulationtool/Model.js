/**
 * Модель позволяет назначать инструмент редактирования
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdEditor.behavior.manipulationtool.Model", {

	extend: qx.core.Object,

	properties: {
		/**
		 * Инструмент редактирования
		 */
		tool: {
			init: new efwdEditor.behavior.manipulationtool.DefaultTool(),
			check: "efwdEditor.behavior.manipulationtool.ITool",
			event: "changeTool"
		}
	},

	/**
	 * @param {efwdUI.commands.Queue} commandsQueue
	 */
	construct: function (commandsQueue)
	{
		this.__commandsQueue = commandsQueue;
	},

	members: {

		initialize: function ()
		{
			this.__commandsQueue.addListener("beforeUndoBatch", function (){ this.resetTool() }, this);
			this.__commandsQueue.addListener("beforeRedoBatch", function (){ this.resetTool() }, this);
		},

		/**
		 * @return {Boolean}
		 */
		isToolDefault: function ()
		{
			return this.toolIs(efwdEditor.behavior.manipulationtool.DefaultTool);
		},

		/**
		 * @param {Function} toolClass
		 * @return {Boolean}
		 */
		toolIs: function (toolClass)
		{
			return this.getTool() instanceof toolClass;
		}
	}
});
