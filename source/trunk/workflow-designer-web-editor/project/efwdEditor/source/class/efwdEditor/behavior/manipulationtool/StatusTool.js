/**
 * Инструмент создания статуса
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdEditor.behavior.manipulationtool.StatusTool", {

	extend: qx.core.Object,

	implement: efwdEditor.behavior.manipulationtool.ITool,

	members: {

	}
});
