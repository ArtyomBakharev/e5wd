/**
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdEditor.canvas.BBoxesModel", {

	extend: qx.core.Object,

	/**
	 * @param {efwdEditor.canvas.CanvasElementsHandling} сanvasElementsHandling
	 */
	construct: function (сanvasElementsHandling)
	{
		this.__сanvasElementsHandling = сanvasElementsHandling;
//		this.__axis = {
//		};
//		this.__yAxis = [];
	},

	members: {

//		initialize: function ()
//		{
//			this.__сanvasElementsHandling.addListener("addElement", function (e)
//			{
//				var element = e.getData().element;
//				var bbox = element.getBBox();
//				var descriptor = {
//					left: bbox.left,
//					top: bbox.top,
//					right: bbox.left + bbox.width,
//					bottom: bbox.top + bbox.height,
//					element: element
//				};
//
//				this.__insertToAxis(this.__xAxis, bbox, "left");
//				this.__insertToAxis(this.__xAxis, bbox, "right");
//				this.__insertToAxis(this.__yAxis, bbox, "top");
//				this.__insertToAxis(this.__yAxis, bbox, "bottom");
//			});
//		},
//		__insertToAxis: function(axis, descriptor, key)
//		{
//			var index = Math.binarySearch(axis, descriptor[key]);
//
//		}

		/**
		 * @param {efwdRenderer.Element} coverElement
		 * @param {Function} [filter=null]
		 * @return {qx.data.Array}
		 */
		handleCoveredElements: function (coverElement, filter)
		{
			var coveredElements = new qx.data.Array();
			var listeners = [];

			function handleElement(element)
			{
				if (filter && !filter(element))
				{
					return;
				}

				listeners.push({
					source: element.onBBoxSet,
					id: element.onBBoxSet.subscribe(
						this.__handleCoveredElementsDoBBoxSet.bind(this, coverElement, coveredElements)
					)
				});
				listeners.push({
					source: element.onDestroy,
					id: element.onDestroy.subscribe(function (){ coveredElements.remove(element) })
				});
			}

			//handle elements
			var elementsListener = this.__сanvasElementsHandling.addListener("addElement", function (e)
			{
				handleElement.call(this, e.getData());
			}, this);
			this.__сanvasElementsHandling.getElements().forEach(function (x){ handleElement.call(this, x) }, this);
			handleElement.call(this, coverElement);

			//extend dispose method
			var oldDispose = coveredElements.dispose;
			var $this = this;
			coveredElements.dispose = function ()
			{
				listeners.forEach(function (x){ x.source.unsubscribe(x.id) });
				$this.__сanvasElementsHandling.removeListenerById(elementsListener);
				oldDispose.call(coveredElements);
			};

			//first process
			this.__handleCoveredElementsDoBBoxSet(coverElement, coveredElements);

			return coveredElements;
		},

		/**
		 * @param {Object} point
		 * @param {Function} [filter=null]
		 * @return {Element[]}
		 */
		getElementsUnderPoint: function (point, filter)
		{
			var elements = this.__сanvasElementsHandling.getElements();
			if (filter)
			{
				elements = elements.filter(filter);
			}

			return elements.filter(function (element)
			{
				return efwdRenderer.Geo.isPointInRect(point, element.getBBox());
			});
		},

		__handleCoveredElementsDoBBoxSet: function (coverElement, coveredElements)
		{
			this.__сanvasElementsHandling.getElements().forEach(function (element)
			{
				if (element != coverElement)
				{
					var covered = efwdRenderer.Geo.isRectCoveredByRect(element.getBBox(),
						coverElement.getBBox());
					var index = coveredElements.indexOf(element);
					if (covered && index === -1)
					{
						coveredElements.push(element);
					}
					else if (!covered && index !== -1)
					{
						coveredElements.removeAt(index);
					}
				}
			}, this);
		}

	}
});
