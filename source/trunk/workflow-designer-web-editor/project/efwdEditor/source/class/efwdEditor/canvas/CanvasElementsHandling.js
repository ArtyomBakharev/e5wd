/**
 * Менеджер графических элементов на канве. Конструирует и отрисовывает виды элементов.
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdEditor.canvas.CanvasElementsHandling", {

	extend: qx.core.Object,

	events: {
		/**
		 * Добавлен новый графический элемент
		 * @param {efwdRenderer.Element} element
		 * @param {efwdDal.entity.AbstractEntity} entity
		 */
		"addElement": "qx.event.type.Data",

		/**
		 * Диаграмма была отрисована
		 */
		"diagramRendered": "qx.event.type.Event"
	},

	/**
	 * @param {library.IServiceLocator} serviceLocator
	 * @param {efwdDal.entity.Process} process
	 * @param {efwdEditor.canvas.views.ElementsRenderer} elementsRenderer
	 */
	construct: function (serviceLocator, process, elementsRenderer)
	{
		this.__serviceLocator = serviceLocator;
		this.__process = process;
		this.__elementsRenderer = elementsRenderer;

		this.__graphicElements = {};
		this.__diagramRendered = false;
	},

	members: {

		/**
		 * @return {efwdRenderer.Element[]}
		 */
		getElements: function ()
		{
			return Object.getValues(this.__graphicElements);
		},

		/**
		 * @return {efwdDal.EntitiesArray} массив сущностей, для которых отрисованы графические элементы
		 */
		getEntitiesArray: function ()
		{
			return this.__elementsArray;
		},

		/**
		 * получить вид сущности на канве
		 * @param {efwdDal.entity.AbstractEntity} entity
		 * @return {efwdRenderer.Element}
		 */
		getElementByEntity: function (entity)
		{
			var id = Object.getUniqueId(entity);
			return this.__graphicElements[id];
		},

		/**
		 * Отрисованный процесс
		 *
		 * @return {efwdDal.entity.Process}
		 */
		getProcess: function ()
		{
			return this.__process;
		},

		/**
		 * Вызывает callback, когда добавлен графический элемент типа type
		 *
		 * @param {String} type
		 * @param {Function} callback
		 * @param {Object} context
		 */
		handleElementsByType: function (type, callback, context)
		{
			this.addListener("addElement", function (e)
			{
				var entity = e.getData().entity;
				if (entity && entity.getEntityType() == type)
				{
					callback.call(context || window, e.getData().element, entity);
				}
			});
		},

		/**
		 * Возвращает true если процесс первичной отрисовки диаграммы закончен
		 *
		 * @return {Boolean}
		 */
		isDiagramRendered: function ()
		{
			return this.__diagramRendered;
		},

		/**
		 * Отрисовывает элементы на канву
		 */
		renderElements: function ()
		{
			this.__renderElements();
		},

		/**
		 * @private
		 */
		__renderElements: function ()
		{
			var statusesArray;
			var actionsArray;

			library.Deferred.parallel([
				this.__process.getStatuses().deferred(),
				this.__process.getStatusChangeActions().deferred()
			]).next(
				function (statuses, actions)
				{
					statuses.forEach(this.__renderGraphicElement, this);
					actions.forEach(this.__renderGraphicElement, this);

					this.__setUpElementsManagement(efwdDal.EntitiesArray.mergeArrays(statuses, actions));
					this.__diagramRendered = true;
					this.fireEvent("diagramRendered");
				}, this);
		},

		/**
		 * @param {efwdDal.entity.AbstractEntity} entity
		 * @return {Deferred}
		 * @private
		 */
		__renderGraphicElement: function (entity)
		{
			var graphicElement = this.__elementsRenderer.renderGraphicElement(entity);

			var id = Object.getUniqueId(entity);
			this.__graphicElements[id] = graphicElement;
			graphicElement.onDestroy.subscribe(function ()
			{
				delete this.__graphicElements[id];
			}, this);

			this.fireDataEvent("addElement", {element: graphicElement, entity: entity});

			return graphicElement;
		},

		/**
		 * @param {efwdDal.EntitiesArray} elementsArray
		 * @private
		 */
		__setUpElementsManagement: function (elementsArray)
		{
			this.__elementsArray = elementsArray;
			elementsArray.addListener("addEntity", function (e)
			{
				this.__renderGraphicElement(e.getData().entity);
			}, this);
		}
	}
});
