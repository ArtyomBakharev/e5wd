/*
 #ignore(efwdConfig.)
 #ignore(efwdConfig.*)
 */

/**
 * Расчитывает привязку графических элементов на канве к сетке
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdEditor.canvas.CanvasGridModel", {

	extend: qx.core.Object,

	/**
	 * @param {efwdEditor.canvas.views.Canvas} canvas
	 */
	construct: function (canvas)
	{
		this.__canvas = canvas;
		this.__keyManager = library.ui.management.KeysManager.getInstance();
	},

	members: {
		/**
		 * @return {Number}
		 */
		getGridLength: function()
		{
			return efwdConfig.efwdEditor.canvas.$style.defaultGridLength;
		},

		/**
		 * Перемещает точку/центр элемента так, чтобы точка/элемент не выходила за пределы канвы
		 *
		 * @param {Object} point
		 * @param {efwdRenderer.Element} element
		 * @param {efwdRenderer.Element[]|Object} [groupOrBBox=null]
		 * @return {Object}
		 */
		snapToBounds: function (point, element, groupOrBBox)
		{
			return this.__snapPoint(point, element, groupOrBBox, false);
		},

		/**
		 * Возвращает точку привязанную к сетке на основе переданной точки
		 *
		 * @param {Object} point
		 * @param {efwdRenderer.Element} [element=null]
		 * @param {efwdRenderer.Element[]|Object} [groupOrBBox=null]
		 * @return {Object}
		 */
		snapToGrid: function (point, element, groupOrBBox)
		{
			return this.__snapPoint(point, element, groupOrBBox, !this.__keyManager.isKeyPressedByIdentifier("Control"));
		},

		/**
		 * @param {Object} point
		 * @param {efwdRenderer.Element} [element=null]
		 * @param {efwdRenderer.Element[]|Object} [groupOrBBox=null]
		 * @param {Boolean} snapToGrid
		 * @return {Object}
		 */
		__snapPoint: function (point, element, groupOrBBox, snapToGrid)
		{
			var leftTop;

			if (snapToGrid)
			{
				point = this.__snapPointToGrid(point);
			}

			if (element)
			{
				var elementBBox = element.getBBox();
				var groupBBox = groupOrBBox
					? Array.isArray(groupOrBBox) ? efwdRenderer.Helper.getBounds(groupOrBBox) : groupOrBBox
					: elementBBox;

				leftTop = {
					x: elementBBox.left + elementBBox.width / 2 - groupBBox.left,
					y: elementBBox.top + elementBBox.height / 2 - groupBBox.top
				};

				if (snapToGrid)
				{
					leftTop = this.__snapPointToGrid(leftTop, Math.ceil);
				}
				else
				{
					leftTop.x = Math.ceil(leftTop.x);
					leftTop.y = Math.ceil(leftTop.y);
				}
			}
			else
			{
				leftTop = {x: 0, y: 0};
			}

			return {
				x: Math.max(point.x, leftTop.x),
				y: Math.max(point.y, leftTop.y)
			};
		},

		/**
		 * @param {Object} point
		 * @param {Function} func
		 * @return {Object}
		 * @private
		 */
		__snapPointToGrid: function (point, func)
		{
			if (!func)
			{
				func = Math.round;
			}
			var gridLength = efwdConfig.efwdEditor.canvas.$style.defaultGridLength;
			return {
				x: func(point.x / gridLength) * gridLength,
				y: func(point.y / gridLength) * gridLength
			};
		}
	}
});

