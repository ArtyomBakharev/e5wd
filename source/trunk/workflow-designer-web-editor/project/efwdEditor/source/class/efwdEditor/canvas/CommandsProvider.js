qx.Class.define("efwdEditor.canvas.CommandsProvider", {
	extend: qx.core.Object,

	/**
	 * @param {efwdEditor.behavior.SelectionModel} selectionModel
	 * @param {library.IServiceLocator} serviceLocator
	 * @param {efwdUI.commands.Queue} queue
	 * @param {efwdUI.behavior.PageModel} pageModel
	 * @param {efwdEditor.behavior.Clipboard} clipboard
	 * @param {efwdEditor.canvas.CanvasElementsHandling} canvasElementsHandling
	 * @param {efwdEditor.behavior.manipulationtool.Model} manipulationToolModel
	 * @param {efwdEditor.canvas.CanvasGridModel} canvasGridModel
	 */
	construct: function (selectionModel, serviceLocator, queue, pageModel, clipboard, canvasElementsHandling,
	                     manipulationToolModel, canvasGridModel)
	{
		this.__selectionModel = selectionModel;
		this.__serviceLocator = serviceLocator;
		this.__queue = queue;
		this.__pageModel = pageModel;
		this.__clipboard = clipboard;
		this.__canvasElementsHandling = canvasElementsHandling;
		this.__manipulationToolModel = manipulationToolModel;
		this.__canvasGridModel = canvasGridModel;
	},

	members: {

		/**
		 * @return {library.ui.Command}
		 * @cacheResult
		 */
		getAddActionToSelectedStatusCommand: function ()
		{
			var selection = this.__selectionModel.getSelection();
			var command = new library.ui.Command(function ()
			{
				var status = selection.getItem(0).getEntity();
				var actionTool = new efwdEditor.behavior.manipulationtool.ActionTool();
				this.__manipulationToolModel.setTool(actionTool);
				actionTool.setSourceStatus(status);
			}, this);

			command.set({
				label: "Add action",
				icon: efwdUI.ui.IconsStore.icons.action
			});

			function checkEnabled()
			{
				command.setEnabled(selection.getLength() == 1 && selection.getItem(0).isOfType("status"));
			}

			selection.addListener("change", checkEnabled, this);
			checkEnabled.call(this);

			return command;
		},

		/**
		 * @return {library.ui.Command}
		 * @cacheResult
		 */
		getCopyCommand: function ()
		{
			var command = new library.ui.Command(function ()
			{
				this.__clipboard.copy(this.__getSelectedEntities());
			}, this);

			command.set({
				label: "Copy",
				icon: efwdUI.ui.IconsStore.icons.copy
			});

			library.data.Helper.multiBind(
				this.__pageModel, "readOnly",
				this.__selectionModel.getSelection(), "count",
				command, "enabled",
				function (readOnly, count)
				{
					return !readOnly && count > 0
				});

			return command;
		},

		/**
		 * @return {library.ui.Command}
		 * @cacheResult
		 */
		getCreateStatusCommand: function ()
		{
			var command = new library.ui.Command(function ()
			{
				var point = command.getData().point;
				var createStatusCommand = this.__serviceLocator.getService(efwdUI.commands.types.CreateStatus);
				createStatusCommand.initialize((function (status)
				{
					return this.__canvasGridModel.snapToGrid(point,
						this.__canvasElementsHandling.getElementByEntity(status));
				}).bind(this));

				this.__queue.addCommand(createStatusCommand);
			}, this);

			command.set({
				label: "Add status",
				icon: efwdUI.ui.IconsStore.icons.status
			});

			return command;
		},

		/**
		 * Возвращает команду, удаляющую выделенный на канве элемент
		 * @return {library.ui.Command}
		 * @cacheResult
		 */
		getDeleteElementsCommand: function ()
		{
			var command = new library.ui.Command(function ()
			{
				var deleteCommand = this.__serviceLocator.getService(efwdUI.commands.types.DeleteEntities);
				deleteCommand.initialize(this.__getSelectedEntities());
				this.__queue.addCommand(deleteCommand);
			}, this);

			command.set({
				label: "Delete elements",
				icon: efwdUI.ui.IconsStore.icons["delete"]
			});


			library.data.Helper.multiBind(
				this.__pageModel, "readOnly",
				this.__selectionModel.getSelection(), "count",
				command, "enabled",
				function (readOnly, count)
				{
					return !readOnly && count > 0
				});

			return command;
		},

		/**
		 * @return {library.ui.Command}
		 * @cacheResult
		 */
		getDeselectAllElementsCommand: function ()
		{
			var command = new library.ui.Command(function ()
			{
				this.__selectionModel.deselectAll();
			}, this);

			command.set({
				label: "Deselect all"
			});

			return command;
		},

		/**
		 * @return {library.ui.Command}
		 * @cacheResult
		 */
		getPasteCommand: function ()
		{
			var command = new library.ui.Command(function ()
			{
				/** @type {efwdEditor.commands.types.PasteEntities} */
				var pasteCommand = this.__serviceLocator.getService(efwdEditor.commands.types.PasteEntities);
				this.__queue.addCommand(pasteCommand);
			}, this);

			command.set({
				label: "Paste",
				icon: efwdUI.ui.IconsStore.icons.paste
			});

			this.__clipboard.bind("empty", command, "enabled", {converter: function (x){ return !x }});

			return command;
		},

		/**
		 * @return {library.ui.Command}
		 * @cacheResult
		 */
		getSelectAllElementsCommand: function ()
		{
			var command = new library.ui.Command(function ()
			{
				this.__selectionModel.selectExact(this.__canvasElementsHandling.getElements());
			}, this);

			command.set({
				label: "Select all",
				icon: efwdUI.ui.IconsStore.icons.selectAll
			});

			return command;
		},

		__getSelectedEntities: function ()
		{
			return this.__selectionModel.getSelection().toArray()
				.map(function (x){ return x.getEntity() }).filter(function (x){ return !!x });
		}
	},

	defer: function (statics, members)
	{
		members.getAddActionToSelectedStatusCommand = members.getAddActionToSelectedStatusCommand.cacheResult();
		members.getCopyCommand = members.getCopyCommand.cacheResult();
		members.getCreateStatusCommand = members.getCreateStatusCommand.cacheResult();
		members.getDeleteElementsCommand = members.getDeleteElementsCommand.cacheResult();
		members.getDeselectAllElementsCommand = members.getDeselectAllElementsCommand.cacheResult();
		members.getPasteCommand = members.getPasteCommand.cacheResult();
		members.getSelectAllElementsCommand = members.getSelectAllElementsCommand.cacheResult();
	}
});
