/**
 * Контроллер канвы графического редактора
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdEditor.canvas.Controller", {

	extend: qx.core.Object,

	/**
	 * @param {efwdEditor.canvas.views.Canvas} canvas
	 * @param {efwdEditor.canvas.CanvasElementsHandling} сanvasElementsHandling
	 * @param {efwdEditor.behavior.manipulationtool.Model} manipulationToolModel
	 * @param {efwdEditor.canvas.behavior.ElementsSelection} selectionBehavior
	 * @param {efwdEditor.canvas.behavior.ElementsCreation} elementsCreationBehavior
	 * @param {efwdEditor.canvas.behavior.ElementsMove} elementsMoveBehavior
	 * @param {efwdEditor.canvas.behavior.ElementsEdit} elementsEditBehavior
	 * @param {efwdEditor.canvas.behavior.ResizeAndScrollCanvas} resizeCanvasBehavior
	 * @param {efwdEditor.canvas.behavior.ConnectorsTransformation} connectorsTransformation
	 * @param {efwdEditor.canvas.behavior.ActivitiesListsShowing} activitiesListsShowing
	 * @param {efwdEditor.canvas.behavior.SelectedElementTool} selectedElementTool
	 * @param {efwdEditor.canvas.behavior.MoveActionPinPoint} moveActionPinPoint
	 * @param {efwdEditor.canvas.behavior.ContextMenu} contextMenu
	 * @param {efwdRenderer.Manager} rendererManager
	 * @param {efwdRenderer.SchemaCompiler} schemaCompiler
	 */
	construct: function (canvas, сanvasElementsHandling, manipulationToolModel, selectionBehavior,
	                     elementsCreationBehavior, elementsMoveBehavior, elementsEditBehavior, resizeCanvasBehavior,
	                     connectorsTransformation, activitiesListsShowing, selectedElementTool, moveActionPinPoint,
	                     contextMenu, rendererManager, schemaCompiler)
	{
		this.__canvas = canvas;
		this.__сanvasElementsHandling = сanvasElementsHandling;
		this.__manipulationToolModel = manipulationToolModel;
		this.__selectionBehavior = selectionBehavior;
		this.__elementsCreationBehavior = elementsCreationBehavior;
		this.__elementsMoveBehavior = elementsMoveBehavior;
		this.__elementsEditBehavior = elementsEditBehavior;
		this.__resizeCanvasBehavior = resizeCanvasBehavior;
		this.__connectorsTransformation = connectorsTransformation;
		this.__activitiesListsShowing = activitiesListsShowing;
		this.__selectedElementTool = selectedElementTool;
		this.__moveActionPinPoint = moveActionPinPoint;
		this.__contextMenu = contextMenu;
		this.__rendererManager = rendererManager;
		this.__schemaCompiler = schemaCompiler;
	},

	members: {
		initialize: function ()
		{
			qx.require(efwdEditor.canvas.ElementExtends);

			this.__schemaCompiler.setSerializer(qx.util.Serializer.toNativeObject.bind(qx.util.Serializer));
			this.__schemaCompiler.registerSchemaPart(efwdConfig.efwdEditor.canvas.$schema);

			this.__setUpGeneralBehavior();

			[
				this.__selectionBehavior,
				this.__elementsCreationBehavior,
				this.__elementsMoveBehavior,
				this.__elementsEditBehavior,
				this.__resizeCanvasBehavior,
				this.__connectorsTransformation,
				this.__activitiesListsShowing,
				this.__selectedElementTool,
				this.__moveActionPinPoint,
				this.__contextMenu
			].forEach(function (x){ x.initialize() });
		},

		/**
		 * @cacheResult
		 * @return {qx.ui.core.Widget} виджет содержащий канву
		 */
		getCanvasWidget: function ()
		{
			this.__canvas.addListener("renderingEnabled", function ()
			{
				this.__rendererManager.setPaper(this.__canvas.getPaper());
				this.__startLoading();
				this.__сanvasElementsHandling.renderElements();
			}, this);

			this.__сanvasElementsHandling.addListener("diagramRendered", function ()
			{
				this.__stopLoading();
			}, this);

			var widget = this.__canvas.render();

			return widget;
		},

		__setUpGeneralBehavior: function ()
		{
			this.__canvas.addListener("rmbMousedown", function (){ this.__manipulationToolModel.resetTool() }, this);
		},

		__startLoading: function ()
		{
			this.__loader = new library.bom.Loader();
			this.__loader.block();
		},

		__stopLoading: function ()
		{
			this.__loader.unblock();
			this.__loader = null;
		}
	},

	defer: function (statics, members, properties)
	{
		members.initialize = members.initialize.forbidMultiCall();
		members.getCanvasWidget = members.getCanvasWidget.cacheResult();
	}

});
