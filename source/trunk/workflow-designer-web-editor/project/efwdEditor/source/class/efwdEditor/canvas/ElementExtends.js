/*
 #ignore(efwdRenderer.)
 #ignore(efwdRenderer.*)
 */

/**
 * Расширение типа {@link efwdRenderer.Element}
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdEditor.canvas.ElementExtends", {

	extend: qx.core.Object,

	defer: function (statics, members)
	{
		qx.lang.Object.merge(efwdRenderer.Element.prototype,
			/**
			 * @lends {efwdRenderer.Element}
			 */
			{

				/**
				 * Добавить обработчик элементу (click, mousedown и тд)
				 * @param {String} event тип события
				 * @param {Function} listener
				 * @param {Object} context
				 */
				addListener: function (event, listener, context)
				{
					event.split(",").forEach(function (x){ this.__addListener(x, listener, context) }, this);
				},

				disableHovering: function ()
				{
					this.__hoveringEnabled = false;
				},

				/**
				 * При наведении курсора на элемент будет вызван метод {@link efwdRenderer.Element#hover}
				 * при удалении курсора с элемента - метод  {@link efwdRenderer.Element#unhover}
				 */
				enableHovering: function ()
				{
					this.__hoveringEnabled = true;
					if (!this.__hoveringListened)
					{
						this.__hoveringListened = true;
						this.listenHover(function (){ this.__hoveringEnabled && this.hover() },
							function (){ this.__hoveringEnabled && this.unhover() }, this);
					}
				},

				/**
				 * Возвращает центр элемента, элемент должен быть типа "positioned"
				 *
				 * @return {Object}
				 */
				getCenter: function ()
				{
					if (qx.core.Environment.get("qx.debug"))
					{
						qx.core.Assert.assert(this.isOfType("positioned"),
							"Not positioned element doesn't have the center");
					}

					return {x: this.getProperty("x"), y: this.getProperty("y")};
				},

				/**
				 * Возвращает сущность связанный с элементом
				 *
				 * @return {efwdDal.entity.AbstractEntity}
				 */
				getEntity: function ()
				{
					return this.getUserData("entity");
				},

				/**
				 * Назначает элементу свойство "hovered"
				 */
				hover: function ()
				{
					this.setProperty("hovered", true);
				},

				/**
				 * Убирает у элемента свойство "selected"
				 */
				deselect: function ()
				{
					this.setProperty("selected", false);
				},

				/**
				 * Назначить элементу обработчики mouseenter и mouseleave
				 *
				 * @param {Function} mouseenter
				 * @param {Function} mouseleave
				 * @param {Object} context
				 */
				listenHover: function (mouseenter, mouseleave, context)
				{
					var elementsSet = this.getAllElementsSet();
					elementsSet.addListener("mouseover", mouseenter, context);
					elementsSet.addListener("mouseout", mouseleave, context);

					this.onAddChildElement.subscribe(function (name, element)
					{
						element.listenHover(mouseenter, mouseleave, context);
					});
					this.onRenderChildElement.subscribe(function (name, element)
					{
						element.listenHover(mouseenter, mouseleave, context);
					});
				},

				/**
				 * Назначить элементу обработчик перемещения
				 *
				 * @param {efwdEditor.canvas.views.Canvas} canvas
				 * @param {Function} startMove
				 * @param {Function} move (point)
				 * @param {Function} [moved=null] (endPoint, startPoint)
				 * @param {Object} [context=windows] контекст для функций startMove, move, moved и onDestroy
				 * @param {Function} [onDestroy=null]
				 */
				listenMove: function (canvas, startMove, move, moved, context, onDestroy)
				{
					var $this = this;
					efwdEditor.canvas.views.Helper.listenElementMove(this.getAllElementsSet(), canvas, startMove, move,
						moved, context, onDestroy,
						function ()
						{
							return $this.isOfType("positioned") ? $this.getCenter() : canvas.getCursorPosition();
						},
						function ()
						{
							return !$this.isDestroyed();
						});
				},

				/**
				 * Назначает элементу свойство "selected"
				 */
				select: function ()
				{
					this.setProperty("selected", true);
				},

				/**
				 * Убирает у элемента свойство "hovered"
				 */
				unhover: function ()
				{
					this.setProperty("hovered", false);
				},

				/**
				 * @param {String} event
				 * @param {Function} listener
				 * @param {Object} context
				 * @private
				 */
				__addListener: function (event, listener, context)
				{
					if (event == "click")
					{
						this.__listenClick(listener, context);
					}
					else
					{
						this.getAllElementsSet().addListener(event, listener, context);
					}

					this.onAddChildElement.subscribe(function (name, element)
					{
						element.__addListener(event, listener, context);
					});
					this.onRenderChildElement.subscribe(function (name, element)
					{
						element.__addListener(event, listener, context);
					});
				},

				/**
				 * @param {Function} listener
				 * @param {Object} context
				 * @private
				 */
				__listenClick: function (listener, context)
				{
					var startX;
					var startY;
					this.getAllElementsSet().addDragListener(null,
						function (x, y)
						{
							startX = x;
							startY = y;
						}, function (e)
						{
							if (efwdRenderer.Geo.isPointsEquals({x: startX, y: startY}, {x: e.clientX, y: e.clientY}, 3))
							{
								listener.call(context || window, e)
							}
						});
				}
			});
	}
});
