/**
 * Класс отвечает за открытие списков активностей для элементов отрисованных на канве
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdEditor.canvas.behavior.ActivitiesListsShowing", {

	extend: qx.core.Object,

	/**
	 * @param {efwdEditor.canvas.CanvasElementsHandling} canvasElementsHandling
	 * @param {efwdEditor.behavior.manipulationtool.Model} manipulationToolModel
	 * @param {efwdEditor.behavior.DialogsModel} dialogsModel
	 */
	construct: function (canvasElementsHandling, manipulationToolModel, dialogsModel)
	{
		this.__canvasElementsHandling = canvasElementsHandling;
		this.__manipulationToolModel = manipulationToolModel;
		this.__dialogsModel = dialogsModel;
	},

	members: {
		initialize: function ()
		{
			this.__canvasElementsHandling.addListener("addElement", function (e)
			{
				var element = e.getData().element;
				var entity = e.getData().entity;

				if (entity instanceof efwdDal.entity.Status || entity instanceof efwdDal.entity.Action)
				{
					this.__setUpActivitiesIndicatorOwner(element);
				}
			}, this);

			this.__dialogsModel.addListener("openActivitiesListDialog", function(e){
				this.__changeActivityIndicatorState(e.getData().entity, true);
			}, this);
			this.__dialogsModel.addListener("closeActivitiesListDialog", function(e){
				this.__changeActivityIndicatorState(e.getData().entity, false);
			}, this);
		},

		/**
		 * @param {efwdDal.entity.AbstractEntity} entity
		 * @param {Boolean} value
		 * @private
		 */
		__changeActivityIndicatorState: function(entity, value)
		{
			var element = this.__canvasElementsHandling.getElementByEntity(entity);
			if (!element)
			{
				return;
			}

			var activitiesIndicator = this.__getActivitiesIndicator(element);

			if (value)
			{
				activitiesIndicator.select();
			}
			else
			{
				activitiesIndicator.deselect();
			}
		},

		/**
		 * @param {efwdRenderer.Element} element
		 * @return {efwdRenderer.Element}
		 * @private
		 */
		__getActivitiesIndicator: function(element)
		{
			return element.getEntity() instanceof efwdDal.entity.Status
				? element.getChild("activitiesIndicator")
				: element.getChild("label").getChild("activitiesIndicator");
		},

		/**
		 * @param {efwdRenderer.Element} element
		 * @private
		 */
		__setUpActivitiesIndicatorOwner: function (element)
		{
			var entity = element.getEntity();
			var activitiesIndicator = this.__getActivitiesIndicator(element);

			activitiesIndicator.enableHovering();

			activitiesIndicator.addListener("click", function (e)
			{
				if (this.__manipulationToolModel.isToolDefault() && !e.shiftKey)
				{
					this.__dialogsModel.toggleActivitiesListDialog(entity);
				}
			}, this);
		}
	},

	defer: function (statics, members, properties)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}

});
