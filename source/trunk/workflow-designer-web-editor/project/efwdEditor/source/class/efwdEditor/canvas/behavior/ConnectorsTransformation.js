/*
 #ignore(efwdRenderer)
 #ignore(efwdRenderer.*)
 */

/**
 * Класс, отвечающий за трансформацию коннекторов на канве
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdEditor.canvas.behavior.ConnectorsTransformation", {

	extend: qx.core.Object,

	/**
	 * @param {efwdEditor.canvas.views.Canvas} canvas
	 * @param {efwdEditor.canvas.CanvasGridModel} canvasGridModel
	 * @param {efwdEditor.behavior.manipulationtool.Model} manipulationToolModel
	 * @param {library.IServiceLocator} serviceLocator
	 * @param {efwdUI.commands.Queue} queue
	 * @param {efwdEditor.canvas.CanvasElementsHandling} canvasElementsHandling
	 * @param {efwdEditor.canvas.behavior.ResizeAndScrollCanvas} resizeCanvasBehavior
	 * @param {efwdUI.behavior.PageModel} pageModel
	 * @param {efwdEditor.behavior.SelectionModel} selectionModel
	 */
	construct: function (canvas, canvasGridModel, manipulationToolModel, serviceLocator, queue, canvasElementsHandling,
	                     resizeCanvasBehavior, pageModel, selectionModel)
	{
		this.__canvas = canvas;
		this.__canvasGridModel = canvasGridModel;
		this.__manipulationToolModel = manipulationToolModel;
		this.__serviceLocator = serviceLocator;
		this.__queue = queue;
		this.__canvasElementsHandling = canvasElementsHandling;
		this.__resizeCanvasBehavior = resizeCanvasBehavior;
		this.__pageModel = pageModel;
		this.__selectionModel = selectionModel;
	},

	members: {
		initialize: function ()
		{
			if (this.__pageModel.isReadOnly())
			{
				return;
			}

			this.__canvasElementsHandling.handleElementsByType("action",
				function (element, entity){ this.__setUpConnectorsTransformationBehavior(element, entity) }, this);
		},

		/**
		 * @param {efwdRenderer.Element} element
		 * @private
		 */
		__setUpConnectorsTransformationBehavior: function (element, entity)
		{
			this.__setUpLabelMoving(element);

			var jointsManager = this.__serviceLocator.getService(efwdEditor.canvas.behavior.tool.ConnectorJointsManaging);
			jointsManager.initialize(element);
		},

		/**
		 * @param {efwdRenderer.Element} element
		 * @private
		 */
		__setUpLabelMoving: function (element)
		{
			var entity = element.getEntity();
			var labelPosition = entity.getSource().getMetadata().getLabelPosition();
			var label = element.getChild("label");
			var delta;
			var startPoint;
			var command;

			function commit()
			{
				this.__queue.addCommand(command);
				command = null;
				this.__resizeCanvasBehavior.cancelFollowCursor();
			}

			//отслеживаем перемещение имени
			label.listenMove(this.__canvas,
				function ()
				{
					//место крепления
					delta = {x: label.getProperty("x") - labelPosition.getX(), y: label.getProperty("y") - labelPosition.getY()};
					if (!this.__manipulationToolModel.isToolDefault() || this.__selectionModel.isMultiselect())
					{
						return false;
					}

					command = this.__serviceLocator.getService(efwdEditor.commands.types.MoveActionLabel);
					command.initialize(element.getEntity());
					startPoint = qx.util.Serializer.toNativeObject(labelPosition);

					this.__resizeCanvasBehavior.followCursor();

				}, function (to)
				{
					command.setPoint(efwdRenderer.Geo.subPoints(this.__canvasGridModel.snapToBounds(to, label), delta));
					command.execute(false);

				}, function (to, from)
				{
					commit.call(this);

				}, this);

			entity.addListenerOnce("removing", function(){
				if (command)
				{
					commit.call(this);
				}
			}, this);
		}
	},

	defer: function (statics, members, properties)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}

});
