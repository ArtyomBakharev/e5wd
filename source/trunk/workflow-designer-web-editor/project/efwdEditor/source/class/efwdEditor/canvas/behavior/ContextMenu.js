qx.Class.define("efwdEditor.canvas.behavior.ContextMenu", {

	extend: qx.core.Object,

	/**
	 * @param {efwdEditor.canvas.views.Canvas} canvas
	 * @param {efwdEditor.behavior.manipulationtool.Model} manipulationToolModel
	 * @param {efwdEditor.behavior.SelectionModel} selectionModel
	 * @param {efwdEditor.canvas.CommandsProvider} canvasCommandsProvider
	 * @param {efwdUI.behavior.GeneralCommandsProvider} uiCommandsProvider
	 * @param {efwdUI.behavior.PageModel} PageModel
	 */
	construct: function (canvas, manipulationToolModel, selectionModel, canvasCommandsProvider, uiCommandsProvider, pageModel)
	{
		this.__canvas = canvas;
		this.__manipulationToolModel = manipulationToolModel;
		this.__selectionModel = selectionModel;
		this.__canvasCommandsProvider = canvasCommandsProvider;
		this.__uiCommandsProvider = uiCommandsProvider;
		this.__pageModel = pageModel;
	},

	members: {
		/**
		 * @forbidMultiCall
		 */
		initialize: function ()
		{
			if (this.__pageModel.isReadOnly())
			{
				return;
			}

			this.__canvas.addListener("contextmenu", function (e)
			{
				if (this.__manipulationToolModel.isToolDefault())
				{
					this.__showMenu(e.getData().point);
				}
			}, this);
		},

		/**
		 * @param {Object} point
		 * @return {Array}
		 * @private
		 */
		__getMenuStructure: function (point)
		{
			var selection = this.__selectionModel.getSelection();
			var element = selection.getLength() == 1 && selection.getItem(0);
			var selectionEmpty = selection.getLength() == 0;
			var commands = [];

			if (selectionEmpty)
			{
				var createStatusCommand = this.__canvasCommandsProvider.getCreateStatusCommand();
				createStatusCommand.setData({point: point});
				commands.push(createStatusCommand);
			}

			if (element && element.isOfType("status"))
			{
				commands.push(this.__canvasCommandsProvider.getAddActionToSelectedStatusCommand());

				var setAsInitialStatusCommand = this.__uiCommandsProvider.getSetAsInitialStatusCommand();
				setAsInitialStatusCommand.setData({status: element.getEntity()});
				commands.push(setAsInitialStatusCommand);
			}

			commands.push("-");

			if (!selectionEmpty)
			{
				commands.push(this.__canvasCommandsProvider.getDeleteElementsCommand());
				commands.push(this.__canvasCommandsProvider.getCopyCommand());
			}

			if (selectionEmpty)
			{
				commands.push(this.__canvasCommandsProvider.getPasteCommand());
			}

			commands.push(this.__canvasCommandsProvider.getSelectAllElementsCommand());

			return commands;
		},

		/**
		 * @param {Object} point
		 * @private
		 */
		__showMenu: function (point)
		{
			var menuStructure = this.__getMenuStructure(point);

			var menu = new qx.ui.menu.Menu();
			var emptySection = true;
			menuStructure.forEach(function (item)
			{
				if (item == "-")
				{
					if (!emptySection)
					{
						menu.addSeparator();
					}
					emptySection = true;
				}
				else
				{
					emptySection = false;
					var button = new qx.ui.menu.Button();
					button.setIconSize(22);
					button.setCommand(item);

					if (item == this.__canvasCommandsProvider.getDeleteElementsCommand()
						&& this.__selectionModel.getSelection().getLength() == 1)
					{
						button.setLabel("Delete element");
					}

					menu.add(button);
				}
			}, this);

			var position = this.__canvas.calculateDocumentPositionByCanvasPosition(point);
			menu.setOpener(this.__canvas.getPaperContainer());
			menu.openAtPoint({left: position.x, top: position.y});
		}
	},

	defer: function (statics, members, properties)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}

});
