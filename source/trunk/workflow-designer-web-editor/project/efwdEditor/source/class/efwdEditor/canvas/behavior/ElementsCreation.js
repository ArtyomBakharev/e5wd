/**
 * Класс отвечающий за логику создания элементов
 */
qx.Class.define("efwdEditor.canvas.behavior.ElementsCreation", {

	extend: qx.core.Object,

	/**
	 * @param {efwdEditor.behavior.manipulationtool.Model} manipulationToolModel
	 * @param {library.IServiceLocator} serviceLocator
	 * @param {efwdEditor.canvas.CanvasElementsHandling} canvasElementsHandling
	 * @param {efwdUI.behavior.PageModel} pageModel
	 * @param {efwdEditor.canvas.views.Canvas} canvas
	 */
	construct: function (manipulationToolModel, serviceLocator, canvasElementsHandling, pageModel, canvas)
	{
		this.__manipulationToolModel = manipulationToolModel;
		this.__serviceLocator = serviceLocator;
		this.__canvasElementsHandling = canvasElementsHandling;
		this.__pageModel = pageModel;
		this.__canvas = canvas;
	},

	members: {

		initialize: function ()
		{
			if (this.__pageModel.isReadOnly())
			{
				return;
			}

			this.__setUpStatusCreation();
			this.__setUpActionCreation();
		},

		/**
		 * @private
		 */
		__handleChangeActionToolState: function ()
		{
			this.__manipulationToolModel.addListener("changeTool", function (e)
			{
				var tool = e.getData();
				if (tool instanceof efwdEditor.behavior.manipulationtool.ActionTool)
				{
					tool.addListener("changeSourceStatus", function (e)
					{
						/** @type {efwdEditor.canvas.behavior.tool.ActionCreation} */
						this.__actionCreationTool = this.__serviceLocator.getService(efwdEditor.canvas.behavior.tool.ActionCreation);
						this.__actionCreationTool.initialize(e.getData());
						this.__actionCreationTool.addListener("actionCreated", function ()
						{
							this.__manipulationToolModel.resetTool.defer(this.__manipulationToolModel);
						}, this);
					}, this);
				}
				else
				{
					if (this.__actionCreationTool)
					{
						this.__actionCreationTool.dispose();
						this.__actionCreationTool = null;
					}
				}
			}, this);
		},

		__handleStatusesForActionCreation: function ()
		{
			this.__canvasElementsHandling.handleElementsByType("status", function (element, entity)
			{
				function setSourceStatus()
				{
					var tool = this.__manipulationToolModel.getTool();
					if (tool instanceof efwdEditor.behavior.manipulationtool.ActionTool && !tool.getSourceStatus())
					{
						tool.setSourceStatus(entity);
					}
				}

				element.listenMove(this.__canvas, null, setSourceStatus, null, this);
				element.addListener("click", setSourceStatus, this);
			}, this);
		},

		/**
		 * @private
		 */
		__setUpActionCreation: function ()
		{
			this.__handleStatusesForActionCreation();
			this.__handleChangeActionToolState();
		},

		/**
		 * @private
		 */
		__setUpStatusCreation: function ()
		{
			this.__manipulationToolModel.addListener("changeTool", function (e)
			{
				if (e.getData() instanceof efwdEditor.behavior.manipulationtool.StatusTool)
				{
					/** @type {efwdEditor.canvas.behavior.tool.StatusCreation} */
					this.__statusCreationTool = this.__serviceLocator.getService(efwdEditor.canvas.behavior.tool.StatusCreation);
					this.__statusCreationTool.initialize(true);
					this.__statusCreationTool.addListener("statusCreated", function ()
					{
						this.__manipulationToolModel.resetTool.defer(this.__manipulationToolModel);
					}, this);
				}
				else if (this.__statusCreationTool)
				{
					this.__statusCreationTool.dispose();
					this.__statusCreationTool = null;
				}
			}, this);
		}

	},

	defer: function (statics, members, properties)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}

});

