/**
 * Класс отвечающий за логику выбора элемента для редактирования
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdEditor.canvas.behavior.ElementsEdit", {

	extend: qx.core.Object,

	/**
	 * @param {efwdEditor.behavior.SelectionModel} selectionModel
	 * @param {efwdUI.behavior.EntityEditionModel} entityEditionModel
	 * @param {efwdEditor.canvas.CanvasElementsHandling} canvasElementsHandling
	 */
	construct: function (selectionModel, entityEditionModel, canvasElementsHandling)
	{
		this.__selectionModel = selectionModel;
		this.__entityEditionModel = entityEditionModel;
		this.__canvasElementsHandling = canvasElementsHandling;
	},

	members: {

		initialize: function ()
		{
			var selection = this.__selectionModel.getSelection();
			selection.addListener("change", function(){
				var element = selection.getLength() > 0 && selection.getItem(0);
				var entity = element ? element.getEntity() : null;
				if ((entity && !(entity instanceof efwdDal.entity.Action || entity instanceof efwdDal.entity.Status))
					|| this.__selectionModel.isMultiselect())
				{
					entity = null;
				}
				if (entity == this.__entityEditionModel.getEntity())
				{
					return;
				}

				this.__entityEditionModel.setEntity(entity);
			}, this);

			this.__entityEditionModel.addListener("changeEntity", function (e)
			{
				if (this.__selectionModel.isMultiselect())
				{
					return;
				}

				var entity = e.getData();
				if (entity === null
					|| !this.__canvasElementsHandling.getEntitiesArray()
					|| !this.__canvasElementsHandling.getEntitiesArray().getFilter()(entity))
				{
					this.__selectionModel.deselectAll();
				}
				else
				{
					this.__selectionModel.select(this.__canvasElementsHandling.getElementByEntity(entity));
				}
			}, this);
		}
	},

	defer: function (statics, members, properties)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}

});
