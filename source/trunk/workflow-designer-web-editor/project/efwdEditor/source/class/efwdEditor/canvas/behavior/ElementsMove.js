/**
 * Класс отвечающий за логику перемещения элементов
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdEditor.canvas.behavior.ElementsMove", {

	extend: qx.core.Object,

	/**
	 * @param {efwdEditor.canvas.views.Canvas} canvas
	 * @param {efwdEditor.behavior.manipulationtool.Model} manipulationToolModel
	 * @param {library.IServiceLocator} serviceLocator
	 * @param {efwdEditor.canvas.CanvasElementsHandling} canvasElementsHandling
	 * @param {efwdUI.behavior.PageModel} pageModel
	 * @param {efwdEditor.behavior.SelectionModel} selectionModel
	 */
	construct: function (canvas, manipulationToolModel, serviceLocator, canvasElementsHandling, pageModel,
	                     selectionModel)
	{
		this.__canvas = canvas;
		this.__manipulationToolModel = manipulationToolModel;
		this.__serviceLocator = serviceLocator;
		this.__canvasElementsHandling = canvasElementsHandling;
		this.__pageModel = pageModel;
		this.__selectionModel = selectionModel;
	},

	members: {
		initialize: function ()
		{
			if (this.__pageModel.isReadOnly())
			{
				return;
			}

			this.__canvasElementsHandling.handleElementsByType("status", function (element, entity)
			{
				this.__setUpStatusMoveBehavior(element, entity)
			}, this);
		},

		/**
		 * @param {efwdRenderer.Element} element
		 * @param {efwdDal.entity.AbstractEntity} entity
		 * @private
		 */
		__setUpStatusMoveBehavior: function (element, entity)
		{
			/** @type {efwdEditor.canvas.behavior.tool.ElementsMove} */
			var elementsMoveTool;
			element.listenMove(this.__canvas,
				function (center, cursorPosition)
				{
					if (!this.__manipulationToolModel.isToolDefault())
					{
						return false;
					}

					elementsMoveTool = this.__serviceLocator.getService(efwdEditor.canvas.behavior.tool.ElementsMove);
					elementsMoveTool.initialize(element, this.__selectionModel.getSelection().toArray().clone(),
						cursorPosition);
				}, function (to)
				{
					elementsMoveTool.moveTo(this.__canvas.getCursorPosition());
				}, function (to, from)
				{
					elementsMoveTool.commit();
					elementsMoveTool = null;
				}, this);
		}
	},

	defer: function (statics, members, properties)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}

});
