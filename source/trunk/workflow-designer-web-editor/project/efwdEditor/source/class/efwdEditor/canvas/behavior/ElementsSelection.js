/**
 * Класс отвечающий за логику выделения и подсветки элементов на канве
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdEditor.canvas.behavior.ElementsSelection", {

	extend: qx.core.Object,

	/**
	 * @param {efwdEditor.canvas.views.Canvas} canvas
	 * @param {efwdEditor.behavior.manipulationtool.Model} manipulationToolModel
	 * @param {efwdEditor.behavior.SelectionModel} selectionModel
	 * @param {efwdEditor.canvas.CanvasElementsHandling} canvasElementsHandling
	 * @param {efwdUI.behavior.EntityEditionModel} entityEditionModel
	 * @param {efwdEditor.canvas.BBoxesModel} bboxesModel
	 * @param {efwdEditor.canvas.behavior.ResizeAndScrollCanvas} resizeAndScrollCanvasBehavior
	 * @param {efwdRenderer.Factory} rendererFactory
	 */
	construct: function (canvas, manipulationToolModel, selectionModel, canvasElementsHandling, entityEditionModel,
	                     bboxesModel, resizeAndScrollCanvasBehavior, rendererFactory)
	{
		this.__canvas = canvas;
		this.__manipulationToolModel = manipulationToolModel;
		this.__selectionModel = selectionModel;
		this.__canvasElementsHandling = canvasElementsHandling;
		this.__entityEditionModel = entityEditionModel;
		this.__bboxesModel = bboxesModel;
		this.__resizeAndScrollCanvasBehavior = resizeAndScrollCanvasBehavior;
		this.__rendererFactory = rendererFactory;
	},

	members: {
		initialize: function ()
		{
			this.__handleAddElement();
			this.__handleSelectionChanged();
			this.__handleToolChanged();
			this.__handleBgClick();
			this.__handleMultiSelection();
		},

		/**
		 * @private
		 */
		__checkSelectionEnabled: function ()
		{
			return this.__manipulationToolModel.isToolDefault();
		},

		/**
		 * @private
		 */
		__handleAddElement: function ()
		{
			//выбрать элемент при клике на нём и при создании
			this.__canvasElementsHandling.addListener("addElement", function (e)
			{
				var element = e.getData().element;
				var entity = e.getData().entity;

				if (entity)
				{
					this.__setUpSelectElement(element);
					element.enableHovering();

					entity.addListenerOnce("removing", function ()
					{
						this.__handleRemovingEntity(element)
					}, this);
				}

			}, this);
		},

		/**
		 * @private
		 */
		__handleBgClick: function ()
		{
			this.__canvas.addListener("backgroundMousedown", function (e)
			{
				if (this.__checkSelectionEnabled() && !e.getData().origEvent.shiftKey)
				{
					this.__selectionModel.deselectAll();
				}
			}, this);
			this.__canvas.addListener("backgroundMouseup", function (e)
			{
				this.__switchMultiselect();
			}, this);
		},

		/**
		 * @private
		 */
		__handleRemovingEntity: function (element)
		{
			this.__selectionModel.deselectAll();
		},

		__handleMultiSelection: function ()
		{
			this.__canvas.addListener("renderingEnabled", function ()
			{
				var coveredElements;
				var startSelection;
				var selectionBox;
				this.__canvas.listenBackgroundMove(function ()
				{
					if (!this.__checkSelectionEnabled())
					{
						return false;
					}

					this.__selectionModel.setMultiselect(true);
					startSelection = this.__selectionModel.getSelection().toArray().clone();
				}, function (point)
				{
					if (!selectionBox)
					{
						selectionBox = this.__rendererFactory.createElement("selectionBox");
						selectionBox.setProperty("firstPoint", point);
						selectionBox.setProperty("secondPoint", point);
						selectionBox.render();
						this.__resizeAndScrollCanvasBehavior.followCursor();

						coveredElements = this.__handleMultiSelectionHandleCoveredElements(selectionBox, startSelection);
					}
					else
					{
						selectionBox.setProperty("secondPoint", point);
					}
				}, function ()
				{
					selectionBox.destroy();
					selectionBox = null;

					coveredElements.dispose();
					coveredElements = null;
					startSelection = null;
					this.__resizeAndScrollCanvasBehavior.cancelFollowCursor();

					this.__switchMultiselect();
				}, this);
			}, this);
		},

		__handleMultiSelectionHandleCoveredElements: function(selectionBox, startSelection)
		{
			var coveredElements = this.__bboxesModel.handleCoveredElements(selectionBox);
			coveredElements.forEach(function(x){ this.__selectionModel.select(x) }, this);
			coveredElements.addListener("changeArray", function(e){
				var data = e.getData();
				data.remove.forEach(function(x){ if (startSelection.indexOf(x) === -1) this.__selectionModel.deselect(x) }, this);
				data.insert.forEach(function(x){ if (startSelection.indexOf(x) === -1) this.__selectionModel.select(x) }, this);
			}, this);

			return coveredElements;
		},


		/**
		 * @private
		 */
		__handleSelectionChanged: function ()
		{
			//подсветить элемент если он выбран
			var selection = this.__selectionModel.getSelection();
			selection.addListener("changeArray", function (e)
			{
				var data = e.getData();
				data.remove.forEach(function (x){ x.deselect() });
				data.insert.forEach(function (x){ x.select() });
			}, this);
		},

		/**
		 * @private
		 */
		__handleToolChanged: function ()
		{
			this.__manipulationToolModel.addListener("changeTool", function (e)
			{
				if (!this.__manipulationToolModel.isToolDefault())
				{
					this.__selectionModel.deselectAll();
				}
			}, this);
		},

		/**
		 * @param {efwdRenderer.Element} element
		 * @private
		 */
		__setUpSelectElement: function (element)
		{
			var selection = this.__selectionModel.getSelection();
			element.addListener("mousedown", function (e)
			{
				element.bringToFront();
				if (this.__checkSelectionEnabled())
				{
					if (e.shiftKey)
					{
						this.__selectionModel.setMultiselect(true);
						this.__selectionModel.toggle(element);
						this.__switchMultiselect();
					}
					else if (selection.indexOf(element) === -1)
					{
						this.__selectionModel.setMultiselect(false);
						this.__selectionModel.deselectAll();
						this.__selectionModel.select(element);
					}
				}

				if (qx.core.Environment.get("qx.debug"))
				{
					efwdEditor.selected = element;
				}
			}, this);
		},

		__switchMultiselect: function()
		{
			this.__selectionModel.setMultiselect(this.__selectionModel.getSelection().getLength() > 1);
		}
	},

	defer: function (statics, members, properties)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}

});
