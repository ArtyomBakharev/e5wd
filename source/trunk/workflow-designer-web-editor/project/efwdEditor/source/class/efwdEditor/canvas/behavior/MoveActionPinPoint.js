/*
 #ignore(efwdRenderer)
 #ignore(efwdRenderer.*)
 */

/**
 * Класс, отвечающий за трансформацию коннекторов на канве
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdEditor.canvas.behavior.MoveActionPinPoint", {

	extend: qx.core.Object,

	/**
	 * @param {efwdEditor.canvas.views.Canvas} canvas
	 * @param {efwdEditor.behavior.manipulationtool.Model} manipulationToolModel
	 * @param {library.IServiceLocator} serviceLocator
	 * @param {efwdUI.commands.Queue} queue
	 * @param {efwdEditor.canvas.CanvasElementsHandling} canvasElementsHandling
	 * @param {efwdUI.behavior.PageModel} pageModel
	 * @param {efwdEditor.canvas.CanvasGridModel} canvasGridModel
	 */
	construct: function (canvas, manipulationToolModel, serviceLocator, queue, canvasElementsHandling, pageModel,
	                     canvasGridModel)
	{
		this.__canvas = canvas;
		this.__manipulationToolModel = manipulationToolModel;
		this.__serviceLocator = serviceLocator;
		this.__queue = queue;
		this.__canvasElementsHandling = canvasElementsHandling;
		this.__pageModel = pageModel;
		this.__canvasGridModel = canvasGridModel;
	},

	members: {
		initialize: function ()
		{
			if (this.__pageModel.isReadOnly())
			{
				return;
			}

			this.__canvasElementsHandling.handleElementsByType("action", function (element, entity)
			{
				this.__setUpActionPinPoint(element, false);
				this.__setUpActionPinPoint(element, true);
			}, this);
		},

		/**
		 * @param {efwdRenderer.Element} element
		 * @param {Boolean} [isDestPoint=false]
		 * @private
		 */
		__setUpActionPinPoint: function (element, isDestPoint)
		{
			var entity = element.getEntity();
			var pointElement = element.getChild("arrow").getChild(isDestPoint ? "destPinPoint" : "sourcePinPoint");
			pointElement.enableHovering();

			function commit()
			{
				pointElement.setProperty("active", false);
				this.__queue.addCommand(command);
				command = null;
			}

			/** @type {efwdEditor.commands.types.MoveActionConnectorPinPoint} */
			var command;
			pointElement.listenMove(this.__canvas, function ()
			{
				if (!this.__manipulationToolModel.isToolDefault())
				{
					return;
				}

				command = this.__serviceLocator.getService(efwdEditor.commands.types.MoveActionConnectorPinPoint);
				command.initialize(entity, isDestPoint);
				pointElement.setProperty("active", true);
			}, function (to)
			{
				command.movePoint(this.__calculatePointPosition(element, to, isDestPoint));
			}, function ()
			{
				commit.call(this);
			}, this, commit);

			entity.addListenerOnce("removing", function ()
			{
				if (command)
				{
					commit.call(this);
				}
			}, this);
		},

		/**
		 * @param {efwdRenderer.Element} element
		 * @param {Object} position
		 * @param {Boolean} [isDestPoint=false]
		 * @return {Object}
		 * @private
		 */
		__calculatePointPosition: function (element, position, isDestPoint)
		{
			var statusElement = this.__canvasElementsHandling
				.getElementByEntity(element.getEntity().get(isDestPoint ? "destStatus" : "sourceStatus"));

			var bbox = statusElement.getBBox();
			var point = this.__canvasGridModel.snapToGrid(position);

			var gridLength = this.__canvasGridModel.getGridLength();
			if (point.x < bbox.left)
			{
				point.x = Math.ceil(bbox.left / gridLength) * gridLength;
			}
			else if (point.x > bbox.right)
			{
				point.x = Math.floor(bbox.right / gridLength) * gridLength;
			}
			if (point.y < bbox.top)
			{
				point.y = Math.ceil(bbox.top / gridLength) * gridLength;
			}
			else if (point.y > bbox.bottom)
			{
				point.y = Math.floor(bbox.bottom / gridLength) * gridLength;
			}

			var distances = [point.y - bbox.top, bbox.right - point.x, bbox.bottom - point.y, point.x - bbox.left];
			var minDistanceIndex = distances.reduce(function (res, val, i)
			{
				return !res || val < res.val ? {val: val, i: i} : res
			}, false).i;

			switch (minDistanceIndex)
			{
				case 0:
					point = {x: point.x, y: bbox.top};
					break;
				case 1:
					point = {x: bbox.right, y: point.y};
					break;
				case 2:
					point = {x: point.x, y: bbox.bottom};
					break;
				case 3:
					point = {x: bbox.left, y: point.y};
					break;
			}

			return efwdRenderer.Geo.subPoints(point, statusElement.getCenter());
		}
	},

	defer: function (statics, members, properties)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}

});
