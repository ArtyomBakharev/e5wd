/*
 #ignore(efwdConfig)
 #ignore(efwdConfig.*)
 #ignore(efwdRenderer)
 #ignore(efwdRenderer.*)
 */

/**
 * Класс отвечающий за логику изменения размеров канвы. Отслеживает только
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdEditor.canvas.behavior.ResizeAndScrollCanvas", {

	extend: qx.core.Object,

	/**
	 * @param {efwdEditor.canvas.views.Canvas} canvas
	 * @param {efwdEditor.canvas.CanvasElementsHandling} canvasElementsHandling
	 */
	construct: function (canvas, canvasElementsHandling)
	{
		this.__canvas = canvas;
		this.__canvasElementsHandling = canvasElementsHandling;

		this.__elements = {};
	},

	members: {

		initialize: function ()
		{
			this.__canvasElementsHandling.addListener("addElement", function (e)
			{
				var element = e.getData().element;
				this.putElement(element);
			}, this);

			this.__canvasElementsHandling.addListener("diagramRendered", function (){ this.update() }, this);

			this.__canvas.addListener("scroll", function (){ this.update() }, this)
		},

		cancelFollowCursor: function ()
		{
			this.__updateAnchorElementInterval && clearInterval(this.__updateAnchorElementInterval);
			this.__updateAnchorElementInterval = null;
		},

		/**
		 * @param {efwdRenderer.Element} element
		 */
		putElement: function (element)
		{
			var id = Object.getUniqueId(element);
			this.__elements[id] = element;

			element.onDestroy.subscribe(function ()
			{
				delete this.__elements[id];
				this.update();
			}, this);

			if (this.__canvasElementsHandling.isDiagramRendered())
			{
				this.update(element);
			}

			element.onBBoxSet.subscribe(function (){ this.update(element) }, this);
		},

		/**
		 * @param {efwdRenderer.Element} [updatedElement=null]
		 */
		update: function (updatedElement)
		{
			if (!this.__updateFunc || this.__lastUpdateElement != updatedElement)
			{
				this.__lastUpdateElement = updatedElement;
				this.__updateFunc = (function ()
				{
					var bounds = efwdRenderer.Helper.getBounds(Object.getValues(this.__elements));//this.__calcBounds(updatedElement);
					var canvasGap = efwdConfig.efwdEditor.canvas.$style.elementsGap;
					this.__canvas.setSize(bounds.right + canvasGap, bounds.bottom + canvasGap);
				}).throttling(100, this);
			}

			this.__updateFunc.defer(this);
		},

		scrollToCursor: function ()
		{
			var point = this.__canvas.getCursorPosition();
			if (!point)
			{
				return;
			}

			var scroll = this.__canvas.getScroll();
			var viewportSize = this.__canvas.getViewportSize();
			var triggerZone = efwdConfig.efwdEditor.canvas.$style.scrollTriggerZone;
			var edges = {
				left: scroll.x,
				top: scroll.y,
				right: scroll.x + viewportSize.width,
				bottom: scroll.y + viewportSize.height
			};

			if (point.x <= edges.left + triggerZone)
			{
				this.__canvas.scrollToX(scroll.x - (edges.left + triggerZone - point.x));
			}
			if (point.y <= edges.top + triggerZone)
			{
				this.__canvas.scrollToY(scroll.y - (edges.top + triggerZone - point.y));
			}

			if (point.x >= edges.right - triggerZone)
			{
				this.__canvas.scrollToX(scroll.x + point.x - (edges.right - triggerZone));
			}
			if (point.y >= edges.bottom - triggerZone)
			{
				this.__canvas.scrollToY(scroll.y + point.y - (edges.bottom - triggerZone));
			}
		},

		/**
		 * @param {efwdRenderer.Element} element
		 * @param {efwdRenderer.Element} old
		 * @private
		 */
		followCursor: function ()
		{
			if (this.__updateAnchorElementInterval)
			{
				return;
			}

			var $this = this;
			this.__updateAnchorElementInterval = setInterval(function (){ $this.scrollToCursor() }, 100);
		}

//		//todo: optimize it
//		/**
//		 * @private
//		 */
//		__calcBounds: function (updatedElement)
//		{
//			var right = 0;
//			var bottom = 0;
//
////			var elements;
////			if (this.__rightBoundElement)
////			{
////				elements = [this.__rightBoundElement];
////				if (this.__rightBoundElement != this.__bottomBoundElement)
////				{
////					elements.push(this.__bottomBoundElement);
////				}
////				if (updatedElement && elements.indexOf(updatedElement) === -1)
////				{
////					elements.push(updatedElement);
////				}
////			}
////			else
////			{
////				elements = Object.getValues(this.__elements);
////			}
//
////			elements.forEach(function (element)
//			Object.forEach(this.__elements, function(element)
//			{
//				var bbox = element.getBBox();
//				if (bbox.left + bbox.width > right)
//				{
//					right = bbox.left + bbox.width;
////					this.__rightBoundElement = element;
//				}
//
//				if (bbox.top + bbox.height > bottom)
//				{
//					bottom = bbox.top + bbox.height;
////					this.__bottomBoundElement = element;
//				}
//			}, this);
//
//			return {
//				right: right,
//				bottom: bottom
//			};
//		}

	}

});
