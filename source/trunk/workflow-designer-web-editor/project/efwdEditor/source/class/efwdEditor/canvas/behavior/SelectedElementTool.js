qx.Class.define("efwdEditor.canvas.behavior.SelectedElementTool", {

	extend: qx.core.Object,

	/**
	 * @param {library.IServiceLocator} serviceLocator
	 * @param {efwdUI.behavior.PageModel} pageModel
	 * @param {efwdEditor.behavior.SelectionModel} selectionModel
	 */
	construct: function (serviceLocator, pageModel, selectionModel)
	{
		this.__serviceLocator = serviceLocator;
		this.__pageModel = pageModel;
		this.__selectionModel = selectionModel;
	},

	members: {

		initialize: function ()
		{
			if (this.__pageModel.isReadOnly())
			{
				return;
			}

			this.__selectionModel.getSelection().addListener("change", function ()
			{
				this.__handleChangeSelection();
			}, this);
		},

		/**
		 * @param {efwdRenderer.Element} element
		 * @private
		 */
		__createTool: function (element)
		{
			/** @type {efwdEditor.canvas.behavior.tool.selectedElementTool.Abstract} */
			var tool;
			if (element.isOfType("status"))
			{
				tool = this.__serviceLocator.getService(efwdEditor.canvas.behavior.tool.selectedElementTool.Status);
			}
			else if (element.isOfType("action"))
			{
				tool = this.__serviceLocator.getService(efwdEditor.canvas.behavior.tool.selectedElementTool.Action);
			}

			if (tool)
			{
				tool.initialize(element);
				this.__currentElement = element;
				this.__currentTool = tool;

				this.__elementDestoryListenerId = element.onDestroy.subscribe(function ()
				{
					this.__destroyCurrentTool();
				}, this);
			}
		},

		__destroyCurrentTool: function ()
		{
			this.__currentElement.onDestroy.unsubscribe(this.__elementDestoryListenerId);
			this.__currentTool.destroy();
			this.__elementDestoryListenerId = null;
			this.__currentTool = null;
			this.__currentElement = null;
		},

		__handleChangeSelection: function ()
		{
			var selection = this.__selectionModel.getSelection();
			var selectedElement = selection.getLength() == 1 && selection.getItem(0);

			if (this.__currentElement == selectedElement)
			{
				return;
			}

			if (this.__currentElement)
			{
				this.__destroyCurrentTool();
			}

			if (selectedElement)
			{
				this.__createTool(selectedElement);
			}
		}
	},

	defer: function (statics, members, properties)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}

});

