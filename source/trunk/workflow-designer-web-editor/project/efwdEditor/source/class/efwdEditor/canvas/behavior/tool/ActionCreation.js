/**
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdEditor.canvas.behavior.tool.ActionCreation", {

	extend: qx.core.Object,

	events: {
		/**
		 * @param {efwdDal.entity.Action} entity
		 */
		"actionCreated": "qx.event.type.Data"
	},

	/**
	 * @param {efwdEditor.canvas.views.Canvas} canvas
	 * @param {library.IServiceLocator} serviceLocator
	 * @param {efwdUI.commands.Queue} queue
	 * @param {efwdEditor.canvas.CanvasElementsHandling} canvasElementsHandling
	 * @param {efwdEditor.canvas.behavior.ResizeAndScrollCanvas} resizeAndScrollCanvasBehavior
	 * @param {efwdEditor.canvas.BBoxesModel} bboxesModel
	 * @param {efwdRenderer.Factory} rendererFactory
	 * @param {efwdRenderer.Manager} rendererManager
	 */
	construct: function (canvas, serviceLocator, queue, canvasElementsHandling, resizeAndScrollCanvasBehavior,
	                     bboxesModel, rendererFactory, rendererManager)
	{
		this.__canvas = canvas;
		this.__serviceLocator = serviceLocator;
		this.__queue = queue;
		this.__canvasElementsHandling = canvasElementsHandling;
		this.__resizeAndScrollCanvasBehavior = resizeAndScrollCanvasBehavior;
		this.__bboxesModel = bboxesModel;
		this.__rendererFactory = rendererFactory;
		this.__rendererManager = rendererManager;

		this.__listenersStore = new library.ListenersStore();
		this.__destStatusElement = null;
	},

	members: {

		/**
		 * @param {efwdDal.entity.Status} sourceStatus
		 */
		initialize: function (sourceStatus)
		{
			this.__sourceStatus = sourceStatus;
			this.__sourceStatusElement = this.__canvasElementsHandling.getElementByEntity(this.__sourceStatus);

			this.__createGhost();
			this.__setUpMovement();
			this.__setUpCreation();
			this.__resizeAndScrollCanvasBehavior.followCursor();
		},

		/**
		 * @param {efwdDal.entity.Status|Function} destStatus
		 * @private
		 */
		__createAction: function (destStatus)
		{
			var createActionCommand;

			if (qx.lang.Type.isFunction(destStatus))
			{
				createActionCommand = this.__serviceLocator.getService(efwdEditor.commands.types.CreateActionAndDestStatus);
				createActionCommand.initialize(this.__sourceStatus, destStatus);
			}
			else
			{
				createActionCommand = this.__serviceLocator.getService(efwdUI.commands.types.CreateAction);
				createActionCommand.initialize(this.__sourceStatus, destStatus);
			}

			this.__queue.addCommand(createActionCommand);
			this.fireDataEvent("actionCreated", {entity: createActionCommand.getAction()});

			this.__clear();
		},

		__createGhost: function ()
		{
			this.__actionGhost = this.__rendererFactory.createElement("actionGhost");
			this.__actionGhost.setProperty("source", this.__sourceStatusElement.getBBox());
			this.__actionGhost.setProperty("dest", this.__canvas.getCursorPosition());
			this.__actionGhost.render();
			this.__resizeAndScrollCanvasBehavior.putElement(this.__actionGhost);
		},

		__createStatusCreationTool: function ()
		{
			/** @type {efwdEditor.canvas.behavior.tool.StatusCreation} */
			this.__statusCreationTool = this.__serviceLocator.getService(efwdEditor.canvas.behavior.tool.StatusCreation);
			this.__statusCreationTool.initialize(false, false);
			this.__statusCreationTool.getGhost().onBBoxSet.subscribe(function (bbox)
			{
				this.__actionGhost.setProperty("dest", bbox);
			}, this);

			this.__statusCreationTool.addListener("createStatusRequest", function (e)
			{
				this.__createAction(e.getData().positionCallback);
			}, this);
		},

		__clear: function ()
		{
			if (this.__cleared)
			{
				return;
			}

			this.__resizeAndScrollCanvasBehavior.cancelFollowCursor();

			this.__actionGhost.destroy();
			if (this.__statusCreationTool)
			{
				this.__statusCreationTool.dispose();
			}
			this.__listenersStore.dispose();

			this.__cleared = true;
		},

		/**
		 * @param {Object} point
		 * @return {efwdRenderer.Element}
		 * @private
		 */
		__getStatusElementUnderPoint: function (point)
		{
			var elements = this.__bboxesModel.getElementsUnderPoint(point,
				function (element){ return element.isOfType("status") });

			var $this = this;
			return elements.reduce(function (max, cur)
			{
				return max ?
					$this.__rendererManager.getElementIndex(cur) > $this.__rendererManager.getElementIndex(max) ? cur : max
					: cur
			}, null);
		},

		__setUpCreation: function ()
		{
			this.__listenersStore.addListener(this.__canvas, "mouseup", function (e)
			{
				if (this.__destStatusElement)
				{
					this.__createAction(this.__destStatusElement.getEntity());
				}
			}, this);
		},

		__setUpMovement: function ()
		{
			this.__createStatusCreationTool();

			this.__listenersStore.addListener(this.__canvas, "mousemove", function (e)
			{
				var destStatusElement = this.__getStatusElementUnderPoint(e.getData().point);
				if (destStatusElement == this.__sourceStatusElement)
				{
					destStatusElement = null;
				}

				if (this.__destStatusElement != destStatusElement)
				{
					if (destStatusElement)
					{
						if (this.__statusCreationTool)
						{
							this.__statusCreationTool.dispose();
							this.__statusCreationTool = null;
						}

						this.__actionGhost.setProperty("dest", destStatusElement.getBBox());
					}
					else
					{
						this.__createStatusCreationTool();
					}

					this.__destStatusElement = destStatusElement;
				}
			}, this);
		}
	},

	destruct: function ()
	{
		this.__clear();
	},

	defer: function (statics, members, properties)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}
});
