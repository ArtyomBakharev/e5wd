/*
 #ignore(efwdRenderer)
 #ignore(efwdRenderer.*)
 #ignore(efwdConfig)
 #ignore(efwdConfig.*)
 */

/**
 * Класс управляет точками соединения переданного коннектора. Отвечает за создание, удаление, перемещение точек.
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdEditor.canvas.behavior.tool.ConnectorJointsManaging", {

	extend: qx.core.Object,

	/**
	 * @param {efwdEditor.canvas.views.Canvas} canvas
	 * @param {efwdEditor.canvas.CanvasGridModel} canvasGridModel
	 * @param {efwdEditor.behavior.manipulationtool.Model} manipulationToolModel
	 * @param {library.IServiceLocator} serviceLocator
	 * @param {efwdUI.commands.Queue} queue
	 * @param {efwdEditor.canvas.CanvasElementsHandling} canvasElementsHandling
	 * @param {efwdEditor.canvas.behavior.ResizeAndScrollCanvas} resizeCanvasBehavior
	 * @param {efwdEditor.behavior.SelectionModel} selectionModel
	 */
	construct: function (canvas, canvasGridModel, manipulationToolModel, serviceLocator, queue, canvasElementsHandling,
	                     resizeCanvasBehavior, selectionModel)
	{
		this.__canvas = canvas;
		this.__canvasGridModel = canvasGridModel;
		this.__manipulationToolModel = manipulationToolModel;
		this.__serviceLocator = serviceLocator;
		this.__queue = queue;
		this.__canvasElementsHandling = canvasElementsHandling;
		this.__resizeCanvasBehavior = resizeCanvasBehavior;
		this.__selectionModel = selectionModel;
	},

	members: {

		/**
		 * @param {efwdRenderer.Element} element
		 */
		initialize: function (element)
		{
			this.__element = element;
			this.__entity = element.getEntity();
			this.__joints = this.__entity.getSource().getMetadata().getJoints();

			//process joints
			var joints = element.getChild("joint");
			joints.forEach(function (x){ this.__setUpJoint(x) }, this);
			element.onAddChildElement.subscribe(function (name, joint)
			{
				if (name == "joint")
				{
					this.__setUpJoint(joint)
				}
			}, this);

			this.__setUpJointsCreation();
		},

		__checkManipulationsEnabled: function()
		{
			return this.__manipulationToolModel.isToolDefault() && !this.__selectionModel.isMultiselect();
		},

		/**
		 * @param {efwdRenderer.Element} element
		 * @return {Number}
		 * @private
		 */
		__getJointIndex: function (element)
		{
			return this.__element.getChild("joint").indexOf(element);
		},

		/**
		 * @param {Object} point
		 * @return {Number}
		 * @private
		 */
		__findJointIndex: function (point)
		{
			var pathArray = this.__element.getProperty("pathArray");
			return efwdRenderer.Geo.findNearestPathSegment(pathArray, point);
		},

		/**
		 * @param {efwdEditor.commands.types.MoveActionConnectorJoint} command
		 * @param {Object} point
		 * @param {Boolean} [lastOne=false]
		 * @private
		 */
		__moveJoint: function (command, point, lastOne)
		{
			var index = command.getJointIndex();
			var geo = efwdRenderer.Geo;

			if (lastOne)
			{
				var pathArray = this.__element.getProperty("pathArray");
				var prevPoint = pathArray[index];
				var nextPoint = pathArray[index + 2];

				var distance = geo.getDistanceToSegment(point, prevPoint, nextPoint);
				if (distance < efwdConfig.efwdEditor.canvas.behavior.tool.ConnectorJointsManaging.minDistanceFromLineToJoint)
				{
					command.deleteJoint();
					return;
				}
			}

			point = this.__canvasGridModel.snapToGrid(point, this.__element.getChild("joint")[index]);

			command.setPoint(point);
			command.moveJoint();
		},

		/**
		 * @param {efwdRenderer.Element} element
		 * @private
		 */
		__setUpJoint: function (element)
		{
			element.enableHovering();

			function commitMove()
			{
				this.__queue.addCommand(command);
				command = null;
				this.__resizeCanvasBehavior.cancelFollowCursor();
			}

			//перемещение
			var command;
			element.listenMove(this.__canvas,
				function ()
				{
					if (element.isDestroyed() || !this.__checkManipulationsEnabled())
					{
						return false;
					}

					command = this.__serviceLocator.getService(efwdEditor.commands.types.MoveActionConnectorJoint);
					command.initialize(this.__entity);
					command.setJointIndex(this.__getJointIndex(element));

					this.__resizeCanvasBehavior.followCursor();
				}, function (to)
				{
					if (!element.isDestroyed())
					{
						this.__moveJoint(command, to);
					}
				}, function (to)
				{
					if (!element.isDestroyed())
					{
						this.__moveJoint(command, to, true);
						commitMove.call(this);
					}
				}, this);

			this.__entity.addListenerOnce("removing", function(){
				if (command)
				{
					commitMove.call(this);
				}
			}, this);

			//удаление
			element.addListener("click", function (e)
			{
				if (!this.__checkManipulationsEnabled())
				{
					return;
				}

				if (e.ctrlKey || e.altKey)
				{
					var command = this.__serviceLocator.getService(efwdEditor.commands.types.MoveActionConnectorJoint);
					command.initialize(this.__entity);
					command.setJointIndex(this.__getJointIndex(element));
					command.deleteJoint();
					this.__queue.addCommand(command);
				}
			}, this);
		},

		/**
		 * @private
		 */
		__setUpJointsCreation: function ()
		{
			var line = this.__element.getChild("arrow").getChild("actionArrow").getChild("line");

			var linePoint;
			line.addListener("mousedown", function ()
			{
				linePoint = this.__canvas.getCursorPosition();
			}, this);

			function commit()
			{
				this.__queue.addCommand(command);
				command = null;
				this.__resizeCanvasBehavior.cancelFollowCursor();
			}

			var command;
			var jointElement;
			line.listenMove(this.__canvas,
				function ()
				{
					if (!this.__checkManipulationsEnabled())
					{
						return false;
					}

					command = this.__serviceLocator.getService(efwdEditor.commands.types.MoveActionConnectorJoint);
					command.initialize(this.__entity);
					command.setJointIndex(this.__findJointIndex(linePoint));
					command.setPoint(this.__canvas.getCursorPosition());
					command.createJoint();

					jointElement = this.__element.getChild("joint")[command.getJointIndex()];

					this.__resizeCanvasBehavior.followCursor();

				}, function (to)
				{
					this.__moveJoint(command, to);
				}, function (to)
				{
					this.__moveJoint(command, to, true);
					commit.call(this);
				}, this);

			this.__entity.addListenerOnce("removing", function(){
				if (command)
				{
					commit.call(this);
				}
			}, this);
		}
	},

	defer: function (statics, members, properties)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}

});
