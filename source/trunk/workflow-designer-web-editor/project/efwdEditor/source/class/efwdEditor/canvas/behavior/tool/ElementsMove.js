/**
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdEditor.canvas.behavior.tool.ElementsMove", {

	extend: qx.core.Object,

	statics: {
		__MAX_ELEMENTS_COUNT_WITHOUT_GHOST_BOX: qx.core.Environment.select("browser.name", {
			"ie|chrome": 20,
			"default": 10
		}),

		__MOVE_CHECK_INTERVAL: qx.core.Environment.select("browser.name", {
			"ie|chrome": false,
			"default": 10
		})
	},

	/**
	 * @param {efwdEditor.canvas.CanvasGridModel} canvasGridModel
	 * @param {library.IServiceLocator} serviceLocator
	 * @param {efwdUI.commands.Queue} queue
	 * @param {efwdEditor.canvas.behavior.ResizeAndScrollCanvas} resizeCanvasBehavior
	 * @param {efwdDal.repository.ActionsRepository} actionsRepository
	 * @param {efwdRenderer.Factory} rendererFactory
	 * @param {efwdEditor.canvas.CanvasElementsHandling} canvasElementsHandling
	 */
	construct: function (canvasGridModel, serviceLocator, queue, resizeCanvasBehavior, actionsRepository,
	                     rendererFactory, canvasElementsHandling)
	{
		this.__canvasGridModel = canvasGridModel;
		this.__serviceLocator = serviceLocator;
		this.__queue = queue;
		this.__resizeCanvasBehavior = resizeCanvasBehavior;
		this.__actionsRepository = actionsRepository;
		this.__rendererFactory = rendererFactory;
		this.__canvasElementsHandling = canvasElementsHandling;

		this.__listenersStore = new library.ListenersStore();
	},

	members: {

		/**
		 * @param {efwdRenderer.Element} anchorElement
		 * @param {efwdRenderer.Element[]} elements
		 * @param {Object} point
		 */
		initialize: function (anchorElement, elements, point)
		{
			this.__elements = elements;
			this.__anchorElement = anchorElement;
			this.__startPoint = point;
			this.__startCenter = this.__lastCenter = anchorElement.getCenter();
			this.__centerAndPointDelta = efwdRenderer.Geo.subPoints(this.__startCenter, this.__startPoint);
			this.__totalDelta = {x: 0, y: 0};
			this.__statuses = this.__getStatuses();
			this.__affectedEntities = this.__getAffectedEntities();

			this.__moveCommand = this.__serviceLocator.getService(efwdEditor.commands.types.MoveStatuses);
			this.__moveCommand.initialize(this.__statuses);
			this.__movableElements = this.__moveCommand.getEntities()
				.map(function (x){ return this.__canvasElementsHandling.getElementByEntity(x) }, this);

			this.__currentBBox = this.__startBBox = efwdRenderer.Helper.getBounds(this.__movableElements);

			this.__prepareElements();

			this.__createGhostBoxIfNecessary();

			this.__resizeCanvasBehavior.followCursor();

			this.__moveCheckInterval = !this.__ghostBox && this.self(arguments).__MOVE_CHECK_INTERVAL;
			if (this.__moveCheckInterval)
			{
				this.__interval = setInterval((function (){ this.__moveEnabled = true; }).bind(this),
					this.__moveCheckInterval);
			}
			else
			{
				this.__moveEnabled = true;
			}
		},

		moveTo: function (point)
		{
			if (this.__commited || !this.__moveEnabled)
			{
				return;
			}

			var center = efwdRenderer.Geo.addPoints(this.__centerAndPointDelta, point);
			var snappedCenter = this.__canvasGridModel.snapToGrid(center, this.__anchorElement, this.__currentBBox);

			var totalDelta = this.__totalDelta = efwdRenderer.Geo.subPoints(snappedCenter, this.__startCenter);
			var bbox = efwdRenderer.Geo.addPointToBBox(this.__startBBox, totalDelta);

			if (efwdRenderer.Geo.isPointsEquals(this.__lastCenter, snappedCenter))
			{
				return;
			}

			if (this.__ghostBox)
			{
				this.__ghostBox.setProperty("box", bbox);
				this.__ghostBox.setProperty("point", efwdRenderer.Geo.addPoints(this.__startPoint, totalDelta));
			}
			else
			{
				this.__moveCommand.move(totalDelta);
				this.__currentBBox = bbox;
			}

			this.__lastCenter = snappedCenter;

			if (this.__moveCheckInterval)
			{
				this.__moveEnabled = false;
			}
		},

		commit: function ()
		{
			if (this.__commited)
			{
				return;
			}

			this.__cleanUp();

			if (this.__ghostBox)
			{
				this.__moveCommand.move(this.__totalDelta);
			}

			this.__queue.addCommand(this.__moveCommand);

			this.__commited = true;
		},

		__cleanUp: function ()
		{
			this.__cleanUpElements();
			if (this.__ghostBox)
			{
				this.__ghostBox.destroy();
			}
			if (this.__interval)
			{
				clearInterval(this.__interval);
			}

			this.__resizeCanvasBehavior.cancelFollowCursor();
			this.__listenersStore.dispose();
		},

		__cleanUpElements: function ()
		{
			this.__movableElements.forEach(function (element)
			{
				if (!element.isDestroyed())
				{
					element.bringToFront(element.isOfType("action") ? "connectors" : "states");
					element.enableHovering();
				}
			});
		},

		__createGhostBoxIfNecessary: function ()
		{
			if (this.__affectedEntities.length > this.self(arguments).__MAX_ELEMENTS_COUNT_WITHOUT_GHOST_BOX)
			{
				this.__ghostBox = this.__rendererFactory.createElement("moveGhostBox");
				this.__ghostBox.setProperty("box", this.__startBBox);
				this.__ghostBox.setProperty("point", this.__startPoint);
				this.__ghostBox.render();

				this.__resizeCanvasBehavior.putElement(this.__ghostBox);
			}
		},

		__getStatuses: function ()
		{
			return this.__elements
				.map(function (x){ return x.getEntity() })
				.filter(function (x){ return x && x instanceof efwdDal.entity.Status });
		},

		/**
		 * @returns {efwdDal.entity.AbstractEntity[]}
		 * @private
		 */
		__getAffectedEntities: function ()
		{
			var actions = this.__statuses.flatten(function (status)
			{
				return this.__actionsRepository.getActionsByStatus(status);
			}, this).unique();

			return this.__statuses.concat(actions);
		},

		__prepareElements: function ()
		{
			this.__movableElements.forEach(function (element)
			{
				element.bringToFront(element.isOfType("action") ? "connectorsUp" : "statesUp");
				element.disableHovering();
				element.unhover();
				var entity = element.getEntity();
				if (entity)
				{
					this.__listenersStore.addListener(entity, "removing", function ()
					{
						this.commit();
					}, this);
				}
			}, this);
		}
	},

	defer: function (statics, members, properties)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}
});
