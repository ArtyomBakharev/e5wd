/**
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdEditor.canvas.behavior.tool.StatusCreation", {

	extend: qx.core.Object,

	events: {
		/**
		 * @param {efwdDal.entity.Status} entity
		 */
		"statusCreated": "qx.event.type.Data",

		/**
		 * @param {Function} positionCallback
		 */
		"createStatusRequest": "qx.event.type.Data"
	},

	/**
	 * @param {efwdEditor.canvas.views.Canvas} canvas
	 * @param {library.IServiceLocator} serviceLocator
	 * @param {efwdUI.commands.Queue} queue
	 * @param {efwdEditor.canvas.CanvasElementsHandling} canvasElementsHandling
	 * @param {efwdEditor.canvas.CanvasGridModel} canvasGridModel
	 * @param {efwdRenderer.Factory} rendererFactory
	 * @param {efwdEditor.canvas.behavior.ResizeAndScrollCanvas} resizeAndScrollCanvasBehavior
	 */
	construct: function (canvas, serviceLocator, queue, canvasElementsHandling, canvasGridModel, rendererFactory,
	                     resizeAndScrollCanvasBehavior)
	{
		this.__canvas = canvas;
		this.__serviceLocator = serviceLocator;
		this.__queue = queue;
		this.__canvasElementsHandling = canvasElementsHandling;
		this.__canvasGridModel = canvasGridModel;
		this.__rendererFactory = rendererFactory;
		this.__resizeAndScrollCanvasBehavior = resizeAndScrollCanvasBehavior;

		this.__listenersStore = new library.ListenersStore();
	},

	members: {

		/**
		 * @param followCursor
		 * @param {Boolean} [createStatus=true] если false, то, вместо создания нового статуса и вызова события
		 * statusCreated, вызывает
		 */
		initialize: function (followCursor, createStatus)
		{
			this.__followCursor = followCursor;
			this.__createStatus = createStatus === undefined || createStatus;

			this.__createGhost();

			if (this.__followCursor)
			{
				this.__resizeAndScrollCanvasBehavior.followCursor();
			}

			this.__setUpCreation();
		},

		/**
		 * @return {efwdRenderer.Element}
		 */
		getGhost: function ()
		{
			return this.__statusGhost;
		},

		__clear: function ()
		{
			if (this.__cleared)
			{
				return;
			}

			if (this.__followCursor)
			{
				this.__resizeAndScrollCanvasBehavior.cancelFollowCursor();
			}

			this.__statusGhost.destroy();
			this.__listenersStore.dispose();

			this.__cleared = true;
		},

		__createGhost: function ()
		{
			this.__statusGhost = this.__rendererFactory.createElement("statusGhost");
			this.__statusGhost.setProperty("x", efwdRenderer.Geo.OUT_OF_CANVAS_POINT.x);
			this.__statusGhost.setProperty("y", efwdRenderer.Geo.OUT_OF_CANVAS_POINT.y);
			this.__statusGhost.render();

			this.__setUpGhost();
		},

		__setUpCreation: function ()
		{
			this.__listenersStore.addListener(this.__canvas, "mouseup", function (e)
			{
				var $this = this;
				function positionCallback()
				{
					return $this.__canvasGridModel.snapToGrid(e.getData().point,
						$this.__canvasElementsHandling.getElementByEntity(status))
				}

				if (this.__createStatus)
				{
					var createStatusCommand = this.__serviceLocator.getService(efwdUI.commands.types.CreateStatus);
					createStatusCommand.initialize(positionCallback);

					this.__queue.addCommand(createStatusCommand);
					this.fireDataEvent("statusCreated", {entity: createStatusCommand.getStatus()});
				}
				else
				{
					this.fireDataEvent("createStatusRequest", {positionCallback: positionCallback});
				}

				this.__clear();
			}, this);
		},

		__setUpGhost: function ()
		{
			this.__resizeAndScrollCanvasBehavior.putElement(this.__statusGhost);

			this.__listenersStore.addListener(this.__canvas, "mousemove", function (e)
			{
				var point = e.getData().point;

				point = this.__canvasGridModel.snapToGrid(point, this.__statusGhost);
				this.__statusGhost.setProperty("x", point.x);
				this.__statusGhost.setProperty("y", point.y);
			}, this);
		}
	},

	destruct: function ()
	{
		this.__clear();
	},

	defer: function (statics, members, properties)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}
});
