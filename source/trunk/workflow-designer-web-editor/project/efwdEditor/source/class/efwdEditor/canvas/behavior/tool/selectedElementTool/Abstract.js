qx.Class.define("efwdEditor.canvas.behavior.tool.selectedElementTool.Abstract", {

	type: "abstract",
	extend: qx.core.Object,

	members: {
		/**
		 * @param {efwdRenderer.Element} element
		 */
		initialize: function(element)
		{
			this._element = element;
		},

		destroy: function(){ this.abstractMethod(arguments) }
	},

	defer: function (statics, members, properties)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}
});
