/*
 #asset(qx/icon/${qx.icontheme}/22/actions/go-last.png)
 */
qx.Class.define("efwdEditor.canvas.behavior.tool.selectedElementTool.Action", {

	extend: efwdEditor.canvas.behavior.tool.selectedElementTool.Abstract,

	/**
	 * @param {efwdRenderer.Factory} rendererFactory
	 * @param {library.IServiceLocator} serviceLocator
	 * @param {efwdUI.commands.Queue} queue
	 */
	construct: function (rendererFactory, serviceLocator, queue)
	{
		this.__rendererFactory = rendererFactory;
		this.__serviceLocator = serviceLocator;
		this.__queue = queue;
	},

	members: {
		/**
		 * @param {efwdRenderer.Element} element
		 */
		initialize: function (element)
		{
			this.base(arguments, element);

			this.__toolElement = this.__rendererFactory.createElement("quickActionTool", null, {
				element: this._element.getModel(),
				pinButtonIcon: qx.util.ResourceManager.getInstance().toUri("efwdEditor/icon/pin16.png")
			});

			this.__setUpUnpining();
			this.__toolElement.render();
		},

		__setUpUnpining: function ()
		{
			this.__toolElement.onRenderChildElement.subscribe(function (child, element)
			{
				var isDestPoint = child == "sourcePointPinButton" ? false : child == "destPointPinButton" ? true : null;
				if (isDestPoint !== null)
				{
					element.addListener("click", function ()
					{
						this.__unpinPoint(isDestPoint);
					}, this);
				}
			}, this);
		},

		__unpinPoint: function (isDestPoint)
		{
			/** @type {efwdEditor.commands.types.MoveActionConnectorPinPoint} */
			var command = this.__serviceLocator.getService(efwdEditor.commands.types.MoveActionConnectorPinPoint);
			command.initialize(this._element.getEntity(), isDestPoint);
			command.unpinPoint();
			this.__queue.addCommand(command);
		},

		destroy: function ()
		{
			this.__toolElement.destroy();
		}
	}
});
