/*
 #asset(qx/icon/${qx.icontheme}/22/actions/go-last.png)
 */
qx.Class.define("efwdEditor.canvas.behavior.tool.selectedElementTool.Status", {

	extend: efwdEditor.canvas.behavior.tool.selectedElementTool.Abstract,

	/**
	 * @param {efwdRenderer.Factory} rendererFactory
	 * @param {efwdEditor.behavior.manipulationtool.Model} manipulationToolModel
	 */
	construct: function (rendererFactory, manipulationToolModel)
	{
		this.__rendererFactory = rendererFactory;
		this.__manipulationToolModel = manipulationToolModel;
	},

	members: {
		/**
		 * @param {efwdRenderer.Element} element
		 */
		initialize: function (element)
		{
			this.base(arguments, element);

			this.__toolElement = this.__rendererFactory.createElement("quickStatusTool", null, {element: this._element.getModel()});
			this.__toolElement.render();
			this.__actionButton = this.__toolElement.getChild("actionButton");
			this.__actionButton.setProperty("icon",
				qx.util.ResourceManager.getInstance().toUri("qx/icon/Oxygen/22/actions/go-last.png"));

			this.__actionButton.addListener("mousedown", function ()
			{
				var actionTool = new efwdEditor.behavior.manipulationtool.ActionTool();
				this.__manipulationToolModel.setTool(actionTool);
				actionTool.setSourceStatus(this._element.getEntity());
			}, this);
		},

		destroy: function ()
		{
			this.__toolElement.destroy();
		}
	}
});
