/*
 #ignore(Raphael)
 #ignore(efwdConfig)
 #ignore(efwdConfig.*)
 */

/**
 * Вид канвы. Отрисовывает виджет и канву на нём. Предоставляет основные события канвы.
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdEditor.canvas.views.Canvas", {

	extend: qx.core.Object,

	events: {
		/**
		 * Клик по фону канвы
		 */
		"backgroundClick": "qx.event.type.Data",

		/**
		 * Событие нажатия клавиши мыши, когда курсор находится на фоне канвы
		 */
		"backgroundMousedown": "qx.event.type.Data",

		/**
		 * Событие отпускания клавиши мыши, когда курсор находится на фоне канвы
		 */
		"backgroundMouseup": "qx.event.type.Data",

		/**
		 * клик по канве (в том числе по любому элементу на канве)
		 * @param {Object} point
		 */
		"click": "qx.event.type.Data",

		/**
		 * @param {Object} point
		 */
		"mousedown": "qx.event.type.Data",

		/**
		 * @param {Object} point
		 */
		"mouseup": "qx.event.type.Data",

		/**
		 * перемещение курсора мыши по канве
		 * @param {Object} point
		 */
		"mousemove": "qx.event.type.Data",

		/**
		 * канва отображена и отрисовка элементов на нём разрешена
		 */
		"renderingEnabled": "qx.event.type.Event",

		/**
		 * @param {Object} point
		 */
		"rmbMousedown": "qx.event.type.Data",

		/**
		 * клик по канве (в том числе по любому элементу на канве) правой клавишей мыши
		 * @param {Object} point
		 */
		"contextmenu": "qx.event.type.Data",

		/**
		 * @param {Number} dx
		 * @param {Number} dy
		 */
		"scroll": "qx.event.type.Data"
	},

	/**
	 * @param {efwdEditor.canvas.views.Cursor} cursorView
	 */
	construct: function (cursorView)
	{
		this.__cursorView = cursorView;
	},

	members: {

		/**
		 * @param {Object} position
		 * @return {Object}
		 */
		calculateDocumentPositionByCanvasPosition: function (position)
		{
			var widgetPosition = qx.bom.element.Location.get(this.__paperContainer.getContentElement().getDomElement());
			return {
				x: position.x + widgetPosition.left,
				y: position.y + widgetPosition.top
			};
		},

		/**
		 * Возвращает текущие положение курсора на канве
		 *
		 * @return {Object}
		 */
		getCursorPosition: function ()
		{
			return this.__cursorPosition;
		},

		/**
		 * канва библиотеки Raphael
		 *
		 * @return {Raphael.Paper}
		 */
		getPaper: function ()
		{
			return this.__paper;
		},

		/**
		 * @return {qx.ui.core.Widget}
		 */
		getPaperContainer: function ()
		{
			return this.__paperContainer;
		},

		/**
		 * Текущее положение полос прокрутки канвы
		 *
		 * @return {Object}
		 */
		getScroll: function ()
		{
			return {
				x: this.__scrollContainer.getScrollX(),
				y: this.__scrollContainer.getScrollY()
			};
		},

		/**
		 * размеры канвы
		 *
		 * @return {Object} {width: Number, height: Number}
		 */
		getSize: function ()
		{
			return {
				width: this.__paperContainer.getWidth(),
				height: this.__paperContainer.getHeight()
			};
		},

		/**
		 * размеры контейнера содержащего канву
		 *
		 * @return {Object} {width: Number, height: Number}
		 */
		getViewportSize: function ()
		{
			var element = this.__scrollContainer.getContainerElement();
			return {
				width: parseFloat(element.getStyle("width")) || 0,
				height: parseFloat(element.getStyle("height")) || 0
			};
		},

		/**
		 * Возвращает зону интерфейса, в которой располагается канва
		 *
		 * @return {library.ui.management.Zone}
		 */
		getZone: function ()
		{
			return this.__zone;
		},

		/**
		 * @param startMove
		 * @param move
		 * @param moved
		 * @param context
		 */
		listenBackgroundMove: function (startMove, move, moved, context)
		{
			efwdEditor.canvas.views.Helper.listenElementMove(this.__canvasBackground, this, startMove, move, moved,
				context);
		},

		/**
		 * Конструирует виджет канвы и возвращает его
		 *
		 * @return {qx.ui.core.Widget}
		 */
		render: function ()
		{
			var widget = this.__renderMainWidget();

			this.__paperContainer.addListener("appear", function ()
			{

				this.__createPaper();
				this.__cursorView.initialize(this.__paperContainer);
				this.fireEvent("renderingEnabled");

			}, this);

			this.__zone = library.ui.management.ZonesManager.getInstance().registerZone(widget);

			return widget;
		},

		/**
		 * Скроллировать канву по горизонтали
		 *
		 * @param {Number} value
		 */
		scrollToX: function (value)
		{
			this.__scrollContainer.scrollToX(value);
		},

		/**
		 * Скроллировать канву по вертикали
		 *
		 * @param {Number} value
		 */
		scrollToY: function (value)
		{
			this.__scrollContainer.scrollToY(value);
		},

		/**
		 * изменить размеры канвы. Канву нельзя уменьшить за счёт видимой в данный момент на экране области.
		 *
		 * @param {Number} width
		 * @param {Number} height
		 */
		setSize: function (width, height)
		{
			this.__canvasWidth = width;
			this.__canvasHeight = height;

			var viewportSize = this.getViewportSize();

			var srollbarsGap = efwdConfig.efwdEditor.canvas.$style.scrollbarsGap;

			var scrollbarX = this.__scrollContainer.getChildControl("scrollbar-x");
			var scrollbarY = this.__scrollContainer.getChildControl("scrollbar-y");

			width = Math.round(
				Math.max(
					Math.max(width, viewportSize.width - srollbarsGap),
					this.__paperContainer.getWidth() - (scrollbarX.getMaximum() - scrollbarX.getPosition())
				)
			);
			this.__paperContainer.setWidth(width);

			height = Math.round(
				Math.max(
					Math.max(height, viewportSize.height - srollbarsGap),
					this.__paperContainer.getHeight() - (scrollbarY.getMaximum() - scrollbarY.getPosition())
				)
			);
			this.__paperContainer.setHeight(height);
		},

		/**
		 * расчитывает положение курсора мыши относительно левого верхнего угла канвы
		 *
		 * @param {Object} cursorToDocumentPosition
		 */
		__computeMousePosition: function (cursorToDocumentPosition)
		{
			var widgetPosition = qx.bom.element.Location.get(this.__paperContainer.getContentElement().getDomElement());
			return {
				x: cursorToDocumentPosition.x - widgetPosition.left,
				y: cursorToDocumentPosition.y - widgetPosition.top
			};
		},

		/**
		 * @private
		 */
		__createPaper: function ()
		{
			this.__paper = Raphael(this.__paperContainer.getContentElement().getDomElement());

			this.__paperContainer.addListener("resize", function ()
			{
				this.__paper.setSize(this.__paperContainer.getWidth(), this.__paperContainer.getHeight());
				this.__canvasBackground.attr({
					width: this.__paperContainer.getWidth(),
					height: this.__paperContainer.getHeight()
				});
			}, this);

			this.__paperContainer.addListener("click", function (e)
			{
				if (e.isLeftPressed())
				{
					this.fireDataEvent("click", {point: this.__cursorPosition});
				}
				else if (e.isRightPressed())
				{
					this.fireDataEvent("contextmenu", {point: this.__cursorPosition});
				}
			}, this);

			this.__paperContainer.addListener("mousemove", function (e)
			{
				this.__lastCursorToDocumentPosition = {x: e.getDocumentLeft(), y: e.getDocumentTop()};
				this.__cursorPosition = this.__computeMousePosition(this.__lastCursorToDocumentPosition);
				this.fireDataEvent("mousemove", {point: this.__cursorPosition});
			}, this);

			this.addListener("scroll", function (e)
			{
				this.__cursorPosition = this.__computeMousePosition(this.__lastCursorToDocumentPosition);
				this.fireDataEvent("mousemove", {point: this.__cursorPosition});
			}, this);

			this.__paperContainer.addListener("mousedown", function (e)
			{
				if (e.isLeftPressed())
				{
					this.fireDataEvent("mousedown", {point: this.__cursorPosition});
				}
				else if (e.isRightPressed())
				{
					this.fireDataEvent("rmbMousedown", {point: this.__cursorPosition});
				}
			}, this);

			this.__paperContainer.addListener("contextmenu", function (e)
			{
				this.fireDataEvent("contextmenu", {point: this.__cursorPosition});
			}, this);

			this.__paperContainer.addListener("mouseup", function (e)
			{
				if (e.isLeftPressed())
				{
					this.fireDataEvent("mouseup", {point: this.__cursorPosition});
				}
			}, this);

			//background
			this.__canvasBackground = this.__paper.rect(0, 0, this.__paperContainer.getWidth(),
				this.__paperContainer.getHeight())
				.attr({
					"stroke-opacity": 0,
					fill: efwdConfig.efwdEditor.canvas.$style.backgroundColor
				});

			this.__canvasBackground.addListener("mousedown",
				function (e){ this.fireDataEvent("backgroundMousedown", {origEvent: e}); }, this);
			this.__canvasBackground.addListener("mouseup",
				function (e){ this.fireDataEvent("backgroundMouseup", {origEvent: e}); }, this);
			this.__canvasBackground.addListener("click",
				function (e){ this.fireDataEvent("backgroundClick", {origEvent: e}); }, this);
		},

		/**
		 * @private
		 */
		__renderMainWidget: function ()
		{
			var style = efwdConfig.efwdEditor.canvas.$style;

			var scrollContainer = this.__scrollContainer = new qx.ui.container.Scroll();
			scrollContainer.setBackgroundColor(style.backgroundColor);

			this.__setUpScrollEvents();

			var paperWrapper = new qx.ui.container.Composite(new qx.ui.layout.Grow());
			paperWrapper.set({
				backgroundColor: style.backgroundColor,
				padding: style.paperPadding
			});
			scrollContainer.add(paperWrapper);

			this.__paperContainer = new qx.ui.container.Composite(new qx.ui.layout.Grow());
			this.__paperContainer.set({
				backgroundColor: style.backgroundColor,
				allowStretchX: [false, false],
				allowStretchY: [false, false]
			});

			this.__setUpSizeBinding();

			paperWrapper.add(this.__paperContainer);
			return scrollContainer;
		},

		/**
		 * @private
		 */
		__setUpScrollEvents: function ()
		{
			var scrollbarX = this.__scrollContainer.getChildControl("scrollbar-x");
			var scrollbarY = this.__scrollContainer.getChildControl("scrollbar-y");

			var currentScroll = {
				x: scrollbarX.getPosition(),
				y: scrollbarY.getPosition()
			};

			function onScroll(e)
			{
				var data = {
					dx: scrollbarX.getPosition() - currentScroll.x,
					dy: scrollbarY.getPosition() - currentScroll.y
				};
				this.__cursorPosition.x += data.dx;
				this.__cursorPosition.y += data.dy;
				this.fireDataEvent("scroll", data);


				currentScroll = {
					x: scrollbarX.getPosition(),
					y: scrollbarY.getPosition()
				};
			}

			scrollbarX.addListener("scroll", onScroll, this);
			scrollbarY.addListener("scroll", onScroll, this);
		},

		/**
		 * @private
		 */
		__setUpSizeBinding: function ()
		{
			this.__scrollContainer.addListener("resize", function ()
			{
				this.setSize(this.__canvasWidth || 0, this.__canvasHeight || 0);
			}, this);
		}
	}
});
