/**
 * Управление видом курсора. Меняет вид курсора в зависимости от выбранного инструмента редактирования.
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdEditor.canvas.views.Cursor", {

	extend: qx.core.Object,

	/**
	 * @param {efwdEditor.behavior.manipulationtool.Model} manipulationToolModel
	 */
	construct: function (manipulationToolModel)
	{
		this.__manipulationToolModel = manipulationToolModel;
	},

	members: {

		/**
		 * @param {qx.ui.core.Widget} paperWidget
		 */
		initialize: function (paperWidget)
		{
			this.__paperWidget = paperWidget;
			this.__setUpToolProcessing();
		},

		/**
		 * @param {efwdEditor.behavior.manipulationtool.ITool} tool
		 * @private
		 */
		__setCursorForTool: function (tool)
		{
			var cursor;
			if (tool instanceof efwdEditor.behavior.manipulationtool.DefaultTool)
			{
				cursor = "default";
			}
			else if (tool instanceof efwdEditor.behavior.manipulationtool.ActionTool)
			{
				cursor = "crosshair";
			}
			else if (tool instanceof efwdEditor.behavior.manipulationtool.StatusTool)
			{
				cursor = "crosshair";
			}

			this.__paperWidget.getContentElement().setStyle("cursor", cursor);
		},

		/**
		 * @private
		 */
		__setUpToolProcessing: function ()
		{
			this.__manipulationToolModel.addListener("changeTool", function (e)
			{
				this.__setCursorForTool(e.getData());
			}, this);
			var tool = this.__manipulationToolModel.getTool();
			if (tool)
			{
				this.__setCursorForTool(tool);
			}
		}
	}
});

