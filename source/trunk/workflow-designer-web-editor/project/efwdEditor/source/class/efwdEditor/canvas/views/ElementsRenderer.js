qx.Class.define("efwdEditor.canvas.views.ElementsRenderer", {

	extend: qx.core.Object,

	/**
	 * @param {efwdRenderer.Factory} rendererFactory
	 */
	construct: function (rendererFactory)
	{
		this.__rendererFactory = rendererFactory;
	},

	members: {

		/**
		 * @param {efwdDal.entity.AbstractEntity} entity
		 * @return {efwdRenderer.Element}
		 * @private
		 */
		renderGraphicElement: function (entity)
		{
			var graphicElement = this.__rendererFactory.createElement(entity.getEntityType(), entity.getSource());
			graphicElement.setUserData("type", "entity");
			graphicElement.setUserData("entity", entity);

			entity.addListenerOnce("removed", function ()
			{
				graphicElement.destroy();
			}, this);

			if (entity instanceof efwdDal.entity.Status)
			{
				this.__setUpStatus(entity, graphicElement);
			}

			graphicElement.render();

			return graphicElement;
		},

		/**
		 * @param {efwdDal.entity.Status} status
		 * @param {efwdRenderer.Element} element
		 * @private
		 */
		__setUpStatus: function(status, element)
		{
			var bindingId = library.data.Helper.addPropertyListener(status, "initialStatus",
				function(x){ element.setProperty("initialStatus", x) });

			element.onDestroy.subscribe(function(){ status.removeBinding(bindingId); });
		}
	}
});

