/**
 * Вспомагательные функции для графических элементов
 */
qx.Class.define("efwdEditor.canvas.views.Helper", {

	type: "static",

	statics: {

		/**
		 * Назначает функции mouseenter и mouseleave всем элементами из набора таким образом, что при переходе мыши
		 * с одного элемента набора на другой эти функции не вызываются (набор элементов воспринимается как один
		 * комплексный элемент)
		 * @param {Raphael.Set} elementsSet
		 * @param {Function} mouseenter
		 * @param {Function} mouseleave
		 * @param [context=null]
		 */
		hoverElementsSet: function (elementsSet, mouseenter, mouseleave, context)
		{
//			var wasDelay = true;
//			var mouseenterExecuted = false;
//			var hoveredCount = 0;
//
//			elementsSet.hover(function ()
//			{
//				if (wasDelay)
//				{
////					if (hoveredCount == 0)
////					{
//						mouseenter.apply(context || window);
////					}
////					++hoveredCount;
//				}
//				mouseenterExecuted = true;
//			}, function ()
//			{
//				wasDelay = false;
//				mouseenterExecuted = false;
//				setTimeout(function ()
//				{
//					if (!mouseenterExecuted)
//					{
////						if (hoveredCount == 1)
////						{
//							mouseleave.apply(context || window);
////						}
////						--hoveredCount;
//					}
//					wasDelay = true;
//				}, 0);
//			});

			elementsSet.hover(mouseenter, mouseleave, context, context);
		},

		/**
		 * @param element
		 * @param canvas
		 * @param startMove
		 * @param [move=null]
		 * @param [moved=null]
		 * @param [context=null]
		 * @param [onInvalidate=null]
		 * @param [centerGetter=null]
		 * @param [validateElement=null]
		 */
		listenElementMove: function(element, canvas, startMove, move, moved, context, onInvalidate, centerGetter, validateElement)
		{
			if (!context)
			{
				context = window;
			}

			var startPoint;
			var cursorPos;
			var elementDelta;
			var listenersStore = new library.ListenersStore();
			var movable;
			var started;

			function clear()
			{
				listenersStore.removeAllListeners();
				elementDelta = null;
				cursorPos = null;
				movable = false;
				started = false;
			}

			function start()
			{
				if (!started)
				{
					started = true;
					movable = (startMove && startMove.call(context, startPoint, cursorPos)) !== false;
				}

				if (!movable)
				{
					clear();
				}

				return movable;
			}

			element.addDragListener(null, function ()
			{
				cursorPos = canvas.getCursorPosition();
				startPoint = centerGetter ? centerGetter.call(context) : cursorPos;
				elementDelta = efwdRenderer.Geo.subPoints(startPoint, cursorPos);

				listenersStore.addListener(canvas, "mousemove", function (e)
				{
					var point = efwdRenderer.Geo.addPoints(e.getData().point, elementDelta);
					if ((!started && efwdRenderer.Geo.isPointsEquals(startPoint, point, 3)) || !start())
					{
						return;
					}

					if (validateElement && !validateElement.call(context))
					{
						clear();
						onInvalidate && onInvalidate.call(context);
					}
					else
					{
						move && move.call(context, point);
					}
				}, this);

			}, function ()
			{
				if (movable && moved)
				{
					moved.call(context, efwdRenderer.Geo.addPoints(canvas.getCursorPosition(), elementDelta), startPoint);
				}

				clear();

			}, this);
		}
	}
});

