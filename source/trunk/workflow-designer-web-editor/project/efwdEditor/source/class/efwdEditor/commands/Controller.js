/**
 * Контроллер:
 *  - проверят нужно ли выводить пользователю сообщение об отмене/повторении какого-либо действия
 *  - выделяет элементы на канве после выполнения/отмены команд
 * @extends {efwdUI.commands.types.AbstractCommand}
 */
qx.Class.define("efwdEditor.commands.Controller", {

	extend: efwdUI.commands.types.AbstractCommand,

	/**
	 * @param {efwdEditor.behavior.DialogsModel} dialogsModel
	 * @param {efwdUI.commands.Queue} queue
	 * @param {efwdUI.behavior.EntityEditionModel} entityEditionModel
	 * @param {efwdEditor.behavior.SelectionModel} selectionModel
	 * @param {efwdEditor.canvas.CanvasElementsHandling} canvasElementsHandling
	 */
	construct: function (dialogsModel, queue, entityEditionModel, selectionModel, canvasElementsHandling)
	{
		this.__dialogsModel = dialogsModel;
		this.__queue = queue;
		this.__entityEditionModel = entityEditionModel;
		this.__selectionModel = selectionModel;
		this.__canvasElementsHandling = canvasElementsHandling;
	},

	members: {
		initialize: function ()
		{
			this.__queue.addListener("commandExecuted",
				function (e)
				{
					if (e.getExecuteType() != "execute" && !e.isNotifyUser())
					{
						e.setNotifyUser(this.__checkNotifyUser(e.getCommand()));
					}

					this.__checkSelection(e.getCommand(), e.getExecuteType() == "undo");
				}, this);
		},

		/**
		 * @param {efwdUI.commands.types.AbstractCommand} command
		 * @private
		 */
		__checkNotifyUser: function (command)
		{
			switch (command.classname)
			{
				case "efwdUI.commands.types.AddActivitiesToOwner":
				case "efwdUI.commands.types.RemoveActivities":
					return !this.__dialogsModel.isActivitiesListDialogOpened(command.getOwner());

				case "efwdUI.commands.types.CreateAction":
					return !command.getSourceStatus() && !this.__dialogsModel.isGlobalActionsListDialog();

				case "efwdUI.commands.types.CreateActivity":
					return command.getOwner()
						? !this.__dialogsModel.isActivitiesListDialogOpened(command.getOwner())
						: !this.__dialogsModel.isGlobalActivitiesListDialog();

				case "efwdUI.commands.types.DeleteEntities":
					return this.__checkNotifyUserForDeleteEntities(command);

				case "efwdUI.commands.types.EditEntity":
					return this.__entityEditionModel.getEntity() != command.getEntity()
						&& !(command.getEntity() instanceof efwdDal.entity.Process && this.__dialogsModel.isProcessPropertiesDialog());

				default:
					return false;
			}
		},

		/**
		 * @param {efwdUI.commands.types.DeleteEntities} command
		 * @private
		 */
		__checkNotifyUserForDeleteEntities: function (command)
		{
			var entities = command.getEntities();
			var hiddenEntities = entities.length;

			entities.forEach(function (entity, index)
			{
				if (entity instanceof efwdDal.entity.Activity)
				{
					if (this.__dialogsModel.isGlobalActivitiesListDialog())
					{
						--hiddenEntities;
					}
				}
				else if (entity instanceof efwdDal.entity.Action && entity.isStatusesLess())
				{
					if (this.__dialogsModel.isGlobalActionsListDialog())
					{
						--hiddenEntities;
					}
				}
				else
				{
					--hiddenEntities;
				}
			}, this);

			return hiddenEntities > 0;
		},

		/**
		 * @param {efwdUI.commands.types.AbstractCommand} command
		 * @param {Boolean} undo
		 * @private
		 */
		__checkSelection: function (command, undo)
		{
			var select;
			if (undo)
			{
				switch (command.classname)
				{
					case "efwdUI.commands.types.DeleteEntities":
						select = command.getEntities();
						break;
				}
			}
			else
			{
				switch (command.classname)
				{
					case "efwdUI.commands.types.CreateAction":
						select = [command.getAction()];
						break;

					case "efwdUI.commands.types.CreateStatus":
						select = [command.getStatus()];
						break;

					case "efwdEditor.commands.types.CreateActionAndDestStatus":
						select = [command.getDestStatus()];
						break;

					case "efwdEditor.commands.types.PasteEntities":
						select = command.getCreatedEntities();
						break;
				}
			}

			if (select)
			{
				this.__selectionModel.selectExact(select.map(function (entity)
				{
					return this.__canvasElementsHandling.getElementByEntity(entity);
				}, this));
			}
		}
	},

	defer: function (statics, members, properties)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}

});
