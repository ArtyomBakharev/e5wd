/**
 * Создаёт действие и статус вместе с ним
 * @extends {efwdUI.commands.types.AbstractCommand}
 */
qx.Class.define("efwdEditor.commands.types.CreateActionAndDestStatus", {

	extend: efwdUI.commands.types.AbstractCommand,

	/**
	 * @param {library.IServiceLocator} serviceLocator
	 */
	construct: function (serviceLocator)
	{
		this.__serviceLocator = serviceLocator;
	},

	members: {
		/**
		 * @param {efwdDal.entity.Status} sourceStatus
		 * @param {Function} destStatusPositionCallback
		 */
		initialize: function (sourceStatus, destStatusPositionCallback)
		{
			this.__sourceStatus = sourceStatus;
			this.__destStatusPositionCallback = destStatusPositionCallback;
		},

		/**
		 * @return {efwdDal.entity.Action}
		 */
		getAction: function()
		{
			return this.__createActionCommand.getAction();
		},

		/**
		 * @return {efwdDal.entity.Status}
		 */
		getDestStatus: function()
		{
			return this.__createStatusCommand.getStatus();
		},

		/**
		 * @return {efwdDal.entity.Status}
		 */
		getSourceStatus: function()
		{
			return this.__sourceStatus;
		},

		getTitle: function ()
		{
			return "creating new action and status";
		},

		execute: function ()
		{
			/** @type {efwdUI.commands.types.CreateStatus} */
			this.__createStatusCommand = this.__serviceLocator.getService(efwdUI.commands.types.CreateStatus);
			this.__createStatusCommand.initialize(this.__destStatusPositionCallback);
			this.__createStatusCommand.execute();

			/** @type {efwdUI.commands.types.CreateAction} */
			this.__createActionCommand = this.__serviceLocator.getService(efwdUI.commands.types.CreateAction);
			this.__createActionCommand.initialize(this.__sourceStatus, this.__createStatusCommand.getStatus());
			this.__createActionCommand.execute();

			return true;
		},

		undo: function ()
		{
			this.__createActionCommand.undo();
			this.__createStatusCommand.undo();
		},

		redo: function ()
		{
			this.__createStatusCommand.redo();
			this.__createActionCommand.redo();
		}
	},

	defer: function (statics, members, properties)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}

});
