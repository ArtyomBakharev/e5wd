/*
 #ignore(efwdRenderer.)
 #ignore(efwdRenderer.*)
 */

/**
 * Команда перемещения точки соединения коннектора действия. Команда предоставляет методы createJoint, deleteJoint,
 * moveJoint, которые могут использоваться сами по себе.
 * @extends {efwdUI.commands.types.AbstractCommand}
 */
qx.Class.define("efwdEditor.commands.types.MoveActionConnectorJoint", {

	extend: efwdUI.commands.types.AbstractCommand,

	properties: {
		/**
		 * Индекс точки соединения
		 */
		jointIndex: {
			init: null,
			check: "Integer"
		},
		/**
		 * Положения центра точки
		 */
		point: {
			init: null,
			check: "Object"
		}
	},

	/**
	 * @param {efwdDal.Context} context
	 */
	construct: function (context)
	{
		this.__context = context;
	},

	members: {
		/**
		 * @param {efwdDal.entity.Action} action
		 */
		initialize: function (action)
		{
			this.__action = action;
			this.__joints = action.getSource().getMetadata().getJoints();
			this.__name = action.getSource().getName();
		},

		/**
		 * Создать точку
		 *
		 * @param [undo=false]
		 */
		createJoint: function (undo)
		{
			if (qx.core.Environment.get("qx.debug"))
			{
				if (!this.__executed)
				{
					this.assertInteger(this.getJointIndex());
					this.assertObject(this.getPoint());
					this.assert(!this.__jointCreated);
				}
			}

			var point = undo ? this.__startPoint : this.getPoint();
			this.__joints.insertAt(this.getJointIndex(), qx.data.marshal.Json.createModel(point));

			if (!undo)
			{
				this.__jointCreated = true;
			}
		},

		/**
		 * Удалить точку
		 *
		 * @param [undo=false]
		 */
		deleteJoint: function (undo)
		{
			if (qx.core.Environment.get("qx.debug"))
			{
				if (!this.__executed)
				{
					this.assertInteger(this.getJointIndex());
					this.assert(!this.__jointDeleted);
				}
			}

			if (!undo)
			{
				if (!this.__startPoint)
				{
					var position = this.__joints.getItem(this.getJointIndex());
					this.__startPoint = qx.util.Serializer.toNativeObject(position);
				}
				this.__jointDeleted = true;
			}

			this.__joints.removeAt(this.getJointIndex());
		},

		getTitle: function ()
		{
			return "transforming connector of action '" + this.__name + "'";
		},

		/**
		 * @return {Boolean}
		 */
		execute: function ()
		{
			var affected = !(this.__jointCreated && this.__jointDeleted);
			if (affected && !this.__jointCreated && !this.__jointDeleted
				&& efwdRenderer.Geo.isPointsEquals(this.__startPoint, this.getPoint()))
			{
				affected = false;
			}

			if (affected)
			{
				this.__context.saveChanges();
			}

			this.__executed = true;
			return affected;
		},

		/**
		 * Переместить точку
		 *
		 * @param [undo=false]
		 */
		moveJoint: function (undo)
		{
			if (qx.core.Environment.get("qx.debug"))
			{
				if (!this.__executed)
				{
					this.assertInteger(this.getJointIndex());
					this.assertObject(this.getPoint());
					this.assert(!this.__jointDeleted);
				}
			}

			var position = this.__joints.getItem(this.getJointIndex());
			var currentPoint = qx.util.Serializer.toNativeObject(position);
			if (!this.__startPoint)
			{
				this.__startPoint = currentPoint;
			}

			var endPoint = undo ? this.__startPoint : this.getPoint();

			if (!efwdRenderer.Geo.isPointsEquals(currentPoint, endPoint))
			{
				position.setX(endPoint.x);
				position.setY(endPoint.y);
			}
		},

		redo: function ()
		{
			if (this.__jointCreated)
			{
				this.createJoint();
			}
			else if (this.__jointDeleted)
			{
				this.deleteJoint();
			}
			else
			{
				this.moveJoint();
			}

			this.__context.saveChanges();
		},

		undo: function ()
		{
			if (this.__jointCreated)
			{
				this.deleteJoint(true);
			}
			else if (this.__jointDeleted)
			{
				this.createJoint(true);
			}
			else
			{
				this.moveJoint(true);
			}

			this.__context.saveChanges();
		}
	},

	defer: function (statics, members, properties)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}

});
