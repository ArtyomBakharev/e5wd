/*
 #ignore(efwdRenderer.)
 #ignore(efwdRenderer.*)
 */

qx.Class.define("efwdEditor.commands.types.MoveActionConnectorPinPoint", {

	extend: efwdUI.commands.types.AbstractCommand,

	/**
	 * @param {efwdDal.Context} context
	 */
	construct: function (context)
	{
		this.__context = context;
	},

	members: {
		/**
		 * @param {efwdDal.entity.Action} action
		 * @param {Boolean} [isDestPoint=false]
		 */
		initialize: function (action, isDestPoint)
		{
			this.__action = action;
			this.__point = this.__action.getSource().getMetadata().get(isDestPoint ? "destPoint" : "sourcePoint");
			this.__startPoint = qx.util.Serializer.toNativeObject(this.__point);
			this.__name = action.getSource().getName();
		},

		movePoint: function(point)
		{
			this.__moveTo = point;
			this.__point.setX(point.x);
			this.__point.setY(point.y);
		},

		unpinPoint: function()
		{
			this.movePoint({x: null, y: null});
		},

		getTitle: function ()
		{
			return "moving pin-point of action '" + this.__name + "'";
		},

		/**
		 * @return {Boolean}
		 */
		execute: function ()
		{
			var affected = !!this.__moveTo;
			if (affected)
			{
				this.__context.saveChanges();
			}

			return affected;
		},

		redo: function ()
		{
			this.movePoint(this.__moveTo);
			this.__context.saveChanges();
		},

		undo: function ()
		{
			this.__point.setX(this.__startPoint.x);
			this.__point.setY(this.__startPoint.y);
			this.__context.saveChanges();
		}
	},

	defer: function (statics, members, properties)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}

});
