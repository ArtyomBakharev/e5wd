/*
 #ignore(efwdRenderer.)
 #ignore(efwdRenderer.*)
 */

/**
 * Команда перемещения надписи для действия
 * @extends {efwdUI.commands.types.AbstractCommand}
 */
qx.Class.define("efwdEditor.commands.types.MoveActionLabel", {

	extend: efwdUI.commands.types.AbstractCommand,

	/**
	 * @param {efwdDal.Context} context
	 */
	construct: function (context)
	{
		this.__context = context;
	},

	properties: {
		/**
		 * Положение надписи
		 */
		point: {
			init: null,
			check: "Object"
		}
	},

	members: {
		/**
		 * @param {efwdDal.entity.Action} action
		 */
		initialize: function (action)
		{
			this.__action = action;
			this.__position = action.getSource().getMetadata().getLabelPosition();
			this.__sourcePoint = {x: this.__position.getX(), y: this.__position.getY()};
			this.__name = this.__action.getSource().getName();
		},

		/**
		 * @param {Boolean} [saveChanges=true] сохранять ли изменения на сервере
		 */
		execute: function (saveChanges)
		{
			saveChanges === undefined && (saveChanges = true);

			this.__moveTo(this.getPoint());
			if (saveChanges && !efwdRenderer.Geo.isPointsEquals(this.__sourcePoint, this.getPoint()))
			{
				this.__context.saveChanges();
				return true;
			}

			return false;
		},

		getTitle: function ()
		{
			return "label moving of action '" + this.__name + "'";
		},

		undo: function ()
		{
			this.__moveTo(this.__sourcePoint);
			this.__context.saveChanges();
		},

		/**
		 * @private
		 */
		__moveTo: function (point)
		{
			this.__position.setX(point.x);
			this.__position.setY(point.y);
		}
	},

	defer: function (statics, members, properties)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}

});
