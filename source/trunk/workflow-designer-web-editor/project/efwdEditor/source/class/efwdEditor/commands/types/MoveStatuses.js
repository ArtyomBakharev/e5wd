/*
 #ignore(efwdRenderer.)
 #ignore(efwdRenderer.*)
 */

/**
 * Комманда, отвечающая за позиционирование статусов на канве
 * @extends {efwdUI.commands.types.AbstractCommand}
 */
qx.Class.define("efwdEditor.commands.types.MoveStatuses", {

	extend: efwdUI.commands.types.AbstractCommand,

	/**
	 * @param {efwdDal.Context} context
	 * @param {efwdDal.repository.ActionsRepository} actionsRepository
	 */
	construct: function (context, actionsRepository)
	{
		this.__context = context;
		this.__actionsRepository = actionsRepository;

		this.__oldOffset = {x: 0, y: 0};
	},

	members: {
		/**
		 * @param {efwdDal.entity.Status[]} statuses
		 */
		initialize: function (statuses)
		{
			this.__statuses = statuses;

			//экшены между статусами
			this.__actions = [];
			this.__statuses.forEach(function (status1, index1)
			{
				this.__statuses.forEach(function (status2, index2)
				{
					if (index2 > index1)
					{
						this.__actions.append(this.__actionsRepository.getActionsByStatuses(status1, status2, true));
					}
				}, this);
			}, this);
		},

		execute: function ()
		{
			this.__context.saveChanges();
			return !!(this.__offset && !efwdRenderer.Geo.isPointsEquals(this.__offset, {x: 0, y: 0}));
		},

		getEntities: function()
		{
			return this.__statuses.concat(this.__actions);
		},

		getTitle: function ()
		{
			return this.__statuses.length > 1
				? "moving status '" + this.__statuses[0].getSource().getName() + "'"
				: "moving elements";
		},

		/**
		 * запускается до запуска execute!
		 */
		move: function (offset)
		{
			this.__offset = offset;
			this.__move(efwdRenderer.Geo.subPoints(this.__offset, this.__oldOffset));

			this.__oldOffset = this.__offset;
		},

		redo: function()
		{
			this.__move(this.__offset);
			this.__context.saveChanges();
		},

		undo: function ()
		{
			this.__move(efwdRenderer.Geo.mulVector(this.__offset, -1));
			this.__context.saveChanges();
		},

		__getPositions: function ()
		{
			return this.__statuses.map(function (x){ return x.getSource().getMetadata() })
				.concat(this.__actions.flatten(function (x){ return x.getSource().getMetadata().getJoints().toArray() }));
		},

		__move: function(offset)
		{
			this.__getPositions().forEach(function (position)
			{
				position.setX(position.getX() + offset.x);
				position.setY(position.getY() + offset.y);
			}, this);
		}
	},

	defer: function (statics, members, properties)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}

});
