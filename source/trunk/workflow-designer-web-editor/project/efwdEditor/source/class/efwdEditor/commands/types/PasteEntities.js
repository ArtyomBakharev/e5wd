/**
 * @extends {efwdUI.commands.types.AbstractCommand}
 */
qx.Class.define("efwdEditor.commands.types.PasteEntities", {

	extend: efwdUI.commands.types.AbstractCommand,

	/**
	 * @param {efwdDal.Context} context
	 * @param {library.IServiceLocator} serviceLocator
	 * @param {efwdEditor.behavior.Clipboard} clipboard
	 */
	construct: function (context, serviceLocator, clipboard)
	{
		this.__context = context;
		this.__serviceLocator = serviceLocator;
		this.__clipboard = clipboard;
	},

	members: {

		execute: function ()
		{
			if (this.__clipboard.isEmpty())
			{
				return false;
			}

			this.__createdEntities = this.__clipboard.paste();

			/** @type {efwdEditor.commands.types.MoveStatuses} */
			var moveCommand = this.__serviceLocator.getService(efwdEditor.commands.types.MoveStatuses);
			moveCommand.initialize(this.__createdEntities.filter(function (x){ return x instanceof efwdDal.entity.Status }));
			moveCommand.move(efwdRenderer.Geo.mulVector(efwdConfig.efwdEditor.commands.types.PasteEntities.elementsOffset,
				this.__clipboard.getConsecutiveInsertionsCount()));

			this.__context.saveChanges();

			return true;
		},

		/**
		 * @return {efwdDal.entity.AbstractEntity[]}
		 */
		getCreatedEntities: function ()
		{
			return this.__createdEntities;
		},

		getTitle: function ()
		{
			return "pasting items";
		},

		redo: function ()
		{
			this.__deleteEntitiesCommand.undo();
		},

		undo: function ()
		{
			if (!this.__deleteEntitiesCommand)
			{
				/** @type {efwdUI.commands.types.DeleteEntities} */
				this.__deleteEntitiesCommand = this.__serviceLocator.getService(efwdUI.commands.types.DeleteEntities);
				this.__deleteEntitiesCommand.initialize(this.__createdEntities);
				this.__deleteEntitiesCommand.execute();
			}
			else
			{
				this.__deleteEntitiesCommand.redo();
			}
		}
	}

});
