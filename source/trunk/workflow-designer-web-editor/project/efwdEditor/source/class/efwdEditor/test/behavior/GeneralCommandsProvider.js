/**
 * @extends {efwdEditor.TestCase}
 */
qx.Class.define("efwdEditor.test.behavior.GeneralCommandsProvider", {

	extend: efwdEditor.TestCase,

	members: {

		setUp: function ()
		{
			this._ioc.clear();
			this.__provider = this._ioc.constructService(efwdUI.behavior.GeneralCommandsProvider);
			this.__commandsQueue = this._ioc.getService(efwdUI.commands.Queue);
		},

		"test: getDeleteElementCommand": function ()
		{
			var element = this.stubClass(efwdEditor.canvas.views.types.EntityElement);
			var entity = new efwdDal.entity.Status({});
			element.getModel = this.stub().returns(entity);

			this._ioc.getService(efwdEditor.behavior.SelectionModel).getSelection = this.stub().returns(new qx.data.Array([element]));

			var deleteCommand = this._ioc.nextTransientService(efwdUI.commands.types.DeleteEntities);

			var command = this.__provider.getDeleteElementsCommand();
			command.execute();
			this.assertArrayEquals([entity], deleteCommand.initialize.args.last()[0]);
			this.assertLastCall(this.__commandsQueue.addCommand, [deleteCommand]);
		}

	}

});
