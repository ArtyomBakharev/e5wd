/**
 * @extends {efwdEditor.TestCase}
 */
qx.Class.define("efwdEditor.test.commands.Queue", {

	extend: efwdEditor.TestCase,

	members: {

		setUp: function ()
		{
			this._ioc.clear();
			this.__queue = this._ioc.constructService(efwdUI.commands.Queue);
		},

		"test: addCommand": function ()
		{
			var undoArray = this.__queue.getUndoArray();
			var redoArray = this.__queue.getRedoArray();

			//add first item
			var firstCommand = this.__createCommandStub();
			this.__queue.addCommand(firstCommand);
			this.assertCalledOnce(firstCommand.execute, "Command should be executed after adding");
			this.assertArrayEquals([firstCommand], undoArray.toArray(), "Command should be unshifted to undoArray after adding");

			//add second item
			var secondCommand = this.__createCommandStub();
			this.__queue.addCommand(secondCommand);
			this.assertArrayEquals([secondCommand, firstCommand], undoArray.toArray(), "Command should be unshifted to undoArray after adding");

			//add command after canceling
			this.__queue.undo();
			this.__queue.undo();
			this.__queue.addCommand(this.__createCommandStub());
			this.assertEquals(0, redoArray.getLength(), "Redo array should be empty after command executing");
		},

		"test: getRedoArray": function ()
		{
			this.assertInstance(this.__queue.getRedoArray(), qx.data.Array);
			this.assertEquals(0, this.__queue.getRedoArray().getLength());
		},

		"test: getUndoArray": function ()
		{
			this.assertInstance(this.__queue.getUndoArray(), qx.data.Array);
			this.assertEquals(0, this.__queue.getUndoArray().getLength());
		},

		"test: undo, undoTo, redo, redoTo": function ()
		{
			var redoArray = this.__queue.getRedoArray().toArray();
			var undoArray = this.__queue.getUndoArray().toArray();
			var beforeUndoListener = this.stub();
			var beforeRedoListener = this.stub();
			this.__queue.addListener("beforeUndoBatch", beforeUndoListener);
			this.__queue.addListener("beforeRedoBatch", beforeRedoListener);

			var commands = [this.__createCommandStub(), this.__createCommandStub(), this.__createCommandStub()];
			this.__queue.addCommand(commands[0]);
			this.__queue.addCommand(commands[1]);
			this.__queue.addCommand(commands[2]);

			//undo
			this.__queue.undo();
			this.assertArrayEquals([commands[1], commands[0]], undoArray);
			this.assertArrayEquals([commands[2]], redoArray);
			this.assertCalledOnce(commands[2].undo);
			this.assertCallCount(beforeUndoListener, 1);

			this.__queue.undoTo(commands[0]);
			this.assertArrayEquals([], undoArray);
			this.assertArrayEquals([commands[0], commands[1], commands[2]], redoArray);
			this.assertCalledOnce(commands[1].undo);
			this.assertCalledOnce(commands[2].undo);
			this.assertCallCount(beforeUndoListener, 2);

			this.__queue.undo();
			this.assertArrayEquals([], undoArray);
			this.assertArrayEquals([commands[0], commands[1], commands[2]], redoArray);

			//redo
			this.__queue.redo();
			this.assertArrayEquals([commands[0]], undoArray);
			this.assertArrayEquals([commands[1], commands[2]], redoArray);
			this.assertCalledOnce(commands[0].redo);
			this.assertCallCount(beforeRedoListener, 1);

			this.__queue.redoTo(commands[2]);
			this.assertArrayEquals([commands[2], commands[1], commands[0]], undoArray);
			this.assertArrayEquals([], redoArray);
			this.assertCalledOnce(commands[1].redo);
			this.assertCalledOnce(commands[2].redo);
			this.assertCallCount(beforeRedoListener, 2);

			this.__queue.redo();
			this.assertArrayEquals([commands[2], commands[1], commands[0]], undoArray);
			this.assertArrayEquals([], redoArray);

			//undoTo
			this.__queue.undoTo(commands[1]);
			this.assertArrayEquals([commands[0]], undoArray);
			this.assertArrayEquals([commands[1], commands[2]], redoArray);
			this.__queue.undo();

			//redoTo
			this.__queue.redoTo(commands[1]);
			this.assertArrayEquals([commands[1], commands[0]], undoArray);
			this.assertArrayEquals([commands[2]], redoArray);
		},

		/**
		 * @private
		 */
		__createCommandStub: function ()
		{
			return this.stubClass(efwdUI.commands.types.AbstractCommand);
		}

	}
});
