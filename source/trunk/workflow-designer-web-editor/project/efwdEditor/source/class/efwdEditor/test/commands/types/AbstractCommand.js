/**
 * @extends {efwdEditor.TestCase}
 */
qx.Class.define("efwdEditor.test.commands.types.AbstractCommand", {

	type: "abstract",

	extend: efwdEditor.TestCase,

	members: {

		"test: getTitle": function ()
		{
			(this._commands || [this._command])
				.forEach(function (x){ this.assert(typeof x.getTitle() == "string") }, this);
		}

	}

});
