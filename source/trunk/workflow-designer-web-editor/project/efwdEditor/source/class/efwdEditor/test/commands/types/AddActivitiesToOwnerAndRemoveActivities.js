/**
 * @extends {efwdEditor.test.commands.types.AbstractCommand}
 */
qx.Class.define("efwdEditor.test.commands.types.AddActivitiesToOwnerAndRemoveActivities", {

	extend: efwdEditor.test.commands.types.AbstractCommand,

	members: {

		setUp: function ()
		{
			this.__context = this.stubClass(efwdDal.Context);

			this.__activities = [new efwdDal.entity.Activity({}), new efwdDal.entity.Activity({})];
			this.__owner = this.stubClass(efwdDal.entity.Status);
			this.__category = "some";

			this.__addCommand = new efwdUI.commands.types.AddActivitiesToOwner(this.__context);
			this.__addCommand.initialize(this.__activities, this.__owner, this.__category);

			this.__removeCommand = new efwdUI.commands.types.RemoveActivities(this.__context);
			this.__removeCommand.initialize(this.__activities, this.__owner, this.__category);

			this._commands = [this.__addCommand, this.__removeCommand];
		},

		"test: undo, redo - direct": function ()
		{
			this.__testUndoRedo(false);
		},

		"test: undo, redo - reverse": function ()
		{
			this.__testUndoRedo(true);
		},

		/**
		 * @private
		 */
		__testUndoRedo: function (reverse)
		{
			var command = reverse ? this.__removeCommand : this.__addCommand;

			var directMethod = reverse ? "removeActivity" : "addActivity";
			var reverseMethod = reverse ? "addActivity" : "removeActivity";

			command.execute();

			command.undo();
			this.assertCalledTwice(this.__context.saveChanges);
			this.assertCalledTwice(this.__owner[reverseMethod]);
			this.assertArrayEquals([this.__category, this.__activities[0]], this.__owner[reverseMethod].args[0]);
			this.assertArrayEquals([this.__category, this.__activities[1]], this.__owner[reverseMethod].args[1]);

			this.__owner[directMethod] = this.stub();
			command.redo();
			this.assertCalledThrice(this.__context.saveChanges);
			this.assertCalledTwice(this.__owner[directMethod]);
			this.assertArrayEquals([this.__category, this.__activities[0]], this.__owner[directMethod].args[0]);
			this.assertArrayEquals([this.__category, this.__activities[1]], this.__owner[directMethod].args[1]);
		}

	}

});
