/**
 * @extends {efwdEditor.test.commands.types.AbstractCommand}
 */
qx.Class.define("efwdEditor.test.commands.types.CreateAction", {

	extend: efwdEditor.test.commands.types.AbstractCommand,


	members: {

		setUp: function ()
		{
			this.__actionsRepository = this.stubClass(efwdDal.repository.ActionsRepository, {
				createAction: this.stub()
					.returns(this.__testAction = this.stubClass(efwdDal.entity.Action, [
					{}
				]))
			});

			this.__context = this.stubClass(efwdDal.Context);

			this._command = new efwdUI.commands.types.CreateAction(this.__actionsRepository, this.__context);
			this._command.initialize(new efwdDal.entity.Status({id: "1"}), new efwdDal.entity.Status({id: "2"}));
		},

		"test: undo, redo": function ()
		{
			this._command.execute();

			this._command.undo();
			this.assertCalledOnce(this.__testAction.remove);
			this.assertCalledTwice(this.__context.saveChanges);

			this._command.redo();
			this.assertCalledOnce(this.__testAction.restore);
			this.assertCalledThrice(this.__context.saveChanges);
		}
	}

});
