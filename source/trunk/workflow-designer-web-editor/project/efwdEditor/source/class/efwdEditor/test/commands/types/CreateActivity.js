/**
 * @extends {efwdEditor.test.commands.types.AbstractCommand}
 */
qx.Class.define("efwdEditor.test.commands.types.CreateActivity", {

	extend: efwdEditor.test.commands.types.AbstractCommand,


	members: {

		setUp: function ()
		{
			this.__activityType = "someType";
			var $this = this;
			this.__activityRepository = this.stubClass(efwdDal.repository.ActivitiesRepository, {
				createActivity: function (type)
				{
					$this.assertEquals($this.__activityType, type, "Incorrect activity type");
					return $this.__activity = $this.stubClass(efwdDal.entity.Activity, [
						{}
					]);
				}
			});

			this.__context = this.stubClass(efwdDal.Context);

			this.__owner = this.stubClass(efwdDal.entity.Status);
			this.__category = "some";

			this.__createCommand(true);
		},

		"test: undo, redo width owner": function ()
		{
			this.__testUndoRedo(true);
		},

		"test: undo, redo widthout owner": function ()
		{
			this.__testUndoRedo(false);
		},

		/**
		 * @private
		 */
		__createCommand: function (withOwner)
		{
			this._command = new efwdUI.commands.types.CreateActivity(this.__activityRepository, this.__context);
			this._command.initialize(this.__activityType, withOwner && this.__owner, withOwner && this.__category);
		},

		/**
		 * @private
		 */
		__testUndoRedo: function (withOwner)
		{
			this.__createCommand(withOwner);

			this._command.execute();

			this._command.undo();

			this.assertCalledTwice(this.__context.saveChanges);
			this.assertCalledOnce(this.__activity.remove);

			this.__owner.addActivity = this.stub();
			this._command.redo();

			this.assertCalledThrice(this.__context.saveChanges);
			if (withOwner)
			{
				withOwner && this.assertCalledOnce(this.__owner.addActivity);
				this.assertArrayEquals([this.__category, this.__activity], this.__owner.addActivity.args[0]);
			}
			this.assertCalledOnce(this.__activity.restore);
		}
	}

});
