/**
 * @extends {efwdEditor.test.commands.types.AbstractCommand}
 */
qx.Class.define("efwdEditor.test.commands.types.CreateStatus", {

	extend: efwdEditor.test.commands.types.AbstractCommand,


	members: {

		setUp: function ()
		{
			var $this = this;

			this.__statusesRepository = this.stubClass(efwdDal.repository.StatusesRepository, {
				createStatus: this.stub()
					.returns(this.__testStatus = new efwdDal.entity.Status({}))
			});

			this.__context = this.stubClass(efwdDal.Context);

			this._command = new efwdUI.commands.types.CreateStatus(this.__statusesRepository, this.__context);
			this._command.initialize(function (x)
			{
				$this.assertInstance(x, efwdDal.entity.Status);
				return {x: 1, y: 1};
			});
		},

		"test: undo, redo": function ()
		{
			this._command.execute();

			this.stub(this.__testStatus, "remove");
			this._command.undo();
			this.assertCalledOnce(this.__testStatus.remove);
			this.assertCalledTwice(this.__context.saveChanges);

			this.stub(this.__testStatus, "restore");
			this._command.redo();
			this.assertCalledOnce(this.__testStatus.restore);
			this.assertCalledThrice(this.__context.saveChanges);
		}
	}

});
