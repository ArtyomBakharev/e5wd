/**
 * @extends {efwdEditor.test.commands.types.AbstractCommand}
 */
qx.Class.define("efwdEditor.test.commands.types.DeleteEntities", {

	extend: efwdEditor.test.commands.types.AbstractCommand,

	members: {

		setUp: function ()
		{
			this._ioc.clear();
			this.__context = this._ioc.getService(efwdDal.Context);
			this.__createCommand(efwdDal.entity.Activity);
		},

		"test: initialize": function ()
		{
			var $this = this;

			this._command = this._ioc.constructService(efwdUI.commands.types.DeleteEntities);
			this.assertException(function ()
			{
				$this._command.initialize([new efwdDal.entity.Activity({}), new efwdDal.entity.Action({})]);
			});
		},

		"test: execute, undo, redo": function ()
		{
			this._command.execute();
			this.__entities.forEach(function (x){ this.assertCalledOnce(x.remove) }, this);
			this.assertCalledOnce(this.__context.saveChanges);

			this._command.undo();
			this.__entities.forEach(function (x){ this.assertCalledOnce(x.restore) }, this);
			this.assertCalledTwice(this.__context.saveChanges);

			this._command.redo();
			this.__entities.forEach(function (x){ this.assertCalledTwice(x.remove) }, this);
			this.assertCalledThrice(this.__context.saveChanges);
		},

		"test: dumping": function ()
		{
//			this.__testDumping(efwdDal.entity.Action, efwdUI.commands.types.statedumper.Action);
			this.__testDumping(efwdDal.entity.Activity, efwdUI.commands.types.statedumper.Activity);
			this.__testDumping(efwdDal.entity.Status, efwdUI.commands.types.statedumper.Status);
		},

		/**
		 * @private
		 */
		__testDumping: function (entityClass, dumperClass)
		{
			var dumpers = [this._ioc.nextTransientService(dumperClass),
				this._ioc.nextTransientService(dumperClass)];

			this.__createCommand(entityClass);
			this.assertNotCalled(dumpers[0].dump);

			this._command.execute();
			this.assertLastCall(dumpers[0].initialize, [this.__entities[0]], 1);
			this.assertCalledOnce(dumpers[0].dump);
			this.assertCalledOnce(dumpers[1].dump);

			this._command.undo();
			this.assertCalledOnce(dumpers[0].restore);
			this.assertCalledOnce(dumpers[1].restore);
			this.assertCallOrder(this.__entities[0].restore, dumpers[0].restore);

			this._command.redo();
			this.assertCalledTwice(dumpers[0].dump);
			this.assertCalledTwice(dumpers[1].dump);
		},

		/**
		 * @private
		 */
		__createCommand: function (entityClass)
		{
			this._command = this._ioc.constructService(efwdUI.commands.types.DeleteEntities);
			this.__entities = this.stubClassArray(entityClass, 2);
			this._command.initialize(this.__entities);
		}
	}

});
