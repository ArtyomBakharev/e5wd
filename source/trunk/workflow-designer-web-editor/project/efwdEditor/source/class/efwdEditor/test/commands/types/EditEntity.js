/**
 * @extends {efwdEditor.test.commands.types.AbstractCommand}
 */
qx.Class.define("efwdEditor.test.commands.types.EditEntity", {

	extend: efwdEditor.test.commands.types.AbstractCommand,

	members: {

		setUp: function ()
		{
			this._ioc.clear();
			this.__context = this._ioc.getService(efwdDal.Context);
			this.__entity = new efwdDal.entity.Status({id: "1", flag: 1, deep: {a: 1}, noChange: 10});

			this._command = this._ioc.constructService(efwdUI.commands.types.EditEntity);
			this._command.initialize(this.__entity, this.__entity.getSource(), ["flag", "deep", "emptyFirst"]);
		},

		"test: undo, redo": function ()
		{
			this.stub(this.__entity, "update");

			var source = this.__entity.getSource();
			source.flag = 2;
			source.id = "2";
			source.noChange = 20;
			var oldDeep = source.deep;
			source.deep = {b: 1};
			source.emptyFirst = 2;

			this._command.execute();

			this._command.undo();
			this.assertCalledTwice(this.__entity.update);
			this.assertCalledTwice(this.__context.saveChanges);
			this.assertIdentical(source, this.__entity.getSource());
			this.assertIdentical(1, source.flag);

			this.assertIdentical("2", source.id);
			this.assertIdentical(20, source.noChange);
			this.assertUndefined(source.emptyFirst);

			var deep = source.deep;
			this.assertIdentical(1, deep.a);
			this.assertFalse("b" in deep);
			this.assertNotIdentical(oldDeep, deep);

			source.id = "3";
			source.noChange = 30;

			this._command.redo();
			this.assertCalledThrice(this.__entity.update);
			this.assertCalledThrice(this.__context.saveChanges);
			this.assertIdentical(2, source.flag);

			this.assertIdentical("3", source.id);
			this.assertIdentical(30, source.noChange);
			this.assertIdentical(2, source.emptyFirst);
		}
	}

});
