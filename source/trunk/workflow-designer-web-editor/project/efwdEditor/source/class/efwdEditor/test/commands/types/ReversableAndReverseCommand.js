/**
 * @extends {efwdEditor.TestCase}
 */
qx.Class.define("efwdEditor.test.commands.types.ReversableAndReverseCommand", {

	extend: efwdEditor.TestCase,


	members: {

		setUp: function ()
		{
			this.__reversableCommand = this.stubClass(efwdUI.commands.types.ReversableCommand);
			var reverseCommandClass = library.dev.Unit.wrapClass(efwdUI.commands.types.ReverseCommand);
			this.__reverseCommand = new reverseCommandClass(this.__reversableCommand);
		},

		"test: execute, undo, redo": function ()
		{
			this.__reverseCommand.execute();
			this.assertCalledOnce(this.__reversableCommand.undo);

			this.__reverseCommand.undo();
			this.assertCalledOnce(this.__reversableCommand.execute);

			this.__reverseCommand.redo();
			this.assertCalledTwice(this.__reversableCommand.undo);
		},

		"test: getTitle": function ()
		{
			this.__reversableCommand.getTitle = function (reverse){ return reverse ? "r" : "d" };

			this.assertIdentical("r", this.__reverseCommand.getTitle());
		}

	}

});
