/**
 * @extends {efwdEditor.TestCase}
 */
qx.Class.define("efwdEditor.test.commands.types.statedumper.Activity", {

	extend: efwdEditor.TestCase,

	members: {

		setUp: function ()
		{
			this.__activity = new efwdDal.entity.Activity({});

			this.__dumper = this._ioc.constructService(efwdUI.commands.types.statedumper.Activity);
			this.__dumper.initialize(this.__activity);

			this.__activitiesRepository = this._ioc.getService(efwdDal.repository.ActivitiesRepository);
		},

		"test: dump, restore": function ()
		{
			var status = this.stubClass(efwdDal.entity.Status);
			var action = this.stubClass(efwdDal.entity.Action);
			this.__activitiesRepository.getOwnersOfActivity.returns([
				{owner: status, category: "one"},
				{owner: status, category: "two"},
				{owner: action, category: "one"}
			]);

			this.__dumper.dump();
			this.__dumper.restore();

			this.assertCalledWith(status.addActivity, "one", this.__activity);
			this.assertCalledWith(status.addActivity, "two", this.__activity);
			this.assertCalledWith(action.addActivity, "one", this.__activity);
		}

	}

});
