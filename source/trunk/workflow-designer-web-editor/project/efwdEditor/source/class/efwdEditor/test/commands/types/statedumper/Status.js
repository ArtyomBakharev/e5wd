/**
 * @extends {efwdEditor.TestCase}
 */
qx.Class.define("efwdEditor.test.commands.types.statedumper.Status", {

	extend: efwdEditor.TestCase,

	members: {

		setUp: function ()
		{
			this.__status = new efwdDal.entity.Status({});

			this.__dumper = this._ioc.constructService(efwdUI.commands.types.statedumper.Status);

			this.__actionsRepository = this._ioc.getService(efwdDal.repository.ActionsRepository);
		},

		"test: dump, restore": function ()
		{
			var $this = this;

			var actions = this.stubClassArray(efwdDal.entity.Action, 2);

			this.__dumper.initialize(this.__status);

			this.__actionsRepository.getActionsByStatus = function (status)
			{
				if (status == $this.__status)
				{
					return actions;
				}
			};

			this.__dumper.dump();

			this.__dumper.restore();

			this.assertCallOrder(actions[0].restore, actions[1].restore);
		}

	}

});
