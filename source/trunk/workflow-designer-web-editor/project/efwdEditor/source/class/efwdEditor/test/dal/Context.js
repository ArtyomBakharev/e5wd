/**
 * @extends {efwdEditor.TestCase}
 */
qx.Class.define("efwdEditor.test.dal.Context", {

	extend: efwdEditor.TestCase,

	members: {

		setUp: function ()
		{
			this._ioc.clear();
			this.__saver = this._ioc.getService(efwdDal.saving.AbstractSaver);
			this.__saver.mergeChangeUnits.returns(false);
			this.__idsMapper = this._ioc.getService(efwdDal.IdsMapper);
			this.__context = this._ioc.constructService(efwdDal.Context);
		},

		"test: attachEntity and idsMapper": function ()
		{
			//entity from server
			var entity = new efwdDal.entity.Status({id: "sid"});

			var preEntityAttach = this.stub();
			this.__context.addListener("preEntityAttach", preEntityAttach);

			this.__context.attachEntity(entity);

			this.assertCallOrder(this.__idsMapper.registerEntity, preEntityAttach);
			this.assertLastCall(this.__idsMapper.registerEntity, [entity], 1);
		},

		"test: attachEntity, remove, restore": function ()
		{
			var entity = new efwdDal.entity.Status({});
			this.__context.attachEntity(entity);

			entity.remove();

			var entityAttached = false;
			this.__context.addListenerOnce("entityAttached", function (e)
			{
				this.assertIdentical(entity, e.getData().entity);
				entityAttached = true;
			}, this);
			entity.restore();

			this.assert(entityAttached, "Entity should be attached after restoring");
		},

		"test: saveChanges": function ()
		{
			var entity = new efwdDal.entity.Status({});
			this.__context.attachEntity(entity);
			entity.remove();
			entity.restore();
			this.__context.saveChanges();

			this.__saver.addChangeUnits = this.stub();
			entity.update();
			this.__context.saveChanges();

			this.assertCalledOnce(this.__saver.addChangeUnits);
			this.assertEquals(1, this.__saver.addChangeUnits.args[0].length);
		}
	}
});
