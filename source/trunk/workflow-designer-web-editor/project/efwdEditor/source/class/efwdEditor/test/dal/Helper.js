qx.Class.define("efwdEditor.test.dal.Helper", {

	type: "static",

	statics: {
		stubEmptyEntitiesArray: function (mock)
		{
			var array = mock.stubClass(efwdDal.EntitiesArray);
			array.toArray.returns([]);
			return array;
		},

		stubEntitiesArray: function (mock, entities)
		{
			var array = mock.stubClass(efwdDal.EntitiesArray);
			array.toArray.returns(entities);
			array.forEach = entities.forEach.bind(entities);
			return array;
		}
	}

});
