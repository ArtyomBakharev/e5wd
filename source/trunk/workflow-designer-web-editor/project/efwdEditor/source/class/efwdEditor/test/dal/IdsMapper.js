/**
 * @extends {efwdEditor.TestCase}
 */
qx.Class.define("efwdEditor.test.dal.IdsMapper", {

	extend: efwdEditor.TestCase,

	members: {

		setUp: function ()
		{
			this._ioc.clear();
			this.__idsMapper = this._ioc.constructService(efwdDal.IdsMapper);
		},

		"test: registerEntity": function ()
		{
			var entity = new efwdDal.entity.Status({});
			this.spy(entity, "initializeClientId");
			this.spy(entity, "initializeServerId");

			this.__idsMapper.registerEntity(entity);
			this.assertCalledOnce(entity.initializeClientId);
			this.assertNotCalled(entity.initializeServerId);
			this.assertIdentical(entity.getSource().getId(), entity.getSource().id);
			this.__idsMapper.registerEntity(entity);
			this.assertCalledOnce(entity.initializeClientId);

			entity = new efwdDal.entity.Status({id: "sid"});
			this.spy(entity, "initializeServerId");
			this.__idsMapper.registerEntity(entity);
			this.assertLastCall(entity.initializeServerId, ["sid"], 1);
			this.assertIdentical(entity.getSource().getId(), entity.getSource().id);
		},

		"test: mapToClientIds, mapToServerIds": function ()
		{
			this.__testMapping("action");
			this.__testMapping("status");
		},

		/**
		 * @private
		 */
		__testMapping: function (entityType)
		{
			var source = {id: "sid"};
			var categories = efwdConfig.efwdCommon.data.types.$activityCategories[entityType];
			categories.forEach(function (x){ source[x] = [x + 1, x + 2] });

			if (entityType == "action")
			{
				source.from_status_id = "fsi";
				source.to_status_id = "tsi";
			}

			var entity = entityType == "status" ? new efwdDal.entity.Status(source)
				: new efwdDal.entity.Action(source, new efwdDal.entity.Status({}), new efwdDal.entity.Status({}));

			this.__idsMapper.registerEntity(entity);

			this.assertNotIdentical(source[categories[0]][0], source[categories[0]][1]);

			if (entityType == "action")
			{
				var fromStatus = new efwdDal.entity.Status({id: "fsi"});
				var toStatus = new efwdDal.entity.Status({id: "tsi"});
				this.__idsMapper.registerEntity(fromStatus);
				this.__idsMapper.registerEntity(toStatus);

				this.assertIdentical(fromStatus.getSource().getId(), source.from_status_id);
				this.assertIdentical(toStatus.getSource().getId(), source.to_status_id);

				fromStatus.remove();
				fromStatus.restore();
				fromStatus.initializeServerId("fsi-new");
			}

			var assertions = [];
			categories.forEach(function (category, index)
			{
				var idIndex = index % 2;
				var activity = new efwdDal.entity.Activity({id: category + (idIndex + 1)});
				this.__idsMapper.registerEntity(activity);
				this.assertIdentical(source[category][idIndex], activity.getSource().getId());

				activity.remove();
				activity.restore();
				activity.initializeServerId(category + "new");

				assertions.push(function (newSource)
				{
					var ids = [category + (idIndex == 0 ? 2 : 1)];
					ids[idIndex == 0 ? "unshift" : "push"](category + "new");

					this.assertArrayEquals(ids, newSource[category]);
				});

			}, this);

			entity.remove();
			entity.restore();

			entity.initializeServerId("sid-new");

			var newSource = Object.deepCopy(source);
			this.__idsMapper.mapToServerIds(entity, newSource);

			this.assertIdentical("sid-new", newSource.id);

			if (entityType == "action")
			{
				this.assertIdentical("fsi-new", newSource.from_status_id);
			}

			assertions.callAll(this, newSource);
		}
	}
});
