/**
 * @extends {efwdEditor.TestCase}
 */
qx.Class.define("efwdEditor.test.dal.entity.AbstractEntity", {

	extend: efwdEditor.TestCase,


	members: {

		setUp: function ()
		{
			this.__entity = new efwdDal.entity.Status({});
		},

		"test: getClientId": function ()
		{
			this.assertUndefined(this.__entity.getSource().getId());
			this.__entity.initializeClientId("some-id");
			this.assertIdentical("some-id", this.__entity.getSource().getId());
		},

		"test: initializeServerId, restore": function ()
		{
			this.__entity.initializeServerId("some-id");
			this.__entity.initializeServerId("some-new-id");
			this.__entity.remove();

			var restoreListener = this.stub();
			this.__entity.addListener("restored", restoreListener);
			this.__entity.restore();
			this.assertCalled(restoreListener, "event 'restored' should fired during restoring");
		},

		"test: remove, restore": function ()
		{
			var entity = this.__entity;

			this.assertException(function (){ entity.restore() }, null, null, "new entity can't be restored");

			entity.remove();
			this.assertException(function (){ entity.remove() }, null, null, "entity can't be removed twice");

			entity.restore();
			entity.remove();

			entity.restore();
			this.assertException(function (){ entity.restore() }, null, null, "entity can't be restored twice");
		}

	}
});
