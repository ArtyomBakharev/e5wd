/**
 * @extends {efwdEditor.TestCase}
 */
qx.Class.define("efwdEditor.test.dal.entity.MActivitiesOwner", {

	extend: efwdEditor.TestCase,


	members: {

		setUp: function ()
		{
			this.__entity = new efwdDal.entity.Status({});
		},

		"test: initializeActivities": function ()
		{
			var activities = {a: this.stubClass(efwdDal.EntitiesArray)};
			this.__entity.initializeActivities(activities);
			this.assertIdentical(activities, this.__entity.getActivities());

			this.assertException((function (){ this.__entity.initializeActivities({}) }).bind(this));

			this.__entity.remove();
			this.assertException((function (){ this.__entity.getActivities("a") }).bind(this));
			this.assertCalled(activities.a.dispose);

			this.assertException((function (){ this.__entity.initializeActivities({}) }).bind(this));

			this.__entity.restore();
			activities = {};
			this.__entity.initializeActivities(activities);
			this.assertIdentical(activities, this.__entity.getActivities());
		}

	}
});
