/**
 * @extends {efwdEditor.TestCase}
 */
qx.Class.define("efwdEditor.test.dal.repository.ActionsRepository", {

	extend: efwdEditor.TestCase,

	members: {

		setUp: function ()
		{
			this._ioc.clear();
			this._ioc.registerService(efwdCommon.data.Repository);
			this.__generalRepository = this._ioc.getService(efwdCommon.data.Repository);
			this.__statusesRepository = this._ioc.getService(efwdDal.repository.StatusesRepository);
			this.__context = this._ioc.getService(efwdDal.Context);
			this.__repository = this._ioc.constructService(efwdDal.repository.ActionsRepository);
		},

		"test: getActionsByStatus, getActionsByStatuses": function ()
		{
			this.__setUpEmptyRepository();

			var statuses = [new efwdDal.entity.Status({}), new efwdDal.entity.Status({}), new efwdDal.entity.Status({})];
			var actions = [new efwdDal.entity.Action({}, statuses[0], statuses[1]),
				new efwdDal.entity.Action({}, statuses[1], statuses[2]),
				new efwdDal.entity.Action({}, statuses[2], statuses[0])];

			this.__repository.getStatusChangeActions();
			actions.forEach(function (x){ this.__context.fireDataEvent("entityAttached", {entity: x}) }, this);

			var actionsByStatus = this.__repository.getActionsByStatus(statuses[0]);
			this.assertIdentical(2, actionsByStatus.length);
			this.assertInArray(actions[0], actionsByStatus);
			this.assertInArray(actions[2], actionsByStatus);

			this.assertArrayEquals([actions[1]], this.__repository.getActionsByStatuses(statuses[1], statuses[2]));
			this.assertArrayEquals([], this.__repository.getActionsByStatuses(statuses[2], statuses[1]));
		},

		"test: getActionsByStatus, getActionsByStatuses before entities loads": function ()
		{
			this.__setUpEmptyRepository();
			this.__generalRepository.getActions = function (callback){};

			this.assertArrayEquals([], this.__repository.getActionsByStatus(new efwdDal.entity.Status({})));
			this.assertArrayEquals([], this.__repository.getActionsByStatuses(new efwdDal.entity.Status({}), new efwdDal.entity.Status({})));
		},

		/**
		 * @private
		 */
		__setUpEmptyRepository: function ()
		{
			this.__context.getEntities.returns([]);
			this.__generalRepository.getActions = function (callback){ callback([]) };
			this.__statusesRepository.getStatuses.returns(
				library.Deferred.success(efwdEditor.test.dal.Helper.stubEmptyEntitiesArray(this))
			);
		}
	}
});
