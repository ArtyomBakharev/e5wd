/**
 * @extends {efwdEditor.TestCase}
 */
qx.Class.define("efwdEditor.test.dal.repository.ActivitiesRepository", {

	extend: efwdEditor.TestCase,

	members: {

		setUp: function ()
		{
			this._ioc.clear();
			this._ioc.registerService(efwdCommon.data.Repository);
			this._ioc.registerService(efwdCommon.Communication);

			this.__generalRepository = this._ioc.getService(efwdCommon.data.Repository);
			this.__generalRepository.getProcessId.returns("process 1");

			this.__communication = this._ioc.getService(efwdCommon.Communication);

			this.__statusesRepository = this._ioc.getService(efwdDal.repository.StatusesRepository);
			this.__actionsRepository = this._ioc.getService(efwdDal.repository.ActionsRepository);
			this.__context = this._ioc.getService(efwdDal.Context);
			this.__repository = this._ioc.constructService(efwdDal.repository.ActivitiesRepository);
		},

		"test: getOwnersOfActivity": function ()
		{
			this.__setUpEmptyRepository();

			var activities = [new efwdDal.entity.Activity({}), new efwdDal.entity.Activity({})];
			var status = new efwdDal.entity.Status({});
			var action = new efwdDal.entity.Action({});

			this.__statusesRepository.getStatuses.returns(
				library.Deferred.success(efwdEditor.test.dal.Helper.stubEntitiesArray(this, [status]))
			);
			this.__actionsRepository.getActions.returns(
				library.Deferred.success(efwdEditor.test.dal.Helper.stubEntitiesArray(this, [action]))
			);

			status.initializeActivities({
				one: efwdEditor.test.dal.Helper.stubEntitiesArray(this, [activities[0], activities[1]]),
				two: efwdEditor.test.dal.Helper.stubEntitiesArray(this, [activities[0]])
			});

			action.initializeActivities({
				one: efwdEditor.test.dal.Helper.stubEntitiesArray(this, [activities[0]]),
				two: efwdEditor.test.dal.Helper.stubEntitiesArray(this, [activities[1]])
			});

			this.__repository.getActivities();
			activities.forEach(function (x){ this.__context.fireDataEvent("entityAttached", {entity: x}) }, this);

			this.assertArrayIncludesEqualMaps(
				[
					{owner: status, category: "one"},
					{owner: status, category: "two"},
					{owner: action, category: "one"}
				],
				this.__repository.getOwnersOfActivity(activities[0]));

			this.assertArrayIncludesEqualMaps(
				[
					{owner: status, category: "one"},
					{owner: action, category: "two"}
				],
				this.__repository.getOwnersOfActivity(activities[1]));
		},

		"test: getOwnersOfActivity before entities loads": function ()
		{
			this.__setUpEmptyRepository();
			this.__actionsRepository.getActions = function (){ return new library.Deferred() };

			this.assertArrayEquals([], this.__repository.getOwnersOfActivity(new efwdDal.entity.Activity({})));
		},

		/**
		 * @private
		 */
		__setUpEmptyRepository: function ()
		{
			this.__context.getEntities.returns([]);

			this.__communication.sendQuery = function (query, callback, context)
			{
				callback.call(context, "");
			};

			this._ioc.getService(efwdDal.Mapper).mapToClientObjects.returns([]);

			this.__statusesRepository.getStatuses.returns(
				library.Deferred.success(efwdEditor.test.dal.Helper.stubEmptyEntitiesArray(this))
			);
			this.__actionsRepository.getActions.returns(
				library.Deferred.success(efwdEditor.test.dal.Helper.stubEmptyEntitiesArray(this))
			);
		}
	}
});
