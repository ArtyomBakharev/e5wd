/**
 * @extends {efwdEditor.TestCase}
 */
qx.Class.define("efwdEditor.test.ui.editor.viewmodels.ActivitiesListViewModel", {

	extend: efwdEditor.TestCase,

	members: {

		setUp: function ()
		{
			this._ioc.clear();
			this.__viewModel = this._ioc.constructService(efwdUI.ui.dialog.viewmodels.ActivitiesListViewModel);
			this.__commandsQueue = this._ioc.getService(efwdUI.commands.Queue);
			this.__owner = this.stubClass(efwdDal.entity.Status);
		},

		"test: deleteEntities from owner": function ()
		{
			var category = "some";
			this.__viewModel.initialize(this.__owner, category);
			var removeCommand = this._ioc.nextTransientService(efwdUI.commands.types.RemoveActivities);
			var entitiesArray = this.stubClassArray(efwdDal.entity.Activity, 3);

			this.__viewModel.deleteEntities(entitiesArray);

			this.assertLastCall(removeCommand.initialize, [entitiesArray, this.__owner, category], 1);
		}
	}
});
