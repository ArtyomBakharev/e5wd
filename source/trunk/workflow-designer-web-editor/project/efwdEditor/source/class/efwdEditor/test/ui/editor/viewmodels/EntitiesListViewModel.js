/**
 * @extends {efwdEditor.TestCase}
 */
qx.Class.define("efwdEditor.test.ui.editor.viewmodels.EntitiesListViewModel", {

	extend: efwdEditor.TestCase,

	members: {

		setUp: function ()
		{
			this._ioc.clear();
			var viewModelClass = library.dev.Unit.wrapClass(efwdUI.ui.dialog.viewmodels.EntitiesListViewModel);
			this.__commandsQueue = this._ioc.getService(efwdUI.commands.Queue);
			this.__viewModel = new viewModelClass(this._ioc.getService(efwdUI.behavior.EntityEditionModel), this._ioc, this.__commandsQueue);
			this.__owner = this.stubClass(efwdDal.entity.Status);
		},

		"test: deleteEntities": function ()
		{
			this.__viewModel.initialize();
			var deleteCommand = this._ioc.nextTransientService(efwdUI.commands.types.DeleteEntities);
			var entitiesArray = this.stubClassArray(efwdDal.entity.Activity, 3);
			this.__viewModel.deleteEntities(entitiesArray);

			this.assertLastCall(deleteCommand.initialize, [entitiesArray], 1);
			this.assertLastCall(this.__commandsQueue.addCommand, [deleteCommand], 1);
		},

		"test: deleteEntities from owner": function ()
		{
			this.__viewModel.initialize(this.__owner);
			var removeCommand = this.stubClass(efwdUI.commands.types.RemoveActivities);
			var entitiesArray = this.stubClassArray(efwdDal.entity.Activity, 3);

			this.__viewModel._createRemoveCommand = this.stub().returns(removeCommand);

			this.__viewModel.deleteEntities(entitiesArray);

			this.assertLastCall(this.__viewModel._createRemoveCommand, [entitiesArray], 1);
			this.assertLastCall(this.__commandsQueue.addCommand, [removeCommand], 1);
		}
	}
});
