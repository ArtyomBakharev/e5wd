/**
 * @extends {efwdEditor.TestCase}
 */
qx.Class.define("efwdEditor.test.ui.entityediting.EntityEditor", {

	extend: efwdEditor.TestCase,

	members: {

		setUp: function ()
		{
			this._ioc.clear();

			this.__statusForm = [
				{name: "field1"},
				{name: "field2"}
			];
			this.__oldConfig = efwdUI.ui.entityediting.viewModel.EntityEditor.FORMS_STRUCTURE_CONFIG;
			efwdUI.ui.entityediting.viewModel.EntityEditor.FORMS_STRUCTURE_CONFIG = {
				header: [],
				parts: {},
				byTypes: {
					status: this.__statusForm
				}
			};

			this.__createViewModel();
		},

		tearDown: function ()
		{
			efwdUI.ui.entityediting.viewModel.EntityEditor.FORMS_STRUCTURE_CONFIG = this.__oldConfig;
			efwdUI.ui.entityediting.viewModel.EntityEditor.clearCache();
		},

		"test: getFormStructure": function ()
		{
			var formsStructure = this.__viewModel.getFormStructure();
			this.assertArrayEquals(this.__statusForm, formsStructure);

			this.__createViewModel();
			this.assertIdentical(formsStructure, this.__viewModel.getFormStructure(), "Caching");

			this.__createViewModel(false);
			this.__entity.getSource().$type = "otherSubType";
			this.__viewModel.initialize(this.__entity);
			this.assertNotIdentical(formsStructure, this.__viewModel.getFormStructure());
		},

		"test: entityUpdatedOutside firing": function ()
		{
			var listener = this.stub();
			this.__viewModel.addListener("entityUpdatedOutside", listener);

			this.__entity.update();
			this.assertCallCount(listener, 1);

			this.__viewModel.updateEntity();
			this.__viewModel.updateEntity(true);
			this.assertCallCount(listener, 1);

			this.__entity.update();
			this.assertCallCount(listener, 2);
		},

		"test: updateEntity": function ()
		{
			this._ioc.setServiceConfigurator(efwdUI.commands.types.EditEntity, function (command)
			{
				command.initialize = function (entity, oldSource, keys)
				{
					command.initialize.called = true;
					command.initialize.oldSource = Object.deepCopy(oldSource);
					command.initialize.keys = keys;
				};
			});

			this.__createViewModel();
			this.__entity.getSource().flag = 1;
			this.__viewModel.updateEntity();
			this.__viewModel.updateEntity(true);

			var command = this.__getLastAddedCommand();
			this.assert(command.initialize.called);
			this.assertIdentical(0, command.initialize.oldSource.flag);
			this.assertArrayEquals(["field1", "field2"], command.initialize.keys);

			this.__viewModel.updateEntity(true);
			var secondCommand = this.__getLastAddedCommand();
			this.assertNotIdentical(command, secondCommand);
			this.assertCalledTwice(this.__commandsQueue.addCommand);

			//after command updating
			this.__entity.getSource().flag = 3;
			this.__entity.update();
			this.__viewModel.updateEntity(true);
			command = this.__getLastAddedCommand();
			this.assert(command.initialize.called);
			this.assertIdentical(3, command.initialize.oldSource.flag);
		},

		/**
		 * @param {Boolean} [initialize=true]
		 * @private
		 */
		__createViewModel: function (initialize)
		{
			this.__viewModel = this._ioc.constructService(efwdUI.ui.entityediting.viewModel.EntityEditor);
			this.__commandsQueue = this._ioc.getService(efwdUI.commands.Queue);
			this.__entity = new efwdDal.entity.Status({id: "1", flag: 0});
			if (initialize === undefined || initialize)
			{
				this.__viewModel.initialize(this.__entity);
			}
		},

		/**
		 * @private
		 */
		__getLastAddedCommand: function ()
		{
			return this.__commandsQueue.addCommand.args.last()[0];
		}
	}
});
