/**
 * @extends {efwdEditor.TestCase}
 */
qx.Class.define("efwdEditor.test.ui.settingspanel.editor.EntityEditor", {

	extend: efwdEditor.TestCase,

	members: {

		setUp: function ()
		{
			this._ioc.clear();
			this.__viewModel = this._ioc.getService(efwdUI.ui.entityediting.viewModel.EntityEditor);
			this.__entity = new efwdDal.entity.Status({id: "1", flag: 0});
			this.__viewModel.getEntity.returns(this.__entity);

			this.__entityEditor = this._ioc.constructService(efwdUI.ui.settingspanel.editor.EntityEditor);
			this.__entityEditor.initialize(this.__viewModel);
		},

		"test: close": function ()
		{
			var entityForm = this._ioc.nextTransientService(efwdUI.ui.entityediting.view.EntityForm);
			this.__entityEditor.render();
			this.__entityEditor.close();
			this.assertCalledOnce(entityForm.dispose);
		}
	}
});
