qx.Theme.define("efwdEditor.theme.Appearance", {

	extend: efwdUI.theme.Appearance,

	appearances: {

		"top-toolbar": {
			include: "toolbar"
		},

		"top-toolbar/autosaveIndicator": {
			include: "label",
			alias: "label",
			style: function (state, style)
			{
				var font = qx.bom.Font.fromConfig(qx.theme.manager.Font.getInstance().getTheme().fonts["default"]).set({
					italic: true
				});

				return {
					margin: 16,
					font: font,
					textColor: "text-disabled"
				}
			}
		},

		"top-toolbar/readOnlyIndicator": {
			include: "top-toolbar/autosaveIndicator",
			style: function (state, style)
			{
				return {
					font: style.font.set({bold: true}),
					textColor: "#F5A700"
				}
			}
		}
	}
});