qx.Theme.define("efwdEditor.theme.Theme",
	{
		meta: {
			color: efwdEditor.theme.Color,
			decoration: efwdEditor.theme.Decoration,
			font: efwdEditor.theme.Font,
			icon: qx.theme.icon.Oxygen,
			appearance: efwdEditor.theme.Appearance
		}
	});