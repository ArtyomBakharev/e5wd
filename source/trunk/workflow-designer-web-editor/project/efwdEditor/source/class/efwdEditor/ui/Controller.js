/**
 * Главный контроллер графического приложения. Отрисовывает первоначальный layout.
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdEditor.ui.Controller", {

	extend: qx.core.Object,

	/**
	 * @param {efwdDal.repository.ProcessesRepository} processesRepository
	 * @param {efwdEditor.ui.views.Layout} layout
	 * @param {efwdUI.ui.settingspanel.Controller} settingsPanelController
	 * @param {efwdEditor.canvas.Controller} canvasController
	 * @param {efwdEditor.behavior.manipulationtool.Model} manipulationToolModel
	 * @param {efwdUI.commands.Queue} commandsQueue
	 * @param {efwdEditor.ui.DialogsController} dialogsController
	 * @param {efwdEditor.Application} application
	 * @param {efwdEditor.commands.Controller} commandsController
	 * @param {efwdEditor.behavior.GeneralCommandsProvider} generalCommandsProvider
	 * @param {efwdEditor.canvas.views.Canvas} canvas
	 * @param {efwdEditor.canvas.CommandsProvider} canvasCommandsProvider
	 */
	construct: function (processesRepository, layout, settingsPanelController, canvasController, manipulationToolModel,
	                     commandsQueue, dialogsController, application, commandsController, generalCommandsProvider,
	                     canvas, canvasCommandsProvider)
	{
		this.__processesRepository = processesRepository;
		this.__layout = layout;
		this.__settingsPanelController = settingsPanelController;
		this.__canvasController = canvasController;
		this.__manipulationToolModel = manipulationToolModel;
		this.__commandsQueue = commandsQueue;
		this.__dialogsController = dialogsController;
		this.__application = application;
		this.__commandsController = commandsController;
		this.__generalCommandsProvider = generalCommandsProvider;
		this.__canvas = canvas;
		this.__canvasCommandsProvider = canvasCommandsProvider;
	},

	members: {
		/**
		 * Отрисовка всех основных видов
		 */
		initialize: function ()
		{
			this.__settingsPanelController.initialize();
			this.__canvasController.initialize();
			this.__dialogsController.initialize();
			this.__commandsController.initialize();

			this.__setUpCommandNotifying();

			this.__layout.initialize();
			this.__layout.render(this.__canvasController.getCanvasWidget());

			this.__registerCommands();

			library.ui.management.ZonesManager.getInstance().focusZone(this.__canvas.getZone());
		},

		/**
		 * @private
		 */
		__registerCommands: function ()
		{
			var manager = library.ui.management.ShortcutsManager.getInstance();
			var rootZone = library.ui.management.ZonesManager.getInstance().getRootZone();
			var canvasZone = this.__canvas.getZone();

			manager.registerCommand("defaultTool", function (){ this.__manipulationToolModel.resetTool() }, this,
				rootZone);
			manager.registerCommand("undo", function (){ this.__commandsQueue.undo() }, this, rootZone);
			manager.registerCommand("redo", function (){ this.__commandsQueue.redo() }, this, rootZone);

			manager.registerCommand("copy", this.__canvasCommandsProvider.getCopyCommand(),
				null, canvasZone);
			manager.registerCommand("paste", this.__canvasCommandsProvider.getPasteCommand(),
				null, canvasZone);
			manager.registerCommand("selectAllElements", this.__canvasCommandsProvider.getSelectAllElementsCommand(),
				null, canvasZone);
			manager.registerCommand("deselectAllElements",
				this.__canvasCommandsProvider.getDeselectAllElementsCommand(), null, canvasZone);
			manager.registerCommand("deleteElement", this.__canvasCommandsProvider.getDeleteElementsCommand(),
				null, canvasZone);
			manager.registerCommand("addAction", this.__generalCommandsProvider.getAddActionCommand(),
				null, canvasZone);
			manager.registerCommand("addStatus", this.__generalCommandsProvider.getAddStatusCommand(),
				null, canvasZone);
		},

		/**
		 * @private
		 */
		__setUpCommandNotifying: function ()
		{
			this.__commandsQueue.addListener("notifyUser", function (e)
			{
				var data = e.getData();
				var message = data.command.getTitle() + " " + (data.redo ? "repeated" : "canceled");
				this.__application.getMessagesQueue().addMessage(library.ui.messagesqueue.Message.createInfoMessage(message));
			}, this);
		}
	},

	defer: function (statics, members, properties)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}

});
