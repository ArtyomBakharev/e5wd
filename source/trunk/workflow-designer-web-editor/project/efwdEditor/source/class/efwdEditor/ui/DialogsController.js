/**
 * Контроллер, отвечающий за открытие/закрытие диалоговых окон при изменении модели {@link efwdEditor.behavior.DialogsModel}
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdEditor.ui.DialogsController", {

	extend: qx.core.Object,

	/**
	 * @param {efwdEditor.behavior.DialogsModel} dialogsModel
	 * @param {library.IServiceLocator} serviceLocator
	 * @param {efwdDal.entity.Process} process
	 */
	construct: function (dialogsModel, serviceLocator, process)
	{
		this.__dialogsModel = dialogsModel;
		this.__serviceLocator = serviceLocator;
		this.__process = process;

		this.__activitiesLists = {};
	},

	members: {

		initialize: function ()
		{
			this.__dialogsModel.addListener("changeGlobalActionsListDialog", function (e)
			{
				if (e.getData())
				{
					this.__openGlobalActionsListDialog();
				}
				else
				{
					this.__closeGlobalActionsListDialog();
				}
			}, this);

			this.__dialogsModel.addListener("changeGlobalActivitiesListDialog", function (e)
			{
				if (e.getData())
				{
					this.__openActivitiesListDialog();
				}
				else
				{
					this.__closeActivitiesListDialog();
				}
			}, this);

			this.__dialogsModel.addListener("changeProcessPropertiesDialog", function (e)
			{
				if (e.getData())
				{
					this.__openProcessPropertiesDialog();
				}
				else
				{
					this.__closeProcessPropertiesDialog();
				}
			}, this);

			this.__dialogsModel.addListener("openActivitiesListDialog", function (e)
			{
				this.__openActivitiesListDialog(e.getData().entity);
			}, this);

			this.__dialogsModel.addListener("closeActivitiesListDialog", function (e)
			{
				this.__closeActivitiesListDialog(e.getData().entity);
			}, this);
		},

		/**
		 * @param {efwdDal.entity.AbstractEntity} [entity=null]
		 * @private
		 */
		__closeActivitiesListDialog: function (entity)
		{
			var key = entity ? Object.getUniqueId(entity) : 0;
			if (this.__activitiesLists[key])
			{
				this.__activitiesLists[key].close();
				delete this.__activitiesLists[key];
			}
		},

		/**
		 * @private
		 */
		__closeGlobalActionsListDialog: function ()
		{
			if (this.__globalActionsList)
			{
				this.__globalActionsList.close();
				delete this.__globalActionsList;
			}
		},

		/**
		 * @private
		 */
		__closeProcessPropertiesDialog: function ()
		{
			if (this.__processPropertiesDialog)
			{
				this.__processPropertiesDialog.close();
				delete this.__processPropertiesDialog;
			}
		},

		/**
		 * @param {efwdDal.entity.AbstractEntity} [entity=null]
		 * @private
		 */
		__openActivitiesListDialog: function (entity)
		{
			var list;
			var viewModel;
			var key = entity ? Object.getUniqueId(entity) : 0;

			if (entity)
			{
				viewModel = this.__serviceLocator.getService(efwdUI.ui.dialog.viewmodels.ActivitiesListsViewModel);
				viewModel.initialize(entity);
				list = this.__serviceLocator.getService(efwdUI.ui.dialog.ActivitiesDialog);
				list.initialize(viewModel);
			}
			else
			{
				viewModel = this.__serviceLocator.getService(efwdUI.ui.dialog.viewmodels.ActivitiesListViewModel);
				viewModel.initialize();
				list = this.__serviceLocator.getService(efwdUI.ui.dialog.AllActivitiesDialog);
				list.initialize(viewModel);
			}

			this.__activitiesLists[key] = list;

			list.addListener("destroyed", function ()
			{
				delete this.__activitiesLists[key];
				viewModel.dispose();

				//делаем изменения в модели, на тот случай если диалог был закрыт напрямую
				if (entity)
				{
					this.__dialogsModel.closeActivitiesListDialog(entity);
				}
				else
				{
					this.__dialogsModel.setGlobalActivitiesListDialog(false);
				}

			}, this);

			list.render();
		},

		/**
		 * @private
		 */
		__openGlobalActionsListDialog: function ()
		{
			var actionsListViewModel = this.__serviceLocator.getService(efwdUI.ui.dialog.viewmodels.ActionsListViewModel);
			actionsListViewModel.initialize("edit", "no_status_action");
			this.__globalActionsList = this.__serviceLocator.getService(efwdUI.ui.dialog.ActionsListDialog);
			this.__globalActionsList.initialize(actionsListViewModel);

			this.__globalActionsList.addListener("destroyed", function ()
			{
				this.__globalActionsList = null;
				actionsListViewModel.dispose();
				this.__dialogsModel.setGlobalActionsListDialog(false);
			}, this);

			this.__globalActionsList.render();
		},

		/**
		 * @private
		 */
		__openProcessPropertiesDialog: function ()
		{
			var viewModel = this.__serviceLocator.getService(efwdUI.ui.entityediting.viewModel.EntityEditor);
			viewModel.initialize(this.__process);

			this.__processPropertiesDialog = this.__serviceLocator.getService(efwdUI.ui.dialog.ProcessDialog);
			this.__processPropertiesDialog.initialize(viewModel);

			this.__processPropertiesDialog.addListener("destroyed", function ()
			{
				this.__processPropertiesDialog = null;
				this.__dialogsModel.setProcessPropertiesDialog(false);
			}, this);

			this.__processPropertiesDialog.render();
		}
	},

	defer: function (statics, members, properties)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}

});
