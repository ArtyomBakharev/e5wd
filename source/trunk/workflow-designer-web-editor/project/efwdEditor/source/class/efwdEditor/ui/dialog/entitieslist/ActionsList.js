/**
 * Список действий с возвожностью открыть список активностей для каждого действия
 * @extends {efwdUI.ui.dialog.entitieslist.ActionsList}
 */
qx.Class.define("efwdEditor.ui.dialog.entitieslist.ActionsList", {

	extend: efwdUI.ui.dialog.entitieslist.ActionsList,

	/**
	 * @param {efwdEditor.behavior.DialogsModel} dialogsModel
	 */
	construct: function (dialogsModel)
	{
		this.base(arguments);
		this.__dialogsModel = dialogsModel;
	},

	members: {

		/**
		 * @param {efwdUI.ui.dialog.viewmodels.ActionsListViewModel} viewModel
		 */
		initialize: function (viewModel)
		{
			var $this = this;
			this.base(arguments, viewModel, {
				buttons: [
					{
						name: "activities",
						title: "activities",
						execute: function (entity)
						{
							$this.__dialogsModel.toggleActivitiesListDialog(entity);
						}
					}
				]
			});
		}
	},

	defer: function (statics, members, properties)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}

});
