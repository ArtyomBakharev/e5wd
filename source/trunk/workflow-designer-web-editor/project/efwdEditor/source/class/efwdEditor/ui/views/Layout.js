/*
 #ignore(efwdConfig)
 #ignore(efwdConfig.*)
 */

/**
 * Вид определяющий основной макет приложения и отрисовывающий основные его элементы.
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdEditor.ui.views.Layout", {

	extend: qx.core.Object,

	/**
	 * @param {efwdEditor.ui.views.Toolbar} toolbar
	 * @param {efwdUI.ui.views.EditorContainer} settingsPanel
	 * @param {efwdEditor.ui.views.Palette} palette
	 * @param {efwdDal.entity.Process} process
	 */
	construct: function (toolbar, settingsPanel, palette, process)
	{
		this.__toolbar = toolbar;
		this.__settingsPanel = settingsPanel;
		this.__palette = palette;
		this.__process = process;
	},

	members: {

		initialize: function ()
		{
			this.__setUpDocumentTitle();
		},

		/**
		 * отрисовка макета приложения
		 * @param {qx.ui.core.Widget} canvasWidget
		 */
		render: function (canvasWidget)
		{
			this.__canvasWidget = canvasWidget;

			var root = qx.core.Init.getApplication().getRoot();
			var container = new qx.ui.container.Composite(new qx.ui.layout.VBox());

			this.__toolbar.initialize();
			container.add(this.__toolbar);
			container.add(this.__createBody(), {flex: 1});

			root.add(container, {edge: 0});
		},

		/**
		 * @return {qx.ui.core.Widget}
		 * @private
		 */
		__createBody: function ()
		{
			var pane = new qx.ui.splitpane.Pane("horizontal");

			pane.add(this.__createLeftPane(), 0);
			pane.add(this.__canvasWidget, 1);

			return pane;
		},


		/**
		 * @private
		 */
		__createLeftPane: function ()
		{
			var scrollContainer = new qx.ui.container.Scroll();
			scrollContainer.set({
				scrollbarX: "off",
				width: efwdConfig.efwdEditor.ui.$style.layout.leftPanelWidth
			});

			var leftContent = new qx.ui.container.Composite(new qx.ui.layout.VBox());
//			leftContent.setWidth(efwdConfig.efwdEditor.ui.$style.layout.leftPanelWidth);
			leftContent.add(this.__palette.render());
			leftContent.add(this.__settingsPanel.render(), {flex: 1});
			scrollContainer.add(leftContent);

			return scrollContainer;
		},

		/**
		 * @private
		 */
		__setUpDocumentTitle: function ()
		{
			this.__documentTitle = document.title;
			this.__process.getSource().addListener("selfChanged", this.__updateDocumentTitle, this);
			this.__updateDocumentTitle();
		},

		/**
		 * @private
		 */
		__updateDocumentTitle: function ()
		{
			document.title = this.__process.getSource().getName() + " - " + this.__documentTitle;
		}
	}
});
