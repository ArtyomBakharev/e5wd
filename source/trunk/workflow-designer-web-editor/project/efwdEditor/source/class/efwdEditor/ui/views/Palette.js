/**
 * Вид палитры элементов
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdEditor.ui.views.Palette", {

	extend: qx.core.Object,

	/**
	 * @param {efwdEditor.behavior.manipulationtool.Model} model
	 * @param {efwdUI.behavior.PageModel} pageModel
	 */
	construct: function (model, pageModel)
	{
		this.__model = model;
		this.__pageModel = pageModel;
	},

	members: {

		/**
		 * @return {qx.ui.core.Widget}
		 */
		render: function ()
		{
			this.__paletteWidget = new qx.ui.groupbox.GroupBox("Add");
			this.__paletteWidget.setLayout(new qx.ui.layout.Grow());
			//this.__paletteWidget.setHeight(100);

			this.__pageModel.bind("readOnly", this.__paletteWidget, "visibility", {converter: function (readOnly)
			{
				return readOnly ? "excluded" : "visible"
			}});

			this.__addButtons(this.__paletteWidget);
			this.__setUpToolSelection();
			return this.__paletteWidget;
		},

		/**
		 * @private
		 */
		__addButtons: function (container)
		{
			this.__buttonsGroup = new qx.ui.form.RadioButtonGroup(new qx.ui.layout.Flow(5, 5));
			var buttonsData = [
				{
					title: "no",
					tool: efwdEditor.behavior.manipulationtool.DefaultTool
				},
				{
					title: "Status",
					tool: efwdEditor.behavior.manipulationtool.StatusTool,
					icon: efwdUI.ui.IconsStore.get("status", 32)
				},
				{
					title: "Action",
					tool: efwdEditor.behavior.manipulationtool.ActionTool,
					icon: efwdUI.ui.IconsStore.get("action", 32)
				}
			];

			buttonsData.forEach(this.__addButton.bind(this, this.__buttonsGroup));

			container.add(this.__buttonsGroup);
		},

		/**
		 * @private
		 */
		__addButton: function (group, data)
		{
			var button = new qx.ui.form.ToggleButton(data.title, data.icon);
			button.setUserData("tool", data.tool);

			if (data.tool == efwdEditor.behavior.manipulationtool.DefaultTool)
			{
				button.setVisibility("excluded");
			}

			group.add(button);
		},

		/**
		 * @private
		 */
		__setUpToolSelection: function ()
		{
			this.__buttonsGroup.bind("selection", this.__model, "tool", {converter: (function (x)
			{
				var toolClass = x.length ? x[0].getUserData("tool") : efwdEditor.behavior.manipulationtool.DefaultTool;
				if (this.__model.toolIs(toolClass))
				{
					return this.__model.getTool();
				}
				return new toolClass();
			}).bind(this)});

			this.__model.bind("tool", this.__buttonsGroup, "selection", {converter: (function (tool)
			{

				var selection = [];
				this.__buttonsGroup.getChildren().forEach(function (button)
				{
					var toolClass = button.getUserData("tool");
					if (tool && tool instanceof toolClass)
					{
						selection.push(button);
					}
				});
				return selection;

			}).bind(this)});
		}
	}
});
