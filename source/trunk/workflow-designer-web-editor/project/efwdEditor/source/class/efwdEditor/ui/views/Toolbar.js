/*
 #ignore(efwdConfig.efwdEditor)
 #ignore(efwdConfig.efwdEditor.*)
 */

/**
 * Вид для панели инструментов редактора
 * @extends {qx.ui.toolbar.ToolBar}
 */
qx.Class.define("efwdEditor.ui.views.Toolbar", {

	extend: qx.ui.toolbar.ToolBar,

	properties: {
		appearance: {
			refine: true,
			init: "top-toolbar"
		}
	},

	/**
	 * @param {efwdDal.saving.AbstractSaver} saver
	 * @param {efwdEditor.behavior.GeneralCommandsProvider} generalCommandsProvider
	 * @param {efwdUI.commands.Queue} commandsQueue
	 * @param {efwdEditor.behavior.DialogsModel} dialogsModel
	 * @param {efwdUI.behavior.PageModel} pageModel
	 * @param {efwdEditor.canvas.CommandsProvider} canvasCommandsProvider
	 */
	construct: function (saver, generalCommandsProvider, commandsQueue, dialogsModel, pageModel,
	                     canvasCommandsProvider)
	{
		this.__saver = saver;
		this.__generalCommandsProvider = generalCommandsProvider;
		this.__commandsQueue = commandsQueue;
		this.__dialogsModel = dialogsModel;
		this.__pageModel = pageModel;
		this.__canvasCommandsProvider = canvasCommandsProvider;

		this.base(arguments);
	},

	members: {

		initialize: function ()
		{
			this.__addButton(this.__generalCommandsProvider.getBackToProcessesListCommand());
			this.addSeparator();

			if (!this.__pageModel.isReadOnly())
			{
				this.__createUndoRedoButtons();
				this.addSeparator();
			}

			this.__addToggleButton("globalActionsListDialog", "Actions list",
				efwdUI.ui.IconsStore.get("action", 32));
			this.__addToggleButton("globalActivitiesListDialog", "All activities",
				efwdUI.ui.IconsStore.get("activity", 32));
			this.__addToggleButton("processPropertiesDialog", "Process properties",
				efwdUI.ui.IconsStore.get("processProperties", 32));

			if (!this.__pageModel.isReadOnly())
			{
				this.addSeparator();
				this.__addButton(this.__canvasCommandsProvider.getSelectAllElementsCommand());
				this.__addButton(this.__canvasCommandsProvider.getCopyCommand());
				this.__addButton(this.__canvasCommandsProvider.getPasteCommand());
				this.__addButton(this.__canvasCommandsProvider.getDeleteElementsCommand());
			}

			this.addSpacer();
			this._createChildControl("readOnlyIndicator");
			this._createChildControl("autosaveIndicator");
		},

		/**
		 * @protected
		 */
		_createChildControlImpl: function (id, hash)
		{
			var child;
			switch (id)
			{
				case "autosaveIndicator":
					child = this.__createAutosaveIndicator();
					this.add(child);
					break;

				case "readOnlyIndicator":
					child = this.__createReadOnlyIndicator();
					this.add(child);
					break;
			}

			return child || this.base(arguments, id, hash);
		},

		/**
		 * @param {qx.ui.core.Command} command
		 * @param {String} [icon=null]
		 * @private
		 */
		__addButton: function (command, icon)
		{
			var button = new qx.ui.toolbar.Button(null, icon);
			button.setIconSize(32);
			button.setCommand(command);
			this.add(button);
		},

		/**
		 * @param {String} property свойство {@link efwdEditor.behavior.DialogsModel}
		 * @param {String} title
		 * @param {String} [icon=null]
		 * @private
		 */
		__addToggleButton: function (property, title, icon)
		{
			var button = new qx.ui.toolbar.CheckBox(title, icon);
			this.__dialogsModel.bind(property, button, "value");
			button.bind("value", this.__dialogsModel, property);
			this.add(button);
		},

		/**
		 * @param {qx.ui.menu.Menu} menu
		 * @param {Boolean} undo
		 * @private
		 */
		__connectCancellingMenuToArray: function (menu, undo)
		{
			new library.ui.WidgetChildrenModelConnector(
				menu,

				(function (command)
				{
					var menuButton = new qx.ui.menu.Button(command.getTitle());
					menuButton.addListener("execute", function ()
					{
						if (undo)
						{
							this.__commandsQueue.undoTo(command);
						}
						else
						{
							this.__commandsQueue.redoTo(command);
						}
					}, this);

					return menuButton;

				}).bind(this),

				undo ? this.__commandsQueue.getUndoArray() : this.__commandsQueue.getRedoArray()
			);
		},

		/**
		 * @private
		 */
		__createAutosaveIndicator: function ()
		{
			var indicator = new qx.ui.basic.Label();

			var lastSave;
			var dateFormat = new qx.util.format.DateFormat(efwdConfig.efwdEditor.$timeFormat);
			this.__saver.addListener("savingStart", function ()
			{
				indicator.setValue("Saving...");
			});

			this.__saver.addListener("savingComplete", function (e)
			{
				indicator.setValue("Saved (" + dateFormat.format(e.getData().revisionTime) + ")");
			});

			return indicator;
		},

		/**
		 * @param {Boolean} undo
		 * @private
		 */
		__createCancellingButton: function (undo)
		{
			var menu = new qx.ui.menu.Menu();
			this.__connectCancellingMenuToArray(menu, undo);

			var button = new qx.ui.toolbar.SplitButton(
				undo ? "Undo" : "Redo",
				undo ? "icon/32/actions/edit-undo.png" : "icon/32/actions/edit-redo.png",
				menu
			);

			button.addListener("execute", function ()
			{
				undo ? this.__commandsQueue.undo() : this.__commandsQueue.redo()
			}, this);

			var array = undo ? this.__commandsQueue.getUndoArray() : this.__commandsQueue.getRedoArray();
			array.bind("change", button, "enabled", {converter: function (){ return array.getLength() > 0 }});
			button.setEnabled(false);

			this.add(button);
		},

		/**
		 * @private
		 */
		__createReadOnlyIndicator: function ()
		{
			var indicator = new qx.ui.basic.Label();
			this.__pageModel.bind("readOnly", indicator, "value", {converter: function (readOnly)
			{
				return readOnly ? "Read only mode!" : ""
			}});

			return indicator;
		},

		/**
		 * @private
		 */
		__createUndoRedoButtons: function ()
		{
			this.__createCancellingButton(true);
			this.__createCancellingButton(false);
		}
	}
});
