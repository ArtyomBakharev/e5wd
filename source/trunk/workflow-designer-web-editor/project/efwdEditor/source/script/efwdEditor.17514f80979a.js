/* ************************************************************************

   qooxdoo - the new era of web development

   http://qooxdoo.org

   Copyright:
     2004-2009 1&1 Internet AG, Germany, http://www.1und1.de

   License:
     LGPL: http://www.gnu.org/licenses/lgpl.html
     EPL: http://www.eclipse.org/org/documents/epl-v10.php
     See the LICENSE file in the project's top-level directory for details.

   Authors:
     * Martin Wittemann (martinwittemann)

************************************************************************ */

/**
 * The radio container handles a collection of items from which only one item
 * can be selected. Selection another item will deselect the previously selected
 * item. For that, it uses the {@link qx.ui.form.RadioGroup} object.
 *
 * This class is used to create radio groups of {@link qx.ui.form.RadioButton}
 * instances.
 *
 * This widget takes care of the layout of the added items. If you want to
 * take full control of the layout and just use the selection behavior,
 * take a look at the {@link qx.ui.form.RadioGroup} object for a loose coupling.
 * @extends {qx.ui.core.Widget}
 */
qx.Class.define("qx.ui.form.RadioButtonGroup",
{
  extend : qx.ui.core.Widget,
  include : [qx.ui.core.MLayoutHandling, qx.ui.form.MModelSelection],
  implement : [
    qx.ui.form.IForm,
    qx.ui.core.ISingleSelection,
    qx.ui.form.IModelSelection
  ],


  /**
   * @param layout {qx.ui.layout.Abstract} The new layout or
   *     <code>null</code> to reset the layout.
   */
  construct : function(layout)
  {
    this.base(arguments);

    // if no layout is given, use the default layout (VBox)
    if (layout == null) {
      this.setLayout(new qx.ui.layout.VBox(4));
    } else {
      this.setLayout(layout);
    }

    // create the radio group
    this.__radioGroup = new qx.ui.form.RadioGroup();

    // attach the listener
    this.__radioGroup.addListener("changeSelection", function(e) {
      this.fireDataEvent("changeSelection", e.getData(), e.getOldData());
    }, this);
  },


  properties :
  {
    /**
     * Flag signaling if the group at all is valid. All children will have the
     * same state.
     */
    valid : {
      check : "Boolean",
      init : true,
      apply : "_applyValid",
      event : "changeValid"
    },

    /**
     * Flag signaling if the group is required.
     */
    required : {
      check : "Boolean",
      init : false,
      event : "changeRequired"
    },

    /**
     * Message which is shown in an invalid tooltip.
     */
    invalidMessage : {
      check : "String",
      init: "",
      event : "changeInvalidMessage",
      apply : "_applyInvalidMessage"
    },


    /**
     * Message which is shown in an invalid tooltip if the {@link #required} is
     * set to true.
     */
    requiredInvalidMessage : {
      check : "String",
      nullable : true,
      event : "changeInvalidMessage"
    }
  },


  events :
  {
    /**
     * Fires after the selection was modified
     */
    "changeSelection" : "qx.event.type.Data"
  },


  members :
  {
    __radioGroup : null,


    /*
    ---------------------------------------------------------------------------
      APPLY ROUTINES
    ---------------------------------------------------------------------------
    */
    // property apply
    /**
     * @protected
     */
    _applyInvalidMessage : function(value, old) {
      var children = this._getChildren();
      for (var i = 0; i < children.length; i++) {
        children[i].setInvalidMessage(value);
      }
    },


    // property apply
    /**
     * @protected
     */
    _applyValid: function(value, old) {
      var children = this._getChildren();
      for (var i = 0; i < children.length; i++) {
        children[i].setValid(value);
      }
    },


    /*
    ---------------------------------------------------------------------------
      REGISTRY
    ---------------------------------------------------------------------------
    */

    /**
     * The internaly used radio group instance will be returned.
     *
     * @return {qx.ui.form.RadioGroup} Returns the used RadioGroup instance.
     */
    getRadioGroup : function() {
      return this.__radioGroup;
    },


    /**
     * Returns the children list
     *
     * @return {qx.ui.core.LayoutItem[]} The children array.
     */
    getChildren : function() {
      return this._getChildren();
    },


    /**
     * Adds a new child widget.
     *
     * The supported keys of the layout options map depend on the layout
     * used to position the widget. The options are documented in the class
     * documentation of each layout manager {@link qx.ui.layout}.
     *
     * @param child {qx.ui.core.LayoutItem} the widget to add.
     * @param [options=null] {Map} Optional layout data for widget.
     */
    add : function(child, options) {
      this.__radioGroup.add(child);
      this._add(child, options);
    },


    /**
     * Remove the given child widget.
     *
     * @param child {qx.ui.core.LayoutItem} the widget to remove
     */
    remove : function(child)
    {
      this.__radioGroup.remove(child);
      this._remove(child);
    },


    /**
     * Remove all children.
     *
     * @return {Array} An array of {@link qx.ui.core.LayoutItem}'s.
     */
    removeAll : function()
    {
      // remove all children from the radio group
      var radioItems = this.__radioGroup.getItems();
      for (var i = 0; i < radioItems.length; i++) {
        this.__radioGroup.remove(radioItems[i]);
      }

      return this._removeAll();
    },



    /*
    ---------------------------------------------------------------------------
      SELECTION
    ---------------------------------------------------------------------------
    */

    /**
     * Returns an array of currently selected items.
     *
     * Note: The result is only a set of selected items, so the order can
     * differ from the sequence in which the items were added.
     *
     * @return {qx.ui.core.Widget[]} List of items.
     */
    getSelection : function() {
      return this.__radioGroup.getSelection();
    },


    /**
     * Replaces current selection with the given items.
     *
     * @param items {qx.ui.core.Widget[]} Items to select.
     * @throws an exception if the item is not a child element.
     */
    setSelection : function(items) {
      return this.__radioGroup.setSelection(items);
    },


    /**
     * Clears the whole selection at once.
     */
    resetSelection : function() {
      return this.__radioGroup.resetSelection();
    },


    /**
     * Detects whether the given item is currently selected.
     *
     * @param item {qx.ui.core.Widget} Any valid selectable item
     * @return {Boolean} Whether the item is selected.
     * @throws an exception if the item is not a child element.
     */
    isSelected : function(item) {
      return this.__radioGroup.isSelected(item);
    },


    /**
     * Whether the selection is empty.
     *
     * @return {Boolean} Whether the selection is empty.
     */
    isSelectionEmpty : function() {
      return this.__radioGroup.isSelectionEmpty();
    },


    /**
     * Returns all elements which are selectable.
     *
     * @param all {boolean} true for all selectables, false for the
     *   selectables the user can interactively select
     * @return {qx.ui.core.Widget[]} The contained items.
     */
    getSelectables: function(all) {
      return this.__radioGroup.getSelectables(all);
    }
  },


  destruct : function()
  {
    this._disposeObjects("__radioGroup");
  }
});
/* ************************************************************************

   qooxdoo - the new era of web development
   http://qooxdoo.org

   Copyright:
     2008 Dihedrals.com, http://www.dihedrals.com

   License:
     LGPL: http://www.gnu.org/licenses/lgpl.html
     EPL: http://www.eclipse.org/org/documents/epl-v10.php
     See the LICENSE file in the project's top-level directory for details.

   Authors:
     * Chris Banford (zermattchris)
     * Fabian Jakobs (fjakobs)

************************************************************************ */

/**
 * A basic layout, which supports positioning of child widgets in a 'flowing'
 * manner, starting at the container's top/left position, placing children left to right
 * (like a HBox) until the there's no remaining room for the next child. When
 * out of room on the current line of elements, a new line is started, cleared
 * below the tallest child of the preceding line -- a bit like using 'float'
 * in CSS, except that a new line wraps all the way back to the left.
 *
 * *Features*
 *
 * <ul>
 * <li> Reversing children order </li>
 * <li> Manual line breaks </li>
 * <li> Horizontal alignment of lines </li>
 * <li> Vertical alignment of individual widgets within a line </li>
 * <li> Margins with horizontal margin collapsing </li>
 * <li> Horizontal and vertical spacing </li>
 * <li> Height for width calculations </li>
 * <li> Auto-sizing </li>
 * </ul>
 *
 * *Item Properties*
 *
 * <ul>
 * <li><strong>lineBreak</strong> <em>(Boolean)</em>: If set to <code>true</code>
 *   a forced line break will happen after this child widget.
 * </li>
 * </ul>
 *
 * *Example*
 *
 * Here is a little example of how to use the Flow layout.
 *
 * <pre class="javascript">
 *  var flowlayout = new qx.ui.layout.Flow();
 *
 *  flowlayout.setAlignX( "center" );  // Align children to the X axis of the container (left|center|right)
 *
 *  var container = new qx.ui.container.Composite(flowlayout);
 *  this.getRoot().add(container, {edge: 0});
 *
 *  var button1 = new qx.ui.form.Button("1. First Button", "flowlayout/test.png");
 *  container.add(button1);
 *
 *  var button2 = new qx.ui.form.Button("2. Second longer Button...", "flowlayout/test.png");
 *  // Have this child create a break in the current Line (next child will always start a new Line)
 *  container.add(button2, {lineBreak: true});
 *
 *  var button3 = new qx.ui.form.Button("3rd really, really, really long Button", "flowlayout/test.png");
 *  button3.setHeight(100);  // tall button
 *  container.add(button3);
 *
 *  var button4 = new qx.ui.form.Button("Number 4", "flowlayout/test.png");
 *  button4.setAlignY("bottom");
 *  container.add(button4);
 *
 *  var button5 = new qx.ui.form.Button("20px Margins around the great big 5th button!");
 *  button5.setHeight(100);  // tall button
 *  button5.setMargin(20);
 *  container.add(button5, {lineBreak: true});    // Line break after this button.
 *
 *  var button6 = new qx.ui.form.Button("Number 6", "flowlayout/test.png");
 *  button6.setAlignY("middle");  // Align this child to the vertical center of this line.
 *  container.add(button6);
 *
 *  var button7 = new qx.ui.form.Button("7th a wide, short button", "flowlayout/test.png");
 *  button7.setMaxHeight(20);  // short button
 *  container.add(button7);
 * </pre>
 *
 * *External Documentation*
 *
 * <a href='http://manual.qooxdoo.org/${qxversion}/pages/layout/flow.html'>
 * Extended documentation</a> and links to demos of this layout in the qooxdoo manuak.
 * @extends {qx.ui.layout.Abstract}
 */
qx.Class.define("qx.ui.layout.Flow",
{
  extend : qx.ui.layout.Abstract,


  /*
  *****************************************************************************
     CONSTRUCTOR
  *****************************************************************************
  * @param [spacingX=0] {Integer} The spacing between child widgets {@link #spacingX}.
   * @param [spacingY=0] {Integer} The spacing between the lines {@link #spacingY}.
   * @param [alignX="left"] {String} Horizontal alignment of the whole children
   *     block {@link #alignX}.
   */
  construct : function(spacingX, spacingY, alignX)
  {
    this.base(arguments);

    if (spacingX) {
      this.setSpacingX(spacingX);
    }

    if (spacingY) {
      this.setSpacingY(spacingY);
    }

    if (alignX) {
      this.setAlignX(alignX);
    }
  },



  /*
  *****************************************************************************
     PROPERTIES
  *****************************************************************************
  */

  properties :
  {
    /**
     * Horizontal alignment of the whole children block. The horizontal
     * alignment of the child is completely ignored in HBoxes (
     * {@link qx.ui.core.LayoutItem#alignX}).
     */
    alignX :
    {
      check : [ "left", "center", "right" ],
      init : "left",
      apply : "_applyLayoutChange"
    },

    /**
     * Vertical alignment of each child. Can be overridden through
     * {@link qx.ui.core.LayoutItem#alignY}.
     */
    alignY :
    {
      check : [ "top", "middle", "bottom"],
      init : "top",
      apply : "_applyLayoutChange"
    },

    /** Horizontal spacing between two children */
    spacingX :
    {
      check : "Integer",
      init : 0,
      apply : "_applyLayoutChange"
    },

    /**
     * The vertical spacing between the lines.
     */
    spacingY :
    {
      check : "Integer",
      init : 0,
      apply : "_applyLayoutChange"
    },

    /** Whether the actual children list should be laid out in reversed order. */
    reversed :
    {
      check : "Boolean",
      init : false,
      apply : "_applyLayoutChange"
    }

  },



  /*
  *****************************************************************************
     MEMBERS
  *****************************************************************************
  */

  members :
  {
    /*
    ---------------------------------------------------------------------------
      LAYOUT INTERFACE
    ---------------------------------------------------------------------------
    */

    // overridden
    verifyLayoutProperty : qx.core.Environment.select("qx.debug",
    {
      "true" : function(item, name, value) {
        this.assertEquals("lineBreak", name, "The property '"+name+"' is not supported by the flow layout!" );
      },

      "false" : null
    }),


    // overridden
    connectToWidget : function(widget)
    {
      this.base(arguments, widget);

      // Necessary to be able to calculate the lines for the flow layout.
      // Otherwise the layout calculates the needed width and height by using
      // only one line of items which is leading to the wrong height. This
      // wrong height does e.g. surpress scrolling since the scroll pane does
      // not know about the correct needed height.
      widget.setAllowShrinkY(false);
    },


    /**
     * The FlowLayout tries to add as many Children as possible to the current 'Line'
     * and when it sees that the next Child won't fit, it starts on a new Line, continuing
     * until all the Children have been added.
     * To enable alignX "left", "center", "right" renderLayout has to calculate the positions
     * of all a Line's children before it draws them.
     *
     * @param availWidth {Integer} Final width available for the content (in pixel)
     * @param availHeight {Integer} Final height available for the content (in pixel)
     * @return {void}
     */
    renderLayout : function(availWidth, availHeight)
    {
      var children = this._getLayoutChildren();

      if (this.getReversed()) {
        children = children.concat().reverse();
      }

      var lineCalculator = new qx.ui.layout.LineSizeIterator(
        children,
        this.getSpacingX()
      );

      var lineTop = 0;
      while (lineCalculator.hasMoreLines())
      {
        var line = lineCalculator.computeNextLine(availWidth);
        this.__renderLine(line, lineTop, availWidth);
        lineTop += line.height + this.getSpacingY();
      }
    },


    /**
     * Render a line in the flow layout
     *
     * @param line {Map} A line configuration as returned by
     *    {@link LineSizeIterator#computeNextLine}.
     * @param lineTop {Integer} The line's top position
     * @param availWidth {Integer} The available line width
     * @private
     */
    __renderLine : function(line, lineTop, availWidth)
    {
      var util = qx.ui.layout.Util;

      var left = 0;
      if (this.getAlignX() != "left") {
        left = availWidth - line.width;
        if (this.getAlignX() == "center") {
          left = Math.round(left / 2);
        }
      }

      for (var i=0; i<line.children.length; i++)
      {
        var child = line.children[i];
        var size = child.getSizeHint();
        var marginTop = child.getMarginTop();
        var marginBottom = child.getMarginBottom();

        var top = util.computeVerticalAlignOffset(
          child.getAlignY() || this.getAlignY(),
          marginTop + size.height + marginBottom,
          line.height,
          marginTop, marginBottom
        );

        child.renderLayout(
          left + line.gapsBefore[i],
          lineTop + top,
          size.width,
          size.height
        );

        left += line.gapsBefore[i] + size.width;
      }
    },


    // overridden
    /**
     * @protected
     */
    _computeSizeHint : function() {
      return this.__computeSize(Infinity);
    },


    // overridden
    hasHeightForWidth : function() {
      return true;
    },


    // overridden
    getHeightForWidth : function(width) {
      return this.__computeSize(width).height;
    },


    /**
     * Compute the preferred size optionally constrained by the available width
     *
     * @param availWidth {Integer} The available width
     * @return {Map} Map containing the preferred height and width of the layout
     * @private
     */
    __computeSize : function(availWidth)
    {
      var lineCalculator = new qx.ui.layout.LineSizeIterator(
        this._getLayoutChildren(),
        this.getSpacingX()
      );

      var height = 0;
      var width = 0;
      var lineCount = 0;

      while (lineCalculator.hasMoreLines())
      {
        var line = lineCalculator.computeNextLine(availWidth);
        lineCount += 1;
        width = Math.max(width, line.width);
        height += line.height;
      }

      return {
        width : width,
        height : height + this.getSpacingY() * (lineCount-1)
      };
    }
  }
});
/* ************************************************************************

   qooxdoo - the new era of web development
   http://qooxdoo.org

   Copyright:
     2008 Dihedrals.com, http://www.dihedrals.com

   License:
     LGPL: http://www.gnu.org/licenses/lgpl.html
     EPL: http://www.eclipse.org/org/documents/epl-v10.php
     See the LICENSE file in the project's top-level directory for details.

   Authors:
     * Chris Banford (zermattchris)
     * Fabian Jakobs (fjakobs)

************************************************************************ */

/**
 * This class iterates over the lines in a flow layout.
 *
 * @internal
 * @extends {Object}
 */
qx.Class.define("qx.ui.layout.LineSizeIterator",
{
  extend : Object,

  /**
   * @param children {qx.ui.core.Widget[]} The children of the flow layout to
   *    compute the lines from
   * @param spacing {Integer} The horizontal spacing between the children
   */
  construct : function(children, spacing)
  {
    this.__children = children;
    this.__spacing = spacing;

    this.__hasMoreLines = children.length > 0;
    this.__childIndex = 0;
  },

  members :
  {
    __children : null,
    __spacing : null,
    __hasMoreLines : null,
    __childIndex : null,


    /**
     * Computes the properties of the next line taking the available width into
     * account
     *
     * @param availWidth {Integer} The available width for the next line
     * @return {Map} A map containing the line's properties.
     */
    computeNextLine : function(availWidth)
    {
      var availWidth = availWidth || Infinity;

      if (!this.__hasMoreLines) {
        throw new Error("No more lines to compute");
      }

      var children = this.__children;
      var lineHeight = 0;
      var lineWidth = 0;
      var lineChildren = [];
      var gapsBefore = [];

      for (var i=this.__childIndex; i<children.length; i++)
      {
        var child = children[i];
        var size = child.getSizeHint();

        var gapBefore = this.__computeGapBeforeChild(i);
        var childWidth = size.width + gapBefore;
        var isFirstChild = i == this.__childIndex;
        if (!isFirstChild && lineWidth + childWidth > availWidth)
        {
          this.__childIndex = i;
          break;
        }

        var childHeight = size.height + child.getMarginTop() + child.getMarginBottom();
        lineChildren.push(child);
        gapsBefore.push(gapBefore);
        lineWidth += childWidth;
        lineHeight = Math.max(lineHeight, childHeight);

        if (child.getLayoutProperties().lineBreak) {
          this.__childIndex = i+1;
          break;
        }
      }

      if (i >= children.length) {
        this.__hasMoreLines = false;
      }

      return {
        height: lineHeight,
        width: lineWidth,
        children: lineChildren,
        gapsBefore : gapsBefore
      }
    },


    /**
     * Computes the gap before the child at the given index
     *
     * @param childIndex {Integer} The index of the child widget
     * @return {Integer} The gap before the given child
     * @private
     */
    __computeGapBeforeChild : function(childIndex)
    {
      var isFirstInLine = childIndex == this.__childIndex
      if (isFirstInLine) {
        return this.__children[childIndex].getMarginLeft();
      } else {
        return Math.max(
          this.__children[childIndex-1].getMarginRight(),
          this.__children[childIndex].getMarginLeft(),
          this.__spacing
        )
      }
    },


    /**
     * Whether there are more lines
     *
     * @return {Boolean} Whether there are more lines
     */
    hasMoreLines : function() {
      return this.__hasMoreLines;
    }
  }
})
