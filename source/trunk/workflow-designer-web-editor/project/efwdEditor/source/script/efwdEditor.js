(function(){

if (!window.qx) window.qx = {};

qx.$$start = new Date();
  
if (!qx.$$environment) qx.$$environment = {};
var envinfo = {"qx.application":"efwdEditor.Application","qx.revision":"","qx.theme":"efwdEditor.theme.Theme","qx.version":"1.6"};
for (var k in envinfo) qx.$$environment[k] = envinfo[k];

if (!qx.$$libraries) qx.$$libraries = {};
var libinfo = {"__out__":{"sourceUri":"script"},"aristo":{"resourceUri":"../../qooxdoo/Aristo/source/resource","sourceUri":"../../qooxdoo/Aristo/source/class"},"efwdDal":{"resourceUri":"../../efwdDal/source/resource","sourceUri":"../../efwdDal/source/class"},"efwdEditor":{"resourceUri":"../source/resource","sourceUri":"../source/class"},"efwdUI":{"resourceUri":"../../efwdUI/source/resource","sourceUri":"../../efwdUI/source/class"},"library":{"resourceUri":"../../library/source/resource","sourceUri":"../../library/source/class"},"qx":{"resourceUri":"../../qooxdoo/qooxdoo-1.6-sdk/framework/source/resource","sourceUri":"../../qooxdoo/qooxdoo-1.6-sdk/framework/source/class"}};
for (var k in libinfo) qx.$$libraries[k] = libinfo[k];

qx.$$resources = {};
qx.$$translations = {"C":null,"en":null};
qx.$$locales = {"C":null,"en":null};
qx.$$packageData = {};

qx.$$loader = {
  parts : {"boot":[0]},
  packages : {"0":{"uris":["__out__:efwdEditor.4829ead7cc22.js","library:library/fx/MBaseExtend.js","library:library/Deferred.js","library:library/ui/messagesqueue/Manager.js","__out__:efwdEditor.c7026edfd93f.js","efwdUI:efwdUI/Application.js","library:library/Extends.js","__out__:efwdEditor.31f129993540.js","library:library/MObjectExtend.js","library:library/ListenersStore.js","library:library/data/Helper.js","__out__:efwdEditor.81d8bfe4ecaf.js","library:library/data/MChangeSelf.js","__out__:efwdEditor.f9a75d7553ca.js","library:library/ui/extend/MListExtends.js","library:library/ui/extend/MExecutableExtends.js","__out__:efwdEditor.e2992647a5e2.js","library:library/data/MArrayExtend.js","__out__:efwdEditor.ede1e5d75ef8.js","library:library/dev/Debug.js","__out__:efwdEditor.e1c62fa23900.js","efwdEditor:efwdEditor/Application.js","library:library/BOM.js","library:library/IServiceLocator.js","library:library/IOC.js","efwdUI:efwdUI/commands/types/AbstractCommand.js","efwdEditor:efwdEditor/commands/types/MoveStatuses.js","efwdDal:efwdDal/EntitiesArray.js","efwdDal:efwdDal/entity/AbstractEntity.js","efwdDal:efwdDal/entity/Process.js","efwdDal:efwdDal/repository/AbstractProcessChildRepository.js","efwdDal:efwdDal/repository/StatusesRepository.js","efwdDal:efwdDal/entity/MProcessChild.js","efwdDal:efwdDal/entity/MActivitiesOwner.js","efwdDal:efwdDal/entity/Status.js","__out__:efwdEditor.467dd81092f1.js","efwdDal:efwdDal/repository/SessionRepository.js","efwdUI:efwdUI/behavior/PageModel.js","__out__:efwdEditor.c7fde629dce9.js","library:library/ui/messagesqueue/Message.js","__out__:efwdEditor.38ce77290eda.js","efwdDal:efwdDal/saving/ChangeUnit.js","efwdDal:efwdDal/Service.js","efwdDal:efwdDal/Mapper.js","efwdDal:efwdDal/Context.js","efwdDal:efwdDal/saving/ChangeUnitsQueue.js","efwdDal:efwdDal/IdsMapper.js","efwdDal:efwdDal/repository/SessionLocalStorage.js","__out__:efwdEditor.a129ee0d3b54.js","efwdDal:efwdDal/repository/ActivitiesRepository.js","efwdDal:efwdDal/entity/Activity.js","efwdDal:efwdDal/repository/ActionsRepository.js","efwdDal:efwdDal/entity/Action.js","efwdDal:efwdDal/AutoNamesGenerator.js","efwdDal:efwdDal/saving/AbstractSaver.js","efwdDal:efwdDal/repository/ProcessesRepository.js","efwdDal:efwdDal/Dependencies.js","__out__:efwdEditor.e4c2cb0e8385.js","library:library/ui/Dialog.js","library:library/ui/Helper.js","__out__:efwdEditor.9499289dcb31.js","library:library/bom/Loader.js","__out__:efwdEditor.d7f206b07341.js","library:library/ui/Command.js","__out__:efwdEditor.45dc7bec4af7.js","efwdUI:efwdUI/ui/dialog/entitieslist/EntitiesList.js","library:library/data/SortedArray.js","efwdUI:efwdUI/ui/dialog/entitieslist/ProcessesList.js","efwdUI:efwdUI/commands/Queue.js","efwdUI:efwdUI/commands/ExecuteCommandEvent.js","efwdUI:efwdUI/commands/types/EditEntity.js","efwdUI:efwdUI/commands/types/DeleteEntities.js","efwdUI:efwdUI/commands/types/statedumper/Abstract.js","efwdUI:efwdUI/commands/types/statedumper/Activity.js","efwdUI:efwdUI/commands/types/statedumper/Status.js","efwdUI:efwdUI/behavior/EntityEditionModel.js","efwdUI:efwdUI/commands/types/CreateAction.js","efwdUI:efwdUI/ui/dialog/Dialog.js","efwdUI:efwdUI/ui/dialog/ProcessDialog.js","efwdUI:efwdUI/ui/IconsStore.js","__out__:efwdEditor.28d315dcff6d.js","efwdUI:efwdUI/ui/entityediting/view/EntityForm.js","library:library/form/TableEditor.js","__out__:efwdEditor.f8c040a6797e.js","library:library/ui/widget/SelectBox.js","__out__:efwdEditor.0c1a1d481b35.js","efwdUI:efwdUI/ui/entityediting/controls/EditablePropertyTable.js","__out__:efwdEditor.cb827fa6da50.js","library:library/form/AbstractChooseField.js","efwdUI:efwdUI/ui/entityediting/controls/AllProcessesActionsChooser.js","efwdUI:efwdUI/ui/dialog/viewmodels/EntitiesListViewModel.js","efwdUI:efwdUI/ui/dialog/viewmodels/ActionsListViewModel.js","efwdUI:efwdUI/ui/dialog/EntitiesListDialog.js","efwdUI:efwdUI/ui/dialog/ActionsListDialog.js","efwdUI:efwdUI/ui/dialog/entitieslist/ActionsList.js","library:library/form/SetAsButton.js","__out__:efwdEditor.afed376ba849.js","library:library/ui/management/ZonesManager.js","library:library/ui/management/Zone.js","__out__:efwdEditor.946f6898ff1c.js","library:library/form/SingleRenderer.js","efwdUI:efwdUI/ui/settingspanel/editor/IEditor.js","efwdUI:efwdUI/ui/settingspanel/editor/EntityEditor.js","efwdUI:efwdUI/ui/entityediting/viewModel/EntityEditor.js","efwdUI:efwdUI/ui/entityediting/viewModel/StatusEditor.js","efwdUI:efwdUI/ui/settingspanel/Controller.js","efwdUI:efwdUI/ui/dialog/viewmodels/ActivitiesListViewModel.js","efwdUI:efwdUI/commands/types/ReversableCommand.js","efwdUI:efwdUI/commands/types/AddActivitiesToOwner.js","efwdUI:efwdUI/commands/types/CreateActivity.js","efwdUI:efwdUI/commands/types/ReverseCommand.js","efwdUI:efwdUI/commands/types/RemoveActivities.js","efwdUI:efwdUI/ui/dialog/ActivitiesDialog.js","__out__:efwdEditor.f64d867cef43.js","efwdUI:efwdUI/ui/dialog/entitieslist/ActivitiesList.js","efwdUI:efwdUI/commands/types/SetInitialStatus.js","efwdUI:efwdUI/behavior/GeneralCommandsProvider.js","efwdUI:efwdUI/ui/dialog/AllActivitiesDialog.js","efwdUI:efwdUI/ui/views/EditorContainer.js","efwdUI:efwdUI/ui/dialog/viewmodels/ProcessesListViewModel.js","efwdUI:efwdUI/commands/types/CreateProcess.js","efwdUI:efwdUI/ui/dialog/viewmodels/ActivitiesListsViewModel.js","efwdUI:efwdUI/commands/types/CreateStatus.js","efwdUI:efwdUI/ApplicationBehaviorController.js","efwdUI:efwdUI/Dependencies.js","efwdEditor:efwdEditor/canvas/views/ElementsRenderer.js","efwdEditor:efwdEditor/behavior/manipulationtool/ITool.js","efwdEditor:efwdEditor/behavior/manipulationtool/StatusTool.js","efwdEditor:efwdEditor/behavior/manipulationtool/ActionTool.js","efwdEditor:efwdEditor/behavior/GeneralCommandsProvider.js","efwdEditor:efwdEditor/canvas/views/Canvas.js","efwdEditor:efwdEditor/canvas/views/Helper.js","__out__:efwdEditor.0a6c2990ecea.js","efwdEditor:efwdEditor/canvas/behavior/tool/StatusCreation.js","efwdEditor:efwdEditor/canvas/behavior/ContextMenu.js","efwdEditor:efwdEditor/behavior/SelectionModel.js","efwdEditor:efwdEditor/canvas/behavior/ElementsSelection.js","efwdEditor:efwdEditor/behavior/manipulationtool/DefaultTool.js","efwdEditor:efwdEditor/behavior/manipulationtool/Model.js","efwdEditor:efwdEditor/commands/types/MoveActionConnectorJoint.js","efwdEditor:efwdEditor/canvas/behavior/tool/ConnectorJointsManaging.js","efwdEditor:efwdEditor/commands/types/MoveActionLabel.js","efwdEditor:efwdEditor/canvas/behavior/ConnectorsTransformation.js","efwdEditor:efwdEditor/canvas/views/Cursor.js","efwdEditor:efwdEditor/canvas/CanvasGridModel.js","library:library/ui/management/KeysManager.js","efwdEditor:efwdEditor/canvas/behavior/ResizeAndScrollCanvas.js","efwdEditor:efwdEditor/behavior/DialogsModel.js","efwdEditor:efwdEditor/canvas/behavior/tool/selectedElementTool/Abstract.js","efwdEditor:efwdEditor/canvas/behavior/tool/selectedElementTool/Action.js","efwdEditor:efwdEditor/commands/types/MoveActionConnectorPinPoint.js","efwdEditor:efwdEditor/canvas/behavior/tool/selectedElementTool/Status.js","efwdEditor:efwdEditor/canvas/behavior/SelectedElementTool.js","efwdEditor:efwdEditor/canvas/CanvasElementsHandling.js","efwdEditor:efwdEditor/commands/Controller.js","efwdEditor:efwdEditor/canvas/behavior/ActivitiesListsShowing.js","efwdEditor:efwdEditor/commands/types/PasteEntities.js","efwdEditor:efwdEditor/canvas/CommandsProvider.js","efwdEditor:efwdEditor/canvas/behavior/ElementsEdit.js","efwdEditor:efwdEditor/canvas/behavior/tool/ElementsMove.js","efwdEditor:efwdEditor/canvas/behavior/ElementsMove.js","efwdEditor:efwdEditor/ui/views/Layout.js","__out__:efwdEditor.5e0680b8480c.js","efwdEditor:efwdEditor/behavior/Clipboard.js","efwdEditor:efwdEditor/canvas/ElementExtends.js","efwdEditor:efwdEditor/canvas/Controller.js","efwdEditor:efwdEditor/commands/types/CreateActionAndDestStatus.js","efwdEditor:efwdEditor/ui/views/Palette.js","__out__:efwdEditor.3fdd1ea35e19.js","efwdEditor:efwdEditor/canvas/BBoxesModel.js","efwdEditor:efwdEditor/canvas/behavior/tool/ActionCreation.js","efwdEditor:efwdEditor/canvas/behavior/ElementsCreation.js","efwdDal:efwdDal/saving/PackageSaver.js","efwdEditor:efwdEditor/ui/dialog/entitieslist/ActionsList.js","library:library/ui/management/ShortcutsManager.js","efwdEditor:efwdEditor/ui/Controller.js","efwdEditor:efwdEditor/canvas/behavior/MoveActionPinPoint.js","efwdEditor:efwdEditor/ui/DialogsController.js","efwdEditor:efwdEditor/ui/views/Toolbar.js","__out__:efwdEditor.b21cab2496b2.js","library:library/ui/WidgetChildrenModelConnector.js","__out__:efwdEditor.0fad02d69a1f.js","efwdEditor:efwdEditor/Dependencies.js","__out__:efwdEditor.d02ea1bc8f71.js","efwdUI:efwdUI/theme/Decoration.js","efwdEditor:efwdEditor/theme/Decoration.js","__out__:efwdEditor.0c1f56951ec6.js","efwdUI:efwdUI/theme/Font.js","efwdEditor:efwdEditor/theme/Font.js","__out__:efwdEditor.bf78c6955ddc.js","efwdUI:efwdUI/theme/Color.js","efwdEditor:efwdEditor/theme/Color.js","__out__:efwdEditor.ca90d2aeb7e9.js","efwdUI:efwdUI/theme/Appearance.js","efwdEditor:efwdEditor/theme/Appearance.js","efwdEditor:efwdEditor/theme/Theme.js"]}},
  urisBefore : [],
  cssBefore : [],
  boot : "boot",
  closureParts : {},
  bootIsInline : false,
  addNoCacheParam : false,
  
  decodeUris : function(compressedUris)
  {
    var libs = qx.$$libraries;
    var uris = [];
    for (var i=0; i<compressedUris.length; i++)
    {
      var uri = compressedUris[i].split(":");
      var euri;
      if (uri.length==2 && uri[0] in libs) {
        var prefix = libs[uri[0]].sourceUri;
        euri = prefix + "/" + uri[1];
      } else {
        euri = compressedUris[i];
      }
      if (qx.$$loader.addNoCacheParam) {
        euri += "?nocache=" + Math.random();
      }
      
      uris.push(euri);
    }
    return uris;      
  }
};  

function loadScript(uri, callback) {
  var elem = document.createElement("script");
  elem.charset = "utf-8";
  elem.src = uri;
  elem.onreadystatechange = elem.onload = function() {
    if (!this.readyState || this.readyState == "loaded" || this.readyState == "complete") {
      elem.onreadystatechange = elem.onload = null;
      callback();
    }
  };
  var head = document.getElementsByTagName("head")[0];
  head.appendChild(elem);
}

function loadCss(uri) {
  var elem = document.createElement("link");
  elem.rel = "stylesheet";
  elem.type= "text/css";
  elem.href= uri;
  var head = document.getElementsByTagName("head")[0];
  head.appendChild(elem);
}

var isWebkit = /AppleWebKit\/([^ ]+)/.test(navigator.userAgent);

function loadScriptList(list, callback) {
  if (list.length == 0) {
    callback();
    return;
  }
  var item = list.shift();
  loadScript(item,  function() {
    if (isWebkit) {
      // force async, else Safari fails with a "maximum recursion depth exceeded"
      window.setTimeout(function() {
        loadScriptList(list, callback);
      }, 0);
    } else {
      loadScriptList(list, callback);
    }
  });
}

var fireContentLoadedEvent = function() {
  qx.$$domReady = true;
  document.removeEventListener('DOMContentLoaded', fireContentLoadedEvent, false);
};
if (document.addEventListener) {
  document.addEventListener('DOMContentLoaded', fireContentLoadedEvent, false);
}

qx.$$loader.importPackageData = function (dataMap, callback) {
  if (dataMap["resources"]){
    var resMap = dataMap["resources"];
    for (var k in resMap) qx.$$resources[k] = resMap[k];
  }
  if (dataMap["locales"]){
    var locMap = dataMap["locales"];
    var qxlocs = qx.$$locales;
    for (var lang in locMap){
      if (!qxlocs[lang]) qxlocs[lang] = locMap[lang];
      else 
        for (var k in locMap[lang]) qxlocs[lang][k] = locMap[lang][k];
    }
  }
  if (dataMap["translations"]){
    var trMap   = dataMap["translations"];
    var qxtrans = qx.$$translations;
    for (var lang in trMap){
      if (!qxtrans[lang]) qxtrans[lang] = trMap[lang];
      else 
        for (var k in trMap[lang]) qxtrans[lang][k] = trMap[lang][k];
    }
  }
  if (callback){
    callback(dataMap);
  }
}

qx.$$loader.signalStartup = function () 
{
  qx.$$loader.scriptLoaded = true;
  if (window.qx && qx.event && qx.event.handler && qx.event.handler.Application) {
    qx.event.handler.Application.onScriptLoaded();
    qx.$$loader.applicationHandlerReady = true; 
  } else {
    qx.$$loader.applicationHandlerReady = false;
  }
}

// Load all stuff
qx.$$loader.init = function(){
  var l=qx.$$loader;
  if (l.cssBefore.length>0) {
    for (var i=0, m=l.cssBefore.length; i<m; i++) {
      loadCss(l.cssBefore[i]);
    }
  }
  if (l.urisBefore.length>0){
    loadScriptList(l.urisBefore, function(){
      l.initUris();
    });
  } else {
    l.initUris();
  }
}

// Load qooxdoo boot stuff
qx.$$loader.initUris = function(){
  var l=qx.$$loader;
  var bootPackageHash=l.parts[l.boot][0];
  if (l.bootIsInline){
    l.importPackageData(qx.$$packageData[bootPackageHash]);
    l.signalStartup();
  } else {
    loadScriptList(l.decodeUris(l.packages[l.parts[l.boot][0]].uris), function(){
      // Opera needs this extra time to parse the scripts
      window.setTimeout(function(){
        l.importPackageData(qx.$$packageData[bootPackageHash] || {});
        l.signalStartup();
      }, 0);
    });
  }
}
})();



qx.$$loader.init();

