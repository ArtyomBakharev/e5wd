/*
 #asset(efwdProcesses/*)
 #ignore(ioc)
 #ignore(JsfIoc)
 * @extends {efwdUI.Application}
 */
qx.Class.define("efwdProcesses.Application", {

	extend: efwdUI.Application,

	statics: {
		/**
		 * Статическая инициализация приложения (используется как в приложении так и в юнит-тестах)
		 */
		staticInit: function ()
		{
			efwdUI.Application.staticInit();
		}
	},

	members: {

		/**
		 * This method contains the initial application code and gets called
		 * during startup of the application
		 */
		main: function ()
		{
			this.base(arguments);
			this.self(arguments).staticInit();

			this.__container = new library.IOC(new JsfIoc());
			if (qx.core.Environment.get("qx.debug"))
			{
				ioc = this.__container;
			}

			this.__registerServices();
			this.__initializeServices();
		},

		/**
		 * @private
		 */
		__registerServices: function ()
		{
			//application
			this.__container.register(efwdUI.Application, {instance: this});
			this.__container.register(efwdProcesses.Application, {instance: this});

			//interfaces
			this.__container.register(library.IServiceLocator, {instance: this.__container});

			//dependencies
			this.__container.registerAll(efwdCommon.Dependencies.DEPENDENCIES);
			this.__container.registerAll(efwdDal.Dependencies.DEPENDENCIES);
			this.__container.registerAll(efwdUI.Dependencies.DEPENDENCIES);
			this.__container.registerAll(efwdProcesses.Dependencies.DEPENDENCIES);
		},

		/**
		 * @private
		 */
		__initializeServices: function ()
		{
			this.__container.getService(efwdDal.AutoNamesGenerator).initialize();

			return this.__container.getService(efwdDal.repository.ProcessesRepository)
				.getProcesses().deferred()
				.next(function ()
				{
					this.__container.getService(efwdUI.ApplicationBehaviorController).initialize();
					this.__container.getService(efwdProcesses.ApplicationBehaviorController).initialize();
					this.__container.getService(efwdProcesses.ui.Controller).initialize();
					removeAppLoadIndicator();
				}, this);
		}
	}
});
