/*
 #ignore(efwdConfig.)
 #ignore(efwdConfig.*)
 */

/**
 * Контроллер решает конфликты при одновременном редактировании процессов несколькими пользователями
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdProcesses.ApplicationBehaviorController", {

	extend: qx.core.Object,

	/**
	 * @param {efwdDal.Service} service
	 * @param {efwdDal.Context} context
	 * @param {efwdProcesses.Application} application
	 */
	construct: function (service, context, application)
	{
		this.__service = service;
		this.__context = context;
		this.__application = application;
	},

	members: {

		initialize: function ()
		{
			this.__service.addListener("handleError", function (e)
			{
				var data = e.getData();
				/** @type {efwdDal.saving.ChangeUnit} */
				var changeUnit = data.changeUnit;
				var entity = data.changeUnit && changeUnit.getEntity();

				if (data.status == efwdConfig.efwdCommon.Communication.codes.entityDoesNotExists && entity && entity instanceof efwdDal.entity.Process)
				{
					if (changeUnit.getChangeType() == efwdDal.saving.ChangeUnit.CHANGE_TYPE_UPDATE)
					{
						this.__context.saveChanges();
						entity.remove();
						this.__context.clearChanges();
					}

					var message = library.ui.messagesqueue.Message.createAlertMessage(
						"The " + entity.getEntityType() + " '" + entity.getSource().getName()
							+ "' which you want to access does not exist on the server, maybe it was deleted by another user.");
					message.setTimeToClose(10000);
					this.__application.getMessagesQueue().addMessage(message);
					e.preventDefault();
				}
			}, this);
		}

	},

	defer: function (statics, members, properties)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}

});
