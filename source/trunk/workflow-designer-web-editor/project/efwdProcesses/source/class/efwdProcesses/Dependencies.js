/*
 #ignore(efwdCommon)
 #ignore(efwdCommon.*)
 #ignore(efwdRenderer)
 #ignore(efwdRenderer.*)
 */

/**
 * Класс хранит список зависимостей для IOC-контейнера
 */
qx.Class.define("efwdProcesses.Dependencies", {

	type: "static",

	statics: {
		DEPENDENCIES: [

		/** {@link efwdDal.saving} */
			[efwdDal.saving.AbstractSaver, {cls: efwdDal.saving.SequentialSaver, args: [
				efwdDal.IdsMapper,
				efwdDal.Service
			]}],

		/** {@link efwdProcesses} */
			[efwdProcesses.ApplicationBehaviorController, {args: [
				efwdDal.Service,
				efwdDal.Context,
				efwdProcesses.Application
			]}],

		/** {@link efwdProcesses.ui} */
			[efwdProcesses.ui.Controller, {args: [
				efwdProcesses.ui.views.Layout,
				efwdUI.ui.settingspanel.Controller,
				efwdUI.ui.dialog.viewmodels.ProcessesListViewModel,
				efwdProcesses.ui.dialog.entitieslist.ProcessesList
			]}],

		/** {@link efwdProcesses.ui.dialog.entitieslist} */
			[efwdProcesses.ui.dialog.entitieslist.ProcessesList, {args: [
				efwdDal.IdsMapper
			]}],

		/** {@link efwdProcesses.ui.views} */
			[efwdProcesses.ui.views.Layout, {args: [
				efwdUI.ui.views.EditorContainer
			]}]
		]
	}
});
