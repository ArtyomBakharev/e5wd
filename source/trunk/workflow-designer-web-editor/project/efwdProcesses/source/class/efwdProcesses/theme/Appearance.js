qx.Theme.define("efwdProcesses.theme.Appearance", {

	extend: efwdUI.theme.Appearance,

	appearances: {
		"processes-list": {
			include: "widget",
			style: function (state, style)
			{
				var font = qx.bom.Font.fromConfig(qx.theme.manager.Font.getInstance().getTheme().fonts["default"]).set({
					size: 25
				});

				return {
					font: font
				}
			}
		},
		"processes-list/create-process-menu": {
			include: "menu",
			style: function (state, style)
			{
				var font = qx.bom.Font.fromConfig(qx.theme.manager.Font.getInstance().getTheme().fonts["default"]).set({
					size: 25
				});

				return {
					font: font
				}
			}
		},
		"processes-list/entities-list": {
			include: "virtual-list",
			style: function (state, style)
			{
				return {
					itemHeight: 40
				}
			}
		}
	}
});