qx.Theme.define("efwdProcesses.theme.Theme",
	{
		meta: {
			color: efwdProcesses.theme.Color,
			decoration: efwdProcesses.theme.Decoration,
			font: efwdProcesses.theme.Font,
			icon: qx.theme.icon.Oxygen,
			appearance: efwdProcesses.theme.Appearance
		}
	});