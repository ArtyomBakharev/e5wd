/**
 * Главный контроллер графического приложения. Отрисовывает первоначальный layout.
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdProcesses.ui.Controller", {

	extend: qx.core.Object,

	/**
	 * @param {efwdProcesses.ui.views.Layout} layout
	 * @param {efwdUI.ui.settingspanel.Controller} settingsPanelController
	 * @param {efwdUI.ui.dialog.viewmodels.ProcessesListViewModel} processesListViewModel
	 * @param {efwdUI.ui.dialog.entitieslist.ProcessesList} processesList
	 */
	construct: function (layout, settingsPanelController, processesListViewModel, processesList)
	{
		this.__layout = layout;
		this.__settingsPanelController = settingsPanelController;
		this.__processesListViewModel = processesListViewModel;
		this.__processesList = processesList;
	},

	members: {
		/**
		 * Отрисовка всех основных видов
		 */
		initialize: function ()
		{
			this.__settingsPanelController.initialize();
			this.__processesListViewModel.initialize();
			this.__processesList.initialize(this.__processesListViewModel);
			this.__layout.render(this.__processesList);
		}
	},

	defer: function (statics, members, properties)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}

});
