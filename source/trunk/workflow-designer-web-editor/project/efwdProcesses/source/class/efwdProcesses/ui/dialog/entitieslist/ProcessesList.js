/**
 * Список процессов с возможностью открытие процесса на редактирование
 * @extends {efwdUI.ui.dialog.entitieslist.ProcessesList}
 */
qx.Class.define("efwdProcesses.ui.dialog.entitieslist.ProcessesList", {

	extend: efwdUI.ui.dialog.entitieslist.ProcessesList,

	/**
	 * @param {efwdDal.IdsMapper} idsMapper
	 */
	construct: function (idsMapper)
	{
		this.base(arguments);
		this.__idsMapper = idsMapper;
	},

	members: {
		/**
		 * @param {efwdUI.ui.dialog.viewmodels.ProcessesListViewModel} viewModel
		 */
		initialize: function (viewModel, options)
		{
			var $this = this;
			this.base(arguments, viewModel, Object.merge({
				buttons: [
					{
						name: "open",
						title: "open",
						execute: function (entity)
						{
							$this.__openProcess(entity);
						}
					}
				],
				events: {
					dblclick: function(entity)
					{
						$this.__openProcess(entity);
					}
				},
				confirmDeletion: true
			}, options || {}));
		},

		/**
		 * @param {efwdDal.entity.Process} process
		 * @private
		 */
		__openProcess: function(process)
		{
			window.open(efwdCommon.Helper.getProcessEditorUrl(this.__idsMapper.getServerId(process.getSource().getId())));
		}
	}

});
