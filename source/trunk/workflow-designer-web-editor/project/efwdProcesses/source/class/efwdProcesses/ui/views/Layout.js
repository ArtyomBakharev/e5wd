/*
 #ignore(efwdConfig)
 #ignore(efwdConfig.*)
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdProcesses.ui.views.Layout", {

	extend: qx.core.Object,

	/**
	 * @param {efwdUI.ui.views.EditorContainer} editorContainer
	 */
	construct: function (editorContainer, processesList)
	{
		this.__editorContainer = editorContainer;
	},

	members: {

		/**
		 * @param {efwdUI.ui.dialog.entitieslist.ProcessesList} processesList
		 */
		render: function (processesList)
		{
			var root = qx.core.Init.getApplication().getRoot();
			var pane = new qx.ui.splitpane.Pane("horizontal");

			processesList.setAppearance("processes-list");
			pane.add(processesList, 1);
			pane.add(this.__createSettingsPane(), 0);

			root.add(pane, {edge: 0});
		},


		/**
		 * @private
		 */
		__createSettingsPane: function ()
		{
			var scrollContainer = new qx.ui.container.Scroll();
			scrollContainer.set({
				scrollbarX: "off",
				width: efwdConfig.efwdProcesses.ui.$style.layout.toolPanelWidth
			});
			scrollContainer.add(this.__editorContainer.render());

			return scrollContainer;
		}
	}
});
