/**
 * Лоадер, показывается пользователю пока приложение не будет загружено (после загрузки следует вызвать removeAppLoadIndicator())
 */
(function ()
{
	document.write('<div id="load-awaiting" style="position: absolute; top: 0; right: 0; bottom: 0; left: 0;' +
		'background: #ffffff; opacity: 0.5; padding-top: 100px; padding-left: 100px; color: #000000; font-size: 40px;' +
		'font-family: Verdana, serif; z-index: 5000;">Wait, please<span id="load-awaiting-dots">...</span></div>');

	var interval = setInterval(function ()
	{
		var element = document.getElementById("load-awaiting-dots");
		if (!element)
		{
			clearInterval(interval);
			return;
		}

		element.innerHTML = element.innerHTML.length < 3 ? element.innerHTML + "." : ".";
	}, 1000);

	window.removeAppLoadIndicator = function()
	{
		var element = document.getElementById("load-awaiting");
		element && element.parentNode.removeChild(element);
	}
})();