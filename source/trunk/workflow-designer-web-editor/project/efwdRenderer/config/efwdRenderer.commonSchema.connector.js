goog.provide("__symbol__.efwdConfig.efwdRenderer.commonSchema.connector");

//noinspection JSUnusedGlobalSymbols
AppTools.config("efwdConfig.efwdRenderer.commonSchema", {

	$functions: {
		getArrowPointerPath: function (pathArray, width, length)
		{
			if (!pathArray)
			{
				return null;
			}

			var geo = efwdRenderer.Geo;
			var normalStart = pathArray[pathArray.length - 2];
			var normalEnd = pathArray[pathArray.length - 1];

			var normal = geo.subPoints(normalEnd, normalStart);
			//вектор для указателя
			var pvec = geo.mulVector(geo.normalizeVector(normal), length);
			var opvec = geo.ortVector(pvec);
			opvec = geo.mulVector(geo.normalizeVector(opvec), width / 2);

			var pointerStart = geo.subPoints(normalEnd, pvec);
			var firstPoint = geo.subPoints(pointerStart, opvec);
			var lastPoint = geo.addPoints(pointerStart, opvec);
			return efwdRenderer.Helper.pointsToPath([
				firstPoint,
				normalEnd,
				lastPoint,
				firstPoint
			]);
		},

		getArrowPathArray: function (source, dest, joints, emptyZone)
		{
			if (!source || !dest)
			{
				return null;
			}

			var geo = efwdRenderer.Geo;
			var destPoint;
			var sourcePoint;
			if (!("width" in dest))
			{
				destPoint = dest;
				dest = null;
			}

			if (!("width" in source))
			{
				sourcePoint = source;
				source = null;
			}

			function rectAndRayIntersection(rect, a, b)
			{
				b = geo.isPointsEquals(a, b) ? {x: a.x + geo.EPSILON, y: a.y} : b;
				return geo.rectAndRayIntersection(rect, a, b)[0];
			}

			var sCenter = source ? {x: source.left + source.width / 2, y: source.top + source.height / 2} : sourcePoint;
			var dCenter = dest ? {x: dest.left + dest.width / 2, y: dest.top + dest.height / 2} : destPoint;

			if (!joints)
			{
				joints = [];
			}

			var firstJoint = joints.length > 0 ? joints[0] : dCenter;
			var lastJoint = joints.length > 0 ? joints[joints.length - 1] : sCenter;

			var path = [sourcePoint || rectAndRayIntersection(source, sCenter, firstJoint)];

			path = path.concat(joints);

			if (destPoint)
			{
				path.push(emptyZone ? this.__takeOutPointFromEmptyZone(lastJoint, destPoint, emptyZone) : destPoint);
			}
			else
			{
				path.push(rectAndRayIntersection(dest, dCenter, lastJoint));
			}

			return efwdRenderer.Helper.isValidPoint(path[0]) && efwdRenderer.Helper.isValidPoint(path[path.length - 1])
				? path : null;
		},

		getPath: function (pathArray)
		{
			return pathArray ? {path: efwdRenderer.Helper.pointsToPath(pathArray), array: pathArray} : null;
		},

		getConnectorLabelFixationPoint: function (pathArray)
		{
			var geo = efwdRenderer.Geo;

			if (!pathArray)
			{
				return geo.OUT_OF_CANVAS_POINT;
			}

			//считаем длину всего пути
			var length = 0;
			var lastPoint;
			var lengths = [];
			pathArray.forEach(function (point)
			{
				if (lastPoint)
				{
					var addLength = geo.lengthBetweenPoints(point, lastPoint);
					length += addLength;
					lengths.push([length, addLength]);
				}

				lastPoint = point;
			});

			var fixationPoint;

			if (length == 0)
			{
				fixationPoint = pathArray[0];
			}
			else
			{
				//ищем середину
				var middle = length / 2;
				for (var i = 0, curLength; (curLength = lengths[i]) !== undefined; ++i)
				{
					if (curLength[0] >= middle)
					{
						var vectorLength = curLength[1] - (curLength[0] - middle);
						var vector = geo.subPoints(pathArray[i + 1], pathArray[i]);
						vector = geo.mulVector(geo.normalizeVector(vector), vectorLength);
						fixationPoint = geo.addPoints(pathArray[i], vector);
						break;
					}
				}
			}

			return fixationPoint;
		},

		/**
		 * @private
		 */
		__takeOutPointFromEmptyZone: function (sourcePoint, destPoint, emptyZone)
		{
			if (emptyZone == 0)
			{
				return destPoint;
			}

			var geo = efwdRenderer.Geo;

			var vector = geo.subPoints(destPoint, sourcePoint);
			var vectorLength = geo.vectorLength(vector);
			if (vectorLength <= emptyZone)
			{
				return sourcePoint;
			}

			return geo.addPoints(sourcePoint, geo.mulVector(geo.normalizeVector(vector), vectorLength - emptyZone));
		}
	},

	arrow: {
		properties: {
			pathArray: {},
			path: { bind: "$f.getPath({pathArray})" }
		},
		children: {
			line: {},
			pointer: {}
		},
		draw: [
			{ child: "line" },
			{ child: "pointer" }
		]
	},

	connector: {

		properties: {
			//observable object
			joints: {},
			labelPosition: {},
			source: {},
			dest: {},
			sourcePoint: {},
			destPoint: {},
			pathArray: {
				bind: "$f.getArrowPathArray(" +
					"{sourcePoint} ? $geo.addPoints({x: {source|x}, y: {source|y}}, {sourcePoint}) : {source|bbox}, " +
					"{destPoint} ? $geo.addPoints({x: {dest|x}, y: {dest|y}}, {destPoint}) : {dest|bbox}, " +
					"$ser({joints}))"
			},
			labelFixationPoint: {
				bind: "$f.getConnectorLabelFixationPoint({pathArray})"
			},
			labelAbsolutePosition: {
				bind: "efwdRenderer.Geo.addPoints({labelFixationPoint}, {labelPosition})"
			}
		},

		children: {
			arrow: {},
			joint: {
				type: "positioned",
				properties: {
					x: { bind: "_.x" },
					y: { bind: "_.y" }
				}
			},
			label: {
				type: "positioned",
				properties: {
					x: { bind: "parent.{labelAbsolutePosition}.x" },
					y: { bind: "parent.{labelAbsolutePosition}.y" }
				}
			},
			labelFixation: {}
		},

		draw: [
			{ child: "arrow" },
			{ child: "joint", multiple: "{joints}" },
			{ child: "label" },
			{ child: "labelFixation" }
		]
	},

	arrowPointer: {

		properties: {
			width: {},
			length: {},
			pathArray: {},
			attrs: {}
		},

		draw: [
			{
				call: "path",
				attrs: {
					path: "=$f.getArrowPointerPath({pathArray}, {width}, {length})",
					include: ["{attrs}"]
				}
			}
		]
	}
});

