goog.provide("__symbol__.efwdConfig.efwdRenderer.commonSchema");

//noinspection JSUnusedGlobalSymbols
AppTools.config("efwdConfig.efwdRenderer.commonSchema", {

	$functions: {
		cutString: function (str, maxLength)
		{
			return str.length <= maxLength ? str : str.substr(0, maxLength - 3) + "...";
		}
	},

	Constants: {
		style: {
			emptyGif: "data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw%3D%3D"
		}
	},

	element: {
		properties: {
			bbox: { dynamic: true },
			selected: { dynamic: true },
			hovered: { dynamic: true }
		},
		calls: {
			"text": {
				attrs: {
					text: "=call.maxLength ? $f.cutString(value, call.maxLength) : value"
				},
				modifiers: {
					maxLength: { initial: false }
				}
			},
			"*": {
				modifiers: {
					enabled: { initial: true },
					bbox: { initial: false }
				}
			}
		}
	},

	positioned: {
		properties: {
			x: { initial: 0, convert: "parent && parent.types.positioned ? parent.{x} + value : value" },
			y: { initial: 0, convert: "parent && parent.types.positioned ? parent.{y} + value : value" },
			width: {},
			height: {}
		},

		calls: {
			"image,rect,text": {
				attrs: {
					x: "=(value || 0)+{x}-(call.centered ? {width}/2 : 0)",
					y: "=(value || 0)+{y}-(call.centered ? {height}/2 : 0)"
				},
				modifiers: {
					centered: { initial: false }
				}
			},
			"circle,ellipse": {
				attrs: {
					cx: "=(value || 0)+{x}",
					cy: "=(value || 0)+{y}"
				}
			}
		}
	}
});

