goog.provide("__symbol__.efwdConfig.efwdRenderer.elements.action");

//noinspection JSUnusedGlobalSymbols
AppTools.config("efwdConfig.efwdRenderer.elements", {

	$functions: {
		enlargeBBox: function (bbox, length)
		{
			return bbox ? {
				left: bbox.left - length,
				top: bbox.top - length,
				right: bbox.right + length,
				bottom: bbox.bottom + length,
				width: bbox.width + length * 2,
				height: bbox.height + length * 2
			} : null;
		}
	},

	//действие, соединяющее статусы
	action: {
		type: "connector",
		layer: "connectors",

		style: {
			shared: {
				//общие стили коннектора
				connector: {
					//цвет коннектора по-умолчанию
					defaultColor: "#707070"
				}
			},
			//точка соединения сегментов коннектора
			joint: {
				circle: {
					attrs: {
						r: 4
					}
				},
				//отрисовывать ли точку
				enabled: false
			},
			//этикетка действия
			label: {
				//имя действия
				name: {
					attrs: {
						fill: "black",
						"font-size": 12,
						"font-weight": "bold",
						y: -15,
						text: "=parent._.name"
					},
					//максимальная длина имени (остальной текст обрезается)
					maxLength: 30
				},
				//индикатор количества активностей
				activitiesIndicator: {
					y: 15
				}
			},
			//точка прикрепления этикетки действия к коннектору и линия от точки крепления до этикетки
			labelFixation: {
				shared: {
					labelBBox: "=parent.children.label.{bbox}",
					stroke: "=$.shared.connector.defaultColor"
				},
				//линия от точки крепления до этикетки
				line: {
					attrs: {
						"stroke-opacity": "=!{labelRect} || $geo.isPointInRect(parent.{labelFixationPoint}, {labelRect}) ? 0 : 1",
						stroke: "=$.labelFixation.shared.stroke",
						path: "=$f.getPath($f.getArrowPathArray(parent.{labelFixationPoint}, {labelRect}))"
					}
				},
				//прямоугольник охватывающий этикетку
				rect: {
					attrs: {
						stroke: "=$.labelFixation.shared.stroke",
						x: "={labelRect} ? {labelRect}.left : 0",
						y: "={labelRect} ? {labelRect}.top : 0",
						width: "={labelRect} ? {labelRect}.width : 0",
						height: "={labelRect} ? {labelRect}.height : 0"
					}
				},
				//отображать ли labelFixation
				enabled: false
			}
		},

		properties: {
			joints: { bind: "_.metadata.joints" },
			labelPosition: { bind: "$ser(_.metadata.labelPosition)" },
			source: { elementId: "_.from_status_id" },
			dest: { elementId: "_.to_status_id" },
			sourcePoint: { bind: "_.metadata.sourcePoint.x !== null && $ser(_.metadata.sourcePoint)" },
			destPoint: { bind: "_.metadata.destPoint.x !== null && $ser(_.metadata.destPoint)" }
		},

		children: {
			labelFixation: {
				properties: {
					labelRect: {
						bind: "$f.enlargeBBox(parent.children.label.{bbox}, 5)"
					}
				}
			}
		},

		drawChildren: {
			arrow: [
				{
					include: "actionArrow",
					child: "actionArrow",
					set: {
						pathArray: "=parent.{pathArray}"
					},
					modifiers: {
						bbox: true
					}
				}
			],
			labelFixation: [
				{
					call: "path",
					attrs: "$.labelFixation.line.attrs"
				},
				{
					call: "rect",
					attrs: "$.labelFixation.rect.attrs"
				}
			],
			label: [
				{
					call: "text",
					attrs: "$.label.name.attrs",
					modifiers: {
						maxLength: "=$.label.name.maxLength",
						bbox: true
					}
				},
				{
					include: "activitiesIndicator",
					child: "activitiesIndicator",
					set: {
						count: "=$f.getActivitiesSum('action', parent._)",
						y: "=$.activitiesIndicator.y"
					},
					modifiers: {
						bbox: true
					}
				}
			],
			joint: [
				{
					call: "circle",
					attrs: "$.joint.circle.attrs"
				}
			]
		},

		draw: [
			{
				child: "arrow",
				modifiers: {
					bbox: true
				}
			},
			{
				child: "label",
				modifiers: {
					bbox: true
				}
			},
			{
				child: "labelFixation",
				modifiers: {
					enabled: "=$.labelFixation.enabled"
				}
			},
			{
				child: "joint",
				modifiers: {
					enabled: "=$.joint.enabled",
					bbox: true
				}
			}
		]
	},

	//стрелка действия
	actionArrow: {
		type: "arrow",
		style: {
			//линия стрелки и указателя
			line: {
				attrs: {
					stroke: "=$$.action.shared.connector.defaultColor",
					"stroke-width": 1,
					"stroke-linecap": "round"
				}
			},
			//указатель стрелки
			pointer: {
				width: 14,
				length: 14,
				attrs: {
					fill: "white"
				}
			}
		},
		drawChildren: {
			line: [
				{
					call: "path",
					attrs: {
						path: "=parent.{path}",
						include: ["$.line.attrs"]
					},
					modifiers: {
						bbox: true
					}
				}
			],
			pointer: [
				{
					include: "arrowPointer",
					set: {
						width: "=$.pointer.width",
						length: "=$.pointer.length",
						pathArray: "=parent.{pathArray}",
						attrs: "=merge($.line.attrs, $.pointer.attrs)"
					}
				}
			]
		},

		draw: [
			{
				child: "line",
				modifiers: {
					bbox: true
				}
			}
		]
	}

});

