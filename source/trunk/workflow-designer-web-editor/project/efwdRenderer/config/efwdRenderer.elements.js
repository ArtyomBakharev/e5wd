goog.provide("__symbol__.efwdConfig.efwdRenderer.elements");

//noinspection JSUnusedGlobalSymbols
AppTools.config("efwdConfig.efwdRenderer.elements", {

	$functions: {
		getActivitiesSum: function (type, model)
		{
			var categories = efwdConfig.efwdCommon.data.types.$activityCategories[type];
			return categories.reduce(function (sum, category){ return sum + model.get(category).getLength() }, 0);
		}
	},

	$layers: [
		"background",
		"states",
		"connectors"
	],

	//индикатор количества активностей
	activitiesIndicator: {
		type: "positioned",

		style: {
			//текст: количество активностей
			count: {
				attrs: {
					"font-size": 13,
					text: "='(' + {count} + ')'"
				}
			}
		},

		properties: {
			count: {}
		},

		draw: [
			{
				call: "text",
				attrs: "$.count.attrs"
			}
		]
	}
});

