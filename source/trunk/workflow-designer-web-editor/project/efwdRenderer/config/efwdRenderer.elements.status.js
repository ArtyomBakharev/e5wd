goog.provide("__symbol__.efwdConfig.efwdRenderer.elements.status");

//noinspection JSUnusedGlobalSymbols
AppTools.config("efwdConfig.efwdRenderer.elements", {

	//статус
	status: {
		type: "positioned",

		layer: "states",

		style: {
			shared: {
				//общие свойства прямоугольника статуса
				box: {
					width: 145,
					height: 70,
					r: 10
				}
			},
			//имя статуса
			title: {
				attrs: {
					fill: "black",
					"font-size": 12,
					"font-weight": "bold",
					y: -10,
					text: "=_.name"
				},
				maxLength: 20
			},
			//индикатор количества активностей
			activitiesIndicator: {
				y: 15
			},
			//индикатор начального статуса
			initialStatusIndicator: {
				circle: {
					attrs: {
						r: 10,
						cx: "=-parent.{width}/2+15",
						cy: "=parent.{height}/2-15",
						fill: "black",
						"stroke-opacity": 0
					}
				}
			}
		},

		properties: {
			width: { initial: "=$.shared.box.width" },
			height: { initial: "=$.shared.box.height" },
			x: { bind: "_.metadata.x" },
			y: { bind: "_.metadata.y" },
			currentStatus: { dynamic: true, initial: false },
			initialStatus: { dynamic: true, initial: false }
		},

		id: "_.id",

		children: {
			initialStatusIndicator: {
				type: "positioned"
			}
		},

		drawChildren: {
			initialStatusIndicator: [
				{
					call: "circle",
					attrs: "$.initialStatusIndicator.circle.attrs"
				}
			]
		},

		draw: [
			{
				include: "statusBox",
				modifiers: {
					bbox: true
				},
				set: {
					currentStatus: "={currentStatus}"
				}
			},
			{
				call: "text",
				attrs: "$.title.attrs",
				modifiers: {
					maxLength: "=$.title.maxLength"
				}
			},
			{
				include: "activitiesIndicator",
				child: "activitiesIndicator",
				set: {
					count: "=$f.getActivitiesSum('status', _)",
					y: "=$.activitiesIndicator.y"
				}
			},
			{
				child: "initialStatusIndicator",
				modifiers: {
					enabled: "={initialStatus}"
				}
			}
		]
	},

	//прямоугольник статуса
	statusBox: {
		type: "positioned",
		style: {
			//прямоугольник
			box: {
				attrs: {
					width: "={width}",
					height: "={height}",
					fill: "={currentStatus} ? '#81DB8D' : '#7ACFF5'",
					r: "=$$.status.shared.box.r"
				}
			}
		},

		properties: {
			width: { bind: "$$.status.shared.box.width" },
			height: { bind: "$$.status.shared.box.height" },
			currentStatus: { initial: false }
		},

		draw: [
			{
				call: "rect",
				attrs: "$.box.attrs",
				modifiers: {
					bbox: true,
					centered: true
				}
			}
		]
	}

});

