goog.provide("efwdRenderer.Element");

efwdRenderer.Element = AppTools.createClass({

	statics: {
		__ATTRS_AFFECTS_ON_BBOX: {
			cx: true,
			cy: true,
			"font-size": true,
			"font-weight": true,
			height: true,
			path: true,
			r: true,
			text: true,
			width: true,
			x: true,
			y: true
		}
	},

	/**
	 * @param {Object} schemaModel
	 */
	construct: function (schemaModel)
	{
		this.__model = schemaModel;
		this.__subscriptions = [];
		this.__userData = {};

		this.onDestroy = new Observer();
		this.onBBoxSet = new Observer();
		/**
		 * @param {Array} collection
		 * @param {efwdRenderer.Element} element
		 */
		this.onAddChildElement = new Observer();

		/**
		 * @param {String} child
		 * @param {efwdRenderer.Element} element
		 * @type {Observer}
		 */
		this.onRenderChildElement = new Observer();

		this.__model.drawn = this;

		//events
		this.__model.onRemoved.subscribe(function (){ this.__destroy() }, this);
	},

	members: {

		/**
		 * @param {String} [layer=null]
		 */
		bringToFront: function (layer)
		{
			this.__manager.insertElementToLayer(this, layer || this.__model.layer, true);
		},

		/**
		 * @param [withModel=true]
		 */
		destroy: function (withModel)
		{
			if (withModel === undefined || withModel)
			{
				this.__model.remove();
			}
			else
			{
				this.__destroy();
				this.__model.draw.forEach(function (drawItem)
				{

					if (drawItem.unsubscriber)
					{
						drawItem.unsubscriber();
					}

					//удаляем дочерние объекты без уничтожения их моделей
					if (drawItem.element)
					{
						drawItem.element.destroy(false);
					}

					if (drawItem.elements)
					{
						drawItem.elements.getValue().forEach(function (x){ x.destroy(false) });
					}
				});

				this.__subscriptions.forEach(function (x){ x[0].unsubscribe(x[1]) });
			}
		},

		/**
		 * @return {Raphael.Set}
		 */
		getAllElementsSet: function ()
		{
			return this.__getElementsSet();
		},

		getBBox: function ()
		{
			return this.__model.getProperty("bbox");
		},

		getBottomElement: function ()
		{
			var elementSet = this.getAllElementsSet();
			return elementSet[0];
		},

		/**
		 * @param name
		 * @return {efwdRenderer.Element}
		 */
		getChild: function (name)
		{
			var child = this.__model.elements[name];
			return child.drawn || child.getValue().map(function (x){ return x.drawn });
		},

		/**
		 * @return {Raphael.Set}
		 */
		getElementsSet: function ()
		{
			return this.__getElementsSet(true);
		},

		getModel: function()
		{
			return this.__model;
		},

		getProperty: function (name)
		{
			return this.__model.getProperty(name);
		},

		getTopElement: function ()
		{
			var elementSet = this.getAllElementsSet();
			return elementSet[elementSet.length - 1];
		},

		getUserData: function (name)
		{
			return this.__userData[name];
		},

		hide: function ()
		{
			if (this.__hidden)
			{
				return;
			}

			this.getAllElementsSet().forEach(function (element)
			{
				element.data("oldOpacity", element.attr("opacity"));
				element.attr("opacity", 0);
			});

			this.__hidden = true;
		},

		/**
		 * @return {Boolean}
		 */
		isDestroyed: function ()
		{
			return !!this.__destroyed;
		},

		isOfType: function (type)
		{
			return !!this.__model.scope.types[type];
		},

		render: function ()
		{
			//draw element
			this.__model.draw.forEach(function (drawItem, index){ this.__setUpDrawItem(drawItem, index); }, this);

			this.__calcBBox(false);

			//insertToLayer
			if (!this.__model.parent)
			{
				this.__manager.insertElementToLayer(this, this.__model.layer);
			}
		},

		setProperty: function (name, value)
		{
			this.__model.setProperty(name, value);
		},

		/**
		 * @param {String} [layer=null]
		 */
		sendToBack: function (layer)
		{
			this.__manager.insertElementToLayer(this, layer || this.__model.layer, false);
		},

		/**
		 * @param {efwdRenderer.Factory} factory
		 */
		setFactory: function (factory)
		{
			this.__factory = factory;
		},

		/**
		 * @param {efwdRenderer.Manager} manager
		 */
		setManager: function (manager)
		{
			this.__manager = manager;
			this.__paper = manager.getPaper();
		},

		setUserData: function (name, value)
		{
			this.__userData[name] = value;
		},

		show: function ()
		{
			if (!this.__hidden)
			{
				return;
			}

			this.getAllElementsSet().forEach(function (element)
			{
				element.attr("opacity", element.data("oldOpacity"));
			});

			this.__hidden = false;
		},

		/**
		 * @private
		 */
		__calcBBox: function (fireEvent)
		{
			var left = Number.MAX_VALUE;
			var top = Number.MAX_VALUE;
			var right = -Number.MAX_VALUE;
			var bottom = -Number.MAX_VALUE;

			var wasCorrection = false;

			function correctResultBBox(bbox)
			{
				if (bbox.left < left)
				{
					left = bbox.left;
				}
				if (bbox.top < top)
				{
					top = bbox.top;
				}
				if (bbox.right > right)
				{
					right = bbox.right;
				}
				if (bbox.bottom > bottom)
				{
					bottom = bbox.bottom;
				}

				wasCorrection = true;
			}

			this.__model.draw.forEach(function (drawItem)
			{
				if (!drawItem.modifiers.bbox || !drawItem.modifiers.enabled)
				{
					return;
				}

				if (drawItem.drawn)
				{
					correctResultBBox(this.__getDrawnElementBBox(drawItem.drawn));
				}
				else if (drawItem.element)
				{
					correctResultBBox(drawItem.element.drawn.getBBox());
				}
				else if (drawItem.elements)
				{
					drawItem.elements.getValue().forEach(function (element){ correctResultBBox(element.drawn.getBBox()) });
				}

			}, this);

			if (!wasCorrection)
			{
				left = right = this.__model.scope.types.positioned ? this.__model.getProperty("x") : 0;
				top = bottom = this.__model.scope.types.positioned ? this.__model.getProperty("y") : 0;
			}

			var bbox = {left: left, top: top, width: right - left, height: bottom - top, right: right, bottom: bottom};

			if (!this.__bbox || !efwdRenderer.Geo.rectsEquals(this.__bbox, bbox))
			{
				this.__bbox = bbox;
				this.__model.setProperty("bbox", bbox);
				if (fireEvent)
				{
					this.onBBoxSet.fire(bbox);
				}
			}

			this.__bbox = bbox;
		},

		/**
		 * @private
		 */
		__destroy: function ()
		{
			delete this.__model.drawn;

			//проходимся по всем графическим элементам, которые отрисованы непосредственно текущим объектом и уничтожаем их
			//остальные будут уничтожены автоматически при уничтожении их моделей
			this.__model.draw.forEach(function (drawItem)
			{
				if (drawItem.drawn)
				{
					drawItem.drawn.remove();
					delete drawItem.drawn;
				}
			});

			this.__destroyed = true;

			this.onDestroy.fire();
		},

		/**
		 * @private
		 */
		__drawItem: function (item)
		{
			var drawn = item.drawn = this.__paper[item.call]();
			drawn.$$actualValues = {x: 0, y: 0, cx: 0, cy: 0, width: 0, height: 0};
			drawn.$$bbox = {left: 0, top: 0, bottom: 0, right: 0, width: 0, height: 0};
			var listeners = [];

			if (item.attrs)
			{
				for (var attrName in item.attrs)
				{
					if (item.attrs.hasOwnProperty(attrName))
					{
						var attrValue = item.attrs[attrName];

						if (attrValue && attrValue instanceof efwdRenderer.ValueObserver)
						{
							this.__drawItemAttr(drawn, attrName, attrValue.getValue(), item.modifiers.bbox, true);

							(function (attrValue, attrName)
							{
								var id = attrValue.subscribe(function (value)
								{
									if (!drawn.node)
									{
										return;
									}

									var bboxChanged = this.__drawItemAttr(drawn, attrName, value, item.modifiers.bbox);
									if (bboxChanged)
									{
										this.__calcBBox(true);
									}
								}, this);

								listeners.push([attrValue, id]);

							}).call(this, attrValue, attrName);
						}
						else
						{
							this.__drawItemAttr(drawn, attrName, attrValue, item.modifiers.bbox, true);
						}
					}
				}
			}

			item.unsubscriber = function ()
			{
				listeners.forEach(function (x){ x[0].unsubscribe(x[1]) });
				delete item.unsubscriber;
			};

			return function ()
			{
				item.unsubscriber();
				drawn.remove();
				delete item.drawn;
			}
		},

		__drawItemAttr: function (drawn, name, value, checkBBox, now)
		{
			drawn.$$actualValues[name] = value;
			if (name == "path")
			{
				value = value && (value.path || value);
			}

			var textMoved = drawn.type == "text" && (name == "x" || name == "y");

			if (now || (drawn.type == "text" && !textMoved))
			{
				drawn.attr(name, value);
			}
			else
			{
				this.__manager.addToDrawQueue(drawn, name, value);
			}

			if (textMoved)
			{
				if (name == "x")
				{
					drawn.$$bbox.left = value - drawn.$$bbox.width / 2;
					drawn.$$bbox.right = drawn.$$bbox.left + drawn.$$bbox.width;
				}
				else
				{
					drawn.$$bbox.top = value - drawn.$$bbox.height / 2;
					drawn.$$bbox.bottom = drawn.$$bbox.top + drawn.$$bbox.height;
				}

				return true;
			}
			else if (checkBBox && efwdRenderer.Element.__ATTRS_AFFECTS_ON_BBOX[name])
			{
				return drawn.$$bboxChanged = true;
			}

			return false;
		},

		/**
		 * @private
		 */
		__drawMultipleItem: function (item, index)
		{
			this.__drawMultipleItemElements(item, item.elements.getValue());

			var listener = item.elements.subscribe(function (value, data)
			{
				var subelements;

				//при добавлении нового элемента рисуем его
				if (data.insert.length)
				{
					subelements = this.__drawMultipleItemElements(item, data.insert);
					this.__insertDrawItem(item, index);
				}

				if (item.modifiers.bbox && (data.insert.length || data.remove.length))
				{
					this.__calcBBox(true);
				}

				//уведомляем о добавлении новых элементов
				if (subelements && item.child)
				{
					subelements.forEach(function (x){ this.onAddChildElement.fire(item.child, x) }, this);
				}

			}, this);

			item.unsubscriber = function ()
			{
				item.elements.unsubscribe(listener);
				delete item.unsubscriber;
			};

			return function ()
			{
				item.unsubscriber();
				item.elements.getValue().forEach(function (x){ x.destroy(false) });
			}
		},

		/**
		 * @private
		 */
		__drawMultipleItemElements: function (drawItem, elements)
		{
			return elements.map(function (element)
			{
				var drawn = this.__factory.createSubElement(element);
				drawn.render();
				this.__processSubElement(drawItem, drawn);

				return drawn;
			}, this);
		},

		/**
		 * @private
		 */
		__getDrawItemAllElementsSet: function (item)
		{
			if (item.drawn)
			{
				return this.__paper.set(item.drawn);
			}

			if (item.element)
			{
				return item.element.drawn.getAllElementsSet();
			}

			if (item.elements)
			{
				var st = this.__paper.set();
				item.elements.getValue().forEach(function (element)
				{
					element.drawn.getAllElementsSet().forEach(function (drawnElement)
					{
						st.push(drawnElement);
					});
				});

				return st;
			}
		},

		/**
		 * @param drawn
		 * @private
		 */
		__getDrawnElementBBox: function (drawn)
		{
			if (drawn.$$bboxChanged)
			{
				var values = drawn.$$actualValues;
				var bbox = {};
				switch (drawn.type)
				{
					case "rect":
						bbox = {
							left: values.x,
							top: values.y,
							width: values.width,
							height: values.height
						};
						break;

					case "circle":
						bbox = {
							left: values.cx - values.width / 2,
							top: values.cy - values.height / 2,
							width: values.r,
							height: values.r
						};
						break;

					case "path":
						bbox = values.path && efwdRenderer.Helper.getPathBBox(values.path.array);
						break;

					case "text":
						bbox = drawn.getBBox();
						bbox.left = bbox.x;
						bbox.top = bbox.y;
						break;
				}

				if (drawn.type != "path")
				{
					bbox.right = bbox.left + bbox.width;
					bbox.bottom = bbox.top + bbox.height;
				}

				if (bbox)
				{
					drawn.$$bbox = bbox;
				}
				drawn.$$bboxChanged = false;
			}

			return drawn.$$bbox;
		},

		/**
		 * @param withoutChild
		 * @return {Raphael.Set}
		 * @private
		 */
		__getElementsSet: function (withoutChild)
		{
			var st = this.__paper.set();
			this.__model.draw.forEach(function (drawItem)
			{
				if ((withoutChild && drawItem.child) || !drawItem.modifiers.enabled)
				{
					return;
				}

				var itemSet = this.__getDrawItemAllElementsSet(drawItem);
				st.append(itemSet);
			}, this);

			return st;
		},

		/**
		 * @private
		 */
		__getNearestDrawnElement: function (index, backward)
		{
			for (var i = index + (backward ? -1 : 1), drawItem; drawItem = this.__model.draw[i]; backward ? --i : ++i)
			{
				if (!drawItem.modifiers.enabled)
				{
					continue;
				}

				if (drawItem.drawn)
				{
					return drawItem.drawn;
				}

				if (drawItem.element)
				{
					return backward ? drawItem.element.drawn.getTopElement() : drawItem.element.drawn.getBottomElement();
				}

				if (drawItem.elements)
				{
					var elements = drawItem.elements.getValue();
					if (elements.length > 0)
					{
						return elements[backward ? elements.length - 1 : 0].drawn[backward ? "getTopElement" : "getBottomElement"]();
					}
				}
			}

			return null;
		},

		/**
		 * @private
		 */
		__insertDrawItem: function (item, index)
		{
			var prevDrawnElement = this.__getNearestDrawnElement(index, true);
			var nextDrawnElement;
			if (!prevDrawnElement)
			{
				nextDrawnElement = this.__getNearestDrawnElement(index, false);
			}

			var elsSet = this.__getDrawItemAllElementsSet(item);
			if (prevDrawnElement)
			{
				elsSet.insertAfter(prevDrawnElement);
			}
			else
			{
				elsSet.insertBefore(nextDrawnElement);
			}
		},

		/**
		 * @private
		 */
		__processSubElement: function (drawItem, element)
		{
			if (drawItem.modifiers.bbox)
			{
				element.onBBoxSet.subscribe(function (){ this.__calcBBox(true) }, this);
			}
		},

		/**
		 * @private
		 */
		__renderDrawItem: function (item, insertAt, insertElement)
		{
			var destroyer;

			if (item.call)
			{
				destroyer = this.__drawItem(item);
			}
			else if (item.element)
			{
				var drawn = this.__factory.createSubElement(item.element);
				drawn.render();
				this.__processSubElement(item, drawn);
				destroyer = function ()
				{
					drawn.destroy(false);
				};

				this.onRenderChildElement.fire(item.child, drawn);
			}
			else if (item.elements)
			{
				destroyer = this.__drawMultipleItem(item, insertAt);
			}

			if (insertElement)
			{
				this.__insertDrawItem(item, insertAt);
			}

			return destroyer;
		},

		/**
		 * @private
		 */
		__setUpDrawItem: function (drawItem, index)
		{
			var bbox = drawItem.modifiers.bbox;

			if (drawItem.callModifiers.enabled)
			{
				if (drawItem.callModifiers.enabled instanceof efwdRenderer.ValueObserver)
				{
					var destroyer = null;
					var first = true;

					//уничтожаем объект при отключении модели и создаём при включении
					var listener = drawItem.callModifiers.enabled.subscribeAndInit(function (value)
					{

						if (value && !destroyer)
						{
							destroyer = this.__renderDrawItem(drawItem, index, !first);
							bbox && !first && this.__calcBBox(true);
						}
						else if (!value && destroyer)
						{
							destroyer();
							destroyer = null;
							bbox && !first && this.__calcBBox(true);
						}

						first = false;
					}, this);

					this.__subscriptions.push([drawItem.callModifiers.enabled, listener]);
				}
				else
				{
					this.__renderDrawItem(drawItem, index, false);
				}
			}
		}
	}

});
