goog.provide("efwdRenderer.Factory");
goog.require("efwdRenderer.Element");

efwdRenderer.Factory = AppTools.createClass({

	/**
	 * @param {efwdRenderer.Manager} manager
	 */
	construct: function (manager)
	{
		this.__manager = manager;
		this.__schemaCompiler = manager.getSchemaCompiler();
	},

	members: {

		/**
		 * @param {String} type
		 * @param {Object} [model=null]
		 * @return {efwdRenderer.Element}
		 */
		createElement: function (type, model, properties)
		{
			var schemaModel = this.__schemaCompiler.compileSchemaElement(type, model, properties ? {propertiesModel: properties} : null);

			var element = new efwdRenderer.Element(schemaModel);
			element.setManager(this.__manager);
			element.setFactory(this);

			return element;
		},

		createSubElement: function (schemaModel)
		{
			var element = new efwdRenderer.Element(schemaModel);
			element.setManager(this.__manager);
			element.setFactory(this);
			return element;
		}
	}

});
