goog.provide("efwdRenderer.Geo");

/**
 * Набор вспомогательных функций для работы с геометрическими объектами
 */
efwdRenderer.Geo = {

	OUT_OF_CANVAS_POINT: {x: -500, y: -500},
	CANVAS_BIG_NUMBER: 1000000000000,
	EPSILON: 0.0000000000001,
	ZERO_BBOX: {left: 0, top: 0, right: 0, bottom: 0, width: 0, height: 0},

	/**
	 * @param bbox
	 * @param point
	 */
	addPointToBBox: function(bbox, point)
	{
		return {
			left: bbox.left + point.x,
			top: bbox.top + point.y,
			right: bbox.right + point.x,
			bottom: bbox.bottom + point.y,
			width: bbox.width,
			height: bbox.height
		};
	},

	/**
	 * Прибавление двух точек
	 * @param {Object} p1
	 * @param {Object} p2
	 * @return {Object}
	 */
	addPoints: function (p1, p2)
	{
		return {
			x: p1.x + p2.x,
			y: p1.y + p2.y
		};
	},

	dotProduct: function (a, b)
	{
		return a.x * b.x + a.y * b.y;
	},

	/**
	 * Деление вектора на число
	 * @param {Object} v
	 * @param {Number} x
	 * @return {Object}
	 */
	divVector: function (v, x)
	{
		return {
			x: v.x / x,
			y: v.y / x
		};
	},

	findNearestPathSegment: function (pathArray, point)
	{
		var minDistance = Number.MAX_VALUE;
		var segmentIndex = -1;
		var last;

		pathArray.forEach(function (current, index)
		{
			if (last)
			{
				var distance = this.getDistanceToSegment(point, last, current);
				if (distance < minDistance)
				{
					segmentIndex = index - 1;
					minDistance = distance;
				}
			}

			last = current;
		}, this);

		return segmentIndex;
	},

	getDistanceToLine: function (point, lineA, lineB)
	{
		return Math.abs(
			((lineA.y - lineB.y) * point.x + (lineB.x - lineA.x) * point.y + (lineA.x * lineB.y - lineB.x * lineA.y))
				/ this.lengthBetweenPoints(lineA, lineB)
		);
	},

	getDistanceToSegment: function (point, segmentA, segmentB)
	{
		var v = this.subPoints(segmentB, segmentA);
		var w = this.subPoints(point, segmentA);

		var c1 = this.dotProduct(w, v);
		if (c1 <= 0)
		{
			return this.lengthBetweenPoints(point, segmentA);
		}

		var c2 = this.dotProduct(v, v);
		if (c2 <= c1)
		{
			return this.lengthBetweenPoints(point, segmentB);
		}

		var b = c1 / c2;
		var Pb = this.addPoints(segmentA, this.mulVector(v, b));
		return this.lengthBetweenPoints(point, Pb);
	},

//	isNormalFromPointToSegmentExists: function(point, segmentA, segmentB)
//	{
//
//	},

	/**
	 * @param {Object} innerRect
	 * @param {Object} outerRect
	 */
	isRectCoveredByRect: function(innerRect, outerRect)
	{
		return innerRect.left > outerRect.left
			&& innerRect.left + innerRect.width < outerRect.left + outerRect.width
			&& innerRect.top > outerRect.top
			&& innerRect.top + innerRect.height < outerRect.top + outerRect.height;
	},

	/**
	 * @param {Object} a
	 * @param {Object} b
	 * @param {Number} [gap=undefined]
	 */
	isPointsEquals: function(a, b, gap)
	{
		if (gap === undefined)
		{
			gap =  this.EPSILON;
		}

		return Math.abs(a.x - b.x) < gap && Math.abs(a.y - b.y) < gap;
	},

	/**
	 * @param {Object} point
	 * @param {Object} rect
	 * @param {Number} [gap=0]
	 * @return {Boolean}
	 */
	isPointInRect: function (point, rect, gap)
	{
		gap = gap || 0;
		return point.x >= rect.left - gap && point.x <= rect.left + rect.width + gap
			&& point.y >= rect.top - gap && point.y <= rect.top + rect.height + gap;
	},

	/**
	 * @param {Object} p1
	 * @param {Object} p2
	 * @return {Number}
	 */
	lengthBetweenPoints: function (p1, p2)
	{
		var vector = this.subPoints(p1, p2);
		return this.vectorLength(vector);
	},

	/**
	 * Перемножение вектора на число
	 * @param {Object} v
	 * @param {Number} x
	 * @return {Object}
	 */
	mulVector: function (v, x)
	{
		return {
			x: v.x * x,
			y: v.y * x
		};
	},

	/**
	 * Вектор единичной длины
	 * @param {Object} v
	 * @return {Object}
	 */
	normalizeVector: function (v)
	{
		return this.divVector(v, this.vectorLength(v));
	},

	/**
	 * Ортоганальный вектор
	 * @param {Object} v
	 * @param {Boolean} [clockWise=false]
	 * @return {Object}
	 */
	ortVector: function (v, clockWise)
	{
		//noinspection JSSuspiciousNameCombination
		return clockWise ? {x: v.y, y: -v.x} : {x: -v.y, y: v.x};
	},

	/**
	 * Пересечение луча и прямоугольника
	 * @param {Object} rect
	 * @param {Object} p1
	 * @param {Object} p2
	 * @return {Object[]}
	 */
	rectAndRayIntersection: function (rect, p1, p2)
	{
		//удаляем p2 от p1 для эмуляции луча (todo: сделать реальный алгоритм)
		var v = this.subPoints(p2, p1);
		//очень длинный вектор
		v = this.mulVector(this.normalizeVector(v), this.CANVAS_BIG_NUMBER);
		p2 = this.addPoints(p1, v);
		return this.rectAndSegmentIntersection(rect, p1, p2);
	},

	/**
	 * Пересечение отрезка и прямоугольника
	 * @param {Object} rect
	 * @param {Object} p1
	 * @param {Object} p2
	 * @return {Object[]}
	 */
	rectAndSegmentIntersection: function (rect, p1, p2)
	{
		var gap = 1;
		var result = [];
		var vars = [
			[
				{x: rect.left - gap, y: rect.top},
				{x: rect.left + rect.width + gap, y: rect.top}
			],
			[
				{x: rect.left, y: rect.top - gap},
				{x: rect.left, y: rect.top + rect.height + gap}
			],
			[
				{x: rect.left + rect.width, y: rect.top - gap},
				{x: rect.left + rect.width, y: rect.top + rect.height + gap}
			],
			[
				{x: rect.left - gap, y: rect.top + rect.height},
				{x: rect.left + rect.width + gap, y: rect.top + rect.height}
			]
		];

		for (var i = 0, variant; variant = vars[i++];)
		{
			var intersection = this.segmentsIntersection(p1, p2, variant[0], variant[1]);
			if (intersection)
			{
				result.push(intersection);
			}
		}
		return result;
	},

	rectsEquals: function (rect1, rect2)
	{
		return rect1.left == rect2.left && rect1.top == rect2.top && rect1.width == rect2.width && rect1.height == rect2.height;
	},

	/**
	 * Пересечение отрезков
	 * @param {Object} start1
	 * @param {Object} end1
	 * @param {Object} start2
	 * @param {Object} end2
	 * @return {Object|Boolean}
	 */
	segmentsIntersection: function (start1, end1, start2, end2)
	{
		var dir1 = this.subPoints(end1, start1);
		var dir2 = this.subPoints(end2, start2);

		//считаем уравнения прямых проходящих через отрезки
		var a1 = -dir1.y;
		var b1 = +dir1.x;
		var d1 = -(a1 * start1.x + b1 * start1.y);

		var a2 = -dir2.y;
		var b2 = +dir2.x;
		var d2 = -(a2 * start2.x + b2 * start2.y);

		//подставляем концы отрезков, для выяснения в каких полуплоскотях они
		var seg1_line2_start = a2 * start1.x + b2 * start1.y + d2;
		var seg1_line2_end = a2 * end1.x + b2 * end1.y + d2;

		var seg2_line1_start = a1 * start2.x + b1 * start2.y + d1;
		var seg2_line1_end = a1 * end2.x + b1 * end2.y + d1;

		//если концы одного отрезка имеют один знак, значит он в одной полуплоскости и пересечения нет.
		if (seg1_line2_start * seg1_line2_end >= 0 || seg2_line1_start * seg2_line1_end >= 0)
		{
			return false;
		}

		var u = seg1_line2_start / (seg1_line2_start - seg1_line2_end);
		return this.addPoints(start1, this.mulVector(dir1, u));
	},

	/**
	 * Вычитаение двух точек
	 * @param {Object} p1
	 * @param {Object} p2
	 * @return {Object}
	 */
	subPoints: function (p1, p2)
	{
		return {
			x: p1.x - p2.x,
			y: p1.y - p2.y
		};
	},

	/**
	 * Длина вектора
	 * @param {Object} v
	 * @return {Number}
	 */
	vectorLength: function (v)
	{
		return Math.sqrt(v.x * v.x + v.y * v.y);
	}//,

//	vectorsAngleCos: function(v1, v2)
//	{
//		return this.dotProduct(v1, v2) / Math.sqrt ((x1*x1 + y1*y1) * (x2*x2 + y2*y2));
//	}

};
