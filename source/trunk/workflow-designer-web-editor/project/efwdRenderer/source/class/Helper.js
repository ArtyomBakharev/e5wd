goog.provide("efwdRenderer.Helper");

/**
 * Вспомагательные функции для графических элементов
 */
efwdRenderer.Helper = {

	/**
	 * На вход принимает хэш с элементами Raphael.Element, возвращает составленный из них Raphael.Set
	 * @param {Object} elements
	 * @param {Raphael.Paper} paper
	 * @return {Raphael.Set}
	 */
	elementsToSet: function (elements, paper)
	{
		var st = paper.set();
		for (var name in elements)
		{
			if (elements.hasOwnProperty(name))
			{
				st.push(elements[name]);
			}
		}

		return st;
	},

	getBounds: function(elements)
	{
		var right = -Infinity;
		var bottom = -Infinity;
		var top = Infinity;
		var left = Infinity;

		elements.forEach(function(element)
		{
			var bbox = element.getBBox();
			if (bbox.right > right)
			{
				right = bbox.right;
			}

			if (bbox.bottom > bottom)
			{
				bottom = bbox.bottom;
			}

			if (bbox.left < left)
			{
				left = bbox.left;
			}

			if (bbox.top < top)
			{
				top = bbox.top;
			}
		});

		return {
			right: right,
			bottom: bottom,
			top: top,
			left: left,
			width: right - left,
			height: bottom - top
		};
	},

	getPathBBox: function(pathArray)
	{
		if (!pathArray)
		{
			return null;
		}

		var right = -Infinity;
		var bottom = -Infinity;
		var top = Infinity;
		var left = Infinity;

		pathArray.forEach(function(point)
		{
			if (point.x > right)
			{
				right = point.x;
			}

			if (point.y > bottom)
			{
				bottom = point.y;
			}

			if (point.x < left)
			{
				left = point.x;
			}

			if (point.y < top)
			{
				top = point.y;
			}
		});

		return {
			right: right,
			bottom: bottom,
			top: top,
			left: left,
			width: right - left,
			height: bottom - top
		};
	},

	isValidPoint: function(point)
	{
		return point && (point.x == 0 || point.x) && (point.y == 0 || point.y);
	},

	/**
	 * Объединяет несколько множеств элементов в одно
	 * @param {Raphael.Paper} paper
	 * @param {Raphael.Set} varargs
	 * @return {Raphael.Set}
	 */
	mergeSets: function (paper, varargs)
	{
		var result = paper.set();
		for (var i = 1, st; st = arguments[i++];)
		{
			st.forEach(function (element){ result.push(element); });
		}

		return result;
	},

	/**
	 * Преобразует массив точек в строку - путь для Raphael.Paper#path
	 * @param {Object[]} points
	 * @return {String}
	 */
	pointsToPath: function (points)
	{
		if (!points || points.length < 2)
		{
			return false;
		}

		var path = "";
		for (var coor, i = 0; coor = points[i]; ++i)
		{
			if (!coor || isNaN(coor.x) || isNaN(coor.y))
			{
				return false;
			}

			path += (i == 0 ? "M" : "L") + coor.x + " " + coor.y;
		}

		return path;
	},

	requestAnimationFrame: function(func, elem)
	{
		if (!this.__requestAnimationFrame)
		{
			this.__requestAnimationFrame = (function(){
				return  window.requestAnimationFrame    ||
					window.webkitRequestAnimationFrame  ||
					window.mozRequestAnimationFrame     ||
					window.oRequestAnimationFrame       ||
					window.msRequestAnimationFrame      ||
					function(/* function */ callback, /* DOMElement */ element){
						window.setTimeout(callback, 1000 / 60);
					};
			})();
		}

		return this.__requestAnimationFrame.call(window, func, elem);
	}

};
