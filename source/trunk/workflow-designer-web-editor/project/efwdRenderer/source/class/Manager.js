goog.provide("efwdRenderer.Manager");

efwdRenderer.Manager = AppTools.createClass({

	/**
	 * @param {efwdRenderer.SchemaCompiler} schemaCompiler
	 */
	construct: function (schemaCompiler)
	{
		this.__schemaCompiler = schemaCompiler;
		this.__schemaCompiler.registerSchemaPart(efwdConfig.efwdRenderer.commonSchema);
		this.__schemaCompiler.registerSchemaPart(efwdConfig.efwdRenderer.elements);

		this.__elements = [];
		this.__elementsRegistry = {};
		this.__elementsIndexesCache = {};
//		this.__drawQueue = {};

//		qx.util.TimerManager.getInstance().start(this.__drawChanges, 1, this);
//		setInterval(this.__drawChanges.bind(this), 1);

		this.__drawChangesBinded = this.__drawChanges.bind(this);
	},

	members: {

		addToDrawQueue: function (element, attr, value)
		{
			if (!this.__drawQueue)
			{
				this.__drawQueue = {};
				efwdRenderer.Helper.requestAnimationFrame(this.__drawChangesBinded,
					this.__paper && this.__paper.canvas);
//			this.__drawChanges.defer(this);
			}

			var hash = this.__drawQueue[element.id];
			if (!hash)
			{
				hash = this.__drawQueue[element.id] = { $element: element };
			}

			hash[attr] = value;
		},

		/**
		 * @param {efwdRenderer.Element} element
		 */
		getElementIndex: function (element)
		{
			var elementId = AppTools.getUniqueId(element);
			var index = this.__elementsIndexesCache[elementId];
			if (index === undefined)
			{
				index = -1;
				this.__elements.some(function (curElement, curIndex)
				{
					if (element == curElement)
					{
						index = curIndex;
						return true;
					}
				});

				this.__elementsIndexesCache[elementId] = index;
			}

			return index;
		},

		/**
		 * @return {Raphael.Paper}
		 */
		getPaper: function ()
		{
			return this.__paper;
		},

		/**
		 * @return {efwdRenderer.SchemaCompiler}
		 */
		getSchemaCompiler: function ()
		{
			return this.__schemaCompiler;
		},

		/**
		 * @param {efwdRenderer.Element} element
		 * @param {String} layer
		 * @param {Boolean} [append=true]
		 */
		insertElementToLayer: function (element, layer, append)
		{
			if (!this.__layers)
			{
				this.__initializeLayers();
			}

			this.__registerElement(element);

			this.__removeElement(element);
			var insertAt = this.__insertElement(element, layer, append === undefined || append);

			//вставляем элемент на канву
			var allElementsSet = element.getAllElementsSet();
			var prevElement = this.__elements[insertAt - 1];
			var nextElement = this.__elements[insertAt + 1];

			if (prevElement)
			{
				allElementsSet.insertAfter(prevElement.getTopElement());
			}
			else if (nextElement)
			{
				allElementsSet.insertBefore(nextElement.getBottomElement());
			}
		},

		/**
		 * @param {Raphael.Paper} paper
		 */
		setPaper: function (paper)
		{
			this.__paper = paper;
		},

		__drawChanges: function ()
		{
			var queue = this.__drawQueue;
			this.__drawQueue = null;
			Object.forEach(queue, function (attrs)
			{
				var element = attrs.$element;
				if (element.id)
				{
					delete attrs.$element;
					element.attr(attrs);
				}
			});
		},

		/**
		 * @private
		 */
		__initializeLayers: function ()
		{
			this.__layers = {};
			this.__layersOrder = this.__schemaCompiler.getLayers();
			this.__layersOrder.forEach(function (layer, index)
			{
				this.__layers[layer] = {start: 0, end: 0, index: index};
			}, this);
		},

		/**
		 * @private
		 */
		__registerElement: function (element)
		{
			var elementId = AppTools.getUniqueId(element);
			if (!this.__elementsRegistry[elementId])
			{
				this.__elementsRegistry[elementId] = {element: element};
				element.onDestroy.subscribe(function ()
				{
					this.__removeElement(element);
					delete this.__elementsRegistry[elementId];
				}, this);
			}
		},

		/**
		 * @private
		 */
		__removeElement: function (element)
		{
			var elementId = AppTools.getUniqueId(element);
			var elementInfo = this.__elementsRegistry[elementId];
			if (!elementInfo.inserted)
			{
				return;
			}

			var layer = elementInfo.layer;
			var layerData = this.__layers[layer];
			var index = this.__elements.indexOf(element);

			this.__elements.splice(index, 1);

			//сдвигаем текущий слой
			--layerData.end;

			//сдвигаем все следующие слои на один элемент влево
			for (var i = layerData.index + 1, curLayer; curLayer = this.__layersOrder[i]; ++i)
			{
				var curLayerData = this.__layers[curLayer];
				--curLayerData.start;
				--curLayerData.end;
			}

			elementInfo.inserted = false;
			delete elementInfo.layer;

			this.__elementsIndexesCache = {};
		},

		/**
		 * @private
		 */
		__insertElement: function (element, layer, append)
		{
			var elementId = AppTools.getUniqueId(element);
			var elementInfo = this.__elementsRegistry[elementId];

			var layerData = this.__layers[layer];

			//вставляем элемент в коллекцию
			var insertAt = append ? layerData.end : layerData.start;
			this.__elements.splice(insertAt, 0, element);

			//сдвигаем текущий слой
			++layerData.end;

			//сдвигаем все следующие слои на один элемент вправо
			for (var i = layerData.index + 1, curLayer; curLayer = this.__layersOrder[i]; ++i)
			{
				var curLayerData = this.__layers[curLayer];
				++curLayerData.start;
				++curLayerData.end;
			}

			elementInfo.inserted = true;
			elementInfo.layer = layer;

			this.__elementsIndexesCache = {};

			return insertAt;
		}
	}

});
