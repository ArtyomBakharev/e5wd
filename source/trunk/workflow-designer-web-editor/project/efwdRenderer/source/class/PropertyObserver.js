goog.provide("efwdRenderer.PropertyObserver");
goog.require("efwdRenderer.ValueObserver");

efwdRenderer.PropertyObserver = AppTools.createClass(efwdRenderer.ValueObserver, {

	construct: function (object, property)
	{
		var value = property ? object.get(property) : object;
		efwdRenderer.ValueObserver.prototype.__construct__.call(this, value);
		this.__object = object;
		this.__property = property;
		this.__event = property ? "propertyChanged" : "selfChanged";
	},

	members: {

		getValue: function ()
		{
			return this.__property ? this.__object.get(this.__property) : this.__object;
		},

		subscribe: function (listener, context)
		{
			return this.__object.addListener(this.__event, function (e)
			{
				var data = e.getData();
				if (!this.__property || data.name == this.__property)
				{
					listener.call(context || window, this.__property ? data.value : data);
				}
			}, this);
		},

		unsubscribe: function (id)
		{
			this.__object.removeListenerById(id);
		},

		fire: function ()
		{
			throw new Error("Forbidden!");
		},

		setValue: function ()
		{
			throw new Error("Forbidden!");
		}
	}

});
