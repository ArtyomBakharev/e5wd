goog.provide("efwdRenderer.SchemaCompiler");

goog.require("efwdRenderer.PropertyObserver");
goog.require("efwdRenderer.ValueObserver");

/**
 * TODO: тотальный рефакторинг и отслеживание ошибок
 * TODO: рефакторинг - блокировка обновлений свойств и аттрибутов при отключении drawItem
 */
efwdRenderer.SchemaCompiler = AppTools.createClass({

	construct: function ()
	{
		this.__schema = {
			$functions: {},
			$layers: []
		};

		this.__globalScope = {
			$f: this.__schema.$functions,
			$geo: efwdRenderer.Geo
		};

		this.__computedTypes = {};
		this.__registeredElements = {};

		var $this = this;
		this.__schemaElementClass = function (){};
		this.__schemaElementClass.prototype = {
			getProperty: function (name){ return $this.__getSchemaElementProperty(this, name) },
			setProperty: function (name, value){ return $this.__setSchemaElementProperty(this, name, value) },
			remove: function (){ return $this.removeCompiledSchemaElement(this) }
		};
	},

	members: {

		getLayers: function ()
		{
			return this.__schema.$layers;
		},

		getSchemaElementById: function (id)
		{
			return this.__registeredElements[id];
		},

		/**
		 * @param type
		 * @param model
		 * @param {Object} [options=null]
		 * @param {Object} [options.parent=null]
		 * @param {Boolean} [options.propertiesModel=null]
		 */
		compileSchemaElement: function (type, model, options)
		{
			if (!options)
			{
				options = {};
			}

			var computedSchemaType = this.__getComputedSchemaType(type);
			var schemaElement = new this.__schemaElementClass();
			AppTools.shallowCopy({
				type: type,
				layer: computedSchemaType.layer,
				properties: {},
				modelDeps: {},
				elements: {},
				scope: {
					$p: {},
					type: type,
					types: computedSchemaType.types,
					_: model,
					parent: options.parent ? options.parent.scope : null,
					children: {}
				},
				onRemoved: new efwdRenderer.ValueObserver()
			}, schemaElement);

			//связь с родителем
			if (options.parent)
			{
				schemaElement.parent = options.parent;
			}

			//использование произвольной модели, либо модели для свойств
			if (options.propertiesModel)
			{
				this.__usePropertiesModel(schemaElement, options.propertiesModel);
			}

			this.__registerSchemaElement(schemaElement);

			if (computedSchemaType.draw)
			{
				this.__compileDrawItems(schemaElement, computedSchemaType);
			}

			return schemaElement;
		},

		registerSchemaPart: function (part)
		{
//			var functions = part.$functions
//				? AppTools.shallowCopy(part.$functions, this.__schema.$functions)
//				: this.__schema.$functions;

			AppTools.deepCopy(part, this.__schema);
			if (part.$layers)
			{
				this.__schema.$layers = part.$layers.concat();
			}
//			this.__schema.$functions = functions;
		},

		removeCompiledSchemaElement: function (schema)
		{
			if (schema.id in this.__registeredElements)
			{
				delete this.__registeredElements[schema.id];
			}

			//удаляем дочерние элементы
			if (schema.draw)
			{
				schema.draw.forEach(function (drawItem)
				{
					if (drawItem.element)
					{
						drawItem.element.remove();
					}

					if (drawItem.elements)
					{
						drawItem.elements.getValue().forEach(function (element){ element.remove() });
					}
				});
			}

			schema.onRemoved.fire();
		},

		/**
		 * @param {String} key
		 * @param {Object} part
		 */
		replaceSchemaPart: function (key, part)
		{
			AppTools.pathAccess(this.__schema, key, AppTools.pathAccess.setVar, AppTools.deepCopy(part));
		},

		/**
		 * @param {Function} serializer
		 */
		setSerializer: function(serializer)
		{
			this.__globalScope.$ser = serializer;
		},

		/**
		 * @private
		 */
		__compileAttribute: function (drawItem, attrName, compiledDrawItem, schemaElement)
		{
			var attrValue = drawItem.attrs[attrName];
			var attrObserver = this.__createDependenciesObserver(attrValue, schemaElement, drawItem, compiledDrawItem);

			//если у этого аттрибута есть конвертер, применяем его
			var computedType = this.__computedTypes[schemaElement.type];
			var callExpression = AppTools.pathAccess(computedType, "calls." + drawItem.call + ".attrs." + attrName);

			if (!callExpression)
			{
				return attrObserver;
			}
			return this.__createDependenciesObserver(callExpression, schemaElement, drawItem, compiledDrawItem,
				attrObserver);
		},

		/**
		 * @private
		 */
		__compileCallModifier: function (callModifier, value, schemaElement, drawItem, compiledDrawItem)
		{
			if (!(callModifier in compiledDrawItem.callModifiers))
			{
				var observer;

				if (drawItem.modifiers && callModifier in drawItem.modifiers)
				{
					if (typeof drawItem.modifiers[callModifier] != "function")
					{
						value = drawItem.modifiers[callModifier];
					}
					else
					{
						observer = this.__createDependenciesObserver(drawItem.modifiers[callModifier], schemaElement,
							drawItem, compiledDrawItem);
						value = observer && observer instanceof efwdRenderer.ValueObserver ? observer.getValue() : observer;
					}
				}
				else
				{
					value = value.initial;
				}

				if (!observer)
				{
					observer = value;
				}

				compiledDrawItem.callModifiers[callModifier] = observer;
				compiledDrawItem.modifiers[callModifier] = value;

				if (observer && observer instanceof efwdRenderer.ValueObserver)
				{
					observer.subscribe(function (x){ compiledDrawItem.modifiers[callModifier] = x });
				}
			}
		},

		/**
		 * @private
		 */
		__compileDrawItems: function (schemaElement, computedSchemaType)
		{
			//идём по draw элементам и высчитываем зависимости аттрибутов
			schemaElement.draw = computedSchemaType.draw.map(function (drawItem)
			{
				var compiledItem = {
					call: drawItem.call,
					callModifiers: {},
					modifiers: {},
					child: drawItem.child
				};

				this.__resolveCallModifiers(schemaElement, drawItem, compiledItem);

				//резолвинг аттрибутов
				if (drawItem.attrs)
				{
					this.__resolveSchemaElementAttrs(schemaElement, computedSchemaType, compiledItem, drawItem);
				}
				//включение дочернего элемента
				else if (drawItem.include)
				{
					if (drawItem.multiple)
					{
						this.__includeMultipleChild(schemaElement, drawItem, compiledItem);
					}
					else
					{
						this.__includeSingleChild(schemaElement, drawItem, compiledItem);
					}
				}

				return compiledItem;
			}, this);
		},

		/**
		 * @param expression
		 * @param type
		 * @param [withoutModifier=false]
		 * @private
		 */
		__compileExpression: function (expression, type, withoutModifier)
		{
			//compile subitems of expression
			if (typeof expression == "object" && !Array.isArray(expression))
			{
				this.__compileObjectProperties(expression, type);
				return expression;
			}

			if (typeof expression != "string" || (!withoutModifier && expression.substr(0, 1) != "="))
			{
				return expression;
			}

			if (!withoutModifier)
			{
				expression = expression.substr(1);
			}

			//merging =merge($.style1, $.style2)
			var match;
			if (match = expression.match(/^merge\((.*)\)$/))
			{
				var resultStyle = {};
				match[1].split(",")
					.forEach(function (x)
					{
						var style = this.__findStyle(x.trim(), type);
						AppTools.deepCopy(style, resultStyle);
					}, this);
				this.__compileObjectProperties(resultStyle, type);
				return resultStyle;
			}

			//inject styles ($.someStyle or $$.someType.someStyle)
			expression = this.__injectStyles(expression, type);

			var dependencies = {};
			expression = this.__findExpressionDependencies(expression, dependencies);

			var result = new Function("globalScope,scope,call,value",
				"with(globalScope){with(scope){return " + expression + "}}");

			//если нет зависимостей, то это константа
			if (Object.keys(dependencies).length == 0)
			{
				return this.__evaluateExpression(result, type);
			}

			result.dependencies = dependencies;
			return result;
		},

		/**
		 * @private
		 */
		__compileObjectProperties: function (object, type)
		{
			for (var name in object)
			{
				if (object.hasOwnProperty(name))
				{
					object[name] = this.__compileExpression(object[name], type);
				}
			}
		},

		/**
		 * @param schemaElement
		 * @param propName
		 * @param [overload=null]
		 * @private
		 */
		__compileProperty: function (schemaElement, propName, overload)
		{
			var computedType = this.__computedTypes[schemaElement.type];
			var property = computedType.properties[propName];

			if (propName in schemaElement.properties || !property)
			{
				return;
			}

			var observer;

			if (overload && overload instanceof this.__schemaElementClass)
			{
				observer = new efwdRenderer.ValueObserver(overload.scope);
				observer.schemaElement = overload;
			}
			else if (overload !== undefined)
			{
				observer = overload;
			}
			else if (property.elementId)
			{
				var idObserver = this.__createDependenciesObserver(property.elementId, schemaElement);
				observer = new efwdRenderer.ValueObserver();
				idObserver.subscribeAndInit(function (id)
				{
					var element = this.getSchemaElementById(id);
					if (element !== observer.getValue())
					{
						observer.setValue(element && element.scope);
						observer.schemaElement = element;
					}
				}, this);
			}
			else if ("bind" in property)
			{
				observer = this.__createDependenciesObserver(property.bind, schemaElement);
			}
			else if (property.dynamic)
			{
				observer = new efwdRenderer.ValueObserver(property.initial);
			}
			else
			{
				observer = property.initial;
			}

			if ("convert" in property)
			{
				observer = this.__createDependenciesObserver(property.convert, schemaElement, null, null, observer);
			}

			schemaElement.properties[propName] = observer;
			if (observer && observer instanceof efwdRenderer.ValueObserver)
			{
				observer.subscribeAndInit(function (x){ schemaElement.scope.$p[propName] = x });
			}
			else
			{
				schemaElement.scope.$p[propName] = observer;
			}
		},

		/**
		 * @param value
		 * @param schemaElement
		 * @param [drawItem=null]
		 * @private
		 */
		__createDependenciesObserver: function (value, schemaElement, drawItem, compiledDrawItem, valueObserver)
		{
			if (typeof value != "function")
			{
				return value;
			}

			var valueObserved = valueObserver && valueObserver instanceof efwdRenderer.ValueObserver;

			var observers = this.__getObserversByDependencies(value.dependencies, schemaElement, drawItem,
				compiledDrawItem);

			//зависимость от valueObserver
			if (valueObserver instanceof efwdRenderer.ValueObserver)
			{
				observers.push(valueObserver);
			}

			var initial = this.__evaluateExpression(value, schemaElement.type, schemaElement, compiledDrawItem,
				valueObserved ? valueObserver.getValue() : valueObserver);

			//аттрибут не имеет зависимостей
			if (observers.length == 0)
			{
				return initial;
			}

			//подписываемся на изменения зависимостей
			var observer = new efwdRenderer.ValueObserver(initial);

			function changesListener()
			{
				var newValue = this.__evaluateExpression(value, schemaElement.type, schemaElement, compiledDrawItem,
					valueObserved ? valueObserver.getValue() : valueObserver);
				observer.setValue(newValue);
			}

			observers.forEach(function (observer)
			{
				var listenerId = observer.subscribe(changesListener, this);
				if (observer.unsubscribeFromOwner)
				{
					this.__setUpObserverUnsubscribing(observer.unsubscribeFromOwner, observer, listenerId);
				}
			}, this);

			return observer;
		},

		/**
		 * @param expression
		 * @param type
		 * @param [schemaElement=null]
		 * @param [compiledDrawItem=null]
		 * @param [value=null]
		 * @private
		 */
		__evaluateExpression: function (expression, type, schemaElement, compiledDrawItem, value)
		{
			var result = expression(
				this.__globalScope,
				schemaElement ? schemaElement.scope : {},
				compiledDrawItem ? compiledDrawItem.modifiers : {},
				value
			);

			return result;
		},

		/**
		 * @private
		 */
		__findExpressionDependencies: function (expression, dependencies)
		{
			//properties
			expression = expression.replace(/([^\w\d_\$\.]|^)((?:parent\.|children\.[\w\d|_\$]*\.)*)\{([\w\d|_\$]*?)\}/g,
				function (match, pre, place, inner)
				{
					var property = inner.split("|");
					AppTools.pathAccess(dependencies, (place || "") + "properties." + property.join("."),
						AppTools.pathAccess.setVar, {});
					return pre + (place || "") + "$p." + property.join(".$p.");
				});

			//model
			expression = expression.replace(/([^\w\d_\$\.]|^)((?:parent\.|children\.[\w\d|_\$]*\.)*)_(?:\.([\w\d_\$\.]*))?([^\w\d_\$\.]|$)/g,
				function (match, pre, place, path, post)
				{
					AppTools.pathAccess(dependencies,
						(place || "") + "model." + (path || efwdRenderer.SchemaCompiler.__MODEL_CHANGE_KEY),
						AppTools.pathAccess.setVar, true);

					path = path ? ".get('" + path.split(".").join("').get('") + "')" : "";
					return pre + (place || "") + "_" + path + post;
				});

			var match;
			var regex;

			//call
			regex = /(?:[^\w\d_\$\.]|^)call\.([\w\d_\$\.]*)?/g;
			while (match = regex.exec(expression))
			{
				(dependencies.call || (dependencies.call = {}))[match[1]] = true;
			}

			//value
			if (/(?:[^\w\d_\$\.]|^)value(?:[^\w\d_\$\.]|$)/.test(expression))
			{
				dependencies.value = true;
			}

			return expression;
		},

		/**
		 * @private
		 */
		__findStyle: function (styleString, type)
		{
			var match = Array.isArray(styleString)
				? styleString
				: styleString.match(new RegExp("^" + efwdRenderer.SchemaCompiler.__STYLE_PATTERN + "$"));

			if (!match)
			{
				return;
			}

			var prefix = match[2];
			var explicitType = match[3];
			var path = match[4];

			var styleObj = prefix == "$$" ? this.__schema[explicitType].style : this.__schema[type].style;
			if (prefix == "$")
			{
				path = explicitType + (path || "");
			}
			else
			{
				path = path.substr(1);
			}

			return AppTools.pathAccess(styleObj, path);
		},

		/**
		 * @private
		 */
		__getCallModifierObserver: function (callModifier, compiledDrawItem)
		{
			return compiledDrawItem.callModifiers[callModifier];
		},

		/**
		 * @private
		 */
		__getComputedSchemaType: function (type)
		{
			if (!this.__computedTypes[type])
			{
				if (!this.__schema.element)
				{
					this.__schema.element = {};
				}

				var computed = this.__computedTypes[type] = {
					types: {},
					calls: {}
				};
				computed.types = {};

				//собираем массив типов схемы
				var types = [];
				var curType = type;
				while (true)
				{
					computed.types[curType] = true;
					var schemaType = this.__schema[curType];
					this.__prepareSchemaType(curType);

					types.unshift(schemaType);

					if (curType == "element")
					{
						break;
					}
					curType = schemaType.type || "element";
				}

				//merge schema types
				types.forEach(function (schemaType)
				{
					//deep copy
//				if (schemaType.properties)
//				{
//					AppTools.deepCopy(schemaType.properties, computed.properties || (computed.properties = {}));
//				}

					//deep copy
					["properties", "children"].forEach(function (key)
					{
						if (key in schemaType)
						{
							AppTools.deepCopy(schemaType[key], computed[key] || (computed[key] = {}));
						}
					});

					//shallow copy
					["layer", "drawChildren"].forEach(function (key)
					{
						if (key in schemaType)
						{
							computed[key] = schemaType[key];
						}
					});

					//draw
					if (schemaType.draw)
					{
						if (computed.draw)
						{
							schemaType.draw.forEach(function (drawItem)
							{
								computed.draw.forEach(function (computedDrawItem)
								{
									if (drawItem.child == computedDrawItem.child)
									{
										AppTools.deepCopy(drawItem, computedDrawItem);
									}
								});
							});
						}
						else
						{
							computed.draw = schemaType.draw;
						}
					}

					//calls
					if (schemaType.calls)
					{
						for (var callTypes in schemaType.calls)
						{
							if (schemaType.calls.hasOwnProperty(callTypes))
							{
								var callItem = schemaType.calls[callTypes];
								callTypes.split(",").map(function (x){ return x.trim() })
									.forEach(function (callType)
									{
										AppTools.deepCopy(callItem,
											computed.calls[callType] || (computed.calls[callType] = {}))
									});
							}
						}
					}

					if ("id" in schemaType)
					{
						computed.id = schemaType.id;
					}
				});

				//расчёт дочерних типов
				if (computed.children)
				{
					AppTools.objForEach(computed.children, function (child, childName)
					{
						var childType = AppTools.shallowCopy(child);
						childType.draw = computed.drawChildren[childName];
						this.__schema[type + "$$" + childName] = childType;
					}, this);

					for (var i = 0, drawItem; drawItem = computed.draw[i]; ++i)
					{
						if (drawItem.child && !drawItem.include)
						{
							drawItem = AppTools.deepCopy(drawItem);
							drawItem.include = type + "$$" + drawItem.child;
							computed.draw[i] = drawItem;
						}
					}
				}
			}

			return this.__computedTypes[type];
		},

		/**
		 * @param deps
		 * @param schemaElement
		 * @param [drawItem=null]
		 * @param [compiledDrawItem=null]
		 * @param [observersOwner=null]
		 * @private
		 */
		__getObserversByDependencies: function (deps, schemaElement, drawItem, compiledDrawItem, observersOwner)
		{
			var observers = [];

			//расчёт зависимостей от свойств
			if (deps.properties)
			{
				observers = observers.concat(this.__getObserversByPropertiesDependencies(deps.properties, schemaElement,
					observersOwner));
			}

			//расчёт зависимостей от модели
			if (deps.model)
			{
				AppTools.objForEach(deps.model, function (val, modelProperty)
				{
					var observer = this.__getModelPropertyObserver(modelProperty, schemaElement);
					if (observersOwner)
					{
						observer.unsubscribeFromOwner = observersOwner;
					}
					observers.push(observer);
				}, this);
			}

			//расчёт зависимостей от модификаторов вызова
			if (deps.call)
			{
				for (var callModifier in deps.call)
				{
					if (deps.call.hasOwnProperty(callModifier))
					{
						var callModifierObserver = this.__getCallModifierObserver(callModifier, compiledDrawItem);
						if (callModifierObserver && callModifierObserver instanceof efwdRenderer.ValueObserver)
						{
							observers.push(callModifierObserver);
						}
					}
				}
			}

			//расчёт зависимостей от родителя
			if (deps.parent && schemaElement.parent)
			{
				observers = observers.concat(this.__getObserversByDependencies(deps.parent, schemaElement.parent, null,
					null, observersOwner || schemaElement));
			}

			//расчёт зависимостей от родителя
			if (deps.children)
			{
				AppTools.objForEach(deps.children, function (deps, name)
				{
					observers = observers.concat(this.__getObserversByDependencies(deps, schemaElement.elements[name],
						null,
						null, observersOwner || schemaElement));
				}, this);
			}

			return observers;
		},

		/**
		 * @param properties
		 * @param schemaElement
		 * @param [observersOwner=null]
		 * @private
		 */
		__getObserversByPropertiesDependencies: function (properties, schemaElement, observersOwner)
		{
			var observers = [];
			AppTools.objForEach(properties, function (propDep, propName)
			{
				this.__compileProperty(schemaElement, propName);
				var property = schemaElement.properties[propName];

				if (property && property instanceof efwdRenderer.ValueObserver)
				{
					observers.push(property);
					if (observersOwner)
					{
						property.unsubscribeFromOwner = observersOwner;
					}
				}

				//subelements dependencies
				if (property && property.schemaElement && Object.keys(propDep).length > 0)
				{
					observers = observers.concat(this.__getObserversByPropertiesDependencies(propDep,
						property.schemaElement, observersOwner || schemaElement));
				}
			}, this);

			return observers;
		},

		/**
		 * @private
		 */
		__getModelPropertyObserver: function (propName, schemaElement)
		{
			if (!schemaElement.modelDeps[propName])
			{
				var observer;
				var model = schemaElement.scope._;
				var props = propName == efwdRenderer.SchemaCompiler.__MODEL_CHANGE_KEY ? [] : propName.split(".");

				var parent;
				var prop;
				var current = model;
				props.forEach(function (curProp)
				{
					parent = current;
					current = current.get(curProp);
					prop = curProp;
				});

				if (current && current.addListener)
				{
					observer = new efwdRenderer.PropertyObserver(current);
				}
				else
				{
					observer = new efwdRenderer.PropertyObserver(parent, prop);
				}

				schemaElement.modelDeps[propName] = observer;
			}

			return schemaElement.modelDeps[propName];
		},

		/**
		 * @private
		 */
		__getSchemaElementProperty: function (schemaElement, name)
		{
			return schemaElement.scope.$p[name];
		},

		/**
		 * @private
		 */
		__includeMultipleChild: function (schemaElement, drawItem, compiledItem)
		{
			//исходный массив данных
			compiledItem.multiple = this.__createDependenciesObserver(drawItem.multiple, schemaElement, drawItem,
				compiledItem);

			//массив скомпилированных элементов
			var elements = schemaElement.elements[drawItem.child] = compiledItem.elements = new efwdRenderer.ValueObserver([]);
			var elementsArray = elements.getValue();

//		//обработчик изменения исходного массива данных
//		function processArrayChange(data, fireEvent)
//		{
//			var insert;
//			var remove;
//
//			switch (data.type)
//			{
//				case "remove":
//					remove = elementsArray.splice(data.start, data.items.length);
//					insert = [];
//
//					break;
//
//				case "add":
//					if (data.items === null)
//					{
//						insert = array.toArray().slice(data.start,
//							data.start + array.getLength() - elementsArray.length);
//						remove = [];
//					}
//					else
//					{
//						insert = data.items;
//						remove = elementsArray.splice(data.start, data.items.length);
//					}
//
//					insert = insert.map(function (item)
//					{
//						return this.compileSchemaElement(
//							drawItem.include, item, {
//								propertiesModel: this.__makePropertiesModel(drawItem.set, schemaElement),
//								parent: schemaElement
//							}
//						);
//					}, this);
//
//					elementsArray.splice.apply(elementsArray, [data.start, 0].concat(insert));
//
//					break;
//
//				default:
//					return;
//			}
//
//			remove.forEach(function (x)
//			{
//				this.removeCompiledSchemaElement(x)
//			}, this);
//
//			if (fireEvent)
//			{
//				elements.fire(elementsArray, {index: data.start, insert: insert, remove: remove});
//			}
//		}

			//обработчик изменения исходного массива данных
			function processArrayChange(data, fireEvent)
			{
				var insert = data.insert.map(function (item)
				{
					return this.compileSchemaElement(
						drawItem.include, item, {
							propertiesModel: this.__makePropertiesModel(drawItem.set, schemaElement),
							parent: schemaElement
						}
					);
				}, this);

				var remove = data.remove;
				if (remove.length > 0)
				{
					remove = elementsArray.slice(data.index, data.index + data.remove.length);
					remove.forEach(function (x)
					{
						this.removeCompiledSchemaElement(x)
					}, this);
				}

				elementsArray.splice.apply(elementsArray, [data.index, remove.length].concat(insert));

				if (fireEvent)
				{
					elements.fire(elementsArray, {index: data.index, insert: insert, remove: remove});
				}
			}

			var array = compiledItem.multiple.getValue();
			if (array.getLength() > 0)
			{
				processArrayChange.call(this, {index: 0, insert: array.toArray(), remove: []}, false);
			}

			var listener = array.addListener("changeArray",
				function (e){ processArrayChange.call(this, e.getData(), true) }, this);
			schemaElement.onRemoved.subscribe(function (){ array.removeListenerById(listener) });
		},

		/**
		 * @private
		 */
		__includeSingleChild: function (schemaElement, drawItem, compiledItem)
		{
			compiledItem.element = this.compileSchemaElement(
				drawItem.include, {},
				{
					propertiesModel: this.__makePropertiesModel(drawItem.set, schemaElement),
					parent: schemaElement
				}
			);

			if (drawItem.child)
			{
				compiledItem.child = drawItem.child;
				schemaElement.elements[drawItem.child] = compiledItem.element;
				schemaElement.scope.children[drawItem.child] = compiledItem.element.scope;
			}
		},

		/**
		 * @private
		 */
		__injectStyles: function (expression, type)
		{
			var $this = this;

			return expression.replace(new RegExp(efwdRenderer.SchemaCompiler.__STYLE_PATTERN, "g"),
				function (match, pre)
				{
					var style = $this.__findStyle(Array.prototype.slice.call(arguments), type);
					if (typeof style == "string" && style.substr(0, 1) == "=")
					{
						return pre + "(" + $this.__injectStyles(style.substr(1), type) + ")";
					}
					else
					{
						return pre + JSON.stringify(style);
					}
				});
		},

		/**
		 * @private
		 */
		__makePropertiesModel: function (setProperties, schemaElement)
		{
			var model = {};
			if (setProperties)
			{
				AppTools.objForEach(setProperties, function (prop, propName)
				{

					//attrs property
					if (typeof prop == "object" && !Array.isArray(prop))
					{
						var newProp = {};
						AppTools.objForEach(prop, function (value, key)
						{
							newProp[key] = this.__createDependenciesObserver(value, schemaElement);
						}, this);

						model[propName] = newProp;
					}
					else
					{
						model[propName] = this.__createDependenciesObserver(prop, schemaElement);
					}
				}, this);
			}

			return model;
		},

		/**
		 * @private
		 */
		__prepareDrawItems: function (drawItems, type)
		{
			drawItems.forEach(function (drawItem)
			{

				if (drawItem.modifiers)
				{
					this.__compileObjectProperties(drawItem.modifiers, type);
				}

				if (drawItem.attrs)
				{
					//include attrs
					if (typeof drawItem.attrs == "string")
					{
						if (drawItem.attrs.substr(0, 1) != "{")
						{
							drawItem.attrs = AppTools.deepCopy(this.__findStyle(drawItem.attrs, type));
						}
					}

					if (typeof drawItem.attrs == "object")
					{
						if (drawItem.attrs.include)
						{
							var attrsOverwrite = drawItem.attrs;

							var attrs = {};
							attrsOverwrite.include = attrsOverwrite.include.filter(function (include)
							{
								if (include.substr(0, 1) == "{")
								{
									return true;
								}

								AppTools.deepCopy(this.__findStyle(include, type), attrs);
								return false;
							}, this);

							if (attrsOverwrite.include.length == 0)
							{
								delete attrsOverwrite.include;
							}

							AppTools.deepCopy(attrsOverwrite, attrs);
							drawItem.attrs = attrs;
						}

						this.__compileObjectProperties(drawItem.attrs, type);
					}
				}

				if (drawItem.set)
				{
					this.__compileObjectProperties(drawItem.set, type);
				}

				if (drawItem.multiple)
				{
					drawItem.multiple = this.__compileExpression(drawItem.multiple, type, true);
				}

			}, this);
		},

		/**
		 * @private
		 */
		__prepareSchemaType: function (type, obj)
		{
			var schemaType = obj || this.__schema[type];
			if (schemaType.compiled)
			{
				return;
			}

			//compile properties
			if (schemaType.properties)
			{
				for (var propName in schemaType.properties)
				{
					if (schemaType.properties.hasOwnProperty(propName))
					{
						var prop = schemaType.properties[propName];

						if ("initial" in prop)
						{
							prop.initial = this.__compileExpression(prop.initial, type);
						}

						if (prop.inherit)
						{
							prop.bind = "parent.{" + propName + "}";
						}

						if (prop.bind)
						{
							prop.bind = this.__compileExpression(prop.bind, type, true);
							if (typeof prop.bind != "function")
							{
								prop.initial = prop.bind;
								delete prop.bind;
							}
						}

						if (prop.convert)
						{
							prop.convert = this.__compileExpression(prop.convert, type, true);
						}

						if (prop.elementId)
						{
							prop.elementId = this.__compileExpression(prop.elementId, type, true);
						}
					}
				}
			}

			//compile call attributes
			if (schemaType.calls)
			{
				for (var callName in schemaType.calls)
				{
					if (schemaType.calls.hasOwnProperty(callName))
					{
						var call = schemaType.calls[callName];
						if (call.attrs)
						{
							this.__compileObjectProperties(call.attrs, type);
						}
					}
				}
			}

			//compile draw
			if (schemaType.draw)
			{
				this.__prepareDrawItems(schemaType.draw, type);
			}

			//compile draw children
			if (schemaType.drawChildren)
			{
				AppTools.objForEach(schemaType.drawChildren, function (x){ this.__prepareDrawItems(x, type) }, this);
			}

			//compile children types
			if (schemaType.children)
			{
				for (var childName in schemaType.children)
				{
					if (schemaType.children.hasOwnProperty(childName))
					{
						this.__prepareSchemaType(type, schemaType.children[childName]);
					}
				}
			}

			//id
			if (schemaType.id)
			{
				schemaType.id = this.__compileExpression(schemaType.id, type, true);
			}

			schemaType.compiled = true;
		},

		/**
		 * @private
		 */
		__registerSchemaElement: function (schemaElement)
		{
			var schemaType = this.__computedTypes[schemaElement.type];
			if ("id" in schemaType)
			{
				schemaElement.id = this.__evaluateExpression(schemaType.id, schemaElement.type, schemaElement);
				this.__registeredElements[schemaElement.id] = schemaElement;
			}
		},

		/**
		 * @private
		 */
		__resolveSchemaElementAttrs: function (schemaElement, computedSchemaType, compiledItem, drawItem)
		{
			compiledItem.attrs = {};

			AppTools.objForEach(drawItem.attrs, function (attr, attrName)
			{
				if (attrName != "include")
				{
					compiledItem.attrs[attrName] = this.__compileAttribute(drawItem, attrName, compiledItem,
						schemaElement);
				}
			}, this);

			//include attrs properties
			if (drawItem.attrs.include)
			{
				drawItem.attrs.include.forEach(function (propName)
				{
					propName = propName.substr(1, propName.length - 2);
					var prop = schemaElement.scope.$p[propName];
					AppTools.objForEach(prop, function (value, key)
					{
						if (!(key in compiledItem.attrs))
						{
							compiledItem.attrs[key] = value;
						}
					});
				}, this);
			}

			//если какие-то аттрибуты присутствуют в модификаторе вызова и отсутствуют в аттрибутах, добавляем их
			var call = computedSchemaType.calls[drawItem.call];
			if (call && call.attrs)
			{
				AppTools.objForEach(call.attrs, function (value, name)
				{
					if (!(name in compiledItem.attrs))
					{
						compiledItem.attrs[name] = this.__compileAttribute(drawItem, name, compiledItem, schemaElement);
					}
				}, this);
			}
		},

		/**
		 * @private
		 */
		__resolveCallModifiers: function (schemaElement, drawItem, compiledItem)
		{
			function resolve(call)
			{
				if (call && call.modifiers)
				{
					AppTools.objForEach(call.modifiers, function (value, name)
					{
						this.__compileCallModifier(name, value, schemaElement, drawItem, compiledItem);
					}, this);
				}
			}

			var computedType = this.__computedTypes[schemaElement.type];
			drawItem.call && resolve.call(this, computedType.calls[drawItem.call]);
			resolve.call(this, computedType.calls["*"]);
		},

		/**
		 * @private
		 */
		__setSchemaElementProperty: function (schemaElement, name, value)
		{
			this.__compileProperty(schemaElement, name);
			var observer = schemaElement.properties[name];
			observer.setValue(value);
		},

		/**
		 * @private
		 */
		__setUpObserverUnsubscribing: function (schemaElement, observer, id)
		{
			schemaElement.onRemoved.subscribe(function ()
			{
				observer.unsubscribe(id);
			});
		},

		/**
		 * @private
		 */
		__usePropertiesModel: function (schemaElement, model)
		{
			AppTools.objForEach(model, function (prop, name)
			{
				this.__compileProperty(schemaElement, name, prop);
			}, this);
		}
	}

});

efwdRenderer.SchemaCompiler.__STYLE_PATTERN = "([^\\w\\d\\$_\\.]|^)(\\$\\$?)\\.([\\w\\d\\$_]*)([\\w\\d\\$_\\.]*)";
efwdRenderer.SchemaCompiler.__MODEL_CHANGE_KEY = "__model change key__";
