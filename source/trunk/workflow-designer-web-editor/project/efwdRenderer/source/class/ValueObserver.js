goog.provide("efwdRenderer.ValueObserver");

efwdRenderer.ValueObserver = AppTools.createClass(Observer, {

	construct: function (value)
	{
		this.__value = value;
		Observer.call(this);
	},

	members: {

		setValue: function (value)
		{
			if (typeof value != "object" && value === this.__value)
			{
				return;
			}

			this.__value = value;
			this.fire(value);
		},

		getValue: function ()
		{
			return this.__value;
		},

		subscribeAndInit: function (listener, context)
		{
			listener.call(context || window, this.__value);
			return this.subscribe(listener, context);
		}
	}

});
