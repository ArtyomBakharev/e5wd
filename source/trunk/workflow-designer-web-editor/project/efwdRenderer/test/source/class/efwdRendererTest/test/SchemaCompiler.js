/**
 * @extends {efwdRendererTest.test.TestCase}
 */
qx.Class.define("efwdRendererTest.test.SchemaCompiler", {
	extend: efwdRendererTest.test.TestCase,
	include: library.dev.MMock,

	members: {
		setUp: function ()
		{
			this.__schemaCompiler = new efwdRenderer.SchemaCompiler();
			this.__schemaCompiler.registerSchemaPart(efwdConfig.efwdRenderer.commonSchema);
		},

		"test: getLayers": function ()
		{
			this.__schemaCompiler.registerSchemaPart({ $layers: ["a"] });
			this.__schemaCompiler.registerSchemaPart({ $layers: ["b", "c"] });
			this.assertArrayEquals(["b", "c"], this.__schemaCompiler.getLayers());
		},

		"test: compileSchemaElement simple": function ()
		{
			this.__schemaCompiler.registerSchemaPart({
				some: {
					style: {
						one: { two: { three: "somestyle", four: "=$.five" } },
						five: "otherstyle",
						circle: {
							attrs: {
								first: "circle-const",
								second: "=$.five",
								third: "={prop1}"
							},
							attrs2: {
								first: "some-const",
								fourth: "nothing",
								fifth: "=$.one.two.four"
							}
						}
					},
					properties: {
						prop1: { bind: "_.bprop1" },
						prop2: { initial: "=$.one.two.three" }
					},
					layer: "a",
					draw: [
						{
							call: "rect",
							attrs: {
								first: "const",
								second: "=$.one.two.three",
								third: "=$.one.two.four",
								fourth: "={prop2}"
							}
						},
						{
							call: "circle",
							attrs: "$.circle.attrs"
						},
						{
							call: "rect",
							attrs: {
								fourth: "={prop2}",
								sixth: "=_.withChild.a",
								include: ["$.circle.attrs", "$.circle.attrs2"]
							}
						}
					]
				}
			});

			var model = this.__createModel({bprop1: "initial", bprop2: "initial2", withChild: { a: 1 }});
			var compiled = this.__schemaCompiler.compileSchemaElement("some", model);
			this.assertIdentical("a", compiled.layer);
			this.assertArray(compiled.draw);
			this.assertIdentical(3, compiled.draw.length);

			//simple
			var drawItem = compiled.draw[0];
			this.assertIdentical("rect", drawItem.call);
			this.assertMapEquals({first: "const", second: "somestyle", third: "otherstyle", fourth: "somestyle"},
				drawItem.attrs);

			//include attrs
			drawItem = compiled.draw[1];
			this.assertIdentical("circle", drawItem.call);
			this.assertIdentical("circle-const", drawItem.attrs.first);
			this.assertIdentical("otherstyle", drawItem.attrs.second);

			//dynamic property
			this.assertInstance(drawItem.attrs.third, efwdRenderer.ValueObserver);
			this.assertIdentical("initial", drawItem.attrs.third.getValue());

			var listener = this.stub();
			drawItem.attrs.third.subscribe(listener);

			model.setBprop2("initialTwo");
			this.assertNotCalled(listener);

			model.setBprop1("newValue");
			this.assertLastCall(listener, ["newValue"], 1);
			this.assertIdentical("newValue", drawItem.attrs.third.getValue());

			//multiple attrs include
			drawItem = compiled.draw[2];
			this.assertIdentical("some-const", drawItem.attrs.first);
			this.assertIdentical("otherstyle", drawItem.attrs.second);
			this.assertInstance(drawItem.attrs.third, efwdRenderer.ValueObserver);
			this.assertIdentical("newValue", drawItem.attrs.third.getValue());
			this.assertIdentical("somestyle", drawItem.attrs.fourth);
			this.assertIdentical("otherstyle", drawItem.attrs.fifth);

			//hierarchical model
			this.assertIdentical(1, drawItem.attrs.sixth.getValue());
			model.getWithChild().setA(2);
			this.assertIdentical(2, drawItem.attrs.sixth.getValue());
		},

		"test: compileSchemaElement call modifiers": function ()
		{
			this.__schemaCompiler.registerSchemaPart({
				element: {
					properties: {
						first: {}
					},
					calls: {
						"rect,circle": {
							attrs: {
								first: "=call.mulIt ? (value || 50) * {first} : (value || 50)"
							},
							modifiers: {
								mulIt: { initial: true },
								noDeps: { initial: false }
							}
						}
					}
				},
				derived: {
					properties: {
						first: { bind: "_.bfirst" },
						second: { initial: 3 }
					},
					calls: {
						"rect": {
							attrs: {
								second: "=call.mulIt ? value * {second} : value"
							}
						}
					}
				},
				some: {
					type: "derived",
					style: {
						first: 10
					},
					properties: {
						mulIt: { bind: "_.bMulIt" }
					},
					draw: [
						{
							call: "rect",
							attrs: {
								first: "=$.first",
								second: 20
							},
							modifiers: {
								noDeps: true
							}
						},
						{
							call: "circle",
							attrs: {
								first: "=$.first",
								second: 20
							},
							modifiers: {
								mulIt: true
							}
						},
						{
							call: "rect",
							attrs: {
								second: 20
							},
							modifiers: {
								mulIt: "={mulIt}"
							}
						}
					]
				}
			});

			var model = this.__createModel({bfirst: 2, bMulIt: false});
			var compiled = this.__schemaCompiler.compileSchemaElement("some", model);

			var drawItem = compiled.draw[0];
			this.assertIdentical(true, drawItem.modifiers.mulIt);
			this.assertIdentical(true, drawItem.modifiers.noDeps,
				"Даже если от модификатора не зависит ни один аттрибут, он всё равно должен вычисляться");
			this.assertInstance(drawItem.attrs.first, efwdRenderer.ValueObserver);
			this.assertIdentical(10 * 2, drawItem.attrs.first.getValue());
			this.assertIdentical(20 * 3, drawItem.attrs.second);

			model.setBfirst(3);
			this.assertIdentical(30, drawItem.attrs.first.getValue());

			drawItem = compiled.draw[1];
			this.assertIdentical(30, drawItem.attrs.first.getValue());
			this.assertIdentical(20, drawItem.attrs.second);

			drawItem = compiled.draw[2];
			this.assertInstance(drawItem.attrs.second, efwdRenderer.ValueObserver);
			this.assertIdentical(20, drawItem.attrs.second.getValue());
			model.setBMulIt(true);
			this.assertIdentical(60, drawItem.attrs.second.getValue());

			this.assertIdentical(50 * 3, drawItem.attrs.first.getValue(),
				"Даже если аттрибута нет, должно подставляться значение по-умолчанию из модификатора вызова");
		},

		"test: compileSchemaElement functions and caching": function ()
		{
			this.__schemaCompiler.registerSchemaPart({
				$functions: {
					sum: function (a, b){ return a + b; }
				},
				element: {
					properties: {
						prop1: { initial: 0 },
						prop2: { bind: "$f.sum({prop1}, 1)" }
					}
				}
			});
			this.__schemaCompiler.registerSchemaPart({
				$functions: {
					byModel: function (model){ return model.getA() + model.getB(); }
				},
				some: {
					style: {
						a: "=$f.byModel(_)"
					},
					properties: {
						prop1: { bind: "_.c" },
						prop3: { bind: "$.a - _.a" }
					},
					draw: [
						{
							call: "rect",
							attrs: {
								one: "={prop2}",
								two: "=$.a",
								three: "=$f.sum(10, 20)",
								four: "={prop3}"
							}
						}
					]
				}
			});

			var model = this.__createModel({a: 1, b: 2, c: 3});
			var compiled = this.__schemaCompiler.compileSchemaElement("some", model);
			var drawItem = compiled.draw[0];

			this.assertIdentical(3 + 1, drawItem.attrs.one.getValue());
			model.setC(4);
			this.assertIdentical(4 + 1, drawItem.attrs.one.getValue());

			//functions depending on model, and simple caching
			this.assertIdentical(1 + 2, drawItem.attrs.two.getValue());
			var listener = this.stub();
			drawItem.attrs.two.subscribe(listener);
			model.setA(2);
//			model.setB(1);
//			this.assertNotCalled(listener);
			model.setB(2);
			this.assertIdentical(2 + 2, drawItem.attrs.two.getValue());

			//static
			this.assertIdentical(10 + 20, drawItem.attrs.three);

			//functions depending on model, and advanced caching
			listener = this.stub();
			drawItem.attrs.four.subscribe(listener);
			model.setA(10);
			this.assertNotCalled(listener);
		},

		"test: compileSchemaElement children, elements including, properties inheriting, parent, parent types": function ()
		{
			this.__schemaCompiler.registerSchemaPart({
				base: {},
				parent: {
					type: "base",

					properties: {
						inheritIt: { bind: "_.a" },
						dontInheritIt: { bind: "_.b" },
						forChild: { bind: "_.c" }
					},

					draw: [
						{
							child: "firstChild",
							include: "child",
							set: {
								setMe: "=_.c",
								setMeConst: "const"
							}
						},
						{
							child: "secondChild",
							include: "child",
							set: {
								setMe: "one",
								setMeConst: "two"
							}
						}
					]
				},
				child: {
					calls: {
						"rect": {
							attrs: {
								byModifier: "=parent.{forChild}"
							}
						}
					},

					properties: {
						inheritIt: { inherit: true, initial: 0 },
						dontInheritIt: { initial: 0 },
						setMe: {},
						setMeConst: {},
						byParent: { bind: "Object.keys(parent.types).sort().join('|') + ' ' + parent.{dontInheritIt}" }
					},

					draw: [
						{
							call: "rect",
							attrs: {
								inheritIt: "={inheritIt}",
								dontInheritIt: "={dontInheritIt}",
								setMe: "={setMe}",
								setMeConst: "={setMeConst}",
								byParent: "={byParent}"
							}
						}
					]
				}
			});

			var model = this.__createModel({a: 1, b: 2, c: 3});
			var compiled = this.__schemaCompiler.compileSchemaElement("parent", model);

			model.setA(10);
			model.setB(20);
			model.setC(30);

			var drawItem = compiled.draw[0];
			this.assertIdentical("firstChild", drawItem.child);
			this.assertIdentical(compiled.elements.firstChild, drawItem.element);
			var childDrawItem = compiled.elements.firstChild.draw[0];
			this.assertIdentical(10, childDrawItem.attrs.inheritIt.getValue());
			this.assertIdentical(0, childDrawItem.attrs.dontInheritIt);
			this.assertIdentical(30, childDrawItem.attrs.setMe.getValue());
			this.assertIdentical("const", childDrawItem.attrs.setMeConst);
			this.assertIdentical("base|element|parent 20", childDrawItem.attrs.byParent.getValue());
			this.assertIdentical(30, childDrawItem.attrs.byModifier.getValue());

			drawItem = compiled.draw[1];
			this.assertIdentical(compiled.elements.secondChild, drawItem.element);
			childDrawItem = compiled.elements.secondChild.draw[0];
			this.assertIdentical("two", childDrawItem.attrs.setMeConst);
		},

		"test: compileSchemaElement drawChildren, draw multiple children, states, exStates": function ()
		{
			this.__schemaCompiler.registerSchemaPart({
				baseChild: {
					properties: {
						bcp: { initial: 1 }
					}
				},
				baseParent: {
					properties: {
						bpp: {},
						collection: {},

						//states
						state1: { dynamic: true, initial: false },
						state2: { dynamic: true, initial: false }
					},

					children: {
						firstChild: {
							type: "baseChild",
							properties: {
								computed: { bind: "{bcp} + ' ' + parent.{bpp}" },
								fcp: { initial: "fcp" },

								//states
								state1: { dynamic: true, initial: false }
							}
						},
						secondChild: {},
						thirdChild: {},
						multipleChild: {
							properties: {
								//states
								state1: { dynamic: true, initial: false }
							}
						}
					},

					draw: [
						{
							child: "firstChild"
						},
						{
							child: "secondChild"
						},
						{
							child: "multipleChild",
							multiple: "{collection}"
						}
					]
				},
				derivedParent: {
					type: "baseParent",

					style: {
						someStyle: "=parent.{state1} && {state1} ? 'instate1' : {fcp}",
						otherStyle: ""
					},

					properties: {
						bpp: { bind: "_.a" },
						collection: { bind: "_.collection" },
						someProp: { initial: 5 }
					},

					drawChildren: {
						firstChild: [
							{
								call: "rect",
								attrs: {
									computed: "={computed} + ',' + parent.{someProp} + ',' + $.someStyle",
									dynamic: "=parent.{bpp}"
								}
							}
						],
						secondChild: [
							{
								call: "rect",
								attrs: {
									dynamic: "=parent.{bpp}"
								}
							}
						],
						multipleChild: [
							{
								call: "rect",
								attrs: {
									fromItem: "=_.a",
									dynamic: "=parent.{bpp}"
								}
							},
							{
								call: "circle",
								modifiers: {
									enabled: "=parent.{state2} || {state1}"
								},
								attrs: {
									dynamic: "=parent.{bpp}"
								}
							}
						]
					},

					draw: [
						{
							child: "secondChild",
							modifiers: {
								enabled: "={state1} || {state2}"
							}
						}
					]
				}
			});

			var model = this.__createModel({a: 10, collection: [{a:1}]});
			var compiled = this.__schemaCompiler.compileSchemaElement("derivedParent", model);
			this.assertIdentical(3, compiled.draw.length);

			var firstChild = compiled.draw[0];
			var firstChildAttrs = compiled.elements.firstChild.draw[0].attrs;
			this.assertIdentical("1 10,5,fcp", firstChildAttrs.computed.getValue());

			//only in state1 and state2
			var secondChild = compiled.draw[1];
			var secondChildElement = compiled.draw[1].element;
			var secondChildDraw = secondChildElement.draw[0];
//			var listener = this.stub();
//			secondChildDraw.attrs.dynamic.subscribe(listener);
			model.setA(20);
//			this.assertNotCalled(listener);

			//add state
			var enabledListener = this.stub();
			this.assertInstance(secondChild.callModifiers.enabled, efwdRenderer.ValueObserver);
			this.assertIdentical(false, secondChild.callModifiers.enabled.getValue());
			secondChild.callModifiers.enabled.subscribe(enabledListener);
			compiled.setProperty("state1", true);
			this.assertLastCall(enabledListener, [true], 1);
			this.assertIdentical(true, secondChild.callModifiers.enabled.getValue());
			this.assertIdentical(20, secondChildDraw.attrs.dynamic.getValue());
//			this.assertNotCalled(listener);

			this.assertIdentical("1 20,5,fcp", firstChildAttrs.computed.getValue());
			compiled.elements.firstChild.setProperty("state1", true);
			this.assertIdentical("1 20,5,instate1", firstChildAttrs.computed.getValue());

			//removeState
			compiled.setProperty("state1", false);
			this.assertIdentical("1 20,5,fcp", firstChildAttrs.computed.getValue());
			this.assertLastCall(enabledListener, [false], 2);
			this.assertIdentical(false, secondChild.callModifiers.enabled.getValue());

			compiled.setProperty("state2", true);
			this.assertIdentical(true, secondChild.callModifiers.enabled.getValue());
			compiled.setProperty("state2", false);

			var multipleChild = compiled.draw[2];

			//multipleChild
			this.assertInstance(multipleChild.elements, efwdRenderer.ValueObserver);

			//first element
			var elements = multipleChild.elements.getValue();
			this.assertIdentical(1, elements.length);
			this.assertIdentical(1, elements[0].draw[0].attrs.fromItem.getValue());

			//remove all
			model.getCollection().removeAll();

			this.assertArrayEquals([], multipleChild.elements.getValue());
			var listener = this.stub();
			multipleChild.elements.subscribe(listener);

			//append elements
			model.getCollection().append([this.__createModel({a: 100}, true), this.__createModel({a: 200}, true)]);

			this.assertIdentical(2, elements.length);
			this.assertIdentical(100, elements[0].draw[0].attrs.fromItem.getValue());
			this.assertIdentical(200, elements[1].draw[0].attrs.fromItem.getValue());

			this.assertCalledOnce(listener);
			this.assertIdentical(elements, listener.args.last()[0]);
			var data = listener.args.last()[1];
			this.assertIdentical(0, data.index);
			this.assertIdentical(0, data.remove.length);
			this.assertArrayEquals([elements[0], elements[1]], data.insert);

			model.getCollection().getItem(1).setA(250);
			this.assertIdentical(250, elements[1].draw[0].attrs.fromItem.getValue());

			//replace
			var lastElement = elements[1];
			model.getCollection().setItem(1, this.__createModel({a: 250}, true));
			this.assertNotIdentical(lastElement, elements[1]);
			this.assertIdentical(250, elements[1].draw[0].attrs.fromItem.getValue());

			data = listener.args.last()[1];
			this.assertIdentical(1, data.index);
			this.assertArrayEquals([lastElement], data.remove);
			this.assertArrayEquals([elements[1]], data.insert);

			//inserting
			var firstElement = elements[0];
			lastElement = elements[1];
			model.getCollection().insertAt(1, this.__createModel({a: 200}, true));
			this.assertIdentical(firstElement, elements[0]);
			this.assertIdentical(lastElement, elements[2]);
			var inserted = elements[1];

			data = listener.args.last()[1];
			this.assertIdentical(1, data.index);
			this.assertIdentical(0, data.remove.length);
			this.assertArrayEquals([elements[1]], data.insert);

			//removing
			model.getCollection().removeAt(1);
			this.assertIdentical(firstElement, elements[0]);
			this.assertIdentical(lastElement, elements[1]);

			data = listener.args.last()[1];
			this.assertIdentical(1, data.index);
			this.assertArrayEquals([inserted], data.remove);
			this.assertIdentical(0, data.insert.length);

			//states in multiple elements
			var enabled = firstElement.draw[1].callModifiers.enabled;
			this.assertIdentical(false, enabled.getValue());
			compiled.setProperty("state2", true);
			this.assertIdentical(true, enabled.getValue());
			compiled.setProperty("state2", false);

			firstElement.setProperty("state1", true);
			this.assertIdentical(true, enabled.getValue());

			//проверка при удалении элементов
			this.assertIdentical(20, inserted.draw[0].attrs.dynamic.getValue());
			model.setA(30);
			this.assertIdentical(20, inserted.draw[0].attrs.dynamic.getValue());
			this.assertIdentical(30, firstElement.draw[0].attrs.dynamic.getValue());
		},

		"test: compileSchemaElement dynamic arrays": function ()
		{
			this.__schemaCompiler.registerSchemaPart({
				$functions: {
					byCollection: function (collection)
					{
						return collection.toArray().reduce(function (res, x){ return res + x.getVal() }, 1);
					}
				},
				some: {
					properties: {
						collection: { bind: "_.collection" }
					},
					draw: [
						{
							call: "rect",
							attrs: {
								computed: "=$f.byCollection({collection})"
							}
						}
					]
				}
			});

			var model = this.__createModel({collection: []});
			var compiled = this.__schemaCompiler.compileSchemaElement("some", model);
			var computed = compiled.draw[0].attrs.computed;

			this.assertIdentical(1, computed.getValue());

			model.getCollection().append([this.__createModel({val: 1}, true), this.__createModel({val: 2}, true)]);
			this.assertIdentical(4, computed.getValue());

			model.getCollection().getItem(0).setVal(10);
			model.getCollection().getItem(1).setVal(20);
			this.assertIdentical(31, computed.getValue());
		},

		"test: compileSchemaElement properties converting, attrs as property, merge attrs": function ()
		{
			this.__schemaCompiler.registerSchemaPart({
				parent: {
					style: {
						attrs: {
							one: "no",
							two: "=_.b"
						},
						attrs2: {
							one: "const",
							three: "=_.c"
						}
					},
					properties: {
						parentProp: { bind: "_.a" }
					},
					draw: [
						{
							include: "child",
							set: {
								parentProp: "const"
							}
						},
						{
							include: "child",
							set: {
								attrs: "=merge($.attrs, $.attrs2)"
							}
						}
					]
				},
				child: {
					properties: {
						childProp: { initial: "child" },
						parentProp: { initial: "init", convert: "value + ',' + parent.{parentProp} + ',' + {childProp}" },
						attrs: { initial: {} }
					},
					draw: [
						{
							call: "rect",
							attrs: {
								dynamic: "={parentProp}",
								include: ["{attrs}"]
							}
						}
					]
				}
			});

			var model = this.__createModel({a: "a", b: "b", c: "c"});
			var compiled = this.__schemaCompiler.compileSchemaElement("parent", model);
			var drawItem = compiled.draw[0].element.draw[0];
			this.assertIdentical("const,a,child", drawItem.attrs.dynamic.getValue());
			drawItem = compiled.draw[1].element.draw[0];
			this.assertIdentical("init,a,child", drawItem.attrs.dynamic.getValue());

			//attrs as property
			this.assertIdentical("const", drawItem.attrs.one);
			this.assertIdentical("b", drawItem.attrs.two.getValue());
			this.assertIdentical("c", drawItem.attrs.three.getValue());

			model.setB("newb");
			model.setC("newc");
			this.assertIdentical("newb", drawItem.attrs.two.getValue());
			this.assertIdentical("newc", drawItem.attrs.three.getValue());
		},

		"test: compileSchemaElement dynamic properties, other elements, other elements properties": function ()
		{
			var $this = this;

			this.__schemaCompiler.registerSchemaPart({
				some: {
					properties: {
						dynamic: { initial: "init", dynamic: true }
					},
					id: "_.id",
					draw: [
						{
							call: "rect",
							attrs: {
								dynamic: "={dynamic}"
							}
						}
					]
				},
				other: {
					properties: {
						some: { elementId: "_.someId" }
					},
					draw: [
						{
							call: "rect",
							attrs: {
								fromSome: "={some|dynamic}"
							}
						}
					]
				}
			});

			var someModel = this.__createModel({ id: "some-id" });
			var some = this.__schemaCompiler.compileSchemaElement("some", someModel);
			this.assertInstance(some.draw[0].attrs.dynamic, efwdRenderer.ValueObserver);

			var otherModel = this.__createModel({ someId: "some-id" });
			var other = this.__schemaCompiler.compileSchemaElement("other", otherModel);

			var drawItem = other.draw[0];
			this.assertIdentical("init", drawItem.attrs.fromSome.getValue());
			some.setProperty("dynamic", "new");
			this.assertIdentical("new", drawItem.attrs.fromSome.getValue());

			this.__schemaCompiler.removeCompiledSchemaElement(some);
			this.assertException(function ()
			{
				$this.__schemaCompiler.compileSchemaElement("other", $this.__createModel({ someId: "some-id" }))
			});

			this.__schemaCompiler.removeCompiledSchemaElement(other);

			//при уничтожении элемента
			some = this.__schemaCompiler.compileSchemaElement("some", someModel);
			other = this.__schemaCompiler.compileSchemaElement("other", otherModel);
			this.__schemaCompiler.removeCompiledSchemaElement(other);
			some.setProperty("dynamic", "no-sense");
			this.assertNotIdentical("no-sense", other.draw[0].attrs.fromSome.getValue());
		},

		"test: compileSchemaElement styles": function ()
		{
			this.__schemaCompiler.registerSchemaPart({
				base: {
					style: {
						a: 1
					},
					properties: {
						bp: { bind: "$.a" }
					}
				},
				derived: {
					type: "base",
					style: {
						a: 2
					},
					properties: {
						dp: { bind: "$.a" }
					},
					draw: [
						{
							call: "rect",
							attrs: {
								computed: "={bp} + ' ' + {dp}",
								computed2: "=$$.base.a + ' ' + $.a"
							}
						}
					]
				}
			});

			var compiled = this.__schemaCompiler.compileSchemaElement("derived", this.__createModel({}));
			var drawItem = compiled.draw[0];
			this.assertIdentical("1 2", drawItem.attrs.computed);
			this.assertIdentical("1 2", drawItem.attrs.computed2);
		},

		"test: destroy": function ()
		{
			this.__schemaCompiler.registerSchemaPart({
				parent: {
					draw: [
						{
							include: "child"
						},
						{
							include: "child",
							child: "someChild"
						}
					]
				},
				child: {
					draw: [
						{
							call: "rect",
							attrs: {
								some: 1
							}
						}
					]
				}
			});

			var compiled = this.__schemaCompiler.compileSchemaElement("parent", this.__createModel({}));

			//при удалении родителя должны удаляться все дочерние элементы
			var firstChild = compiled.draw[0].element;
			var secondChild = compiled.elements.someChild;

			var firstListener = this.stub();
			firstChild.onRemoved.subscribe(firstListener);
			var secondListener = this.stub();
			secondChild.onRemoved.subscribe(secondListener);

			compiled.remove();
			this.assertCalledOnce(firstListener);
			this.assertCalledOnce(secondListener);
		},

		/**
		 * @param obj
		 * @param [asProperty=false]
		 * @private
		 */
		__createModel: function (obj, asProperty)
		{
			var model = qx.data.marshal.Json.createModel(obj);
			if (!asProperty)
			{
				library.data.Helper.setUpHierarchyEvents(model);
			}
			return model;
		}
	}
});
