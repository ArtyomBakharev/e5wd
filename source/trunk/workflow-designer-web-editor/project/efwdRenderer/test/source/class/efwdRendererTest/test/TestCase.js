/**
 * @extends {qx.dev.unit.TestCase}
 */
qx.Class.define("efwdRendererTest.test.TestCase", {
	
	extend: qx.dev.unit.TestCase,
	type: "abstract",

	include: library.dev.MMock,

	construct: function()
	{
		if (!this.__staticInited)
		{
			library.Extends.extendNativeTypes();
			qx.Class.patch(qx.data.Array, library.data.MArrayExtend);
			this.__staticInited = true;
		}
	}
});
