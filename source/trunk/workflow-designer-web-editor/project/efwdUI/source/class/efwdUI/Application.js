/**
 * Базовый класс для приложений использующих визуальные компоненты
 * @extends {qx.application.Standalone}
 */
qx.Class.define("efwdUI.Application", {

	extend: qx.application.Standalone,

	events: {
		/**
		 * Событие вызывается перед закрытием страницы, перед событием beforePageClose
		 */
		"prepareToPageClose": "qx.event.type.Event",
		/**
		 * Событие вызвается перед закрытием страницы, после события prepareToPageClose
		 */
		"beforePageClose": "qx.event.type.Event"
	},

	properties: {
		/**
		 * Если свойство не null, то при попытке закрыть страницу будет показан диалог с текстом равным этому свойству
		 */
		pageCloseMessage: {
			init: null,
			nullable: true
		}
	},

	statics: {
		/**
		 * Статическая инициализация приложения (используется как в приложении так и в юнит-тестах)
		 */
		staticInit: function ()
		{
			library.Extends.extendNativeTypes();
			qx.Class.patch(qx.ui.list.List, library.ui.extend.MListExtends);
			qx.Class.patch(qx.ui.form.Button, library.ui.extend.MExecutableExtends);
			qx.Class.patch(qx.ui.menu.AbstractButton, library.ui.extend.MExecutableExtends);
			qx.require(library.data.MArrayExtend);

			// Enable logging in debug variant
			if (qx.core.Environment.get("qx.debug"))
			{
				// support native logging capabilities, e.g. Firebug for Firefox
				qx.require(qx.log.appender.Native);
				// support additional cross-browser console. Press F7 to toggle visibility
				qx.require(qx.log.appender.Console);

				qx.require(library.dev.Debug);

				window.console && (console.mirror = function(x, name)
				{
					console.log((name ? name + ": " : "") + qx.lang.Type.isObject(x) ? JSON.stringify(x) : x);
					return x;
				});
			}
		}
	},

	members: {

		close: function ()
		{
			this.fireEvent("prepareToPageClose");
			this.fireEvent("beforePageClose");
			return this.getPageCloseMessage();
		},

		/**
		 * Менеджер всплывающих сообщений
		 *
		 * @return {library.ui.messagesqueue.Manager}
		 * @cacheResult
		 */
		getMessagesQueue: function ()
		{
			return new library.ui.messagesqueue.Manager();
		}
	},

	defer: function (statics, members)
	{
		members.getMessagesQueue = members.getMessagesQueue.cacheResult();
	}
});
