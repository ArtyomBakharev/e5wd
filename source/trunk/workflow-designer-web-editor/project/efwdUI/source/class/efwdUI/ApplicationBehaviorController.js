/*
 #ignore(efwdConfig.)
 #ignore(efwdConfig.*)
 */

/**
 * Контроллер реагирующий на определённые события приложения
 * При закрытии страницы, если ещё не все изменения сохранены, уведомляет об этом пользователя
 * Если происходит ошибка на сервере, уведомляет об этом пользователя и перезагружает редактор
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdUI.ApplicationBehaviorController", {

	extend: qx.core.Object,

	/**
	 * @param {efwdDal.saving.AbstractSaver} saver
	 * @param {efwdDal.Service} service
	 * @param {efwdDal.repository.SessionRepository} sessionRepository
	 */
	construct: function (saver, service, sessionRepository)
	{
		this.__saver = saver;
		this.__service = service;
		this.__sessionRepository = sessionRepository;

		this.__unsavedStateChecksAllowed = true;
	},

	members: {

		initialize: function ()
		{
			this.__setUpServerErrorsProcessing();
			this.__setUpPageClosingProcessing();
		},

		/**
		 * @private
		 */
		__setUpPageClosingProcessing: function ()
		{
			var application = qx.core.Init.getApplication();
			application.addListener("beforePageClose", function ()
			{
				if (this.__unsavedStateChecksAllowed && this.__saver.isDuringSaving())
				{
					application.setPageCloseMessage("Not all changes are saved. Please, click on 'stay on the page' button and wait until all changes will be saved.");
					(function (){ this.__showCloseWindows() }).defer(this);
				}
				else
				{
					application.setPageCloseMessage(null);
					if (this.__sessionRepository.isSessionIdAcquired())
					{
						this.__sessionRepository.deleteSession();
					}
				}
			}, this);
		},

		/**
		 * @private
		 */
		__setUpServerErrorsProcessing: function ()
		{
			var $this = this;
			this.__service.addListener("unhandledError", function (e)
			{
				var message;
				switch (e.getData().code)
				{
					case efwdConfig.efwdCommon.Communication.codes.diagramLocked:
						message = "Diagram is locked by another user! Editor will be reloaded to read only mode.";
						break;

					default:
						message = "An error was occured on server or server is unavailable! Application will be reloaded.";
				}
				this.__unsavedStateChecksAllowed = false;

				this.__saver.disableSaving();
				removeAppLoadIndicator();
				library.ui.Dialog.alert(message, function (){ location.reload() });
			}, this);
		},

		/**
		 * @private
		 */
		__showCloseWindows: function ()
		{
			var application = qx.core.Init.getApplication();

			var $this = this;
			if (this.__closeWindowShown)
			{
				return;
			}

			this.__closeWindowShown = true;
			function savingComplete()
			{
				application.getMessagesQueue()
					.addMessage(library.ui.messagesqueue.Message.createInfoMessage("All changes saved, you can close editor now"));
				$this.__closeWindowShown = false;
			}

			if (this.__saver.isDuringSaving())
			{
				var batchesTotal = this.__saver.getBatchesLeft();
				var listenersStore = new library.ListenersStore();
				var handler = library.ui.Dialog.progressBar(batchesTotal, 0, "Please wait saving changes");

				listenersStore.addBinding(this.__saver, "batchesLeft", handler.progressBar, "value",
					function (x){ return batchesTotal - x });
				listenersStore.addListener(this.__saver, "savingComplete", function ()
				{
					listenersStore.dispose();
					handler.destroy();
					savingComplete();
				}, this);
			}
			else
			{
				savingComplete();
			}
		}

	},

	defer: function (statics, members, properties)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}

});
