/*
 #ignore(efwdCommon)
 #ignore(efwdCommon.*)
 */

/**
 * Класс хранит список зависимостей для IOC-контейнера
 */
qx.Class.define("efwdUI.Dependencies", {

	type: "static",

	statics: {
		DEPENDENCIES: [

			//efwdUI
			[efwdUI.ApplicationBehaviorController, {args: [
				efwdDal.saving.AbstractSaver,
				efwdDal.Service,
				efwdDal.repository.SessionRepository
			]}],

			//efwdUI.behavior
			[efwdUI.behavior.EntityEditionModel],
			[efwdUI.behavior.GeneralCommandsProvider, {args: [
				library.IServiceLocator,
				efwdUI.commands.Queue
			]}],
			[efwdUI.behavior.PageModel],

			//efwdUI.commands
			[efwdUI.commands.Queue],

			//efwdUI.commands.types
			[efwdUI.commands.types.AddActivitiesToOwner, {scope: "transient", args: [
				efwdDal.Context
			]}],
			[efwdUI.commands.types.CreateAction, {scope: "transient", args: [
				efwdDal.repository.ActionsRepository,
				efwdDal.Context,
				efwdDal.entity.Process
			]}],
			[efwdUI.commands.types.CreateActivity, {scope: "transient", args: [
				efwdDal.repository.ActivitiesRepository,
				efwdDal.Context,
				efwdDal.entity.Process
			]}],
			[efwdUI.commands.types.CreateProcess, {scope: "transient", args: [
				efwdDal.repository.ProcessesRepository,
				efwdDal.Context
			]}],
			[efwdUI.commands.types.CreateStatus, {scope: "transient", args: [
				efwdDal.repository.StatusesRepository,
				efwdDal.Context,
				efwdDal.entity.Process
			]}],
			[efwdUI.commands.types.DeleteEntities, {scope: "transient", args: [
				efwdDal.Context,
				library.IServiceLocator
			]}],
			[efwdUI.commands.types.EditEntity, {scope: "transient", args: [
				efwdDal.Context,
				efwdUI.behavior.EntityEditionModel
			]}],
			[efwdUI.commands.types.RemoveActivities, {scope: "transient", args: [
				efwdDal.Context
			]}],
			[efwdUI.commands.types.SetInitialStatus, {scope: "transient", args: [
				efwdDal.Context
			]}],

			//efwdUI.commands.types.statedumper
			[efwdUI.commands.types.statedumper.Activity, {scope: "transient", args: [
				efwdDal.repository.ActivitiesRepository
			]}],
			[efwdUI.commands.types.statedumper.Status, {scope: "transient", args: [
				library.IServiceLocator,
				efwdDal.repository.ActionsRepository
			]}],

			//efwdUI.ui.dialog
			[efwdUI.ui.dialog.ActionsListDialog, {scope: "transient", args: [
				library.IServiceLocator
			]}],
			[efwdUI.ui.dialog.ActivitiesDialog, {scope: "transient", args: [
				library.IServiceLocator
			]}],
			[efwdUI.ui.dialog.AllActivitiesDialog, {scope: "transient", args: [
				library.IServiceLocator
			]}],
			[efwdUI.ui.dialog.ProcessDialog, {scope: "transient", args: [
				library.IServiceLocator
			]}],

			//efwdUI.ui.dialog.entitieslist
			[efwdUI.ui.dialog.entitieslist.ActionsList, {scope: "transient"}],
			[efwdUI.ui.dialog.entitieslist.ActivitiesList, {scope: "transient", args: [
				efwdUI.behavior.GeneralCommandsProvider
			]}],
			[efwdUI.ui.dialog.entitieslist.ProcessesList, {scope: "transient"}],

			//efwdUI.ui.dialog.viewmodels
			[efwdUI.ui.dialog.viewmodels.ActionsListViewModel, {scope: "transient", args: [
				efwdDal.entity.Process,
				efwdUI.behavior.EntityEditionModel,
				library.IServiceLocator,
				efwdUI.commands.Queue,
				efwdUI.behavior.PageModel,
				efwdDal.repository.ProcessesRepository
			]}],
			[efwdUI.ui.dialog.viewmodels.ActivitiesListsViewModel, {scope: "transient", args: [
				library.IServiceLocator
			]}],
			[efwdUI.ui.dialog.viewmodels.ActivitiesListViewModel, {scope: "transient", args: [
				efwdUI.behavior.EntityEditionModel,
				library.IServiceLocator,
				efwdUI.commands.Queue,
				efwdUI.behavior.PageModel,
				efwdDal.entity.Process
			]}],
			[efwdUI.ui.dialog.viewmodels.ProcessesListViewModel, {scope: "transient", args: [
				efwdUI.behavior.EntityEditionModel,
				library.IServiceLocator,
				efwdUI.commands.Queue,
				efwdUI.behavior.PageModel,
				efwdDal.repository.ProcessesRepository
			]}],

			//efwdUI.ui.entityediting.controls
			[efwdUI.ui.entityediting.controls.AllProcessesActionsChooser, {scope: "transient", args: [
				library.IServiceLocator,
				efwdDal.repository.ProcessesRepository,
				efwdDal.IdsMapper,
				efwdDal.entity.Process
			]}],

			//efwdUI.ui.entityediting.view
			[efwdUI.ui.entityediting.view.EntityForm, {scope: "transient", args: [
				efwdUI.Application,
				library.IServiceLocator
			]}],

			//efwdUI.ui.entityediting.viewModel
			[efwdUI.ui.entityediting.viewModel.EntityEditor, {scope: "transient", args: [
				library.IServiceLocator,
				efwdUI.commands.Queue,
				efwdUI.behavior.PageModel
			]}],
			[efwdUI.ui.entityediting.viewModel.StatusEditor, {scope: "transient", args: [
				library.IServiceLocator,
				efwdUI.commands.Queue,
				efwdUI.behavior.PageModel,
				efwdUI.behavior.GeneralCommandsProvider
			]}],

			//efwdUI.ui.settingspanel
			[efwdUI.ui.settingspanel.Controller, {args: [
				efwdUI.behavior.EntityEditionModel,
				efwdUI.ui.views.EditorContainer,
				library.IServiceLocator
			]}],

			//efwdUI.ui.settingspanel.editor
			[efwdUI.ui.settingspanel.editor.EntityEditor, {scope: "transient", args: [
				library.IServiceLocator
			]}],

			//efwdUI.ui.views
			[efwdUI.ui.views.EditorContainer]
		]
	}
});
