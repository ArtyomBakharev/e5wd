/**
 * Модель позволяет назначать сущность, для редактирования
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdUI.behavior.EntityEditionModel", {

	extend: qx.core.Object,

	properties: {
		/**
		 * Редактируемая сущность
		 */
		entity: {
			init: null,
			nullable: true,
			check: "efwdDal.entity.AbstractEntity",
			event: "changeEntity"
		}
	}
});
