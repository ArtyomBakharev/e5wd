/**
 * Поставщик основных команд интерфейса редактора
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdUI.behavior.GeneralCommandsProvider", {

	extend: qx.core.Object,

	/**
	 * @param {library.IServiceLocator} serviceLocator
	 * @param {efwdUI.commands.Queue} queue
	 */
	construct: function (serviceLocator, queue)
	{
		this.__serviceLocator = serviceLocator;
		this.__queue = queue;
	},

	members: {

		/**
		 * Создаёт команду, вызывающую диалог выбора активностей
		 * @param {Function} callback (efwdDal.entity.Activity[]) функция вызывается, когда пользователь выбрал
		 * одну или несколько активностей
		 * @return {library.ui.Command}
		 */
		createSelectActivitiesCommand: function (callback)
		{
			return new library.ui.Command(function ()
			{

				var viewModel = this.__serviceLocator.getService(efwdUI.ui.dialog.viewmodels.ActivitiesListViewModel);
				viewModel.initialize(null, null, "select");
				var list = this.__serviceLocator.getService(efwdUI.ui.dialog.AllActivitiesDialog);
				list.initialize(viewModel);

				list.addListener("destroyed", function ()
				{
					var selection = viewModel.getSelection().toArray();
					if (selection.length)
					{
						callback(selection);
					}
					viewModel.dispose();
				});

				list.render();

			}, this);
		},

		/**
		 * @return {library.ui.Command}
		 * @cacheResult
		 */
		getSetAsInitialStatusCommand: function()
		{
			return new library.ui.Command(function (data)
			{
				var command = this.__serviceLocator.getService(efwdUI.commands.types.SetInitialStatus);
				command.initialize(data.status);
				this.__queue.addCommand(command);
			}, this).set({
					label: "Set as the initial status",
					icon: efwdUI.ui.IconsStore.icons.initialStatus
				});
		}
	},

	defer: function(statics, members)
	{
		members.getSetAsInitialStatusCommand = members.getSetAsInitialStatusCommand.cacheResult();
	}
});
