/**
 * Модель выделения элементов на канве
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdUI.behavior.PageModel", {

	extend: qx.core.Object,

	properties: {
		readOnly: {
			init: false,
			check: "Boolean",
			event: "changeReadOnly"
		}
	}
});
