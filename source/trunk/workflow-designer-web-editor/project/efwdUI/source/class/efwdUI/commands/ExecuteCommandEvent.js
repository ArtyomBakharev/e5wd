
/**
 * @extends {qx.event.type.Event}
 */
qx.Class.define("efwdUI.commands.ExecuteCommandEvent", {

	extend: qx.event.type.Event,

	properties: {
		notifyUser: {
			init: false,
			nullable: false,
			check: "Boolean"
		}
	},


	construct: function()
	{
	},

	members: {

		/**
		 * @return {efwdUI.commands.types.AbstractCommand}
		 */
		getCommand: function()
		{
			return this.__command;
		},

		/**
		 * @return {String}
		 */
		getExecuteType: function()
		{
			return this.__executeType;
		},

		/**
		 * @param {efwdUI.commands.types.AbstractCommand} command
		 * @param {String} executeType (execute, undo, redo)
		 * @param {Boolean} [notifyUser=false]
		 */
		init: function(command, executeType, notifyUser)
		{
			this.__command = command;
			this.__executeType = executeType;
			this.setNotifyUser(!!notifyUser);
		}
	}

});
