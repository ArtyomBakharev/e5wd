/**
 * Очередь команд редактора. Пока просто выполняет команду сразу же после её добавления
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdUI.commands.Queue", {

	extend: qx.core.Object,

	construct: function ()
	{
		this.__undoArray = new qx.data.Array();
		this.__redoArray = new qx.data.Array();
	},

	events: {
		/**
		 * Вызвается перед отменой группы команд
		 */
		"beforeUndoBatch": "qx.event.type.Event",

		/**
		 * Вызвается перед повторением группы команд
		 */
		"beforeRedoBatch": "qx.event.type.Event",

		/**
		 * command execute, undo, redo
		 */
		"commandExecuted": "efwdUI.commands.ExecuteCommandEvent",

		/**
		 * @param {efwdUI.commands.types.AbstractCommand} command
		 * @param {Boolean} redo
		 */
		"notifyUser": "qx.event.type.Data"
	},

	members: {
		/**
		 * добавить команду в очередь
		 * @param {efwdUI.commands.types.AbstractCommand} command
		 */
		addCommand: function (command)
		{
			var result = command.execute();

			if (qx.core.Environment.get("qx.debug"))
			{
				this.assertBoolean(result);
			}
			if (!result)
			{
				return;
			}

			this.__afterExecuteCommand(command, "execute");

			this.__undoArray.unshift(command);
			this.__redoArray.removeAll();
		},

		/**
		 * Массив всех команд, которые можно повторить
		 * @return {qx.data.Array}
		 */
		getRedoArray: function ()
		{
			return this.__redoArray;
		},

		/**
		 * Массив команд, которые можно отменить
		 * @return {qx.data.Array}
		 */
		getUndoArray: function ()
		{
			return this.__undoArray;
		},

		/**
		 * Повторение последней отменённой команды
		 */
		redo: function ()
		{
			if (this.__redoArray.getLength() == 0)
			{
				return;
			}

			this.redoTo(this.__redoArray.getItem(0));
		},

		/**
		 * Повторение отменённых команд вплоть до command
		 * @param {efwdUI.commands.types.AbstractCommand} command
		 */
		redoTo: function (command)
		{
			this.fireEvent("beforeRedoBatch");

			var curCommand;
			do {
				curCommand = this.__redoArray.shift();
				curCommand.redo();
				this.__undoArray.unshift(curCommand);

				this.__afterExecuteCommand(curCommand, "redo");
			} while (curCommand != command);
		},

		/**
		 * Отмена последней выполненой команды
		 */
		undo: function ()
		{
			if (this.__undoArray.getLength() == 0)
			{
				return;
			}

			this.undoTo(this.__undoArray.getItem(0));
		},

		/**
		 * Отмена выполненых команд вплоть до command
		 * @param {efwdUI.commands.types.AbstractCommand} command
		 */
		undoTo: function (command)
		{
			this.fireEvent("beforeUndoBatch");

			var curCommand;
			do {
				curCommand = this.__undoArray.shift();
				curCommand.undo();
				this.__redoArray.unshift(curCommand);

				this.__afterExecuteCommand(curCommand, "undo");
			} while (curCommand != command);
		},

		/**
		 * @param {efwdUI.commands.types.AbstractCommand} command
		 * @param {String} executeType (execute, undo, redo)
		 * @private
		 */
		__afterExecuteCommand: function(command, executeType)
		{
			var event = qx.event.Registration.createEvent("commandExecuted", efwdUI.commands.ExecuteCommandEvent, [
				command,
				executeType,
				executeType != "execute" ? command.needToNotifyUser(executeType == "redo") : false
			]);
			this.dispatchEvent(event);

			if (executeType != "execute" && event.isNotifyUser())
			{
				this.fireDataEvent("notifyUser", {command: command, redo: executeType == "redo"});
			}
		}
	}
});
