/**
 * Комманда, инкапсулирующая определённую логику изменений посредством редактора, а также логику
 * отмены и повторения этих изменений
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdUI.commands.types.AbstractCommand", {

	type: "abstract",

	extend: qx.core.Object,

	members: {

		getTitle: function (){ throw "efwdUI.commands.types.AbstractCommand#getTitle] Method is abstract!" },

		/**
		 * выполнить комманду
		 * @return {Boolean}
		 */
		execute: function (){ throw "efwdUI.commands.types.AbstractCommand#execute] Method is abstract!" },

		undo: function (){ throw "efwdUI.commands.types.AbstractCommand#undo] Method is abstract!" },

		redo: function ()
		{
			this.execute();
		},

		/**
		 * @param {Boolean} redo
		 * @return {Boolean}
		 */
		needToNotifyUser: function(redo){ return false; }
	}
});
