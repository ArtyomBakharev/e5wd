/**
 * Добавляет активности к владельцу
 * @extends {efwdUI.commands.types.ReversableCommand}
 */
qx.Class.define("efwdUI.commands.types.AddActivitiesToOwner", {

	extend: efwdUI.commands.types.ReversableCommand,

	/**
	 * @param {efwdDal.Context} context
	 */
	construct: function (context)
	{
		this.__context = context;
	},

	members: {
		/**
		 * @param {efwdDal.entity.Activity[]} activities
		 * @param {efwdDal.entity.MActivitiesOwner} owner
		 * @param {String} category
		 */
		initialize: function (activities, owner, category)
		{
			this.__activities = activities;
			this.__owner = owner;
			this.__category = category;
		},

		/**
		 * @return {efwdDal.entity.MActivitiesOwner}
		 */
		getOwner: function()
		{
			return this.__owner;
		},

		getTitle: function (reverse)
		{
			return qx.bom.Template.tmpl("{{action}} existing activit{{acts}} {{toOrFrom}} {{owner}}", {
				"action": reverse ? "removing" : "adding",
				"acts": this.__activities.length > 1 ? "ies" : "y",
				"toOrFrom": reverse ? "from" : "to",
				"owner": this.__owner.getEntityType()
			});
		},

		execute: function ()
		{
			this.__activities.forEach(function (x){ this.__owner.addActivity(this.__category, x) }, this);
			this.__context.saveChanges();

			return true;
		},

		undo: function ()
		{
			this.__activities.forEach(function (x){ this.__owner.removeActivity(this.__category, x) }, this);
			this.__context.saveChanges();

			return true;
		}
	},

	defer: function (statics, members, properties)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}

});
