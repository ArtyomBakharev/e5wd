/**
 * Создаёт действие
 * @extends {efwdUI.commands.types.AbstractCommand}
 */
qx.Class.define("efwdUI.commands.types.CreateAction", {

	extend: efwdUI.commands.types.AbstractCommand,

	/**
	 * @param {efwdDal.repository.ActionsRepository} actionsRepository
	 * @param {efwdDal.Context} context
	 * @param {efwdDal.entity.Process} process
	 */
	construct: function (actionsRepository, context, process)
	{
		this.__actionsRepository = actionsRepository;
		this.__context = context;
		this.__process = process;
	},

	members: {
		/**
		 * @param {efwdDal.entity.Status} [sourceStatus=null]
		 * @param {efwdDal.entity.Status} [destStatus=null]
		 */
		initialize: function (sourceStatus, destStatus)
		{
			this.__sourceStatus = sourceStatus;
			this.__destStatus = destStatus;
		},

		/**
		 * @return {efwdDal.entity.Action}
		 */
		getAction: function()
		{
			return this.__action;
		},

		/**
		 * @return {efwdDal.entity.Status}
		 */
		getDestStatus: function()
		{
			return this.__destStatus;
		},

		/**
		 * @return {efwdDal.entity.Status}
		 */
		getSourceStatus: function()
		{
			return this.__sourceStatus;
		},

		getTitle: function ()
		{
			return "creating new action";
		},

		execute: function ()
		{
			this.__action = this.__actionsRepository.createAction(this.__process, null, this.__sourceStatus, this.__destStatus);
			this.__context.saveChanges();

			return true;
		},

		undo: function ()
		{
			this.__action.remove();
			this.__context.saveChanges();
		},

		redo: function ()
		{
			this.__action.restore();
			this.__context.saveChanges();
		}
	},

	defer: function (statics, members, properties)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}

});
