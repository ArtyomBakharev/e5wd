/**
 * Создаёт новую активность и опционально добавляет её к владельцу
 * @extends {efwdUI.commands.types.AbstractCommand}
 */
qx.Class.define("efwdUI.commands.types.CreateActivity", {

	extend: efwdUI.commands.types.AbstractCommand,

	/**
	 * @param {efwdDal.repository.ActivitiesRepository} activitiesRepository
	 * @param {efwdDal.Context} context
	 * @param {efwdDal.entity.Process} process
	 */
	construct: function (activitiesRepository, context, process)
	{
		this.__activitiesRepository = activitiesRepository;
		this.__context = context;
		this.__process = process;
	},

	members: {
		/**
		 * @param {String} type
		 * @param {efwdDal.entity.MActivitiesOwner} [owner=null]
		 * @param {String} [category=null]
		 */
		initialize: function (type, owner, category)
		{
			this.__type = type;
			this.__owner = owner;
			this.__category = category;
		},

		/**
		 * @return {efwdDal.entity.MActivitiesOwner}
		 */
		getOwner: function()
		{
			return this.__owner;
		},

		getTitle: function ()
		{
			return "creating new activity";
		},

		execute: function ()
		{
			this.__activities = this.__activitiesRepository.createActivity(this.__process, this.__type);
			this.__do();

			return true;
		},

		undo: function ()
		{
			this.__activities.remove();
			this.__context.saveChanges();
		},

		redo: function ()
		{
			this.__activities.restore();
			this.__do();
		},

		/**
		 * @private
		 */
		__do: function ()
		{
			if (this.__owner)
			{
				this.__owner.addActivity(this.__category, this.__activities);
			}
			this.__context.saveChanges();
		}
	},

	defer: function (statics, members, properties)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}

});
