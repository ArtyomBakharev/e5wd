/**
 * @extends {efwdUI.commands.types.AbstractCommand}
 */
qx.Class.define("efwdUI.commands.types.CreateProcess", {

	extend: efwdUI.commands.types.AbstractCommand,

	/**
	 * @param {efwdDal.repository.ProcessesRepository} processesRepository
	 * @param {efwdDal.Context} context
	 */
	construct: function (processesRepository, context)
	{
		this.__processesRepository = processesRepository;
		this.__context = context;
	},

	members: {
		/**
		 * @param {Boolean} sqlRepository
		 */
		initialize: function (sqlRepository)
		{
			this.__sqlRepository = sqlRepository;
		},

		getTitle: function ()
		{
			return "creating new process";
		},

		execute: function ()
		{
			this.__processesRepository.createProcess(this.__sqlRepository);
			this.__context.saveChanges();
			return true;
		},

		undo: function ()
		{
			throw new Error("[efwdUI.commands.types.CreateProcess] command canceling is not allowed");
		},

		redo: function ()
		{
			throw new Error("[efwdUI.commands.types.CreateProcess] command repeating is not allowed");
		}
	},

	defer: function (statics, members, properties)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}

});
