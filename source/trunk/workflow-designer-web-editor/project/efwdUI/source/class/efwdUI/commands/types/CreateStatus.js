/**
 * Создаёт новый статус
 * @extends {efwdUI.commands.types.AbstractCommand}
 */
qx.Class.define("efwdUI.commands.types.CreateStatus", {

	extend: efwdUI.commands.types.AbstractCommand,

	/**
	 * @param {efwdDal.repository.StatusesRepository} statusesRepository
	 * @param {efwdDal.Context} context
	 * @param {efwdDal.entity.Process} process
	 */
	construct: function (statusesRepository, context, process)
	{
		this.__statusesRepository = statusesRepository;
		this.__context = context;
		this.__process = process;
	},

	members: {

		/**
		 * @param {Function} positionCallback (efwdDal.entity.Status):Object
		 */
		initialize: function (positionCallback)
		{
			this.__positionCallback = positionCallback;
			this.__isFirstStatus = this.__process.getStatuses().getLength() == 0;
		},

		/**
		 * @return {efwdDal.entity.Status}
		 */
		getStatus: function()
		{
			return this.__status;
		},

		getTitle: function ()
		{
			return "creating new status";
		},

		execute: function ()
		{
			this.__status = this.__statusesRepository.createStatus(this.__process);

			var position = this.__positionCallback(this.__status);

			var metadata = this.__status.getSource().getMetadata();
			metadata.setX(position.x);
			metadata.setY(position.y);

			if (this.__isFirstStatus)
			{
				this.__status.setInitialStatus(true);
			}
			this.__context.saveChanges();

			return true;
		},

		undo: function ()
		{
			this.__status.remove();
			this.__context.saveChanges();
		},

		redo: function ()
		{
			this.__status.restore();
			if (this.__isFirstStatus)
			{
				this.__status.setInitialStatus(true);
			}
			this.__context.saveChanges();
		}
	},

	defer: function (statics, members, properties)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}

});
