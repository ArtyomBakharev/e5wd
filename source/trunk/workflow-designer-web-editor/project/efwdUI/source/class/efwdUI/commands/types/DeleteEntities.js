/**
 * Удаляет сущности
 * @extends {efwdUI.commands.types.AbstractCommand}
 */
qx.Class.define("efwdUI.commands.types.DeleteEntities", {

	extend: efwdUI.commands.types.AbstractCommand,

	/**
	 * @param {efwdDal.Context} context
	 * @param {library.IServiceLocator} serviceLocator
	 */
	construct: function (context, serviceLocator)
	{
		this.__context = context;
		this.__serviceLocator = serviceLocator;

		this.__deleteData = [];
	},

	members: {
		/**
		 * @param {efwdDal.entity.AbstractEntity[]} entities
		 */
		initialize: function (entities)
		{
			this.__entities = entities;
		},

		/**
		 * @return {efwdDal.entity.AbstractEntity[]}
		 */
		getEntities: function ()
		{
			return this.__entities;
		},

		getTitle: function ()
		{
			return qx.bom.Template.tmpl("deleting " + (this.__entities.length > 1 ? "items" : this.__entities[0].getEntityType()));
		},

		execute: function ()
		{
			this.__entities.forEach(function (entity)
			{
				if (!entity.isRemoved())
				{
					var dumper = this.__createDumper(entity);
					dumper && dumper.dump();
					entity.remove();

					this.__deleteData.push({
						dumper: dumper,
						entity: entity
					});
				}
			}, this);

			this.__deleteDataReversed = this.__deleteData.clone().reverse();
			this.__context.saveChanges();

			return true;
		},

		undo: function ()
		{
			this.__deleteDataReversed.forEach(function(x){ x.entity.restore(); x.dumper && x.dumper.restore(); });
			this.__context.saveChanges();
		},

		redo: function()
		{
			this.__deleteData.forEach(function(x){ x.dumper && x.dumper.dump(); x.entity.remove(); });
			this.__context.saveChanges();
		},

		/**
		 * @private
		 */
		__createDumper: function (entity)
		{
			var dumperClass;

			if (entity instanceof efwdDal.entity.Activity)
			{
				dumperClass = efwdUI.commands.types.statedumper.Activity;
			}
			else if (entity instanceof efwdDal.entity.Status)
			{
				dumperClass = efwdUI.commands.types.statedumper.Status;
			}

			if (dumperClass)
			{
				var dumper = this.__serviceLocator.getService(dumperClass);
				dumper.initialize(entity);
				return dumper;
			}
		}
	},

	defer: function (statics, members, properties)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}

});
