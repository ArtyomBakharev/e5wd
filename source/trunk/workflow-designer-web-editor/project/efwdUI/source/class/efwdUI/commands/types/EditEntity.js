/**
 * Изменение свойств сущности (просто обновляет сущность и сохраняет изменения на сервер)
 * @extends {efwdUI.commands.types.AbstractCommand}
 */
qx.Class.define("efwdUI.commands.types.EditEntity", {

	extend: efwdUI.commands.types.AbstractCommand,

	/**
	 * @param {efwdDal.Context} context
	 */
	construct: function (context)
	{
		this.__context = context;
	},

	members: {
		/**
		 * @param {efwdDal.entity.AbstractEntity} entity
		 * @param {Object} oldSource
		 * @param {String[]} keys
		 */
		initialize: function (entity, oldSource, keys)
		{
			this.__entity = entity;
			this.__keys = keys;
			this.__oldSource = this.__dumpSource(oldSource);
		},

		/**
		 * @return {efwdDal.entity.AbstractEntity}
		 */
		getEntity: function()
		{
			return this.__entity;
		},

		getTitle: function ()
		{
			return "changing " + this.__entity.getEntityType() + " properties";
		},

		/**
		 * @param {Boolean} [saveChanges=true] сохранять ли изменения на сервере
		 */
		execute: function (saveChanges)
		{
			if (saveChanges === undefined || saveChanges)
			{
				this.__newSource = this.__dumpSource(this.__entity.getSource());
				this.__context.saveChanges();
			}

			return true;
		},

		undo: function ()
		{
			library.data.Helper.restoreObjectToModel(this.__oldSource, this.__entity.getSource());
			this.__context.saveChanges();
		},

		redo: function ()
		{
			library.data.Helper.restoreObjectToModel(this.__newSource, this.__entity.getSource());
			this.__context.saveChanges();
		},

		/**
		 * @private
		 */
		__dumpSource: function (source)
		{
			var dump = source instanceof qx.core.Object ? qx.util.Serializer.toNativeObject(source) : Object.deepCopy(source);
			Object.keys(dump).exclude(this.__keys).forEach(function (x){ delete dump[x] });

			return dump;
		}
	},

	defer: function (statics, members, properties)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}

});
