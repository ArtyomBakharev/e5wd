/**
 * Открепление активностей от их владельца
 * @extends {efwdUI.commands.types.ReverseCommand}
 */
qx.Class.define("efwdUI.commands.types.RemoveActivities", {

	extend: efwdUI.commands.types.ReverseCommand,

	/**
	 * @param {efwdDal.Context} context
	 */
	construct: function (context, dialogsModel)
	{
		this.base(arguments, new efwdUI.commands.types.AddActivitiesToOwner(context));
	},

	members: {
		/**
		 * @param {efwdDal.entity.Activity[]} activities
		 * @param {efwdDal.entity.MActivitiesOwner} [owner=null]
		 * @param {String} [category=null]
		 */
		initialize: function (activities, owner, category)
		{
			this._reversableCommand.initialize(activities, owner, category);
		},

		/**
		 * @return {efwdDal.entity.MActivitiesOwner}
		 */
		getOwner: function()
		{
			return this._reversableCommand.getOwner();
		}
	},

	defer: function (statics, members, properties)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}

});
