/**
 * Команда, сама декларирующая как выполнять действие обратное тому, которое она совершает
 * @extends {efwdUI.commands.types.AbstractCommand}
 */
qx.Class.define("efwdUI.commands.types.ReversableCommand", {

	type: "abstract",

	extend: efwdUI.commands.types.AbstractCommand,

	members: {

		/**
		 * @param {Boolean} [reverse=false]
		 */
		getTitle: function (reverse){ this.abstractMethod(arguments) }

	}
});

