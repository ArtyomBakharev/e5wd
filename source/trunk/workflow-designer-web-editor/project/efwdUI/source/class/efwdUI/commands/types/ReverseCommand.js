/**
 * Команда основывается на {@link efwdUI.commands.types.ReversableCommand} и выполняет действие обратное ей
 * @extends {efwdUI.commands.types.AbstractCommand}
 */
qx.Class.define("efwdUI.commands.types.ReverseCommand", {

	type: "abstract",

	extend: efwdUI.commands.types.AbstractCommand,

	/**
	 * @param {efwdUI.commands.types.ReversableCommand} reversableCommand
	 */
	construct: function (reversableCommand)
	{
		this._reversableCommand = reversableCommand;
	},

	members: {

		getTitle: function ()
		{
			return this._reversableCommand.getTitle(true);
		},

		execute: function ()
		{
			return this._reversableCommand.undo();
		},

		undo: function ()
		{
			this._reversableCommand.execute();
		},

		/**
		 * @param {Boolean} redo
		 * @return {Boolean}
		 */
		needToNotifyUser: function (redo)
		{
			return this._reversableCommand.needToNotifyUser(redo);
		}

	}
});

