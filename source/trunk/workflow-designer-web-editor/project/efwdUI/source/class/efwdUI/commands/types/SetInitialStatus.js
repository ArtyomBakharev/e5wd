qx.Class.define("efwdUI.commands.types.SetInitialStatus", {

	extend: efwdUI.commands.types.AbstractCommand,

	/**
	 * @param {efwdDal.Context} context
	 */
	construct: function (context)
	{
		this.__context = context;
	},

	members: {
		/**
		 * @param {efwdDal.entity.Status} status
		 */
		initialize: function (status)
		{
			this.__status = status;
			this.__lastInitialStatusId = status.getProcess().getSource().getInitial_status_id();
			this.__name = this.__status.getSource().getName();
		},

		execute: function ()
		{
			this.__status.setInitialStatus(true);
			this.__context.saveChanges();
			return true;
		},

		getTitle: function ()
		{
			return "setting the initial status '" + this.__name + "'";
		},

		undo: function ()
		{
			this.__status.getProcess().getSource().setInitial_status_id(this.__lastInitialStatusId);
			this.__context.saveChanges();
		}
	},

	defer: function (statics, members, properties)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}

});
