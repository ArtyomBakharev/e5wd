/**
 * Базовый класс для классов производящих сохранение и восстановление состояния сущностей (ту его часть, которую они
 * сами не хранят сами)
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdUI.commands.types.statedumper.Abstract", {

	type: "abstract",

	extend: qx.core.Object,

	members: {
		/**
		 * Сохранить состояние сущности. Вызывается перед её удалением.
		 */
		dump: function (){ this.abstractMethod(arguments) },
		/**
		 * Восстановить состояние сущности. Вызывается после её восстановления.
		 */
		restore: function (){ this.abstractMethod(arguments) }
	}

});
