/**
 * Statedumper для активности. Запоминает её владельцев перед удалением и после восстановления добавляет её к ним.
 * @extends {efwdUI.commands.types.statedumper.Abstract}
 */
qx.Class.define("efwdUI.commands.types.statedumper.Activity", {

	extend: efwdUI.commands.types.statedumper.Abstract,

	/**
	 * @param {efwdDal.repository.ActivitiesRepository} activitiesRepsoitory
	 */
	construct: function (activitiesRepsoitory)
	{
		this.__activitiesRepsoitory = activitiesRepsoitory;
	},

	members: {
		/**
		 * @param {efwdDal.entity.Activity} activity
		 */
		initialize: function (activity)
		{
			this.__entity = activity;
		},

		dump: function ()
		{
			this.__owners = this.__activitiesRepsoitory.getOwnersOfActivity(this.__entity);
		},

		getOwners: function()
		{
			return this.__owners;
		},

		restore: function ()
		{
			this.__owners.forEach(function (owner)
			{
				owner.owner.addActivity(owner.category, this.__entity);
			}, this);
		}

	}

});
