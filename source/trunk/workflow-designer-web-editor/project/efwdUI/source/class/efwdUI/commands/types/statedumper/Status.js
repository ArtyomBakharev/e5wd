/**
 * Statedumper для статуса. Сохраняет и восстанавливает действия, связанные с ним.
 * @extends {efwdUI.commands.types.statedumper.Abstract}
 */
qx.Class.define("efwdUI.commands.types.statedumper.Status", {

	extend: efwdUI.commands.types.statedumper.Abstract,

	/**
	 * @param {library.IServiceLocator} serviceLocator
	 * @param {efwdDal.repository.ActionsRepository} actionsRepository
	 */
	construct: function (serviceLocator, actionsRepository)
	{
		this.__serviceLocator = serviceLocator;
		this.__actionsRepository = actionsRepository;
	},

	members: {
		/**
		 * @param {efwdDal.entity.Status} status
		 */
		initialize: function (status)
		{
			this.__status = status;
			if (this.__status.isInitialStatus())
			{
				this.__wasInitialStatus = true;
			}
		},

		dump: function ()
		{
			this.__actions = this.__actionsRepository.getActionsByStatus(this.__status);
		},

		restore: function ()
		{
			this.__actions.invoke("restore");
			if (this.__wasInitialStatus)
			{
				this.__status.setInitialStatus(true);
			}
		}
	},

	defer: function (statics, members, properties)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}

});
