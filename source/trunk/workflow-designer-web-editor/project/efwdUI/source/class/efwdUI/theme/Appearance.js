qx.Theme.define("efwdUI.theme.Appearance", {

	extend: aristo.theme.Appearance,

	appearances: {

		"window/icon": {
			style: function (states)
			{
				return {
					alignY: "middle",
					marginLeft: 5
				};
			}
		},

		"root": {
			base: true,
			style: function (state, style)
			{
				return {
					blockerOpacity: 0.5,
					blockerColor: "black"
				}
			}
		},

		"form-renderer-label": {},

		/**
		 * messagequeue-message appearance
		 * todo: move to library package
		 */
		"messagequeue-message": {
			style: function (states)
			{
				return {
					decorator : "messagequeue-message-box",
					shadow: "messagequeue-message-shadow",
					contentPadding: 10,
					padding: 10
				};
			}
		},

		"messagequeue-message/close-button": "window/close-button"
	}
});