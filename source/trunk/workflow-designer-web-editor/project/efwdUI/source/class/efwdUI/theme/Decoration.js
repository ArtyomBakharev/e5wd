qx.Theme.define("efwdUI.theme.Decoration", {

	extend: aristo.theme.Decoration,

	decorations: {
		//Aristo fixes
		"menubar": {
			decorator: qx.ui.decoration.Single,

			style: {
				// backgroundColor: 'background-menu',
				backgroundImage: "aristo/decoration/toolbar/toolbar-gradient.png",
				backgroundRepeat: "scale",

				width: [ 1, 1 ],
				color: "border-main",
				style: "solid"
			}
		},
		"button-css": {
			decorator: [
				qx.ui.decoration.MLinearBackgroundGradient,
				qx.ui.decoration.MBorderRadius,
				qx.ui.decoration.MSingleBorder,
				qx.ui.decoration.MBackgroundColor],
			style: {
				startColor: "button-gradient-start",
				endColor: "button-gradient-end",
				radius: 2,
				width: 1,
				color: "border-main",
				backgroundColor: qx.core.Environment.get("engine.name") == "mshtml" && qx.core.Environment.get("engine.version") == 9 ? "#E7E7E7" : "transparent"
			}
		},
		"button-hovered-css": {
			decorator: [
				qx.ui.decoration.MLinearBackgroundGradient,
				qx.ui.decoration.MBorderRadius,
				qx.ui.decoration.MSingleBorder,
				qx.ui.decoration.MBackgroundColor ],
			style: {
				startColor: "button-active-gradient-start",
				endColor: "button-active-gradient-end",
				radius: 2,
				width: 1,
				color: "border-main",
				backgroundColor: qx.core.Environment.get("engine.name") == "mshtml" && qx.core.Environment.get("engine.version") == 9 ? "#AFD7EE" : "transparent"
			}
		},
		"button-pressed-css": {
			decorator: [
				qx.ui.decoration.MLinearBackgroundGradient,
				qx.ui.decoration.MBorderRadius,
				qx.ui.decoration.MSingleBorder,
				qx.ui.decoration.MBackgroundColor ],
			style: {
				startColor: "button-gradient-end",
				endColor: "button-gradient-start",
				radius: 2,
				width: 1,
				color: "border-main",
				backgroundColor: qx.core.Environment.get("engine.name") == "mshtml" && qx.core.Environment.get("engine.version") == 9 ? "#D6D6D6" : "transparent"
			}
		},

		/**
		 * messagequeue-message decoration
		 * todo: move to library package
		 */
		"messagequeue-message-shadow" : {
			decorator : qx.ui.decoration.Grid,

			style : {
				baseImage : "aristo/decoration/shadow/shadow-window.png",
				insets : [ 7, 7, 7, 7 ]
			}
		},

		"messagequeue-message-box" :
		{
			decorator : [
				qx.ui.decoration.MBackgroundColor,
				qx.ui.decoration.MBorderRadius,
				qx.ui.decoration.MSingleBorder
			],

			style : {
				backgroundColor : "white",
				radius : 4,
				color : "silver",
				width: 1
			}
		}
	}
});