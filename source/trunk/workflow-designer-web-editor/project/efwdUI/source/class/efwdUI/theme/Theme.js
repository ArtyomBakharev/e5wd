qx.Theme.define("efwdUI.theme.Theme",
	{
		meta: {
			color: efwdUI.theme.Color,
			decoration: efwdUI.theme.Decoration,
			font: efwdUI.theme.Font,
			icon: qx.theme.icon.Oxygen,
			appearance: efwdUI.theme.Appearance
		}
	});