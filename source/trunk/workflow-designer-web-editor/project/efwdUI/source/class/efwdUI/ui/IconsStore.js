/*
 #asset(qx/icon/${qx.icontheme}/22/actions/go-last.png)
 #asset(qx/icon/${qx.icontheme}/32/actions/go-last.png)
 #asset(qx/icon/${qx.icontheme}/22/actions/document-send.png)
 #asset(qx/icon/${qx.icontheme}/32/actions/document-send.png)
 #asset(qx/icon/${qx.icontheme}/22/actions/document-properties.png)
 #asset(qx/icon/${qx.icontheme}/32/actions/document-properties.png)
 #asset(qx/icon/${qx.icontheme}/22/apps/office-web.png)
 #asset(qx/icon/${qx.icontheme}/32/apps/office-web.png)
 #asset(qx/icon/${qx.icontheme}/22/actions/system-run.png)
 #asset(qx/icon/${qx.icontheme}/32/actions/system-run.png)

 #asset(qx/icon/${qx.icontheme}/32/actions/edit-delete.png)
 #asset(qx/icon/${qx.icontheme}/22/actions/edit-delete.png)
 #asset(qx/icon/${qx.icontheme}/32/actions/edit-redo.png)
 #asset(qx/icon/${qx.icontheme}/32/actions/edit-undo.png)
 #asset(qx/icon/${qx.icontheme}/32/actions/edit-copy.png)
 #asset(qx/icon/${qx.icontheme}/22/actions/edit-copy.png)
 #asset(qx/icon/${qx.icontheme}/32/actions/edit-paste.png)
 #asset(qx/icon/${qx.icontheme}/22/actions/edit-paste.png)
 #asset(qx/icon/${qx.icontheme}/32/actions/go-previous.png)
 #asset(qx/icon/${qx.icontheme}/32/actions/edit-select-all.png)
 #asset(qx/icon/${qx.icontheme}/22/actions/edit-select-all.png)
 #asset(efwdUI/icon/initial-status.png)
 */
qx.Class.define("efwdUI.ui.IconsStore", {
	type: "static",

	statics: {

		get: function (key, size)
		{
			return this.icons[key].replace("{size}", size);
		},

		icons: {
			action: "icon/{size}/actions/go-last.png",
			activity: "icon/{size}/actions/document-send.png",
			processProperties: "icon/{size}/actions/document-properties.png",
			process: "icon/{size}/actions/system-run.png",
			status: "icon/{size}/apps/office-web.png",

			"delete": "icon/{size}/actions/edit-delete.png",
			redo: "icon/{size}/actions/edit-redo.png",
			undo: "icon/{size}/actions/edit-undo.png",
			copy: "icon/{size}/actions/edit-copy.png",
			paste: "icon/{size}/actions/edit-paste.png",
			goBackToProcesses: "icon/{size}/actions/go-previous.png",
			selectAll: "icon/{size}/actions/edit-select-all.png",
			initialStatus: "efwdUI/icon/initial-status.png"
		}
	}
});
