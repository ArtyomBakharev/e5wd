/**
 * Диалог, отображающий список действий
 * @extends {efwdUI.ui.dialog.EntitiesListDialog}
 */
qx.Class.define("efwdUI.ui.dialog.ActionsListDialog", {

	extend: efwdUI.ui.dialog.EntitiesListDialog,

	/**
	 * @param {library.IServiceLocator} serviceLocator
	 */
	construct: function (serviceLocator)
	{
		this.__serviceLocator = serviceLocator;
	},

	members: {

		/**
		 * @protected
		 */
		_configureWindow: function (window)
		{
			this.base(arguments, window);

			window.set({
				caption: this._viewModel.isAllProcesses() ? "Actions" : "Process actions",
				icon: efwdUI.ui.IconsStore.get("action", 22)
			});
		},

		/**
		 * @protected
		 */
		_createInnerBody: function (container)
		{
			this.__entitiesList = this.__serviceLocator.getService(efwdUI.ui.dialog.entitieslist.ActionsList);
			this.__entitiesList.initialize(this._viewModel);
			container.add(this.__entitiesList, {flex: 1});
		},

		/**
		 * @protected
		 */
		_destroy: function (){ this.__entitiesList.dispose(); }
	}

});
