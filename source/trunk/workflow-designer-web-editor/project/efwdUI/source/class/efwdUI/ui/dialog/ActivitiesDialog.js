/*
 #ignore(efwdConfig)
 #ignore(efwdConfig.*)
 */

/**
 * Диалог со списками активностей владельца с разделением по категориям
 * @extends {efwdUI.ui.dialog.Dialog}
 */
qx.Class.define("efwdUI.ui.dialog.ActivitiesDialog", {

	extend: efwdUI.ui.dialog.Dialog,

	/**
	 * @param {library.IServiceLocator} serviceLocator
	 */
	construct: function (serviceLocator)
	{
		this.__serviceLocator = serviceLocator;

		this.__listenersStore = new library.ListenersStore();
	},

	members: {
		/**
		 * @param {efwdUI.ui.dialog.viewmodels.ActivitiesListsViewModel} viewModel
		 */
		initialize: function (viewModel)
		{
			this.__viewModel = viewModel;
			this.__viewModel.getOwner().addListener("removed", function (){ this.close() }, this);
		},

		/**
		 * @protected
		 */
		_configureWindow: function (window)
		{
			window.set({
				width: efwdConfig.efwdUI.ui.$style.editor.itemsList.window.width,
				height: efwdConfig.efwdUI.ui.$style.editor.itemsList.window.height,
				icon: efwdUI.ui.IconsStore.get("activity", 22),
				contentPadding: [5, 2, 2, 2]
			});

			var entityType = this.__viewModel.getOwner().getEntityType();
			this.__listenersStore.addBinding(this.__viewModel.getOwner().getSource(), "name", window, "caption",
				function (name)
				{
					return "Activities of " + entityType + " '" + name + "'";
				});
		},

		/**
		 * @protected
		 */
		_createBody: function (container)
		{
			this.__tabView = new qx.ui.tabview.TabView();

			//opera focus bugfix
			if (qx.core.Environment.get("browser.name") == "opera")
			{
				this.__tabView.addListener("changeSelection", function ()
				{
					setTimeout(function ()
					{
						document.activeElement && document.activeElement.blur && document.activeElement.blur();
					}, 0);
				});
			}

			this.__entitiesLists = [];
			this.__viewModel.getListsViewModels().forEach(function (viewModel)
			{

				var list = this.__serviceLocator.getService(efwdUI.ui.dialog.entitieslist.ActivitiesList);
				list.initialize(viewModel);
				this.__entitiesLists.push(list);

				var page = new qx.ui.tabview.Page(viewModel.getCategory().humanize().upFirstLetterInWords());
				page.set({
					layout: new qx.ui.layout.VBox()
				});
				page.add(list, {flex: 1});

				this.__tabView.add(page);

			}, this);

			container.add(this.__tabView, {flex: 1});
		},

		/**
		 * @protected
		 */
		_destroy: function ()
		{
			this.__entitiesLists.forEach(function (x){ x.dispose() });
			this.__listenersStore.dispose();
		}
	}

});
