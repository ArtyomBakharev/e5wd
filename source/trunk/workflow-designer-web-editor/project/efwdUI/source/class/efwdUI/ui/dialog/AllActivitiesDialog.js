/**
 * Диалог, отображающий список активностей без разделения их на категории
 * @extends {efwdUI.ui.dialog.EntitiesListDialog}
 */
qx.Class.define("efwdUI.ui.dialog.AllActivitiesDialog", {

	extend: efwdUI.ui.dialog.EntitiesListDialog,

	/**
	 * @param {library.IServiceLocator} serviceLocator
	 */
	construct: function (serviceLocator)
	{
		this.__serviceLocator = serviceLocator;
	},

	members: {

		/**
		 * @protected
		 */
		_configureWindow: function (window)
		{
			this.base(arguments, window);

			window.set({
				caption: "All activities",
				icon: efwdUI.ui.IconsStore.get("activity", 22)
			});
		},

		/**
		 * @protected
		 */
		_createInnerBody: function (container)
		{
			this.__list = this.__serviceLocator.getService(efwdUI.ui.dialog.entitieslist.ActivitiesList);
			this.__list.initialize(this._viewModel);
			container.add(this.__list, {flex: 1});
		},

		/**
		 * @protected
		 */
		_destroy: function (){ this.__list.dispose() }
	}

});
