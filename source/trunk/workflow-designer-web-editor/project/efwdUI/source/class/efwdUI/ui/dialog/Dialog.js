/**
 * Базовый класс для диалоговых окон редакторов
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdUI.ui.dialog.Dialog", {

	extend: qx.core.Object,
	type: "abstract",

	events: {
		"destroyed": "qx.event.type.Event"
	},

	members: {

		/**
		 * Закрыть диалог
		 */
		close: function ()
		{
			this.__window.close();
		},

		/**
		 * Активация окна, либо его закрытие, если оно уже активировано
		 */
		activateOrClose: function ()
		{
			this.__window.getActive() ? this.close() : this.__window.setActive(true);
		},

		/**
		 * отобразить и открыть окно
		 */
		render: function ()
		{
			this.__createWindow();
			this._createBody(this.__window);

			//opera focus bugfix
			if (qx.core.Environment.get("browser.name") == "opera")
			{
				this.__window.addListener("appear", function ()
				{
					document.activeElement && document.activeElement.blur && document.activeElement.blur();
				});
			}

			library.ui.Helper.openAndCenterWindow(this.__window, true);
		},

		/**
		 * В этом методе классы-наследники могут сконфигурировать окно
		 * @virtual
		 * @param {qx.ui.window.Window} window
		 * @protected
		 */
		_configureWindow: function (window){},

		/**
		 * Отрисовка содержимого диалога в container
		 * @abstract
		 * @param {qx.ui.core.Widget} container
		 * @protected
		 */
		_createBody: function (container){ throw new Error("[efwdUI.ui.dialog.Dialog#_createBody] Method is abstract!"); },

		/**
		 * Метод вызывается перед разрушением окна
		 * @virtual
		 * @protected
		 */
		_destroy: function (){},

		/**
		 * @private
		 */
		__createWindow: function ()
		{
			this.__window = new qx.ui.window.Window();
			this.__window.set({
				modal: false,
				resizable: true,
				allowMaximize: false,
				allowMinimize: false,
				allowClose: true,
				showMaximize: false,
				showMinimize: false,
				showClose: true,
				layout: new qx.ui.layout.VBox(),
				useResizeFrame: false
			});

			this.__window.addListener("close", function ()
			{
				this._destroy();
				this.__window.destroy();
				this.fireEvent("destroyed");
			}, this);

			this._configureWindow(this.__window);

			return this.__window;
		}
	}
});
