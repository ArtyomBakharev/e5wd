/*
 #ignore(efwdConfig.)
 #ignore(efwdConfig.*)
 */

/**
 * Диалог отображающий список сущностей для их редактирования или выбора
 * @extends {efwdUI.ui.dialog.Dialog}
 */
qx.Class.define("efwdUI.ui.dialog.EntitiesListDialog", {

	type: "abstract",

	extend: efwdUI.ui.dialog.Dialog,

	members: {
		/**
		 * @param {efwdUI.ui.dialog.viewmodels.ActivitiesListViewModel} viewModel
		 */
		initialize: function (viewModel)
		{
			this._viewModel = viewModel;
		},

		/**
		 * @protected
		 */
		_configureWindow: function (window)
		{
			window.set({
				width: efwdConfig.efwdUI.ui.$style.editor.itemsList.window.width,
				height: efwdConfig.efwdUI.ui.$style.editor.itemsList.window.height,
				modal: this._viewModel.getMode() == "select"
			});
		},

		/**
		 * @protected
		 */
		_createBody: function (container)
		{
			this._createInnerBody(container);

			if (this._viewModel.getMode() == "select")
			{
				var pane = new qx.ui.container.Composite(new qx.ui.layout.HBox(10, "right"));
				pane.set({
					padding: 10
				});

				var okButton = new qx.ui.form.Button("Ok");
				this._viewModel.bind("selection[0]", okButton, "enabled", {converter: function(x){ return !!x }});
				okButton.addListener("execute", function (){ this.close() }, this);
				pane.add(okButton);

				var cancelButton = new qx.ui.form.Button("Cancel");
				cancelButton.addListener("execute", function ()
				{
					this._viewModel.getSelection().removeAll();
					this.close();
				}, this);
				pane.add(cancelButton);

				container.add(pane);
			}
		},

		/**
		 * Отрисовка внутренних элементов диалога
		 * @abstract
		 * @param {qx.ui.core.Widget} container
		 * @protected
		 */
		_createInnerBody: function (container){ throw new Error("[efwdUI.ui.dialog.EntitiesListDialog#_createInnerBody] Method is abstract!"); }
	}

});
