/**
 * @extends {efwdUI.ui.dialog.Dialog}
 */
qx.Class.define("efwdUI.ui.dialog.EntitiesListsDialog", {

	type: "abstract",
	extend: efwdUI.ui.dialog.Dialog,

	/**
	 * @param {library.IServiceLocator} serviceLocator
	 */
	construct: function (serviceLocator)
	{
		this.__serviceLocator = serviceLocator;

		this.__listenersStore = new library.ListenersStore();
	},

	members: {
		/**
		 * @param {efwdUI.ui.dialog.viewmodels.EntitiesListsViewModel} viewModel
		 */
		initialize: function (viewModel)
		{
			this._viewModel = viewModel;
		},

		/**
		 * @protected
		 */
		_configureWindow: function (window)
		{
			window.set({
				width: efwdConfig.efwdUI.ui.$style.editor.itemsList.window.width,
				height: efwdConfig.efwdUI.ui.$style.editor.itemsList.window.height,
				contentPadding: [5, 2, 2, 2]
			});
		},

		/**
		 * @protected
		 */
		_createBody: function (container)
		{
			this.__tabView = new qx.ui.tabview.TabView();

			this.__entitiesLists = [];
			this._viewModel.getListsViewModels().forEach(function (viewModel)
			{

				var list = this._createList(viewModel);
				this.__entitiesLists.push(list);

				var page = new qx.ui.tabview.Page(viewModel.getCategory().humanize().upFirstLetterInWords());
				page.set({
					layout: new qx.ui.layout.VBox()
				});
				page.add(list, {flex: 1});

				this.__tabView.add(page);

			}, this);

			container.add(this.__tabView, {flex: 1});
		},

		/**
		 * @param {efwdUI.ui.dialog.viewmodels.EntitiesListViewModel} viewModel
		 * @return {efwdUI.ui.dialog.entitieslist.EntitiesList}
		 * @private
		 */
		_createList: function(viewModel)
		{
			this.abstractMethod(arguments);
		},

		/**
		 * @protected
		 */
		_destroy: function ()
		{
			this.__entitiesLists.forEach(function (x){ x.dispose() });
			this.__listenersStore.dispose();
		}
	}

});
