/*
 #ignore(efwdConfig)
 #ignore(efwdConfig.*)
 */

/**
 * Диалог редактирования свойств процесса
 * использует {@link efwdUI.ui.entityediting.view.EntityForm} для создания формы
 * @extends {efwdUI.ui.dialog.Dialog}
 */
qx.Class.define("efwdUI.ui.dialog.ProcessDialog", {

	extend: efwdUI.ui.dialog.Dialog,

	/**
	 * @param {library.IServiceLocator} serviceLocator
	 */
	construct: function (serviceLocator)
	{
		this.__serviceLocator = serviceLocator;
	},

	members: {
		/**
		 * @param {efwdUI.ui.entityediting.viewModel.EntityEditor} viewModel
		 */
		initialize: function (viewModel)
		{
			this.__viewModel = viewModel;
		},

		/**
		 * @protected
		 */
		_configureWindow: function (window)
		{
			window.set({
				width: efwdConfig.efwdUI.ui.$style.editor.entityForm.window.width,
				caption: "Process properties",
				icon: efwdUI.ui.IconsStore.get("processProperties", 22)
			});
		},

		/**
		 * @protected
		 */
		_createBody: function (container)
		{
			var group = new qx.ui.groupbox.GroupBox();
			group.setLayout(new qx.ui.layout.Grow());

			this.__form = this.__serviceLocator.getService(efwdUI.ui.entityediting.view.EntityForm);
			this.__form.initialize(this.__viewModel);
//			group.add(this.__form.render());

			container.add(this.__form.render());
		},

		/**
		 * @protected
		 */
		_destroy: function ()
		{
			this.__form.save();
			this.__form.dispose();
		},

		/**
		 * @return {String}
		 * @protected
		 */
		_getTitle: function ()
		{
			return "Process properties";
		}
	},

	defer: function (statics, members, properties)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}

});
