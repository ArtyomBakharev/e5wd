/**
 * Список действий
 * @extends {efwdUI.ui.dialog.entitieslist.EntitiesList}
 */
qx.Class.define("efwdUI.ui.dialog.entitieslist.ActionsList", {

	extend: efwdUI.ui.dialog.entitieslist.EntitiesList,

	members: {

		/**
		 * @protected
		 */
		_renderHeader: function ()
		{
			if (this._viewModel.isAllProcesses())
			{
				this.add(new qx.ui.basic.Label("Choose process:"));
				this.__processSelect = new library.ui.widget.SelectBox();
				this.__processSelect.setNullable(false);
				this.__processSelect.set({
					enabled: false,
					marginBottom: 5
				});
				this.__processSelect.setItems(["loading..."]);
				this.add(this.__processSelect);

				var selectedEntity = this._viewModel.getSelection().toArray()[0];

				this._viewModel.getProcesses().deferred().next(function (processes)
				{
					this.__processSelect.setVisibility("visible");
					this.__processSelect.setItems(
						processes.toArray()
							.clone().sort(function (a, b)
							{
								a = a.getSource().getName();
								b = b.getSource().getName();
								return a > b ? 1 : a < b ? -1 : 0;
							})
							.map(function (x){ return [x, x.getSource().getName()] })
					);

					this.__processSelect.set({
						enabled: true,
						value: selectedEntity ? selectedEntity.getProcess() : this._viewModel.getCurrentProcess()
					});
					this.__processSelect.bind("value", this._viewModel, "currentProcess");
					if (selectedEntity)
					{
						this._viewModel.getSelection().removeAll();
						this._viewModel.getSelection().push(selectedEntity);
					}

				}, this);
			}
		}
	}

});
