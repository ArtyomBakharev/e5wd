/*
 #ignore(efwdConfig)
 #ignore(efwdConfig.*)
 */

/**
 * Список активностей
 * @extends {efwdUI.ui.dialog.entitieslist.EntitiesList}
 */
qx.Class.define("efwdUI.ui.dialog.entitieslist.ActivitiesList", {

	extend: efwdUI.ui.dialog.entitieslist.EntitiesList,

	/**
	 * @param {efwdUI.behavior.GeneralCommandsProvider} generalCommandsProvider
	 */
	construct: function (generalCommandsProvider)
	{
		this.base(arguments);
		this.__generalCommandsProvider = generalCommandsProvider;
	},

	members: {
		/**
		 * @param {efwdUI.ui.dialog.viewmodels.ActionsListViewModel} viewModel
		 */
		initialize: function (viewModel)
		{
			this.base(arguments, viewModel, {
				includeSubtype: true
			});
		},

		/**
		 * @param {qx.ui.menubar.MenuBar} toolbar
		 * @protected
		 */
		_addAddEntityMenu: function (toolbar)
		{
			toolbar.add(this.__createAddActivityButton());
			if (this._viewModel.getOwner())
			{
				toolbar.add(this.__createAddExistingActivityButton());
			}
		},

		/**
		 * @protected
		 */
		_renderHeader: function ()
		{
			this.__typeSelect = new library.ui.widget.SelectBox("all types");
			this.__typeSelect.setNullable(true);
			this.__typeSelect.setItems(
				efwdConfig.efwdCommon.data.types.$activityTypes.map(function (item){ return [item, item.humanize().toLowerCase()] })
			);
			this.__typeSelect.setMarginBottom(5);
			this.add(this.__typeSelect);

			this.__typeSelect.bind("value", this._viewModel, "activityType");
		},

		/**
		 * @private
		 */
		__createAddActivityButton: function ()
		{
			var addActivityMenu = new qx.ui.menu.Menu();

			efwdConfig.efwdCommon.data.types.$activityTypes.forEach(function (activityType)
			{
				var button = new qx.ui.menu.Button(activityType.humanize().firstUp());
				button.addListener("execute", function ()
				{
					if (activityType != this.__typeSelect.getValue())
					{
						this.__typeSelect.setValue(null);
					}
					this._viewModel.addEntity({type: activityType});

				}, this);

				addActivityMenu.add(button);

			}, this);

			return new qx.ui.menubar.Button("Add new", "icon/16/actions/list-add.png", addActivityMenu);
		},

		/**
		 * @private
		 */
		__createAddExistingActivityButton: function ()
		{
			var button = new qx.ui.menubar.Button("Add existing", "icon/16/actions/list-add.png");

			var command = this.__generalCommandsProvider.createSelectActivitiesCommand((function (activities)
			{
				this.__typeSelect.setValue(null);
				this._viewModel.addExistingEntities(activities);
			}).bind(this));
			button.setCommand(command);

			return button;
		}
	}

});
