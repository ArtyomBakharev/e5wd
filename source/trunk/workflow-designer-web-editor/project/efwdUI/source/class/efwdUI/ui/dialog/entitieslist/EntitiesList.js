/*
 #asset(qx/icon/${qx.icontheme}/16/actions/list-add.png)
 #asset(qx/icon/${qx.icontheme}/16/actions/list-remove.png)
 */

/**
 * Виджет, отображающий список сущностей
 * @extends {qx.ui.container.Composite}
 */
qx.Class.define("efwdUI.ui.dialog.entitieslist.EntitiesList", {

	extend: qx.ui.container.Composite,

	construct: function ()
	{
		this.base(arguments, new qx.ui.layout.VBox());

		this._listenersStore = new library.ListenersStore();
	},

	members: {
		/**
		 * @param {efwdUI.ui.dialog.viewmodels.EntitiesListViewModel} viewModel
		 * @param {Object} [options={}]
		 * @param {Object[]} options.buttons массив кнопок для каждой строки списка в режиме редактирования.
		 * Каждый объект в массиве должен содержать поля: name - машинное имя кнопки; title - наименование; execute - функция,
		 * выполняющая по нажатию кнопки, принимает единственный параметр entity - сущность
		 * @param {Object} [options.events={}]
		 * @param {Function} [options.events.dblclick=null] (entity)
		 * @param {Boolean} [options.confirmDeletion=false]
		 * @param {Boolean} [options.includeSubtype=false]
		 */
		initialize: function (viewModel, options)
		{
			options === undefined && (options = {});
			if (!options.events)
			{
				options.events = {};
			}

			this._viewModel = viewModel;

			this.__initializeButtons(options.buttons);

			//!options.rowTmpl && (options.rowTmpl = "{{source.name}}");

			this.__options = options;

			this.__render();
		},
		/**
		 * метод, добавляющий кнопки создания/добавления сущностей в меню
		 * @virtual
		 * @param {qx.ui.menubar.MenuBar} toolbar
		 * @protected
		 */
		_addAddEntityMenu: function (toolbar)
		{
			var addEntityButton = new qx.ui.menubar.Button("Add new", "icon/16/actions/list-add.png");
			addEntityButton.addListener("execute", function (){ this._viewModel.addEntity() }, this);
			toolbar.add(addEntityButton);
		},

		/**
		 * @protected
		 */
		_createChildControlImpl: function (id)
		{
			var control;
			switch (id)
			{
				case "entities-list":
					control = this.__createList();
					this.add(control, {flex: 1});
					break;
			}

			return control || this.base(arguments, id);
		},

		/**
		 * метод можно переопределить для отрисовки произвольной шапки виджета
		 * @virtual
		 * @protected
		 */
		_renderHeader: function (){},

		/**
		 * @private
		 */
		__bindListSelection: function ()
		{
			this._listenersStore.addBinding(this._viewModel, "selection", this._list, "selection");
		},

		/**
		 * @private
		 */
		__bindListToModel: function ()
		{
			var loader = new library.bom.Loader();

			this._listenersStore.addBinding(this._viewModel, "entitiesArray", this._list, "model",
				{converter: (function (x)
				{
					//loader.block.defer(loader, this._list.getContainerElement().getDomElement());

					if (this.__sortedArray)
					{
						this.__sortedArray.dispose();
					}

					this.__sortedArray = new library.data.SortedArray(x.getDataArray(), function (a, b)
					{
						a = a.getSource().getName();
						b = b.getSource().getName();
						return a > b ? 1 : a < b ? -1 : 0;
					});

					return this.__sortedArray;
				}).bind(this)});
		},

		/**
		 * @private
		 */
		__bindItemLabel: function (controller, item, id)
		{
			controller.bindProperty("source.name", "label", {converter: (function (name)
			{
				var entity = item.getEntityPart();

				var buttons = qx.bom.Template.tmpl(
					"<a href='javascript:;' data-entityHash='" + Object.getUniqueId(entity) + "' class='{{name}}-button' style='color: green;'>{{title}}</a>&nbsp;&nbsp;&nbsp;",
					this.__buttons
				);

				var text = name;
				if (this.__options.includeSubtype)
				{
					text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i style='color:#999999'>(" + entity.getSource().get$type().humanize().toLowerCase() + ")</i>";
				}

				return "<div style='white-space: nowrap;'>" + buttons + " " + text + "</div>";
			}).bind(this)}, item, id);
		},

		/**
		 * @private
		 */
		__createList: function ()
		{
			this._list = new qx.ui.list.List();
			this.__bindListToModel();

			this._listenersStore.addBinding(this._viewModel, "multiSelect", this._list, "selectionMode",
				function (x){ return x ? "multi" : "single" });

			this._list.setDelegate({
				configureItem: (function (item)
				{
					item.setRich(true);

					item.registerObjectPart("entityPart");
					if (this.__options.events.dblclick)
					{
						item.addListener("dblclick", function ()
						{
							this.__options.events.dblclick(item.getEntityPart());
						}, this);
					}
				}).bind(this),

				bindItem: (function (controller, item, id)
				{
					controller.bindProperty(null, "entityPart", {}, item, id);

					this.__bindItemLabel(controller, item, id);
				}).bind(this)
			});

			//hot keys
			this._list.addListener("keydown", function (e)
			{
				if (e.getKeyIdentifier() == "Delete")
				{
					this.__getDeleteSelectionCommand().execute(this._list);
					e.preventDefault();
				}
				else if (e.getKeyIdentifier() == "A" && e.isCtrlPressed())
				{
					this._viewModel.selectAll();
					e.preventDefault();
				}
			}, this);

			this.__bindListSelection();
			this.__setUpItemButtons();

			return this._list;
		},

		/**
		 * @private
		 */
		__createToolbar: function ()
		{
			this.__toolbar = new qx.ui.menubar.MenuBar();
			this.__toolbar.addSpacer();

			this._addAddEntityMenu(this.__toolbar);

			var deleteActionButton = new qx.ui.menubar.Button(this._viewModel.getDeletionType().firstUp(),
				"icon/16/actions/list-remove.png");
			deleteActionButton.setCommand(this.__getDeleteSelectionCommand());
			this.__toolbar.add(deleteActionButton);

			return this.__toolbar;
		},

		/**
		 * @param {efwdDal.entity.AbstractEntity[]} entities
		 * @private
		 */
		__deleteEntities: function (entities)
		{
			if (this.__options.confirmDeletion)
			{
				library.ui.Dialog.confirm(
					entities.length > 1 ? "Delete items" : "Delete " + entities[0].getEntityType(),

					"Do you realy want to delete " + (entities.length > 1
						? "selected items"
						: entities[0].getEntityType() + " '" + entities[0].getSource().getName() + "'") + "?",

					function (kind)
					{
						if (kind == library.ui.Dialog.KIND_OK)
						{
							this._viewModel.deleteEntities(entities);
						}
					}, this);
			}
			else
			{
				this._viewModel.deleteEntities(entities);
			}
		},

		/**
		 *
		 * @return {library.ui.Command}
		 * @private
		 */
		__getDeleteSelectionCommand: function ()
		{
			if (!this.__deleteSelectionCommand)
			{
				this.__deleteSelectionCommand = new library.ui.Command(function ()
				{
					this.__deleteEntities(this._list.getSelection().toArray().clone());
				}, this);
			}

			return this.__deleteSelectionCommand;
		},

		/**
		 * @private
		 */
		__initializeButtons: function (buttons)
		{
			if (this._viewModel.getMode() != "edit")
			{
				this.__buttons = []
			}
			else
			{
				this.__buttons = this._viewModel.isReadOnly() ? []
					: [
					{
						name: this._viewModel.getDeletionType(),
						title: this._viewModel.getDeletionType(),
						execute: (function (entity)
						{
							this.__deleteEntities([entity])
						}).bind(this)
					}
				];

				if (buttons)
				{
					this.__buttons.append(buttons);
				}
			}
		},

		/**
		 * @private
		 */
		__render: function ()
		{
			this._renderHeader();

			if (this._viewModel.getMode() == "edit" && !this._viewModel.isReadOnly())
			{
				this.add(this.__createToolbar());
			}

			this._createChildControl("entities-list");

			this.__setUpCommands();
			this.__setUpLoader();
		},

		/**
		 * @private
		 */
		__setUpCommands: function ()
		{
			if (this._viewModel.getMode() == "edit")
			{
				this._list.bind("selection[0]", this.__getDeleteSelectionCommand(), "enabled",
					{converter: function (x){ return !!x }});
			}
		},

		/**
		 * @private
		 */
		__setUpItemButtons: function ()
		{
			this._list.addListener("appear", function ()
			{
				this._list.getContentElement().addListener("click", function (e)
				{
					var collection = qx.bom.Collection.create(e.getTarget());
					this.__buttons.forEach(function (button)
					{
						if (collection.hasClass(button.name + "-button"))
						{
							var entityHash = collection.getAttribute("data-entityHash");
							var entity = this._viewModel.getEntitiesArray().toArray().filter(function (x){ return Object.getUniqueId(x) == entityHash })[0];
							button.execute(entity);
						}
					}, this);

					e.preventDefault();

				}, this, true);

			}, this);
		},

		__setUpLoader: function ()
		{
			this._list.addListener("appear", function ()
			{
				var loader = new library.bom.Loader(this._list.getContainerElement().getDomElement());
				this._listenersStore.addBinding(this._viewModel, "entitiesArray.loaded", loader, "blocked",
					function (x){ return !x });
			}, this);

			this.__toolbar && this.__toolbar.addListener("appear", function ()
			{
				var loader = new library.bom.Loader(this.__toolbar.getContainerElement().getDomElement(), false);
				this._listenersStore.addBinding(this._viewModel, "entitiesArray.loaded", loader, "blocked",
					function (x){ return !x });
			}, this);
		}
	},

	destruct: function ()
	{
		if (this.__sortedArray)
		{
			this.__sortedArray.dispose();
		}

		this._list.setSelection(new qx.data.Array());
		this._list.setModel(null);
		this._list.removeAllBindings();
		this._listenersStore.dispose();
	},

	defer: function (statics, members, properties)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}

});
