/**
 * Список процессов
 * @extends {efwdUI.ui.dialog.entitieslist.EntitiesList}
 */
qx.Class.define("efwdUI.ui.dialog.entitieslist.ProcessesList", {

	extend: efwdUI.ui.dialog.entitieslist.EntitiesList,

	members: {
		/**
		 * @param {qx.ui.menubar.MenuBar} toolbar
		 * @protected
		 */
		_addAddEntityMenu: function (toolbar)
		{
			toolbar.add(this.__createAddProcessButton());
		},

		/**
		 * @protected
		 */
		_createChildControlImpl: function (id)
		{
			var control;
			switch (id)
			{
				case "create-process-menu":
					control = this.__createAddProcessMenu();
					break;
			}

			return control || this.base(arguments, id);
		},

		/**
		 * @private
		 */
		__createAddProcessButton: function ()
		{
			return new qx.ui.menubar.Button("Create process", "icon/16/actions/list-add.png", this._createChildControl("create-process-menu"));
		},

		/**
		 * @private
		 */
		__createAddProcessMenu: function()
		{
			var addProcessMenu = new qx.ui.menu.Menu();

			[
				{title: "SQL repository", sqlRepository: true},
				{title: "XML repository", sqlRepository: false}
			].forEach(function (item)
				{
					var button = new qx.ui.menu.Button(item.title);
					button.addListener("execute", function ()
					{
						this._viewModel.addEntity({sqlRepository: item.sqlRepository});
					}, this);

					addProcessMenu.add(button);
				}, this);

			return addProcessMenu;
		}
	}

});
