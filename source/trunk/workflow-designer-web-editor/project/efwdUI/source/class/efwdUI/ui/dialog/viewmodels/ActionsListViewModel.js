/**
 * viewModel для списка действий
 * @extends {efwdUI.ui.dialog.viewmodels.EntitiesListViewModel}
 */
qx.Class.define("efwdUI.ui.dialog.viewmodels.ActionsListViewModel", {

	extend: efwdUI.ui.dialog.viewmodels.EntitiesListViewModel,

	properties: {
		currentProcess: {
			deferredInit: true,
			nullable: false,
			check: "efwdDal.entity.Process",
			event: "changeCurrentProcess"
		},
		actionType: {
			init: null,
			check: ["no_status_action", "status_change_action"],
			event: "changeActionType"
		}
	},

	/**
	 * @param {efwdDal.entity.Process} process
	 * @param {efwdUI.behavior.EntityEditionModel} entityEditionModel
	 * @param {library.IServiceLocator} serviceLocator
	 * @param {efwdUI.commands.Queue} queue
	 * @param {efwdUI.behavior.PageModel} pageModel
	 * @param {efwdDal.repository.ProcessesRepository} processesRepository
	 */
	construct: function (process, entityEditionModel, serviceLocator, queue, pageModel, processesRepository)
	{
		this.base(arguments, entityEditionModel, serviceLocator, queue, pageModel);
		this.__process = process;
		this.__serviceLocator = serviceLocator;
		this.__processesRepository = processesRepository;

		this.initCurrentProcess(process);
	},

	members: {

		/**
		 * @param [mode="edit"]
		 * @param [actionType=null]
		 * @param [allProcesses=null]
		 */
		initialize: function (mode, actionType, allProcesses)
		{
			this.base(arguments, null, mode);

			this.__allProcesses = !!allProcesses;
			actionType && this.setActionType(actionType);

			library.data.Helper.multiBind(
				this, "currentProcess", this, "actionType",
				this, "entitiesArray", function (process, actionType)
				{
					switch (actionType)
					{
						case "no_status_action":
							return process.getNoStatusActions();

						case "status_change_action":
							return process.getStatusChangeActions();

						default:
							return process.getActions();
					}
				});
		},

		/**
		 * @return {efwdDal.EntitiesArray}
		 */
		getProcesses: function()
		{
			return this.__processesRepository.getProcesses();
		},

		isAllProcesses: function()
		{
			return this.__allProcesses;
		},

		/**
		 * @return {efwdUI.commands.types.AbstractCommand}
		 * @protected
		 */
		_createCreateEntityCommand: function ()
		{
			var command = this.__serviceLocator.getService(efwdUI.commands.types.CreateAction);
			command.initialize();
			return command;
		}
	}

});
