/**
 * viewModel для списка активностей
 * @extends {efwdUI.ui.dialog.viewmodels.EntitiesListViewModel}
 */
qx.Class.define("efwdUI.ui.dialog.viewmodels.ActivitiesListViewModel", {

	extend: efwdUI.ui.dialog.viewmodels.EntitiesListViewModel,

	events: {
		"changeActivityType": "qx.event.type.Data",
		"changeActivitiesArray": "qx.event.type.Data"
	},

	properties: {
		/**
		 * тип активности
		 */
		activityType: {
			init: null,
			nullable: true,
			check: "String",
			event: "changeActivityType"
		}
	},

	/**
	 * @param {efwdUI.behavior.EntityEditionModel} entityEditionModel
	 * @param {library.IServiceLocator} serviceLocator
	 * @param {efwdUI.commands.Queue} queue
	 * @param {efwdUI.behavior.PageModel} pageModel
	 * @param {efwdDal.entity.Process} process
	 */
	construct: function (entityEditionModel, serviceLocator, queue, pageModel, process)
	{
		this.base(arguments, entityEditionModel, serviceLocator, queue, pageModel);
		this.__serviceLocator = serviceLocator;
		this.__process = process;
		this.__queue = queue;
		this.__entityEditionModel = entityEditionModel;
	},

	members: {
		/**
		 * @param {efwdDal.entity.MActivitiesOwner} [owner=null] владелец активностей
		 * @param {String} [category=null] категория активностей владельца
		 * @param {String} [mode="edit"] режим работы списка сущностей: "edit" - редактирование списка, "select" - выбор из списка
		 */
		initialize: function (owner, category, mode)
		{
			this.base(arguments, owner, mode);
			this.__category = category;

			this.setEntitiesArray(this.__mainActivitiesArray = this.__getActivitiesArray());

			this.bind("activityType", this, "entitiesArray", {converter: (function (activityType)
			{
				if (this.getEntitiesArray() && this.getEntitiesArray() !== this.__mainActivitiesArray)
				{
					this.getEntitiesArray().dispose();
				}

				return activityType === null
					? this.__mainActivitiesArray
					: efwdDal.EntitiesArray.filterArray(this.__mainActivitiesArray, function (entity)
				{
					return entity.getSource().get$type() == activityType;
				});
			}).bind(this)});
		},

		/**
		 * @return {String} категория активностей владельца
		 */
		getCategory: function ()
		{
			return this.__category;
		},

		/**
		 * Добавить владельцу уже существующие активности
		 * @param {efwdDal.entity.Activity[]} entities
		 */
		addExistingEntities: function (entities)
		{
			if (this.isReadOnly())
			{
				throw new Error("[efwdUI.ui.dialog.viewmodels.EntitiesListViewModel] Can't add entity, because read only mode!");
			}

			if (qx.core.Environment.get("qx.debug"))
			{
				this.assert(!!this.getOwner(), "owner must be presented for adding existing entity to it");
			}

			var entitiesArray = this.getOwner().getActivities(this.__category).toArray();
			entities = entities.clone().exclude(entitiesArray);
			if (!entities.length)
			{
				return;
			}

			var command = this.__serviceLocator.getService(efwdUI.commands.types.AddActivitiesToOwner);
			command.initialize(entities, this.getOwner(), this.__category);
			this.__queue.addCommand(command);
		},

		/**
		 * @return {efwdUI.commands.types.AbstractCommand}
		 * @protected
		 */
		_createCreateEntityCommand: function (options)
		{
			var activityType = options.type;
			var command = this.__serviceLocator.getService(efwdUI.commands.types.CreateActivity);
			command.initialize(activityType, this.getOwner(), this.__category);
			return command;
		},

		/**
		 * @param {efwdDal.entity.AbstractEntity[]} entities
		 * @return {efwdUI.commands.types.AbstractCommand}
		 * @protected
		 */
		_createRemoveCommand: function (entities)
		{
			if (qx.core.Environment.get("qx.debug"))
			{
				this.assert(!!this.getOwner(), "No list owner specified!");
			}

			var command = this.__serviceLocator.getService(efwdUI.commands.types.RemoveActivities);
			command.initialize(entities, this.getOwner(), this.__category);
			return command;
		},

		/**
		 * @return {Deferred} (efwdDal.EntitiesArray)
		 * @private
		 */
		__getActivitiesArray: function ()
		{
			return this.getOwner()
				? this.getOwner().getActivities(this.__category)
				: this.__process.getActivities();
		}
	},

	destruct: function ()
	{
		var curActivitiesArray = this.getEntitiesArray();
		if (curActivitiesArray !== this.__mainActivitiesArray)
		{
			curActivitiesArray.dispose();
		}
	},

	defer: function (statics, members, properties)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}

});
