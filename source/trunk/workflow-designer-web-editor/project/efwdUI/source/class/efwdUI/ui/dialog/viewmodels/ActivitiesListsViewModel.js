/*
 #ignore(efwdConfig)
 #ignore(efwdConfig.*)
 */

/**
 * Модель для вида, отображающего активности владельца по категориям
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdUI.ui.dialog.viewmodels.ActivitiesListsViewModel", {

	extend: qx.core.Object,

	/**
	 * @param {library.IServiceLocator} serviceLocator
	 */
	construct: function (serviceLocator)
	{
		this.__serviceLocator = serviceLocator;
	},

	members: {
		/**
		 * @param {efwdDal.entity.MActivitiesOwner} owner владелец активностей
		 */
		initialize: function (owner)
		{
			this.__owner = owner;

			var activityCategories = efwdConfig.efwdCommon.data.types.$activityCategories[owner.getEntityType()];
			this.__viewModels = activityCategories.map(function (category)
			{
				var viewModel = this.__serviceLocator.getService(efwdUI.ui.dialog.viewmodels.ActivitiesListViewModel);
				viewModel.initialize(this.__owner, category);
				return viewModel;
			}, this);
		},

		/**
		 * @return {efwdUI.ui.dialog.viewmodels.ActivitiesListViewModel[]} массив моделей для видов списков активностей для каждой категории
		 */
		getListsViewModels: function ()
		{
			return this.__viewModels;
		},

		/**
		 * @return {efwdDal.entity.MActivitiesOwner} владелец активностей
		 */
		getOwner: function ()
		{
			return this.__owner;
		}
	},

	destruct: function ()
	{
		qx.util.DisposeUtil.disposeArray(this, "__viewModels");
	},

	defer: function (statics, members, properties)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}

});
