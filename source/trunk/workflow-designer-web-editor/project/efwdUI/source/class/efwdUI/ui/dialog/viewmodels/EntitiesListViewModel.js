/**
 * Модель для видов, отображающих список сущностей
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdUI.ui.dialog.viewmodels.EntitiesListViewModel", {

	type: "abstract",
	extend: qx.core.Object,

	properties: {

		/**
		 * @type {efwdDal.EntitiesArray}
		 */
		entitiesArray: {
			init: null,
			check: "efwdDal.EntitiesArray",
			event: "changeEntitiesArray",
			apply: "__applyEntitiesArray"
		},

		multiSelect: {
			init: true,
			check: "Boolean",
			event: "changeMultiSelect"
		},

		/**
		 * Выбранные в списке сущности
		 */
		selection: {
			deferredInit: true,
			nullable: true,
			check: "qx.data.Array",
			event: "changeSelection"
		},

		readOnly: {
			init: false,
			check: "Boolean",
			event: "changeReadOnly"
		}
	},

	/**
	 * @param {efwdUI.behavior.EntityEditionModel} entityEditionModel
	 * @param {library.IServiceLocator} serviceLocator
	 * @param {efwdUI.commands.Queue} queue
	 * @param {efwdUI.behavior.PageModel} pageModel
	 */
	construct: function (entityEditionModel, serviceLocator, queue, pageModel)
	{
		this.__entityEditionModel = entityEditionModel;
		this.__serviceLocator = serviceLocator;
		this.__queue = queue;
		this.__pageModel = pageModel;

		this.initSelection(new qx.data.Array());
		this._listenersStore = new library.ListenersStore();
	},

	members: {

		/**
		 * @param {efwdDal.entity.AbstractEntity} [owner=null] владелец сущностей
		 * @param {String} [mode="edit"] режим работы списка сущностей: "edit" - редактирование списка, "select" - выбор из списка
		 */
		initialize: function (owner, mode)
		{
			this.__owner = owner;
			this.__mode = mode || "edit";

			if (this.__mode == "edit")
			{
				this.__setUpSelectionBinding();
			}

			this._listenersStore.addBinding(this.__pageModel, "readOnly", this, "readOnly");
		},

		/**
		 * Инициировать добавление новой сущности
		 * @param {Object} [options=null] опции для добавления
		 */
		addEntity: function (options)
		{
			if (this.isReadOnly())
			{
				throw new Error("[efwdUI.ui.dialog.viewmodels.EntitiesListViewModel] Can't add entity, because read only mode!");
			}

			this.__queue.addCommand(this._createCreateEntityCommand(options));
		},

		/**
		 * Тип удаления сущности показывает, что произойдёт при вызове метода deleteEntity.
		 * Если тип "remove" - то сущность будет отсоединена от её владельца (owner),
		 * если тип "delete" - то сущность будет полностью удалена.
		 */
		getDeletionType: function ()
		{
			return this.__owner ? "remove" : "delete";
		},

		/**
		 * @return {String} Режим работы списка сущностей
		 */
		getMode: function ()
		{
			return this.__mode;
		},

		/**
		 * @return {efwdDal.entity.AbstractEntity} Владелец сущностей
		 */
		getOwner: function ()
		{
			return this.__owner;
		},

		/**
		 * Удаление сущностей
		 * @param {efwdDal.entity.AbstractEntity[]} entities
		 */
		deleteEntities: function (entities)
		{
			if (this.isReadOnly())
			{
				throw new Error("[efwdUI.ui.dialog.viewmodels.EntitiesListViewModel] Can't delete entities, because read only mode!");
			}

			var deleteCommand;
			if (this.getDeletionType() == "delete")
			{
				deleteCommand = this.__serviceLocator.getService(efwdUI.commands.types.DeleteEntities);
				deleteCommand.initialize(entities);
			}
			else
			{
				deleteCommand = this._createRemoveCommand(entities);
			}

			this.__queue.addCommand(deleteCommand);

			if (this.__entityEditionModel.getEntity() == entities)
			{
				this.__entityEditionModel.resetEntity();
			}
		},

		selectAll: function()
		{
			var selection = this.getSelection();
			selection.splice.apply(selection, [0, selection.getLength()].concat(this.getEntitiesArray().toArray()));
		},

		/**
		 * @param {efwdDal.EntitiesArray} value
		 * @param {efwdDal.EntitiesArray} old
		 * @private
		 */
		__applyEntitiesArray: function (value, old)
		{
			if (old)
			{
				this._listenersStore.removeObject(old.getDataArray());
			}

			if (this.__mode == "edit")
			{
				this._listenersStore.addListener(value.getDataArray(), "changeArray", function (e)
				{
					var data = e.getData();
					(function ()
					{
						if (data.insert.length > 0)
						{
							var selection = this.getSelection();
							selection.removeAll();
							selection.append(data.insert);
						}
					}).defer(this);
				}, this);
			}
		},

		/**
		 * Возвращает команду, которая создаёт новую сущность
		 * @abstract
		 * @return {efwdUI.commands.types.AbstractCommand}
		 * @protected
		 */
		_createCreateEntityCommand: function (options){ throw new Error("[efwdUI.ui.dialog.EntitiesListViewModel#_createCreateEntityCommand] Method is abstract!"); },

		/**
		 * Возвращает команду, которая удаляет переданные сущности
		 * @param {efwdDal.entity.AbstractEntity[]} entities
		 * @return {efwdUI.commands.types.AbstractCommand}
		 * @protected
		 */
		_createRemoveCommand: function (entities){ throw new Error("[efwdUI.ui.dialog.EntitiesListViewModel] Removing command for this entities list has not realized"); },

		/**
		 * @private
		 */
		__setUpSelectionBinding: function ()
		{
			this._listenersStore.addListener(this.__entityEditionModel, "changeEntity", function (e)
			{
				var selection = this.getSelection();
				//если выбрано несколько сущностей, то не изменяем это
				if (selection.getLength() > 1)
				{
					return;
				}

				var entity = e.getData();
				//изменяем свойство только если сущность подходящего типа
				if (entity && this.getEntitiesArray() && this.getEntitiesArray().getFilter()(entity))
				{
					if (selection.getItem(0) !== entity)
					{
						selection.setItem(0, entity);
					}
				}
				else
				{
					selection.removeAll();
				}
			}, this);

			this._listenersStore.addListener(this.getSelection(), "change", function ()
			{
				var entity = this.getSelection().getItem(0);
				//сбрасываем сущность для редактирования, если в списке более одной выбранной сущности
				if (this.getSelection().getLength() > 1)
				{
					entity = null;
				}

				if (entity == this.__entityEditionModel.getEntity())
				{
					return;
				}

				//не сбрасываем её если сейчас редактируется не подходящая сущность
				if (entity || !this.getEntitiesArray() || this.getEntitiesArray().getFilter()(this.__entityEditionModel.getEntity()))
				{
					this.__entityEditionModel.setEntity(entity || null);
				}
			}, this);
		}
	},

	destruct: function ()
	{
		this.getSelection().removeAll();
		this._listenersStore.dispose();
	},

	defer: function (statics, members, properties)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}

});
