/**
 * viewModel для списка процессов
 * @extends {efwdUI.ui.dialog.viewmodels.EntitiesListViewModel}
 */
qx.Class.define("efwdUI.ui.dialog.viewmodels.ProcessesListViewModel", {

	extend: efwdUI.ui.dialog.viewmodels.EntitiesListViewModel,

	/**
	 * @param {efwdUI.behavior.EntityEditionModel} entityEditionModel
	 * @param {library.IServiceLocator} serviceLocator
	 * @param {efwdUI.commands.Queue} queue
	 * @param {efwdUI.behavior.PageModel} pageModel
	 * @param {efwdDal.repository.ProcessesRepository} processesRepository
	 */
	construct: function (entityEditionModel, serviceLocator, queue, pageModel, processesRepository)
	{
		this.base(arguments, entityEditionModel, serviceLocator, queue, pageModel);
		this.__processesRepository = processesRepository;
		this.__serviceLocator = serviceLocator;
	},

	members: {

		/**
		 * @param [mode="edit"]
		 */
		initialize: function (mode)
		{
			this.base(arguments, null, mode);

			this.setEntitiesArray(this.__processesRepository.getProcesses());
		},

		/**
		 * @return {efwdUI.commands.types.AbstractCommand}
		 * @protected
		 */
		_createCreateEntityCommand: function (options)
		{
			var command = this.__serviceLocator.getService(efwdUI.commands.types.CreateProcess);
			command.initialize(!!options.sqlRepository);
			return command;
		}
	}

});
