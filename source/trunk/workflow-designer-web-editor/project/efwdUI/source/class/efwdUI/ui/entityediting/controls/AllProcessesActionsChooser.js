/*
 #asset(qx/icon/${qx.icontheme}/22/actions/document-open.png)

 #ignore(efwdCommon)
 #ignore(efwdCommon.*)
 * @extends {library.form.AbstractChooseField}
 */
qx.Class.define("efwdUI.ui.entityediting.controls.AllProcessesActionsChooser", {

	extend: library.form.AbstractChooseField,

	/**
	 * @param {library.IServiceLocator} serviceLocator
	 * @param {efwdDal.repository.ProcessesRepository} processesRepository
	 * @param {efwdDal.IdsMapper} idsMapper
	 */
	construct: function (serviceLocator, processesRepository, idsMapper, process)
	{
		this.base(arguments);

		this.__serviceLocator = serviceLocator;
		this.__processesRepository = processesRepository;
		this.__idsMapper = idsMapper;
		this.__process = process;

		this._createChildControlImpl("openProcessButton");
	},

	members: {

		setProcessAndActionIds: function (processId, actionId)
		{
			this.setReadOnly(true);
			var textField = this.getChildControl("textField");
			textField.setValue("loading...");


			this.__processesRepository.getProcesses().deferred()
				.next(function (processes)
				{
					var deferred = new library.Deferred();

					var process = processes.toArray().filter(function (process)
					{
						return process.getSource().getId() == processId;
					}, this)[0];

					if (process)
					{
						process.getActions().deferred().next(function (actions)
						{
							var action = actions.toArray().filter(function (action)
							{
								return action.getSource().getId() == actionId;
							})[0];

							deferred.call(action);
						});
					}
					else
					{
						deferred.call();
					}

					return deferred;
				}, this)
				.next(function (action)
				{
					textField.setValue(null);
					if (action)
					{
						this.setValue(action);
					}
					this.setReadOnly(false);
				}, this);
		},

		/**
		 * @protected
		 */
		_closeChooser: function ()
		{
			if (!this.__dialog)
			{
				return;
			}

			this.__dialog.close();
		},

		/**
		 * @protected
		 */
		_createChildControlImpl: function (id)
		{
			var control;
			switch (id)
			{
				case "openProcessButton":
					control = this.__createOpenProcessButton();
					this._add(control);
					break;
			}

			return control || this.base(arguments, id);
		},

		/**
		 * @protected
		 */
		_openChooser: function ()
		{
			if (this.__dialog)
			{
				return;
			}

			this.__viewModel = this.__serviceLocator.getService(efwdUI.ui.dialog.viewmodels.ActionsListViewModel);
			this.__viewModel.initialize("select", null, true);
			this.__viewModel.setMultiSelect(false);
			if (this.getValue())
			{
				this.__viewModel.getSelection().removeAll();
				this.__viewModel.getSelection().push(this.getValue());
			}

			this.__dialog = this.__serviceLocator.getService(efwdUI.ui.dialog.ActionsListDialog);
			this.__dialog.initialize(this.__viewModel);

			this.__dialog.addListener("destroyed", function ()
			{
				if (this.__viewModel.getSelection().getLength() > 0)
				{
					this.setValue(this.__viewModel.getSelection().toArray()[0]);
				}

				this.__dialog = null;
				this.__viewModel = null;

				this._chooserClosed();
			}, this);

			this.__dialog.render();
		},

		/**
		 * @param {efwdDal.entity.Action} value
		 * @return {String}
		 * @private
		 */
		_valueToString: function (value)
		{
			return value.getSource().getName() + " (" + value.getProcess().getSource().getName() + ")";
		},

		/**
		 * @private
		 */
		__createOpenProcessButton: function ()
		{
			this.__openProcessButton = new qx.ui.form.Button(null, "icon/22/actions/document-open.png");
			this.__openProcessButton.setPadding(library.form.AbstractChooseField._BUTTON_PADDING);

			this.bind("value", this.__openProcessButton, "enabled", {converter: (function (action)
			{
				return action !== null && action.getProcess() != this.__process
			}).bind(this)});

			this.__openProcessButton.addListener("execute", function ()
			{
				open(efwdCommon.Helper.getProcessEditorUrl(this.__idsMapper.getServerId(this.getValue().getProcess().getSource().getId())));
			}, this);

			return this.__openProcessButton;
		}

	}
});
