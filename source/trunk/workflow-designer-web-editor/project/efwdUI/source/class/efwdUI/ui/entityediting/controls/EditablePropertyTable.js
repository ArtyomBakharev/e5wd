/*
 #asset(qx/icon/${qx.icontheme}/22/actions/document-open.png)

 #ignore(efwdCommon)
 #ignore(efwdCommon.*)
 * @extends {library.form.AbstractChooseField}
 */
qx.Class.define("efwdUI.ui.entityediting.controls.EditablePropertyTable", {

	extend: library.form.TableEditor,

	members: {

		/**
		 * @param name
		 * @return {qx.ui.table.ICellEditorFactory}
		 * @protected
		 */
		_getCellEditor: function (name)
		{
			return name == "scope" ? new qx.ui.table.celleditor.CheckBox() : this.base(arguments, name);
		},

		/**
		 * @param name
		 * @return {qx.ui.table.ICellRenderer}
		 * @protected
		 */
		_getCellRenderer: function (name)
		{
			return name == "scope" ? new qx.ui.table.cellrenderer.Boolean() : this.base(arguments, name);
		},

		/**
		 * @param {String} name
		 * @return {*}
		 * @protected
		 */
		_getDefaultValue: function(name)
		{
			return name == "scope" ? 0 : this.base(arguments, name);
		},

		/**
		 * @param name
		 * @return {Boolean}
		 * @protected
		 */
		_isColumnEditable: function(name)
		{
			return name == "scope" ? false : this.base(arguments, name);
		},

		_normalizeValue: function (name, value)
		{
			return name == "scope" ? (!value || value == "0" ? 0 : 1) : this.base(arguments, name, value);
		},

		/**
		 * @param {qx.ui.table.Table} table
		 * @protected
		 */
		_setUpTable: function(table)
		{
			this.base(arguments, table);

			var tableModel = this._getTableModel();
			table.addListener("cellDblclick", function (e)
			{
				var row = e.getRow();
				if (this.isReadOnly() || row >= tableModel.getRowCount())
				{
					return;
				}

				var index = e.getColumn();

				if (tableModel.getColumnId(index) == "scope")
				{
					var value = tableModel.getValue(index, row);
					tableModel.setValue(index, row, !value);
				}
			}, this);
		},

		/**
		 * @param name
		 * @param value
		 * @return {*}
		 * @protected
		 */
		_transformToActualValue: function(name, value)
		{
			return name == "scope" ? (!!value ? 1 : 0) : this.base(arguments, name, value);
		},

		/**
		 * @param name
		 * @param value
		 * @return {*}
		 * @protected
		 */
		_transformToTableValue: function(name, value)
		{
			return name == "scope" ? !!value : this.base(arguments, name, value);
		}
	}
});
