/**
 * Создаёт форму для редактирования сущности
 * на основе конфигурации efwdConfig.efwdUI.ui.entityediting.$formsStructure
 * сохраняет изменения автоматически
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdUI.ui.entityediting.view.EntityForm", {

	extend: qx.core.Object,

	/**
	 * @param {efwdUI.Application} application
	 * @param {library.IServiceLocator} serviceLocator
	 */
	construct: function (application, serviceLocator)
	{
		this.__application = application;
		this.__serviceLocator = serviceLocator;

		this.__controls = {};
		this.__validValues = {};

		this.__unsavedChanges = {};

		this.__listenersStore = new library.ListenersStore();
	},

	members: {

		/**
		 * @param {efwdUI.ui.entityediting.viewModel.EntityEditor} viewModel
		 */
		initialize: function (viewModel)
		{
			this.__viewModel = viewModel;
			this.__items = this.__viewModel.getFormStructure();
		},

		hasUnsavedChanges: function ()
		{
			Object.forEach(this.__controls, function (control)
			{
				if (control instanceof library.form.TableEditor)
				{
					control.flush();
				}
			});
			return Object.getValues(this.__unsavedChanges).some(function (x){ return x });
		},

		/**
		 * возвращает отрисованную форму
		 * @return {qx.ui.form.renderer.AbstractRenderer}
		 */
		render: function ()
		{
			//перед закрытием страницы, нужно сохранить все данные формы
			this.__listenersStore.addListener(qx.core.Init.getApplication(), "prepareToPageClose",
				function (){ this.save(true) }, this);

			this.__createForm();

			var renderer = this.__createRenderer();

			this.__setUpSaving();
			this.__setUpSourceBinding();

			return renderer;
		},

		/**
		 * Сохранить несохранённые изменения в форме
		 */
		save: function (immidiatly)
		{
			if (!this.hasUnsavedChanges())
			{
				return;
			}

			if (immidiatly)
			{
				this.__saveChanges();
			}
			else
			{
				Object.forEach(this.__controls, function (control){ control.blur(); });
			}
		},

		/**
		 * @private
		 */
		__createControl: function (item, source)
		{
			var control;
			switch (item.type)
			{
				case "select":
					control = new library.ui.widget.SelectBox();
					control.setItems(item.values);
					break;

				case "boolean":
					control = new qx.ui.form.CheckBox();
					break;

				case "table":
					var cls = item.tableType == "EditablePropertyTable"
						? efwdUI.ui.entityediting.controls.EditablePropertyTable : library.form.TableEditor;

					control = new cls(item.items.map(function (x){return x[0]}),
						item.items.map(function (x){return x[1]}));
					break;

				case "action":
					control = this.__serviceLocator.getService(efwdUI.ui.entityediting.controls.AllProcessesActionsChooser);
					break;

				case "setAsButton":
					control = new library.form.SetAsButton(item.title, item.altTitle);
					break;

				default:
					control = item.type == "textarea" ? new qx.ui.form.TextArea() : new qx.ui.form.TextField();
					library.ui.management.ZonesManager.getInstance().bindControlToNeutralZone(control);
			}

			control.setUserData("itemData", item);

			if ("required" in item)
			{
				control.setRequired(item.required);
			}

			this.__setControlValue(control, this.__viewModel.getSourceProperty(item.name));

			this.__validValues[item.name] = this.__getControlValue(control);

			if (this.__viewModel.isReadOnly())
			{
				if (control.setReadOnly)
				{
					control.setReadOnly(true);
				}
				else
				{
					control.setEnabled(false);
				}
			}

			return control;
		},

		/**
		 * @private
		 */
		__createForm: function ()
		{
			this.__form = new qx.ui.form.Form();
			var source = this.__viewModel.getEntity().getSource();

			this.__items.forEach(function (item)
			{
				var control = this.__createControl(item, source);
				this.__form.add(control, item.title);
				this.__controls[item.name] = control;
			}, this);

			this.__items.forEach(function (item)
			{
				if (item.type == "setAsButton")
				{
					library.form.SingleRenderer.renderInOneCell(this.__controls[item.name]);
				}

				if (item.wideField || ["textarea", "table", "action"].indexOf(item.type) !== -1)
				{
					library.form.SingleRenderer.renderInOneColumn(this.__controls[item.name])
				}
			}, this);
		},

		/**
		 * @private
		 */
		__createRenderer: function ()
		{
			var renderer = new library.form.SingleRenderer(this.__form);

			var layout = renderer.getLayout();
			layout.setColumnFlex(0, 0);
			layout.setColumnFlex(1, 1);

			return renderer;
		},

		/**
		 * @private
		 */
		__getControlValue: function (control)
		{
			var itemData = control.getUserData("itemData");
			var value = control.getValue();

			switch (itemData.type)
			{
				case "boolean":
					if ("values" in itemData)
					{
						value = itemData.values[value ? 1 : 0];
					}
					break;

				case "action":
					return !value ? null : {
						process: value.getProcess().getSource().getId(),
						action: value.getSource().getId()
					};
					break;
			}

			return value;
		},

		/**
		 * @private
		 */
		__saveChanges: function ()
		{
			if (this.hasUnsavedChanges() && this.__form.validate())
			{
				this.__viewModel.updateEntity(Object.mapObject(this.__unsavedChanges, function (value, key)
				{
					return this.__getControlValue(this.__controls[key]);
				}, this), true);
				this.__unsavedChanges = {};
			}
		},

		/**
		 * @private
		 */
		__setControlValue: function (control, value)
		{
			var itemData = control.getUserData("itemData");

			if (!value && value !== false && value !== "" && value !== null)
			{
				value = null;
			}

			if (itemData.type == "action")
			{
				if (value)
				{
					control.setProcessAndActionIds(value.process, value.action);
				}
				else
				{
					control.setValue(null);
				}
			}
			else
			{
				switch (itemData.type)
				{
					case "boolean":
						if ("values" in itemData)
						{
							value = value == itemData.values[1];
						}
						break;
				}

				control.setValue(value);
			}
		},

		/**
		 * @private
		 */
		__setUpSourceBinding: function ()
		{
			this.__listenersStore.addListener(this.__viewModel, "entityUpdatedOutside", function ()
			{
				this.__savingDisabled = true;
				Object.forEach(this.__controls, function (control, key)
				{
					var itemData = control.getUserData("itemData");
					this.__setControlValue(control, this.__viewModel.getSourceProperty(key));
				}, this);
				this.__savingDisabled = false;

			}, this);
		},

		/**
		 * @private
		 */
		__setUpSaving: function ()
		{
			Object.forEach(this.__controls, function (control, name)
			{
				if (control instanceof qx.ui.form.AbstractField)
				{
					control.addListener("input", function ()
					{
						this.__updateEntityField(name, control, false);
					}, this);
				}

				control.addListener("changeValue", function ()
				{
					this.__updateEntityField(name, control, true);
				}, this);

			}, this);
		},

		/**
		 * @private
		 */
		__updateEntityField: function (name, control, withCommit)
		{
			if (this.__savingDisabled)
			{
				return;
			}

			withCommit = !this.__noCommitEntity && withCommit;
			this.__form.validate();

			if (!control.isValid() && withCommit)
			{
				this.__application.getMessagesQueue()
					.addMessage(library.ui.messagesqueue.Message.createAlertMessage("Entered field value is not allowed!"));

				this.__noCommitEntity = true;
				this.__setControlValue(control, this.__validValues[name]);
				this.__noCommitEntity = false;
				delete this.__unsavedChanges[name];
				return;
			}

			var value = this.__getControlValue(control);

			if (withCommit)
			{
				this.__validValues[name] = value;
				this.__unsavedChanges = {};
			}
			else
			{
				this.__unsavedChanges[name] = true;
			}

			var update = {};
			update[name] = value;

			this.__viewModel.updateEntity(update, withCommit);
		}
	},

	destruct: function ()
	{
		this.__listenersStore.dispose();
	}
});
