/*
 #ignore(efwdConfig)
 #ignore(efwdConfig.*)
 */

/**
 * viewModel для видов осуществляющих редактирование сущности
 */
qx.Class.define("efwdUI.ui.entityediting.viewModel.EntityEditor", {

	extend: qx.core.Object,

	statics: {
		FORMS_STRUCTURE_CONFIG: efwdConfig.efwdUI.ui.entityediting.$formsStructure,

		/**
		 * for unit tests
		 */
		clearCache: function ()
		{
			this.__formsStructureCache = {};
		},

		/**
		 * @param {efwdDal.entity.AbstractEntity} entity
		 * @return {Object[]}
		 * @private
		 */
		__getFormStructure: function (entity)
		{
			var fullType = entity.getEntityType() + "+" + entity.getSource().get$type();

			if (!this.__formsStructureCache[fullType])
			{
				var formsStructure = this.FORMS_STRUCTURE_CONFIG;
				var typeFormStructure = formsStructure.byTypes[entity.getEntityType()];

				var resultStructure = formsStructure.header.clone();
				var baseStructure = Array.isArray(typeFormStructure)
					? typeFormStructure
					: typeFormStructure.base || typeFormStructure[entity.getSource().get$type()];

				function processItem(item)
				{
					if (item.include)
					{
						if (item.include == "subtype")
						{
							typeFormStructure[entity.getSource().get$type()].forEach(processItem);
						}
						else
						{
							formsStructure.parts[item.include].forEach(processItem);
						}
					}
					else
					{
						resultStructure.push(item);
					}
				}

				baseStructure.forEach(processItem);

				this.__formsStructureCache[fullType] = resultStructure;
			}

			return this.__formsStructureCache[fullType];
		},

		__formsStructureCache: {}
	},

	events: {
		"entityUpdatedOutside": "qx.event.type.Event"
	},

	properties: {
		readOnly: {
			init: false,
			check: "Boolean",
			event: "changeReadOnly"
		}
	},

	/**
	 * @param {library.IServiceLocator} serviceLocator
	 * @param {efwdUI.commands.Queue} queue
	 * @param {efwdUI.behavior.PageModel} pageModel
	 */
	construct: function (serviceLocator, queue, pageModel)
	{
		this.__serviceLocator = serviceLocator;
		this.__queue = queue;
		this.__pageModel = pageModel;
	},

	members: {
		/**
		 * @param {efwdDal.entity.AbstractEntity} entity сущность для редактирования
		 */
		initialize: function (entity)
		{
			this._entity = entity;
			this.getListenersStore().addListener(this._entity.getSource(), "selfChanged",
				function (){ this._entityUpdated() }, this);

			this.__createEditCommand();

			this.getListenersStore().addBinding(this.__pageModel, "readOnly", this, "readOnly");
		},

		/**
		 * @return {efwdDal.entity.AbstractEntity} сущность для редактирования
		 */
		getEntity: function ()
		{
			return this._entity;
		},

		/**
		 * @return {Object[]}
		 */
		getFormStructure: function ()
		{
			return this.__formStructure || (this.__formStructure = this.self(arguments).__getFormStructure(this._entity));
		},

		/**
		 * @return {Object}
		 */
		getFormStructureIndex: function ()
		{
			return this.__formStructureIndex || (this.__formStructureIndex = this.getFormStructure().buildIndex(function(x){ return x.name }));
		},

		getSourceProperty: function(name)
		{
			var source = this._entity.getSource();
			var item = this.getFormStructureIndex()[name];

			switch (item.type)
			{
				case "action":
					return source.get(name) ? {process: source.get(item.processId), action: source.get(name)} : null;

				case "table":
					return qx.util.Serializer.toNativeObject(source.get(name));

				default:
					return source.get(name);
			}
		},

		/**
		 * Обновить сущность
		 * @param {Object} changes
		 * @param {Boolean} [commit=false] сохранять ли изменения в хранилище
		 */
		updateEntity: function (changes, commit)
		{
			if (this.isReadOnly())
			{
				throw new Error("[efwdUI.ui.entityediting.viewModel.EntityEditor] Can't update entity, because read only mode!");
			}

			this.__updatingEntity = true;

			Object.forEach(changes, function (value, name)
			{
				this.__setSourceProperty(name, value);
			}, this);

			if (commit)
			{
				this.__queue.addCommand(this.__editEntityCommand);
				this.__createEditCommand();
			}
			else
			{
				this.__editEntityCommand.execute(false);
			}

			this.__updatingEntity = false;
		},

		/**
		 * @protected
		 */
		_entityUpdated: function ()
		{
			if (!this.__updatingEntity)
			{
				this.__createEditCommand();
				this.fireEvent("entityUpdatedOutside");
			}
		},

		__setSourceProperty: function(name, value)
		{
			var source = this._entity.getSource();
			var item = this.getFormStructureIndex()[name];

			switch (item.type)
			{
				case "action":
					source.set(item.processId, value && value.process);
					source.set(name, value && value.action);
					break;

				case "table":
					source.set(name, qx.data.marshal.Json.createModel(value));
					break;

				default:
					source.set(name, value);
					break;
			}
		},

		/**
		 * @private
		 */
		__createEditCommand: function ()
		{
			if (!this.__keys)
			{
				this.__keys = this.getFormStructure().map(function (x){ return x.name });
			}

			this.__editEntityCommand = this.__serviceLocator.getService(efwdUI.commands.types.EditEntity);
			this.__editEntityCommand.initialize(this._entity, this._entity.getSource(), this.__keys);
		}
	},

	defer: function (statics, members, properties)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}

});
