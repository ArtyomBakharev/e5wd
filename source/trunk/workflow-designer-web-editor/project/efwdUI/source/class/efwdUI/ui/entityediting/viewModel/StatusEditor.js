/**
 * viewModel для видов осуществляющих редактирование сущности
 */
qx.Class.define("efwdUI.ui.entityediting.viewModel.StatusEditor", {

	extend: efwdUI.ui.entityediting.viewModel.EntityEditor,

	/**
	 * @param {library.IServiceLocator} serviceLocator
	 * @param {efwdUI.commands.Queue} queue
	 * @param {efwdUI.behavior.PageModel} pageModel
	 * @param {efwdUI.behavior.GeneralCommandsProvider} uiCommandProvider
	 */
	construct: function (serviceLocator, queue, pageModel, uiCommandProvider)
	{
		this.base(arguments, serviceLocator, queue, pageModel);
		this.__uiCommandProvider = uiCommandProvider;
	},

	members: {
		/**
		 * @param {efwdDal.entity.AbstractEntity} entity сущность для редактирования
		 */
		initialize: function (entity)
		{
			this.base(arguments, entity);
			this.getListenersStore()
				.addListener(this._entity, "changeInitialStatus", function(){ this._entityUpdated() }, this);
		},

		getSourceProperty: function (name)
		{
			if (name == "initialStatus")
			{
				return this._entity.isInitialStatus();
			}
			return this.base(arguments, name);
		},

		/**
		 * Обновить сущность
		 * @param {Object} changes
		 * @param {Boolean} [commit=false] сохранять ли изменения в хранилище
		 */
		updateEntity: function (changes, commit)
		{
			if (!this.isReadOnly() && changes.initialStatus)
			{
				this.__uiCommandProvider.getSetAsInitialStatusCommand().execute(null, {status: this._entity});
				delete changes.initialStatus;
			}

			if (Object.keys(changes).length > 0)
			{
				this.base(arguments, changes, commit);
			}
		}
	}

});
