/*
 #ignore(efwdConfig)
 #ignore(efwdConfig.*)
 */

/**
 * Контроллер помещающий форму для редактирования сущности в контейнер {@link efwdUI.ui.views.EditorContainer}
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdUI.ui.settingspanel.Controller", {

	extend: qx.core.Object,

	/**
	 * @param {efwdUI.behavior.EntityEditionModel} entityEditionModel
	 * @param {efwdUI.ui.views.EditorContainer} containerView
	 * @param {library.IServiceLocator} serviceLocator
	 */
	construct: function (entityEditionModel, containerView, serviceLocator)
	{
		this.__entityEditionModel = entityEditionModel;
		this.__containerView = containerView;
		this.__serviceLocator = serviceLocator;
	},

	members: {
		initialize: function ()
		{
			this.__entityEditionModel.addListener("changeEntity", function (e)
			{
				this.__changeEditor(e.getData());
			}, this);
		},

		/**
		 * @param {efwdDal.entity.AbstractEntity} [entity=null]
		 * @private
		 */
		__changeEditor: function (entity)
		{
			if (this.__editorViewModel)
			{
				this.__editorViewModel.dispose();
				this.__editorViewModel = null;
			}

			var editor;
			if (!entity || !(editor = this.__createEditorForEntity(entity)))
			{
				this.__containerView.closeEditor();
				return;
			}

			this.__containerView.setEditor(editor);
		},

		/**
		 * @private
		 */
		__createEditorForEntity: function (entity)
		{
			if (!(entity.getEntityType() in efwdConfig.efwdUI.ui.entityediting.$formsStructure.byTypes))
			{
				return false;
			}

			var editor = this.__serviceLocator.getService(efwdUI.ui.settingspanel.editor.EntityEditor);

			this.__editorViewModel = this.__serviceLocator.getService(entity instanceof efwdDal.entity.Status
				? efwdUI.ui.entityediting.viewModel.StatusEditor : efwdUI.ui.entityediting.viewModel.EntityEditor);

			this.__editorViewModel.initialize(entity);
			editor.initialize(this.__editorViewModel);

			return editor;
		}
	},

	defer: function (statics, members, properties)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}

});
