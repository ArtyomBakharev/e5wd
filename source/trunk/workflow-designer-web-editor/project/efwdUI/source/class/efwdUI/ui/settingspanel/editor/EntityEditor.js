/**
 * Редактор сущности
 * использует {@link efwdUI.ui.entityediting.view.EntityForm} для создания формы
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdUI.ui.settingspanel.editor.EntityEditor", {

	extend: qx.core.Object,

	implement: efwdUI.ui.settingspanel.editor.IEditor,

	properties: {
		title: {
			init: null,
			check: "String",
			event: "changeTitle"
		},

		icon: {
			init: null,
			check: "String",
			event: "changeIcon"
		}
	},

	/**
	 * @param {library.IServiceLocator} serviceLocator
	 */
	construct: function (serviceLocator)
	{
		this.__serviceLocator = serviceLocator;
		this.__listenersStore = new library.ListenersStore();
	},

	members: {
		/**
		 * @param {efwdUI.ui.entityediting.viewModel.EntityEditor} viewModel
		 */
		initialize: function (viewModel)
		{
			this.__viewModel = viewModel;
		},

		/**
		 * Уведомить редактор о закрытии
		 */
		close: function ()
		{
			this.__form.save();
			this.__form.dispose();
			this.__listenersStore.dispose();
		},

		/**
		 * @return {qx.ui.core.Widget}
		 */
		render: function ()
		{
			var entity = this.__viewModel.getEntity();
			this.__listenersStore.addBinding(entity.getSource(), "name", this, "title", function (name)
			{
				return entity.getEntityType().firstUp() + " '" + name + "' parameters";
			});

			this.__chooseIcon();

			this.__form = this.__serviceLocator.getService(efwdUI.ui.entityediting.view.EntityForm);
			this.__form.initialize(this.__viewModel);
			return this.__form.render();
		},

		/**
		 * @private
		 */
		__chooseIcon: function ()
		{
			this.setIcon(efwdUI.ui.IconsStore.get(this.__viewModel.getEntity().getEntityType(), 22));
		}
	},

	defer: function (statics, members, properties)
	{
		members.initialize = members.initialize.forbidMultiCall();
	}

});
