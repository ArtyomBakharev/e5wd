/**
 * Интерфейс для редактора, помещаемого на панель инструментов
 */
qx.Interface.define("efwdUI.ui.settingspanel.editor.IEditor", {

	properties: {
		title: {
			init: null,
			check: "String",
			event: "changeTitle"
		},

		icon: {
			init: null,
			check: "String",
			event: "changeIcon"
		}
	},

	members: {

		/**
		 * Уведомить редактор о закрытии
		 */
		close: function ()
		{
		},

		/**
		 * Отобразить редактор
		 * @return {qx.ui.core.Widget}
		 */
		render: function ()
		{
		}
	}
});
