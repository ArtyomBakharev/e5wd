/**
 * Вид-контейнер для редактора на панели инструментов
 * @extends {qx.core.Object}
 */
qx.Class.define("efwdUI.ui.views.EditorContainer", {

	extend: qx.core.Object,

	construct: function ()
	{
		this.__listenersStore = new library.ListenersStore();
	},

	members: {

		/**
		 * Удаление редактора с панели
		 */
		closeEditor: function ()
		{
			if (!this.__editor)
			{
				return;
			}

			this.__listenersStore.removeObject(this.__editor);
			this.__editor.close();
			this.__editor = null;
			this.__groupBox.setVisibility("excluded");
			this.__groupBox.removeAll();
		},

		/**
		 * @return {qx.ui.core.Widget}
		 */
		render: function ()
		{
			this.__groupBox = new qx.ui.groupbox.GroupBox();
			this.__groupBox.set({
				marginTop: 10,
				layout: new qx.ui.layout.VBox(),
				visibility: "excluded"
			});
			return this.__groupBox;
		},

		/**
		 * Помещение редактора в контейнер
		 * @param {efwdUI.ui.settingspanel.editor.IEditor} editor
		 */
		setEditor: function (editor)
		{
			if (this.__editor)
			{
				this.closeEditor();
			}

			this.__editor = editor;

			this.__groupBox.setVisibility("visible");

			this.__listenersStore.addBinding(this.__editor, "title", this.__groupBox, "legend");

			this.__groupBox.add(this.__editor.render());
			this.__listenersStore.addBinding(this.__editor, "icon", this.__groupBox.getChildControl("legend"), "icon");
		}
	}
});
