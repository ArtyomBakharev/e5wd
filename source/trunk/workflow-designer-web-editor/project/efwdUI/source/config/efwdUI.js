goog.require("__symbol__.efwdConfig.efwdUI.ui.entityediting");

AppTools.config("efwdConfig.efwdUI", {

	ui: {
		$style: {
			layout: {
				leftPanelWidth: 300
			},

			editor: {
				itemsList: {
					window: {
						width: 420,
						height: 500
					}
				},

				entityForm: {
					window: {
						width: 400
					}
				}
			}
		}
	}

});
