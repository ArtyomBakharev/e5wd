goog.provide("__symbol__.efwdConfig.efwdUI.ui.entityediting");

AppTools.config("efwdConfig.efwdUI.ui.entityediting", {

	//описание форм редактирования сущностей
	$formsStructure: {

		//элементы включаются в начало каждой формы
		header: [
			{
				name: "name",
				title: "Name",
				required: true,
				wideField: true
			}, {
				name: "commentary",
				title: "Commentary",
				required: false,
				type: "textarea"
			}
		],

		//части доступные для включения в формы
		parts: {
			accessList: [
				{
					name: "access_list",
					title: "Access Control",
					required: false,
					type: "table",
					items: [["accessor", "accessor"], ["level", "level"]]
				}
			],
			propertyChanges: [
				{
					name: "property_changes",
					title: "Properties",
					required: false,
					type: "table",
					items: [["name", "name"], ["value", "value"]]
				}
			]
		},

		//элементы форм для каждого типа
		byTypes: {

			action: [
				{
					name: "data_type",
					title: "Data type",
//					type: "select",
//					values: [["STRING", "string"], ["INTEGER", "integer"], ["DECIMAL", "decimal"]],
					required: true,
					type: "textarea"
				}, {
					name: "availability_condition",
					title: "Availability Condition",
					required: true,
					type: "textarea"
				}, {
					name: "evaluation_message",
					title: "Evaluation Message",
					required: true,
					type: "textarea"
				}, {
					name: "is_history_enabled",
					title: "Enable History",
					type: "boolean",
					values: ["false", "true"]
				}, {
					name: "is_autocommit_enabled",
					title: "Enable Autocommit",
					type: "boolean",
					values: ["false", "true"]
				}
			],

			activity: {
				//базовый шаблон для форм всех подтипов
				base: [
					{
						name: "run_condition",
						title: "Run Condition",
						required: true,
						type: "textarea"
					}, {
						name: "return_formula",
						title: "Return Formula",
						required: true,
						type: "textarea"
					}, {
						name: "return_type",
						title: "Return Value",
						required: false,
						type: "boolean",
						values: ["VOID", "VALUE"]
					}, {
						//включить элементы формы подтипа
						include: "subtype"
					}
				],

				//далее идут элементы формы для подтипов
//				blank_activity: [],

				send_mail_activity: [
					{
						name: "sendto",
						title: "Recipients",
						required: true,
						type: "textarea"
					}, {
						name: "copyto",
						title: "CC",
						required: false,
						type: "textarea"
					}, {
						name: "blindcopyto",
						title: "BCC",
						required: false,
						type: "textarea"
					}, {
						name: "subject",
						title: "Subject",
						required: true,
						type: "textarea"
					}, {
						name: "body_content",
						title: "Message Test",
						required: true,
						type: "textarea"
					}
				],

				set_property_activity: [
					{
						include: "propertyChanges"
					}
				],

				invoke_method_activity: [
					{
						name: "class_name",
						title: "Class Name",
						required: true,
						type: "textarea"
					}, {
						name: "method_name",
						title: "Method Name",
						required: true,
						type: "textarea"
					}, {
						include: "propertyChanges"
					}
				],

				parametrized_property_local_activity: [
					{
						name: "form",
						title: "Form",
						required: true,
						type: "textarea"
					}, {
						name: "property_changes",
						title: "Properties",
						required: false,
						type: "table",
						tableType: "EditablePropertyTable",
						items: [["name", "name"], ["value", "value"], ["scope", "scope"]]
					}
				],

				remote_transaction_activity: [
					{
						name: "action_id",
						title: "Action",
						required: true,
						type: "action",
						processId: "process_id"
					},{
						name: "data_type",
						title: "Data type",
						required: true,
						type: "textarea"
					}, {
						name: "data_search_script",
						title: "Data search script",
						type: "textarea"
					}
				],

				invoke_script_activity: [
					{
						name: "script",
						title: "Script",
						required: true,
						type: "textarea"
					}
				]
			},

			process: [
				{
					name: "data_type",
					title: "Data type",
					required: true,
					type: "textarea"
				}, {
					//включить элементы формы из части accessList
					include: "accessList"
				}
			],

			status: [
				{
					name: "alias",
					title: "Alias",
					required: false
				}, {
					name: "data_type",
					title: "Data type",
					required: true,
					type: "textarea"
				},{
					name: "initialStatus",
					title: "Set as the initial status",
					altTitle: "The initial status",
					type: "setAsButton"
				}, {
					include: "accessList"
				}
			]
		}
	}
});
