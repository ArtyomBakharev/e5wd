"use strict";
goog.provide("efwdViewer.Application");

goog.require("efwdViewer.Diagram");

efwdViewer.Application = AppTools.createClass({

	statics: {
		getExtender: function(id)
		{
			return this.__extenders[id];
		},
		registerExtender: function (extender)
		{
			var script;
			if (typeof document.scripts != "undefined")
			{
				script = document.scripts[document.scripts.length - 1];
			}
			else
			{
				var scripts = document.getElementsByTagName("script");
				script = scripts[scripts.length - 1];
			}

			this.__extenders[script.id] = extender;
		},
		__extenders: {}
	},

	construct: function ()
	{
		AppTools.DOMReady.add(this.__main.bind(this));
		efwdViewer.createDiagram = this.createDiagram.bind(this);
		efwdViewer.registerExtender = efwdViewer.Application.registerExtender.bind(efwdViewer.Application);
	},

	members: {

		/**
		 * @param {Element|String} element
		 * @param {*} processId
		 * @param {*} [currentStatusId=null]
		 */
		createDiagram: function (element, processId, currentStatusId, params)
		{
			var $this = this;

			AppTools.DOMReady.add(function ()
			{
				if (typeof element == "string")
				{
					element = document.getElementById(element);
				}

				if (!params.data || !params.data.actions || !params.data.statuses)
				{
					element.innerHTML = "<div class='error'>No data!</div>";
				}
				else
				{
					$this.__generalRepository.registerData(processId, params.data);
					var diagram = new efwdViewer.Diagram($this.__generalRepository);
					diagram.initialize(element, processId, currentStatusId, params.schemaExtend);
				}
			});
		},

		/**
		 * @private
		 */
		__main: function ()
		{
			this.__communication = new efwdCommon.Communication();
			this.__mapper = new efwdCommon.data.Mapper();
			this.__generalRepository = new efwdCommon.data.JsonRepository(this.__mapper);

			this.__scanDocument();
		},

		__scanDocument: function ()
		{
			var elements = document.getElementsByClassName(efwdViewer.Diagram.VIEWER_CLASS);
			Array.prototype.forEach.call(elements, function (element)
			{
				var params = Array.prototype.filter.call(element.children,
					function (x){ return x.nodeName.toLowerCase() == "script" })[0];
				params = JSON.parse(params.innerHTML);
				this.createDiagram(element, element.getAttribute("data-process"),
					element.getAttribute("data-current-status"), params);
			}, this);
		}
	}

});
