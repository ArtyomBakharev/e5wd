"use strict";
goog.provide("efwdViewer.Canvas");

efwdViewer.Canvas = AppTools.createClass({

	members: {

		getPaper: function ()
		{
			return this.__paper;
		},

		/**
		 * @param {Element} element
		 */
		render: function (element)
		{
			this.__wrapper = element;
			this.__paperContainer = document.createElement("div");
			this.__wrapper.appendChild(this.__paperContainer);
			this.__paper = Raphael(this.__paperContainer);
			this.setCanvasFrame(0, 0, 0, 0);
		},

		centerScreenTo: function (point)
		{
			point = {x: point.x - this.__left, y: point.y - this.__top};
			var elementSize = {
				width: parseInt(domUtil.getStyle(this.__wrapper, "width")),
				height: parseInt(domUtil.getStyle(this.__wrapper, "height"))
			};
			var scrollBarWidth = this.__getScrollBarWidth();
			this.__wrapper.scrollLeft = point.x - elementSize.width / 2 + scrollBarWidth;
			this.__wrapper.scrollTop = point.y - elementSize.height / 2 + scrollBarWidth;
		},

		setCanvasFrame: function (left, top, width, height)
		{
			this.__left = left - efwdConfig.efwdViewer.Canvas.canvasGap;
			this.__top = top - efwdConfig.efwdViewer.Canvas.canvasGap;
			this.__paperContainer.style.marginLeft = (-this.__left) + "px";
			this.__paperContainer.style.marginTop = (-this.__top) + "px";

			this.__width = width + this.__left + 2 * efwdConfig.efwdViewer.Canvas.canvasGap;
			this.__height = height + this.__top + 2 * efwdConfig.efwdViewer.Canvas.canvasGap;
			this.__paperContainer.style.width = this.__width + "px";
			this.__paperContainer.style.height = this.__height + "px";
			this.__paper.setSize(this.__width, this.__height);
		},

		/**
		 * @private
		 */
		__getScrollBarWidth: function ()
		{
			var inner = document.createElement('p');
			inner.style.width = "100%";
			inner.style.height = "200px";

			var outer = document.createElement('div');
			outer.style.position = "absolute";
			outer.style.top = "0px";
			outer.style.left = "0px";
			outer.style.visibility = "hidden";
			outer.style.width = "200px";
			outer.style.height = "150px";
			outer.style.overflow = "hidden";
			outer.appendChild(inner);

			document.body.appendChild(outer);
			var w1 = inner.offsetWidth;
			outer.style.overflow = 'scroll';
			var w2 = inner.offsetWidth;
			if (w1 == w2)
			{
				w2 = outer.clientWidth;
			}

			document.body.removeChild(outer);

			return (w1 - w2);
		}
	}

});
