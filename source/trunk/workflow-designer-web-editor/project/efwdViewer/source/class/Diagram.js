"use strict";
goog.provide("efwdViewer.Diagram");

goog.require("efwdViewer.data.Repository");
goog.require("efwdViewer.Canvas");
goog.require("efwdViewer.Renderer");
goog.require("efwdViewer.ResizeAndScrollCanvasBehavior");

efwdViewer.Diagram = AppTools.createClass({

	/**
	 * @param {efwdCommon.data.Repository} generalRepository
	 */
	construct: function (generalRepository)
	{
		this.__generalRepository = generalRepository;
	},

	members: {

		/**
		 * @param {Element} element
		 * @param {*} processId
		 * @param {*} [currentStatusId=null]
		 * @param {Object} [extendSchemaData={}]
		 * @param {Object} [extendSchemaData.extend={}]
		 * @param {Object} [extendSchemaData.replace={}]
		 */
		initialize: function (element, processId, currentStatusId, extendSchemaData)
		{
			this.__element = element;
			this.__processId = processId;
			this.__currentStatusId = currentStatusId;
			this.__extendSchemaData = extendSchemaData || {};

			this.__setUpViewerWrapper(element);
			this.__createRenderer();
			this.__renderDiagram();
		},

		__applyExtender: function (extender)
		{
			extender.extend && this.__schemaCompiler.registerSchemaPart(extender.extend);
			if (extender.replace)
			{
				AppTools.objForEach(extender.replace, function (part, key)
				{
					this.__schemaCompiler.replaceSchemaPart(key, part);
				}, this);
			}
		},

		__createRenderer: function ()
		{
			this.__canvas = new efwdViewer.Canvas();
			this.__canvas.render(this.__element);

			this.__schemaCompiler = new efwdRenderer.SchemaCompiler();
			this.__schemaCompiler.setSerializer(efwdViewer.data.SimpleObjectModel.serialize);

			this.__rendererManager = new efwdRenderer.Manager(this.__schemaCompiler);
			this.__extendSchema();

			this.__rendererManager.setPaper(this.__canvas.getPaper());
			this.__rendererFactory = new efwdRenderer.Factory(this.__rendererManager);

			this.__repository = new efwdViewer.data.Repository(this.__generalRepository);
			this.__repository.initialize(this.__processId);

			this.__renderer = new efwdViewer.Renderer(this.__rendererFactory, this.__repository);
		},

		__extendSchema: function ()
		{
			this.__applyExtender(this.__extendSchemaData);
			if (this.__extendSchemaData.load)
			{
				this.__extendSchemaData.load.forEach(function (id)
				{
					this.__applyExtender(efwdViewer.Application.getExtender(id));
				}, this);
			}
		},

		__insertLoader: function ()
		{
			this.__loader = document.createElement("div");
			domUtil.addClass(this.__loader, "loader");
			this.__element.appendChild(this.__loader);

			var icon = document.createElement("div");
			domUtil.addClass(icon, "icon");
			this.__loader.appendChild(icon);
		},

		__removeLoader: function ()
		{
			domUtil.removeElement(this.__loader);
		},

		__renderDiagram: function ()
		{
			this.__insertLoader();

			if (this.__currentStatusId)
			{
				this.__renderer.setCurrentStatusId(this.__currentStatusId);
			}
			this.__resizeAndScrollCanvas = new efwdViewer.ResizeAndScrollCanvasBehavior(this.__renderer, this.__canvas);

			this.__renderer.onDiagramRendered.subscribe(function ()
			{
				this.__resizeAndScrollCanvas.update();
				this.__removeLoader();
			}, this);
			this.__renderer.render();
		},

		/**
		 * @param {Element} element
		 * @private
		 */
		__setUpViewerWrapper: function (element)
		{
			domUtil.addClass(element, efwdViewer.Diagram.VIEWER_CLASS);
		}
	}
});

efwdViewer.Diagram.VIEWER_CLASS = "efwd-viewer";
