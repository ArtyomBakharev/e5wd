"use strict";
goog.provide("efwdViewer.Renderer");

efwdViewer.Renderer = AppTools.createClass({

	/**
	 * @param {efwdRenderer.Factory} rendererFactory
	 * @param {efwdViewer.data.Repository} repository
	 */
	construct: function (rendererFactory, repository)
	{
		this.onDiagramRendered = new Observer();

		this.__rendererFactory = rendererFactory;
		this.__repository = repository;
	},

	members: {

		getCurrentStatusElement: function ()
		{
			return this.__currentStatusElement;
		},

		getElements: function ()
		{
			return this.__statuses.concat(this.__actions);
		},

		render: function ()
		{
			this.__getEntities(function ()
			{
				this.__statuses = this.__statuses.map(function (status)
				{
					return this.__createElement("status", status)
				}, this);
				this.__actions = this.__actions.map(function (action){ return this.__createElement("action", action) },
					this);
				this.onDiagramRendered.fire();
			}, this);
		},

		setCurrentStatusId: function (statusId)
		{
			this.__currentStatusId = statusId;
		},

		/**
		 * @private
		 */
		__createElement: function (type, model)
		{
			var element = this.__rendererFactory.createElement(type, model);
			element.setUserData("model", model);

			if (type == "status")
			{
				if (this.__currentStatusId && model.getId() == this.__currentStatusId)
				{
					this.__currentStatusElement = element;
					element.setProperty("currentStatus", true);
				}

				if (this.__process.getInitial_status_id() == model.getId())
				{
					element.setProperty("initialStatus", true);
				}
			}

			element.render();
			return element;
		},

		/**
		 * @private
		 */
		__getEntities: function (callback, context)
		{
			var count = 3;

			function next()
			{
				if (--count == 0)
				{
					callback.call(context || window);
				}
			}

			this.__repository.getStatusChangeActions(function (actions)
			{
				this.__actions = actions;
				next();
			}, this);

			this.__repository.getStatuses(function (statuses)
			{
				this.__statuses = statuses;
				next();
			}, this);

			this.__repository.getProcess(function (process)
			{
				this.__process = process;
				next();
			}, this);
		}
	}

});
