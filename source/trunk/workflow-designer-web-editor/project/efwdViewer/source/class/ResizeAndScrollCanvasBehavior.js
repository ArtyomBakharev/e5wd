"use strict";
goog.provide("efwdViewer.ResizeAndScrollCanvasBehavior");

efwdViewer.ResizeAndScrollCanvasBehavior = AppTools.createClass({

	/**
	 * @param {efwdViewer.Renderer} renderer
	 * @param {efwdViewer.Canvas} canvas
	 */
	construct: function (renderer, canvas)
	{
		this.__renderer = renderer;
		this.__canvas = canvas;
	},

	members: {

		update: function ()
		{
			var bounds = efwdRenderer.Helper.getBounds(this.__renderer.getElements());
			this.__canvas.setCanvasFrame(bounds.left, bounds.top, bounds.right - bounds.left,
				bounds.bottom - bounds.top);
			var currentStatusElement = this.__renderer.getCurrentStatusElement();
			if (currentStatusElement)
			{
				this.__canvas.centerScreenTo({x: currentStatusElement.getProperty("x"), y: currentStatusElement.getProperty("y")});
			}
		}
	}

});
