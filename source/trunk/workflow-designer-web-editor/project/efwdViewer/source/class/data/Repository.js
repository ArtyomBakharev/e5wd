"use strict";
goog.provide("efwdViewer.data.Repository");

goog.require("efwdViewer.data.SimpleObjectModel");

efwdViewer.data.Repository = AppTools.createClass({

	/**
	 * @param {efwdCommon.data.Repository} generalRepository
	 */
	construct: function (generalRepository)
	{
		this.__repository = generalRepository;
	},

	members: {

		initialize: function (processId)
		{
			this.__processId = processId;
		},

		getProcess: function (callback, context)
		{
			if (this.__process)
			{
				callback.call(context || window, this.__process);
			}
			else
			{
				this.__repository.getProcess(this.__processId, function (process)
				{
					this.__process = this.__transformObject("process", process);
					callback.call(context || window, this.__process);
				}, this);
			}
		},

		getStatusChangeActions: function (callback, context)
		{
			if (this.__actions)
			{
				callback.call(context || window, this.__actions);
			}
			else
			{
				this.__repository.getActions(this.__processId, function (actions)
				{
					this.__actions = actions.filter(function (x){ return !!x.from_status_id })
						.map(function (x){ return this.__transformObject("action", x) }, this);
					callback.call(context || window, this.__actions);
				}, this);
			}
		},

		getStatuses: function (callback, context)
		{
			if (this.__statuses)
			{
				callback.call(context || window, this.__statuses);
			}
			else
			{
				this.__repository.getStatuses(this.__processId, function (statuses)
				{
					this.__statuses = statuses.map(function (x){ return this.__transformObject("status", x) }, this);
					callback.call(context || window, this.__statuses);
				}, this);
			}
		},

		/**
		 * @private
		 */
		__transformObject: function (type, object)
		{
			var skeleton = AppTools.deepCopy(efwdCommon.Helper.getEntitySkeleton(type, object.$type));
			AppTools.deepCopy(object, skeleton);

			return new efwdViewer.data.SimpleObjectModel(skeleton);
		}
	}

});
