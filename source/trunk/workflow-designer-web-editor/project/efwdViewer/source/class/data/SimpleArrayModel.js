"use strict";
goog.provide("efwdViewer.data.SimpleArrayModel");

efwdViewer.data.SimpleArrayModel = AppTools.createClass({

	statics: {
		/**
		 * @param {efwdViewer.data.SimpleArrayModel} model
		 */
		serialize: function(model)
		{
			return model.__array.map(function(x){ return efwdViewer.data.SimpleObjectModel.serialize(x) });
		}
	},

	construct: function (array)
	{
		this.__array = array.map(function (item)
		{
			return efwdViewer.data.SimpleObjectModel.createModel(item);
		});
		this.length = array.length;
	},

	members: {

		getLength: function ()
		{
			return this.length;
		},

		toArray: function ()
		{
			return this.__array;
		},

		addListener: function (){},
		removeListenerById: function (){}
	}
});
