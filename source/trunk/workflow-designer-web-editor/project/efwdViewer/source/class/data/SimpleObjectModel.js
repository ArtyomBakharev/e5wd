"use strict";
goog.provide("efwdViewer.data.SimpleObjectModel");

goog.require("efwdViewer.data.SimpleArrayModel");

efwdViewer.data.SimpleObjectModel = AppTools.createClass({

	statics: {
		createModel: function (source)
		{
			var model = source;
			if (Array.isArray(model))
			{
				model = new efwdViewer.data.SimpleArrayModel(model);
			}
			else if (model !== null && typeof model == "object")
			{
				model = new efwdViewer.data.SimpleObjectModel(model);
			}

			return model;
		},

		/**
		 * @param {efwdViewer.data.SimpleObjectModel|efwdViewer.data.SimpleArrayModel} model
		 */
		serialize: function (model)
		{
			if (model)
			{
				if (model instanceof efwdViewer.data.SimpleArrayModel)
				{
					return efwdViewer.data.SimpleArrayModel.serialize(model);
				}
				else if (model instanceof efwdViewer.data.SimpleObjectModel)
				{
					var serialized = {};
					AppTools.objForEach(model.__data, function (value, key)
					{
						if (value && (value instanceof efwdViewer.data.SimpleObjectModel || value instanceof efwdViewer.data.SimpleArrayModel))
						{
							value = efwdViewer.data.SimpleObjectModel.serialize(value);
						}

						serialized[key] = value
					});

					return serialized;
				}
			}

			return model;
		}
	},

	construct: function (source)
	{
		this.__data = {};
		for (var key in source)
		{
			if (source.hasOwnProperty(key))
			{
				this.__data[key] = efwdViewer.data.SimpleObjectModel.createModel(source[key]);
				this["get" + key.substr(0, 1).toUpperCase() + key.substr(1)] = this.get.bind(this, key);
			}
		}
	},

	members: {

		get: function (key)
		{
			return this.__data[key];
		},

		addListener: function (){},
		removeListenerById: function (){}
	}
});
