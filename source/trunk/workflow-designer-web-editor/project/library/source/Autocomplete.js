
({
	/**
	 * @param fake
	 * @param [msg=""]
	 */
	assertCalledOnce: function(fake, msg){},
	/**
	 * @param fake
	 * @param [msg=""]
	 */
	assertCalledTwice: function(fake, msg){},
	/**
	 * @param fake
	 * @param [msg=""]
	 */
	assertCalledThrice: function(fake, msg){},
	/**
	 * @param fake
	 * @param [msg=""]
	 */
	assertCalled: function(fake, msg){},
	/**
	 * @param method
	 * @param count
	 */
	assertCallCount: function(method, count){},
	assertCalledWith: function(method){},
	/**
	 * @param fake
	 * @param [msg=""]
	 */
	assertNotCalled: function(fake, msg){},
	assertCallOrder: function(){}
});
