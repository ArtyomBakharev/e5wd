/**
 * Хелпер для стандартных функций браузера
 */
qx.Class.define("library.BOM", {

	type: "static",

	statics: {
		/**
		 * Возвращает хеш url-параметров (которые следуют за ? в url)
		 */
		getUrlParameters: function ()
		{
			if (!this.__urlParameters)
			{
				this.__urlParameters = window.location.search
					.substr(1).split("&")
					.map(function (x){ return x.split("="); })
					.mapToObject(function (x){ return x.length >= 2 ? x : x.concat(["", ""]); });
			}

			return this.__urlParameters;
		}
	}
});
