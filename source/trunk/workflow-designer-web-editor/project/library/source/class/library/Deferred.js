/*
 #ignore(Deferred)
 */

/**
 * @lends {Deferred}
 */
qx.Class.define("library.Deferred", {type: "static"});
library.Deferred = Deferred;
