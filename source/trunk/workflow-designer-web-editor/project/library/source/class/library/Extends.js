qx.Class.define("library.Extends", {

	type: "static",

	statics: {
		extendNativeTypes: function ()
		{
			if (this.__nativeTypesExtended)
			{
				return;
			}

			//qx
			qx.require = function (cls){};

			qx.require(qx.lang.Array);
			qx.require(qx.lang.Function);
			qx.require(qx.lang.Number);
			qx.require(qx.lang.Object);
			qx.require(qx.lang.String);

			var items = {
				Array: {
					members: ["append", "clone", "contains", "exclude", "insertAfter", "insertAt", "insertBefore", "max",
						"min", "remove", "removeAll", "removeAt", "sum", "unique"],
					statics: ["cast", "equals", "fromArguments", "fromShortHand", "toArray"]
				},
				Function: {
					members: ["attempt", "bind", "create", "curry", "delay", "getName", "listener", "periodical"],
					statics: ["empty", "getCaller", "globalEval", "returnFalse", "returnNull", "returnThis",
						"returnTrue", "returnZero"]
				},
				Number: {
					members: ["isBetweenRange", "isInRange", "limit"]
				},
				Object: {
					statics: ["carefullyMergeWith", "clone", "contains", "empty", "fromArray", "getKeyFromValue", "getKeys",
						"getKeysAsString", "getLength", "getValues", "hasMinLength", "invert", "isEmpty", "merge", "mergeWith",
						"select", "toUriParameter"]
				},
				String: {
					members: ["camelCase", "capitalize", "clean", "contains", "endsWith", "escapeRegexpChars", "firstLow",
						"firstUp", "format", "hyphenate", "pad", "repeat", "startsWith", "stripScripts", "stripTags",
						"toArray", "trim", "trimLeft", "trimRight"]
				}
			};

			Object.forEach(items, function (ext, cls)
			{
				var source = qx.lang[cls];
				var dest;
				if (ext.statics)
				{
					dest = window[cls];
					Object.forEach(ext.statics, function (name)
					{
						if (!dest[name])
						{
							dest[name] = qx.lang.Function.bind(source[name], source);
						}
					});
				}
				if (ext.members)
				{
					dest = window[cls].prototype;
					Object.forEach(ext.members, function (name)
					{
						if (!dest[name])
						{
							dest[name] = source[name].methodize(source);
						}
					});
				}
			});

			!Object.keys && (Object.keys = Object.getKeys);

			Object.mapsEqual = function (map1, map2)
			{
				var keys = Object.keys(map1);
				var keys2 = Object.keys(map2);
				if (keys.length != keys2.length || keys2.exclude(keys).length > 0)
				{
					return false;
				}

				return keys.every(function (key){ return map1[key] == map2[key] });
			};

			this.checkEs5();

			qx.bom.Template.tmpl = this.__tmpl;

			qx.Class.patch(qx.core.Object, library.MObjectExtend);

			this.__nativeTypesExtended = true;
		},

		checkEs5: function()
		{
			//JSON
			if (!window.JSON)
			{
				window.JSON = qx.lang.Json;
			}

			//Array.isArray
			if (!Array.isArray)
			{
				Array.isArray = qx.lang.Type.isArray.bind(qx.lang.Type);
			}

			if (!Array.prototype.reduce) {
				Array.prototype.reduce = function reduce(accumulator){
					if (this===null || this===undefined) throw new TypeError("Object is null or undefined");
					var i = 0, l = this.length >> 0, curr;

					if(typeof accumulator !== "function") // ES5 : "If IsCallable(callbackfn) is false, throw a TypeError exception."
						throw new TypeError("First argument is not callable");

					if(arguments.length < 2) {
						if (l === 0) throw new TypeError("Array length is 0 and no second argument");
						curr = this[0];
						i = 1; // start accumulating at the second element
					}
					else
						curr = arguments[1];

					while (i < l) {
						if(i in this) curr = accumulator.call(undefined, curr, this[i], i, this);
						++i;
					}

					return curr;
				};
			}
		},

		/**
		 * @private
		 */
		__tmpl: function (template, view, partials, send_fun)
		{
			if (qx.lang.Type.isArray(view))
			{
				return view.map(
					function (item)
					{
						return qx.bom.Template.toHtml(template, item, partials, send_fun);
					}).join("");
			}
			else
			{
				return qx.bom.Template.toHtml(template, view, partials, send_fun);
			}
		}

		/*,

		 extendUiTypes: function()
		 {
		 qx.Class.patch(qx.ui.form.Button, library.ui.extend.MExecutableExtends);
		 qx.Class.patch(qx.ui.menu.AbstractButton, library.ui.extend.MExecutableExtends);
		 qx.Class.patch(qx.ui.form.SelectBox, library.ui.extend.MSelectBoxExtends);
		 qx.Class.patch(qx.ui.table.cellrenderer.Abstract, library.ui.extend.MAbstractCellRendererExtends);
		 qx.Class.patch(qx.ui.list.List, library.ui.extend.MListExtends);
		 }*/
	}
});
