/**
 * Реализация паттерна IOC/DI
 * @extends {qx.core.Object}
 */
qx.Class.define("library.IOC", {

	extend: qx.core.Object,

	implement: library.IServiceLocator,

	statics: {
		SCOPE_DEFAULT: "default",
		SCOPE_SINGLETON: "singleton"
	},

	construct: function (container)
	{
		this._ioc = container;
		this.__nextClassId = 0;
	},

	members: {

		register: function (cls, options)
		{
			var self = this;

			if (!options)
			{
				options = {};
			}

			if (!options.instance)
			{
				var ioc = this._ioc.Register(this.__getClassId(cls)).withConstructor(options.cls || cls);
				if (options.args)
				{
					ioc = ioc.withDependencies.apply(ioc, options.args.map(function (arg){ return self.__getClassId(arg) }));
				}

				if (!("scope" in options) || options.scope == this.self(arguments).SCOPE_SINGLETON)
				{
					ioc = ioc.asSingleton();
				}
			}
			else
			{
				this._ioc.RegisterInstance(this.__getClassId(cls), options.instance);
			}
		},

		registerAll: function (scheme)
		{
			scheme.forEach(function (item, index)
			{
				this.register(item[0], item[1] || {});
			}, this);
		},

		/**
		 * @param {Function} service
		 */
		getService: function (service)
		{
			return this._ioc.Load(this.__getClassId(service));
		},

		/**
		 * @private
		 */
		__getClassId: function (cls)
		{
			if (cls.$$type == "Class")
			{
				return cls.classname;
			}

			if (cls.$$type == "Interface")
			{
				return cls.name;
			}

			if (!cls.$$IOC_class_id)
			{
				cls.$$IOC_class_id = "clsId" + this.__nextClassId++;
			}

			return cls.$$IOC_class_id;
		}

	}
});
