/**
 * Объект хранит обработчики событий и биндинги для разных объектов. По требованию он удаляет все обработчики и биндинги
 * или для определённого объекта
 * @extends {qx.core.Object}
 */
qx.Class.define("library.ListenersStore", {

	extend: qx.core.Object,

	construct: function ()
	{
		this.__listeners = [];
	},

	members: {

		/**
		 * Добавить биндинг к объекту
		 *
		 * @param {qx.core.Object} source
		 * @param {String} sourceProperty
		 * @param {qx.core.Object} target
		 * @param {String} targetProperty
		 * @param {Object|Function} [options={}]
		 * @param {Object} [context=null]
		 */
		addBinding: function (source, sourceProperty, target, targetProperty, options, context)
		{
			if (qx.lang.Type.isFunction(options))
			{
				options = {converter: options.bind(context || window)};
			}

			this.__listeners.push({
				object: source,
				id: source.bind(sourceProperty, target, targetProperty, options),
				binding: true
			});
		},

		/**
		 * Добавить обработчик события для объекта
		 *
		 * @param {qx.core.Object} object
		 * @param {String} name
		 * @param {Function} callback
		 * @param {*} [context=window]
		 */
		addListener: function (object, name, callback, context)
		{
			this.__listeners.push({
				object: object,
				id: object.addListener(name, callback, context || window),
				binding: false
			});
		},

		/**
		 * @param {qx.core.Object} source
		 * @param {String} sourceProperty
		 * @param {qx.core.Object} target
		 * @param {String} targetProperty
		 * @param {Function} converter
		 * @param {Object} [context=null]
		 */
		addMultiBinding: function (source, sourceProperty, /*source2, source2Property, ...*/target, targetProperty, converter, context)
		{
			var args = Array.slice(arguments);
			if (qx.lang.Type.isFunction(args.last()))
			{
				args.push(window);
			}
			args.push(true);

			var ids = library.data.Helper.multiBind.apply(library.data.Helper, args);

			this.__listeners.append(ids.map(function (x)
			{
				return {
					object: x[0],
					id: x[1],
					binding: true
				}
			}));
		},

		/**
		 * @param {qx.core.Object} object
		 * @param {String} property
		 * @param {Function} listener
		 * @param {Object} [context=null]
		 * @return {*}
		 */
		addPropertyListener: function(object, property, listener, context)
		{
			this.__listeners.push({
				object: object,
				id: library.data.Helper.addPropertyListener(object, property, listener, context),
				binding: true
			});
		},

		/**
		 * Удалить все обработчики и биндинги, связанные с определённым объектом
		 *
		 * @param {qx.core.Object} object
		 * @param {Boolean} [removeListeners=true]
		 */
		removeObject: function (object, removeListeners)
		{
			if (removeListeners === undefined || removeListeners)
			{
				this.__listeners
					.filter(function (x){ return x.object == object })
					.forEach(function (x){ this.__removeListener(x) }, this);
			}

			this.__listeners.removeByLambda(function (x){ return x.object == object });
		},

		/**
		 * Удалить объект по возбуждению события
		 *
		 * @param {qx.core.Object} object
		 * @param {String} event
		 */
		removeObjectOn: function (object, event)
		{
			this.addListener(object, event, function (){ this.removeObject(object) }, this);
		},

		/**
		 * Удалить все биндинги и обработчики
		 */
		removeAllListeners: function ()
		{
			this.__listeners.forEach(function (x){ this.__removeListener(x) }, this);
			this.__listeners = [];
		},

		/**
		 * @private
		 */
		__removeListener: function (listener)
		{
			if (listener.binding)
			{
				listener.object.removeBinding(listener.id);
			}
			else
			{
				listener.object.removeListenerById(listener.id);
			}
		}

	},

	destruct: function ()
	{
		this.removeAllListeners();
	}
});
