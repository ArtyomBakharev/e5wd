qx.Mixin.define("library.MObjectExtend", {

	members: {

		abstractMethod: function (args)
		{
			throw new Error("[" + args.callee.self.classname + "#" + args.callee.displayName.match(/\.([^\.]*)\(\)$/)[1] + "] Method is abstract!");
		},

		/**
		 * @return {library.ListenersStore}
		 */
		getListenersStore: function()
		{
			return this.__listenersStore || (this.__listenersStore = new library.ListenersStore());
		},

		implementsInterface: function (iface)
		{
			return qx.Class.hasInterface(this.constructor, iface);
		},

		includesMixin: function (mixin)
		{
			return qx.Class.hasMixin(this.constructor, mixin);
		},

		notImplemented: function (args)
		{
			throw new Error("[" + args.callee.self.classname + "#" + args.callee.displayName.match(/\.([^\.]*)\(\)$/)[1] + "] Not implemented case!");
		},

		/**
		 * @param name
		 * @param [part=null]
		 */
		registerObjectPart: function (name, part)
		{
			var getter = "get" + name.firstUp();
			var setter = "set" + name.firstUp();

			if (this[getter] || this[setter])
			{
				throw new Error("This field in object already exists!");
			}

			this[getter] = function (){ return part; };
			this[setter] = function (value)
			{
				part = value;
				return this;
			};
		}
	},

	destruct: function()
	{
		this.__listenersStore && this.__listenersStore.dispose();
	}
});
