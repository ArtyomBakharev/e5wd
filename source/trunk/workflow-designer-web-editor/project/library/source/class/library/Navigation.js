/**
 * @extends {qx.core.Object}
 */
qx.Class.define("library.Navigation", {

	extend: qx.core.Object,

	members: {
		init: function ()
		{
			this._currentHash = location.hash.substr(1);
			this._handlers = [];

			var self = this;

			function checkHash()
			{
				setTimeout(function ()
				{
					self._checkHash();
					checkHash();
				}, 100);
			}

			checkHash();
		},
		firstCheck: function ()
		{
			if (location.hash.substr(1))
			{
				this._currentHash = "";
				this._checkHash();
			}
		},
		handleHashChanged: function (prefix, handler, context)
		{
			this._handlers.push({prefix: prefix.trim("/"), func: handler, context: context});
		},

		setHashArguments: function (prefix, args, fireEvent/*= true*/)
		{
			var hash = prefix.trim("/") + "/" + args.join("/");
			location.hash = hash;
			if (!Object.ifUndef(fireEvent, true))
			{
				this._currentHash = hash;
			}
			else
			{
				this._checkHash();
			}
		},
		/**
		 * @protected
		 */
		_checkHash: function ()
		{
			if (location.hash.substr(1) != this._currentHash)
			{
				this._hashChanged();
			}
		},
		/**
		 * @protected
		 */
		_hashChanged: function ()
		{
			var hash = this._currentHash = location.hash.substr(1).trim("/");
			//var args = hash.split("/");
			this._handlers.forEach(function (handler)
			{
				if (hash.startsWith(handler.prefix))
				{
					var args = hash.substr(handler.prefix.length).trimChars("/").split("/");
					handler.func.apply(handler.context || window, args);
				}
			});
		}
	}
});
