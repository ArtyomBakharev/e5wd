/*
 #asset(library/icon/ajax-loader.gif)
 */

qx.Class.define("library.bom.Loader", {

	extend: qx.core.Object,

	properties: {
		blocked: {
			init: false,
			nullable: false,
			apply: "__applyBlocked",
			check: "Boolean"
		},
		element: {
			init: document.body,
			check: "Element"
		}
	},

	/**
	 * @param {Element} [element=null]
	 * @param {Boolean} [withImage=true]
	 */
	construct: function(element, withImage)
	{
		element && this.setElement(element);
		this.__withImage = withImage === undefined ? true : !!withImage;
	},

	members: {

		block: function()
		{
			if (!this.__blockerElement)
			{
				this.__blockerElement = qx.bom.Element.create("div");
				qx.bom.element.Style.setStyles(this.__blockerElement, {
					position: "absolute",
					left: 0,
					right: 0,
					top: 0,
					bottom: 0,
					backgroundColor: "white",
					opacity: 0.5,
					zIndex: 10000
				});

				if (this.__withImage)
				{
					/** @type {qx.util.ResourceManager} */
					var resourceManager = qx.util.ResourceManager.getInstance();
					var imageId = "library/icon/ajax-loader.gif";
					var loader = qx.bom.Element.create("img", {
						src: resourceManager.toUri(imageId),
						alt: "loading..."
					});
					var loaderWidth = resourceManager.getImageWidth(imageId);
					var loaderHeight = resourceManager.getImageHeight(imageId);
					qx.bom.element.Style.setStyles(loader, {
						width: loaderWidth + "px",
						height: loaderHeight + "px",
						left: "50%",
						top: "50%",
						marginLeft: "-" + (loaderWidth / 2) + "px",
						marginTop: "-" + (loaderHeight / 2) + "px",
						position: "absolute"
					});

					this.__blockerElement.appendChild(loader);
				}

				this.getElement().appendChild(this.__blockerElement);

				this.setBlocked(true);
			}
		},

		unblock: function()
		{
			if (this.__blockerElement)
			{
				qx.bom.Collection.create(this.__blockerElement).remove();
				this.__blockerElement = null;
				this.setBlocked(false);
			}
		},

		__applyBlocked: function(blocked)
		{
			if (blocked)
			{
				this.block();
			}
			else
			{
				this.unblock();
			}
		}
	}
});
