qx.Class.define("library.data.Helper", {

	type: "static",

	statics: {

		/**
		 * @param {qx.core.Object} object
		 * @param {String} property
		 * @param {Function} listener
		 * @param {Object} [context=null]
		 * @return {*}
		 */
		addPropertyListener: function (object, property, listener, context)
		{
			if (!this.__propertyPlaceHolder)
			{
				this.__propertyPlaceHolder = qx.data.marshal.Json.createModel({data: null});
			}
			return object.bind(property, this.__propertyPlaceHolder, "data", {converter: function (x)
			{
				listener.call(context || window, x);
				return null;
			}});
		},

		getPropertyGetter: function (object, chain)
		{
			chain = chain
				.replace(/(^|\.)([^\[\]\(\)\.]+)(\[|\.|$)/g, "$1get('$2')$3")
				.replace(/(^|\.)([^\[\]\(\)\.]+)(\[|\.|$)/g, "$1get('$2')$3")
				.replace(/\[(\d+)\]/g, ".getItem('$1')");

			if (chain.substr(0, 1) != ".")
			{
				chain = "." + chain;
			}

			var getter = new Function("return this" + chain);
			return getter.bind(object);
		},

		/**
		 * @param {qx.core.Object} source
		 * @param {String} sourceProperty
		 * @param {qx.core.Object} target
		 * @param {String} targetProperty
		 * @param {Function} converter
		 * @param {Object} [context=null]
		 * @param {Boolean} [returnExtendedResult=false]
		 * @return {Object[]}
		 */
		multiBind: function (source, sourceProperty, /*source2, source2Property, ...*/target, targetProperty, converter,
		                     context, returnExtendedResult)
		{
			var sources;
			if (Array.isArray(source))
			{
				sources = source;
				returnExtendedResult = context;
				context = converter;
				converter = targetProperty;
				targetProperty = target;
				target = sourceProperty;
			}
			else
			{
				sources = [];

				for (var i = 1; i - 1 < arguments.length; i += 2)
				{
					var curSource = arguments[i - 1];
					var curSourceProperty = arguments[i];
					if (!qx.lang.Type.isString(curSourceProperty))
					{
						converter = curSource;
						context = curSourceProperty;
						returnExtendedResult = arguments[i + 1];
						break;
					}

					sources.push([curSource, curSourceProperty]);
				}

				target = sources.pop();
				targetProperty = target[1];
				target = target[0];
			}

			if (!context)
			{
				context = window;
			}

			var sourceGetters = sources.map(function (source){ return this.getPropertyGetter(source[0], source[1]) },
				this);

			return sources.map(function (source)
			{
				var id = source[0].bind(source[1], target, targetProperty, {converter: function ()
				{
					return converter.apply(context, sourceGetters.callAll());
				}});

				return returnExtendedResult ? [source[0], id] : id;
			});
		},

		/**
		 * @param {Object} object
		 * @param {qx.core.Object} model
		 */
		restoreObjectToModel: function (object, model)
		{
			Object.forEach(object, function (value, key)
			{

				var curValue = model.get(key);

				if (curValue && curValue instanceof qx.data.Array)
				{
					curValue.removeAll();
					if (qx.lang.Type.isArray(value))
					{
						curValue.append(value.map(function (x){ return qx.data.marshal.Json.createModel(x) }));
					}
				}
				else if (curValue && curValue instanceof qx.core.Object)
				{
					if (qx.lang.Type.isObject)
					{
						this.restoreObjectToModel(value, curValue);
					}
				}
				else
				{
					model.set(key, value);
				}
			}, this);
		},

		/**
		 * @param object
		 */
		setUpHierarchyEvents: function (object)
		{
			object.__suhe = true;

			if (!qx.Class.hasMixin(object.constructor, library.data.MChangeSelf))
			{
				qx.Class.include(object.constructor, library.data.MChangeSelf);
			}

			if (object instanceof qx.data.Array)
			{
				this.__setUpArrayHierarchyEvents(object);
			}
			else
			{
				this.__setUpObjectHierarchyEvents(object);
			}
		},

		/**
		 * @private
		 */
		__checkAndSetUpHierarchyEvents: function (object, parent, parentProperty)
		{
			if (object && object instanceof qx.core.Object)
			{
				this.setUpHierarchyEvents(object);
				object.addListener("selfChanged", function ()
				{
					if (parentProperty.array)
					{
						parent.fireDataEvent("change", {type: "modified", items: [object]});
					}
					else
					{
						parent.fireDataEvent("propertyChanged", {name: parentProperty, value: object})
					}
				});
			}
		},

		/**
		 * @private
		 */
		__setUpObjectHierarchyEvents: function (object)
		{
			var properties = qx.util.PropertyUtil.getProperties(object.constructor);
			Object.forEach(properties, function (property, name)
			{
				if (!property.event)
				{
					return;
				}

				object.addListener(property.event, function (e)
				{

					//модифицируем новый объект
					var value = e.getData();
					if (value !== e.getOldData())
					{
						this.__checkAndSetUpHierarchyEvents(value, object, property);
					}

					object.fireDataEvent("propertyChanged", {name: name, value: value});

				}, this);


				//модифицируем дочерний объект
				var value = object.get(name);
				this.__checkAndSetUpHierarchyEvents(value, object, property);
			}, this);

			object.addListener("propertyChanged", function (){ object.fireDataEvent("selfChanged", object, object); });
		},

		/**
		 * @private
		 */
		__setUpArrayHierarchyEvents: function (array)
		{
			array.addListener("changeArray", function (e)
			{
				var data = e.getData();

				//модифицируем новые объекты
				data.insert.forEach(function (item)
				{
					if (item && item instanceof qx.core.Object)
					{
						this.__checkAndSetUpHierarchyEvents(item, array, {array: true})
					}
				}, this);

				array.fireDataEvent("selfChanged", array, array);

			}, this);

			//модифицируем дочерние объекты
			array.forEach(function (item)
			{
				this.__checkAndSetUpHierarchyEvents(item, array, { array: true });
			}, this);
		}
	}

});
