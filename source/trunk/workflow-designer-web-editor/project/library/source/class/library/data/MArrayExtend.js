qx.Mixin.define("library.data.MArrayExtend", {

	events: {
		"changeArray": "qx.event.type.Data"
	},

	properties: {
		count: {
			init: 0,
			check: "Integer",
			nullable: false,
			event: "changeCount"
		},
		loaded: {
			init: false,
			check: "Boolean",
			nullable: false,
			event: "changeLoaded"
		}
	},

	construct: function ()
	{
		this.setCount(this.getLength());
		this.addListener("change", function (e)
		{
			var data = e.getData();
			var insert;
			var remove;

			if (this.__splice)
			{
				remove = this.__splice.removed;
				insert = this.__splice.inserted;
			}
			else
			{
				switch (data.type)
				{
					case "remove":
						remove = data.items;
						insert = [];

						break;

					case "add":
						if (data.items === null)
						{
							insert = this.toArray().slice(data.start,
								data.start + this.getLength() - this.getCount());
							remove = [];
						}
						else
						{
							insert = data.items;
							remove = this.__removedItems || [];
						}

						break;

					case "modified":
						insert = [];
						remove = [];
						break;

					case "order":
						insert = [];
						remove = this.__removedItems;
						break;

					default:
						return;
				}
			}

			this.__removedItems = null;
			this.setCount(this.getLength());
			this.fireDataEvent("changeArray", {index: data.start, insert: insert, remove: remove});
		}, this);
	},

	members: {

		__oldSetItem: qx.data.Array.prototype.setItem,
		__oldSpliceItem: qx.data.Array.prototype.splice,
//		__oldRemoveAt: qx.data.Array.prototype.removeAt,

//		removeAt: function(index)
//		{
//			this.__removedItems = [this.getItem(index)];
//			var result = this.__oldRemoveAt(index);
//			this.__removedItems = null;
//			return result;
//		},

		/**
		 * @return {library.Deferred}
		 */
		deferred: function()
		{
			if (this.isLoaded())
			{
				return library.Deferred.success(this);
			}
			else
			{
				var deferred = library.Deferred();
				this.addListenerOnce("changeLoaded", function(){
					deferred.call(this);
				}, this);
				return deferred;
			}
		},

		setItem: function (index, value)
		{
			this.__removedItems = [this.getItem(index)];
			this.__oldSetItem(index, value);
			this.__removedItems = null;
		},

		splice: function (startIndex, amount, varargs)
		{
			this.__splice = {
				removed: this.toArray().slice(startIndex, startIndex + amount),
				inserted: Array.slice(arguments, 2)
			};

			var arr = this.__oldSpliceItem.apply(this, arguments);
			this.__splice = null;
			return arr;
		}

	}

});

qx.Class.patch(qx.data.Array, library.data.MArrayExtend);
