qx.Mixin.define("library.data.MChangeSelf", {

	events: {
		/**
		 * @param name
		 * @param value
		 */
		"propertyChanged": "qx.event.type.Data",
		"selfChanged": "qx.event.type.Data"
	}

});