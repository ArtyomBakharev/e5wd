/**
 * @extends {qx.core.Object}
 */
qx.Class.define("library.data.ObjectBinding", {

	extend: qx.core.Object,

	statics: {
		isFieldModified: function (key, changes)
		{
			return Util.breakableEach(changes, function ($break, change)
			{

				if (this.isChangeModifyField(key, change))
				{
					$break(true);
				}

			}, this, false, false, false);
		},

		getModifiedObject: function (changes)
		{
			var modified = {};
			changes.forEach(function (change)
			{
				if (change.key.length == 0)
				{
					modified = Object.deepCopy(change.value);
				}
				else
				{
					Object.pathAccess(modified, change.key, change.action == "set" ? Object.deepCopy(change.value) : false, change.action == "remove");
				}
			});

			return modified;
		},

		removeChangesByKey: function (key, changes)
		{
			var newChanges = [];
			changes.forEach(function (change)
			{
				if (!this.isChangeModifyField(key, change))
				{
					newChanges.push(change);
				}
			}, this);

			return newChanges;
		},

		isChangeModifyField: function (key, change)
		{
			var keyArray = key;
			var changeArray = change.key;

			if (keyArray.length > changeArray.length)
			{
				keyArray = keyArray.slice(0, changeArray.length);
			}
			else if (keyArray.length < changeArray.length)
			{
				changeArray = changeArray.slice(0, keyArray.length);
			}

			return Array.equals(keyArray, changeArray);
		}
	},

	construct: function (object)
	{
		this._object = object || {};
		this._handlers = {};
		this._lastHandlerId = 0;
		this._changes = [];
	},

	members: {

		/**
		 * @return {Object}
		 */
		getObject: function ()
		{
			return this._object;
		},

		getLastCopy: function (){ return this._lastCopy || this._object; },

		commit: function (handler)
		{
			var lastCopy = Object.deepCopy(this._object);

			this._changes.forEach(function (item)
			{
				item.old = item.key.length ? Object.pathAccess(this._object, item.key, item.value, item.action == "remove") : this._object;
				if (!item.key.length)
				{
					this._object = item.value;
				}
			}, this);

			this._changes.removeByLambda(function (item){return item.value === item.old});

			if (this._changes.length)
			{
				this._preprocessCommit(lastCopy)
					.hand((function ()
				{

					this._lastCopy = lastCopy;
					Object.forEach(this._handlers, function (handler_)
					{
						if (!handler || handler != handler_)
						{
							handler_.func && handler_.func.call(handler_.context, this._changes, this._object);
						}
					}, this);

				}).bind(this), (function ()
				{

					this._object = lastCopy;

				}).bind(this))
					.doIt(function (){ this._changes = []; }, this);
			}
		},

		/**
		 * @param {string[]} key
		 * @return {mixed} value
		 */
		getProp: function (key)
		{
			if (key.length == 0)
			{
				return this.getObject();
			}
			return Object.pathAccess(this._object, key);
		},

		/**
		 * @param {function} func function<key:string[], value:mixed, oldValue: mixed, object:Object>
		 */
		handleChanges: function (func, context)
		{
			var self = this;
			var handler = {
				id: ++this._lastHandlerId,
				func: func,
				context: context,
				remove: function ()
				{
					delete self._handlers[handler.id];
				},
				commit: function ()
				{
					self.commit(handler);
				},
				undo: function ()
				{
					self.undo(handler);
				}
			};

			return this._handlers[handler.id] = handler;
		},

		rollback: function ()
		{
			this._changes = [];
		},

		/**
		 * @param {Object} object
		 * @param [handler=undefined]
		 */
		setObject: function (object)
		{
			this.setProp([], object);
		},

		/**
		 * @param {string} action
		 * @param {string[]} key
		 * @param value
		 * @param [handler=undefined]
		 */
		modifyProp: function (action, key, value)
		{
			this._changes.push({action: action, key: key, value: value});
		},

		/**
		 * @param {string[]} key
		 * @param value
		 * @param [handler=undefined]
		 */
		setProp: function (key, value)
		{
			this.modifyProp("set", key, value);
		},

		/**
		 * @param {string[]} key
		 * @param [handler=undefined]
		 */
		removeProp: function (key)
		{
			this.modifyProp("remove", key, null);
		},

		//коммит последней копии объекта
		undo: function (handler)
		{
			this.setObject(this._lastCopy);
			this.commit(handler);
		},

		/**
		 * @protected
		 */
		_preprocessCommit: function (lastCopy){ return Deferred.success(); }
	}
});
