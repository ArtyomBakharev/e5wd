/**
 * Массив постоянно синхронизируется с {@link qx.data.Array} при этом оставаясь отсортированным
 * @extends {qx.data.Array}
 */
qx.Class.define("library.data.SortedArray", {

	extend: qx.data.Array,

	/**
	 * @param {qx.data.Array} array
	 * @param {Function} sortFunc
	 */
	construct: function (array, sortFunc)
	{
		this.__listenersStore = new library.ListenersStore();
		this.__sourceArray = array;
		this.__sortFunc = sortFunc;
		var arr = this.__sourceArray.toArray().clone().sort(this.__sortFunc);
		this.base(arguments, arr);

//		this.__updateOn = updateOn;
//		arr.forEach(function(x){ this.__processItemAdded(x) }, this);

		this.__setUpSynchronization();
	},

	members: {

		/**
		 * Ищет первый элемент в массиве, который меньше чем item (возвращает -1, если все элементы больше или элементов нет)
		 * @param {*} item
		 */
		findLeft: function (item)
		{
			var array = this.toArray();
			var left = 0;
			var right = array.length;
			if (right == 0)
			{
				return -1;
			}

			while (true)
			{
				var center = Math.floor((left + right) / 2);
				var centerItem = array[center];
				var eq = this.__sortFunc(item, centerItem);

				if (eq == 0)
				{
					return center;
				}

				if (eq > 0)
				{
					if (left == center)
					{
						return center;
					}
					left = center;
				}
				else
				{
					if (right == center)
					{
						return center - 1;
					}
					right = center;
				}
			}
		},

		push: function (varargs)
		{
			for (var i = 0, item; item = arguments[i++];)
			{
//				this.__processItemAdded(item);
				this.__rawPush(item);
			}
		},

		remove: function (item)
		{
//			this.__processItemDeleted(item);
			this.__rawRemove(item);
		},

		/**
		 * @private
		 */
//		__processItemAdded: function(item)
//		{
//			if (this.__updateOn)
//			{
//				this.__listenersStore.addListener(item, this.__updateOn)
//			}
//		},
//
		/**
		 * @private
		 */
		//__processItemDeleted: function(item)
//		{
//
//		},

		/**
		 * @private
		 */
		__rawPush: function (item)
		{
			this.insertAt(this.findLeft(item) + 1, item);
		},

		/**
		 * @private
		 */
		__rawRemove: function (item)
		{
			qx.data.Array.prototype.remove.call(this, item);
		},

		/**
		 * @private
		 */
		__setUpSynchronization: function ()
		{
			this.__listenersStore.addListener(this.__sourceArray, "change", function (e)
			{
				var data = e.getData();
				switch (data.type)
				{
					case "remove":
						data.items.forEach(function (x){ this.remove(x) }, this);
						break;
					case "add":
						data.items.forEach(function (x){ this.push(x) }, this);
						break;
				}
			}, this)
		}

	},

	destruct: function ()
	{
		this.__listenersStore.dispose();
	}
});
