qx.Class.define("library.data.Validation", {

	type: "static",

	statics: {

		FUNCTIONS_MAP: null,

		createValidationFunction: function (data, includeRequired, showAlert)
		{
			var self = this;
			if (includeRequired && Object.ifUndef(data.required, true))
			{
				return function (newValue, oldValue)
				{
					if (self.getRequiredFunction()(newValue))
					{
						return newValue;
					}
					else
					{
						if (showAlert)
						{
							dialog.Dialog.alert("Введите значение!");
						}

						return oldValue;
					}
				}
			}
			else
			{
				return Function.returnFirstArg;
			}
		},

		getRequiredFunction: function ()
		{
			return this._requiredFunction
				|| (this._requiredFunction = function (value){return !String.isNullUndefinedOrEmpty(value);});
		}

	},

	defer: function (statics)
	{
		statics.FUNCTIONS_MAP = {
			"required": statics.getRequiredFunction
		};
	}
});
