qx.Class.define("library.dev.Debug", {

	type: "static",

	statics: {
		__defAjaxParams: {
			method: "GET"
		},

		ajax: function (params)
		{
			params = qx.lang.Object.merge({}, this.__defAjaxParams, params);
			var request = new qx.io.request.Xhr(params.url);
			request.setMethod(params.method);
			if (params.data)
			{
				request.setRequestData(params.data);
			}

			request.send();
		}
	}
});
