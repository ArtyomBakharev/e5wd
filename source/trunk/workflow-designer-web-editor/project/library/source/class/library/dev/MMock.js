qx.Mixin.define("library.dev.MMock", {

	include: qx.dev.unit.MMock,

	members: {

		/**
		 * @param func
		 * @param expectedArgs
		 * @param [callCount=null]
		 */
		assertLastCall: function (func, expectedArgs, callCount)
		{
			if (callCount)
			{
				this.assertCallCount(func, callCount);
			}
			this.assertArrayEquals(expectedArgs, func.args.last());
		},

		/**
		 * @param expected
		 * @param found
		 * @param [msg=null]
		 */
		assertMapEquals: function (expected, found, msg)
		{
			msg = "maps don't equal " + (msg ? "; " + msg : msg);
			this.assertMap(found);
			this.assert(Object.mapsEqual(expected, found), msg);
		},

		assertArrayIncludesEqualElements: function (expected, found, msg)
		{
			msg = "arrays don't include equals elements " + (msg ? "; " + msg : msg);
			this.assertArray(found);
			this.assertIdentical(expected.length, found.length, msg);
			this.assertIdentical(0, expected.clone().exclude(found).length, msg);
		},

		assertArrayIncludesEqualMaps: function (expected, found, msg)
		{
			msg = "arrays don't include equals elements " + (msg ? "; " + msg : msg);

			this.assertArray(found);

			expected = expected.clone();
			this.assertIdentical(expected.length, found.length, msg);

			found.forEach(function (item)
			{
				for (var i = 0, expItem; expItem = expected[i]; ++i)
				{
					if (Object.mapsEqual(item, expItem))
					{
						expected.removeAt(i);
						break;
					}
				}
			});

			this.assertIdentical(0, expected.length, msg);
		},

		/**
		 * @param cls
		 * @param args
		 */
		constructWithStubs: function (cls, args)
		{
			return library.dev.Unit.constructWithStubs(this, cls, args);
		},

		/**
		 * @param cls
		 * @param [args=null]
		 * @param [methods=null]
		 */
		stubClass: function (cls, args, methods)
		{
			return library.dev.Unit.stubClass(this, cls, args, methods);
		},

		/**
		 *
		 * @param cls
		 * @param count
		 * @param [args=null]
		 * @param [methods=null]
		 */
		stubClassArray: function (cls, count, args, methods)
		{
			var arr = [];
			for (var i = 0; i < count; ++i)
			{
				arr.push(this.stubClass(cls, args, methods));
			}
			return arr;
		}
	}
});
