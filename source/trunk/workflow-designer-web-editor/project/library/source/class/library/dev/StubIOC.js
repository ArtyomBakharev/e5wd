/**
 * @extends {qx.core.Object}
 */
qx.Class.define("library.dev.StubIOC", {

	extend: qx.core.Object,

	implement: library.IServiceLocator,

	construct: function (mock, dependencies)
	{
		this.__mock = mock;
		this.__dependencies = dependencies.buildIndex(function (x){ return Object.getUniqueId(x[0]) });
		this.clear();
	},

	members: {

		constructService: function (cls, wrapClass)
		{
			var uid = Object.getUniqueId(cls);
			var deps = this.__dependencies[uid];
			if (!deps)
			{
				throw new Error("Dependencies for class " + cls.classname + " not found");
			}

			if (wrapClass)
			{
				cls = library.dev.Unit.wrapClass(cls);
			}

			var args = deps[1] && deps[1].args ? deps[1].args : [];

			return Object.construct(cls, args.map(function (x){ return this.getService(x) }, this));
		},

		getService: function (cls)
		{
			if (cls == library.IServiceLocator)
			{
				return this;
			}

			var uid = Object.getUniqueId(cls);
			var deps = this.__dependencies[uid];
			if (!deps)
			{
				throw new Error("Dependencies for class " + cls.classname + " not found");
			}

			if (cls == library.IServiceLocator)
			{
				return this;
			}

			var service;
			if (deps[1] && deps[1].scope == "transient")
			{
				var servicesPool = this.__transientServices[uid];
				if (!servicesPool || servicesPool.length == 0)
				{
					this.nextTransientService(cls);
				}

				service = this.__transientServices[uid].shift();
			}
			else
			{
				service = this.__servicesStubs[uid] || (this.__servicesStubs[uid] = library.dev.Unit.stubClass(this.__mock, cls));
			}

			if (this.__configurators[uid])
			{
				this.__configurators[uid](service);
			}

			return service;
		},

		nextTransientService: function (cls)
		{
			var uid = Object.getUniqueId(cls);
			!this.__transientServices[uid] && (this.__transientServices[uid] = []);

			var service = library.dev.Unit.stubClass(this.__mock, cls);
			this.__transientServices[uid].push(service);
			return service;
		},

		/**
		 * @param cls
		 * @param [instance=null]
		 */
		registerService: function (cls, instance)
		{
			var uid = Object.getUniqueId(cls);

			if (instance)
			{
				this.__servicesStubs[uid] = instance;
			}
			else
			{
				this.__dependencies[uid] = [cls];
			}
		},

		setServiceConfigurator: function (cls, configurator, context)
		{
			var uid = Object.getUniqueId(cls);
			this.__configurators[uid] = configurator.bind(context || window);
		},

		clear: function ()
		{
			this.__servicesStubs = {};
			this.__transientServices = {};
			this.__configurators = {};
		}

	}

});