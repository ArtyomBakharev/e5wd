/**
 * @extends {cls}
 */
qx.Class.define("library.dev.Unit", {

	type: "static",

	statics: {

		__wrappedClasses: {},
		__propertiesMaps: {},

		/**
		 * @param {Function} cls
		 */
		stubClass: function (mock, cls, args, methods)
		{
			if (!Array.isArray(args) && typeof args == "object")
			{
				methods = args;
				args = null;
			}

			var instance = Object.construct(this.wrapClass(cls, !args), args || []);

			var propertiesMap = qx.Class.isSubClassOf(cls, qx.core.Object)
				? this.__propertiesMaps[cls.classname]
				|| (this.__propertiesMaps[cls.classname] = qx.util.PropertyUtil.getAllProperties(cls))
				: {};

			for (var name in instance)
			{
				//noinspection JSUnfilteredForInLoop
				var method = instance[name];

				//noinspection JSUnfilteredForInLoop
				if (["toString", "constructor"].indexOf(name) !== -1
					|| qx.core.MEvents.$$members[name]
					|| !qx.lang.Type.isFunction(method) || (methods && methods[name])
					|| name.startsWith("_")
					|| ((name.startsWith("get") || name.startsWith("set")) && propertiesMap[name.substr(3).firstLow()]))
				{
					continue;
				}

				//noinspection JSUnfilteredForInLoop
				instance[name] = mock.stub();
			}

			if (methods)
			{
				Object.merge(instance, methods);
			}

			return instance;
		},

		constructWithStubs: function (mock, cls, args)
		{
			Object.construct(cls, args.map(function (x){ return this.stubClass(mock, x) }, this));
		},

		wrapClass: function (cls, suppressConstructor)
		{
			var key = cls.classname + (suppressConstructor ? "_sup" : "");
			if (!this.__wrappedClasses[key])
			{
				var members = {extend: cls};
				if (suppressConstructor)
				{
					members.construct = function (){};
				}

				this.__wrappedClasses[key] = qx.Class.define("tst", members);
			}

			return this.__wrappedClasses[key];
		}
	}
});
