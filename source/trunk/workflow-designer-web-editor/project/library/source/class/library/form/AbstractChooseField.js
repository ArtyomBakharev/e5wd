/*
 #asset(qx/icon/${qx.icontheme}/22/actions/edit-find.png)
 #asset(qx/icon/${qx.icontheme}/22/actions/edit-delete.png)
 * @extends {qx.ui.core.Widget}
 */
qx.Class.define("library.form.AbstractChooseField", {

	type: "abstract",
	extend: qx.ui.core.Widget,

	implement: [qx.ui.form.IForm, qx.ui.form.IStringForm],
	include: qx.ui.form.MForm,

	properties: {
		value: {
			init: null,
			nullable: true,
			event: "changeValue"
		},
		focusable: {
			refine: true,
			init: true
		},
		readOnly: {
			init: false,
			check: "Boolean",
			event: "changeReadOnly"
		}
	},

	construct: function ()
	{
		this.base(arguments);

		this._setLayout(new qx.ui.layout.HBox(3));

		this._createChildControl("textField");
		this._createChildControl("chooseButton");
		this._createChildControl("clearButton");
	},

	statics: {
		_BUTTON_PADDING: 2
	},

	members: {

		flush: function()
		{
			this._closeChooser();
		},

		/**
		 * @protected
		 */
		_chooserClosed: function()
		{
			this.__chooseButton.setEnabled(true);
		},

		/**
		 * @protected
		 */
		_closeChooser: function ()
		{
			this.abstractMethod(arguments);
		},

		/**
		 * @protected
		 */
		_createChildControlImpl: function (id)
		{
			var control;
			switch (id)
			{
				case "textField":
					control = this.__createTextField();
					this._add(control, {flex: 1});
					break;

				case "chooseButton":
					control = this.__createChooseButton();
					this._add(control);
					break;

				case "clearButton":
					control = this.__createClearButton();
					this._add(control);
					break;
			}

			return control || this.base(arguments, id);
		},

		/**
		 * @protected
		 */
		_openChooser: function ()
		{
			this.abstractMethod(arguments);
		},

		/**
		 * @protected
		 */
		_valueToString: function(value)
		{
			return value.toString();
		},

		/**
		 * @private
		 */
		__createTextField: function ()
		{
			this.__textField = new qx.ui.form.TextField();
			this.__textField.setReadOnly(true);
			this.bind("value", this.__textField, "value",
				{converter: (function (x){ return x === null ? "" : this._valueToString(x) }).bind(this)});

			return this.__textField;
		},

		/**
		 * @private
		 */
		__createChooseButton: function ()
		{
			this.__chooseButton = new qx.ui.form.Button(null, "icon/22/actions/edit-find.png");
			this.__chooseButton.setPadding(this.self(arguments)._BUTTON_PADDING);

			this.bind("readOnly", this.__chooseButton, "enabled", {converter: function (x){ return !x }});

			this.__chooseButton.addListener("execute", function ()
			{
				this.__chooseButton.setEnabled(false);
				this._openChooser();
			}, this);

			return this.__chooseButton;
		},

		/**
		 * @private
		 */
		__createClearButton: function ()
		{
			this.__clearButton = new qx.ui.form.Button(null, "icon/22/actions/edit-delete.png");
			this.__clearButton.setPadding(this.self(arguments)._BUTTON_PADDING);
			this.bind("required", this.__clearButton, "visibility",
				{converter: function(x){ return x ? "excluded" : "visible" }});

			library.data.Helper.multiBind(
				this, "value",
				this, "readOnly",
				this.__clearButton, "enabled",
				function (value, readOnly){ return readOnly ? false : value !== null });

			this.__clearButton.addListener("execute", function ()
			{
				this.setValue(null);
			}, this);

			return this.__clearButton;
		}

	}
});
