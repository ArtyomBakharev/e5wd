/*
 #asset(qx/icon/${qx.icontheme}/22/actions/dialog-apply.png)
 #asset(qx/icon/${qx.icontheme}/22/actions/dialog-close.png)
 * @extends {qx.ui.window.Window}
 */
qx.Class.define("library.form.AbstractForm", {

	extend: qx.ui.window.Window,

	type: "abstract",

	events: {
		submit: "qx.event.type.Data"
	},

	construct: function (title, icon)
	{
		this.base(arguments, title, icon);
		this._closable = true;
	},

	members: {

		/**
		 * @return Deferred
		 */
		block: function ()
		{
//			library.ui.widget.WaitDialog.wait(this._waitText);
			return Deferred.success();
		},

		destroy: function ()
		{
			this.close();
			this.base(arguments);
		},

		render: function ()
		{
			this._setUpWindow();
			qx.core.Init.getApplication().getRoot().add(this);
			library.ui.Helper.openAndCenterWindow(this);
		},

		/**
		 * @param {string} error
		 * @return {Deferred}
		 */
		unblock: function (error)
		{
			if (error)
			{
				this._label.set({
					visibility: "visible",
					value: error
				});
			}
			else
			{
				this._label.setVisibility("excluded");
			}

//			library.ui.widget.WaitDialog.stopWaiting();

			return Deferred.success();
		},

		/**
		 * @protected
		 */
		_setUpWindow: function ()
		{
			if (this.__settedUp)
			{
				return;
			}

			this.set({
				modal: true,
				resizable: false,
				allowMaximize: false,
				allowMinimize: false,
				allowClose: this._closable,
				showMaximize: false,
				showMinimize: false,
				showClose: this._closable,
				layout: new qx.ui.layout.VBox(10)
			});

			var buttons = new qx.ui.container.Composite(new qx.ui.layout.HBox(10).set({alignX: "right"}));

			if (this._closable)
			{
				var closeButton = new qx.ui.form.Button("Отмена", "icon/22/actions/dialog-close.png");
				closeButton.addListener("execute", this.destroy, this);
				buttons.add(closeButton);
			}

			var submitButton = new qx.ui.form.Button("Ok", "icon/22/actions/dialog-apply.png");
			submitButton.addListener("execute", function ()
			{
				this._validate().next(function ()
				{
					this.fireEvent("submit", qx.event.type.Data, [this._getData()]);
				}, this);
			}, this);
			buttons.add(submitButton);

			this.add(this._label = new qx.ui.basic.Label().set({visibility: "excluded"}));
			this._initBody();
			this.add(buttons);

			this.__settedUp = true;
		},

		/**
		 * @protected
		 */
		_initBody: function (){throw new Error("abstract");},
		/**
		 * @protected
		 */
		_getData: function (){throw new Error("abstract");},
		/**
		 * @protected
		 */
		_validate: function (){ return Deferred.success(); }
	}
});
