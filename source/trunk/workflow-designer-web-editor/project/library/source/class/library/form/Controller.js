/**
 * @extends {qx.core.Object}
 */
qx.Class.define("library.form.Controller", {

	extend: qx.core.Object,

	events: {
		/**
		 * data = {values: Object, deferred: Deferred}
		 */
		"submit": "qx.event.type.Data"
	},

	/**
	 * @param {AbstractForm} form
	 */
	construct: function (form)
	{
		this._form = form;
	},

	members: {

		renderForm: function ()
		{
			var deferred = Deferred();
			var form = this._form;

			form.addListener("submit", function (e)
			{
				var result;

				form.block().next(
					function ()
					{

						var deferred;
						this.addListenerOnce("submit", function (e){deferred = e.getData().deferred});
						this.fireEvent("submit", qx.event.type.Data, [
							{values: e.getData(), deferred: null}
						]);
						return deferred.hand(
							function (){ return form.unblock(); },
							function (message){ return form.unblock(message).nextFailure(); }
						);

					}, this).next(function ()
					{

						form.destroy();
						deferred.call(true);

					}, this);
			}, this);

			form.addListener("close", function (){ deferred.call(false); });
			form.render();

			return deferred;
		}

	}
});
