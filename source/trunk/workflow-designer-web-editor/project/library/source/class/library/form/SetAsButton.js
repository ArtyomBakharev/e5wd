qx.Class.define("library.form.SetAsButton", {

	extend: qx.ui.core.Widget,

	implement: [qx.ui.form.IForm, qx.ui.form.IBooleanForm],
	include: qx.ui.form.MForm,

	properties: {
		value: {
			init: false,
			check: "Boolean",
			event: "changeValue"
		},
		focusable: {
			refine: true,
			init: true
		},
		readOnly: {
			init: false,
			check: "Boolean",
			event: "changeReadOnly"
		}
	},

	/**
	 * @param {String} falseLabel
	 * @param {String} trueLabel
	 */
	construct: function (falseLabel, trueLabel)
	{
		this.base(arguments);
		this._setLayout(new qx.ui.layout.Grow());

		this.__falseLabel = falseLabel;
		this.__trueLabel = trueLabel;

		this._createChildControl("button");
	},

	members: {

		/**
		 * @protected
		 */
		_createChildControlImpl: function (id)
		{
			var control;
			switch (id)
			{
				case "button":
					control = this.__createButton();
					this._add(control);
					break;
			}

			return control || this.base(arguments, id);
		},

		__createButton: function()
		{
			var $this = this;

			this.__button = new qx.ui.form.Button();
			this.bind("value", this.__button, "label",
				{converter: function(x){ return x ? $this.__trueLabel : $this.__falseLabel }});

			library.data.Helper.multiBind(
				this, "readOnly",
				this, "value",
				this.__button, "enabled",
				function(readOnly, value){
					return !readOnly && !value;
				});

			this.__button.addListener("execute", function(){ this.setValue(true) }, this);

			return this.__button;
		}

	}
});
