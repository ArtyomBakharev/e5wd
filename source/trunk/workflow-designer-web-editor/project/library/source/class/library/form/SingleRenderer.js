/**
 * @extends {qx.ui.form.renderer.Single}
 */
qx.Class.define("library.form.SingleRenderer", {

	extend: qx.ui.form.renderer.Single,

	statics: {
		renderInOneCell: function (widget)
		{
			widget.setUserData(this.__RENDER_IN_ONE_CELL_KEY, true);
		},

		renderInOneColumn: function (widget)
		{
			widget.setUserData(this.__RENDER_IN_ONE_COLUMN_KEY, true);
		},

		__RENDER_IN_ONE_CELL_KEY: "renderInOneCell",
		__RENDER_IN_ONE_COLUMN_KEY: "renderInOneColumn"
	},

	members: {

		addItems: function (items, names, title)
		{

			var renderInOneCellKey = this.self(arguments).__RENDER_IN_ONE_CELL_KEY;
			var renderInOneColumnKey = this.self(arguments).__RENDER_IN_ONE_COLUMN_KEY;

			// add the header
			if (title != null)
			{
				this._add(
					this._createHeader(title), {row: this._row, column: 0, colSpan: 2}
				);
				this._row++;
			}

			// add the items
			for (var i = 0; i < items.length; i++)
			{
				var item = items[i];
				var label = null;
				if (!item.getUserData(renderInOneCellKey))
				{
					label = this._createLabel(names[i], item);
					this._add(label, {row: this._row, column: 0});
					label.setBuddy(item);
				}

				if (item.getUserData(renderInOneColumnKey))
				{
					this._row++;
					this._add(item, {row: this._row, column: 0, colSpan: 2});
				}
				else if (label)
				{
					this._add(item, {row: this._row, column: 1});
				}
				else
				{
					this._add(item, {row: this._row, column: 0, colSpan: 2});
				}
				this._row++;

				if (label)
				{
					this._connectVisibility(item, label);
					if (qx.core.Environment.get("qx.dynlocale"))
					{
						this._names.push({name: names[i], label: label, item: item});
					}
				}
			}
		}

	}
});
