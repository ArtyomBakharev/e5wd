/*
 #asset(qx/icon/${qx.icontheme}/22/actions/list-add.png)
 #asset(qx/icon/${qx.icontheme}/22/actions/list-remove.png)
 * @extends {qx.ui.core.Widget}
 */
qx.Class.define("library.form.TableEditor", {

	extend: qx.ui.core.Widget,

	//todo: remove qx.ui.form.IStringForm interface!!
	implement: [qx.ui.form.IForm, qx.ui.form.IStringForm],
	include: qx.ui.form.MForm,

	properties: {
		value: {
			init: null,
			nullable: true,
			transform: "__transformValue",
			apply: "__applyValue",
			event: "changeValue"
		},
		focusable: {
			refine: true,
			init: true
		},
		readOnly: {
			init: false,
			check: "Boolean",
			event: "changeReadOnly"
		}
	},

	/**
	 * @param names
	 * @param titles
	 */
	construct: function (names, titles)
	{
		this.__names = names;
		this.__titles = titles || names;

		this.__applyingValueHandling = true;

		this.base(arguments);

		this._setLayout(new qx.ui.layout.VBox());

		this._createChildControl("header");
		this._createChildControl("table");

		this.__setUpChildrenBinding();
	},

	members: {

		flush: function ()
		{
			this.__table.stopEditing();
		},

		/**
		 * @protected
		 */
		_createChildControlImpl: function (id)
		{
			var control;
			switch (id)
			{
				case "header":
					control = this.__createHeader();
					break;

				case "table":
					control = this.__createTable();
					break;
			}

			if (control)
			{
				this._add(control);
			}

			return control || this.base(arguments, id);
		},

		/**
		 * @param name
		 * @return {qx.ui.table.ICellEditorFactory}
		 * @protected
		 */
		_getCellEditor: function (name)
		{
			return null;
		},

		/**
		 * @param name
		 * @return {qx.ui.table.ICellRenderer}
		 * @protected
		 */
		_getCellRenderer: function (name)
		{
			return null;
		},

		/**
		 * @param {String} name
		 * @return {*}
		 * @protected
		 */
		_getDefaultValue: function (name)
		{
			return null;
		},

		/**
		 * @return {qx.ui.table.Table}
		 * @protected
		 */
		_getTable: function ()
		{
			return this.__table;
		},

		/**
		 * @return {qx.ui.table.model.Abstract}
		 * @protected
		 */
		_getTableModel: function ()
		{
			return this.__tableModel;
		},

		/**
		 * @param name
		 * @return {Boolean}
		 * @protected
		 */
		_isColumnEditable: function (name)
		{
			return !this.isReadOnly();
		},

		_normalizeValue: function (name, value)
		{
			return value === undefined || value === null ? "" : value.toString();
		},

		/**
		 * @param {qx.ui.table.Table} table
		 * @protected
		 */
		_setUpTable: function (table)
		{
			this.__table.set({
				statusBarVisible: false,
				columnVisibilityButtonVisible: false,
				height: 170
			});

			var tableColumnModel = this.__table.getTableColumnModel();
			this.__names.forEach(function (name, index)
			{
				var editor = this._getCellEditor(name);
				var renderer = this._getCellRenderer(name);

				if (editor)
				{
					tableColumnModel.setCellEditorFactory(index, editor);
				}

				if (renderer)
				{
					tableColumnModel.setDataCellRenderer(index, renderer);
				}
			}, this);
		},

		/**
		 * @param name
		 * @param value
		 * @return {*}
		 * @protected
		 */
		_transformToActualValue: function (name, value)
		{
			return value;
		},

		/**
		 * @param name
		 * @param value
		 * @return {*}
		 * @protected
		 */
		_transformToTableValue: function (name, value)
		{
			return value;
		},

		/**
		 * @private
		 */
		__addRow: function ()
		{
			this.__tableModel.addRows([
				this.__names.map(function (x)
				{
					return this._transformToTableValue(x, this._getDefaultValue(x))
				}, this)
			]);
		},

		/**
		 * @private
		 */
		__applyValue: function (value)
		{
			if (!this.__applyingValueHandling)
			{
				return;
			}

			this.__tableModel.setDataAsMapArray(this.__transformToTableValue(value));
		},

		/**
		 * @private
		 */
		__bindColumnsEditable: function ()
		{
			function changeReadOnly()
			{
				this.__names.forEach(function (name, index)
				{
					this.__tableModel.setColumnEditable(index, this._isColumnEditable(name))
				}, this);
			}

			this.addListener("changeReadOnly", changeReadOnly, this);
			changeReadOnly.call(this);
		},

		/**
		 * @private
		 */
		__createHeader: function ()
		{
			var header = new qx.ui.container.Composite(new qx.ui.layout.HBox(5, "right"));
			header.setPadding(5);

			var addButton = new qx.ui.form.Button(null, "icon/22/actions/list-add.png");
			addButton.addListener("execute", function ()
			{
				this.__addRow();
				this.focus();
			}, this);
			header.add(addButton);

			var deleteButton = this.__deleteButton = new qx.ui.form.Button(null, "icon/22/actions/list-remove.png");
			deleteButton.addListener("execute", function ()
			{
				this.__deleteRow();
				this.focus();
			}, this);
			header.add(deleteButton);

			this.bind("readOnly", header, "visibility", {converter: function (readOnly)
			{
				return readOnly ? "excluded" : "visible"
			}});

			return header;
		},

		/**
		 * @private
		 */
		__createTable: function ()
		{
			this.__tableModel = new qx.ui.table.model.Simple();
			this.__tableModel.setColumns(this.__titles, this.__names);
			this.__names.forEach(function (name, index){ this.__tableModel.setColumnSortable(index, false) }, this);

			this.__bindColumnsEditable();
			this.__tableModel.addListener("dataChanged", function (){ this.__updateValue() }, this);

			var custom = {
				tableColumnModel: function (obj)
				{
					return new qx.ui.table.columnmodel.Resize(obj);
				}
			};

			this.__table = new qx.ui.table.Table(this.__tableModel, custom);
			this._setUpTable(this.__table);

			return this.__table;
		},

		/**
		 * @private
		 */
		__deleteRow: function ()
		{
			this.__table.cancelEditing();
			var row = this.__table.getFocusedRow();
			this.__tableModel.removeRows(row, 1);
		},

		/**
		 * @private
		 */
		__setUpChildrenBinding: function ()
		{
			this.__deleteButton.setEnabled(false);
			function setDeleteButtonEnabled()
			{
				var enabled = !!((this.__table.getFocusedRow() === 0 || this.__table.getFocusedRow()) && this.__table.getTableModel().getRowCount() > 0);
				this.__deleteButton.setEnabled(enabled);
			}

			this.__table.getSelectionModel().addListener("changeSelection", setDeleteButtonEnabled, this);
			this.__table.getTableModel().addListener("dataChanged", setDeleteButtonEnabled, this);
		},

		/**
		 * @private
		 */
		__transformValue: function (value)
		{
			return value
				?
				value.map(function (tuple)
				{
					return this.__names.mapToObject(function (name)
					{
						var value = tuple.hasOwnProperty(name)
							? this._normalizeValue(name, tuple[name]) : this._getDefaultValue(name);
						return [name, value];
					}, this);
				}, this)
				: [];
		},

		__transformToActualValue: function (value)
		{
			return value.map(function (tuple)
			{
				return Object.mapObject(tuple, function (x, key)
				{
					return this._transformToActualValue(key, x);
				}, this);
			}, this);
		},

		__transformToTableValue: function (value)
		{
			return value.map(function (tuple)
			{
				return Object.mapObject(tuple, function (x, key)
				{
					return this._transformToTableValue(key, x);
				}, this);
			}, this);
		},

		/**
		 * @private
		 */
		__updateValue: function ()
		{
			this.__applyingValueHandling = false;
			var value = this.__tableModel.getDataAsMapArray();
			this.setValue(this.__transformToActualValue(value));
			this.__applyingValueHandling = true;
		}

	}
});
