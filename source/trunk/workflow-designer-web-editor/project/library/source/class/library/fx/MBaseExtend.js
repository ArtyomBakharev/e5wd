qx.Mixin.define("library.fx.MBaseExtend", {
	members: {

		getChain: function()
		{
			return this.chain.bind(this);
		},

		chain: function()
		{
			var deferred = new library.Deferred();
			this.start();
			this.addListenerOnce("finish", function(){ deferred.call() });

			return deferred;
		}

	}
});
