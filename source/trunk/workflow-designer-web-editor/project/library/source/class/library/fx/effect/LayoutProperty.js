/**
 * @extends {qx.fx.Base}
 */
qx.Class.define("library.fx.effect.LayoutProperty", {
	
	extend: qx.fx.Base,

	/*
	 *****************************************************************************
	 CONSTRUCTOR
	 *****************************************************************************
	 * @param element {Object} The DOM element
	 * @param property {String} Name of the qooxdoo property to animate.
	 * @param func {Function} Function which modifies the numeric value given by
	 * update().
	 */
	construct: function (element, property, func)
	{
		if (property)
		{
			if (qx.core.Environment.get("qx.debug"))
			{
				this.assertString(property);
			}
			this.__property = property;
		}
		else
		{
			throw new Error("A layout property must be specified!");
		}

		if (func)
		{
			if (qx.core.Environment.get("qx.debug"))
			{
				this.assertFunction(func);
			}
			this.__func = func;
		}

		this.base(arguments, element);
	},

	members: {
		__property: null,
		__func: null,

		// overridden
		update: function (position)
		{
			this.base(arguments);
			var value = this.__func ? this.__func(position) : position;

			var properties = this._getElement().getLayoutProperties();
			properties[this.__property] = value;
			this._getElement().setLayoutProperties(properties);
		}
	}


});
