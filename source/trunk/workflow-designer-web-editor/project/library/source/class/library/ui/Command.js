/**
 * @extends {qx.ui.core.Command}
 */
qx.Class.define("library.ui.Command", {

	extend: qx.ui.core.Command,

	events: {
		"preExecute": "qx.event.type.Data",
		"executeWithData": "qx.event.type.Data"
	},

	construct: function (handler, context, shortcut)
	{
		this.base(arguments, shortcut);

		this.addListener("execute", function (){ this.fireDataEvent("executeWithData", this.getData()); }, this);

		if (handler)
		{
			this.addListener("executeWithData", function (e){ handler.call(context || window, e.getData()); });
		}
	},

	members: {

		getData: function (){ return this._addData; },
		setData: function (value){ this._addData = value; },

		/**
		 * @param [target=null]
		 * @param [data=null]
		 */
		execute: function (target, data)
		{
			if (!this.getEnabled())
			{
				return;
			}

			if (data !== undefined)
			{
				this.setData(data);
			}

			this.fireDataEvent("preExecute", {command: this, target: target});
			this.base(arguments, target);
		},

		attachToWidgetWithKey: function (widget, key)
		{
			widget.addListener("keypress", function (e){
				if (e.getKeyIdentifier() == key)
				{
					this.execute();
				}
			}, this);
		}

	}
});
