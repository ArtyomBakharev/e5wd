/*
 #asset(qx/icon/${qx.icontheme}/64/actions/document-open-recent.png)
 #asset(qx/icon/${qx.icontheme}/64/status/dialog-warning.png)
 */

/**
 * Коллекция различных диалоговых окон, типа alert и подобных
 */
qx.Class.define("library.ui.Dialog", {
	type: "static",

	statics: {

		KIND_OK: "ok",
		KIND_CANCEL: "cancel",

		/**
		 * Диалоговое окно, предупрежедение
		 *
		 * @param {String} [caption=""]
		 * @param {String} text
		 * @param {Function} [callback=null] функция вызывается после закрытия окна
		 */
		alert: function (caption, text, callback, context)
		{
			if (!qx.lang.Type.isString(text))
			{
				callback = text;
				text = caption;
				caption = "";
			}

			var window = this.__constructWindow(caption);
			this.__addBody(window, text, "icon/64/status/dialog-warning.png");
			this.__addButtons(window, [
				{label: "Ok", kind: this.KIND_OK}
			], callback, context);
			library.ui.Helper.openAndCenterWindow(window);
		},

		/**
		 * Диалоговое окно, предупрежедение
		 *
		 * @param {String} [caption=""]
		 * @param {String} text
		 * @param {Function} [callback=null] функция вызывается после закрытия окна
		 */
		confirm: function (caption, text, callback, context)
		{
			if (!qx.lang.Type.isString(text))
			{
				callback = text;
				text = caption;
				caption = "";
			}

			var window = this.__constructWindow(caption);
			this.__addBody(window, text);
			this.__addButtons(window, [
				{label: "Ok", kind: this.KIND_OK},
				{label: "Cancel", kind: this.KIND_CANCEL}
			], callback, context);
			library.ui.Helper.openAndCenterWindow(window);
		},

		/**
		 * Диалог с прогрессбаром
		 *
		 * @param [maximum=100]
		 * @param [initValue=0]
		 * @param {String} [caption=null]
		 * @param {String} [message=null]
		 * @return {Object} {destroy:Function, setValue: Function, window: qx.ui.window.Window, progressBar: qx.ui.indicator.ProgressBar}
		 */
		progressBar: function (maximum, initValue, caption, message)
		{
			var window = this.__constructWindow(caption || "");
			if (message)
			{
				this.__addBody(window, message);
			}

			var progressBar = new qx.ui.indicator.ProgressBar(initValue || 0, maximum || 100);
			progressBar.set({width: 300, height: 25});
			window.add(progressBar);
			library.ui.Helper.openAndCenterWindow(window);

			return {
				window: window,
				progressBar: progressBar,
				destroy: function (){ window.destroy() },
				setValue: function (val){ progressBar.setValue(val) }
			};
		},

		/**
		 * Даилог ожидания
		 *
		 * @param caption
		 * @param message
		 * @return {qx.ui.window.Window}
		 */
		wait: function (caption, message)
		{
			if (arguments.length <= 1)
			{
				message = caption || "Wait please...";
				caption = "";
			}

			var window = this.__constructWindow(caption);
			this.__addBody(window, message, "icon/64/actions/document-open-recent.png");
			library.ui.Helper.openAndCenterWindow(window);
			return window;
		},

		/**
		 * @private
		 */
		__addButtons: function (window, buttons, callback, context)
		{
			var buttonsPane = new qx.ui.container.Composite(new qx.ui.layout.HBox(10, "center"));
			buttons.forEach(function (button)
			{
				var buttonWidget = new qx.ui.form.Button(button.label);
				buttonWidget.setMinWidth(50);
				buttonWidget.addListener("execute", function ()
				{
					window.destroy();
					callback && callback.call(context || window, "result" in button ? button.result() : button.kind);
				});
				buttonsPane.add(buttonWidget);
			});

			window.add(buttonsPane);
		},

		/**
		 * @private
		 */
		__addBody: function (window, text, image)
		{
			var atom = new qx.ui.basic.Atom(text, image);
			atom.setPadding(10);
			window.add(atom);
		},

		/**
		 * @private
		 */
		__constructWindow: function (caption)
		{
			var window = new qx.ui.window.Window(caption || "");
			window.set({
				modal: true,
				resizable: false,
				allowMaximize: false,
				allowMinimize: false,
				allowClose: false,
				showMaximize: false,
				showMinimize: false,
				showClose: false,
				layout: new qx.ui.layout.VBox()
			});

			return window;
		}
	}
});
