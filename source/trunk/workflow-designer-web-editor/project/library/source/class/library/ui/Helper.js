qx.Class.define("library.ui.Helper", {

	type: "static",

	statics: {

		blockUI: function (text)
		{
			if (!this._window)
			{
				this._window = new qx.ui.window.Window("Подождите...");
				this._window.set({
					modal: true,
					resizable: false,
					allowMaximize: false,
					allowMinimize: false,
					allowClose: false,
					layout: new qx.ui.layout.VBox(10),
					width: 200
				});
				this._window.add(new qx.ui.basic.Label().set({value: text || ""}));
			}

			this.openAndCenterWindow(this._window);
		},

		openAndCenterWindow: function (win, addToRoot)
		{
			if (addToRoot)
			{
				qx.core.Init.getApplication().getRoot().add(win);
			}
			win.setOpacity(0);
			win.open();
			win.addListener("appear", function ()
			{
				win.center();
				win.setOpacity(1);
			});
		},

		unblockUI: function ()
		{
			this._window.close();
		}
	}
});
