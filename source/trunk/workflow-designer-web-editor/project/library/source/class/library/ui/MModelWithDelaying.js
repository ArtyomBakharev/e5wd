qx.Mixin.define("library.ui.MModelWithDelaying", {

	events: {
		delayStart: "qx.event.type.Data",
		delayStop: "qx.event.type.Event"
	},

	members: {
		delayStart: function (message){ this.fireDataEvent("delayStart", {message: message}); },
		delayStop: function (){ this.fireEvent("delayStop"); }
	}
});
