qx.Mixin.define("library.ui.MRepresentorWithDelaying", {

	members: {
		connectModelWithDelaying: function (model)
		{
			model.addListener("delayStart", function (e){ this.delayStart(e.getData().message); }, this);
			model.addListener("delayStop", this.delayStop, this);
		},
		delayStart: function (message)
		{
			library.ui.WaitDialog.wait(message);
		},
		delayStop: function ()
		{
			library.ui.WaitDialog.stopWaiting();
		}
	}
});
