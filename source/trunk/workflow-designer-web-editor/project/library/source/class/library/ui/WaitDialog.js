/**
 * Хелпер для создания диалога ожидания. Повторное открытие такого диалога приведёт только к смене его текста.
 */
qx.Class.define("library.ui.WaitDialog", {

	type: "static",

	statics: {

		_stopWaitingTimes: 0,
		_waitingHandler: null,
		DELAY_TIME: 400,

		/**
		 * Закрытие диалога ожидания, если он открыт
		 *
		 * @param {Boolean} [destroyOnly=false] internal!
		 */
		stopWaiting: function (destroyOnly)
		{
			if (!destroyOnly)
			{
				if (++this._stopWaitingTimes < 0)
				{
					return;
				}
			}

			if (this._dialog)
			{
				if (destroyOnly)
				{
					this._hideDialog();
				}
				else
				{
					this._stopWaitingHandler = setTimeout(this._hideDialog.bind(this), 100);
				}
			}

			if (this._waitingHandler)
			{
				clearTimeout(this._waitingHandler);
				this._waitingHandler = null;
			}
		},

		/**
		 * Выполнение операции только после открытия диалога ожидания
		 *
		 * @param {Function} operation
		 * @param {*} context
		 * @param {String} message
		 */
		syncWait: function (operation, context, message)
		{
			this.wait(message, 0);
			operation.timeout(100, context);
		},

		/**
		 * Открытие диалога ожидания. Открывается через delaytime миллисекунд после вызова метода во избежание мелькания
		 *
		 * @param {String} [message=null]
		 * @param {Number} [delaytime=library.ui.WaitDialog.DELAY_TIME]
		 */
		wait: function (message, delaytime)
		{
			var root = qx.core.Init.getApplication().getRoot();
			if (!root.getBounds())
			{
				this.wait.defer(this, message);
				return;
			}

			if (--this._stopWaitingTimes >= 0)
			{
				return;
			}

			var dialogShown = this._dialog;
			this.stopWaiting(true);

			if (!dialogShown)
			{
				this._waitingHandler = setTimeout(this._showDialog.bind(this, message), Object.ifUndef(delaytime, this.DELAY_TIME));
			}
			else
			{
				this._showDialog(message);
			}
		},

		/**
		 * @protected
		 */
		_showDialog: function (message)
		{
			if (this._stopWaitingHandler)
			{
				clearTimeout(this._stopWaitingHandler);
				this._stopWaitingHandler = null;
			}
			this._waitingHandler = null;
			this._dialog = library.ui.Dialog.wait(message);
		},

		/**
		 * @protected
		 */
		_hideDialog: function ()
		{
			if (this._stopWaitingHandler)
			{
				clearTimeout(this._stopWaitingHandler);
				this._stopWaitingHandler = null;
			}
			this._dialog.destroy();
			this._dialog = null;
		}
	}
});
