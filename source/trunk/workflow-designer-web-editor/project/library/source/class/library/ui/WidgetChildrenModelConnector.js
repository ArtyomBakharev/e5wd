/**
 * @extends {qx.core.Object}
 */
qx.Class.define("library.ui.WidgetChildrenModelConnector", {

	extend: qx.core.Object,

	properties: {
		model: {
			init: null,
			check: "qx.data.Array",
			apply: "__applyModel"
		}
	},

	/**
	 * @param {qx.ui.core.MChildrenHandling|qx.ui.core.MRemoteChildrenHandling} widget
	 * @param {Function} createItem
	 * @param {qx.data.Array} [array=null]
	 */
	construct: function (widget, createItem, array)
	{
		this.__widget = widget;
		this.__createItem = createItem;
		if (array)
		{
			this.setModel(array);
		}
	},

	members: {
		/**
		 * @private
		 */
		__applyModel: function (model, old)
		{
			if (old)
			{
				old.removeListenerById(this.__modelChangeListenerId);
			}

			this.__modelChangeListenerId = model.addListener("change", function (e)
			{
				var data = e.getData();

				switch (data.type)
				{
					case "add":
						var start = data.start;
						data.items.forEach(function (item)
						{
							var itemWidget = this.__createItem(item);
							this.__widget.addAt(itemWidget, start++);
						}, this);
						break;

					case "remove":
						for (var i = 0; i < data.items.length; ++i)
						{
							this.__widget.removeAt(data.start);
						}
						break;

					case "order":
						throw new Error("Not imlemented case!");
				}

			}, this);
		}
	}
});
