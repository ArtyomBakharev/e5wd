qx.Mixin.define("library.ui.extend.MAbstractCellRendererExtends", {

	members: {

		setDynamicCellStyle: function (func)
		{
			this._dynamicCellStyle = func;
		},

		getDynamicCellStyle: function (func)
		{
			return this._dynamicCellStyle;
		},

		/**
		 * @protected
		 */
		_getCellStyle: function (cellInfo)
		{
			var defStyle = this.base(arguments, cellInfo);
			if (this._dynamicCellStyle && cellInfo.rowData)
			{
				return this._dynamicCellStyle(cellInfo, defStyle);
			}
			return defStyle;
		}

	}

});
