qx.Mixin.define("library.ui.extend.MExecutableExtends", {

	properties: {
		iconSize: {
			nullable: true,
			apply: "_applyIconSize"
		}
//		command :
//		{
//			check : "qx.ui.core.Command",
//			apply : "_applyCommandExtended",
//			event : "changeCommand",
//			nullable : true
//		}
	},

	members: {

		__newExecutableBindingIds: null,

//		/**
//		 * @protected
//		 */
//		_applyIcon : function(value, old)
//		{
//			var iconSize = this.getIconSize();
//			if (iconSize && value.indexOf("{size}") !== -1)
//			{
//				this.setIcon(value.split("{size}").join(iconSize));
//				return;
//			}
//
//			var icon = this.getChildControl("icon", true);
//			if (icon) {
//				icon.setSource(value);
//			}
//
//			this._handleIcon();
//		},

		// property apply
		/**
		 * @protected
		 */
		_applyCommand: function (value, old)
		{

			var bindableProperties = this._bindableProperties;
			this._bindableProperties = [];
			this._applyCommandOld(value, old);
			this._bindableProperties = bindableProperties;

			var ids = this.__newExecutableBindingIds;
			if (ids == null)
			{
				this.__newExecutableBindingIds = ids = {};
			}

			var selfPropertyValue;
			for (var i = 0; i < this._bindableProperties.length; i++)
			{
				var property = this._bindableProperties[i];

				// remove the old binding
				if (old != null && !old.isDisposed() && ids[property] != null)
				{
					old.removeBinding(ids[property]);
					ids[property] = null;
				}

				// add the new binding
				if (value != null && qx.Class.hasProperty(this.constructor, property))
				{
					// handle the init value (dont sync the initial null)
					var cmdPropertyValue = value.get(property);
					if (cmdPropertyValue == null)
					{
						selfPropertyValue = this.get(property);
					}
					else
					{
						// Reset the self property value [BUG #4534]
						selfPropertyValue = null;
					}
					// set up the binding
					ids[property] = value.bind(property, this, property, property == "icon" ? {converter: this.__convertIconValue.bind(this)} : {});
					// reapply the former value
					if (selfPropertyValue)
					{
						this.set(property, property == "icon" ? this.__convertIconValue(selfPropertyValue) : selfPropertyValue);
					}
				}
			}
		},

		_applyCommandOld: qx.ui.form.Button.prototype._applyCommand,

		/**
		 * @private
		 */
		__convertIconValue: function (icon)
		{
			var iconSize = this.getIconSize();
			return icon && icon.replace(/\{size(?::(.*?))?\}/g, function (val, size)
			{
				return iconSize === null ? size : iconSize;
			});
		},

		/**
		 * @protected
		 */
		_applyIconSize: function (value, old)
		{
//			var icon = this.getIcon();
//			if (icon)
//				this._applyIcon(icon, icon);
		},

		destruct: function ()
		{
			this._applyCommand(null, this.getCommand());
			this.__newExecutableBindingIds = null;
		}
	}
});
