qx.Mixin.define("library.ui.extend.MListExtends", {

	statics: {
		__oldOnModelChange: qx.ui.list.List.prototype._onModelChange
	},

	members: {
		/**
		 * scroll bugfix
		 * @param e
		 * @protected
		 */
		_onModelChange: function (e)
		{
			library.ui.extend.MListExtends.__oldOnModelChange.call(this, e);
			if (e && e.getData().type == "remove")
			{
				setTimeout((function ()
				{
					var scroll = this.getScrollY();
					this.scrollToY(scroll - 1);
					this.scrollToY(scroll);
				}).bind(this), 0);
			}
		}
	}
});