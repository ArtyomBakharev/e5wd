qx.Class.define("library.ui.extend.menuWidget.Extend", {

	type: "static",

	statics: {
		extendMenuWidget: function ()
		{
			this.oldComputeSizeHint = qx.ui.menu.Layout.prototype._computeSizeHint;
			qx.Class.patch(qx.ui.menu.Layout, library.ui.extend.menuWidget.MMenuLayout);
			qx.Class.patch(qx.ui.core.Widget, library.ui.extend.menuWidget.MMenuWidget);
		}
	}
});
