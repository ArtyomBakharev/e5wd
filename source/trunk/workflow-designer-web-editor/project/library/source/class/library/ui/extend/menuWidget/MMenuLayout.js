qx.Mixin.define("library.ui.extend.menuWidget.MMenuLayout", {

	members: {
		/**
		 * @protected
		 */
		_computeSizeHint: function ()
		{
			var sizeHint = library.ui.extend.menuWidget.Extend.oldComputeSizeHint.call(this);

			this._getLayoutChildren().forEach(function (item)
			{
				if (!(item instanceof qx.ui.menu.AbstractButton))
				{
					sizeHint.width = Math.max(sizeHint.width, item.getMarginLeft() + item.getSizeHint().width + item.getMarginRight());
				}
			});

			return sizeHint;
		}
	}

});
