/**
 * @extends {qx.core.Object}
 */
qx.Class.define("library.ui.management.KeysManager", {

	type: "singleton",

	extend: qx.core.Object,

	construct: function()
	{
		this.__root = qx.core.Init.getApplication().getRoot();
		this.__pressedKeys = {};
		this.__pressedKeysCodes = {};

		this.__startMonitoring();
	},

	members: {

		/**
		 * @param {Number} code
		 * @return {Boolean}
		 */
		isKeyPressedByCode: function(code)
		{
			return !!this.__pressedKeysCodes[code];
		},

		/**
		 * @param {String} identifier
		 * @return {Boolean}
		 */
		isKeyPressedByIdentifier: function(identifier)
		{
			return !!this.__pressedKeys[identifier];
		},

		__startMonitoring: function()
		{
			this.__root.addListener("keydown", function(e){
				this.__pressedKeys[e.getKeyIdentifier()] = true;
				this.__pressedKeysCodes[e.getKeyCode()] = true;
			}, this);

			this.__root.addListener("keyup", function(e){
				this.__pressedKeys[e.getKeyIdentifier()] = false;
				this.__pressedKeysCodes[e.getKeyCode()] = false;
			}, this);
		}
	}
});
