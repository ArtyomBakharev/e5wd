/**
 * @extends {qx.core.Object}
 */
qx.Class.define("library.ui.management.ShortcutsManager", {

	type: "singleton",

	extend: qx.core.Object,

	members: {

		/**
		 * @param {Object} shortcuts
		 */
		setShortcuts: function (shortcuts)
		{
			this.__shortcuts = shortcuts;
		},

		/**
		 * @param {String} command
		 * @param {Function|qx.ui.core.Command} funcOrCommand
		 * @param [context=window]
		 * @param {library.ui.management.Zone} [zone=null]
		 */
		registerCommand: function (command, funcOrCommand, context, zone)
		{
			if (this.__shortcuts && this.__shortcuts[command])
			{
				var shortcuts = this.__shortcuts[command];
				if (!Array.isArray(shortcuts))
				{
					shortcuts = [shortcuts];
				}

				shortcuts.forEach(function (shortcut)
				{
					this.registerShortcut(shortcut, funcOrCommand, context, zone);
				}, this);
			}
		},

		/**
		 * @param {String} shortcut
		 * @param {Function|qx.ui.core.Command} funcOrCommand
		 * @param [context=window]
		 * @param {library.ui.management.Zone} [zone=null]
		 */
		registerShortcut: function (shortcut, funcOrCommand, context, zone)
		{
//			var shortcutObj = new qx.bom.Shortcut(shortcut);
//
//			shortcutObj.addListener("execute", function ()
//			{
//				funcOrCommand instanceof qx.ui.core.Command
//					? funcOrCommand.execute()
//					: funcOrCommand.call(context || window);
//			}, this);
//
//			if (zone)
//			{
//				zone.bind("focused", shortcutObj, "enabled");
//			}

			function execute()
			{
				funcOrCommand instanceof qx.ui.core.Command
					? funcOrCommand.execute()
					: funcOrCommand.call(context || window);
			}

			shortcut.replace("Control", "Ctrl");

//			var shortcutObj = new qx.bom.Shortcut(shortcut);
//
//			shortcutObj.addListener("execute", function ()
//			{
//				funcOrCommand instanceof qx.ui.core.Command
//					? funcOrCommand.execute()
//					: funcOrCommand.call(context || window);
//			}, this);

			if (zone)
			{
				//zone.bind("focused", shortcutObj, "enabled");
				library.data.Helper.addPropertyListener(zone, "focused", function (focused)
				{
					if (focused)
					{
						window.shortcut.add(shortcut, execute);
					}
					else
					{
						window.shortcut.remove(shortcut);
					}
				});
			}
			else
			{
				window.shortcut.add(shortcut, execute);
			}
		}

	}
});
