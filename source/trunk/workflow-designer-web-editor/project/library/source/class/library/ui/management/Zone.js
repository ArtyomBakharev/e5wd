/**
 * @extends {qx.core.Object}
 */
qx.Class.define("library.ui.management.Zone", {
	extend: qx.core.Object,

//	events: {
//		"focus": "qx.event.type.Event",
//		"blur": "qx.event.type.Event"
//	},

	properties: {
		focused: {
			init: false,
			check: "Boolean",
			event: "changeFocused"
//			,apply: "__applyFocused"
		}
	},

	/**
	 * @param {qx.ui.core.Widget} [widget=null]
	 */
	construct: function (widget)
	{
		this.__widget = widget;
	},

	members: {

		/**
		 * @return {qx.ui.core.Widget} widget
		 */
		getWidget: function ()
		{
			return this.__widget;
		}

//		/**
//		 * @private
//		 */
//		__applyFocused: function(value)
//		{
//			this.fireEvent(value ? "focus" : "blur");
//		}

	}
});
