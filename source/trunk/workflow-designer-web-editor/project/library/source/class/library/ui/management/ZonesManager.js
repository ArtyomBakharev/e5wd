/**
 * @extends {qx.core.Object}
 */
qx.Class.define("library.ui.management.ZonesManager", {
	type: "singleton",

	extend: qx.core.Object,

	construct: function ()
	{
		this.__zones = [];

		this.__neutralZone = this.registerZone();
		this.__rootZone = this.registerZone(qx.core.Init.getApplication().getRoot());
	},

	members: {

		/**
		 * @param {qx.ui.core.Widget} control
		 */
		bindControlToNeutralZone: function (control)
		{
			this.bindControlToZone(control, this.__neutralZone);
		},

		/**
		 * @param {qx.ui.core.Widget} control
		 * @param {library.ui.management.Zone} zone
		 */
		bindControlToZone: function (control, zone)
		{
			control.addListener("focus", function ()
			{
				this.__disableFocusing = true;
				this.focusZone(zone);
			}, this);
			control.addListener("blur", function ()
			{
				this.__disableFocusing = false;
				zone.setFocused(false);
			}, this);
		},

		/**
		 * @return {library.ui.management.Zone}
		 */
		getNeutralZone: function ()
		{
			return this.__neutralZone;
		},

		/**
		 * @return {library.ui.management.Zone}
		 */
		getRootZone: function ()
		{
			return this.__rootZone;
		},

		focusNeutralZone: function ()
		{
			this.focusZone(this.__neutralZone);
		},

		/**
		 * @param {library.ui.management.Zone} zone
		 */
		focusZone: function (zone)
		{
			this.__changeZonesFocusedState(zone);
		},

		/**
		 * @param {qx.ui.core.Widget} [widget=null]
		 * @return {library.ui.management.Zone}
		 */
		registerZone: function (widget)
		{
			this.__setUpFocusing();

			var zone = new library.ui.management.Zone(widget);
			this.__zones.push(zone);
			return zone;
		},

		/**
		 * @param {qx.ui.core.Widget} widget
		 * @return {qx.ui.core.Widget}
		 * @private
		 */
		__getRealWidget: function (widget)
		{
			var realWidget = widget;
			var openerFound = false;
			while (true)
			{
				if (realWidget.getOpener && realWidget.getOpener())
				{
					realWidget = realWidget.getOpener();
					openerFound = true;
					continue;
				}

				if (openerFound)
				{
					break;
				}

				if (realWidget.getLayoutParent())
				{
					realWidget = realWidget.getLayoutParent();
				}
				else
				{
					realWidget = widget;
					break;
				}
			}

			return realWidget;
		},

		__changeZonesFocusedState: function (widgetOrZone)
		{
			var zone = widgetOrZone instanceof library.ui.management.Zone && widgetOrZone;
			var widget = zone ? zone.getWidget() : widgetOrZone;

			widget = widget && this.__getRealWidget(widget);

			this.__zones.forEach(function (curZone)
			{
				curZone.setFocused(!!(
					(curZone == zone)
						|| (
						widget && curZone.getWidget() && (
							qx.ui.core.Widget.contains(curZone.getWidget(), widget) || curZone.getWidget() == widget
							)
						)
					));
			}, this);
		},

		/**
		 * @private
		 */
		__setUpFocusing: function ()
		{
			if (this.__focusingSettedUp)
			{
				return;
			}
			this.__focusingSettedUp = true;

			function mouseaction(e)
			{
				if (this.__disableFocusing)
				{
					return;
				}

				var target = e.getTarget();
				if (target)
				{
					this.__changeZonesFocusedState(e.getTarget());
				}
			}

			var root = qx.core.Init.getApplication().getRoot();
			root.addListener("mousedown", mouseaction, this);
//			root.addListener("mouseup", mouseaction, this);
//			root.addListener("mousedown", mouseaction, this, true);
//			root.addListener("mouseup", mouseaction, this, true);
		}
	}
});
