/**
 * @extends {qx.core.Object}
 */
qx.Class.define("library.ui.messagesqueue.Manager", {

	extend: qx.core.Object,

	properties: {

		messageWidth: {
			init: 300,
			check: "Integer"
		},

		queueLength: {
			init: 5,
			check: "Integer"
		},

		rightMargin: {
			init: 10,
			check: "Number"
		},

		bottomMargin: {
			init: 10,
			check: "Number"
		},

		messagesDistance: {
			init: 10,
			check: "Number"
		},

		zIndex: {
			init: 10,
			check: "Integer"
		}

	},

	construct: function ()
	{
		this.__queue = [];
		this.__animation = new library.Deferred();
		this.__animation.call();
		this.__root = qx.core.Init.getApplication().getRoot();
	},

	members: {

		/**
		 * @param {library.ui.messagesqueue.Message} message
		 */
		addMessage: function (message)
		{
			message.addListener("close", function (){ this.removeMessage(message) }, this);

			this.__animation = this.__animation.next(this.__processNewMessage(message));

			var chains = [this.__placeNewMessage(message), this.__moveMessagesQueue()];
			if (this.__queue.length > this.getQueueLength())
			{
				chains.push(this.__removeMessage(0));
			}

			this.__animation = this.__animation.parallel(chains);
		},

		/**
		 * @param {library.ui.messagesqueue.Message} message
		 */
		removeMessage: function (message)
		{
			var index = this.__queue.indexOf(message);
			if (index !== -1)
			{
				this.__animation = this.__animation.next(this.__removeMessage(index));

				if (index !== 0)
				{
					this.__animation = this.__animation.next(this.__moveMessagesQueue());
				}
			}
		},

		/**
		 * @private
		 */
		__calculateMessagesPositions: function (messages)
		{
			var nextBottom = this.getBottomMargin();
			return messages
				.clone()
				.reverse()
				.map(function (message)
				{
					var result = nextBottom;
					nextBottom += parseInt(qx.bom.element.Style.get(message.getContainerElement().getDomElement(),
						"height")) + this.getMessagesDistance();
					return result;
				}, this)
				.reverse();
		},

		/**
		 * @private
		 */
		__createHideEffect: function (message)
		{
			return new qx.fx.effect.core.Fade(message.getContainerElement().getDomElement());
		},

		/**
		 * @private
		 */
		__createMoveEffect: function (message, newPosition)
		{
			var element = message.getContainerElement().getDomElement();
			var currentBottom = parseInt(qx.bom.element.Style.get(element, "bottom"));

			if (currentBottom != newPosition)
			{
				var effect = new qx.fx.effect.core.Style(element, "bottom",
					function (x){ return x + "px" });

				effect.set({
					from: currentBottom,
					to: newPosition
				});

				return effect;
			}
		},

		/**
		 * @private
		 */
		__createShowEffect: function (message)
		{
			return new qx.fx.effect.core.Fade(message.getContainerElement().getDomElement())
				.set({
					from: 0,
					to: 1
				});
		},

		/**
		 * @private
		 */
		__moveMessagesQueue: function ()
		{
			var messages = this.__queue.clone();

			return (function ()
			{
				var positions = this.__calculateMessagesPositions(messages);
				var effects = [];

				positions.forEach(function (bottom, index)
				{
					var message = messages[index];
					var effect = this.__createMoveEffect(message, bottom);
					if (effect)
					{
						effects.push(effect.chain());
					}
				}, this);

				return library.Deferred.parallel(effects);

			}).bind(this);
		},

		/**
		 * @private
		 */
		__placeNewMessage: function (message)
		{
			return (function ()
			{
				return this.__createShowEffect(message).chain()
					.next(function (){ message.show() });

			}).bind(this);
		},

		/**
		 * @private
		 */
		__processNewMessage: function (message)
		{
			this.__queue.push(message);

			return (function ()
			{
				this.__root.add(message);
				return library.Deferred.wait(0)
					.next(function (){ this.__setNewMessageStyles(message) }, this);

			}).bind(this);
		},

		/**
		 * @private
		 */
		__removeMessage: function (index)
		{
			var message = this.__queue.removeAt(index);

			return (function ()
			{
				return this.__createHideEffect(message).chain()
					.next(function (){
						message.close();
						message.destroy();
					});

			}).bind(this);
		},

		/**
		 * @private
		 */
		__setNewMessageStyles: function (message)
		{
			message.set({
				width: this.getMessageWidth(),
				zIndex: this.getZIndex()
			});

			qx.bom.element.Style.setStyles(message.getContainerElement().getDomElement(), {
				top: "auto",
				left: "auto",
				right: this.getRightMargin() + "px",
				bottom: (-parseInt(qx.bom.element.Style.get(message.getContainerElement().getDomElement(),
					"height")) - this.getBottomMargin() - this.getMessagesDistance()) + "px",
				opacity: 0
			});
		}
	},

	defer: function ()
	{
		qx.Class.patch(qx.fx.Base, library.fx.MBaseExtend);
	}
});
