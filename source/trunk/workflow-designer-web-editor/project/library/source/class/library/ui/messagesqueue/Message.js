/*
 #asset(qx/icon/${qx.icontheme}/64/status/dialog-warning.png)
 #asset(qx/icon/${qx.icontheme}/64/status/dialog-information.png)
 * @extends {qx.ui.core.Widget}
 */
qx.Class.define("library.ui.messagesqueue.Message", {

	extend: qx.ui.core.Widget,

	include: [
		qx.ui.core.MRemoteChildrenHandling,
		qx.ui.core.MRemoteLayoutHandling,
		qx.ui.core.MContentPadding
	],

	statics: {
		BUTTON_KIND_OK: "ok",
		BUTTON_KIND_CANCEL: "cancel",

		/**
		 * @param {String} message
		 * @return {library.ui.messagesqueue.Message}
		 */
		createAlertMessage: function (message)
		{
			return new library.ui.messagesqueue.Message(message, "icon/64/status/dialog-warning.png");
		},

		/**
		 * @param {String} message
		 * @return {library.ui.messagesqueue.Message}
		 */
		createInfoMessage: function (message)
		{
			return new library.ui.messagesqueue.Message(message, "icon/64/status/dialog-information.png");
		}
	},

	events: {
		"close": "qx.event.type.Event"
	},

	properties: {
		appearance: {
			refine: true,
			init: "messagequeue-message"
		},
		buttons: {
			init: null,
			check: "Array",
			apply: "_applyButtons"
		},
		icon: {
			init: null,
			check: "String",
			event: "changeIcon"
		},
		resultKind: {
			init: null,
			check: "String"
		},
		timeToClose: {
			init: 5000,
			check: "Integer"
		}
	},

	construct: function (label, icon, buttons)
	{
		this.base(arguments);
		var layout = new qx.ui.layout.Grid();
		this._setLayout(layout);
		layout.setColumnFlex(0, 1);

		this._createChildControl("body");
		this._createChildControl("pane");
		this._createChildControl("icon");
		this._createChildControl("buttons-pane");
		this._createChildControl("close-button");

		if (label)
		{
			this.setLayout(new qx.ui.layout.VBox(0, "middle"));
			this.add(new qx.ui.basic.Label(label));
		}

		if (icon)
		{
			this.setIcon(icon);
		}

		if (buttons)
		{
			this.setButtons(buttons);
		}
	},

	members: {

		close: function ()
		{
			this.fireEvent("close");
		},

		getChildrenContainer: function ()
		{
			return this.getChildControl("pane");
		},

		show: function()
		{
			setTimeout((function (){ this.close() }).bind(this), this.getTimeToClose());
		},

		/**
		 * @protected
		 */
		_applyButtons: function (buttons)
		{
			var buttonsPane = this.getChildControl("buttons-pane");
			buttonsPane.removeAll();

			buttons.forEach(function (button)
			{
				var control = new qx.ui.form.Button(button.title);

				if (button.kind)
				{
					control.addListener("execute", function (){ this.setKind(button.kind) }, this);
				}

				if (button.execute)
				{
					if (button.execute instanceof qx.ui.core.Command)
					{
						control.setCommand(button.execute);
					}
					else
					{
						control.addListener("execute", button.execute, button.context || this);
					}
				}

				if (!button.dontClose)
				{
					control.addListener("execute", function (){ this.close() }, this);
				}
			}, this);
		},

		/**
		 * @protected
		 */
		_createChildControlImpl: function (id)
		{
			var control;

			switch (id)
			{
				case "body":
					var layout = new qx.ui.layout.Grid();
					control = new qx.ui.container.Composite(layout);
					layout.setRowFlex(0, 1);
					this._add(control, {row: 0, column: 0});
					break;

				case "pane":
					control = new qx.ui.container.Composite();
					this.getChildControl("body").add(control, {row: 0, column: 1});
					break;

				case "icon":
					control = new qx.ui.basic.Image();
					this.bind("icon", control, "source");
					this.getChildControl("body").add(control, {row: 0, column: 0, rowSpan: 2});
					break;

				case "buttons-pane":
					control = new qx.ui.container.Composite(new qx.ui.layout.HBox(5, "right"));
					this.getChildControl("body").add(control, {row: 1, column: 1});
					break;

				case "close-button":
					control = new qx.ui.form.Button();
					control.addState("active");
					control.setFocusable(false);
					control.setAllowGrowY(false);
					control.addListener("execute", function(){ this.close() }, this);

					this._add(control, {row: 0, column: 1});
					break;
			}

			return control || this.base(arguments, id);
		},

		/**
		 * @protected
		 */
		_getContentPaddingTarget: function ()
		{
			return this.getChildControl("pane");
		}
	}
});
