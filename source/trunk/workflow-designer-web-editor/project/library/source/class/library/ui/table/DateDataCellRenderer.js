/**
 * @extends {qx.ui.table.cellrenderer.Date}
 */
qx.Class.define("library.ui.table.DateDataCellRenderer", {

	extend: qx.ui.table.cellrenderer.Date,

	members: {

		/**
		 * @protected
		 */
		_getContentHtml: function (cellInfo)
		{
			if (cellInfo.value && qx.lang.Type.isString(cellInfo.value))
			{
				cellInfo.value = Date.parseISO(cellInfo.value);
			}

			return this.base(arguments, cellInfo);
		}
	}
});
