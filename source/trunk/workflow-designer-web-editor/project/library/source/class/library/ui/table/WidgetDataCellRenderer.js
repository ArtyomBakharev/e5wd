/**
 * @extends {qx.ui.table.cellrenderer.Html}
 */
qx.Class.define("library.ui.table.WidgetDataCellRenderer", {

	extend: qx.ui.table.cellrenderer.Html,

	statics: {
		_LAST_ID: -1
	},

	members: {

		/**
		 * @param {qx.ui.core.Widget} widgetFactory
		 */
		setWidgetFactory: function (widgetFactory){this._widgetFactory = widgetFactory;},
		getWidgetFactory: function (){return this._widgetFactory;},

		/**
		 * @protected
		 */
		_getContentHtml: function (cellInfo)
		{
			var widget = this._widgetFactory(cellInfo);
			var id = "custom-table-cell-widget-" + (++this.self(arguments)._LAST_ID);
			return "<div id='" + id + "'></div>";
		}
	}
});
