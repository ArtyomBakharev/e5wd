/*
 #asset(qx/icon/${qx.icontheme}/22/actions/dialog-apply.png)
 #asset(qx/icon/${qx.icontheme}/22/actions/dialog-cancel.png)
 */
qx.Class.define("library.ui.widget.ButtonsPane", {

	extend: qx.ui.core.Widget,

	statics: {
		__ICON_BY_KIND: {
			ok: "icon/22/actions/dialog-apply.png",
			cancel: "icon/22/actions/dialog-cancel.png"
		}
	},

	/**
	 * @param {Object[]} buttons [{label: String, execute: Function[, context: Object][, kind: String]}, ...]
	 * button kind - ok, cancel
	 */
	construct: function (buttons)
	{
		this.base(arguments);

		this._setLayout(new qx.ui.layout.HBox(10, "right"));
		buttons.forEach(function (button)
		{
			var icon = button.icon;
			if (!icon && button.kind)
			{
				icon = this.self(arguments).__ICON_BY_KIND[button.kind];
			}

			var buttonWidget = new qx.ui.form.Button(button.label, icon || null);
			buttonWidget.addListener("execute", function(){ button.execute.call(button.context || window) });

			this._add(buttonWidget);
		}, this);
	}
});
