/**
 * @extends {qx.ui.form.SelectBox}
 */
qx.Class.define("library.ui.widget.SelectBox", {

	extend: qx.ui.form.SelectBox,

	events: {
		"changeValue": "qx.event.type.Data"
	},

	properties: {
		value: {
			init: null,
			nullable: true,
			event: "changeValue",
			apply: "_setValue"
		}
	},

	construct: function (noLabel)
	{
		this.__noLabel = noLabel || "нет";

		this.base(arguments);
		this.bind("changeSelection", this, "value", {converter: (function (x)
		{
			return x && x.length > 0 && x[0].getModel() !== this.__SB_NULL_MARKER ? x[0].getModel() : null
		}).bind(this)});

//		this.addListener("changeSelection", function(){
//			this.fireDataEvent("changeValue", this.getValue());
//		}, this);
	},

	members: {

		__SB_NULL_MARKER: {},

		setItems: function (items)
		{
			var childrenCount = this.getChildren().length;

			this._sbItems = items;

			if (this._sbNullable)
			{
				this.add(new qx.ui.form.ListItem(this.__noLabel, null, this.__SB_NULL_MARKER));
			}

			items.forEach(function (row)
			{
				if (!qx.lang.Type.isArray(row))
				{
					row = [row];
				}

				this.add(new qx.ui.form.ListItem(row.length > 1 ? row[1].toString() : row[0].toString(), null, row[0]));
			}, this);

			if (childrenCount > 0)
			{
				for (var i = 0; i < childrenCount; ++i)
				{
					this.removeAt(0);
				}
			}
		},

//		getValue: function(){
//			var item = this.getSelection()[0];
//			if (!item || item.getModel() == this.__SB_NULL_MARKER)
//				return null;
//
//			return item.getModel() === null ? item.getLabel() : item.getModel();
//		},

		/**
		 * @protected
		 */
		_setValue: function (value)
		{
			if (value === null)
			{
				value = this.__SB_NULL_MARKER;
			}

			var children = this.getChildren();
			for (var i = 0; i < children.length; ++i)
			{
				var child = children[i];
				if (child instanceof qx.ui.form.ListItem && (child.getModel() == value || (child.getModel() === null && child.getLabel() == value.toString())))
				{
					this.setSelection([child]);
					return;
				}
			}

			throw "[SelectBox] Нет такого значения: " + value;
		},

		setNullable: function (nullable)
		{
			this._sbNullable = nullable;
			this.setItems(this._sbItems || []);
		},

		getNullable: function ()
		{
			return this._sbNullable;
		}
	}
});
