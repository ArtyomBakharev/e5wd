/*
 #asset(qx/icon/${qx.icontheme}/48/status/image-loading.png)
 * @extends {qx.ui.container.Composite}
 */
qx.Class.define("library.ui.widget.SelectTimePicker", {

	extend: qx.ui.container.Composite,

	construct: function ()
	{
		this.base(arguments, new qx.ui.layout.HBox(5));

		this._seconds = 0;
		this._minutesStep = 5;

		var width = 45;
		this._hoursWidget = new qx.ui.form.SelectBox().set({width: width});
		this._minutesWidget = new qx.ui.form.SelectBox().set({width: width});

		var hours = [];
		for (var i = 0; i < 24; ++i)
		{
			hours.push([i, i]);
		}
		this._hoursWidget.setItems(hours);

		var minutes = [];
		for (var i = 0; i < 60; i += this._minutesStep)
		{
			minutes.push([i, i]);
		}
		this._minutesWidget.setItems(minutes);

		this.add(this._hoursWidget);
		this.add(new qx.ui.basic.Label("ч"));
		this.add(this._minutesWidget);
		this.add(new qx.ui.basic.Label("м"));
	},

	members: {

		getHours: function (){ return this._hoursWidget.getValue(); },
		setHours: function (hours)
		{
			hours = parseInt(hours);
			qx.core.Assert.assert(!isNaN(hours) && hours >= 0 && hours < 24, "Incorrect hours value");
			this._hoursWidget.setValue(hours);
		},

		getMinutes: function (){ return this._minutesWidget.getValue(); },
		setMinutes: function (minutes)
		{
			minutes = parseInt(minutes);
			qx.core.Assert.assert(!isNaN(minutes) && minutes >= 0 && minutes < 60, "Incorrect minutes value");
			this._minutesWidget.setValue(Math.floor(minutes / this._minutesStep) * this._minutesStep);
		},

		getSeconds: function (){ return this._seconds; },
		setSeconds: function (seconds)
		{
			seconds = parseInt(seconds);
			qx.core.Assert.assert(!isNaN(seconds) && seconds >= 0 && seconds < 60, "Incorrect seconds value");
			this._seconds = seconds;
		},

		resetValue: function ()
		{
			this._hoursWidget.setValue(0);
			this._minutesWidget.setValue(0);
			this._seconds = 0;
		}

	}
});
