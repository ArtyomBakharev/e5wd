/*
 #asset(qx/icon/${qx.icontheme}/48/status/image-loading.png)
 * @extends {qx.ui.container.Composite}
 */
qx.Class.define("library.ui.widget.TextTimePicker", {

	extend: qx.ui.container.Composite,

	construct: function ()
	{
		this.base(arguments, new qx.ui.layout.HBox(5));

		this._seconds = 0;
		this._minutesStep = 5;

		var width = 55;
		tb = this._textBox = new qx.ui.form.TextField().set({width: width});
		this.resetValue();

		this._handleChangeValue = true;
		this._textBox.addListener("changeValue", function ()
		{

			if (!this._handleChangeValue)
			{
				return;
			}

			try
			{

				var matches = this._textBox.getValue().match(/^(\d\d?):(\d\d?):(\d\d?)$/);
				this.setHours(matches[1]);
				this.setMinutes(matches[2]);
				this.setSeconds(matches[3]);

			} catch (e)
			{
			}

			this._textBox.setValue(this._getTimeString());

		}, this);

		this.add(this._textBox);
	},

	members: {

		getHours: function (){ return this._hours; },
		setHours: function (hours, withText){ this._setTimePart("hours", hours, withText); },

		getMinutes: function (){ return this._minutes; },
		setMinutes: function (minutes, withText){ this._setTimePart("minutes", minutes, withText); },

		getSeconds: function (){ return this._seconds; },
		setSeconds: function (seconds, withText){ this._setTimePart("seconds", seconds, withText); },

		resetValue: function ()
		{
			this._textBox.setValue("00:00:00");
			this._hours = 0;
			this._minutes = 0;
			this._seconds = 0;
		},

		/**
		 * @protected
		 */
		_setTimePart: function (part, value, withText)
		{
			value = parseInt(value);
			qx.core.Assert.assert(!isNaN(value) && value >= 0 && value < {seconds: 60, minutes: 60, hours: 24}[part], "Incorrect " + part + " value");

			switch (part)
			{
				case "hours":
					this._hours = value;
					break;
				case "minutes":
					this._minutes = value;
					break;
				case "seconds":
					this._seconds = value;
					break;
			}

			if (withText)
			{
				this._handleChangeValue = false;
				this._textBox.setValue(this._getTimeString());
				this._handleChangeValue = true;
			}
		},

		/**
		 * @protected
		 */
		_getTimeString: function ()
		{
			return this._timePartToString(this._hours) + ":" + this._timePartToString(this._minutes) + ":" + this._timePartToString(this._seconds);
		},

		/**
		 * @protected
		 */
		_timePartToString: function (value)
		{
			value = value.toString();
			if (value.length < 2)
			{
				value = "0" + value;
			}

			return value;
		}

	}
});
