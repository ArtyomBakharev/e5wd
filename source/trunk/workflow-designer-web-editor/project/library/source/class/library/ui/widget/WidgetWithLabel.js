/**
 * @extends {qx.ui.container.Composite}
 */
qx.Class.define("library.ui.widget.WidgetWithLabel", {

	extend: qx.ui.container.Composite,

	/**
	 * @param {qx.ui.core.Widget} widget
	 * @param labelText
	 * @param labelWidth
	 */
	construct: function (widget, labelText, labelOptions)
	{
		this.setLayout(new qx.ui.layout.HBox());

		this._widget = widget;
		this._label = new qx.ui.basic.Label().set({buddy: widget});
		if (labelText)
		{
			this._label.setValue(labelText);
		}
		if (labelOptions)
		{
			this._label.set(labelOptions);
		}

		this.base(arguments);
		this.add(this._label);
		this.add(this._widget);
	},

	members: {
		/**
		 * @return {qx.ui.core.Widget}
		 */
		getWidget: function (){ return this._widget; },
		/**
		 * @return {qx.ui.form.Label}
		 */
		getLabel: function (){ return this._label; }
	}
});
