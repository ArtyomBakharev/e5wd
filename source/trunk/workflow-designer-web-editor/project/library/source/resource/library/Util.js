//utils
Util = {};
Util.breakable = function (func, funcContext, ifNotBreaks, ifNotBreaksContext, defaultResult)
{
	var broken = false;
	var breaker = {};
	var result = defaultResult;
	var breakFunc = function (result)
	{
		breaker.result = result;
		throw breaker;
	};

	try
	{
		result = func.call(funcContext || window, breakFunc);
	} catch (e)
	{
		if (e !== breaker)
		{
			throw e;
		}
		broken = true;
		result = breaker.result;
	}

	if (!broken && ifNotBreaks)
	{
		result = ifNotBreaks.apply(ifNotBreaksContext || window);
	}

	return result;
};

Util.breakableEach = function (collection, func, funcContext, ifNotBreaks, ifNotBreaksContext, defaultResult)
{
	return Util.breakable(function ($break)
	{
		if (Array.isArray(collection))
		{
			collection.forEach(func.bind(funcContext || window, $break));
		}
		else
		{
			Object.forEach(collection, func.bind(funcContext || window, $break));
		}
	}, window, ifNotBreaks, ifNotBreaksContext, defaultResult);
};
