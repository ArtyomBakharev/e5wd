Array.slice = function (arr, from, to)
{
	return Array.prototype.slice.call(arr, from || 0, to);
};

Array.prototype.buildIndex = function (keyFunc)
{
	if (typeof keyFunc == "string")
	{
		keyFunc = (function (key){ return function (obj){ return obj[key]; }; })(keyFunc);
	}

	var index = {};
	this.forEach(function (value)
	{
		index[keyFunc(value)] = value;
	});

	return index;
};

Array.prototype.callAll = function (context, varargs)
{
	var args = arguments;
	return this.map(function (x){ return x.call.apply(x, args) });
};

Array.prototype.last = function ()
{
	return this[this.length - 1];
};

Array.prototype.removeByLambda = function (lambda)
{
	var excluded;
	for (var i = this.length - 1; i >= 0; --i)
	{
		if (lambda(this[i]))
		{
			excluded = this.splice(i, 1);
		}
	}

	return excluded;
};

Array.prototype.invoke = function (method, varargs)
{
	var results = [];
	var args = Array.slice(arguments, 1);
	this.forEach(function (item){ results.push(item[method].apply(item, args)); });
	return results;
};

Array.prototype.mapToObject = function (selector, context)
{
	var newObj = {};
	this.forEach(function (value, key)
	{
		var result = selector.call(context || window, value, key);
		newObj[result[0]] = result[1];
	});

	return newObj;
};

Array.prototype.flatten = function (selector, context)
{
	if (!selector)
	{
		selector = Function.returnFirstArg;
	}

	var result = [];
	this.forEach(function (item, key)
	{
		result = result.concat(selector.call(context || window, item, key));
	});

	return result;
};
