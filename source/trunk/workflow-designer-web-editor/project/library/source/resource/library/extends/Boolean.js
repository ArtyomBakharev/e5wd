Boolean.xor = function()
{
	var wasTrue = false;
	for (var i = 0, operand; operand = arguments[i++];)
		if (operand)
			if (wasTrue)
				return false;
			else
				wasTrue = true;

	return wasTrue;
};
