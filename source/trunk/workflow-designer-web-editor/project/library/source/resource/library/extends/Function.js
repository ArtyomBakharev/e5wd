Function.abstractFunc = function (){ throw "abstract"; };

Function.returnFirstArg = function (arg){ return arg; };

Function.proxy = function (obj, method, varargs)
{
	return obj[method].bind.apply(obj[method], [obj].concat(Array.slice(arguments, 2)));
};

Function.mergePredicates = function (method)
{
	var predicates = Array.slice(arguments, 1);
	return function ()
	{
		var args = arguments;
		var thisObj = this;
		return predicates[method](function (p){ return p.apply(thisObj, args) });
	}
};

Function.prototype.methodize = function (context)
{
	var func = this;
	return function ()
	{
		return func.apply(context || window, [this].concat(Array.toArray(arguments)));
	}
};

Function.prototype.closedBind = function (self, varargs)
{
	var func = this.bind.apply(this, arguments);
	return function (){ return func(); };
};

Function.prototype.defer = function (context, varargs)
{
	return setTimeout(this.bind.apply(this, arguments), 0);
};

Function.prototype.timeout = function (time, context, varargs)
{
	return setTimeout(this.bind.apply(this, Array.slice(arguments, 1)), time);
};

Function.prototype.argumentNames = function ()
{
	var names = this.toString().match(/^[\s\(]*function[^(]*\(([^)]*)\)/)[1]
		.replace(/\/\/.*?[\r\n]|\/\*(?:.|[\r\n])*?\*\//g, '')
		.replace(/\s+/g, '').split(',');
	return names.length == 1 && !names[0] ? [] : names;
};

/**
 * @param [throwException=true]
 */
Function.prototype.forbidMultiCall = function (throwException)
{
	var called = {};
	var fn = this;
	throwException = Object.ifUndef(throwException, true);

	return function ()
	{
		var id = Object.getUniqueId(this);
		if (called[id])
		{
			if (throwException)
			{
				throw "Эта функция должна быть вызвана лишь однажды для каждого контекста!";
			}
		}
		else
		{
			called[id] = true;
			return fn.apply(this, arguments);
		}
	}
};

/**
 * Для каждого контекста кэширует результат выполнения функции и возвращает его при последующих вызовах
 */
Function.prototype.cacheResult = function ()
{
	var result = {};
	var fn = this;

	var wrapper = function ()
	{
		var id = Object.getUniqueId(this);
		if (id in result)
		{
			return result[id];
		}
		else
		{
			return result[id] = fn.apply(this, arguments);
		}
	};

	wrapper.remove = function (obj)
	{
		delete result[Object.getUniqueId(obj)];
	};

	return wrapper;
};

/**
 * позволяет «затормозить» функцию — функция будет выполняться не чаще
 * одного раза в указанный период, даже если она будет вызвана много раз
 * в течение этого периода. Т.е. все промежуточные вызовы будут игнорироваться.
 */
Function.prototype.throttling = function (timeout, ctx)
{
	var fn = this;
	var timer, args, needInvoke;

	return function ()
	{

		args = arguments;
		needInvoke = true;
		ctx = ctx || this;

		if (!timer)
		{
			(function invoke()
			{
				if (needInvoke)
				{
					first = false;
					fn.apply(ctx, args);
					needInvoke = false;
					timer = setTimeout(invoke, timeout);
				}
				else
				{
					timer = null;
				}
			})();
		}
	};
};

Function.prototype.lateThrottling = function (timeout, ctx)
{
	var fn = this;
	var timer, args;

	return function ()
	{

		args = arguments;
		ctx = ctx || this;

		if (!timer)
		{
			setTimeout(function ()
			{
				fn.apply(ctx, args);
				timer = null;
			}, timeout);
		}
	};
};
