/**
 * @param {Array} array
 * @param {Function} lambda
 * @return {Number} индекс меньшего элемента, -1 если такового нет
 */
Math.binarySearch = function(array, lambda)
{
	var left = 0;
	var right = array.length;
	if (right == 0)
	{
		return -1;
	}

	while (true)
	{
		var center = Math.floor((left + right) / 2);
		var centerItem = array[center];
		var eq = lambda(centerItem);

		if (eq == 0)
		{
			return center;
		}

		if (eq > 0)
		{
			if (left == center)
			{
				return center;
			}
			left = center;
		}
		else
		{
			if (right == center)
			{
				return center - 1;
			}
			right = center;
		}
	}
};
