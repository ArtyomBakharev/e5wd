Object.forEach = function(object, iterator, context)
{
	for (var name in object)
		if (object.hasOwnProperty(name))
			iterator.call(context || window, object[name], name, object);
};

Object.pathAccess = AppTools.pathAccess;

Object.ifUndef = function(obj, def) {
	return obj === undefined ? def : obj;
};

Object.fetchFirstKey = function(obj)
{
	for (var i in obj)
		if (obj.hasOwnProperty(i))
			return i;
};

(function(){
	var filterObjectDefault = function(value){return value !== null && value !== undefined;};
	Object.filterObject = function(obj, predicate)
	{
		var newObj = {};
		predicate = predicate || filterObjectDefault;
		Object.forEach(obj, function(value, key){
			if (predicate(value))
				newObj[key] = value;
		});

		return newObj;
	};
})();

/**
 * @param obj
 * @param [selector=null]
 * @param [context=null]
 * @return {Object}
 */
Object.mapObject = function(obj, selector, context)
{
	var newObj = {};
	Object.forEach(obj, function(value, key){
		newObj[key] = selector.call(context || window, value, key);
	});

	return newObj;
};

Object.mapObjectToArray = function(obj, selector, context)
{
	var arr = [];
	Object.forEach(obj, function(value, key){
		arr.push(selector.call(context || window, value, key));
	});

	return arr;
};

Object.deepCopy = AppTools.deepCopy;
Object.getUniqueId = AppTools.getUniqueId;

Object.construct = function(cls, args)
{
	function tmp(){}
	tmp.prototype = cls.prototype;
	tmp.constructor = cls;
	var obj = new tmp();
	cls.apply(obj, args || []);
	return obj;
};

