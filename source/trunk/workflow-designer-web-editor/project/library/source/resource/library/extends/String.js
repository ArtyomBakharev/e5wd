String.prototype.trimChars = function(characters)
{
	if (!characters)
		return this.trim();
	characters = characters ? characters.escapeRegexpChars() : '\\s';
	return this.replace(new RegExp('\^[' + characters + ']+|[' + characters + ']+$', 'g'), '');
};

String.prototype.humanize = function()
{
	return this.hyphenate().replace(/-|_/g, " ");
};

String.prototype.upFirstLetterInWords = function()
{
	return this.split(" ").map(function(x){ return x.firstUp() }).join(" ");
};

String.prototype.upFirstLetterInWords = function()
{
	return this.split(" ").map(function(x){ return x.firstUp() }).join(" ");
};

String.isNullUndefinedOrEmpty = function(str)
{
	return str === null || str === undefined || str === "";
};
