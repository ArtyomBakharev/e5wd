/**
 * ООП и конфигурация для приложения
 */
var AppTools = {

	getUniqueId: (function ()
	{
		var key = "$$uniqueObjectId";
		var nextId = 1;
		return function (obj)
		{
			return obj[key] || (obj[key] = nextId++);
		};
	})(),

	getUniqueString: function ()
	{
		return Date.now().toString() + Math.round(Math.random() * 1000000).toString();
	},

	/**
	 * Сделать Child наследником класса Parent
	 * @param {Function} Child
	 * @param {Function} Parent
	 */
	inheritClass: function (Child, Parent)
	{
		var F = function () { };
		F.prototype = Parent.prototype;
		Child.prototype = new F();
		Child.prototype.constructor = Child;
		Child.superclass = Parent.prototype;
	},

	/**
	 * Создать класс с родителем Parent и методами Methods (если передан один аргумент, то он интерпретируется как хеш с методами)
	 * Функция с ключём construct в методах интерпретируется как конструктор класса
	 * @param [base=null]
	 * @param description
	 */
	createClass: function (base, description)
	{
		if (arguments.length == 1)
		{
			description = base;
			base = null;
		}

		var construct = description.construct || function () {};

		function cls() { construct.apply(this, arguments); }

		if (description.statics)
		{
			this.shallowCopy(description.statics, cls);
		}

		if (base)
		{
			this.inheritClass(cls, base);
		}

		if (description.members)
		{
			this.shallowCopy(description.members, cls.prototype);
		}

		if (description.construct)
		{
			cls.prototype.__construct__ = description.construct;
		}

		return cls;
	},

	/**
	 * Размещает объединяет конфигурацию config с конфигурацией расположенной по пути symbol (например "app.config.foo.bar")
	 * @param {String} symbol
	 * @param {Object} config
	 */
	config: function (symbol, config)
	{
		var dest = AppTools.pathAccess(symbol, AppTools.pathAccess.setVarIfNotExists);
		AppTools.deepCopy(config, dest);
	},

	/**
	 * Доступ к данным внутри объекта по переданному пути
	 * @param {Object} obj объект, к которому производится доступ
	 * @param {String} path путь до требуемых данных (например, "foo.bar" для доступа к obj.foo.bar)
	 * @param {String} [action=AppTools.pathAccess.getVar] тип доступа:
	 * AppTools.pathAccess.getVar - получение данных (если данные отсутствуют возвращается undefined)
	 * AppTools.pathAccess.setVar - присвоить полю данные data, при этом если какая-то чать пути отсутствует, то она создаётся как пустой объект
	 * AppTools.pathAccess.setVarIfNotExists - присвоить полю данные data, если оно отсутствует, при этом если какая-то чать пути отсутствует, то она создаётся как пустой объект
	 * AppTools.pathAccess.deleteVar - удалить поле по указанному пути
	 * @param {*} [data=null] данные, которые нужно назначить, если action равно getVar или setVar
	 * @return {*}
	 */
	pathAccess: function (obj, path, action, data)
	{
		if (typeof obj == "string")
		{
			data = action;
			action = path;
			path = obj;
			obj = window;
		}

		if (!action)
		{
			action = AppTools.pathAccess.getVar;
		}

		if (!data)
		{
			data = {};
		}

		path = path.split(".");

		for (var i = 0, length = path.length; i < length; ++i)
		{
			var chunk = path[i];

			if (i == length - 1)
			{
				var old = obj[chunk];
				switch (action)
				{
					case AppTools.pathAccess.getVar:
						return obj[chunk];
					case AppTools.pathAccess.setVar:
						obj[chunk] = data;
						return old;
					case AppTools.pathAccess.setVarIfNotExists:
						if (old === undefined)
						{
							obj[chunk] = data;
						}
						return obj[chunk];
					case AppTools.pathAccess.deleteVar:
						delete obj[chunk];
						return old;
				}
			}
			else if (obj[chunk] === undefined)
			{
				if (action == AppTools.pathAccess.setVar || action == AppTools.pathAccess.setVarIfNotExists)
				{
					obj[chunk] = {};
				}
				else
				{
					return undefined;
				}
			}

			obj = obj[chunk];
		}
	},

	/**
	 * Глубокая копия объекта source в объект dest. Одноимённые поля в dest затираются.
	 * @param {Object} source
	 * @param {Object} [dest={}]
	 */
	deepCopy: function (source, dest)
	{
		if (typeof source == "object" && source !== null)
		{
			var sourceIsArray = source.constructor === Array;
			if (!dest)
			{
				dest = sourceIsArray ? [] : {};
			}

			var destIsArray = dest.constructor === Array;

			var prepend;
			var append;
			AppTools.objForEach(source, function (value, key)
			{
				if (destIsArray && !sourceIsArray)
				{
					if (key == "prepend")
					{
						prepend = value;
						return;
					}
					else if (key == "append")
					{
						append = value;
						return;
					}
				}

				dest[key] = AppTools.deepCopy(value, dest[key]);
			});

			if (append)
			{
				for (var i = 0; i < append.length; ++i)
				{
					dest.push(append[i]);
				}
			}

			if (prepend)
			{
				for (var i = prepend.length - 1; i >= 0; --i)
				{
					dest.unshift(prepend[i]);
				}
			}
		}
		else
		{
			dest = source;
		}

		return dest;
	},

	/**
	 * Расширение объекта dest полями из объекта source
	 * @param {Object} source
	 * @param {Object} [dest={}]
	 * @return {Object} dest
	 */
	shallowCopy: function (source, dest)
	{
		if (dest == null)
		{
			dest = {};
		}

		this.objForEach(source, function (value, key)
		{
			dest[key] = value;
		});


		return dest;
	},

	objForEach: function (obj, iterator, context)
	{
		for (var i in obj)
		{
			if (obj.hasOwnProperty(i))
			{
				iterator.call(context || window, obj[i], i, obj);
			}
		}
	},

	/**
	 * Создаёт объект XMLHttpRequest
	 * @return {XMLHttpRequest}
	 */
	getXHR: function ()
	{
		var xmlhttp;
		try
		{
			xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e)
		{
			try
			{
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (E)
			{
				xmlhttp = false;
			}
		}
		if (!xmlhttp && typeof XMLHttpRequest != 'undefined')
		{
			xmlhttp = new XMLHttpRequest();
		}
		return xmlhttp;
	},

	DOMReady: (function ()
	{
		var $this = this;

		var fns = [],
			ready = function ()
			{
				$this.isReady = true;

				// call all functions
				for (var x = 0; x < fns.length; x++)
				{
					fns[x].call();
				}
			};

		this.isReady = false;

		// public add method
		this.add = function (fn)
		{
			// eval string in a function
			if (fn.constructor == String)
			{
				var strFunc = fn;
				fn = function () { eval(strFunc); };
			}

			// call imediately when DOM is already ready
			if ($this.isReady)
			{
				fn.call();
			}
			else
			{
				// add to the list
				fns[fns.length] = fn;
			}
		};

		// For all browsers except IE
		if (window.addEventListener)
		{
			document.addEventListener('DOMContentLoaded', function () { ready(); }, false);
		}
		else
		// For IE
		// Code taken from http://ajaxian.com/archives/iecontentloaded-yet-another-domcontentloaded
		{
			(function ()
			{
				// check IE's proprietary DOM members
				if (!document.uniqueID && document.expando)
				{
					return;
				}

				// you can create any tagName, even customTag like <document :ready />
				var tempNode = document.createElement('document:ready');

				try
				{
					// see if it throws errors until after ondocumentready
					tempNode.doScroll('left');
				} catch (err)
				{
					setTimeout(arguments.callee, 0);
					return;
				}

				// call ready
				ready();
			})();
		}

		return this;
	})(),

	gccBugFix: function (localSymbol, globalName)
	{
		if (localSymbol !== window[globalName])
		{
			if (window[globalName])
			{
				this.shallowCopy(window[globalName], localSymbol);
			}

			window[globalName] = localSymbol;
		}
	}
};

AppTools.pathAccess.getVar = 1;
AppTools.pathAccess.setVar = 2;
AppTools.pathAccess.setVarIfNotExists = 3;
AppTools.pathAccess.deleteVar = 4;
