var Deferred = (function ()
{
	var $A = function (obj, from){ return Array.prototype.slice.call(obj, from || 0) };

	var Deferred = function (callback, context, beginNow/*=true*/)
	{
		if (this instanceof Deferred)
		{
			this._init(callback, context, beginNow);
		}
		else
		{
			return new Deferred(callback, context, beginNow);
		}
	};

	Deferred.prototype = {

		/**
		 * @protected
		 */
		_init: function (callback, context, beginNow/*=true*/)
		{
			this._success = undefined;
			this._toFire = {};
			this._callback = callback;
			this._context = context;

			if (callback && (beginNow === undefined || beginNow))
			{
				this._begin("ok", []);
			}
		},

		isSuccess: function ()
		{
			return this._success;
		},

		next: function (func, context)
		{
			return this._post("ok", func, context);
		},

		error: function (func, context)
		{
			return this._post("ng", func, context);
		},

		hand: function (next, error, context)
		{
			if (typeof error != "function")
			{
				if (error)
				{
					var handler = next;
					next = function ()
					{
						return handler.apply(this, [true].concat($A(arguments)));
					};
					error = function ()
					{
						return handler.apply(this, [false].concat($A(arguments)));
					};
				}
				else
				{
					error = next;
				}
			}

			var mainDeferred = new Deferred(undefined, undefined, false);

			next = this.next(next, context);
			error = this.error(error, context);

			mainDeferred.connectTo(next);
			mainDeferred.connectTo(error);

			return mainDeferred;
		},

		call: function (/*params*/)
		{
			this._fire("ok", $A(arguments));
		},

		fail: function (/*params*/)
		{
			this._fire("ng", $A(arguments));
		},

		nextFailure: function (/*params*/)
		{
			var args = arguments;
			return this.next(function (){ return Deferred.failure.apply(Deferred, args); });
		},

		doIt: function (fun, context, withArgs/*=true*/)
		{
			withArgs = Object.ifUndef(withArgs, true);
			return this.hand(function ()
			{
				if (withArgs)
				{
					fun = fun.bind.apply(fun, [context || window].concat(Array.slice(arguments)));
				}
				return new Deferred(fun, context).hand(Deferred.success.apply(Deferred, arguments));
			}, function ()
			{
				if (withArgs)
				{
					fun = fun.bind.apply(fun, [context || window].concat(Array.slice(arguments)));
				}
				return new Deferred(fun, context).hand(Deferred.failure.apply(Deferred, arguments));
			});
		},

		connectTo: function (deferred)
		{
			this.nextFrom(deferred);
			this.errorFrom(deferred);
			return this;
		},

		nextFrom: function (deferred)
		{
			deferred.next(function ()
			{
				this.call.apply(this, arguments);
			}, this);

			return this;
		},

		errorFrom: function (deferred)
		{
			deferred.error(function ()
			{
				this.fail.apply(this, arguments);
			}, this);

			return this;
		},

		parallel: function (deferreds)
		{
			var self = this;

			if (arguments.length > 1)
			{
				deferreds = $A(arguments);
			}

			return this.hand(function ()
			{
				var args = $A(arguments);
				args.unshift(true);
				return Deferred.parallel(deferreds, true, true, args);
			}, function ()
			{
				var args = $A(arguments);
				args.unshift(false);
				return Deferred.parallel(deferreds, true, true, args);
			});
		},

		parallelChain: function ()
		{
			if (this._success !== undefined)
			{
				return this;
			}

			if (!this._parallelChains)
			{
				this._parallelChains = [];
				this._parallelDeferred = this.parallel(this._parallelChains);
			}

			var def = new Deferred();
			this._parallelChains.push(function (success)
			{
				var args = $A(arguments, 1);
				if (success)
				{
					def.call.apply(def, args);
				}
				else
				{
					def.fail.apply(def, args);
				}

				return def;
			});
			return def;
		},

		afterParallel: function ()
		{
			return this._parallelDeferred || this;
		},

		earlier: function (deferreds)
		{
			var self = this;

			if (arguments.length > 1)
			{
				deferreds = $A(arguments);
			}

			return this._post("ok", function ()
			{
				return Deferred.earlier(deferreds);
			});
		},

		log: function (text/*=undefined*/)
		{
			return this.hand(function (param)
			{
				var toLog = text !== undefined ? [text] : $A(arguments);
				console.log.apply(console, toLog);
				return Deferred.Array($A(arguments));
			});
		},

		wait: function (msec)
		{
			return this.hand(function ()
			{
				return Deferred.wait(msec, $A(arguments), "ok")
			}, function ()
			{
				return Deferred.wait(msec, $A(arguments), "ng")
			});
		},

		getParallelContainer: function ()
		{
			var self = this;
			return new Deferred.Container(function ()
			{
				return this.parallel.apply(this, arguments);
			});
		},

		getEarlierContainer: function ()
		{
			var self = this;
			return new Deferred.Container(function ()
			{
				return this.earlier.apply(this, arguments);
			});
		},

		syncResult: function ()
		{
			var result;
			this.next(function (x){ result = x });

			return result;
		},

		/**
		 * @protected
		 */
		_post: function (type, func, context/*=undefined*/)
		{
			var d = new Deferred(func, context, false);
			this._postChain(type, d);
			return d;
		},

		/**
		 * @protected
		 */
		_postChain: function (type, chain)
		{
			var field = this._fieldByType(type);
			var d = this[field] = chain;

			this._fireSaved(type);

			if (this._innerChain)
			{
				this._innerChain._postChain(type, d);
				//this._innerChain[field] = d;
				//this._innerChain._fireSaved(type);
			}
		},

		/**
		 * @protected
		 */
		_fire: function (type, params)
		{
			this._success = type == "ok";

			var field = this._fieldByType(type);
			if (this[field])
			{
				this[field]._begin(type, params);
			}
			else
			{
				this._toFire[type] = params;
			}
		},

		/**
		 * @protected
		 */
		_begin: function (type, params)
		{
			var def = typeof this._callback == "function"
				? this._callback.apply(this._context, params)
				: this._callback;

			if (def instanceof Deferred)
			{
				//def._next = this._next;
				//def._error = this._error;

				this._innerChain = def;
				if (this._next)
				{
					def._postChain("ok", this._next);
				}
				if (this._error)
				{
					def._postChain("ng", this._error);
				}
			}
			else
			{
				this._fire(type, def instanceof Deferred.Array ? def._arr : [def]);
			}
		},

		/**
		 * @protected
		 */
		_fireSaved: function (type)
		{
			if (this._toFire[type])
			{
				this[this._fieldByType(type)]._begin(type, this._toFire[type]);
			}
		},

		/**
		 * @protected
		 */
		_fieldByType: function (type)
		{
			return type == "ok" ? "_next" : "_error";
		}

	};

	Deferred._resolveArguments = function (deferreds, args)
	{
		var resolved = deferreds.constructor == Array ? [] : {};

		for (var key in deferreds)
		{
			if (deferreds.hasOwnProperty(key))
			{
				if (deferreds.hasOwnProperty(key) && typeof deferreds[key] == "function")
				{
					resolved[key] = deferreds[key].apply(window, args);
				}
				else if (!(deferreds[key] instanceof Deferred))
				{
					resolved[key] = Deferred.success(deferreds[key]);
				}
				else
				{
					resolved[key] = deferreds[key];
				}
			}
		}

		return resolved;
	};

	Deferred.Array = function (arr)
	{
		if (this instanceof Deferred.Array)
		{
			this._arr = arr;
		}
		else
		{
			return new Deferred.Array(arr);
		}
	};

	/**
	 * @return {Deferred}
	 */
	Deferred.success = function (/*params*/)
	{
		var d = new Deferred();
		d.call.apply(d, arguments);
		return d;
	};

	Deferred.failure = function (/*params*/)
	{
		var d = new Deferred();
		d.fail.apply(d, arguments);
		return d;
	};

	Deferred.hand = function (callback, errback, callbackContext, errbackContext)
	{
		var d = new Deferred();
		d.next(callback, callbackContext);
		d.error(errback, errbackContext);

		return d;
	};

	Deferred.parallel = function (deferreds, onlyOneResult, expandResults, args)
	{
		onlyOneResult === undefined && (onlyOneResult = true);
		expandResults === undefined && (expandResults = true);

		var mainDeferred = new Deferred();
		var count = 0;
		deferreds = Deferred._resolveArguments(deferreds, args);

		for (var key in deferreds)
		{
			if (deferreds.hasOwnProperty(key)/* && (!(deferreds[key] instanceof Deferred) || deferreds[key]._success === undefined)*/)
			{
				++count;
			}
		}

		if (count == 0)
		{
			return Deferred.success({});
		}

		var results = expandResults ? [] : {};
		var fails = [];

		var tryToContinue = function ()
		{
			--count;
			if (count == 0)
			{
				if (fails.length == 0)
				{
					if (expandResults)
					{
						mainDeferred.call.apply(mainDeferred, results);
					}
					else
					{
						mainDeferred.call(results);
					}
				}
				else
				{
					mainDeferred.fail(results, fails);
				}
			}
		};

		var getPostCallback = function (type, key)
		{
			return function (result)
			{
				result = onlyOneResult ? result : $A(arguments);

				results[key] = result;

				if (type == "ng")
				{
					fails.push(key);
				}
				tryToContinue();
			}
		};

		for (key in deferreds)
		{
			if (deferreds.hasOwnProperty(key))
			{
				deferreds[key].hand(
					getPostCallback("ok", key),
					getPostCallback("ng", key)
				);
			}
		}

		return mainDeferred;
	};

	Deferred.earlier = function (deferreds)
	{
		if (arguments.length > 1)
		{
			deferreds = $A(arguments);
		}

		deferreds = Deferred._resolveArguments(deferreds);

		var mainDeferred = new Deferred();
		var done = false;

		var getPostCallback = function (type, key)
		{
			return function ()
			{
				if (done)
				{
					return;
				}

				mainDeferred[type == "ok" ? "call" : "fail"](key, $A(arguments));
				done = true;
			}
		};

		for (var key in deferreds)
		{
			if (deferreds.hasOwnProperty(key))
			{
				deferreds[key].hand(
					getPostCallback("ok", key),
					getPostCallback("ng", key)
				);
			}
		}

		return mainDeferred;
	};

	Deferred.wait = function (msec/*=0*/, params/*=[]*/, type/*="ok"*/)
	{
		var deferred = new Deferred();
		setTimeout(function ()
		{
			var f = type == "ok" || type === undefined ? deferred.call : deferred.fail;
			f.apply(deferred, params || []);
		}, msec || 0);
		return deferred;
	};

	Deferred.deferrize = function (func, context)
	{
		return function ()
		{
			var def = new Deferred();
			func.apply(context || window, $A(arguments).concat([function ()
			{
				def.call.apply(def, arguments);
			}]));
			return def;
		}
	};

	//Deferred parallel
	Deferred.Container = function (resolver)
	{
		this._container = {};
		this._lastIndex = -1;
		this.resolve = function ()
		{
			return resolver(this._container);
		};
	};

	Deferred.Container.prototype = {
		add: function (key, func, context)
		{
			if (key in this._container)
			{
				throw "key already in container";
			}
			this._container[key] = func.call(context || window);
		},

		//	deferred: function(key)
		//	{
		//		if (key === undefined)
		//			while((key = ++this._lastIndex) in this._container){}
		//
		//		if (key in this._container)
		//			throw "key already in container";
		//
		//		return this._container[key] = new Deferred();
		//	},

		callback: function (key)
		{
			if (key === undefined)
			{
				while ((key = ++this._lastIndex) in this._container)
				{}
			}

			if (key in this._container)
			{
				throw "key already in container";
			}

			var deferred = new Deferred();
			this._container[key] = deferred;
			return function ()
			{
				deferred.call.apply(deferred, arguments);
			};
		},

		errback: function (key)
		{
			if (key === undefined)
			{
				key = this._lastIndex;
			}

			if (!(key in this._container))
			{
				throw "key is not present in container";
			}

			var deferred = this._container[key];
			return function ()
			{
				deferred.fail.apply(deferred, arguments);
			};
		}
	};

	Deferred.parallelContainer = function ()
	{
		return new Deferred.Container(Deferred.parallel);
	};

	Deferred.earlierContainer = function ()
	{
		return new Deferred.Container(Deferred.earlier);
	};

	Deferred.Callback = function ()
	{
		var deferred = new Deferred();
		this.callback = function ()
		{
			deferred.call.apply(deferred, arguments);
		};

		this.errback = function ()
		{
			deferred.fail.apply(deferred, arguments);
		};

		this.deferred = deferred;
	};

	//Recource
	Deferred.Resource = function ()
	{
		var linkDeferred = new Deferred();
		this.getResource = function ()
		{
			if (this.__resource)
			{
				return Deferred.success(this.__resource);
			}
			else
			{
				return linkDeferred.parallelChain();
			}
		};

		this.getRawResource = function ()
		{
			return this.__resource;
		};

		this.init = function (startDeferred)
		{
			startDeferred = startDeferred.next(function (resource)
			{
				this.__resource = resource;
				return resource;
			}, this);
			linkDeferred.connectTo(startDeferred);
		};
	};

	return Deferred;

})();
