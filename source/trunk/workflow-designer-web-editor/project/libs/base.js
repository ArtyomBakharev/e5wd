var goog = {};
goog.require = function(symbol){};
goog.provide = function(symbol){
	goog._lastSymbol = symbol;
	var elements = symbol.split(".");
	var last = window;
	for (var i = 0; i < elements.length; ++i)
	{
		if (last[elements[i]] === undefined)
			last[elements[i]] = {};
		last = last[elements[i]];
	}
};
/**
 * �������� str �� ��������������� ����������
 * @example var obj = {foo:"bar"}; console.log(obj[goog.sym("foo")]);
 */
goog.sym = function(str){
	if (str.substr(0, 1) == ".")
		return str.substr(1);
	return str;
};
/**
 * �������� ������� � ���� �� �������������� �� ����� ����������
 * @expamle goog.addExports("(DoIt)") MyAPI = {DoIt:function(){}, /**
  * @protected
  * _foo:function(){}}; MyAPI.DoIt(); MyAPI._foo();
 * ���������� �� MyAPI = {DoIt:function(){},a:functionj(){}}; MyAPI.DoIt(); MyAPI.a();
 * @param str string �������� ���������� ���������. �������� ���� ��� ��� ������. 
 * ������, �������������� ������ - ������� ����� ����� ��������. ������ - ������� �������.
 */
goog.addExports = function(str){};
