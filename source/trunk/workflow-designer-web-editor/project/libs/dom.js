if (document.getElementsByClassName == undefined)
{
	document.getElementsByClassName = function (className)
	{
		var hasClassName = new RegExp("(?:^|\\s)" + className + "(?:$|\\s)");
		var allElements = document.getElementsByTagName("*");
		var results = [];

		var element;
		for (var i = 0; (element = allElements[i]) != null; i++)
		{
			var elementClass = element.className;
			if (elementClass && elementClass.indexOf(className) != -1 && hasClassName.test(elementClass))
			{
				results.push(element);
			}
		}

		return results;
	}
}

var domUtil = {
	addClass: function (ele, cls)
	{
		if (!this.hasClass(ele, cls))
		{
			ele.className += " " + cls;
		}
	},
	getStyle: function (el, style)
	{
		var value = el.style[this.__toCamelCase(style)];

		if (!value)
		{
			if (document.defaultView)
			{
				value = document.defaultView.
					getComputedStyle(el, "").getPropertyValue(style);
			}

			else if (el.currentStyle)
			{
				value = el.currentStyle[this.__toCamelCase(style)];
			}
		}

		return value;
	},
	hasClass: function (ele, cls)
	{
		return ele.className.match(new RegExp('(\\s|^)' + cls + '(\\s|$)'));
	},
	removeClass: function (ele, cls)
	{
		if (Util.hasClass(ele, cls))
		{
			var reg = new RegExp('(\\s|^)' + cls + '(\\s|$)');
			ele.className = ele.className.replace(reg, ' ');
		}
	},
	/**
	 * @param {Element} element
	 */
	removeElement: function (element)
	{
		element.parentNode.removeChild(element);
	},

	__toCamelCase: function (sInput)
	{
		return sInput.split("-").map(function(x, i){ return i == 0 ? x : x.substr(0, 1).toUpperCase() + x.substr(1) });
	}
};
