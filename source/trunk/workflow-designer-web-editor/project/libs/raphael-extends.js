!(function ()
{
	Raphael.st.orderedToBack = function ()
	{
		for (var i = this.length - 1, element; element = this[i--];)
		{
			element.toBack();
		}
	};

	Raphael.st.orderedInsertBefore = function (other)
	{
		for (var i = this.length - 1, element; element = this[i--];)
		{
			element.insertBefore(other);
		}
	};

	Raphael.st.append = function (other)
	{
		other.forEach(function (x){ this.push(x) }, this);
	};

	Raphael.st.addListener = function ()
	{
		var args = arguments;
		this.forEach(function (x){ x.addListener.apply(x, args) });
	};

	Raphael.st.addDragListener = function ()
	{
		var args = arguments;
		this.forEach(function (x){ x.addDragListener.apply(x, args) });
	};

	if (Raphael.svg)
	{
		Raphael.el.attr = function (name, value)
		{
			if (typeof name == "object")
			{
				for (var attrName in name)
				{
					if (name.hasOwnProperty(attrName))
					{
						var attrValue = name[attrName];
						this.attr(attrName, attrValue);
					}
				}
				return this;
			}

			switch (name)
			{
				case "r":
					if (this.type == "rect")
					{
						return this.attr({rx: value, ry: value});
					}
					break;

				case "text":
					this.node.childNodes[0].textContent = value;
					return this;

				case "path":
					name = "d";
					break;

				case "src":
					name = "href";
					break;

				case "font-family":
				case "font-size":
				case "font-weight":
					this.fontAttrs || (this.fontAttrs = { "font-family": "Arial" });
					this.fontAttrs[name] = value;
					var font = (this.fontAttrs["font-weight"] || "") + " " + (this.fontAttrs["font-size"] ? this.fontAttrs["font-size"] + "px" : "") + " " + (this.fontAttrs["font-family"] ? '"' + this.fontAttrs["font-family"] + '"' : "");
					this.node.setAttribute("font", font);
					this.node.style["font"] = font;

				case "opacity":
				case "text-anchor":
					this.node.style[name] = value;
					break;
			}

			this.node.setAttribute(name, value);

			return this;
		};

		Raphael.el.getBBox = function()
		{
			return this.node.getBBox();
		}
	}

	function createObserver()
	{
		function observer()
		{
			var args = arguments;
			observer.__listeners.forEach(function (x){ x[0].apply(x[1] || window, args) });
		}

		observer.__listeners = [];

		return observer;
	}

	Raphael.el.addListener = function (event, listener, context)
	{
		if (!this.__observers)
		{
			this.__observers = {};
		}

		var observer = this.__observers[event];
		if (!observer)
		{
			observer = createObserver();
			this[event](observer);
		}

		observer.__listeners.push([listener, context]);
	};

	Raphael.el.addDragListener = function (move, start, stop, context)
	{
		if (!this.__observers)
		{
			this.__observers = {};
		}

		if (!this.__observers["dragStart"])
		{
			var dragStart = this.__observers["dragStart"] = createObserver();
			var dragStop = this.__observers["dragStop"] = createObserver();
			var dragMove = this.__observers["dragMove"] = createObserver();
			this["drag"](dragMove, dragStart, dragStop);
		}

		start && this.__observers["dragStart"].__listeners.push([start, context]);
		stop && this.__observers["dragStop"].__listeners.push([stop, context]);
		move && this.__observers["dragMove"].__listeners.push([move, context]);
	};

//	Raphael.el.addHoverListener = function (enter, leave, context)
//	{
//		if (!this.__observers)
//		{
//			this.__observers = {};
//		}
//
//		if (!this.__observers["hoverEnter"])
//		{
//			var dragStart = this.__observers["dragStart"] = createObserver();
//			var dragStop = this.__observers["dragStop"] = createObserver();
//			var dragMove = this.__observers["dragMove"] = createObserver();
//			this["drag"](dragMove, dragStart, dragStop);
//		}
//
//		this.__observers["dragStart"].__listeners.push([start, context]);
//		this.__observers["dragStop"].__listeners.push([stop, context]);
//		this.__observers["dragMove"].__listeners.push([move, context]);
//	};
})();
