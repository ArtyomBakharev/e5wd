package ru.efive.workflow.designer.web.rest;

import com.sun.xml.bind.IDResolver;
import org.apache.log4j.Logger;
import org.xml.sax.SAXException;
import ru.efive.workflow.designer.interfaces.*;
import ru.efive.workflow.designer.interfaces.Process;
import ru.efive.workflow.designer.model.*;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import java.util.concurrent.Callable;


/**
 * EntityIDResolver resolve object from Id
 *
 * @author Pavel Shashok
 *
 */
@Stateless
@Local
@SuppressWarnings("rawtypes")
public class EntityIDResolver extends IDResolver {
	protected static Logger log = Logger.getLogger(ProcessResource.class);
	
	@EJB
	private RepositoryManager repoMng;

    @EJB
    private SessionManager sessionManagerBean;

	@Override
	public void bind(final String id, final Object value) throws SAXException {
	}

	@Override
	public Callable<?> resolve(final String id, final Class type)
			throws SAXException {
		return new Callable<Object>() {
			public Object call() throws Exception {
                boolean cid = false;

                log.warn("$$$$$$ EntityIDResolver.resolve type = " + type + " id = " + id);
                if(id.length() > 3) {
                    if("cid".equals(id.substring(0,3))) {
                        cid = true;  
                    }
                }


				if (type == ProcessXml.class) {
					Process target = repoMng.getProcessRepository().findOne(id);

					if(target == null)
						return null;

					ProcessXml temp;
					if(target.getClass() != ProcessXml.class){
						temp = new ProcessXml();
						temp.fromInterface(target);
					}
					else
						temp = (ProcessXml) target;

					return temp;
				}
				else if( type == NoStatusActionXml.class)
				{
                    if(cid){
                        NoStatusActionXml ret = new NoStatusActionXml();
                        ret.setId(id);
                        return ret;
                    }
                    
					String prId = repoMng.getProcessByAction(id);
                    if(prId == null)   {
                        log.error("$$$$$$$$$$ getProcessByAction == null, id = " + id);
                        return null;
                    }

					NoStatusAction target = (NoStatusAction) repoMng.getActionRepository(prId).findOne(id);

					if(target == null)
						return null;

					NoStatusActionXml temp;
					if(target.getClass() != NoStatusActionXml.class){
						temp = new NoStatusActionXml();
						temp.fromInterface(target);
					}
					else
						temp = (NoStatusActionXml) target;
					return temp;
				}
				else if(type == StatusChangeActionXml.class)
				{
                    if(cid){
                        StatusChangeActionXml ret = new StatusChangeActionXml();
                        ret.setId(id);
                        return ret;
                    }

					String prId = repoMng.getProcessByAction(id);
					if(prId == null)
						return null;

					StatusChangeAction target = (StatusChangeAction) repoMng.getActionRepository(prId).findOne(id);

					if(target == null)
						return null;

					StatusChangeActionXml temp;
					if(target.getClass() != StatusChangeActionXml.class){
						temp = new StatusChangeActionXml();
						temp.fromInterface(target);
					}
					else
						temp = (StatusChangeActionXml) target;
					return temp;
				}
				else if( type == StatusXml.class)
				{
                    if(cid){
                        StatusXml ret = new StatusXml();
                        ret.setId(id);
                        return ret;
                    }

					String prId = repoMng.getProcessByStatus(id);
                    if(prId == null)   {
                        log.error("$$$$$$$$$$ getProcessByStatus == null, id = " + id);
                        return null;
                    }

					Status target = (Status) repoMng.getStatusRepository(prId).findOne(id);

					if(target == null)
						return null;

					StatusXml temp;
					if(target.getClass() != StatusXml.class){
						temp = new StatusXml();
						temp.fromInterface(target);
					}
					else
						temp = (StatusXml) target;
					return temp;
				}

                if(cid){
                    ActivityXml ret = new ActivityXml();
                    ret.setId(id);
                    return ret;
                }

				String prId = repoMng.getProcessByActivity(id);
				if(prId == null)   {
                    log.error("$$$$$$$$$$ getProcessByActivity == null, id = " + id);
					return null;
                }

				Activity target = (Activity) repoMng.getActivityRepository(prId).findOne(id);
				if(target == null)
					return null;
				if(target instanceof SendMailActivityXml )
					return (SendMailActivityXml)target;
				else if(target instanceof InvokeMethodActivityXml )
					return (InvokeMethodActivityXml)target;
				else if(target instanceof ParametrizedPropertyLocalActivityXml )
					return (ParametrizedPropertyLocalActivityXml)target;
				else if(target instanceof SetPropertyActivityXml )
					return (SetPropertyActivityXml)target;
				else if(target instanceof ActivityXml )
					return (ActivityXml)target;
				else if(target instanceof SendMailActivity ){
					SendMailActivityXml st = new SendMailActivityXml();
					st.fromInterface(target);
					return st;
				}
				else if(target instanceof InvokeMethodActivity ){
					InvokeMethodActivityXml st = new InvokeMethodActivityXml();
					st.fromInterface(target);
					return st;
				}
				else if(target instanceof ParametrizedPropertyLocalActivity ){
					ParametrizedPropertyLocalActivityXml st = new ParametrizedPropertyLocalActivityXml();
					st.fromInterface(target);
					return st;
				}
				else if(target instanceof SetPropertyActivity ){
					SetPropertyActivityXml st = new SetPropertyActivityXml();
					st.fromInterface(target);
					return st;
				}
				else if(target instanceof Activity ){
					ActivityXml st = new ActivityXml();
					st.fromInterface(target);
					return st;
				}
				return null;
				//return repoMng.getActivityRepository(null).findOne(id); // TODO: king of hack
			}
		};
	}

}