package ru.efive.workflow.designer.web.rest;

import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.*;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.log4j.Logger;

import com.sun.xml.bind.IDResolver;

/**
 * JAXBProvider
 *
 * @author Pavel Shashok
 *
 */
@Provider
@Stateless
public class JAXBProvider implements MessageBodyReader<Object> {
    protected static Logger log = Logger.getLogger(ProcessResource.class);
	
	@EJB 
	EntityIDResolver idresolver;

	public boolean isReadable(Class<?> type, Type genericType,
			Annotation[] annotations, MediaType mediaType) {
		return type.isAnnotationPresent(XmlRootElement.class);
	}

	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Object readFrom(Class<Object> type, Type genericType,
			Annotation[] annotations, MediaType mediaType,
			MultivaluedMap<String, String> httpHeaders, InputStream entityStream)
			throws IOException {
		try {
            log.warn("readFrom called type = " + type);

			JAXBContext jaxb = JAXBContext.newInstance(type);

			Unmarshaller unmarshaller = jaxb.createUnmarshaller();
			unmarshaller.setProperty(IDResolver.class.getName(),
					idresolver);
			
			Object obj = unmarshaller.unmarshal(entityStream);

			/*if (obj instanceof JAXBElement)
				obj = ((JAXBElement<?>) obj).getValue();
			if (!type.isInstance(obj))
				throw new WebApplicationException(
						HttpURLConnection.HTTP_BAD_REQUEST);*/
            log.warn("return obj");
			return obj;
		} catch (WebApplicationException e) {
			throw e;
		} catch (Throwable e) {
			throw new WebApplicationException(e);
		}
	}

	public long getSize(Object arg0) {
		return -1;
	}

	public boolean isWriteable(Class<?> type, Type genericType,
			Annotation[] annotations, MediaType mediaType) {
		return type.isAnnotationPresent(XmlRootElement.class);
	}

}