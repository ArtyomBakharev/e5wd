package ru.efive.workflow.designer.web.rest;


import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import ru.efive.workflow.designer.interfaces.*;
import ru.efive.workflow.designer.interfaces.Process;
import ru.efive.workflow.designer.model.*;
import ru.efive.workflow.designer.repository.*;
import ru.efive.workflow.designer.rest.BatchContainer;
import ru.efive.workflow.designer.rest.BatchResponse;
import ru.efive.workflow.designer.rest.Operation;
import ru.efive.workflow.designer.rest.ResponseItem;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;


/**
 * ProcessResource resful resource for editor
 *
 * @author Pavel Shashok
 *
 */
@Stateless
@Path(value = "/processes")
@SuppressWarnings("unchecked")
public class ProcessResource {
	protected static Logger log = Logger.getLogger(ProcessResource.class);
	
	@EJB
	private RepositoryManager repoMng;

    @EJB
    private SessionManager sessionManagerBean;

    @GET
    @Path(value = "/log")
    public String logOn() {
        BasicConfigurator.configure();
        return "log on";
    }


    @GET
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public JaxbList<ProcessXml> getProcesses(@QueryParam(value = "name") String name, @QueryParam(value = "data_type") String data_type) {
        log.warn("$$$$ JaxbList<ProcessXml> getProcesses()");
		JaxbList<ProcessXml> p = new JaxbList<ProcessXml>();
		List<ProcessXml> la = new ArrayList<ProcessXml>();

        Iterable<? extends Process> r;
        if(name != null)      {
            r = repoMng.getProcessRepository().findAllByName(null, name, 0, (int)repoMng.getProcessRepository().count(), null ,false);
        }  else if(data_type != null){
            ProcessXml t = new ProcessXml();
            t.setDataType(data_type);
            r = repoMng.getProcessRepository().findAllByName(t, null, 0, (int)repoMng.getProcessRepository().count(), null ,false);
        }  else{
            r = repoMng.getProcessRepository().findAll();
        }

		for (Process t : r){
            log.warn("$$$$ in loop t =  " +  t + "t.getclass() = " + t.getClass());
			if(t instanceof ProcessXml)   {
                log.warn("t instanceof ProcessXml");
				la.add((ProcessXml) t);
            }
			else if(t instanceof Process){
				ProcessXml pr = new ProcessXml();	
				pr.fromInterface(t);
                log.warn("t instanceof Process fromInterface = " + pr);
				la.add(pr);
			} else{
                log.error("$$$$   Unknown Process type");
            }

		}
		p.setItems(la);

		return p;
	}

	@GET
	@Path(value = "/{id}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public ProcessXml getProcess(@PathParam(value = "id") String id) {
        log.warn("$$$$ getProcess called id =  " + id);
		Process t =  repoMng.getProcessRepository().findOne(id);

		if(t instanceof ProcessXml )      {
            log.warn("$$$$ t instanceof ProcessXml");
			return (ProcessXml) t;
        }
		else if(t instanceof Process ){
			ProcessXml pr = new ProcessXml();	
			pr.fromInterface(t);
            log.warn("$$$$ t instanceof Process fromInterface = " + pr);
			return pr;
		}  else{
            log.error("$$$$   Unknown Process type");
        }
        return null;
	}

	@POST
	@Path(value = "/{id}")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response createProces(ProcessXml process) {
        log.warn("$$$$ createProces process = " + process);
		ProcessRepository<ProcessXml> processes = (ProcessRepository<ProcessXml>) repoMng.getProcessRepository();
        ProcessRepository<ProcessXml> processesXml = (ProcessRepository<ProcessXml>) repoMng.getProcessRepositoryXml();
        Process ret = processes.save(process);
        if(process.getRepository()==0) processesXml.save((ProcessXml)ret);
        if(ret != null)            {
            log.warn("ret = " + ret);
            return Response.status(201).entity(ret.getId()).build();
        } else{
            log.error("$$$$$$$$ ret is null");
            return Response.status(400).build();
        }
	}
	
	@PUT
	@Path(value = "/{id}")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response updateProces(ProcessXml process) {
        try{
            log.warn("$$$$ updateProces process = " + process);
            ProcessRepository<ProcessXml> processes = (ProcessRepository<ProcessXml>) repoMng.getProcessRepository();
            ProcessRepository<ProcessXml> processesXml = (ProcessRepository<ProcessXml>) repoMng.getProcessRepositoryXml();

            if (process.getId() == null)    {
                return Response.status(400).build();
            }
            if ( processes.findOne(process.getId()) == null)    {
                return Response.status(451).build();
            }
            Process ret = processes.save(process);
            if(process.getRepository()==0) processesXml.save((ProcessXml)ret);

            if(ret != null)            {
                log.warn("ret = " + ret);
                return Response.status(200).entity(ret.getId()).build();
            } else{
                log.error("$$$$$$$$ ret is null");
                return Response.status(400).build();
            }
        }   catch (Exception e){
            log.error("$$$$$$$$ Exception in updateProces " + e );
            return Response.status(455).build();
        }
	}

    @DELETE
    @Path(value = "/{id}")
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response deleteProcess(@PathParam(value = "id") String processId) {
        try{
            log.warn("$$$$ deleteProcess processId = " + processId);
            ProcessRepository<ProcessXml> processes = (ProcessRepository<ProcessXml>) repoMng.getProcessRepository();
            ProcessRepository<ProcessXml> processesXml = (ProcessRepository<ProcessXml>) repoMng.getProcessRepositoryXml();

            if (processId == null)   {
                return Response.status(400).build();
            }
            Process p = processes.findOne(processId);
            if (p == null)   {
                return Response.status(451).build();
            }
            if(p.getRepository()==0) {

                ActivityRepository<? extends Activity> rActivity = repoMng.getActivityRepository(processId);
                for (Activity t : rActivity.findAll(p, 0, (int) rActivity.count(), null, false)){
                    rActivity.delete(t.getId());
                }

                StatusRepository<? extends Status> rStatus = repoMng.getStatusRepository(processId);
                for (Status t : rStatus.findAll(p, 0, (int) rStatus.count(), null, false)){
                    rStatus.delete(t.getId());
                }

                ActionRepository<? extends Object> rAction = repoMng.getActionRepository(processId);
                for (Action t : rAction.findAll(p, 0, (int) rAction.count(), null, false)){
                    rAction.delete(t.getId());
                }

                if(processesXml.findOne(processId) != null)    {
                    processesXml.delete(processId);
                }
            }
            log.error("$$$$$$$$ processes.delete(processId);");
            processes.delete(processId);
            sessionManagerBean.onProcessRemove(processId);

            return Response.status(200).build();

        }   catch (Exception e){
            log.error("$$$$$$$$ Exception in deleteProcess " + e );
            return Response.status(455).build();
        }
    }

	
	private <T extends BaseEntityXml> Response newEntry(T entity,String session) {
        try{
            log.warn("$$$ newEntry entity = " + entity + " entity.getProcess() = " + entity.getProcess());
            if(!sessionManagerBean.canAccess(entity.getProcess().getId(),session))  {
                return Response.status(450).build();
            }
            if (entity.getId() != null)  {
                return Response.status(400).build();
            }
            if(entity.isCorrect() != true)   {
                log.error("$$$$  incorrect data ");
                return Response.status(400).build();
            }

            T ret = null;
            log.error("$$$$ entity.getClass( " + entity.getClass());
            if(entity instanceof StatusXml ){
                log.warn("entity instanceof StatusXml");
                StatusRepository<StatusXml> statuses = (StatusRepository<StatusXml>) repoMng.getStatusRepository(entity.getProcess().getId());
                ret = (T)statuses.save((StatusXml)entity);
            }   else if(entity instanceof ActionXml){
                log.warn("entity instanceof ActionXml");
                ActionRepository<ActionXml> actions = (ActionRepository<ActionXml>) repoMng.getActionRepository(entity.getProcess().getId());
                ret = (T)actions.save((ActionXml)entity);
            }   else if(entity instanceof ActivityXml){
                ActivityRepository<ActivityXml> activities = (ActivityRepository<ActivityXml>) repoMng.getActivityRepository(entity.getProcess().getId());
                ret = (T)activities.save((ActivityXml)entity);
            }  else{
                log.error("$$$$$$$$ bad type");
            }
            if(ret != null)            {
                log.warn("$$$ ret = " + ret);
                return Response.status(201).entity(ret.getId()).build();
            } else{
                log.error("$$$$$$$$ ret is null");
                return Response.status(400).build();
            }
        }   catch (Exception e){
            log.error("$$$$$$$$ Exception in newEntry " + e );
            return Response.status(455).entity(e.getMessage()).build();
        }

	}

	private <T extends BaseEntityXml> Response updateEntry(T entity,String session) {
        try{
            log.warn("$$$ updateEntry " + "entity = " + entity.getClass() + " " + entity);
            if(!sessionManagerBean.canAccess(entity.getProcess().getId(),session))  {
                return Response.status(450).build();
            }
            if (entity.getId() == null )      {
                return Response.status(400).build();
            }
            if(entity.isCorrect() != true)   {
                log.error("$$$$  incorrect data ");
                return Response.status(400).build();
            }

            T ret = null;
            if(entity instanceof StatusXml ){
                log.warn("entity instanceof StatusXml");
                StatusRepository<StatusXml> statuses = (StatusRepository<StatusXml>) repoMng.getStatusRepository(entity.getProcess().getId());
                if (statuses.findOne(entity.getId()) == null)      {
                    return Response.status(451).build();
                }
                ret = (T)statuses.save((StatusXml)entity);
            }   else if(entity instanceof ActionXml){
                ActionRepository<ActionXml> actions = (ActionRepository<ActionXml>) repoMng.getActionRepository(entity.getProcess().getId());
                if (actions.findOne(entity.getId()) == null)      {
                    return Response.status(451).build();
                }
                ret = (T)actions.save((ActionXml)entity);
            }   else if(entity instanceof ActivityXml){
                ActivityRepository<ActivityXml> activities = (ActivityRepository<ActivityXml>) repoMng.getActivityRepository(entity.getProcess().getId());
                if (activities.findOne(entity.getId()) == null)      {
                    return Response.status(451).build();
                }
                ret = (T)activities.save((ActivityXml)entity);
            }  else{
                log.error("$$$$$$$$ bad type");
            }

            if(ret != null)            {
                log.warn("$$$ ret = " + ret);
                return Response.status(200).entity(ret.getId()).build();
            } else{
                log.error("$$$$$$$$ ret is null");
                return Response.status(400).build();
            }
        }   catch (Exception e){
            log.error("$$$$$$$$ Exception in updateEntry " + e );
            return Response.status(455).build();
        }

	}

	private <T extends BaseEntity> Response deleteEntryById(
            Repository<String, T> r, String id, String session) {
        try{
            T t = r.findOne(id);

            if (t == null)      {
                return Response.status(451).build();
            }
            if(t.getProcess() != null)  {
                if(!sessionManagerBean.canAccess(t.getProcess().getId(),session))  {
                    return Response.status(450).build();
                }
            }
            log.warn("deleteEntryById " + "Repository = " + r.getClass() + " id = " + id);
            if (id == null || t == null)
                return Response.status(400).build();
            r.delete(id);
            return Response.status(200).build();
        }   catch (Exception e){
                log.error("$$$$$$$$ Exception in deleteEntryById " + e );
            return Response.status(455).build();
        }
	}

    private <T extends BaseEntity> Response deleteEntry(T entity,String session)  {
        try{
            if(entity instanceof StatusXml ){
                log.warn("entity instanceof StatusXml");
                StatusRepository<StatusXml> statuses = (StatusRepository<StatusXml>) repoMng.getStatusRepository(entity.getProcess().getId());
                return deleteEntryById(statuses, entity.getId(), session);
            }   else if(entity instanceof ActionXml){
                ActionRepository<ActionXml> actions = (ActionRepository<ActionXml>) repoMng.getActionRepository(entity.getProcess().getId());
                return deleteEntryById(actions, entity.getId(), session);
            }   else if(entity instanceof ActivityXml){
                ActivityRepository<ActivityXml> activities = (ActivityRepository<ActivityXml>) repoMng.getActivityRepository(entity.getProcess().getId());
                return deleteEntryById(activities, entity.getId(), session);
            }  else{
                log.error("$$$$$$$$ bad type");
                return Response.status(402).build();
            }

        }   catch (Exception e){
            log.error("$$$$$$$$ Exception in deleteEntry " + e );
            return Response.status(455).build();
        }
    }

    @GET
    @Path(value = "/{id}/batch")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public BatchContainer getOperators(@PathParam(value = "id") String id) {
        log.warn("$$$$ getOperators called id =  " + id);

        BatchContainer p = new BatchContainer();
        List<Operation> la = new ArrayList<Operation>();
        Operation t = new Operation();
        StatusXml x = new StatusXml();
        x.setId("123");
        x.setName("test_name");
        x.setDataType("dt");
        t.setElement(x);
        t.setNumber(10);
        t.setMethod(Operation.Method.POST);
        la.add(t);
        Operation t1 = new Operation();
        t1.setNumber(1);
        la.add( t1 );
        p.setItems(la);
        return p;
    }

    @POST
    @Path(value = "/{id}/batch")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response postOperators(BatchContainer op,@PathParam(value = "id") String id, @QueryParam("session") String session) {
        //log.warn("$$$$ getOperators called id =  " + id);
        BatchResponse ret = new BatchResponse();
        List<ResponseItem> rel = new ArrayList<ResponseItem>();
        Map<String,String> cidId = new HashMap<String, String>();

        try{
            Process pr = repoMng.getProcessRepository().findOne(id);
            if(pr == null)      {
                return Response.status(451).build();
            }

            ActionRepository<ActionXml> actions = (ActionRepository<ActionXml>) repoMng.getActionRepository(id);
            StatusRepository<StatusXml> statuses = (StatusRepository<StatusXml>) repoMng.getStatusRepository(id);
            ActivityRepository<ActivityXml> activities = (ActivityRepository<ActivityXml>) repoMng.getActivityRepository(id);

            log.warn("$$$$$$$$  op.get() = " + op.get());
            
            for(Operation o : op.getItems()) {
                ResponseItem tempResponse = new ResponseItem();
                tempResponse.setNumber( o.getNumber() );
                Response tempR = null;

                log.warn("$$$$$$$$ o.getMethod() = " + o.getMethod());
                log.warn("$$$$$$$$ o.getElement() = " + o.getElement());

                if( o.getElement() instanceof ProcessXml )  {
                    if(o.getMethod() == Operation.Method.PUT) {
                        ProcessXml pro = (ProcessXml)o.getElement();
                        String realId = cidId.get( pro.getInitialStatusId() );
                        if(realId != null)
                            pro.setInitialStatusId(realId);
                        tempR = updateProces( pro );
                    }
                } else {
                    BaseEntityXml elem = (BaseEntityXml)o.getElement();
                    if(o.getMethod() != Operation.Method.DELETE)  {
                        if( elem instanceof StatusChangeActionXml )  {
                            StatusChangeActionXml t = (StatusChangeActionXml)elem;
                            if( t.getFromStatus().getProcess() == null){
                                t.setFromStatus( statuses.findOne( cidId.get( t.getFromStatus().getId() ) ) );
                            }

                            if( t.getToStatus().getProcess() == null){
                                t.setToStatus( statuses.findOne( cidId.get( t.getToStatus().getId() ) ) );
                            }


                        }
                        if( elem instanceof ActionXml )   {
                            ActionXml t = (ActionXml)elem;
                            List<ActivityXml> act = new ArrayList<ActivityXml>();

                            act = t.getPostActivities();
                            for(int i=0; i < act.size(); i++){
                                ActivityXml real = activities.findOne( cidId.get( act.get(i).getId() ) );
                                if(real != null){
                                    act.set(i, real);
                                }
                            }
                            t.setPostActivities(act);


                            act = t.getPreActivities();
                            for(int i=0; i < act.size(); i++){
                                ActivityXml real = activities.findOne( cidId.get( act.get(i).getId() ) );
                                if(real != null){
                                    act.set(i, real);
                                }
                            }
                            t.setPreActivities(act);

                            act = t.getLocalActivities();
                            for(int i=0; i < act.size(); i++){
                                ActivityXml real = activities.findOne( cidId.get( act.get(i).getId() ) );
                                if(real != null){
                                    act.set(i, real);
                                }
                            }
                            t.setLocalActivities(act);

                        }
                        if( elem instanceof StatusXml )   {
                            StatusXml t = (StatusXml)elem;
                            List<ActivityXml> act = new ArrayList<ActivityXml>();

                            act = t.getPostActivities();
                            for(int i=0; i < act.size(); i++){
                                ActivityXml real = activities.findOne( cidId.get( act.get(i).getId() ) );
                                if(real != null){
                                    act.set(i, real);
                                }
                            }
                            t.setPostActivities(act);


                            act = t.getPreActivities();
                            for(int i=0; i < act.size(); i++){
                                ActivityXml real = activities.findOne( cidId.get( act.get(i).getId() ) );
                                if(real != null){
                                    act.set(i, real);
                                }
                            }
                            t.setPreActivities(act);

                            act = t.getLocalActivities();
                            for(int i=0; i < act.size(); i++){
                                ActivityXml real = activities.findOne( cidId.get( act.get(i).getId() ) );
                                if(real != null){
                                    act.set(i, real);
                                }
                            }
                            t.setLocalActivities(act);
                        }
                    }

                    if(o.getMethod() == Operation.Method.POST) {
                            String cid = elem.getId();
                            elem.setId(null);
                            tempR = newEntry(elem, session);
                            String realId = (String)tempR.getEntity();
                            if(cid != null && realId != null) {
                                log.warn("cidId.put realId = " + realId + " cid = " + cid);
                                cidId.put(cid, realId);
                            }


                    }  else {
                        String realId = cidId.get(elem.getId());
                        if(realId != null){
                            log.warn("$$$$$$$$ getIdByCid realId = " + realId + " elem.getId() = " + elem.getId());
                            elem.setId(realId);
                        }

                        if(o.getMethod() == Operation.Method.PUT){
                                tempR = updateEntry(elem, session);
                        }  else if(o.getMethod() == Operation.Method.DELETE){
                                elem.setProcess(pr);
                                tempR = deleteEntry(elem, session);
                        }

                    }
                }


                log.warn("$$$$$$$$  tempR = " + tempR.getEntity());

                if(tempR != null){
                    tempResponse.setStatus(tempR.getStatus());
                    tempResponse.setRetId((String)tempR.getEntity());
                }  else {
                    tempResponse.setStatus(452);
                    tempResponse.setRetId(null);
                }
                rel.add(tempResponse);

            }       

        }   catch (Exception e){
            log.error("$$$$$$$$ getStatuses in newEntry " + e );
        }

        ret.setItems(rel);
        return Response.status(200).entity(ret).build();
    }


    @POST
    @Path(value = "/batch")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response getOperators(BatchContainer op) {
        BatchResponse ret = new BatchResponse();
        List<ResponseItem> rel = new ArrayList<ResponseItem>();

        try{
            for(Operation o : op.getItems()) {
                ResponseItem tempResponse = new ResponseItem();
                tempResponse.setNumber( o.getNumber() );
                log.warn("$$$$$$$$ o.getMethod() = " + o.getMethod());
                log.warn("$$$$$$$$ o.getElement() = " + o.getElement());

                NamedEntityXml tempEl = o.getElement();
                NamedEntityXml retEl = null;
                if(tempEl != null)    {
                    if(tempEl instanceof ActionXml) {
                        ActionXml t = (ActionXml) tempEl;
                        if( t.getProcess() != null) {
                            retEl = (ActionXml)repoMng.getActionRepository( t.getProcess().getId() ).findOne( t.getId() );
                        }
                    }   else if(tempEl instanceof StatusXml) {
                        StatusXml t = (StatusXml) tempEl;
                        if( t.getProcess() != null) {
                            retEl = (StatusXml)repoMng.getStatusRepository( t.getProcess().getId() ).findOne( t.getId() );
                        }
                    }   else if(tempEl instanceof ActivityXml) {
                        ActivityXml t = (ActivityXml) tempEl;
                        if( t.getProcess() != null) {
                            retEl = (ActivityXml)repoMng.getActivityRepository( t.getProcess().getId() ).findOne( t.getId() );
                        }
                    }
                }

                tempResponse.setElement(retEl);
                rel.add(tempResponse);
            }

        }   catch (Exception e){
            log.error("$$$$$$$$ getStatuses in newEntry " + e );
        }

        ret.setItems(rel);
        return Response.status(200).entity(ret).build();
    }

	@GET
	@Path(value = "/{id}/statuses")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public JaxbList<StatusXml> getStatuses(@PathParam(value = "id") String processId,
                                           @QueryParam(value = "id") String id) {
        log.warn("$$$$ axbList<ActivityXml> getStatuses called id =  " + processId);
		ProcessRepository<ProcessXml> processes = (ProcessRepository<ProcessXml>) repoMng.getProcessRepository();
		
		JaxbList<StatusXml> p = new JaxbList<StatusXml>();
		List<StatusXml> la = new ArrayList<StatusXml>();
        List<Status> tempList = new ArrayList<Status>();

		StatusRepository<? extends Status> r = repoMng.getStatusRepository(processId);

        try{
            Process temp = (Process) processes.findOne(processId);

            log.warn("$$$$ getStatuses r.count() =  " +  r.count());
            log.warn("$$$$ getStatuses process "+temp.toString());

            List<Status>  a;

            if(id!=null)   {
                Status stsTemp = r.findOne(id);
                tempList.add(stsTemp);
                a = tempList;
            }  else{
                a = (List<Status>) r.findAll(temp, 0, (int) r.count(), null, false);
            }

            log.warn("$$$$ getStatuses List<Status>  a.size() -  "+a.size());

            for (Status t : a){
                log.warn("$$$$ in loop t =  " +  t + "t.getclass() = " + t.getClass());
                if(t instanceof StatusXml ){
                    log.warn("t instanceof StatusXml");
                    la.add((StatusXml)t);
                }
                else if(t instanceof Status ){
                    StatusXml st = new StatusXml();
                    st.fromInterface(t);
                    log.warn("t instanceof Status fromInterface = " + st);
                    la.add(st);
                }   else{
                    log.error("$$$$   Unknown Status type");
                }
            }
            p.setItems(la);

        }   catch (Exception e){
            log.error("$$$$$$$$ getStatuses in newEntry " + e );
        }

		return p;
	}

	@GET
	@Path(value = "/{id}/actions")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public JaxbList<ActionXml> getActions(@PathParam(value = "id") String processId, @QueryParam(value = "ByFromStatus") String statusId,
                                          @QueryParam(value = "id") String id) {
		ProcessRepository<ProcessXml> processes = (ProcessRepository<ProcessXml>) repoMng.getProcessRepository();
        log.warn("$$$$ JaxbList<ActionXml> getActions called ID = "+processId + " ByFromStatus= " + statusId);
		
		JaxbList<ActionXml> p = new JaxbList<ActionXml>();
		List<ActionXml> la = new ArrayList<ActionXml>();
        List<Action> tempList = new ArrayList<Action>();
		ActionRepository<? extends Object> r = repoMng.getActionRepository(processId);

        long r_kol= r.count();
        log.warn("$$$$ getActions count record - " + r_kol);

        try{

            log.warn("$$$$ getActions findOne process ID - " + processId);
            Process temp = (Process) processes.findOne(processId);
            if(temp!=null)  log.warn("$$$$ getActions findOne get process " + temp.toString());
            else log.warn("$$$$ getActions NO findOne process ID - " + processId);

    /*
            List<Object>  a = (List<Object>) r.findAll(temp, 0, (int) r.count(), null, false);
            log.warn(" JaxbList<ActionXml> r.findAll !!!!!!!!!!!!!!!!!!!!!!!!!! a.size() - " + a.size());

            for (Object t : a){
    */
            if(temp!=null){
                log.warn("$$$$ findAll  Action");
                Iterator<? extends Action> a;
                if(statusId!=null)   {
                    a = r.findFromStatus(temp,statusId).iterator();
                }   else if(id!=null)   {
                    Action actTemp = r.findOne(id);
                    tempList.add(actTemp);
                    a = tempList.iterator();
                }
                else{
                    a = r.findAll(temp, 0, (int) r_kol, null, false).iterator();
                }

                log.warn("$$$$ findAll  Action end");
                while ( a.hasNext() ){
                    Object t = a.next();
                    log.warn("$$$$   in loop Action t = " + t.getClass());
                    if(t instanceof StatusChangeActionXml )      {
                        log.warn("$$$$   la.add((StatusChangeActionXml)t);");
                        la.add((StatusChangeActionXml)t);
                    }
                    else if(t instanceof NoStatusActionXml){
                        log.warn("$$$$   la.add((NoStatusActionXml)t);");
                        la.add((NoStatusActionXml)t);
                    }
                    else if(t instanceof StatusChangeAction ){
                        StatusChangeActionXml st = new StatusChangeActionXml();
                        st.fromInterface((StatusChangeAction)t);
                        log.warn("$$$$   StatusChangeActionXml");
                        if(((StatusChangeAction)t).getFromStatus()==null)
                            log.warn("$$$$   ((StatusChangeAction)t).getFromStatus()==null) ");
                        else
                            log.warn("$$$$   ((StatusChangeAction)t).getFromStatus()!=null) "+((StatusChangeAction)t).getFromStatus().toString());
                        if (st.getFromStatus()==null)  log.warn("$$$$   fromInterface do not work ");
                        la.add(st);
                    }
                    else if(t instanceof NoStatusAction ){
                        NoStatusActionXml st = new NoStatusActionXml();
                        st.fromInterface((NoStatusAction)t);
                        log.warn("$$$$   NoStatusActionXml");
                        la.add(st);
                    } else {
                        log.error("$$$$   Unknown Action type");
                    }
                }

                p.setItems(la);
                }

        }   catch (Exception e){
            log.error("$$$$$$$$ getActions in newEntry " + e );
        }
		return p;

	}

	@GET
	@Path(value = "/{processId}/activities")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public JaxbList<ActivityXml> getActivities(@PathParam(value = "processId") String processId, @QueryParam(value = "ByAction") String actionId,
                                               @QueryParam(value = "ByStatus") String statusId, @QueryParam(value = "id") String id) {
        log.warn("$$$$ axbList<ActivityXml> getActivities called processId =  " + processId);
		ProcessRepository<Process> processes = (ProcessRepository<Process>) repoMng.getProcessRepository();		
		
		JaxbList<ActivityXml> p = new JaxbList<ActivityXml>();
		List<ActivityXml> la = new ArrayList<ActivityXml>();
        List<Activity> tempList = new ArrayList<Activity>();
		ActivityRepository<? extends Activity> r = repoMng.getActivityRepository(processId);

        try{

            Process temp = (Process) processes.findOne(processId);

            log.warn("$$$$ r.count() =  " +  r.count());
            List<Activity>  a;

            if(actionId != null)            {
                a = (List<Activity>) repoMng.getActionRepository(processId).findActivity(temp, actionId);
            } else if(statusId != null){
                a = (List<Activity>) repoMng.getStatusRepository(processId).findActivity(temp, statusId);
            } else if(id != null){
                Activity actTemp = r.findOne(id);
                tempList.add(actTemp);
                a = tempList;
            } else{
                a = (List<Activity>) r.findAll(temp, 0, (int) r.count(), null, false);
            }
            for (Activity t : a){
                log.warn("$$$$ in loop t =  " +  t + "t.getclass() = " + t.getClass());
                //Xml
                if(t instanceof InvokeScriptActivityXml )     {
                    log.warn("t instanceof InvokeScriptActivityXml");
                    la.add((InvokeScriptActivityXml)t);
                }
                else if(t instanceof RemoteTransactionActivityXml )     {
                    log.warn("t instanceof RemoteTransactionActivityXml");
                    la.add((RemoteTransactionActivityXml)t);
                }
                else if(t instanceof SendMailActivityXml )     {
                    log.warn("t instanceof SendMailActivityXml");
                    la.add((SendMailActivityXml)t);
                }
                else if(t instanceof InvokeMethodActivityXml ) {
                    log.warn("t instanceof InvokeMethodActivityXml");
                    la.add((InvokeMethodActivityXml)t);
                }
                else if(t instanceof ParametrizedPropertyLocalActivityXml ) {
                    log.warn("t instanceof ParametrizedPropertyLocalActivityXml");
                    la.add((ParametrizedPropertyLocalActivityXml)t);
                }
                else if(t instanceof SetPropertyActivityXml )   {
                    log.warn("t instanceof SetPropertyActivityXml");
                    la.add((SetPropertyActivityXml)t);
                }
                else if(t instanceof ActivityXml )     {
                    log.warn("t instanceof ActivityXml");
                    la.add((ActivityXml)t);
                } else {
                    log.error("Unknown Activity type");
                }
                //Entity
                /*
                if(t instanceof RemoteTransactionActivity )     {
                    RemoteTransactionActivityXml st = new RemoteTransactionActivityXml();
                    st.fromInterface(t);
                    log.warn("t instanceof RemoteTransactionActivity fromInterface = " + st);
                    la.add(st);
                }
                else if(t instanceof SendMailActivity ){
                    SendMailActivityXml st = new SendMailActivityXml();
                    st.fromInterface(t);
                    log.warn("t instanceof SendMailActivity fromInterface = " + st);
                    la.add(st);
                }
                else if(t instanceof InvokeMethodActivity ){
                    InvokeMethodActivityXml st = new InvokeMethodActivityXml();
                    st.fromInterface(t);
                    log.warn("t instanceof InvokeMethodActivity fromInterface = " + st);
                    la.add(st);
                }
                else if(t instanceof ParametrizedPropertyLocalActivity ){
                    ParametrizedPropertyLocalActivityXml st = new ParametrizedPropertyLocalActivityXml();
                    st.fromInterface(t);
                    log.warn("t instanceof ParametrizedPropertyLocalActivity fromInterface = " + st);
                    la.add(st);
                }
                else if(t instanceof SetPropertyActivity ){
                    SetPropertyActivityXml st = new SetPropertyActivityXml();
                    st.fromInterface(t);
                    log.warn("t instanceof SetPropertyActivity fromInterface = " + st);
                    la.add(st);
                }
                else if(t instanceof Activity ){
                    ActivityXml st = new ActivityXml();
                    st.fromInterface(t);
                    log.warn("t instanceof Activity fromInterface = " + st);
                    la.add(st);
                } else {
                    log.error("Unknown Activity type");
                }
                */

            }
            p.setItems(la);

        }   catch (Exception e){
            log.error("$$$$$$$$ getActivities in newEntry " + e );
        }

		return p;
	}

    @GET
    @Path(value = "/{process_id}/statuses/{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public StatusXml getStatus(@PathParam(value = "process_id") String process_id, @PathParam(value = "id") String id) {
        log.warn("$$$$ getProcess called id =  " + id);
        Process p =  repoMng.getProcessRepository().findOne(process_id);
        if(p == null){
            return null;            
        }

        Status t = repoMng.getStatusRepository(p.getId()).findOne(id);

        if(t == null){
            return null;
        }

        log.warn("$$$$ getStatus" +  t);
        if(t instanceof StatusXml ){
            log.warn("t instanceof StatusXml");
            return (StatusXml)t;
        }
        else if(t instanceof Status ){
            StatusXml st = new StatusXml();
            st.fromInterface(t);
            log.warn("t instanceof Status fromInterface = " + st);
            return st;
        }   else{
            log.error("$$$$   Unknown Status type");
        }

        return null;
    }

    @GET
    @Path(value = "/{process_id}/actions/{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public ActionXml getAction(@PathParam(value = "process_id") String process_id, @PathParam(value = "id") String id) {
        log.warn("$$$$ getProcess called id =  " + id);
        Process p =  repoMng.getProcessRepository().findOne(process_id);
        if(p == null){
            return null;
        }

        Action t = repoMng.getActionRepository(p.getId()).findOne(id);

        if(t == null){
            return null;
        }

        log.warn("$$$$ getAction t = " + t);
        if(t instanceof StatusChangeActionXml )      {
            log.warn("$$$$   la.add((StatusChangeActionXml)t);");
            return (StatusChangeActionXml)t;
        }
        else if(t instanceof NoStatusActionXml){
            log.warn("$$$$   la.add((NoStatusActionXml)t);");
            return (NoStatusActionXml)t;
        }
        else if(t instanceof StatusChangeAction ){
            StatusChangeActionXml st = new StatusChangeActionXml();
            st.fromInterface((StatusChangeAction)t);
            log.warn("$$$$   StatusChangeActionXml");
            return st;
        }
        else if(t instanceof NoStatusAction ){
            NoStatusActionXml st = new NoStatusActionXml();
            st.fromInterface((NoStatusAction)t);
            log.warn("$$$$   NoStatusActionXml");
            return st;
        } else {
            log.error("$$$$   Unknown Action type");
        }

        return null;
    }



    @POST
	@Path(value = "/{id}/statuses/{status_id}")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response createStatus(StatusXml status, @QueryParam("session") String session) {
        log.warn("$$$$   createStatus " + status + " session= " + session);
		return newEntry(status, session);
	}

	@POST
	@Path(value = "/{id}/actions/no_status_action/{action_id}")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response createNoStatusAction(NoStatusActionXml action, @QueryParam("session") String session) {
        log.warn("$$$$   createNoStatusAction " + action);
		return newEntry(action, session);
	}
	
	@POST
	@Path(value = "/{id}/actions/status_change_action/{action_id}")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response createStatusChangeAction(StatusChangeActionXml action, @QueryParam("session") String session) {
        log.warn("$$$$ createStatusChangeAction " + action + " session= " + session);
		return newEntry(action, session);
	}

	@POST
	@Path(value = "/{id}/activities/blank_activity/{activity_id}")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response createActivity(ActivityXml activity, @QueryParam("session") String session) {
        log.warn("$$$$   createActivity " + activity + " session= " + session);
		return newEntry(activity, session);
	}

    @POST
    @Path(value = "/{id}/activities/remote_transaction_activity/{activity_id}")
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response createRemoteTransactionActivityXml(RemoteTransactionActivityXml activity, @QueryParam("session") String session) {
        log.warn("$$$$   RemoteTransactionActivityXml " + activity + " session= " + session);
        return newEntry(activity, session);
    }

    @POST
    @Path(value = "/{id}/activities/invoke_script_activity/{activity_id}")
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response createInvokeScriptActivity(InvokeScriptActivityXml activity, @QueryParam("session") String session) {
        log.warn("$$$$   InvokeScriptActivity " + activity);
        return newEntry(activity, session);
    }
	
	@POST
	@Path(value = "/{id}/activities/send_mail_activity/{activity_id}")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response createSendMailActivity(SendMailActivityXml activity, @QueryParam("session") String session) {
        log.warn("$$$$   createSendMailActivity " + activity);
		return newEntry(activity, session);
	}
	
	@POST
	@Path(value = "/{id}/activities/set_property_activity/{activity_id}")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response createSetPropertyActivity(SetPropertyActivityXml activity, @QueryParam("session") String session) {
        log.warn("$$$$   createSetPropertyActivity " + activity);
		return newEntry(activity, session);
	}
	
	@POST
	@Path(value = "/{id}/activities/invoke_method_activity/{activity_id}")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response createInvokeMethodActivity(InvokeMethodActivityXml activity, @QueryParam("session") String session) {
        log.warn("$$$$   createInvokeMethodActivity " + activity);
		return newEntry(activity, session);
	}
	
	@POST
	@Path(value = "/{id}/activities/parametrized_property_local_activity/{activity_id}")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response createParametrizedPropertyLocalActivity(ParametrizedPropertyLocalActivityXml activity, @QueryParam("session") String session) {
        log.warn("$$$$   createParametrizedPropertyLocalActivity " + activity);
		return newEntry(activity, session);
	}
	
	

	@PUT
	@Path(value = "/{id}/statuses/{status_id}")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response updateStatus(StatusXml status, @QueryParam("session") String session) {
        log.warn("$$$$   updateStatus " + status);
		return updateEntry(status, session);
	}

	
	@PUT
	@Path(value = "/{id}/actions/no_status_action/{action_id}")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response updateNoStatusAction(NoStatusActionXml action, @QueryParam("session") String session) {
        log.warn("$$$$   NoStatusActionXml " + action);
		return updateEntry(action, session);
	}
	
	@PUT
	@Path(value = "/{id}/actions/status_change_action/{action_id}")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response updateStatusChangeAction(StatusChangeActionXml action, @QueryParam("session") String session) {
        log.warn("$$$$   updateStatusChangeAction " + action);
		return updateEntry(action, session);
	}
	

	@PUT
	@Path(value = "/{id}/activities/blank_activity/{activity_id}")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response updateActivity(ActivityXml activity, @QueryParam("session") String session) {
        log.warn("$$$$   updateActivity " + activity);
		return updateEntry( activity, session);
	}

    @PUT
    @Path(value = "/{id}/activities/remote_transaction_activity/{activity_id}")
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response updateRemoteTransactionActivityXml(RemoteTransactionActivityXml activity, @QueryParam("session") String session) {
        log.warn("$$$$   RemoteTransactionActivityXml " + activity);
        return updateEntry(activity, session);
    }

    @PUT
    @Path(value = "/{id}/activities/invoke_script_activity/{activity_id}")
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response updateInvokeScriptActivity(InvokeScriptActivityXml activity, @QueryParam("session") String session) {
        log.warn("$$$$   InvokeScriptActivity " + activity);
        return updateEntry(activity, session);
    }
	
	@PUT
	@Path(value = "/{id}/activities/send_mail_activity/{activity_id}")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response updateSendMailActivity(SendMailActivityXml activity, @QueryParam("session") String session) {
        log.warn("$$$$   updateSendMailActivity " + activity);
		return updateEntry(activity, session);
	}
	
	@PUT
	@Path(value = "/{id}/activities/set_property_activity/{activity_id}")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response updateSetPropertyActivity(SetPropertyActivityXml activity, @QueryParam("session") String session) {
        log.warn("$$$$   updateSetPropertyActivity " + activity);
		return updateEntry(activity, session);
	}
	
	@PUT
	@Path(value = "/{id}/activities/invoke_method_activity/{activity_id}")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response updateInvokeMethodActivity(InvokeMethodActivityXml activity, @QueryParam("session") String session) {
        log.warn("$$$$   updateInvokeMethodActivity " + activity);
		return updateEntry(activity, session);
	}
	
	@PUT
	@Path(value = "/{id}/activities/parametrized_property_local_activity/{activity_id}")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response updateParametrizedPropertyLocalActivity(ParametrizedPropertyLocalActivityXml activity, @QueryParam("session") String session) {
        log.warn("$$$$   updateParametrizedPropertyLocalActivity " + activity);
		return updateEntry(activity, session);
	}
	

	@DELETE
	@Path(value = "/{id}/statuses/{status_id}")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response deleteStatus(@PathParam(value = "id") String provessId, @PathParam(value = "status_id") String id,
                                 @QueryParam("session") String session) {
        log.warn("$$$$   deleteStatus id = " + id + " provessId = " + provessId);
		return deleteEntryById(repoMng.getStatusRepository(provessId), id, session);
	}

	@DELETE
	@Path(value = "/{id}/actions/status_change_action/{action_id}")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response deleteStatusChangeAction(@PathParam(value = "id") String provessId, @PathParam(value = "action_id") String id,
                                             @QueryParam("session") String session) {
        log.warn("$$$$   deleteStatusChangeAction id = " + id + " provessId = " + provessId);
		ActionRepository<ActionXml> actions = (ActionRepository<ActionXml>) repoMng.getActionRepository(provessId);
		return deleteEntryById(actions, id, session);
	}
	
	@DELETE
	@Path(value = "/{id}/actions/no_status_action/{action_id}")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response deleteNoStatusAction(@PathParam(value = "id") String provessId, @PathParam(value = "action_id") String id,
                                         @QueryParam("session") String session) {
        log.warn("$$$$   deleteNoStatusAction id = " + id + " provessId = " + provessId);
		ActionRepository<ActionXml> actions = (ActionRepository<ActionXml>) repoMng.getActionRepository(provessId);
		return deleteEntryById(actions, id, session);
	}

	@DELETE
	@Path(value = "/{id}/activities/blank_activity/{activity_id}")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response deleteActiviti(@PathParam(value = "id") String provessId, @PathParam(value = "activity_id") String id,
                                   @QueryParam("session") String session) {
        log.warn("$$$$   deleteActiviti id = " + id + " provessId = " + provessId);
		ActivityRepository<ActivityXml> activities = (ActivityRepository<ActivityXml>) repoMng.getActivityRepository(provessId);
		return deleteEntryById(activities, id, session);
	}

    @DELETE
    @Path(value = "/{id}/activities/remote_transaction_activity/{activity_id}")
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response deleteRemoteTransactionActivity(@PathParam(value = "id") String provessId, @PathParam(value = "activity_id") String id,
                                   @QueryParam("session") String session) {
        log.warn("$$$$   deleteRemoteTransactionActivity id = " + id + " provessId = " + provessId);
        ActivityRepository<ActivityXml> activities = (ActivityRepository<ActivityXml>) repoMng.getActivityRepository(provessId);
        return deleteEntryById(activities, id, session);
    }

    @DELETE
    @Path(value = "/{id}/activities/invoke_script_activity/{activity_id}")
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response deleteInvokeScriptActivity(@PathParam(value = "id") String provessId, @PathParam(value = "activity_id") String id,
                                           @QueryParam("session") String session) {
        log.warn("$$$$   InvokeScriptActivity id = " + id + " provessId = " + provessId);
        ActivityRepository<ActivityXml> activities = (ActivityRepository<ActivityXml>) repoMng.getActivityRepository(provessId);
        return deleteEntryById(activities, id, session);
    }
	
	@DELETE
	@Path(value = "/{id}/activities/send_mail_activity/{activity_id}")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response deleteSendMailActivity(@PathParam(value = "id") String provessId, @PathParam(value = "activity_id") String id,
                                           @QueryParam("session") String session) {
        log.warn("$$$$   deleteSendMailActivity id = " + id + " provessId = " + provessId);
		ActivityRepository<ActivityXml> activities = (ActivityRepository<ActivityXml>) repoMng.getActivityRepository(provessId);
		return deleteEntryById(activities, id, session);
	}
	
	@DELETE
	@Path(value = "/{id}/activities/set_property_activity/{activity_id}")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response deleteSetPropertyActivity(@PathParam(value = "id") String provessId, @PathParam(value = "activity_id") String id,
                                              @QueryParam("session") String session) {
        log.warn("$$$$   deleteSetPropertyActivity id = " + id + " provessId = " + provessId);
		ActivityRepository<ActivityXml> activities = (ActivityRepository<ActivityXml>) repoMng.getActivityRepository(provessId);
		return deleteEntryById(activities, id, session);
	}
	
	@DELETE
	@Path(value = "/{id}/activities/invoke_method_activity/{activity_id}")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response deleteInvokeMethodActivity(@PathParam(value = "id") String provessId, @PathParam(value = "activity_id") String id,
                                               @QueryParam("session") String session) {
        log.warn("$$$$   deleteInvokeMethodActivity id = " + id + " provessId = " + provessId);
		ActivityRepository<ActivityXml> activities = (ActivityRepository<ActivityXml>) repoMng.getActivityRepository(provessId);
		return deleteEntryById(activities, id, session);
	}
	
	@DELETE
	@Path(value = "/{id}/activities/parametrized_property_local_activity/{activity_id}")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response deleteParametrizedPropertyLocalActivity(@PathParam(value = "id") String provessId, @PathParam(value = "activity_id") String id,
                                                            @QueryParam("session") String session) {
        log.warn("$$$$   deleteParametrizedPropertyLocalActivity id = " + id + " provessId = " + provessId);
		ActivityRepository<ActivityXml> activities = (ActivityRepository<ActivityXml>) repoMng.getActivityRepository(provessId);
		return deleteEntryById(activities, id, session);
	}


    @POST
    @Path(value = "/{id}/sessions/{session_id}")
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response createSession(@PathParam(value = "id") String provessId, @PathParam(value = "session_id") String sessionId) {
        log.warn("$$$$   createSession provessId = " + provessId );
        String s = sessionManagerBean.createSession(provessId, null);
        if(s == null){
            return Response.status(450).build();
        }
        return Response.status(201).entity(s).build();
    }

    @GET
    @Path(value = "/{id}/sessions/{session_id}")
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response updateSession(@PathParam(value = "id") String provessId, @PathParam(value = "session_id") String sessionId) {
        log.warn("$$$$   updateSession provessId = " + provessId);
        if(!sessionManagerBean.updateSession(provessId,sessionId)){
            return Response.status(450).build();
        }
        return Response.status(200).build();
    }

    @DELETE
    @Path(value = "/{id}/sessions/{session_id}")
    @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response deleteSession(@PathParam(value = "id") String provessId, @PathParam(value = "session_id") String sessionId) {
        log.warn("$$$$   deleteSession sessionId = " + sessionId + " provessId = " + provessId);
        sessionManagerBean.closeSession(sessionId);
        return Response.status(200).build();
    }




}
