package ru.efive.workflow.designer.web.rest;

import ru.efive.workflow.designer.repository.ProcessRepository;
import ru.efive.workflow.designer.interfaces.Process;
import ru.efive.workflow.designer.repository.ActionRepository;
import ru.efive.workflow.designer.interfaces.Action;
import ru.efive.workflow.designer.repository.ActivityRepository;
import ru.efive.workflow.designer.interfaces.Activity;
import ru.efive.workflow.designer.repository.StatusRepository;
import ru.efive.workflow.designer.interfaces.Status;

public interface RepositoryManager {
	
	public ProcessRepository<? extends Process> getProcessRepository();
    public ProcessRepository<? extends Process> getProcessRepositoryXml();

    public ActionRepository<? extends Action> getActionRepository(String processId);
	
	public ActivityRepository<? extends Activity> getActivityRepository(String processId);
	
	public StatusRepository<? extends Status> getStatusRepository(String processId);
	
	public String getProcessByAction(String id);
	public String getProcessByActivity(String id);
	public String getProcessByStatus(String id);	

}
