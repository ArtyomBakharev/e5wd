package ru.efive.workflow.designer.web.rest;

import org.apache.log4j.Logger;
import ru.efive.workflow.designer.interfaces.Action;
import ru.efive.workflow.designer.interfaces.Activity;
import ru.efive.workflow.designer.interfaces.Process;
import ru.efive.workflow.designer.interfaces.Status;
import ru.efive.workflow.designer.model.ActionXml;
import ru.efive.workflow.designer.model.ActivityXml;
import ru.efive.workflow.designer.model.ProcessXml;
import ru.efive.workflow.designer.model.StatusXml;
import ru.efive.workflow.designer.repository.*;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Singleton;
import javax.ejb.Startup;


@Startup
@Singleton(name = "RepositoryManagerBean")
@Local(RepositoryManager.class)
public class RepositoryManagerBean implements RepositoryManager {
    private static Logger log = Logger.getLogger(RepositoryManagerBean.class);
	@EJB
	private ProcessRepository<ProcessXml> processes;

	@EJB
	private StatusRepository<StatusXml> statuses;

	@EJB
	private ActionRepository<ActionXml> actions;

	@EJB
	private ActivityRepository<ActivityXml> activities;
	
    @EJB
    private ProcessRepositoryDAO<Process> processesDAOImpl;
    @EJB
	private ActionRepositoryDAO<Action> actionsDAOImpl;
	@EJB
	private ActivityRepositoryDAO<Activity> activitysDAOImpl;
	@EJB
	private StatusRepositoryDAO<Status> statusesDAOImpl;
	

	@Override
	public ProcessRepository<? extends Process> getProcessRepository() {
        log.warn("$$$$   getProcessRepository");
		return processesDAOImpl;
	}
    @Override
    public ProcessRepository<? extends Process> getProcessRepositoryXml() {
        log.warn("$$$$   getProcessRepositoryXml");
        return processes;
    }

    @Override
	public ActionRepository<? extends Action> getActionRepository(String processId) {
        log.warn("$$$$   getActionRepository processId = " + processId);
        Process p = processesDAOImpl.findOne(processId);
        log.warn("$$$$   processesDAOImpl.findOne(processId) = " + p + "p.getRepository() = " + p.getRepository());
		if(p.getRepository() == 0)
			return actions;
		return actionsDAOImpl;
	}

	@Override
	public ActivityRepository<? extends Activity> getActivityRepository(String processId) {
        log.warn("$$$$   getActivityRepository processId = " + processId);
		Process p = processesDAOImpl.findOne(processId);
        log.warn("$$$$   processesDAOImpl.findOne(processId) = " + p + "p.getRepository() = " + p.getRepository());
		if(p.getRepository() == 0)
			return activities;
		return activitysDAOImpl;
	}

	@Override
	public StatusRepository<? extends Status> getStatusRepository(String processId) {
        log.warn("$$$$   getStatusRepository processId = " + processId);
        Process p = processesDAOImpl.findOne(processId);
        log.warn("$$$$   processesDAOImpl.findOne(processId) = " + p + "p.getRepository() = " + p.getRepository());
		if(p.getRepository() == 0)
			return statuses;
		return statusesDAOImpl;
	}
	
	@Override
	public	String getProcessByAction(String id)
	{
		Action a = actions.findOne(id);
		if(a != null)
			return a.getProcess().getId();
		
		a = actionsDAOImpl.findOne(id);
		if(a != null)
			return a.getProcess().getId();
		
		return null;
	}
	
	@Override
	public String getProcessByActivity(String id)
	{
		Activity a = activities.findOne(id);
		if(a != null)
			return a.getProcess().getId();
		
		a = activitysDAOImpl.findOne(id);
		if(a != null)
			return a.getProcess().getId();
		
		return null;		
	}
	
	@Override
	public String getProcessByStatus(String id)
	{
		Status s = statuses.findOne(id);
        log.warn("$$$$getProcessByStatus id = " + id);
		if(s != null)
			return s.getProcess().getId();

		s = statusesDAOImpl.findOne(id);
		if(s != null)
			return s.getProcess().getId();
		
		return null;		
	}

}
