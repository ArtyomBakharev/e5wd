package ru.efive.workflow.designer.web.rest;

import ru.efive.workflow.designer.model.BaseEntityXml;

/**
 * SessionManager interface
 *
 * @author Pavel Shashok
 *
 */
public interface SessionManager {

    public String createSession(String process, String id);

    public void onProcessRemove(String process);

    public boolean updateSession(String process, String session);

    public void closeSession(String session);

    public boolean canAccess(String process, String session);

    public boolean createCid(String cid, BaseEntityXml id);

    public BaseEntityXml getByCid(String cid);
}
