package ru.efive.workflow.designer.web.rest;

import org.apache.log4j.Logger;
import ru.efive.workflow.designer.model.BaseEntityXml;

import javax.ejb.Local;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.StringTokenizer;

import static java.lang.Math.abs;

/**
 * SessionManagerBean implements SessionManager
 *
 * @author Pavel Shashok
 *
 */
@Startup
@Singleton(name = "SessionManagerBean")
@Local(SessionManager.class)
public class SessionManagerBean implements SessionManager {
    protected static Logger log = Logger.getLogger(SessionManagerBean.class);
    
    private static long timeOffset = 1000 * 60;
    private Random random = new Random();

    private Map<String,String> processSessionMap = new HashMap<String, String>();
    private Map<String,Long> sessionTimeMap = new HashMap<String, Long>();
    private Map<String,Map<String,BaseEntityXml>> cidElMap = new HashMap<String,Map<String,BaseEntityXml>>();

    public String createSession(String process, String id){
        log.warn("$$$ createSession process = " + process);
        String s = processSessionMap.get(process);
        if(sessionTimeMap.containsKey(s))     {
            if(s != null){
                if(canAccess(process,s)){
                    log.warn("$$$ cant access to process = " + process);
                    return null;
                }
            }
        }
        if(id == null){
            int rInt = random.nextInt();    
            s = Integer.toHexString(rInt);
        }   else{
            s = id;
        }
        log.warn("$$$ canAccess session = " + s);
        sessionTimeMap.put(s, System.currentTimeMillis());
        processSessionMap.put(process, s);
        return s;
    }

    public void onProcessRemove(String process){
        String s = processSessionMap.get(process);
        if(s == null){
            return;
        }
        closeSession(s);
    }

    public boolean updateSession(String process, String session){
        if(canAccess(process,session) || session.equals(processSessionMap.get(process))){
            sessionTimeMap.put(session, System.currentTimeMillis());
            return true;
        }
        return false;
    }

    public void closeSession(String session){
        sessionTimeMap.remove(session);
    }

    public boolean canAccess(String process, String session){
        log.warn("$$$ canAccess process = " + process + " session = " + session);
        if(process == null ){
            log.warn("$$$ process == null");
            return false;
        }
        String s = processSessionMap.get(process);
        if(s == null) {
            log.warn("$$$ processSessionMap.get(process) == null");
            if(createSession(process,session) != null) {
                return true;
            }
            return false;
        } else if(!s.equals(session))  {
            log.warn("$$$ bad session");
            return false;
        }
        
        if( abs( sessionTimeMap.get(session) - System.currentTimeMillis() ) > timeOffset ) {
            log.warn("$$$ time out");
            return false;
        }
        
        return true;
    }

    @Override
    public boolean createCid(String cid,BaseEntityXml el) {
        log.warn("$$$ createCid cid = " + cid + " id = " + el );

        if(cid == null)
            return false;

        StringTokenizer st = new StringTokenizer(cid, "-");

        if(!st.hasMoreTokens()){
            return false;
        }

        if(!"cid".equals(st.nextToken())) {
            return false;
        }

        if(!st.hasMoreTokens()){
            return false;
        }

        String session = st.nextToken();

        log.warn("$$$ session = " + session);

        Map<String, BaseEntityXml> cidEl = cidElMap.get(session);

        if(cidEl == null){
            cidEl = new HashMap<String,BaseEntityXml>();
            cidElMap.put(session, cidEl);
        }

        cidEl.put(cid, el);

        log.warn("return true");
        return true;
    }

    @Override
    public BaseEntityXml getByCid(String cid) {
        log.warn("$$$ getIdByCid cid = " + cid);

        if(cid == null)
            return null;

        StringTokenizer st = new StringTokenizer(cid, "-");

        if(!st.hasMoreTokens()){
            log.warn("$$$ !st.hasMoreTokens()");
            return null;
        }

        if(!"cid".equals(st.nextToken())) {
            log.warn("$$$ !\"cid\".equals(st.nextToken())");
            return null;
        }

        if(!st.hasMoreTokens()){
            return null;
        }

        String session = st.nextToken();

        log.warn("$$$ session = " + session);

        Map<String, BaseEntityXml> cidEl = cidElMap.get(session);

        if(cidEl == null){
            log.warn("$$$ cidEl == null");
            return null;
        }

        log.warn("$$$ return cidId.get(cid");
        return cidEl.get(cid);
    }
}
