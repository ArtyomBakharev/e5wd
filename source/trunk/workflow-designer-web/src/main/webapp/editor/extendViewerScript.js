efwdViewer.registerExtender({
	"extend": {
		"status": {
			"style": {
				"shared": {
					"box": {
						"r": 30
					}
				}
			}
		}
	},
	"replace": {
		"actionArrow.style.line.attrs": {
			"stroke": "#8080FF",
			"stroke-width": 5,
			"stroke-linecap": "square"
		}
	}
});
